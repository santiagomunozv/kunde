<p>
    No se puede eliminar el registro debido a que ya esta relacionado con información histórica en los siguientes módulos del sistema:
</p>
<table class="table table-hover table-sm scalia-grid dataTable">
    <thead class="bg-primary text-light">  
        <tr>
            <td>Formulario</td>
            <td>Campo</td>
        </tr>
    </thead>
    <tbody>


    @foreach($tablas as $dato)
        <tr>
            <td>{{$dato->Tabla}}</td>
            <td>{{$dato->Campo}}</td> 
        </tr>
    @endforeach
    </tbody>
</table>
