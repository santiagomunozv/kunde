<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-sm-6">
        {!!Form::label('fechaInicialCreacion', 'Fecha inicial de creación', array('class' => 'text-sm text-primary mb-1')) !!}
        {!!Form::date('fechaInicialCreacion',null,['class'=>'form-control','readonly'])!!}
    </div>
    <div class="col-sm-6">
        {!!Form::label('fechaFinalCreacion','Fecha Final de creación', array('class' => 'text-sm text-primary mb-1')) !!}
        {!!Form::text('fechaFinalCreacion',null,[ 'class'=>'form-control','readonly'])!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!Form::label('clasificacionTerceroTipo', 'Clasificación Tercero (Tipo Tercero)', array('class' => 'text-sm text-primary mb-1')) !!}
        {!!Form::select('clasificacionTerceroTipo[]',['1'=>'Cliente','2'=>'Proveedor','3'=>'Empleado'],null,['class'=>'chosen-select','id'=>'clasificacionTerceroTipo','multiple'])!!}
    </div>
    <div class="col-sm-6">
        {!!Form::label('clasificacionTercero','Clasificación', array('class' =>'text-sm text-primary mb-1')) !!}
        {!!Form::select('clasificacionTercero[]',['Excluido'=>'Excluido','Eventual'=>'Eventual','Consumo'=>'Consumo','Seguimiento'=>'Seguimiento'],null,['class'=>'chosen-select','multiple'])!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!Form::label('CompaniaTercero', 'Compañia Tercero', array('class' => 'text-sm text-primary mb-1')) !!}
        {!!Form::select('CompaniaTercero[]',['2'=>'Iblu','3'=>'Extiblu'],null,['class'=>'chosen-select','id'=>'CompaniaTercero','multiple'])!!}
    </div>
    <div class="col-sm-6">
        {!!Form::label('tipoServicio', 'Tipo servicio', array('class' => 'text-sm text-primary mb-1')) !!}
        {!!Form::select('tipoServicio[]',['Confeccion'=>'Confección','Comercializacion'=>'Comercialización','Logistica'=>'Logística'],null,['class'=>'chosen-select','id'=>'tipoServicio','multiple'])!!}
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        {!!Form::label('EstadoTercero','Estado Tercero', array('class' =>'text-sm text-primary mb-1')) !!}
        {!!Form::select('EstadoTercero',['Activo'=>'Activo','Inactivo'=>'Inactivo',],null,['class'=>'form-control'])!!}
    </div>
</div>