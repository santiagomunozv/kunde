@extends("layouts.principal")
@section('nombreModulo')
    Inicio
@endsection
@section('scripts')

    {{ Html::script('https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js') }}
    {{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.min.js') }}

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">

    <script>
        $(document).ready(function() {
            $('.bxslider').bxSlider({
                auto: 'fade',
                infiniteLoop: true,
                responsive: true,
                video: true,
                pager: true,
                moveSlides: 1,
                slideMargin: 50,
                slideWidth: 400,
                minSlides: 2,
                maxSlides: 2,
                autoControls: true,
                stopAutoOnClick: false,
                useCSS: false,
                captions: true,
            });
        });

        function mostrarReglamento() {
            window.open('http://' + location.host + '/docs/reglamento/REGLAMENTO_INTERNO_KUNDE.pdf', '_blank',
                'width=2500px, height=700px, scrollbars=yes');
        }
    </script>

    <style>
        .audio-container {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-top: 300px;
        }

        .video-container {
            display: flex;
            justify-content: center;
            align-items: center;
            margin: auto;
        }

        .image-container {
            margin: auto;
            height: 20%;
        }

    </style>
@endsection
@section('contenido')

    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Indicadores</h6>
            </div>

            <div class="card-body">

                <div class="container-fluid">

                    <div class="row">

                        <!-- Meta  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Meta llamadas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$metaLlamadas}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-tasks fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Realizados  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Llamadas realizadas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$llamadasRealizadas}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-check-circle fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Avance  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Avance
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$avanceLlamadas}}%</div>
                                                </div>
                                                <div class="col">
                                                    <div class="progress progress-sm mr-2">
                                                        <div class="progress-bar bg-info" role="progressbar"
                                                            style="width: {{$avanceLlamadas}}%" aria-valuenow="50" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Meta  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Meta citas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$metasCitas}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-tasks fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Realizados  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Llamadas logradas</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$llamadasLogradas}}</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-check-circle fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Avance  -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Avance
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$avanceCitas}}%</div>
                                                </div>
                                                <div class="col">
                                                    <div class="progress progress-sm mr-2">
                                                        <div class="progress-bar bg-info" role="progressbar"
                                                            style="width: {{$avanceCitas}}%" aria-valuenow="50" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Cartelera</h6>
            </div>
            <div class="card-body mx-auto">

                @if (isset($publicaciones) && count($publicaciones) > 0)
                    <div class="bxslider">
                        @foreach ($publicaciones as $reg => $publicacion)
                            @php
                                $extension = pathinfo($publicacion->txRutaPublicacionImagen, PATHINFO_EXTENSION);
                            @endphp
                            @if ($extension == 'mp4' || $extension == 'mp3' || $extension == 'avi')
                                <div class="video-container">
                                    <video src="/gestionhumana/consultarimagen/{{ $publicacion->oidPublicacionImagen }}"
                                        controls width='100%' height='100%'></video>
                                </div>
                            @else
                                <div class="slider">
                                    <img src="/gestionhumana/consultarimagen/{{ $publicacion->oidPublicacionImagen }}">
                                </div>
                            @endif

                        @endforeach
                    </div>
                @endif

                <!-- Campos Segun su tipo imagen/vido/audio -->
                <!--
                                            <div class="slider">
                                            <div><img src="images/31.jpeg" title="Titulo de la imagen"></div>
                                        </div>
                                        <div class="slider">
                                            <div><img src="images/32.jpeg"></div>
                                        </div>
                                        <div class="audio-container">
                                            <audio controls>
                                                <source src="images/audio.mp3" type="audio/mpeg">
                                            </audio>
                                        </div>
                                        <div class="video-container">
                                            <video src="images/video.mp4" controls width='100%' height='100%'></video>
                                        </div>
                                    -->

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tareas</h6>
                </div>
                <div class="card-body">
                    <ol>
                        @foreach ($tareas as $tarea)
                            <a target="_blank" href="registroseguimiento/tarea/{{ $tarea->oidTarea }}/edit">
                                <ul><b>Asunto:</b> {{ $tarea->txAsuntoTarea }} - <b>Vencimiento:</b>
                                    {{ $tarea->dtFechaVencimientoTarea }} - <b>Prioridad:</b>
                                    {{ $tarea->txPrioridadTarea }}
                                </ul>
                            </a>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Reglamento interno de trabajo</h6>
                </div>
                <div class="card-body">
                    <p>
                        Accede al enlace para que conozcas el reglamento interno de Kunde S.A.S.
                    </p>
                    <a href="#" class="link-primary" onclick="mostrarReglamento()">
                        Ver reglamento Kunde
                    </a>

                </div>
            </div>
        </div>



        {{-- {{ Html::script('js/carousel.js') }}
        <style>
            .item {
                position: relative;
                height: 500px;
                left: 300px;
            }

        </style> --}}


        {{-- <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Cartelera</h6>
            </div>
            <div class="card-body">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @if (isset($publicaciones))
                            @foreach ($publicaciones as $reg => $publicacion)
                                @php
                                    $active = $reg == 0 ? 'carousel-item active' : 'carousel-item';
                                @endphp
                                <div class="{{ $active }}">
                                    <img class="img-fluid item"
                                        src="/gestionhumana/consultarimagen/{{ $publicacion->oidPublicacionImagen }}"
                                        style="">
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" style="color: blue !important" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tareas</h6>
                </div>
                <div class="card-body">
                    <ol>
                        @foreach ($tareas as $tarea)
                            <a target="_blank" href="registroseguimiento/tarea/{{ $tarea->oidTarea }}/edit">
                                <ul><b>Asunto:</b> {{ $tarea->txAsuntoTarea }} - <b>Vencimiento:</b>
                                    {{ $tarea->dtFechaVencimientoTarea }} - <b>Prioridad:</b>
                                    {{ $tarea->txPrioridadTarea }}
                                </ul>
                            </a>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>



        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Reglamento interno de trabajo</h6>
                </div>
                <div class="card-body">
                    <p>
                        Accede al enlace para que conozcas el reglamento interno de Kunde S.A.S.
                    </p>
                    <a href="#" class="link-primary" onclick="mostrarReglamento()">
                        Ver reglamento Kunde
                    </a>

                </div>
            </div>
        </div>

    </div> --}}





    @endsection
