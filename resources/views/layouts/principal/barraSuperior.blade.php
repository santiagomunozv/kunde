<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav container-fluid">
        <div class="container-fluid m-auto">
            <h1 class="h3 mb-2 text-gray-800">@yield('nombreModulo')</h1>
        </div>
        <div class="topbar-divider d-none d-sm-block"></div>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <span
                    class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Session::get('session_usr_nombreTercero') }}</span>
                <i><img src="/asociadonegocio/consultaimagen/{{ Session::get('oidTercero') }}"
                        style="border-radius: 50%;" height="35"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                {{-- <a class="dropdown-item" href="#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Perfil
                </a>
                <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Configuración
                </a> --}}
                {{-- <a class="dropdown-item" href="/registroseguimiento/tarea">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Tareas
                </a> --}}
                {{-- <a class="dropdown-item" href="#" onclick="modalTareasRapidas()">
                    <i class="fas fa-thumbtack fa-sm fa-fw mr-2 text-gray-400"></i>
                    Tareas Rapidas
                </a> --}}
                {{-- <div class="dropdown-divider"></div> --}}
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Salir
                </a>
            </div>
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="companiaDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-building"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="companiaDropdown">
                @foreach (Session::get('session_usr_companiasAsociadas') as $compania)
                    {{ Form::open(['url' => '/cambiocompania', 'method' => 'POST']) }}
                    <button type="submit"
                        class="dropdown-item {{ $compania->oidCompania === Session::get('oidCompania') ? 'disabled' : '' }}">
                        {{ $compania->txNombreCompania }}
                        @if ($compania->oidCompania === Session::get('oidCompania'))
                            <i class="fas fa-check text-success"></i>
                        @endif
                    </button>
                    <input type="hidden" name="compania" value="{{ $compania->oidCompania }}">
                    {{ Form::close() }}
                @endforeach
            </div>
        </li>
    </ul>
</nav>

{{-- <script>
    function modalTareasRapidas() {
        modal.cargando();
        $.get("/registroseguimiento/tarearapida", function(resp) {
            modal
                .mostrarModal("Crear tarea rapida", resp, function() {
                    modal.cargando();
                })
                .grande()
                .sinPie();
        }).fail(function(resp) {
            modal.mostrarModal(
                "OOOOOPPPSSSS ocurrió un problema",
                "status:" + resp.status
            );
        });
    }
</script> --}}
