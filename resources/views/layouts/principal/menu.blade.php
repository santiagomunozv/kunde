@foreach (Session::get('packageDinamicMenuScaliaPaquetes') as $paquete)
    <!-- LINEA DE DIVISION -->
    <hr class="sidebar-divider">
    {{-- PAQUETE - NIVEL 1 DEL MENU --}}
    <div class="sidebar-heading">{{ $paquete->txNombrePaquete }}</div>
    @foreach (Session::get('packageDinamicMenuScaliaModulos')[$paquete->oidPaquete] as $modulo)
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse"
                data-target="#collapse{{ $modulo->oidModulo }}" aria-expanded="true"
                aria-controls="collapse{{ $modulo->oidModulo }}">
                <i class="{{ $modulo->txIconoModulo }}"></i>
                <span>{{ $modulo->txNombreModulo }}</span>
            </a>
            <div id="collapse{{ $modulo->oidModulo }}" class="collapse"
                aria-labelledby="heading{{ $modulo->oidModulo }}" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @switch($modulo->txNombreModulo)
                        @case('Ficha Tecnica')
                            @if (Session::has('packageDinamicMenuScaliaClaPro'))
                                @foreach (Session::get('packageDinamicMenuScaliaClaPro') as $clasificacion)
                                    <a class="collapse-item"
                                        href="{{ url('/fichatecnica/ficha?cp=' . $clasificacion->oidClasificacionProducto) }}">
                                        {{ $clasificacion->txNombreClasificacionProducto }}
                                    </a>
                                @endforeach
                            @endif
                        @break

                        @case('Asociado de Negocio')
                            @if (Session::has('packageDinamicMenuScaliaClaTer'))
                                @foreach (Session::get('packageDinamicMenuScaliaClaTer') as $clasificacion)
                                    <a class="collapse-item"
                                        href="{{ url('/asociadonegocio/tercerogrid?ct=' . $clasificacion->oidClasificacionTercero) }}">
                                        {{ $clasificacion->txNombreClasificacionTercero }}
                                    </a>
                                @endforeach
                                <a class="collapse-item" href="{{ url('/asociadonegocio/parametroInforme?accion=informe') }}">
                                    Informes
                                </a>
                            @endif
                        @break

                        @default
                            @foreach (Session::get('packageDinamicMenuScaliaOpciones')[$modulo->oidModulo] as $opcion)
                                <a class="collapse-item"
                                    href="{{ url($opcion->txRutaOpcion) }}">{{ $opcion->txNombreOpcion }}</a>
                            @endforeach
                    @endswitch
                </div>
            </div>
        </li>
    @endforeach
@endforeach


{{-- 
@foreach (Session::get('packageDinamicMenuScalia') as $paquete)
    @if (!$paquete->Modulo->isEmpty())
        
       
        @foreach ($paquete->Modulo->sortBy('txNombreModulo') as $modulo)
            @if (!$modulo->Opcion->isEmpty())
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse{{$modulo->oidModulo}}" aria-expanded="true" aria-controls="collapse{{$modulo->oidModulo}}">
                        <i class="{{$modulo->txIconoModulo}}"></i>
                        <span>{{$modulo->txNombreModulo}}</span>
                    </a>
                    <div id="collapse{{$modulo->oidModulo}}" class="collapse" aria-labelledby="heading{{$modulo->oidModulo}}" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">

                            @switch($modulo->txNombreModulo)
                                @case("Ficha Tecnica")
                                    @if (Session::has('packageDinamicMenuScaliaClaPro'))
                                        @foreach (Session::get('packageDinamicMenuScaliaClaPro') as $clasificacion)
                                            <a class="collapse-item" href="{{url("/fichatecnica/ficha?cp=".$clasificacion->oidClasificacionProducto)}}">
                                                {{$clasificacion->txNombreClasificacionProducto}}</a>
                                        @endforeach
                                    @endif
                                    @break
                                @case("Asociado de Negocio")
                                    @if (Session::has('packageDinamicMenuScaliaClaTer'))
                                        @foreach (Session::get('packageDinamicMenuScaliaClaTer') as $clasificacion)
                                            <a class="collapse-item" href="{{url("/asociadonegocio/tercerogrid/".$clasificacion->oidClasificacionTercero)}}">
                                                {{$clasificacion->txNombreClasificacionTercero}}</a>
                                        @endforeach
                                    @endif
                                    @break
                                @default
                                    @foreach ($modulo->Opcion->sortBy('txNombreOpcion') as $opcion)
                                        <a class="collapse-item" href="{{url($opcion->txRutaOpcion)}}">{{$opcion->txNombreOpcion}}</a>
                                    @endforeach
                            @endswitch

                        </div>
                    </div>
                </li>
                
            @endif
        @endforeach
    @endif
@endforeach --}}

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">
