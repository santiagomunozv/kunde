<html>

<head>
    <title></title>
</head>

<body>
    <p>{!! $mensaje !!}</p>
    Antes de imprimir este correo electrónico, considere bien si es necesario hacerlo. El medio ambiente es cuestión de
    todos. Si decide imprimirlo, piense si es necesario hacerlo a color; el consumo de tinta o tóner será mucho mayor.
</body>

</html>
