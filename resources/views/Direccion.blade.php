<div class="card card-body" style="width: 100% !important; ">
        <!--Primer label-->
        <div class="row col-sm-12">
            <div class="col-sm-4 text-center">
                {!!Form::label('Via1', 'Vía', array('class' => 'text-sm text-primary mb-1')) !!} 
                {!!Form::select('Via1',[''=>'','AV'=>'Avenida','BLV'=>'Bulevar','CL'=>'Calle','CR'=>'Carrera','CRT'=>'Carretera','CIR'=>'Circular','CRV'=>'Circunvalar','DG'=>'Diagonal','TV'=>'Transversal','TC'=>'Troncal','VT'=>'Variante','VI'=>'Vía','CRG'=>'Corregimiento','KM'=>'Kilómetro','VRD'=>'Vereda','AUT'=>'Autopista ','GT'=>'Glorieta','AER'=>'Aeropuerto'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()', 'id'=>'Via1'])!!}
            </div>
            <div class="col-sm-2 text-center">
                {!!Form::label('Numero1', 'Número', array('class' => 'text-sm text-primary mb-1')) !!} 
                {!!Form::text('Numero1',null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()', 'id'=>'Numero1'])!!} 
            </div>
            <div class="col-sm-2 text-center">
                {!!Form::label('Nomenclatura1', 'Nomenclatura', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::select('Nomenclatura1',[''=>'','A'=>'A','AA'=>'AA','AB'=>'AB','AC'=>'AC','AD'=>'AD','AE'=>'AE','AF'=>'AF','AG'=>'AG','AH'=>'AH','B'=>'B','BB'=>'BB','BA'=>'BA','BC'=>'BC','BD'=>'BD','BE'=>'BE','BF'=>'BF','BG'=>'BG','BH'=>'BH','C'=>'C','CC'=>'CC','CA'=>'CA','CB'=>'CB','CD'=>'CD','CE'=>'CE','CF'=>'CF','CG'=>'CG','CH'=>'CH','D'=>'D','DD'=>'DD','DA'=>'DA','DB'=>'DB','DC'=>'DC','DE'=>'DE','DF'=>'DF','DG'=>'DG','DH'=>'DH','E'=>'E','EE'=>'EE','EA'=>'EA','EB'=>'EB','EC'=>'EC','ED'=>'ED','EF'=>'EF','EG'=>'EG','EH'=>'EH','F'=>'F','FA'=>'FA','FB'=>'FB','FC'=>'FC','FD'=>'FD','FE'=>'FE','FG'=>'FG','FH'=>'FH','G'=>'G','GG'=>'GG','GA'=>'GA','GB'=>'GB','GC'=>'GC','GD'=>'GD','GE'=>'GE','GF'=>'GF','GH'=>'GH','H'=>'H','HH'=>'HH','HA'=>'HA','HB'=>'HB','HC'=>'HC','HD'=>'HD','HE'=>'HE','HF'=>'HF','HG'=>'HG'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!} 
            </div>
            <div class="col-sm-4 text-center">
                {!!Form::label('Cardinalidad1', 'Cardinalidad', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::select('Cardinalidad1',[''=>'','Este'=>'Este','Norte'=>'Norte','Oeste'=>'Oeste','Sur'=>'Sur'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!} 
            </div>
        </div><br>  
        <div class="row col-sm-12">                        
            <!--Segundo label-->
            <div class="col-sm-4 text-center"> 
                {!!Form::label('Via2', 'Vía', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::select('Via2',[''=>'','AV'=>'Avenida','BLV'=>'Bulevar','CL'=>'Calle','CR'=>'Carrera','CRT'=>'Carretera','CIR'=>'Circular','CRV'=>'Circunvalar','DG'=>'Diagonal','TV'=>'Transversal','TC'=>'Troncal','VT'=>'Variante','VI'=>'Vía','CRG'=>'Corregimiento','KM'=>'Kilómetro','VRD'=>'Vereda','AUT'=>'Autopista ','GT'=>'Glorieta','AER'=>'Aeropuerto'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!}
            </div>
            <div class="col-sm-2 text-center">
                {!!Form::label('Numero2', 'Número', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::text('Numero2',null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!} 
            </div>
            <div class="col-sm-2 text-center">
                {!!Form::label('Nomenclatura2', 'Nomenclatura', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::select('Nomenclatura2',[''=>'','A'=>'A','AA'=>'AA','AB'=>'AB','AC'=>'AC','AD'=>'AD','AE'=>'AE','AF'=>'AF','AG'=>'AG','AH'=>'AH','B'=>'B','BB'=>'BB','BA'=>'BA','BC'=>'BC','BD'=>'BD','BE'=>'BE','BF'=>'BF','BG'=>'BG','BH'=>'BH','C'=>'C','CC'=>'CC','CA'=>'CA','CB'=>'CB','CD'=>'CD','CE'=>'CE','CF'=>'CF','CG'=>'CG','CH'=>'CH','D'=>'D','DD'=>'DD','DA'=>'DA','DB'=>'DB','DC'=>'DC','DE'=>'DE','DF'=>'DF','DG'=>'DG','DH'=>'DH','E'=>'E','EE'=>'EE','EA'=>'EA','EB'=>'EB','EC'=>'EC','ED'=>'ED','EF'=>'EF','EG'=>'EG','EH'=>'EH','F'=>'F','FA'=>'FA','FB'=>'FB','FC'=>'FC','FD'=>'FD','FE'=>'FE','FG'=>'FG','FH'=>'FH','G'=>'G','GG'=>'GG','GA'=>'GA','GB'=>'GB','GC'=>'GC','GD'=>'GD','GE'=>'GE','GF'=>'GF','GH'=>'GH','H'=>'H','HH'=>'HH','HA'=>'HA','HB'=>'HB','HC'=>'HC','HD'=>'HD','HE'=>'HE','HF'=>'HF','HG'=>'HG'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!} 
            </div>
            <div class="col-sm-2 text-center">
                {!!Form::label('Cardinalidad2', 'Cardinalidad', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::select('Cardinalidad2',[''=>'','Este'=>'Este','Norte'=>'Norte','Oeste'=>'Oeste','Sur'=>'Sur'],null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!}
            </div>
            <div class="col-sm-2 text-centers">
                {!!Form::label('Placa', 'Placa', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::text('Placa',null,[ 'class'=>'form-control', 'onchange'=>'concatenarDireccion()'])!!}
            </div>
        </div><br>
        <div class="col-sm-12">
            {!!Form::label('Complemento', 'Componente', array('class' => 'control-label')) !!}
        </div>
        <div class="col-sm-12">
                {!!Form::text('Complemento',null,[ 'class'=>'form-control','placeholder'=>'Ej = ED Las Margaritas BL 1 AP 201', 'onblur'=>'concatenarDireccion()'])!!} 
        </div>
        <div class="col-sm-12">
                {!!Form::text('Direccion',null,[ 'class'=>'form-control','readonly', 'id'=>'Direccion'])!!} 
        </div>
        <div class="modal-footer">
            {!!Form::button('Adicionar',["class"=>"btn btn-success", "onclick"=>"cerrarDireccion()", 'type' => 'button', 'data-toggle'=>'collapse'])!!}
        </div>
    </div>