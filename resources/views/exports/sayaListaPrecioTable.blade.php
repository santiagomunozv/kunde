<table>
    <tr>
        <td colspan="54"></td>
        <td colspan="48">ENCABEZADO LISTA PRECIOS</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Número</td>
        <td>{{$encabezado->codigoAlternoListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Nombre</td>
        <td>{{$encabezado->nombreListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Redondeo</td>
        <td>{{$encabezado->redondeoListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Inicio vigencia</td>
        <td>{{$encabezado->fechaInicialListaPrecio}}</td>
        <td>{{$encabezado->horaInicialListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>fin vigencia</td>
        <td>{{$encabezado->fechaFinalListaPrecio}}</td>
        <td>{{$encabezado->horaFinalListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Código moneda</td>
        <td>{{$encabezado->codigoAlternoMoneda}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Código componentes costo</td>
        <td>{{$encabezado->codigoAlternoComponenteCosto}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Modificar precio del producto</td>
        <td>{{$encabezado->modificarPrecioProductoListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>IVA incluido</td>
        <td>{{$encabezado->ivaIncluidoProductoListaPrecio}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Acción</td>
        <td>{{$encabezado->actualizar}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td></td>
        <td>Estado del producto</td>
        <td>{{$encabezado->estadoListaPrecioDetalle}}</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td>codigoAlternoBodegaLP</td>
        <td>referenciaProductoLP</td>
        <td></td>
        <td>valorBaseListaPrecioDetalle</td>
        @foreach ($conceptosScalia as $concepto)
            <td></td>
        @endforeach
        <td>margenListaPrecioDetalle</td>
        <td>descuentoListaPrecioDetalle</td>
        <td>descuentoMaxListaPrecioDetalle</td>
        <td>valorDescuentoMaxListaPrecioDetalle</td>
        <td>precioListaPrecioDetalle</td>
        <td></td>
        <td>EANTerceroLP</td>
    </tr>
    <tr>
        <td colspan="54"></td>
        <td>Bodega</td>
        <td>Referencia producto Lista de Precios</td>
        <td>Descripción Producto Lista Precios</td>
        <td>Valor base</td>
        @foreach ($conceptosScalia as $concepto)
            <td>{{$concepto->txCodigoComponenteCostoConcepto}}</td>
        @endforeach
        <td>Margen (%)</td>
        <td>Descuento</td>
        <td>Descuento Maximo</td>
        <td>Valor descuento Maximo</td>
        <td>Precio del Producto</td>
        <td></td>
        <td style="min-width: 800px;width: 1000px;">EAN del Tercero</td>
        <td>Nombre (Opcional)</td>
    </tr>
    @foreach ($detalles as $row)
    <tr>
        <td colspan="54"></td>
        <td>{{$row->codigoAlternoBodega}}</td>
        <td>{{$row->referenciaProducto}}</td>
        <td>{{$row->nombreProducto}}</td>
        <td>{{$row->valorBaseListaPrecioDetalle}}</td>
        @foreach ($row->conceptos as $concepto)
            <td>{{$concepto}}</td>
        @endforeach
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>0</td>
        <td>{{$row->precioListaPrecioDetalle}}</td>
        <td></td>
        <td>{{$row->ean}}</td>
        <td></td>
    </tr>
    @endforeach
</table>