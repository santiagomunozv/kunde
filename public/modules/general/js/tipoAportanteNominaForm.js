"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-tipoaportantenomina";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipoaportantenomina";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoTipoAportanteNomina_Input = $("#txCodigoTipoAportanteNomina");
        let txCodigoTipoAportanteNomina_AE = txCodigoTipoAportanteNomina_Input.val();
        txCodigoTipoAportanteNomina_Input.removeClass(errorClass);
        if(!txCodigoTipoAportanteNomina_AE){
            txCodigoTipoAportanteNomina_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreTipoAportanteNomina_Input = $("#txNombreTipoAportanteNomina");
        let txNombreTipoAportanteNomina_AE = txNombreTipoAportanteNomina_Input.val();
        txNombreTipoAportanteNomina_Input.removeClass(errorClass);
        if(!txNombreTipoAportanteNomina_AE){
            txNombreTipoAportanteNomina_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}