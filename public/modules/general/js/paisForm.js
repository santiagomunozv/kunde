"use strict";
let errorClass = 'is-invalid';

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-pais';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = '/general/pais';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        let nombreInput = $('#txNombrePais');
        let isoInput = $('#txCodigoISOPais');
        let mensajes =[];
        let nombrePais = nombreInput.val();
        let codigoIso = isoInput.val();
        nombreInput.removeClass(errorClass);
        isoInput.removeClass(errorClass);
        if(!nombrePais){
            nombreInput.addClass(errorClass);
            mensajes.push('El campo Código ISO es obligatorio');
        }
        if(!codigoIso){
            isoInput.addClass(errorClass)
            mensajes.push('El campo Nombre es obligatorio');
        }
        return mensajes;
    }
}