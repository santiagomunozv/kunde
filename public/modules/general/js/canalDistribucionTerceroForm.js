"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-canaldistribuciontercero";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/canaldistribuciontercero";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCanalDistribucionTercero_Input = $("#txCodigoCanalDistribucionTercero");
        let txCodigoCanalDistribucionTercero_AE = txCodigoCanalDistribucionTercero_Input.val();
        txCodigoCanalDistribucionTercero_Input.removeClass(errorClass);
        if(!txCodigoCanalDistribucionTercero_AE){
            txCodigoCanalDistribucionTercero_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCanalDistribucionTercero_Input = $("#txNombreCanalDistribucionTercero");
        let txNombreCanalDistribucionTercero_AE = txNombreCanalDistribucionTercero_Input.val();
        txNombreCanalDistribucionTercero_Input.removeClass(errorClass);
        if(!txNombreCanalDistribucionTercero_AE){
            txNombreCanalDistribucionTercero_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}