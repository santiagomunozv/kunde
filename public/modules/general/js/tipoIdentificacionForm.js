"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-tipoidentificacion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipoidentificacion";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoTipoIdentificacion_Input = $("#txCodigoTipoIdentificacion");
        let txCodigoTipoIdentificacion_AE = txCodigoTipoIdentificacion_Input.val();
        txCodigoTipoIdentificacion_Input.removeClass(errorClass);
        if(!txCodigoTipoIdentificacion_AE){
            txCodigoTipoIdentificacion_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreTipoIdentificacion_Input = $("#txNombreTipoIdentificacion");
        let txNombreTipoIdentificacion_AE = txNombreTipoIdentificacion_Input.val();
        txNombreTipoIdentificacion_Input.removeClass(errorClass);
        if(!txNombreTipoIdentificacion_AE){
            txNombreTipoIdentificacion_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoTipoIdentificacion_Input = $("#txEstadoTipoIdentificacion");
        let txEstadoTipoIdentificacion_AE = txEstadoTipoIdentificacion_Input.val();
        txEstadoTipoIdentificacion_Input.removeClass(errorClass);
        if(!txEstadoTipoIdentificacion_AE){
            txEstadoTipoIdentificacion_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}