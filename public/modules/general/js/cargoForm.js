"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-cargo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/cargo";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCargo_Input = $("#txCodigoCargo");
        let txCodigoCargo_AE = txCodigoCargo_Input.val();
        txCodigoCargo_Input.removeClass(errorClass);
        if(!txCodigoCargo_AE){
            txCodigoCargo_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCargo_Input = $("#txNombreCargo");
        let txNombreCargo_AE = txNombreCargo_Input.val();
        txNombreCargo_Input.removeClass(errorClass);
        if(!txNombreCargo_AE){
            txNombreCargo_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoCargo_Input = $("#txEstadoCargo");
        let txEstadoCargo_AE = txEstadoCargo_Input.val();
        txEstadoCargo_Input.removeClass(errorClass);
        if(!txEstadoCargo_AE){
            txEstadoCargo_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}