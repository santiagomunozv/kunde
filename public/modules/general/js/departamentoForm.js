"use strict" ;
var errorClass = 'is-invalid';
var gen_ciudad;

$(function(){
    gen_ciudad = new GeneradorMultiRegistro('gen_ciudad','contenedor_gen_ciudad','gen_ciudad_');
    gen_ciudad.campoid = 'oidCiudad';
    gen_ciudad.campoEliminacion = 'eliminarCiudad';
    gen_ciudad.botonEliminacion = true;
    gen_ciudad.funcionEliminacion = '';
    gen_ciudad.campos = ['oidCiudad','txCodigoCiudad','txNombreCiudad','txCodigoPostalCiudad'];
    gen_ciudad.etiqueta = ['input','input','input','input'];
    gen_ciudad.tipo = ['hidden','text','text','text'];
    gen_ciudad.clase = ['numerico ','','',''];
    gen_ciudad.sololectura = [true,false,false,false];
    gen_ciudad.opciones = ['','','',''];
    gen_ciudad.funciones = ['','','',''];
    gen_ciudad.otrosAtributos = ['','','',''];
    ciudades.forEach(ciudad => {
        gen_ciudad.agregarCampos(ciudad,'L');
    });
});

function validarDepartamento(){
    let messages = [];
    let validaciones = [
        {'input' :'Pais_oidDepartamento' , 'message' : 'El campo Pais es obligatorio.'},
        {'input' :'txCodigoDepartamento' , 'message' : 'El campo Código es obligatorio.'},
        {'input' :'txNombreDepartamento' , 'message' : 'El campo Nombre es obligatorio.'},
        {'input' :'txEstadoDepartamento' , 'message' : 'El campo Estado es obligatorio.'},
    ];
    validaciones.forEach( validacion => {
        let input = $('#'+validacion.input);
        if(!input.val()){
            input.addClass( errorClass );
            messages.push( validacion.message);
        }
    });
    let codigos = $('input[name="txCodigoCiudad[]"]');
    let nombres = $('input[name="txNombreCiudad[]"]');
    let maxLength = codigos.length >= nombres.length ? codigos.length : nombres.length;
    for(let i = 0 ; i < maxLength; i++){
        let codigo = codigos[i];
        let nombre = nombres[i];
        if(codigo && !codigo.value){
            codigo.classList.add( errorClass );
            messages.push('El código de la ciudad en la linea '+(i + 1)+' es requerido');
        }
        if(nombre && !nombre.value){
            nombre.classList.add( errorClass );
            messages.push('El nombre de la ciudad en la linea '+(i + 1)+' es requerido');
        }
    }
    return messages;
}

function grabar(){
    modal.cargando();
    $('.'+errorClass).removeClass(errorClass);
    let mensajes = validarDepartamento();
    if( mensajes && mensajes.length ){
        modal.mostrarErrores( mensajes );
    }else{
        let formularioId = '#form-departamento';
        let url = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(url,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = '/general/departamento';
            });
            modal.mostrarModal('Información' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyCodigo = 'txCodigoCiudad';
            let keyNombre = 'txNombreCiudad';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let codigos = $('input[name="'+keyCodigo+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace(".","");
                // let registro = index.substr(keyCodigo.length , index.length)
                // let personalizado = value[0].replace("."+registro," "+(parseFloat(registro) + 1));
                // personalizado = personalizado.replace("txCodigoCiudad","Codigo de ciudad");
                mensajes.push( value );

                if( index.startsWith(keyCodigo)){
                    setErroresMultiregistro(index.substr(keyCodigo.length , index.length) , codigos);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes, url ,resp);
        });
    }
}

function setErroresMultiregistro( index , array){
    if(array && array.length){
        array[index].classList.add(errorClass);
    }
}