"use strict";
let errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


});
var valorPreguntaOeaDetalle = ['0','0','','',];
var gen_preguntaoeadetalle;
$(function(){

    gen_preguntaoeadetalle = new GeneradorMultiRegistro('gen_preguntaoeadetalle','contenedor_gen_preguntaoeadetalle','gen_preguntaoeadetalle_');

    gen_preguntaoeadetalle.altura = '35px;';
    gen_preguntaoeadetalle.campoid = 'oidPreguntaOeaDetalle';
    gen_preguntaoeadetalle.campoEliminacion = 'eliminarPreguntaOeaDetalle';
    gen_preguntaoeadetalle.botonEliminacion = true;
    gen_preguntaoeadetalle.funcionEliminacion = '';

    gen_preguntaoeadetalle.campos = ['oidPreguntaOeaDetalle','inCalificacionPreguntaOeaDetalle','txTextoPreguntaOeaDetalle','txObservacionPreguntaOeaDetalle','daVigenciaPreguntaOeaDetalle'];

    gen_preguntaoeadetalle.etiqueta = ['input','input','input','input','input'];
    gen_preguntaoeadetalle.tipo = ['hidden','text','text','text','date'];
    gen_preguntaoeadetalle.estilo = ['','','','',''];
    gen_preguntaoeadetalle.clase = ['text-right ','text-right ','','',''];
    gen_preguntaoeadetalle.sololectura = [true,false,false,false,false];

    gen_preguntaoeadetalle.opciones = ['','','','',''];

    gen_preguntaoeadetalle.funciones = ['','','','',''];
    gen_preguntaoeadetalle.otrosAtributos = ['','','','',''];

    PreguntaOeaDetalle.forEach(PreguntaOeaDetallereg => {
        gen_preguntaoeadetalle.agregarCampos(PreguntaOeaDetallereg,'L');
    });

});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-preguntaoea";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/preguntaoea";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoPreguntaOea_Input = $("#txCodigoPreguntaOea");
        let txCodigoPreguntaOea_AE = txCodigoPreguntaOea_Input.val();
        txCodigoPreguntaOea_Input.removeClass(errorClass);
        if(!txCodigoPreguntaOea_AE){
            txCodigoPreguntaOea_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombrePreguntaOea_Input = $("#txNombrePreguntaOea");
        let txNombrePreguntaOea_AE = txNombrePreguntaOea_Input.val();
        txNombrePreguntaOea_Input.removeClass(errorClass);
        if(!txNombrePreguntaOea_AE){
            txNombrePreguntaOea_Input.addClass(errorClass)
            mensajes.push("El campo Pregunta es obligatorio");
        }
		let lsTipoPreguntaOea_Input = $("#lsTipoPreguntaOea");
        let lsTipoPreguntaOea_AE = lsTipoPreguntaOea_Input.val();
        lsTipoPreguntaOea_Input.removeClass(errorClass);
        if(!lsTipoPreguntaOea_AE){
            lsTipoPreguntaOea_Input.addClass(errorClass)
            mensajes.push("El campo Tipo de Pregunta es obligatorio");
        }
		let inPorcentajePreguntaOea_Input = $("#inPorcentajePreguntaOea");
        let inPorcentajePreguntaOea_AE = inPorcentajePreguntaOea_Input.val();
        inPorcentajePreguntaOea_Input.removeClass(errorClass);
        if(!inPorcentajePreguntaOea_AE){
            inPorcentajePreguntaOea_Input.addClass(errorClass)
            mensajes.push("El campo Porcentaje es obligatorio");
        }
		let txEstadoPreguntaOea_Input = $("#txEstadoPreguntaOea");
        let txEstadoPreguntaOea_AE = txEstadoPreguntaOea_Input.val();
        txEstadoPreguntaOea_Input.removeClass(errorClass);
        if(!txEstadoPreguntaOea_AE){
            txEstadoPreguntaOea_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}