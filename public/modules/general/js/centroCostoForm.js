"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-centrocosto";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/centrocosto";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCentroCosto_Input = $("#txCodigoCentroCosto");
        let txCodigoCentroCosto_AE = txCodigoCentroCosto_Input.val();
        txCodigoCentroCosto_Input.removeClass(errorClass);
        if(!txCodigoCentroCosto_AE){
            txCodigoCentroCosto_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCentroCosto_Input = $("#txNombreCentroCosto");
        let txNombreCentroCosto_AE = txNombreCentroCosto_Input.val();
        txNombreCentroCosto_Input.removeClass(errorClass);
        if(!txNombreCentroCosto_AE){
            txNombreCentroCosto_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txResponsableCentroCosto_Input = $("#txResponsableCentroCosto");
        let txResponsableCentroCosto_AE = txResponsableCentroCosto_Input.val();
        txResponsableCentroCosto_Input.removeClass(errorClass);
        if(!txResponsableCentroCosto_AE){
            txResponsableCentroCosto_Input.addClass(errorClass)
            mensajes.push("El campo Responsable es obligatorio");
        }
		let txEstadoCentroCosto_Input = $("#txEstadoCentroCosto");
        let txEstadoCentroCosto_AE = txEstadoCentroCosto_Input.val();
        txEstadoCentroCosto_Input.removeClass(errorClass);
        if(!txEstadoCentroCosto_AE){
            txEstadoCentroCosto_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}