"use strict";
let errorClass = "is-invalid";
$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" },
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-tipocertificado";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipocertificado";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}

    function validarForm(){
        
        let mensajes =[];
        let txCodigoTipoCertificado_Input = $("#txCodigoTipoCertificado");
        let txCodigoTipoCertificado_AE = txCodigoTipoCertificado_Input.val();
        txCodigoTipoCertificado_Input.removeClass(errorClass);
        if(!txCodigoTipoCertificado_AE){
            txCodigoTipoCertificado_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreTipoCertificado_Input = $("#txNombreTipoCertificado");
        let txNombreTipoCertificado_AE = txNombreTipoCertificado_Input.val();
        txNombreTipoCertificado_Input.removeClass(errorClass);
        if(!txNombreTipoCertificado_AE){
            txNombreTipoCertificado_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoTipoCertificado_Input = $("#txEstadoTipoCertificado");
        let txEstadoTipoCertificado_AE = txEstadoTipoCertificado_Input.val();
        txEstadoTipoCertificado_Input.removeClass(errorClass);
        if(!txEstadoTipoCertificado_AE){
            txEstadoTipoCertificado_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
        let lsTipoTipoCertificado_Input = $("#lsTipoTipoCertificado");
        let lsTipoTipoCertificado_AE = lsTipoTipoCertificado_Input.val();
        lsTipoTipoCertificado_Input.removeClass(errorClass);
        if(!lsTipoTipoCertificado_AE){
            lsTipoTipoCertificado_Input.addClass(errorClass)
            mensajes.push("El campo Tipo Certificado es obligatorio");
        }
        let txVigenciaTipoCertificado_Input = $("#txVigenciaTipoCertificado");
        let txVigenciaTipoCertificado_AE = txVigenciaTipoCertificado_Input.val();
        lsTipoTipoCertificado_Input.removeClass(errorClass);
        if(!txVigenciaTipoCertificado_AE){
            txVigenciaTipoCertificado_Input.addClass(errorClass)
            mensajes.push("El campo Vigencia es obligatorio");
        }

        let Compania_oidCompania_Input = $("#Compania_oidCompania");
        let Compania_oidCompania_AE = Compania_oidCompania_Input.val();
        Compania_oidCompania_Input.removeClass(errorClass);
        if(!Compania_oidCompania_AE){
            Compania_oidCompania_Input.addClass(errorClass)
            mensajes.push("El campo compañia es obligatorio");
        }
		return mensajes;
    }