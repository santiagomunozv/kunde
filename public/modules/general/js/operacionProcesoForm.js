"use strict";
let errorClass = "is-invalid";

var valorOperacionProcesoDetalle = ['0','','','','',];
var gen_operacionprocesodetalle;
$(function(){

    gen_operacionprocesodetalle = new GeneradorMultiRegistro('gen_operacionprocesodetalle','contenedor_gen_operacionprocesodetalle','gen_operacionprocesodetalle_');

    gen_operacionprocesodetalle.altura = '35px;';
    gen_operacionprocesodetalle.campoid = 'oidOperacionProcesoDetalle';
    gen_operacionprocesodetalle.campoEliminacion = 'eliminarOperacionProcesoDetalle';
    gen_operacionprocesodetalle.botonEliminacion = true;
    gen_operacionprocesodetalle.funcionEliminacion = '';

    gen_operacionprocesodetalle.campos = ['oidOperacionProcesoDetalle','txSecuenciaOperacionProcesoDetalle','txNombreOperacionProcesoDetalle','deSamOperacionProcesoDetalle','txObservacionOperacionProcesoDetalle',];

    gen_operacionprocesodetalle.etiqueta = ['input','input','input','input','input',];
    gen_operacionprocesodetalle.tipo = ['hidden','text','text','text','text',];
    gen_operacionprocesodetalle.estilo = ['','','','','',];
    gen_operacionprocesodetalle.clase = ['text-right ','','','','',];
    gen_operacionprocesodetalle.sololectura = [true,false,false,false,false,];

    gen_operacionprocesodetalle.opciones = ['','','','','',];

    gen_operacionprocesodetalle.funciones = ['','','','','',];
    gen_operacionprocesodetalle.otrosAtributos = ['','','','','',];

    OperacionProcesoDetalle.forEach(OperacionProcesoDetallereg => {
        gen_operacionprocesodetalle.agregarCampos(OperacionProcesoDetallereg,'L');
    });

});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-operacionproceso";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/operacionproceso";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let CentroProduccion_oidOperacionProceso_Input = $("#CentroProduccion_oidOperacionProceso");
        let CentroProduccion_oidOperacionProceso_AE = CentroProduccion_oidOperacionProceso_Input.val();
        CentroProduccion_oidOperacionProceso_Input.removeClass(errorClass);
        if(!CentroProduccion_oidOperacionProceso_AE){
            CentroProduccion_oidOperacionProceso_Input.addClass(errorClass)
            mensajes.push("El campo Centro de Producción es obligatorio");
        }
		let ClasificacionProducto_oidOperacionProceso_Input = $("#ClasificacionProducto_oidOperacionProceso");
        let ClasificacionProducto_oidOperacionProceso_AE = ClasificacionProducto_oidOperacionProceso_Input.val();
        ClasificacionProducto_oidOperacionProceso_Input.removeClass(errorClass);
        if(!ClasificacionProducto_oidOperacionProceso_AE){
            ClasificacionProducto_oidOperacionProceso_Input.addClass(errorClass)
            mensajes.push("El campo Clasificación de Producto es obligatorio");
        }
		let LineaProduccion_oidOperacionProceso_Input = $("#LineaProduccion_oidOperacionProceso");
        let LineaProduccion_oidOperacionProceso_AE = LineaProduccion_oidOperacionProceso_Input.val();
        LineaProduccion_oidOperacionProceso_Input.removeClass(errorClass);
        if(!LineaProduccion_oidOperacionProceso_AE){
            LineaProduccion_oidOperacionProceso_Input.addClass(errorClass)
            mensajes.push("El campo Línea de Producción es obligatorio");
        }
		let txEstadoOperacionProceso_Input = $("#txEstadoOperacionProceso");
        let txEstadoOperacionProceso_AE = txEstadoOperacionProceso_Input.val();
        txEstadoOperacionProceso_Input.removeClass(errorClass);
        if(!txEstadoOperacionProceso_AE){
            txEstadoOperacionProceso_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}