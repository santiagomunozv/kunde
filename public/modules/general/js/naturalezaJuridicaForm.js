"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-naturalezajuridica";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/naturalezajuridica";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoNaturalezaJuridica_Input = $("#txCodigoNaturalezaJuridica");
        let txCodigoNaturalezaJuridica_AE = txCodigoNaturalezaJuridica_Input.val();
        txCodigoNaturalezaJuridica_Input.removeClass(errorClass);
        if(!txCodigoNaturalezaJuridica_AE){
            txCodigoNaturalezaJuridica_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreNaturalezaJuridica_Input = $("#txNombreNaturalezaJuridica");
        let txNombreNaturalezaJuridica_AE = txNombreNaturalezaJuridica_Input.val();
        txNombreNaturalezaJuridica_Input.removeClass(errorClass);
        if(!txNombreNaturalezaJuridica_AE){
            txNombreNaturalezaJuridica_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoNaturalezaJuridica_Input = $("#txEstadoNaturalezaJuridica");
        let txEstadoNaturalezaJuridica_AE = txEstadoNaturalezaJuridica_Input.val();
        txEstadoNaturalezaJuridica_Input.removeClass(errorClass);
        if(!txEstadoNaturalezaJuridica_AE){
            txEstadoNaturalezaJuridica_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}