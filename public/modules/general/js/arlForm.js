"use strict";
var errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-arl";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/arl";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoArl_Input = $("#txCodigoArl");
        let txCodigoArl_AE = txCodigoArl_Input.val();
        txCodigoArl_Input.removeClass(errorClass);
        if(!txCodigoArl_AE){
            txCodigoArl_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreArl_Input = $("#txNombreArl");
        let txNombreArl_AE = txNombreArl_Input.val();
        txNombreArl_Input.removeClass(errorClass);
        if(!txNombreArl_AE){
            txNombreArl_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}