"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-centrotrabajo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/centrotrabajo";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCentroTrabajo_Input = $("#txCodigoCentroTrabajo");
        let txCodigoCentroTrabajo_AE = txCodigoCentroTrabajo_Input.val();
        txCodigoCentroTrabajo_Input.removeClass(errorClass);
        if(!txCodigoCentroTrabajo_AE){
            txCodigoCentroTrabajo_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txCodigoNominaCentroTrabajo_Input = $("#txCodigoNominaCentroTrabajo");
        let txCodigoNominaCentroTrabajo_AE = txCodigoNominaCentroTrabajo_Input.val();
        txCodigoNominaCentroTrabajo_Input.removeClass(errorClass);
        if(!txCodigoNominaCentroTrabajo_AE){
            txCodigoNominaCentroTrabajo_Input.addClass(errorClass)
            mensajes.push("El campo Código Nomina es obligatorio");
        }
		let Tercero_oidSucursal_Input = $("#Tercero_oidSucursal");
        let Tercero_oidSucursal_AE = Tercero_oidSucursal_Input.val();
        Tercero_oidSucursal_Input.removeClass(errorClass);
        if(!Tercero_oidSucursal_AE){
            Tercero_oidSucursal_Input.addClass(errorClass)
            mensajes.push("El campo Sucursal es obligatorio");
        }
		let txNombreCentroTrabajo_Input = $("#txNombreCentroTrabajo");
        let txNombreCentroTrabajo_AE = txNombreCentroTrabajo_Input.val();
        txNombreCentroTrabajo_Input.removeClass(errorClass);
        if(!txNombreCentroTrabajo_AE){
            txNombreCentroTrabajo_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let dePorcentajeARPCentroTrabajo_Input = $("#dePorcentajeARPCentroTrabajo");
        let dePorcentajeARPCentroTrabajo_AE = dePorcentajeARPCentroTrabajo_Input.val();
        dePorcentajeARPCentroTrabajo_Input.removeClass(errorClass);
        if(!dePorcentajeARPCentroTrabajo_AE){
            dePorcentajeARPCentroTrabajo_Input.addClass(errorClass)
            mensajes.push("El campo % ARP es obligatorio");
        }
		let txEstadoCentroTrabajo_Input = $("#txEstadoCentroTrabajo");
        let txEstadoCentroTrabajo_AE = txEstadoCentroTrabajo_Input.val();
        txEstadoCentroTrabajo_Input.removeClass(errorClass);
        if(!txEstadoCentroTrabajo_AE){
            txEstadoCentroTrabajo_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}