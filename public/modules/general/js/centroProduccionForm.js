"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-centroproduccion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/centroproduccion";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCentroProduccion_Input = $("#txCodigoCentroProduccion");
        let txCodigoCentroProduccion_AE = txCodigoCentroProduccion_Input.val();
        txCodigoCentroProduccion_Input.removeClass(errorClass);
        if(!txCodigoCentroProduccion_AE){
            txCodigoCentroProduccion_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCentroProduccion_Input = $("#txNombreCentroProduccion");
        let txNombreCentroProduccion_AE = txNombreCentroProduccion_Input.val();
        txNombreCentroProduccion_Input.removeClass(errorClass);
        if(!txNombreCentroProduccion_AE){
            txNombreCentroProduccion_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let deValorMinutoCentroProduccion_Input = $("#deValorMinutoCentroProduccion");
        let deValorMinutoCentroProduccion_AE = deValorMinutoCentroProduccion_Input.val();
        deValorMinutoCentroProduccion_Input.removeClass(errorClass);
        if(!deValorMinutoCentroProduccion_AE){
            deValorMinutoCentroProduccion_Input.addClass(errorClass)
            mensajes.push("El campo Valor Minuto es obligatorio");
        }
		let lsClasificacionCentroProduccion_Input = $("#lsClasificacionCentroProduccion");
        let lsClasificacionCentroProduccion_AE = lsClasificacionCentroProduccion_Input.val();
        lsClasificacionCentroProduccion_Input.removeClass(errorClass);
        if(!lsClasificacionCentroProduccion_AE){
            lsClasificacionCentroProduccion_Input.addClass(errorClass)
            mensajes.push("El campo Clasificación es obligatorio");
        }
		let txEstadoCentroProduccion_Input = $("#txEstadoCentroProduccion");
        let txEstadoCentroProduccion_AE = txEstadoCentroProduccion_Input.val();
        txEstadoCentroProduccion_Input.removeClass(errorClass);
        if(!txEstadoCentroProduccion_AE){
            txEstadoCentroProduccion_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}