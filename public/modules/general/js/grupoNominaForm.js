"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-gruponomina";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/gruponomina";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoGrupoNomina_Input = $("#txCodigoGrupoNomina");
        let txCodigoGrupoNomina_AE = txCodigoGrupoNomina_Input.val();
        txCodigoGrupoNomina_Input.removeClass(errorClass);
        if(!txCodigoGrupoNomina_AE){
            txCodigoGrupoNomina_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreGrupoNomina_Input = $("#txNombreGrupoNomina");
        let txNombreGrupoNomina_AE = txNombreGrupoNomina_Input.val();
        txNombreGrupoNomina_Input.removeClass(errorClass);
        if(!txNombreGrupoNomina_AE){
            txNombreGrupoNomina_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let lsBasePagoGrupoNomina_Input = $("#lsBasePagoGrupoNomina");
        let lsBasePagoGrupoNomina_AE = lsBasePagoGrupoNomina_Input.val();
        lsBasePagoGrupoNomina_Input.removeClass(errorClass);
        if(!lsBasePagoGrupoNomina_AE){
            lsBasePagoGrupoNomina_Input.addClass(errorClass)
            mensajes.push("El campo Base de Pago es obligatorio");
        }
		let txEstadoGrupoNomina_Input = $("#txEstadoGrupoNomina");
        let txEstadoGrupoNomina_AE = txEstadoGrupoNomina_Input.val();
        txEstadoGrupoNomina_Input.removeClass(errorClass);
        if(!txEstadoGrupoNomina_AE){
            txEstadoGrupoNomina_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}