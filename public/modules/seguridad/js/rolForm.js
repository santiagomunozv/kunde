"use strict";
let errorClass = "is-invalid";

var valorRolOpcion = ['0','0','0','0','0','0','0',];
var seg_rolopcion;
$(function(){

    seg_rolopcion = new GeneradorMultiRegistro('seg_rolopcion','contenedor_seg_rolopcion','seg_rolopcion_');

    seg_rolopcion.altura = '35px;';
    seg_rolopcion.campoid = 'oidRolOpcion';
    seg_rolopcion.campoEliminacion = 'eliminarRolOpcion';
    seg_rolopcion.botonEliminacion = true;
    seg_rolopcion.funcionEliminacion = '';

    seg_rolopcion.campos = ['oidRolOpcion','Opcion_oidRolOpcion','chAdicionarRolOpcion','chModificarRolOpcion','chEliminarRolOpcion','chConsultarRolOpcion','chInactivarRolOpcion',];

    seg_rolopcion.etiqueta = ['input','select','checkbox','checkbox','checkbox','checkbox','checkbox',];
    seg_rolopcion.tipo = ['hidden','','','','','','',];
    seg_rolopcion.estilo = ['','','','','','','',];
    seg_rolopcion.clase = ['text-right ','chosen-select','','','','','',];
    seg_rolopcion.sololectura = [true,false,false,false,false,false,false,];

    seg_rolopcion.opciones = ['',Opcion_oidRolOpcionlista,'','','','','',];

    seg_rolopcion.funciones = ['','','','','','','',];
    seg_rolopcion.otrosAtributos = ['','','','','','','',];

    RolOpcion.forEach(RolOpcionreg => {
        seg_rolopcion.agregarCampos(RolOpcionreg,'L');
    });

});

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-rol";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/seguridad/rol";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoRol_Input = $("#txCodigoRol");
        let txCodigoRol_AE = txCodigoRol_Input.val();
        txCodigoRol_Input.removeClass(errorClass);
        if(!txCodigoRol_AE){
            txCodigoRol_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreRol_Input = $("#txNombreRol");
        let txNombreRol_AE = txNombreRol_Input.val();
        txNombreRol_Input.removeClass(errorClass);
        if(!txNombreRol_AE){
            txNombreRol_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoRol_Input = $("#txEstadoRol");
        let txEstadoRol_AE = txEstadoRol_Input.val();
        txEstadoRol_Input.removeClass(errorClass);
        if(!txEstadoRol_AE){
            txEstadoRol_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}