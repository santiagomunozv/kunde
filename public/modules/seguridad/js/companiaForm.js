"use strict";
let errorClass = "is-invalid";

var valorCompaniaTercero = ['0','0','',];
var seg_companiatercero;
$(function(){

    seg_companiatercero = new GeneradorMultiRegistro('seg_companiatercero','contenedor_seg_companiatercero','seg_companiatercero_');

    seg_companiatercero.altura = '35px;';
    seg_companiatercero.campoid = 'oidCompaniaTercero';
    seg_companiatercero.campoEliminacion = 'eliminarCompaniaTercero';
    seg_companiatercero.botonEliminacion = true;
    seg_companiatercero.funcionEliminacion = '';

    seg_companiatercero.campos = ['oidCompaniaTercero','Tercero_oidCompaniaTercero','lsCodificacionCompaniaTercero',];

    seg_companiatercero.etiqueta = ['input','select','select',];
    seg_companiatercero.tipo = ['hidden','','',];
    seg_companiatercero.estilo = ['','','',];
    seg_companiatercero.clase = ['text-right ','','',];
    seg_companiatercero.sololectura = [true,false,false,];

    seg_companiatercero.opciones = ['',Tercero_oidCompaniaTercerolista,[['R','RC','RT','RCT',], ['Referencia','Referencia - Color','Referencia - Talla','Referencia - Color - Talla',]],];

    seg_companiatercero.funciones = ['','','',];
    seg_companiatercero.otrosAtributos = ['','','',];

    CompaniaTercero.forEach(CompaniaTerceroreg => {
        seg_companiatercero.agregarCampos(CompaniaTerceroreg,'L');
    });

});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-compania";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/seguridad/compania";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoCompania_Input = $("#txCodigoCompania");
        let txCodigoCompania_AE = txCodigoCompania_Input.val();
        txCodigoCompania_Input.removeClass(errorClass);
        if(!txCodigoCompania_AE){
            txCodigoCompania_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCompania_Input = $("#txNombreCompania");
        let txNombreCompania_AE = txNombreCompania_Input.val();
        txNombreCompania_Input.removeClass(errorClass);
        if(!txNombreCompania_AE){
            txNombreCompania_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoCompania_Input = $("#txEstadoCompania");
        let txEstadoCompania_AE = txEstadoCompania_Input.val();
        txEstadoCompania_Input.removeClass(errorClass);
        if(!txEstadoCompania_AE){
            txEstadoCompania_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}