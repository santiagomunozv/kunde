"use strict";
let errorClass = "is-invalid";

var valorOpcion = ['0','','','',];
var seg_opcion;
$(function(){

    seg_opcion = new GeneradorMultiRegistro('seg_opcion','contenedor_seg_opcion','seg_opcion_');

    seg_opcion.altura = '35px;';
    seg_opcion.campoid = 'oidOpcion';
    seg_opcion.campoEliminacion = 'eliminarOpcion';
    seg_opcion.botonEliminacion = true;
    seg_opcion.funcionEliminacion = '';

    seg_opcion.campos = ['oidOpcion','txNombreOpcion','txRutaOpcion','txIconoOpcion',];

    seg_opcion.etiqueta = ['input','input','input','input',];
    seg_opcion.tipo = ['hidden','text','text','text',];
    seg_opcion.estilo = ['','','','',];
    seg_opcion.clase = ['text-right ','','','',];
    seg_opcion.sololectura = [true,false,false,false,];

    seg_opcion.opciones = ['','','','',];

    seg_opcion.funciones = ['','','','',];
    seg_opcion.otrosAtributos = ['','','','',];

    Opcion.forEach(Opcionreg => {
        seg_opcion.agregarCampos(Opcionreg,'L');
    });

});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-modulo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/seguridad/modulo";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let Paquete_oidPaquete_1aM_Input = $("#Paquete_oidPaquete_1aM");
        let Paquete_oidPaquete_1aM_AE = Paquete_oidPaquete_1aM_Input.val();
        Paquete_oidPaquete_1aM_Input.removeClass(errorClass);
        if(!Paquete_oidPaquete_1aM_AE){
            Paquete_oidPaquete_1aM_Input.addClass(errorClass)
            mensajes.push("El campo Paquete es obligatorio");
        }
		let txNombreModulo_Input = $("#txNombreModulo");
        let txNombreModulo_AE = txNombreModulo_Input.val();
        txNombreModulo_Input.removeClass(errorClass);
        if(!txNombreModulo_AE){
            txNombreModulo_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}