"use strict";
let errorClass = "is-invalid";

var valorUsuarioCompaniaRol = ['0','0','0',];
var seg_usuariocompaniarol;
$(function(){

    seg_usuariocompaniarol = new GeneradorMultiRegistro('seg_usuariocompaniarol','contenedor_seg_usuariocompaniarol','seg_usuariocompaniarol_');

    seg_usuariocompaniarol.altura = '35px;';
    seg_usuariocompaniarol.campoid = 'oidUsuarioCompaniaRol';
    seg_usuariocompaniarol.campoEliminacion = 'eliminarUsuarioCompaniaRol';
    seg_usuariocompaniarol.botonEliminacion = true;
    seg_usuariocompaniarol.funcionEliminacion = '';
    seg_usuariocompaniarol.campos = ['oidUsuarioCompaniaRol','Compania_oidUsuarioCompaniaRol','Rol_oidUsuarioCompaniaRol',];
    seg_usuariocompaniarol.etiqueta = ['input','select','select',];
    seg_usuariocompaniarol.tipo = ['hidden','','',];
    seg_usuariocompaniarol.estilo = ['','','',];
    seg_usuariocompaniarol.clase = ['text-right ','chosen-select','chosen-select',];
    seg_usuariocompaniarol.sololectura = [true,false,false,];
    seg_usuariocompaniarol.opciones = ['',Compania_oidUsuarioCompaniaRollista,Rol_oidUsuarioCompaniaRollista,];
    seg_usuariocompaniarol.funciones = ['','','',];
    seg_usuariocompaniarol.otrosAtributos = ['','','',];

    UsuarioCompaniaRol.forEach(UsuarioCompaniaRolreg => {
        seg_usuariocompaniarol.agregarCampos(UsuarioCompaniaRolreg,'L');
    });

    $("#Tercero_oidUsuario").chosen(chosenConfig.single);

});

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-usuario";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/seguridad/usuario";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){

        
        let mensajes =[];

        let idUsuarioInput = $("#oidUsuario");
        if (idUsuarioInput.val()){
            let password_Input = $("#password");
            let password_AE = password_Input.val();
            password_Input.removeClass(errorClass);
            if(!password_AE){
                password_Input.addClass(errorClass)
                mensajes.push("El campo Clave es obligatorio");
            }
        }

        let txLoginUsuario_Input = $("#txLoginUsuario");
        let txLoginUsuario_AE = txLoginUsuario_Input.val();
        txLoginUsuario_Input.removeClass(errorClass);
        if(!txLoginUsuario_AE){
            txLoginUsuario_Input.addClass(errorClass)
            mensajes.push("El campo Login es obligatorio");
        }
		let txEstadoUsuario_Input = $("#txEstadoUsuario");
        let txEstadoUsuario_AE = txEstadoUsuario_Input.val();
        txEstadoUsuario_Input.removeClass(errorClass);
        if(!txEstadoUsuario_AE){
            txEstadoUsuario_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}