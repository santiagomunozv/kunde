'use strict'
let bancoTercero = [];
let tercero = [JSON.parse(idTerceroL), JSON.parse(nombreTercero)];
$(function(){
    // -------------GENERAMOS MULTI DE COLORES-----------------
    bancoTercero = new GeneradorMultiRegistro('bancoTercero','contenedorBanco','banco');
    bancoTercero.campoid = 'oidTerceroBanco';
    bancoTercero.campoEliminacion = 'eliminarBanco';
    bancoTercero.etiqueta = ['input','select','input','input','select','input'];
    bancoTercero.tipo = ['hidden','text','text','text','text','text'];
    bancoTercero.campos = ['oidTerceroBanco','Tercero_oidTerceroBanco','txNombreCuentaTerceroBanco','txTitularTerceroBanco','lsTipoTerceroBanco','txNumeroCuentaTerceroBanco']
    bancoTercero.opciones = ['',tercero,'','',[['AHORROS','CORRIENTE',], ['AHORROS','CORRIENTE',]],''];
    bancoTercero.clase = ['','chosen-select ','','','',''];
    bancoTercero.sololectura = [true,false,false,false,false,false];

    bancos.forEach( dato => {bancoTercero.agregarCampos(dato , 'L');
        });

});
let errorClass = 'is-invalid';
function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#tercerobanco';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyNombre = 'txNombreCuentaTerceroBanco';
            let keyTitular = 'txTitularTerceroBanco';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let titular = $('input[name="'+keyTitular+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyTitular)){
                    setErroresMultiregistro(index.substr(keyTitular.length , index.length) , titular);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }
}

function setErroresMultiregistro( index , array){
    if(array && array.length){
        array[index].classList.add(errorClass);
    }
}

function validarForm(){
    let mensajes = [];
    let titulares = $('input[name="txTitularTerceroBanco[]"]');
    let nombres = $('input[name="txNombreCuentaTerceroBanco[]"]');
    let tipos = $('select[name="lsTipoTerceroBanco[]"]');
    let bancos = $('select[name="Tercero_oidTerceroBanco[]"]');
    let numeros = $('input[name="txNumeroCuentaTerceroBanco[]"]');
    let maxLength = titulares.length >= nombres.length ? titulares.length : nombres.length;
    for(let i = 0 ; i < maxLength; i++){
        let titular = titulares[i];
        let nombre = nombres[i];
        let tipo = tipos[i];
        let banco = bancos[i];
        let numero = numeros[i];
        if(titular && !titular.value){
            titular.classList.add( errorClass );
            mensajes.push('El titular de la Cuenta Bancaria en el registro '+(i + 1)+' es requerido');
        }
        if(nombre && !nombre.value){
            nombre.classList.add( errorClass );
            mensajes.push('El Nombre de la Cuenta Bancaria en el registro '+(i + 1)+' es requerido');
        }
        if(tipo && !tipo.value){
            tipo.classList.add( errorClass );
            mensajes.push('El Tipo de la Cuenta Bancaria en el registro '+(i + 1)+' es requerido');
        }
        if(banco && !banco.value){
            banco.classList.add( errorClass );
            mensajes.push('El Banco de la Cuenta Bancaria en el registro '+(i + 1)+' es requerido');
        }
        if(numero && !numero.value){
            numero.classList.add( errorClass );
            mensajes.push('El Número de la Cuenta Bancaria en el registro '+(i + 1)+' es requerido');
        }
    
    }
    return mensajes;
}