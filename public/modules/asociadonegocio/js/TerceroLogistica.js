$(function(){
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

    $("#txObservacionTerceroLogistica").summernote({ height: 150 });

})


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#terceroLogistica';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}


function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

let errorClass = 'is-invalid';
function validarForm(){
    let messages = [];

    let validaciones = [
        {'input' :'txTiempoPermanenciaTerceroLogistica' , 'message' : 'El campo Tiempo permanencia en bodega es obligatorio.'},
    ];
    validaciones.forEach( validacion => {
        let input = $('#'+validacion.input);         
        let value = input.val();
        if(!value){
            input.addClass(errorClass);
            messages.push( validacion.message);
        }
    });

    let sitioEntrega = $('#lmSitioEntregaTerceroLogistica').serialize();
    if(!sitioEntrega){
        $('#lmSitioEntregaTerceroLogistica_chosen').addClass(errorClass)
        messages.push( "Debes seleccionar al menos un sitio de entrega");
    }
    let requisitoEntrega = $('#lmRequisitoEntregaTerceroLogistica').serialize();
    if(!requisitoEntrega){
        $('#lmRequisitoEntregaTerceroLogistica_chosen').addClass(errorClass)
        messages.push( "Debes seleccionar al menos un requisito de entrega");
    }

    return messages;
}
