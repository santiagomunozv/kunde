'use strict'
let grupoTercero = [];
$(function(){
    // -------------GENERAMOS MULTI DE COLORES-----------------
    grupoTercero = new GeneradorMultiRegistro('grupoTercero','contenedorGrupoTercero','GrupoTercero');
    grupoTercero.campoid = 'oidGrupoTercero';
    grupoTercero.campoEliminacion = 'eliminarGrupoTercero';
    grupoTercero.etiqueta = ['input','input','input','input','input','checkbox'];
    grupoTercero.tipo = ['hidden','text','text',correo,correo,''];
    grupoTercero.campos = ['oidGrupoTercero','txCodigoGrupoTercero','txNombreGrupoTercero','txCorreoGrupoTercero','txMensajeGrupoTercero','chObservacionGrupoTercero']
    grupoTercero.opciones = ['','','','','',''];
    grupoTercero.clase = ['','','','','',''];
    grupoTercero.sololectura = [true,false,false,false,false,false];

    GrupoTercero.forEach( dato => {grupoTercero.agregarCampos(dato , 'L');
    });
});
let errorClass = 'is-invalid';
function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#GrupoTercero';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = '/asociadonegocio/grupotercero ';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyNombre = 'txNombreGrupoTercero';
            let keyCodigo = 'txCodigoGrupoTercero';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let codigos = $('input[name="'+keyCodigo+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyCodigo)){
                    setErroresMultiregistro(index.substr(keyCodigo.length , index.length) , codigos);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function setErroresMultiregistro( index , array){
        if(array && array.length){
            array[index].classList.add(errorClass);
        }
    }
 
    function validarForm(){
        let mensajes = [];
        let codigos = $('input[name="txCodigoGrupoTercero[]"]');
        let nombres = $('input[name="txCodigoGrupoTercero[]"]');
        let maxLength = codigos.length >= nombres.length ? codigos.length : nombres.length;
        for(let i = 0 ; i < maxLength; i++){
            let codigo = codigos[i];
            let nombre = nombres[i];
            if(codigo && !codigo.value){
                codigo.classList.add( errorClass );
                mensajes.push('El código de grupos en el registro '+(i + 1)+' es requerido');
            }
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de grupos en el registro '+(i + 1)+' es requerido');
            }
        }
        return mensajes;
    }
}