"use strict";
let errorClass = 'is-invalid';
var terceroimpuesto;
var arrayImpuestos = [];


$(function () {

    //iniciando chosen select
    $('#ActividadEconomica_oidTerceroTributario').chosen();
    $('#Tercero_oidVendedor').chosen();

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

});

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-asn_tercerocomercial';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}
function validarForm(){
    let regimen = $('#lsRegimenVentaTerceroTributario');
    let clasificacionRenta = $('#ClasificacionRenta_oidTerceroTributario');
    let impuestos = $('select[name="lsConceptoTerceroImpuesto[]"]');
    let mensajes =[];
    $('.'+errorClass).removeClass(errorClass);
    requerido(clasificacionRenta,mensajes,'El campo Clasificación es obligatorio.');
    requerido(regimen,mensajes,'El campo Régimen es obligatorio.');
    for(let i = 0 ; i < impuestos.length; i++){
        requerido(impuestos[i],mensajes,'El Impuesto es obligatorio');
    }
    return mensajes;
}
function requerido( input, messages , message){
    if(!$(input).val()){
        $(input).addClass( errorClass );
        messages.push(message);
    }
}