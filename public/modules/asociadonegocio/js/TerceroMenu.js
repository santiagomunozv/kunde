

function modalTercero() {
    let ruta ='../asn_tercero/'+window.id+'/edit';
    let iframe = $('#iframeGeneral' );
    if (iframe.length) {
        iframe.attr('src', ruta);
        $('#ModalGeneral').modal('show');
    }
}


$(function(){
    let token = parent.token;
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': token
        },
        dataType: "json",
        url: '/Asn_TerceroMenuData?id=' + id,
        type: 'get',
        success: function(data) {
            document.getElementById("txDocumentoTercero").innerHTML = data[1];
            document.getElementById("txNombreTercero").innerHTML = data[2];
            document.getElementById("txCorreoElectronicoTercero").innerHTML = data[3];
            document.getElementById("txTelefonoTercero").innerHTML = data[4];
            document.getElementById("Ciudad_oidTercero").innerHTML = data[5];
            document.getElementById("txDireccionTercero").innerHTML = data[6];
            document.getElementById("imImagenTercero").src = "/" + data[7];
            document.getElementById("txEstadoTercero").innerHTML = data[8];
        },
        error: function(xhr, err) {
            alert('Se ha producido un error: ' + err);
        }
    });
});


