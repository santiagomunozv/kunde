let errorClass = 'is-invalid';

let tercerocontacto = [];
valorTipoTercero =  Array("COM","ADM","CTR","CON","2","3","4");
nombreTipoTercero =  Array("Comercial","Administrativo", "Cartera", "Contabilidad","Entrega / Despacho","Contabilidad","Compras / Ventas");
let tipoTercero = [valorTipoTercero,nombreTipoTercero];

let tipoContactoTercero = [JSON.parse(idTipoContacto), JSON.parse(nombreTipoContacto)];

$(function(){
    
    tercerocontacto = new GeneradorMultiRegistro('tercerocontacto','contenedortercerocontacto','tercerocontacto');      
    tercerocontacto.altura = '35px;';
    tercerocontacto.campoid = 'oidTerceroContacto';
    tercerocontacto.campoEliminacion = 'eliminarAsn_TerceroContacto';
    tercerocontacto.botonEliminacion = true;
    tercerocontacto.funcionEliminacion = '';  
    tercerocontacto.campos = ['oidTerceroContacto','txNombreTerceroContacto','txCargoTerceroContacto','txCorreoTerceroContacto','txMovilTerceroContacto','txFormularioTerceroContacto','TipoContactoTercero_oidContactoTercero'];
    tercerocontacto.etiqueta = ['input','input','input','input','input','input','select'];
    tercerocontacto.tipo = ['hidden','text','text','text','text','text',''];
    tercerocontacto.estilo = ['','','','','','',''];
    tercerocontacto.clase = ['','','','','','','chosen-select'];
    tercerocontacto.sololectura = [true,false,false,false,false,false,false];
    tercerocontacto.opciones = ['','','','','','',tipoContactoTercero];
    tercerocontacto.funciones = ['','','','','','',''];
    tercerocontacto.otrosAtributos = ['','','','','','',''];

    valorAsn_TerceroContacto.forEach( (dato,key) => {tercerocontacto.agregarCampos(dato , 'L');
        $('table#tabla-contactos').removeClass('d-none');
        // cuando la lista de selección es MULTIPLE, recibimos los valores como un strin separado por
        // comas y lo convertimos en array para entregarlo al objeto select chosen
        arraySelect = dato['txTipoTerceroContacto'].split(",");
        if(arraySelect.length > 0)
        {
            $("#txTipoTerceroContacto"+key).val(arraySelect).trigger("chosen:updated");        
        }
    
    });
});

function grabar(idPadre){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-asn_tercerocontacto';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyNombre = 'txNombreTerceroContacto';
            let keyCargo = 'txCorreoTerceroContacto';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let correo = $('input[name="'+keyCargo+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyCargo)){
                    setErroresMultiregistro(index.substr(keyCargo.length , index.length) , correo);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    } 
}

function setErroresMultiregistro( index , array){
    if(array && array.length){
        array[index].classList.add(errorClass);
    }
}

function validarForm(){
    let mensajes = [];
    let correos = $('input[name="txCorreoTerceroContacto[]"]');
    let nombres = $('input[name="txNombreTerceroContacto[]"]');
    let movils = $('input[name="txMovilTerceroContacto[]"]');
    let maxLength = correos.length >= nombres.length ? correos.length : nombres.length;
    for(let i = 0 ; i < maxLength; i++){
        let correo = correos[i];
        let nombre = nombres[i];
        let movil = movils[i];
        if(correo && !correo.value){
            correo.classList.add( errorClass );
            mensajes.push('El Correo del contacto en el registro '+(i + 1)+' es requerido');
        }
        if(nombre && !nombre.value){
            nombre.classList.add( errorClass );
            mensajes.push('El Nombre del Contacto en el registro '+(i + 1)+' es requerido');
        }
        if(movil && !movil.value){
            movil.classList.add( errorClass );
            mensajes.push('El Móvil del Contacto en el registro '+(i + 1)+' es requerido');
        }
    }
    return mensajes;
}