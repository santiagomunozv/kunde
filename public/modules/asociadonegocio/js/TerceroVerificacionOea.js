let errorClass = 'is-invalid';
let registros = JSON.parse(valores);
let registroDetalle = JSON.parse(valoresDetalle);
let TerceroVerificacion = [];
// valorImpacto =  Array("1","3","7","10");
// nombreImpacto =  Array("1-Sin impacto relevante","3-Bajo impacto", "7-Impacto moderado pero no crítico", "10-Impacto critico");
// let impacto = [valorImpacto,nombreImpacto];

let calcularPonderacon = ['onchange','calcularPonderacion()']
$(function(){    TerceroVerificacion = new GeneradorMultiRegistro('TerceroVerificacion','contenedorterceroverificacion','TerceroVerificacion');
    TerceroVerificacion.campoid = 'oidTerceroVerificacion';
    TerceroVerificacion.campoEliminacion = '';
    TerceroVerificacion.etiqueta = ['input','input','input','input','select'];
    TerceroVerificacion.tipo = ['hidden','text','text','text','select'];
    TerceroVerificacion.campos = ['Preguntas_oidTerceroVerificacion','lsTipoPreguntaOea','txNombrePreguntaOea','inPorcentajePreguntaOea','lsImpactoTerceroVerificacion']
    TerceroVerificacion.opciones = ['','','','',''];
    TerceroVerificacion.funciones = ['','','','',calcularPonderacon];
    TerceroVerificacion.clase = ['','','','',''];
    TerceroVerificacion.sololectura = [true,true,true,true,false];
    TerceroVerificacion.botonEliminacion = false;

    reg = 0;
    while (reg < registros.length) {
        TerceroVerificacion.agregarCampos(registros[reg] , 'L')
        crearSelect(registros[reg].Preguntas_oidTerceroVerificacion,reg,registros[reg].lsImpactoTerceroVerificacion);
        reg = reg + 1;
    }
});

function crearSelect(idPregunta,registro,valueActual){
    let token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            dataType: "json",
            url: '/asociadonegocio/consultaSelectOea/'+idPregunta,
            type: 'GET',
            success: function(resp){

                var respuesta = resp;

                reg = registro;
                    
                var select = document.getElementById('lsImpactoTerceroVerificacion'+reg);
                    
                select.options.length = 0;
                var option = '';

                option = document.createElement('option');
                option.value = 0;
                option.text = 'Seleccione...';
                select.appendChild(option);

                for (var i = 0; i < respuesta.length ; i++)
                {
                    option = document.createElement('option');
                    option.value = respuesta[i]["inCalificacionPreguntaOeaDetalle"];
                    option.title = respuesta[i]["txObservacionPreguntaOeaDetalle"];
                    option.text = respuesta[i]["inCalificacionPreguntaOeaDetalle"]+' - '+ respuesta[i]["txTextoPreguntaOeaDetalle"];
                    option.selected = (valueActual ==  respuesta[i]["inCalificacionPreguntaOeaDetalle"] ? true : false);
                    select.appendChild(option);
                }

            },
            error: function(resp) {

            }
        });
}


function cambiarMensaje(){
}


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#terceroverificacionoea';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}

function validarForm(){
    let impacto = $('select[name="lsImpactoTerceroVerificacion[]"]');
    let mensajes =[];
    $('.'+errorClass).removeClass(errorClass);
    requerido(impacto,mensajes,'El campo de impacto es obligatorio.');
    for(let i = 0 ; i < impacto.length; i++){
        requerido(impacto[i],mensajes,'El campo de impacto es obligatorio en la posición :  '+[i]);
    }
    return mensajes;
}
function requerido( input, messages , message){
    if(!$(input).val()){
        $(input).addClass( errorClass );
        messages.push(message);
    }
}

function calcularPonderacion(){

    let totalPonderacion = 0;
    let valorReg = 0;
    let porcentaje = 0
    
    for (let i = 0; i < TerceroVerificacion.contador; i++) 
    {
        if($("#lsImpactoTerceroVerificacion"+i).val() != '')
        {
            valorReg = $("#lsImpactoTerceroVerificacion"+i).val();
        }

        porcentaje = $("#inPorcentajePreguntaOea"+i).val()/100;

        totalPonderacion = totalPonderacion + (porcentaje * valorReg);

        $("#dePonderacionTerceroVerificacionOea").val(parseFloat(totalPonderacion).toFixed(1));

    }

    if(totalPonderacion < 6.0)
        $("#txImpactoTerceroVerificacionOea").val('No critico')
    else
    $("#txImpactoTerceroVerificacionOea").val('critico')

}