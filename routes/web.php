<?php
Route::get('login', 'Auth\LoginController@mostrarLoginForm');
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get("/getdiscocfile" , "GeneralController@downloadDiscoCFile");
Route::group(['middleware' => 'auth'], function(){
    Route::get('/','HomeController@index');
    Route::post("/uploadtmpfile" , "GeneralController@uploadTmpFile");
    Route::post("/cambiocompania" , "Auth\LoginController@cambioCompania");

    Route::post('/cambiarestadodata' , function(){
        $id = (isset($_POST["id"]) ? $_POST["id"] : '');
        $tabla = (isset($_POST["tabla"]) ? $_POST["tabla"] : '');
        $campo = (isset($_POST["campo"]) ? $_POST["campo"] : '');
        DB::update("UPDATE $tabla SET txEstado$campo = CASE WHEN txEstado$campo = 'Activo' THEN 'Anulado' ELSE 'Activo' END WHERE oid$campo = $id");
        return response(200);
    });

    Route::post('/eliminarregistrodata' , function(){
        $id = (isset($_POST["id"]) ? $_POST["id"] : '');
        $tabla = (isset($_POST["tabla"]) ? $_POST["tabla"] : '');
        $terminacionTabla = (isset($_POST["terminacionTabla"]) ? $_POST["terminacionTabla"] : '');
        try {
            $consulta = DB::delete("DELETE FROM  $tabla WHERE oid$terminacionTabla = $id");
            return response(200);
        } catch (Exception $e) {
            $bd = ENV('DB_DATABASE','scalia2');
            // cuando se genera error es porque el registro tiene FK que evitan su eliminación
            $tablas = DB::select(
                "SELECT TABLE_COMMENT as Tabla, COLUMN_COMMENT as Campo
                FROM information_schema.COLUMNS 
                LEFT JOIN information_schema.TABLES ON COLUMNS.TABLE_NAME = TABLES.TABLE_NAME AND COLUMNS.TABLE_SCHEMA = TABLES.TABLE_SCHEMA
                where COLUMNS.TABLE_SCHEMA = '$bd'
                    and COLUMNS.COLUMN_NAME like CONCAT('$terminacionTabla','_%')
                    and COLUMNS.TABLE_NAME not like CONCAT('$tabla','%')
                ORDER BY COLUMNS.TABLE_NAME;");
                
            $view = view('eliminarRegistro', compact('tablas'))->render();

            // devolvemos error 423 -> Locked: este recurso está bloqueado.
            return response(['tabla'=>$view],423);
        }
    });

});