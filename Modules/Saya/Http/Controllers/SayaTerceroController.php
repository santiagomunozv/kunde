<?php

namespace Modules\Saya\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Modules\Seguridad\Entities\CompaniaModel;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Database\Eloquent\Collection;
use Modules\AsociadoNegocio\Entities\GrupoProductoModel;
use Modules\General\Entities\ClasificacionProductoGrupoModel;
use Modules\General\Entities\PartidaArancelariaGravamenModel;
use Modules\Saya\Entities\SayaFichaFactory;

// abstract
abstract class SayaTerceroController extends Controller
{
    private static $DATABASE_IDENTIFIER = 'txBaseDatosCompania';
    private static $SCALIA_DB = 'DB_DATABASE';
    private function all($tabla, $idPadre, &$origen, &$destino,$estructura){
        //inicializamos cvariables para armar un INSERT UPDATE
        $camposInsert = '';
        $valoresInsert = '';
        $valoresUpdate = '';

        // recorremos los campos de la tabla de SAYA para crear un insert update con ellos
        foreach($estructura as $reg => $valor){
            
            // Buscamos el nombre de campo en el array de datos, si existe lo llenamos en el objeto, sino no
            if(array_key_exists($valor->COLUMN_NAME, $origen) and !($origen[$valor->COLUMN_NAME]=== '' or $origen[$valor->COLUMN_NAME] === null))
            {
                $dato = "'".$origen[$valor->COLUMN_NAME]."'";
            }else
            {
                if(array_key_exists($valor->COLUMN_NAME, $destino) and !($destino[$valor->COLUMN_NAME]=== '' or $destino[$valor->COLUMN_NAME] === null)){
                    $dato = "'".$destino[$valor->COLUMN_NAME]."'";
                }else{
                     // si el campo no lo tenemos en nuestra BD de Scalia, lo inicializamos con un valor por defecto segun el tipo de dato
                    switch($valor->DATA_TYPE)
                    {
                        case 'date':
                            $dato = "'2019-01-01'";
                            break;
                        case 'time':
                            $dato = "'01:00:00'";
                            break;
                        case 'datetime':
                            $dato = "'2019-01-01 01:00:00'";
                            break;
                        case 'int':
                            $dato = 0;
                            break;
                        case 'decimal':
                            $dato = 0.0;
                            break;
                        case 'tinyint':
                            $dato = 0;
                            break;
                        default:
                            $dato = "''";
                            break;
                    }
                }
            }
            // si es el ID de una tabla padre lo llenamos ocn le parametro
            if($valor->COLUMN_NAME == $idPadre)
            {
     
                // concatenamos los datos del INSERT (campos y valores)
                $camposInsert .= $idPadre.',';
                $valoresInsert .=  $destino["idPadre"].',';

                // Para el UPDATE no incluimos el ID que siempre es el primer campo de la estructura
                if($reg > 0)
                    $valoresUpdate .= $idPadre .' = '. $destino["idPadre"] . ',';
            }
            // si es el campo de id principal de la tabla, tomamos el valor desde la tabla de destino, no de origen
            elseif($valor->COLUMN_NAME == 'id'.$tabla)
            {
                $dato = ($destino['id'.$tabla] == '' ? 'null' : $destino['id'.$tabla]);
                // concatenamos los datos del INSERT (campos y valores)
                $camposInsert .= 'id'.$tabla.',';
                $valoresInsert .=  $dato.',';

                // Para el UPDATE no incluimos el ID que siempre es el primer campo de la estructura
                if($reg > 0)
                    $valoresUpdate .= 'id'.$tabla .' = '. $dato . ',';
            }
            //solo aplica para la tabla tipo idet
            elseif($valor->COLUMN_NAME == 'idIdentificacion')
            {
                $dato = ($destino['idIdentificacion'] == '' ? 'null' : $destino['idIdentificacion']);
                // concatenamos los datos del INSERT (campos y valores)
                $camposInsert .= 'idIdentificacion'.',';
                $valoresInsert .=  $dato.',';

                // Para el UPDATE no incluimos el ID que siempre es el primer campo de la estructura
                if($reg > 0)
                    $valoresUpdate .= 'idIdentificacion' .' = '. $dato . ',';
            }
            else
            {
                // concatenamos los datos del INSERT (campos y valores)
                $camposInsert .= $valor->COLUMN_NAME.',';
                $valoresInsert .= $dato.',';

                // Para el UPDATE no incluimos el ID que siempre es el primer campo de la estructura
                if($reg > 0)
                    $valoresUpdate .= $valor->COLUMN_NAME .' = '. $dato . ',';
            }
        }

        // quitamos la ultima coma al final de cada variable concatenada
        $camposInsert = substr($camposInsert, 0 , strlen($camposInsert)-1);
        $valoresInsert = substr($valoresInsert, 0 , strlen($valoresInsert)-1);
        $valoresUpdate = substr($valoresUpdate, 0 , strlen($valoresUpdate)-1);

        // armamos y retornamos la instruccion insert update para que se ejecute en el proceso principal
        // echo "INSERT INTO $tabla ($camposInsert) VALUES($valoresInsert) ON DUPLICATE KEY UPDATE $valoresUpdate\n\n";
        return "INSERT INTO $tabla ($camposInsert) VALUES($valoresInsert) ON DUPLICATE KEY UPDATE $valoresUpdate;";
    }

    public function estructuraTabla($conexion,$bd,$tabla){
        // Consultamos la estructura de la tabla de destiono de SAYA en la BD correspondiente
        $estructura = $conexion->select(
            "SELECT COLUMN_NAME, DATA_TYPE
            FROM information_schema.COLUMNS 
            LEFT JOIN information_schema.TABLES ON COLUMNS.TABLE_NAME = TABLES.TABLE_NAME AND COLUMNS.TABLE_SCHEMA = TABLES.TABLE_SCHEMA
            where COLUMNS.TABLE_SCHEMA = '$bd' 
                -- and IS_NULLABLE = 'NO'
                and COLUMNS.TABLE_NAME = '$tabla'
            ORDER BY COLUMNS.TABLE_NAME;");

        return $estructura;
    }

    public function actualizarTercero($id,$documentoAnterior)
    {
        // consultamos el id de usuario en saya con el usaurio de scalia
        set_time_limit(0);
        DB::beginTransaction();
        try {
                $companias = DB::select("SELECT
                Compania_oidCompania
                FROM asn_tercero
                LEFT JOIN 
                    asn_tercerocompania on Tercero_oidTercero_1aM = oidTercero
                WHERE oidTercero = $id");
                // Convertimos los id de compañías en una array para recorrerlos
                // $companias = explode(',', $companias[0]->txCompaniasTercero);
                foreach($companias as $reg => $valor)
                {
                    // Consultamos la compañía para obtener el nombre de la base de datos
                    $bdCompania = CompaniaModel::where('oidCompania','=',$valor->Compania_oidCompania)->get();
                    $BDExterna = $bdCompania[0]["txBaseDatosCompania"];
                    $idCompania = $bdCompania[0]["oidCompania"];
                    $idUsuario = Auth::id();
                    $idCreador = 0;
                    $idModificador = 0;

                    
                    //consultamos el usuario creador homologado con saya para guardarlos en la db de saya
                    $userCreador = DB::select("SELECT
                    IL.id as terceroCreador
                    FROM seg_usuario
                    LEFT JOIN $BDExterna.SegLogin IL on txLoginUsuario = usuarioLogin
                    WHERE oidUsuario = $idUsuario");
                    $idCreador = ($userCreador[0]->terceroCreador ? $userCreador[0]->terceroCreador : '0');
                    
                    
                    //consultamos el usuario creador homologado con saya para guardarlos en la db de saya
                    $userModificador = DB::select("SELECT
                    IL.id as terceroCreador
                    FROM seg_usuario
                    LEFT JOIN $BDExterna.SegLogin IL on txLoginUsuario = usuarioLogin
                    WHERE oidUsuario = $idUsuario");
                    $idModificador = ($userModificador[0]->terceroCreador ? $userModificador[0]->terceroCreador : '0');
                    
                    // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
                    $consulta = DB::select("SELECT 
                        ST.txCompaniasTercero,
                        ITI.idIdentificacion AS TipoIdentificacion_idIdentificacion,
                        ST.txDigitoVerificacionTercero AS digitoVerificacionTercero,
                        ST.txDocumentoTercero AS documentoTercero,
                        ST.txCodigoTercero AS codigoAlterno1Tercero,
                        ST.txCodigoBarrasTercero AS codigoBarrasTercero,
                        ST.txPrimerNombreTercero AS nombreATercero,
                        ST.txSegundoNombreTercero AS nombreBTercero,
                        ST.txPrimerApellidoTercero AS apellidoATercero,
                        ST.txSegundoApellidoTercero AS apellidoBTercero,
                        ST.txNombreTercero AS nombre1Tercero,
                        ST.txNombreComercialTercero AS nombre2Tercero,
                        ST.daFechaCreacionTercero AS fechaCreacionTercero,
                        ST.daFechaCreacionTercero AS fechaAdicionarTercero,
                        ST.txDireccionTercero AS direccionTercero,
                        ST.txDireccionTercero AS direccionRutTercero,
                        IC.idCiudad AS Ciudad_idCiudad,
                        ST.txTelefonoTercero AS telefono1Tercero,
                        ST.txMovilTercero AS movilTercero,
                        ST.txCorreoElectronicoTercero AS correoElectronicoTercero,
                        ST.txEstadoTercero AS estadoTercero,
                        CONCAT('../documentos_terceros/foto/',ST.imImagenTercero) AS imagenTercero,
                        tipo.tipoTercero AS tipoTercero,
                        ITVD.idTercero AS Tercero_idVendedor,
                        IFPV.idFormaPago AS FormaPago_idFormaPago,
                        IFPC.idFormaPago AS FormaPago_idFormaPagoCompra,
                        chPracticaRetencionTerceroTributario AS esAgenteRetenedorTercero,
                        lsRegimenVentaTerceroTributario AS regimenVentasTercero,
                        chAutoretenedorTerceroTributario AS esAutoretenedor,
                        idNaturalezaJuridica AS NaturalezaJuridica_idNaturalezaJuridica,
                        chAutoreenedorCREETerceroTributario AS esAutoretenedorCREETercero,
                        actividadEconomica.idActividadEconomica AS ActividadEconomica_idActividadEconomica,
                        chGranContribuyenteTerceroTributario AS esGranContribuyente,
                        idClasificacionRenta AS ClasificacionRenta_idClasificacionRenta,
                        chEntidadEstadoTerceroTributario AS esEntidadEstadoTercero,
                        chNoResponsableIVATerceroTributario AS noResponsableIVATercero,
                        idGrupoNomina AS GrupoNomina_idGrupoNomina,
                        cargos.idCargo AS Cargo_idCargo,
                        inPersonasACargoTerceroLaboral AS personasACargoTercero,
                        inNumeroHijosTerceroLaboral AS numeroHijosTercero,
                        deIngresosFamiliares AS ingresoFamiliarTercero,
                        daFechaRetiroPension AS fechaRetiroPensionTercero,
                        chExtranjeroPension AS extranjeroNoPensionTercero,
                        chColombianoExterior AS colombianoResidenteExteriorTercero,
                        ITS.idTercero AS Tercero_idSalud,
                        ITP.idTercero AS Tercero_idPension,
                        ITC.idTercero AS Tercero_idCesantias,
                        ITCJ.idTercero AS Tercero_idCajaCompensacion,
                        centroCosto.idCentroCosto AS CentroCosto_idCentroCosto,
                        centroTrabajo.idCentroTrabajo AS CentroTrabajo_idCentroTrabajo,
                        IF(centroCosto.idCentroCosto != '', 'U', NULL) AS tipoDistribucionTercero,
                        lsSexoTerceroPersonal AS sexoTercero,
                        lsGrupoSanguineoTerceroPersonal AS grupoSanguineoTercero,
                        lsRHTerceroPersonal AS factorRHTercero,
                        daFechaExpedicionTerceroPersonal AS fechaExpedicionDocumentoTercero,
                        ICEX.idCiudad AS Ciudad_idLugarExpedicion,
                        daFechaNacimientoTerceroPersonal AS fechaNacimientoTercero,
                        ICNC.idCiudad AS Ciudad_idLugarNacimiento,
                        lsEstadoCivilTerceroPersonal AS estadoCivilTercero,
                        clasificacion.idClasificacionTercero AS ClasificacionTercero_idClasificacionTercero,
                        IF(IT.SegLogin_idAdicionar != 0,IT.SegLogin_idAdicionar,$idCreador) AS SegLogin_idAdicionar,
                        $idModificador AS SegLogin_idModificar,
                        calculosTributarios.calcularRetencionFuenteProveedorSinBaseTercero,
                        calculosTributarios.calcularRetencionFuenteClienteSinBaseTercero,
                        calculosTributarios.calcularRetencionCreeProveedorSinBaseTercero,
                        calculosTributarios.calcularRetencionCreeClienteSinBaseTercero,
                        calculosTributarios.calcularRetencionIvaProveedorSinBaseTercero,
                        calculosTributarios.calcularRetencionIvaClienteSinBaseTercero, 
                        calculosTributarios.calcularRetencionIcaProveedorSinBaseTercero,
                        calculosTributarios.calcularRetencionIcaClienteSinBaseTercero

                    FROM
                        asn_tercero AS ST
                            LEFT JOIN
                        (SELECT 
                            Tercero_oidTercero_1aM,
                                GROUP_CONCAT(IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 1, '*01*', 
                                IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 2, '*02*', 
                                IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 3, '*05*', 
                                IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 4, '*06*', 
                                IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 5, '*19*', 
                                IF(IT.ClasificacionTercero_oidTerceroClasificacion_1aM = 6, '*11**12**13**14**15*', 0))))))) AS tipoTercero
                        FROM
                            asn_terceroclasificacion IT
                        WHERE
                            Tercero_oidTercero_1aM = $id) tipo ON tipo.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS IT ON BINARY IT.documentoTercero = BINARY ST.txDocumentoTercero
                            AND IT.tipoTercero NOT LIKE '%18%'
                            LEFT JOIN
                        asn_tercerocomercial AS TC ON TC.Tercero_oidTercero_1a1 = ST.oidTercero AND TC.Compania_oidCompania = $idCompania   
                            LEFT JOIN
                        asn_tercerotributario AS TT ON TT.Tercero_oidTercero_1a1 = ST.oidTercero
                            LEFT JOIN
                        asn_tercerolaboral AS TL ON TL.Tercero_oidTercero_1a1 = ST.oidTercero
                            LEFT JOIN
                        asn_terceropersonal AS TP ON TP.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        asn_tercerosucursal AS TS ON TS.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        gen_formapago AS SFPV ON TC.FormaPago_oidVenta = SFPV.oidFormaPago
                            LEFT JOIN
                        $BDExterna.FormaPago AS IFPV ON BINARY IFPV.codigoAlternoFormaPago = BINARY SFPV.txCodigoFormaPago
                            LEFT JOIN
                        gen_formapago AS SFPC ON TC.FormaPago_oidCompra = SFPC.oidFormaPago
                            LEFT JOIN
                        $BDExterna.FormaPago AS IFPC ON BINARY IFPC.codigoAlternoFormaPago = BINARY SFPC.txCodigoFormaPago
                            LEFT JOIN
                        gen_tipoidentificacion AS STI ON ST.TipoIdentificacion_oidTercero = STI.oidTipoIdentificacion
                            LEFT JOIN
                        $BDExterna.TipoIdentificacion AS ITI ON BINARY STI.txCodigoTipoIdentificacion = BINARY ITI.codigoIdentificacion
                            LEFT JOIN
                        gen_ciudad AS SC ON ST.Ciudad_oidTercero = SC.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS IC ON BINARY IC.codigoAlternoCiudad = BINARY SC.txCodigoCiudad
                            LEFT JOIN
                        gen_naturalezajuridica AS SNJ ON TT.NaturalezaJuridica_oidTerceroTributario = SNJ.oidNaturalezaJuridica
                            LEFT JOIN
                        $BDExterna.NaturalezaJuridica AS INJ ON BINARY INJ.codigoAlternoNaturalezaJuridica = BINARY SNJ.txCodigoNaturalezaJuridica
                            LEFT JOIN
                        gen_clasificacionrenta AS SCR ON TT.ClasificacionRenta_oidTerceroTributario = SCR.oidClasificacionRenta
                            LEFT JOIN
                        $BDExterna.ClasificacionRenta AS ICR ON BINARY ICR.codigoAlternoClasificacionRenta = BINARY SCR.txCodigoClasificacionRenta
                            LEFT JOIN
                        gen_gruponomina AS SGN ON TL.GrupoNomina_oidTerceroLaboral = SGN.oidGrupoNomina
                            LEFT JOIN
                        $BDExterna.GrupoNomina AS IGN ON BINARY IGN.codigoAlternoGrupoNomina = BINARY SGN.txCodigoGrupoNomina
                            LEFT JOIN
                        asn_tercero AS STVD ON TC.Tercero_oidVendedor = STVD.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITVD ON BINARY ITVD.documentoTercero = BINARY STVD.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STS ON TL.Tercero_oidSalud = STS.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITS ON BINARY ITS.documentoTercero = BINARY STS.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STP ON TL.Tercero_oidPension = STP.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITP ON BINARY ITP.documentoTercero = BINARY STP.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STC ON TL.Tercero_oidCesantias = STC.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITC ON BINARY ITC.documentoTercero = BINARY STC.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STCJ ON TL.Tercero_oidCaja = STCJ.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITCJ ON BINARY ITCJ.documentoTercero = BINARY STCJ.txDocumentoTercero
                            LEFT JOIN
                        gen_ciudad AS SCEX ON TP.Ciudad_oidExpedicion = SCEX.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS ICEX ON BINARY ICEX.codigoAlternoCiudad = BINARY SCEX.txCodigoCiudad
                            LEFT JOIN
                        gen_ciudad AS SCNC ON TP.Ciudad_oidNacimiento = SCNC.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS ICNC ON BINARY ICNC.codigoAlternoCiudad = BINARY SCNC.txCodigoCiudad
                            LEFT JOIN
                        (
                            SELECT 
                                codigoAlternoClasificacionTercero,
                                    Tercero_oidTercero_1aM,
                                    idClasificacionTercero
                            FROM
                                asn_tercero ST
                            LEFT JOIN asn_tercerogrupo STG ON STG.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN asn_grupotercero SGT ON STG.GrupoTercero_oidTerceroGrupo = SGT.oidGrupoTercero
                            LEFT JOIN gen_clasificaciontercerogrupo SCTG ON SGT.ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = SCTG.oidClasificacionTerceroGrupo
                            LEFT JOIN $BDExterna.ClasificacionTercero ICFT ON BINARY SGT.txCodigoGrupoTercero = BINARY ICFT.codigoAlternoClasificacionTercero
                            WHERE
                                oidTercero = $id
                                    AND SCTG.txNombreClasificacionTerceroGrupo = 'Clasificacion'
                            GROUP BY oidTercero) clasificacion ON clasificacion.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        (
                            SELECT 
                                idCentroCosto,
                                Tercero_oidTercero_1aM
                            FROM
                                asn_tercerocentrocosto
                                    LEFT JOIN
                                gen_centrocosto ON CentroCosto_oidTerceroCentroCosto = oidCentroCosto
                                    LEFT JOIN
                                $BDExterna.CentroCosto ON BINARY txCodigoCentroCosto = BINARY codigoAlternoCentroCosto
                                    LEFT JOIN
                                seg_compania ON Compania_oidCompania = oidCompania
                            WHERE
                                Tercero_oidTercero_1aM = $id
                                AND oidCompania = $idCompania
                                group by idCentroCosto
                        ) centroCosto on centroCosto.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        (
                            SELECT 
                                idCentroTrabajo,
                                Tercero_oidTercero_1aM
                            FROM
                                asn_tercerocentrotrabajo
                                    LEFT JOIN
                                gen_centrotrabajo ON CentroTrabajo_oidTerceroCentroTrabajo = oidCentroTrabajo
                                    LEFT JOIN
                                $BDExterna.CentroTrabajo ON BINARY txCodigoCentroTrabajo = BINARY codigoAlternoCentroTrabajo
                                    LEFT JOIN
                                seg_compania ON Compania_oidCompania = oidCompania
                            WHERE
                                Tercero_oidTercero_1aM = $id
                                    AND oidCompania = $idCompania
                            GROUP BY idCentroTrabajo
                        ) centroTrabajo on centroTrabajo.Tercero_oidTercero_1aM = ST.oidTercero
                        LEFT JOIN
                        (
                            SELECT 
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Fuente', chProveedorBase0TerceroImpuesto, null )),0) as calcularRetencionFuenteProveedorSinBaseTercero,
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Fuente', chClienteBase0TerceroImpuesto, null )),0) as calcularRetencionFuenteClienteSinBaseTercero,

                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. CREE', chProveedorBase0TerceroImpuesto, null )),0) as calcularRetencionCreeProveedorSinBaseTercero,
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. CREE', chClienteBase0TerceroImpuesto, null )),0) as calcularRetencionCreeClienteSinBaseTercero,
                                
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Iva', chProveedorBase0TerceroImpuesto, null )),0) as calcularRetencionIvaProveedorSinBaseTercero,
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Iva', chClienteBase0TerceroImpuesto, null )),0) as calcularRetencionIvaClienteSinBaseTercero,
                                
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Ica', chProveedorBase0TerceroImpuesto, null )),0) as calcularRetencionIcaProveedorSinBaseTercero,
                                ifnull(MIN(if(lsConceptoTerceroImpuesto = 'Ret. Ica', chClienteBase0TerceroImpuesto, null )),0) as calcularRetencionIcaClienteSinBaseTercero,
                                
                                oidTercero
                            FROM
                                asn_tercero
                                    LEFT JOIN
                                kunde.asn_terceroimpuesto ON Tercero_oidTercero_1aM = oidTercero
                            WHERE
                                oidTercero = $id
                        ) calculosTributarios on calculosTributarios.oidTercero = ST.oidTercero
                        LEFT JOIN
                        (
                            SELECT 
                                txCodigoCargo, 
                                codigoAlternoCargo,
                                Tercero_oidTercero_1aM,
                                idCargo
                            FROM
                                asn_tercerocargo
                                    LEFT JOIN
                                gen_cargo ON Cargo_oidTerceroCargo = oidCargo
                                    LEFT JOIN
                                $BDExterna.Cargo ON BINARY txCodigoCargo = BINARY codigoAlternoCargo
                            WHERE
                                Compania_oidCompania = $idCompania
                                    AND Tercero_oidTercero_1aM = $id
                        ) cargos on cargos.Tercero_oidTercero_1aM = ST.oidTercero
                        LEFT JOIN
                        (
                            SELECT 
                                idActividadEconomica,
                                Tercero_oidTercero_1aM
                            FROM
                                asn_tercero
                                    LEFT JOIN
                                asn_terceroactividadeconomica ON Tercero_oidTercero_1aM = oidTercero
                                    LEFT JOIN
                                gen_actividadeconomica ON ActividadEconomica_oidActividadEconomica_1aM = oidActividadEconomica
                                    left join
                                $BDExterna.ActividadEconomica on BINARY txCodigoActividadEconomica = BINARY  codigoAlternoActividadEconomica
                            WHERE
                                Tercero_oidTercero_1aM = $id
                                group by Tercero_oidTercero_1aM
                        ) actividadEconomica on actividadEconomica.Tercero_oidTercero_1aM = ST.oidTercero
                        WHERE
                            ST.oidTercero = $id
                        GROUP BY ST.oidTercero");
                        
                    // COnvertimos la consulta en Array  
                    $consulta = get_object_vars($consulta[0]);
                    $documento = ($documentoAnterior != $consulta['documentoTercero'] && $documentoAnterior != '' ? $documentoAnterior : $consulta['documentoTercero']);
                
                    // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                    $tercero = DB::connection($BDExterna)->select(
                        'SELECT * 
                        FROM Tercero 
                        WHERE documentoTercero = "'.$documento.'"
                            AND tipoTercero NOT LIKE "%18%"');
                    if($tercero){
                        $tercerodet = get_object_vars($tercero[0]);
                    }
                    else{
                        $tercerodet = array('idTercero' => 0);
                    }
                    // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                    $estructura = $this->estructuraTabla(DB::connection($BDExterna),$BDExterna,'Tercero');
                    // echo($this->all('Tercero','', $consulta, $tercerodet,$estructura));
                    $resp = DB::connection($BDExterna)->insert($this->all('Tercero','', $consulta, $tercerodet,$estructura));
                }

            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        } 
       
    }

    public function actualizarSucursal($id)
    {   
        set_time_limit(0);
        DB::beginTransaction();
        try {   
                $companias = DB::select("SELECT
                Compania_oidCompania
                FROM asn_tercerosucursal TS
                left join asn_tercero on TS.Tercero_oidTercero_1aM = oidTercero
                left join asn_tercerocompania TC on TC.Tercero_oidTercero_1aM = oidTercero
                WHERE oidTerceroSucursal = $id");

                // Convertimos los id de compañías en una array para recorrerlos
                // $companias = explode(',', $companias[0]->txCompaniasTercero);
                foreach($companias as $reg => $valor)
                {
                    // Consultamos la compañía para obtener el nombre de la base de datos
                    $bdCompania = CompaniaModel::where('oidCompania','=',$valor->Compania_oidCompania)->get();
                    $BDExterna = $bdCompania[0]["txBaseDatosCompania"];
                    $idUsuario = Auth::id();
                
                    //consultamos el usuario creador homologado con saya para guardarlos en la db de saya
                    $userCreador = DB::select("SELECT
                    IL.id as terceroCreador
                    FROM seg_usuario
                    LEFT JOIN $BDExterna.SegLogin IL on txLoginUsuario = usuarioLogin
                    WHERE oidUsuario = $idUsuario");
                    $idCreador = ($userCreador[0]->terceroCreador ? $userCreador[0]->terceroCreador : '0');
                    
                    
                    //consultamos el usuario creador homologado con saya para guardarlos en la db de saya
                    $userModificador = DB::select("SELECT
                    IL.id as terceroCreador
                    FROM seg_usuario
                    LEFT JOIN $BDExterna.SegLogin IL on txLoginUsuario = usuarioLogin
                    WHERE oidUsuario = $idUsuario");   
                    $idModificador = ($userModificador[0]->terceroCreador ? $userModificador[0]->terceroCreador : '0');

                    // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
                    $scalia = DB::select("SELECT  IT.idTercero,
                        ST.daFechaCreacionTercero AS fechaCreacionTercero,
                        ST.daFechaCreacionTercero AS fechaAdicionarTercero,
                        ST.txCompaniasTercero,
                        ITI.idIdentificacion AS TipoIdentificacion_idIdentificacion,
                        ST.txDocumentoTercero AS documentoTercero,
                        TS.txCodigoTerceroSucursal AS codigoAlterno1Tercero,
                        TS.txCodigoBarrasTerceroSucursal AS codigoBarrasTercero,
                        ST.txPrimerNombreTercero AS nombreATercero,
                        ST.txSegundoNombreTercero AS nombreBTercero,
                        ST.txPrimerApellidoTercero AS apellidoATercero,
                        ST.txSegundoApellidoTercero AS apellidoBTercero,
                        ST.txNombreTercero AS nombre1Tercero,
                        TS.txNombreComercialTerceroSucursal AS nombre2Tercero,
                        TS.txDireccionTerceroSucursal AS direccionTercero,
                        TS.txDireccionTerceroSucursal AS direccionRutTercero,
                        ICTS.idCiudad AS Ciudad_idCiudad,
                        TS.txTelefono1TerceroSucursal AS telefono1Tercero,
                        ST.txMovilTercero AS movilTercero,
                        TS.txCorreoElectronicoTerceroSucursal AS correoElectronicoTercero,
                        ST.txEstadoTercero AS estadoTercero,
                        ST.imImagenTercero AS imagenTercero,
                        IF(IT.tipoTercero != '',IT.tipoTercero,
                        '*18*') AS tipoTercero,
                        ITVD.idTercero AS Tercero_idVendedor,
                        IFPV.idFormaPago AS FormaPago_idFormaPago,
                        IFPC.idFormaPago AS FormaPago_idFormaPagoCompra,
                        chPracticaRetencionTerceroTributario AS esAgenteRetenedorTercero,
                        lsRegimenVentaTerceroTributario AS regimenVentasTercero,
                        chAutoretenedorTerceroTributario AS esAutoretenedor,
                        idNaturalezaJuridica AS NaturalezaJuridica_idNaturalezaJuridica,
                        chAutoreenedorCREETerceroTributario AS esAutoretenedorCREETercero,
                        actividadEconomica.idActividadEconomica AS ActividadEconomica_idActividadEconomica,
                        chGranContribuyenteTerceroTributario AS esGranContribuyente,
                        idClasificacionRenta AS ClasificacionRenta_idClasificacionRenta,
                        chEntidadEstadoTerceroTributario AS esEntidadEstadoTercero,
                        chNoResponsableIVATerceroTributario AS noResponsableIVATercero,
                        idGrupoNomina AS GrupoNomina_idGrupoNomina,
                        inPersonasACargoTerceroLaboral AS personasACargoTercero,
                        inNumeroHijosTerceroLaboral AS numeroHijosTercero,
                        deIngresosFamiliares AS ingresoFamiliarTercero,
                        daFechaRetiroPension AS fechaRetiroPensionTercero,
                        chExtranjeroPension AS extranjeroNoPensionTercero,
                        chColombianoExterior AS colombianoResidenteExteriorTercero,
                        ITS.idTercero AS Tercero_idSalud,
                        ITP.idTercero AS Tercero_idPension,
                        ITC.idTercero AS Tercero_idCesantias,
                        ITCJ.idTercero AS Tercero_idCajaCompensacion,
                        lsSexoTerceroPersonal AS sexoTercero,
                        lsGrupoSanguineoTerceroPersonal AS grupoSanguineoTercero,
                        lsRHTerceroPersonal AS factorRHTercero,
                        daFechaExpedicionTerceroPersonal AS fechaExpedicionDocumentoTercero,
                        ICEX.idCiudad AS Ciudad_idLugarExpedicion,
                        daFechaNacimientoTerceroPersonal AS fechaNacimientoTercero,
                        ICNC.idCiudad AS Ciudad_idLugarNacimiento,
                        lsEstadoCivilTerceroPersonal AS estadoCivilTercero,
                        IF(IT.SegLogin_idAdicionar != 0,IT.SegLogin_idAdicionar,$idCreador) as SegLogin_idAdicionar,
                        $idModificador as SegLogin_idModificar
                        FROM
                        asn_tercero AS ST
                            LEFT JOIN
                        asn_tercerosucursal AS TS ON TS.Tercero_oidTercero_1aM = ST.oidTercero
                                LEFT JOIN
                        $BDExterna.Tercero AS IT ON BINARY IT.codigoAlterno1Tercero = BINARY TS.txCodigoTerceroSucursal and IT.tipoTercero like '%18%'
                            LEFT JOIN
                        asn_tercerocomercial AS TC ON TC.Tercero_oidTercero_1a1 = ST.oidTercero
                            LEFT JOIN
                        asn_tercerotributario AS TT ON TT.Tercero_oidTercero_1a1 = ST.oidTercero
                            LEFT JOIN
                        asn_tercerolaboral AS TL ON TL.Tercero_oidTercero_1a1 = ST.oidTercero
                            LEFT JOIN
                        asn_terceropersonal AS TP ON TP.Tercero_oidTercero_1aM = ST.oidTercero
                            LEFT JOIN
                        gen_formapago AS SFPV ON TC.FormaPago_oidVenta = SFPV.oidFormaPago
                            LEFT JOIN
                        $BDExterna.FormaPago AS IFPV ON BINARY IFPV.codigoAlternoFormaPago = BINARY SFPV.txCodigoFormaPago
                            LEFT JOIN
                        gen_formapago AS SFPC ON TC.FormaPago_oidCompra = SFPC.oidFormaPago
                            LEFT JOIN
                        $BDExterna.FormaPago AS IFPC ON BINARY IFPC.codigoAlternoFormaPago = BINARY SFPC.txCodigoFormaPago
                            LEFT JOIN
                        gen_tipoidentificacion AS STI ON ST.TipoIdentificacion_oidTercero = STI.oidTipoIdentificacion
                            LEFT JOIN
                        $BDExterna.TipoIdentificacion AS ITI ON BINARY ITI.codigoIdentificacion = BINARY STI.txCodigoTipoIdentificacion
                            LEFT JOIN
                        gen_ciudad AS SC ON ST.Ciudad_oidTercero = SC.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS IC ON BINARY IC.codigoAlternoCiudad = BINARY SC.txCodigoCiudad
                            LEFT JOIN
                        gen_naturalezajuridica AS SNJ ON TT.NaturalezaJuridica_oidTerceroTributario = SNJ.oidNaturalezaJuridica
                            LEFT JOIN
                        $BDExterna.NaturalezaJuridica AS INJ ON BINARY INJ.codigoAlternoNaturalezaJuridica = BINARY SNJ.txCodigoNaturalezaJuridica
                            LEFT JOIN
                        gen_clasificacionrenta AS SCR ON TT.ClasificacionRenta_oidTerceroTributario = SCR.oidClasificacionRenta
                            LEFT JOIN
                        $BDExterna.ClasificacionRenta AS ICR ON BINARY ICR.codigoAlternoClasificacionRenta = BINARY SCR.txCodigoClasificacionRenta
                            LEFT JOIN
                        gen_gruponomina AS SGN ON TL.GrupoNomina_oidTerceroLaboral = SGN.oidGrupoNomina
                            LEFT JOIN
                        $BDExterna.GrupoNomina AS IGN ON BINARY IGN.codigoAlternoGrupoNomina = BINARY SGN.txCodigoGrupoNomina
                            LEFT JOIN
                        asn_tercero AS STVD ON TC.Tercero_oidVendedor = STVD.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITVD ON BINARY ITVD.documentoTercero = BINARY STVD.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STS ON TL.Tercero_oidSalud = STS.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITS ON BINARY ITS.documentoTercero = BINARY STS.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STP ON TL.Tercero_oidPension = STP.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITP ON BINARY ITP.documentoTercero = BINARY STP.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STC ON TL.Tercero_oidCesantias = STC.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITC ON BINARY ITC.documentoTercero = BINARY STC.txDocumentoTercero
                            LEFT JOIN
                        asn_tercero AS STCJ ON TL.Tercero_oidCaja = STCJ.oidTercero
                            LEFT JOIN
                        $BDExterna.Tercero AS ITCJ ON BINARY ITCJ.documentoTercero = BINARY STCJ.txDocumentoTercero
                            LEFT JOIN
                        gen_ciudad AS SCEX ON TP.Ciudad_oidExpedicion = SCEX.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS ICEX ON BINARY ICEX.codigoAlternoCiudad = BINARY SCEX.txCodigoCiudad
                            LEFT JOIN
                        gen_ciudad AS SCNC ON TP.Ciudad_oidNacimiento = SCNC.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS ICNC ON BINARY ICNC.codigoAlternoCiudad = BINARY SCNC.txCodigoCiudad
                            LEFT JOIN
                        gen_ciudad AS SCS ON TS.Ciudad_oidTerceroSucursal = SCS.oidCiudad
                            LEFT JOIN
                        $BDExterna.Ciudad AS ICTS ON BINARY ICTS.codigoAlternoCiudad = BINARY SCS.txCodigoCiudad
                            LEFT JOIN
                        (
                            SELECT 
                                idActividadEconomica,
                                Tercero_oidTercero_1aM
                            FROM
                                asn_tercero
                                    LEFT JOIN
                                asn_terceroactividadeconomica ON Tercero_oidTercero_1aM = oidTercero
                                    LEFT JOIN
                                gen_actividadeconomica ON ActividadEconomica_oidActividadEconomica_1aM = oidActividadEconomica
                                    left join
                                $BDExterna.ActividadEconomica on BINARY txCodigoActividadEconomica = BINARY  codigoAlternoActividadEconomica
                            WHERE
                                Tercero_oidTercero_1aM = $id
                                group by Tercero_oidTercero_1aM
                        ) actividadEconomica on actividadEconomica.Tercero_oidTercero_1aM = ST.oidTercero
                            WHERE
                            TS.oidTerceroSucursal = $id");

                        // COnvertimos la consulta en Array  
                        $consulta = get_object_vars($scalia[0]);
                                
                    foreach($scalia as $pos => $val){
                
                        // COnvertimos la consulta en Array  
                        $consulta = get_object_vars($scalia[$pos]);

                        // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                        $tercero = DB::connection($BDExterna)->select(
                            'SELECT * 
                            FROM Tercero 
                            WHERE documentoTercero = "'.$consulta['documentoTercero'].'"
                                AND tipoTercero  LIKE "%18%" AND codigoAlterno1Tercero ="'.$consulta['codigoAlterno1Tercero'].'"');

                        if($tercero){
                            $tercerodet = get_object_vars($tercero[0]);
                        }
                        else{
                            $tercerodet = array('idTercero' => 0);
                        }
                        // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                        $estructura = $this->estructuraTabla(DB::connection($BDExterna),$BDExterna,'Tercero');
                        $resp = DB::connection($BDExterna)->insert($this->all('Tercero','', $consulta, $tercerodet,$estructura));
                    }
                }
            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }
            
    }

    public function actualizarTerceroBanco($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
                $companias = DB::select("SELECT
                Compania_oidCompania
                FROM asn_tercero
                LEFT JOIN 
                    asn_tercerocompania on Tercero_oidTercero_1aM = oidTercero
                WHERE oidTercero = $id");
                // Convertimos los id de compañías en una array para recorrerlos
                foreach($companias as $reg => $valor)
                {
                    // Consultamos la compañía para obtener el nombre de la base de datos
                    $bdCompania = CompaniaModel::where('oidCompania','=',$valor->Compania_oidCompania)->get();
                    $BDExterna = $bdCompania[0]["txBaseDatosCompania"];

                    // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
                    $scalia = DB::select("SELECT 
                        T.txCompaniasTercero,
                        T.txDocumentoTercero AS documentoTercero,
                        TIB.idTercero AS Tercero_idBanco,
                        TBI.oidTercero as bancoSacalia,
                        txNombreCuentaTerceroBanco AS nombreCuentaTerceroBanco,
                        txTitularTerceroBanco AS titularCuentaTerceroBanco,
                        lsTipoTerceroBanco AS tipoCuentaTerceroBanco,
                        txNumeroCuentaTerceroBanco AS numeroCuentaTerceroBanco,
                        TC.CuentaContable_idCuentaContable,
                        TC.CuentaContable_idCuentaContableNIIF,
                        TC.longitudTerceroBanco,
                        TC.contabilidadTerceroBanco,
                        TC.numeroInicialTerceroBanco,
                        TC.numeroFinalTerceroBanco,
                        TC.numeroActualTerceroBanco
                        FROM
                            asn_tercero as T
                                LEFT JOIN
                            asn_tercerobanco AS TB ON TB.Tercero_oidTercero_1aM = T.oidTercero
                                LEFT JOIN
                            asn_tercero AS TBI ON TB.Tercero_oidTerceroBanco = TBI.oidTercero
                                LEFT JOIN
                            $BDExterna.Tercero AS TIB ON BINARY TIB.documentoTercero = BINARY TBI.txDocumentoTercero
                                LEFT JOIN 
                            $BDExterna.TerceroBanco TC on BINARY TC.numeroCuentaTerceroBanco = BINARY TB.txNumeroCuentaTerceroBanco
                        WHERE
                            T.oidTercero = $id;");
                    
                    foreach($scalia as $pos => $val)
                    {
                        // COnvertimos la consulta en Array  
                        $consulta = get_object_vars($scalia[$pos]);
                    
                        // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                        $tercero = DB::connection($BDExterna)->select(
                            'SELECT idTercero as idPadre, TC.*
                            FROM Tercero
                            LEFT JOIN TerceroBanco as TC on Tercero_idTercero = idTercero
                            WHERE documentoTercero = "'.$consulta['documentoTercero'].'" AND numeroCuentaTerceroBanco = "'.$consulta['numeroCuentaTerceroBanco'].'" AND tipoTercero NOT LIKE "%18%"');
                    
                        $sw = true;
                        if($tercero){
                            $tercerodet = get_object_vars($tercero[0]);
                        }
                        else
                        {
                            $tercero = DB::connection($BDExterna)->select(
                                'SELECT idTercero as idPadre,
                                    0 as idTerceroBanco,
                                    TC.Tercero_idTercero,
                                    TC.Tercero_idBanco,
                                    TC.nombreCuentaTerceroBanco,
                                    TC.tipoCuentaTerceroBanco,
                                    TC.numeroCuentaTerceroBanco,
                                    TC.titularCuentaTerceroBanco,
                                    TC.CuentaContable_idCuentaContable,
                                    TC.CuentaContable_idCuentaContableNIIF,
                                    TC.longitudTerceroBanco,
                                    TC.contabilidadTerceroBanco,
                                    TC.numeroInicialTerceroBanco,
                                    TC.numeroFinalTerceroBanco,
                                    TC.numeroActualTerceroBanco
                                FROM Tercero
                                LEFT JOIN TerceroBanco as TC on Tercero_idTercero = idTercero
                                WHERE documentoTercero = "'.$consulta['documentoTercero'].'" AND tipoTercero NOT LIKE "%18%"');
                            if($tercero)
                                $tercerodet = get_object_vars($tercero[0]);
                            else
                                $sw = false;
                            }
                            if($sw)
                            {
                                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                                $estructura = $this->estructuraTabla(DB::connection($BDExterna),$BDExterna,'TerceroBanco');
                                $resp = DB::connection($BDExterna)->insert($this->all('TerceroBanco','Tercero_idTercero', $consulta, $tercerodet,$estructura));
                                
                    }
                        
                }
            }
        DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }

    }

    public function actualizarTerceroContacto($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
                $companias = DB::select("SELECT
                Compania_oidCompania
                FROM asn_tercero
                LEFT JOIN 
                    asn_tercerocompania on Tercero_oidTercero_1aM = oidTercero
                WHERE oidTercero = $id");
                // Convertimos los id de compañías en una array para recorrerlos
                foreach($companias as $reg => $valor)
                {
                    // Consultamos la compañía para obtener el nombre de la base de datos
                    $bdCompania = CompaniaModel::where('oidCompania','=',$valor->Compania_oidCompania)->get();
                    $BDExterna = $bdCompania[0]["txBaseDatosCompania"];
                    // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
                    $scalia = DB::select("SELECT 
                        txDocumentoTercero as documentoTercero,
                        txNombreTerceroContacto AS nombreTerceroContacto,
                        txCargoTerceroContacto AS cargoTerceroContacto,
                        txCorreoTerceroContacto AS correoElectronicoTerceroContacto,
                        txMovilTerceroContacto AS movilTerceroContacto,
                        'contacto' AS tipoTerceroContacto,
                        CONCAT('*',txCodigoTipoContactoTercero,'*') AS tipoCorreoTerceroContacto
                    FROM
                        asn_tercero AS T
                            LEFT JOIN
                        asn_tercerocontacto AS STC ON STC.Tercero_oidTercero_1aM = T.oidTercero
                            LEFT JOIN
                        gen_tipocontactotercero TC on STC.TipoContactoTercero_oidContactoTercero = TC.oidTipoContactoTercero 
                            LEFT JOIN
                        $BDExterna.Tercero AS IT ON BINARY IT.documentoTercero = BINARY T.txDocumentoTercero
                            LEFT JOIN
                        $BDExterna.TerceroContacto ITC ON BINARY ITC.nombreTerceroContacto = BINARY STC.txNombreTerceroContacto
                    WHERE
                        T.oidTercero = $id
                    group by txNombreTerceroContacto");

                    foreach($scalia as $pos => $val)
                    {
                        // COnvertimos la consulta en Array  
                        $consulta = get_object_vars($scalia[$pos]);
                    
                        // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                        $tercero = DB::connection($BDExterna)->select(
                            'SELECT idTercero as idPadre, TC.*
                            FROM Tercero
                            LEFT JOIN TerceroContacto as TC on Tercero_idTercero = idTercero
                            WHERE documentoTercero = "'.$consulta['documentoTercero'].'" AND nombreTerceroContacto = "'.$consulta['nombreTerceroContacto'].'" AND tipoTercero NOT LIKE "%18%"');
                    
                        $sw = true;
                        if($tercero){
                            $tercerodet = get_object_vars($tercero[0]);
                        }
                        else
                        {
                            $tercero = DB::connection($BDExterna)->select(
                                'SELECT 
                                idTercero AS idPadre,
                                0 AS idTerceroContacto,
                                Tercero_idTercero,
                                tipoTerceroContacto,
                                nombreTerceroContacto,
                                cargoTerceroContacto,
                                telefonoTerceroContacto,
                                movilTerceroContacto,
                                correoElectronicoTerceroContacto,
                                tipoCorreoTerceroContacto,
                                parentescoTerceroContacto,
                                fechaNacimientoTerceroContacto,
                                nivelEducativoTerceroContacto,
                                ocupacionTerceroContacto,
                                direccionTerceroContacto,
                                observacionTerceroContacto
                            FROM
                                Tercero
                                    LEFT JOIN
                                TerceroContacto AS TC ON Tercero_idTercero = idTercero
                            WHERE
                                documentoTercero = "'.$consulta['documentoTercero'].'"
                                    AND tipoTercero NOT LIKE "%18%"');
                            if($tercero)
                                $tercerodet = get_object_vars($tercero[0]);
                            else
                                $sw = false;
                            }
                            if($sw)
                            {
                                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                                $estructura = $this->estructuraTabla(DB::connection($BDExterna),$BDExterna,'TerceroContacto');
                                $resp = DB::connection($BDExterna)->insert($this->all('TerceroContacto','Tercero_idTercero', $consulta, $tercerodet,$estructura));
                                
                    }
                        
                }
            }
        DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }

    }

    public function actualizarTerceroReferencia($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
                $companias = DB::select("SELECT
                Compania_oidCompania
                FROM asn_tercero
                LEFT JOIN 
                    asn_tercerocompania on Tercero_oidTercero_1aM = oidTercero
                WHERE oidTercero = $id");
                // Convertimos los id de compañías en una array para recorrerlos
                foreach($companias as $reg => $valor)
                {
                    // Consultamos la compañía para obtener el nombre de la base de datos
                    $bdCompania = CompaniaModel::where('oidCompania','=',$valor->Compania_oidCompania)->get();
                    $BDExterna = $bdCompania[0]["txBaseDatosCompania"];
                    // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
                    $scalia = DB::select("SELECT 
                        txDocumentoTercero AS documentoTercero,
                        txTipoTerceroReferencia as tipoTerceroContacto, 
                        txNombreTerceroReferencia as nombreTerceroContacto,
                        txParentescoTerceroReferencia as parentescoTerceroContacto,
                        daFechaNacimientoTerceroReferencia as fechaNacimientoTerceroContacto,
                        txNivelEducativoTerceroReferencia as nivelEducativoTerceroContacto,
                        txOcupacionTerceroReferencia as ocupacionTerceroContacto,
                        txTelefonoTerceroReferencia as telefonoTerceroContacto,
                        txDireccionTerceroReferencia as direccionTerceroContacto,
                        txObservacionTerceroReferencia as direccionTerceroContacto
                    FROM
                        asn_tercero AS T
                            LEFT JOIN
                        asn_terceroreferencia AS STR ON STR.Tercero_oidTercero_1aM = T.oidTercero
                            LEFT JOIN
                            $BDExterna.Tercero AS IT ON BINARY IT.documentoTercero = BINARY T.txDocumentoTercero
                            LEFT JOIN
                            $BDExterna.TerceroContacto ITC ON BINARY ITC.nombreTerceroContacto = BINARY STR.txNombreTerceroReferencia
                    WHERE
                        T.oidTercero = $id
                    GROUP BY txNombreTerceroReferencia");

                    foreach($scalia as $pos => $val)
                    {
                        // COnvertimos la consulta en Array  
                        $consulta = get_object_vars($scalia[$pos]);
                    
                        // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                        $tercero = DB::connection($BDExterna)->select(
                            'SELECT idTercero as idPadre, TC.*
                            FROM Tercero
                            LEFT JOIN TerceroContacto as TC on Tercero_idTercero = idTercero
                            WHERE documentoTercero = "'.$consulta['documentoTercero'].'" AND nombreTerceroContacto = "'.$consulta['nombreTerceroContacto'].'" AND tipoTercero NOT LIKE "%18%"');
                    
                        $sw = true;
                        if($tercero){
                            $tercerodet = get_object_vars($tercero[0]);
                        }
                        else
                        {
                            $tercero = DB::connection($BDExterna)->select(
                                'SELECT 
                                idTercero AS idPadre,
                                0 AS idTerceroContacto,
                                Tercero_idTercero,
                                tipoTerceroContacto,
                                nombreTerceroContacto,
                                cargoTerceroContacto,
                                telefonoTerceroContacto,
                                movilTerceroContacto,
                                correoElectronicoTerceroContacto,
                                tipoCorreoTerceroContacto,
                                parentescoTerceroContacto,
                                fechaNacimientoTerceroContacto,
                                nivelEducativoTerceroContacto,
                                ocupacionTerceroContacto,
                                direccionTerceroContacto,
                                observacionTerceroContacto
                            FROM
                                Tercero
                                    LEFT JOIN
                                TerceroContacto AS TC ON Tercero_idTercero = idTercero
                            WHERE
                                documentoTercero = "'.$consulta['documentoTercero'].'"
                                    AND tipoTercero NOT LIKE "%18%"');
                            if($tercero)
                                $tercerodet = get_object_vars($tercero[0]);
                            else
                                $sw = false;
                            }
                            if($sw)
                            {
                                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                                $estructura = $this->estructuraTabla(DB::connection($BDExterna),$BDExterna,'TerceroContacto');
                                $resp = DB::connection($BDExterna)->insert($this->all('TerceroContacto','Tercero_idTercero', $consulta, $tercerodet,$estructura));
                                
                            }
                        
                    }
            }
        DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }

    }
    //maestros
    public function actualizarCostoAdicional($id)
    {
        set_time_limit(0);
        // DB::beginTransaction();
        // try {
            
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_costoadicional')->select(
                'txCodigoCostoAdicional as codigoAlternoCostoAdicional',
                'txNombreCostoAdicional as nombreCostoAdicional',
                'lsTipoCostoAdicional as tipoCostoAdicional',
                'deValorCostoAdicional as valorCostoAdicional'
                )
                ->where('oidCostoAdicional','=',$id)
                ->get();

            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"]
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $costoAdicional = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM CostoAdicional
                    WHERE codigoAlternoCostoAdicional = "'.$consulta["codigoAlternoCostoAdicional"].'"');  
                
                if($costoAdicional)
                    $costoAdicionaldet = get_object_vars($costoAdicional[0]);
                else
                    $costoAdicionaldet = array('idCostoAdicional' => 0);
                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'CostoAdicional');
                $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('CostoAdicional','', $consulta, $costoAdicionaldet,$estructura));
                
            }
        //     DB::commit();
        // } catch (\Exception $error) {
        //     DB::rollback();
        //     return $error->getMessage();
        // }
    }

    public function actualizarCostoIndirecto($id)
    {
        set_time_limit(0);
        // DB::beginTransaction();
        // try {
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_costoindirecto')->select(
                'txCodigoCostoIndirecto as codigoAlternoCostoIndirecto',
                'txNombreCostoIndirecto as nombreCostoIndirecto',
                'lsTipoCostoIndirecto as tipoCostoIndirecto',
                'lsBaseCostoIndirecto as costoBaseCostoIndirecto',
                'deValorCostoIndirecto as valorCostoIndirecto'
                )
                ->where('oidCostoIndirecto','=',$id)
                ->get();
            
            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"]
                
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $costoIndirecto = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM CostoIndirecto
                    WHERE codigoAlternoCostoIndirecto = "'.$consulta["codigoAlternoCostoIndirecto"].'"');  
                
                if($costoIndirecto)
                    $costoIndirectodet = get_object_vars($costoIndirecto[0]);
                else
                    $costoIndirectodet = array('idCostoIndirecto' => 0);
                
                     // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'CostoIndirecto');
                $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('CostoIndirecto','', $consulta, $costoIndirectodet,$estructura));
                
            }
        //     DB::commit();
        // } catch (\Exception $error) {
        //     DB::rollback();
        //     return $error->getMessage();
        // }
    }

    public function actualizarCentroProduccion($id)
    {
        set_time_limit(0);
        // DB::beginTransaction();
        // try {
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_centroproduccion')->select(
            'txCodigoCentroProduccion as codigoAlternoCentroProduccion',
            'txNombreCentroProduccion as nombreCentroProduccion',
            'Producto_oidConceptoServicio as Producto_idConceptoServicio',
            'deValorMinutoCentroProduccion as valorMinutoCentroProduccion',
            'inOrdenCentroProduccion as ordenCentroProduccion',
            'lsClasificacionCentroProduccion as clasificacionCentroProduccion',
            'chAfectaInventarioPTCentroProduccion as afectaInventarioProductoCentroProduccion',
            'chAfectaInventarioMPCentroProduccion as afectaInventarioMaterialCentroProduccion',
            'chPermiteToleranciaCentroProduccion as permitirToleranciaCentroProduccion'
            )
            ->where('oidCentroProduccion','=',$id)
            ->get();
            
            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"]
                
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $centroProduccion = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM CentroProduccion
                    WHERE codigoAlternoCentroProduccion = "'.$consulta["codigoAlternoCentroProduccion"].'"');  
                
                if($centroProduccion)
                    $centroProducciondet = get_object_vars($centroProduccion[0]);
                else
                    $centroProducciondet = array('idCentroProduccion' => 0);

                                     // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'CentroProduccion');
                $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('CentroProduccion','', $consulta, $centroProducciondet,$estructura));       
            }
        //     DB::commit();
        // } catch (\Exception $error) {
        //     DB::rollback();
        //     return $error->getMessage();
        // }
    }

    public function actualizarPartidaArancelaria($partida , $companias){
        set_time_limit(0);
        // Consultamos la compañía para obtener el nombre de la base de datos
        $tabla = "PosicionArancelaria";
        $codigo = $partida->getCodigoInciso();
        $ahora = Carbon::now();
        foreach($companias as $compania){
            $baseDeDatos = $compania->txBaseDatosCompania;
            $usuarioSaya = SayaFichaFactory::usuarioSaya($baseDeDatos);
            //se configura la conexion a la base de datos externa.
            $conexion = DB::connection($baseDeDatos);
            $posicionArancelaria = $conexion->select("SELECT * FROM {$tabla} WHERE codigoAlternoPosicionArancelaria = '{$codigo}'");
            if( $posicionArancelaria ){
                $id = $posicionArancelaria[0]->idPosicionArancelaria;
                $conexion->update("UPDATE {$tabla} SET codigoAlternoPosicionArancelaria = ?, nombrePosicionArancelaria = ?, SegLogin_idUsuarioModifica=?,fechaModificacionPosicionArancelaria=?  WHERE idPosicionArancelaria = ?" , [
                    $codigo , $partida->txNombrePartidaArancelaria , $usuarioSaya , $ahora , $id
                ]);
            }else{
                $conexion->insert("INSERT INTO {$tabla} (codigoAlternoPosicionArancelaria , nombrePosicionArancelaria, porcentajePosicionArancelaria,SegLogin_idUsuarioCrea,SegLogin_idUsuarioModifica,fechaModificacionPosicionArancelaria , fechaElaboracionPosicionArancelaria) VALUES (?,?,?,?,?,?,?)" , [
                    $codigo , $partida->txNombrePartidaArancelaria, 0 , $usuarioSaya, $usuarioSaya, $ahora, $ahora
                ]);
            }
        }
    }
    
    //maestros asociado
    public function actualizarFormaPago($id)
    {
        set_time_limit(0);
        // DB::beginTransaction();
        // try {

            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_formapago')->select(
            'txCodigoFormaPago as codigoAlternoFormaPago',
            'txNombreFormaPago as nombreFormaPago',
            'txVencimientoFormaPago as diasFormaPago',
            'chAfectaCFormaPago as afectaCarteraFormaPago',
            'chAfectaCCFormaPago as afectaCarteraCreditoFormaPago',
            'lsTipoFormaPago as tipoFormaPago'
            )
            ->where('oidFormaPago','=',$id)
            ->get();

            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();
            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $formaPago = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM FormaPago 
                    WHERE codigoAlternoFormaPago = "'.$consulta["codigoAlternoFormaPago"].'"');  
                
                if($formaPago)
                    $formaPagodet = get_object_vars($formaPago[0]);
                else
                    $formaPagodet = array('idFormaPago' => 0);

                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'FormaPago');
                $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('FormaPago','', $consulta, $formaPagodet,$estructura));
                
            } 

        //     DB::commit();
        // } catch (\Exception $error) {
        //     DB::rollback();
        //     return $error->getMessage();
        // }
    }

    public function actualizarNaturalezaJuridica($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_naturalezajuridica')->select(
            'txCodigoNaturalezaJuridica as codigoAlternoNaturalezaJuridica',
            'txNombreNaturalezaJuridica as nombreNaturalezaJuridica'
            )
            ->where('oidNaturalezaJuridica','=',$id)
            ->get();

            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"] 
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $naturalezaJuri = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM NaturalezaJuridica 
                    WHERE codigoAlternoNaturalezaJuridica = "'.$consulta["codigoAlternoNaturalezaJuridica"].'"');  
                
                if($naturalezaJuri)
                    $naturalezaJuridet = get_object_vars($naturalezaJuri[0]);
                else
                    $naturalezaJuridet = array('idNaturalezaJuridica' => 0);

                      // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                      $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'NaturalezaJuridica');
                      $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('NaturalezaJuridica','', $consulta, $naturalezaJuridet,$estructura));
                
            } 
            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }
    }

    public function actualizarTipoAportanteNomina($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_tipoaportantenomina')->select(
                'txCodigoTipoAportanteNomina as codigoAlternoTipoAportanteNomina',
                'txNombreTipoAportanteNomina as nombreTipoAportanteNomina'
                )
                ->where('oidTipoAportanteNomina','=',$id)
                ->get();

            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"] 
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $tipoAportante = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM TipoAportanteNomina 
                    WHERE codigoAlternoTipoAportanteNomina = "'.$consulta["codigoAlternoTipoAportanteNomina"].'"');  
                
                if($tipoAportante)
                    $tipoAportantedet = get_object_vars($tipoAportante[0]);
                else
                    $tipoAportantedet = array('idTipoAportanteNomina' => 0);

                      // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                      $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'TipoAportanteNomina');
                      $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('TipoAportanteNomina','', $consulta, $tipoAportantedet,$estructura));
                
            } 
            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }
    }

    public function actualizarTipoIdentificacion($id)
    {
        set_time_limit(0);
        DB::beginTransaction();
        try {
            // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
            $consulta = DB::table('gen_tipoidentificacion')->select(
                'txCodigoTipoIdentificacion as codigoIdentificacion',
                'txNombreTipoIdentificacion as nombreIdentificacion',
                'txCodigoDIANTipoIdentificacion as codigoDianIdentificacion',
                'txCodigoNominaTipoIdentificacion as codigoNominaIdentificacion',
                'lsTipoNombreTipoIdentificacion as regimenIdentificacion',
                'chDocumentoAutomaticoTipoIdentificacion as autoNumeroIdentificacion',
                'txDocumentoPrefijoTipoIdentificacion as prefijoIdentificacion',
                'inDocumentoLongitudTipoIdentificacion as longitudIdentificacion',
                'chDocumentoVerificacionTipoIdentificacion as calculardvIdentificacion',
                'chReportarUbicacionMediosTipoIdentificacion as reportarIdentificacion'
                )
                ->where('oidTipoIdentificacion','=',$id)
                ->get();
 
            // Consultamos la compañía para obtener el nombre de la base de datos
            $bdCompania = CompaniaModel::select('oidCompania','txBaseDatosCompania')->get();

            // COnvertimos la consulta en Array  
            $consulta = get_object_vars($consulta[0]);
        
            foreach($bdCompania as $reg => $valor)
            {
                
                // $bdCompania[0]["txBaseDatosCompania"] 
                // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
                $tipoIdentificacion = DB::connection($valor->txBaseDatosCompania)->select(
                    'SELECT * 
                    FROM TipoIdentificacion 
                    WHERE codigoIdentificacion = "'.$consulta["codigoIdentificacion"].'"');  
               
                if($tipoIdentificacion)
                    $tipoIdentificaciondet = get_object_vars($tipoIdentificacion[0]);
                else
                    $tipoIdentificaciondet = array('idIdentificacion' => 0);

                // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
                $estructura = $this->estructuraTabla(DB::connection($valor->txBaseDatosCompania),$valor->txBaseDatosCompania,'TipoIdentificacion');
                $resp = DB::connection($valor->txBaseDatosCompania)->insert($this->all('TipoIdentificacion','', $consulta, $tipoIdentificaciondet,$estructura));
                
            } 
            DB::commit();
        } catch (\Exception $error) {
            DB::rollback();
            return $error->getMessage();
        }
    }

    public function actualizarPais($id){
        set_time_limit(0);
        // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
        $scalia = DB::table('gen_pais')->select(
        'txCodigoDIANPais as codigoAlternoPais',
        'txCodigoEANPais as codigoEANPais',
        'txCodigoISOPais as codigoISOPais',
        'txNombrePais as nombrePais'
        )
        ->where('oidPais','=',$id)
        ->get();
        $consulta = get_object_vars($scalia[0]);
        // Consultamos la compañía para obtener el nombre de la base de datos
        $companias = CompaniaModel::select('txBaseDatosCompania')->get();
        foreach($companias as $compania){
            $baseDeDatos = $compania->txBaseDatosCompania;
            //se configura la conexion a la base de datos externa.
            $pais = DB::connection($baseDeDatos)
            ->select('SELECT * FROM Pais WHERE codigoAlternoPais = "'.$consulta["codigoAlternoPais"].'"');
            $pais = $pais? get_object_vars($pais[0]) : array('idPais' => 0);
            // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
            $estructura = $this->estructuraTabla(DB::connection($baseDeDatos),$baseDeDatos,'Pais');
            DB::connection($baseDeDatos)->insert($this->all('Pais','', $consulta, $pais, $estructura));
        }
    }

    public function actualizarDepartamento($id){
        $baseDatosExterna = 'Iblu';
        set_time_limit(0);
        // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
        $scalia = DB::select("SELECT 
        ID.idDepartamento AS idDepartamento,
        IP.idPais AS Pais_idPais,
        txCodigoDIANPais AS codigoAlternoPais,
        txCodigoDepartamento AS codigoAlternoDepartamento,
        txNombreDepartamento AS nombreDepartamento
        FROM
            gen_departamento AS D
            LEFT JOIN 
            gen_pais AS SP on Pais_oidDepartamento = SP.oidPais
            LEFT JOIN 
            $baseDatosExterna.Departamento AS ID on ID.codigoAlternoDepartamento = D.txCodigoDepartamento
            LEFT JOIN 
            $baseDatosExterna.Pais AS IP on IP.codigoAlternoPais = SP.txCodigoDIANPais
        WHERE
            oidDepartamento = $id
        ");
 
        $consulta = get_object_vars($scalia[0]);
        // dd($consulta);
        // Consultamos la compañía para obtener el nombre de la base de datos
        $companias = CompaniaModel::select('txBaseDatosCompania')->get();
        foreach($companias as $compania){
            $baseDeDatos = $compania->txBaseDatosCompania;

              // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
              $departamento = DB::connection($baseDeDatos)
                ->select('SELECT *
                FROM Iblu.Departamento 
                WHERE codigoAlternoDepartamento = "'.$consulta['codigoAlternoDepartamento'].'"');

            if($departamento)
                $departamentoDet = get_object_vars($departamento[0]);
            else
                $departamentoDet = array('idDepartamento' => 0);
              
            // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
            $estructura = $this->estructuraTabla(DB::connection($baseDeDatos),$baseDeDatos,'Departamento');
            $resp = DB::connection($baseDeDatos)->insert($this->all('Departamento','', $consulta, $departamentoDet,$estructura));
            
        }
    }

    public function actualizarCiudad($id){
        $baseDatosExterna = 'Iblu';
        set_time_limit(0);
        // consultamos los datos de las tablas de SCALIA con los campos requeridos para crear o actualizar en la BD de SAYA
        $scalia = DB::select("SELECT 
        ID.idDepartamento AS Departamento_idDepartamento,
        txCodigoCiudad AS codigoAlternoCiudad,
        txNombreCiudad AS nombreCiudad
        FROM
            gen_ciudad AS C
            LEFT JOIN 
            gen_departamento AS D on C.Departamento_oidDepartamento_1aM = D.oidDepartamento
            LEFT JOIN 
            $baseDatosExterna.Departamento AS ID on ID.codigoAlternoDepartamento = D.txCodigoDepartamento
            LEFT JOIN 
            $baseDatosExterna.Ciudad AS IC on IC.codigoAlternoCiudad = C.txCodigoCiudad
        WHERE
            oidCiudad = $id");
 
        $consulta = get_object_vars($scalia[0]);
        // dd($consulta);
        // Consultamos la compañía para obtener el nombre de la base de datos
        $companias = CompaniaModel::select('txBaseDatosCompania')->get();
        foreach($companias as $compania){
            $baseDeDatos = $compania->txBaseDatosCompania;

              // consutlamos los datos en la BD de SAYA (segun la BD de la conexion)
              $ciudad = DB::connection($baseDeDatos)
                ->select('SELECT *
                FROM Iblu.Ciudad 
                WHERE codigoAlternoCiudad = "'.$consulta['codigoAlternoCiudad'].'"');

            if($ciudad)
                $ciudadDet = get_object_vars($ciudad[0]);
            else
                $ciudadDet = array('idCiudad' => 0);
              
            // llenamos automáticamente todos los campos del registro en una instruccion INSERT O UPDATE para SAYA
            $estructura = $this->estructuraTabla(DB::connection($baseDeDatos),$baseDeDatos,'Ciudad');
            $resp = DB::connection($baseDeDatos)->insert($this->all('Ciudad','', $consulta, $ciudadDet,$estructura));
            
        }
    }

    public function sayaActualizarDinamicas(GrupoProductoModel $grupoProducto,$codigoAnterior,Collection $companias){
        $tablaSaya = ClasificacionProductoGrupoModel::getTableNameDinamic($grupoProducto->oidGrupoProducto)->txRelacionClasificacionProductoGrupo;
        $idConcatenado = "id".$tablaSaya;
        $codigo = $codigoAnterior ? $codigoAnterior : $grupoProducto->txCodigoGrupoProducto;
        $companias->each(function($compania) use ($codigo,$idConcatenado,$tablaSaya,$grupoProducto){
            $baseDeDatos = $compania->txBaseDatosCompania;
            $conexion = DB::connection($baseDeDatos);
            $saya = $conexion->select("SELECT id{$tablaSaya} FROM {$tablaSaya} WHERE codigoAlterno{$tablaSaya} = '$codigo'");
            $idSaya = $saya ? $saya[0]->$idConcatenado : null;
            if(!$idSaya){
                $conexion->insert("INSERT INTO {$tablaSaya} (codigoAlterno{$tablaSaya},nombre{$tablaSaya}) VALUES (?,?)",
                [$grupoProducto->txCodigoGrupoProducto,$grupoProducto->txNombreGrupoProducto]);
            }else{
                $conexion->update("UPDATE {$tablaSaya} SET codigoAlterno{$tablaSaya} = ?,nombre{$tablaSaya} = ? WHERE id{$tablaSaya} = ?",
                [$grupoProducto->txCodigoGrupoProducto,$grupoProducto->txNombreGrupoProducto,$idSaya]);
            }
        });
    }
}

 
