<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\General\Entities\ComponenteCostoConceptoModel;

class ComponenteCostoConceptoSayaModel extends Model
{
    protected $table = 'ComponenteCostoDetalle';
    protected $primaryKey = 'idComponenteCostoDetalle';
    public $timestamps = false;

    public static function fromComponenteCostoScalia(ComponenteCostoConceptoModel $concepto , ComponenteCostoSayaModel $componente)
    {
        $conceptoSaya = new self();
        //si el conceptosaya trae un id o no, se establece en el concepto de saya
        $conceptoSaya->idComponenteCostoDetalle = $concepto->oidSayaComponenteCostoConcepto;
        $conceptoSaya->exists = $conceptoSaya->idComponenteCostoDetalle ? true :false;
        $conceptoSaya->ComponenteCosto_idComponenteCosto = $componente->idComponenteCosto;
        $conceptoSaya->codigoAlternoComponenteCostoDetalle = $concepto->txCodigoComponenteCostoConcepto;
        $conceptoSaya->nombreLargoComponenteCostoDetalle = $concepto->txNombreComponenteCostoConcepto;
        $conceptoSaya->nombreCortoComponenteCostoDetalle = $concepto->txNombreComponenteCostoConcepto;
        $conceptoSaya->tipoValorComponenteCostoDetalle = $concepto->lsTipoComponenteCostoConcepto == 'Porcentaje' ? "%":"$";
        return $conceptoSaya;
    }
}