<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;

class DocumentoModel extends Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = \Session::get('txBaseDatosCompania').'.Documento';
    }

    protected $primaryKey = 'idDocumento';
    protected $fillable = ['moduloDocumento' , 'nombreDocumento' , 'codigoAlternoDocumento'];
    public $timestamps = false;

    public function getConcatNameAttribute(){
        return $this->codigoAlternoDocumento . ' - ' . $this->nombreDocumento;
    }
}
