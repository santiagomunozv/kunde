<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;
use Session;

class MovimientoModel extends Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table = \Session::get('txBaseDatosCompania').'.Movimiento';
    }
    public $timestamps= false;
    protected $primaryKey = 'idMovimiento';
    protected $fillable = [
        'fechaElaboracionMovimiento',
        'numeroMovimiento',
        'Tercero_idPrincipal',
        'Tercero_idEntrega',
        'valorTotalMovimiento',
        'totalUnidadesMovimiento',
        'Documento_idDocumento',
        'tasaCambioMovimiento'
    ];

    public function terceroPrincipal()
    {
        return $this->belongsTo('Modules\Saya\Entities\TerceroModel','Tercero_idPrincipal');
    }
    public function terceroEntrega()
    {
        return $this->belongsTo('Modules\Saya\Entities\TerceroModel','Tercero_idEntrega');
    }
}
