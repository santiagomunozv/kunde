<?php

namespace Modules\Saya\Entities;

use App\AuthService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Comercial\Entities\ListaPrecioModel;
use Session;

class ListaPrecioSayaModel extends Model
{
    protected $table = 'ListaPrecio';
    protected $primaryKey = 'idListaPrecio';
    public $timestamps = false;

    public static function fromListaPrecioScalia(ListaPrecioModel $listaPrecio , $idMoneda)
    {
        $usuario = SayaFichaFactory::usuarioSaya(Session::get(AuthService::$DATABASE_IDENTIFIER));
        $ahora = Carbon::now();
        $inicioLista = Carbon::parse($listaPrecio->dtFechaInicioListaPrecio);
        $finLista = Carbon::parse($listaPrecio->dtFechaFinListaPrecio);
        $listaPrecioSaya = new Self();
        $listaPrecioSaya->idListaPrecio = $listaPrecio->oidSayaListaPrecio;
        $listaPrecioSaya->codigoAlternoListaPrecio = $listaPrecio->inNumeroListaPrecio;
        $listaPrecioSaya->nombreListaPrecio = $listaPrecio->txNombreListaPrecio;
        $listaPrecioSaya->fechaInicialListaPrecio = $inicioLista;
        $listaPrecioSaya->fechaFinalListaPrecio = $finLista;
        $listaPrecioSaya->Moneda_idMoneda = $idMoneda;
        $listaPrecioSaya->redondeoListaPrecio = $listaPrecio->lsRedondeoListaPrecio;
        $listaPrecioSaya->ComponenteCosto_idComponenteCosto = $listaPrecio->componenteCosto->inIdSaya;
        $listaPrecioSaya->SegLogin_idAdicionar = $usuario;
        $listaPrecioSaya->SegLogin_idModificar = $usuario;
        $listaPrecioSaya->fechaAdicionarListaPrecio = $ahora;
        $listaPrecioSaya->fechaModificarListaPrecio = $ahora;
        $listaPrecioSaya->horaAdicionarListaPrecio = $ahora->format('H:i:s');
        $listaPrecioSaya->horaModificarListaPrecio = $ahora->format('H:i:s');
        $listaPrecioSaya->horaInicialListaPrecio = $inicioLista->format('H:i:s');
        $listaPrecioSaya->horaFinalListaPrecio = $finLista->format('H:i:s');

/*
        $listaPrecioSaya->ivaIncluidoProductoListaPrecio = 0;
        $listaPrecioSaya->puntosListaPrecio = '';
        $listaPrecioSaya->modificarPrecioProductoListaPrecio = 1;
        $listaPrecioSaya->basadoEnListaPrecio = $listaPrecio->;
        $listaPrecioSaya->ListaPrecio_idBasadoenListaPrecio = null;
        $listaPrecioSaya->equivalePuntosListaPrecio = $listaPrecio->;

        oidListaPrecio,
        Compania_oidCompania, 
        Tercero_oidListaPrecio, 
        Bodega_oidBodegaListaPrecio, 
        ComponenteCosto_oidListaPrecio, 
        Moneda_oidDestino, 
        Moneda_oidOrigen, 
        TerceroSucursal_oidTerceroSucursal,
        TransaccionComercial_oidListaPrecio,
        inNumeroListaPrecio,
        txNombreListaPrecio, 
        dtFechaElaboracionListaPrecio, 
        dtFechaInicioListaPrecio, 
        dtFechaFinListaPrecio, 
        lsRedondeoListaPrecio, 
        deTasaCambioListaPrecio, 
        deConversionMonedaListaPrecio, 
        txEstadoListaPrecio, 
        oidSayaListaPrecio*/
        return $listaPrecioSaya;
    }
}
