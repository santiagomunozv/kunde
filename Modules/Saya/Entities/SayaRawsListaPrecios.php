<?php

namespace Modules\Saya\Entities;
use DB;
use Session;
use App\AuthService;
use Carbon\Carbon;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\FichaTecnica\Entities\FichaTecnicaModel;
use Modules\General\Entities\BodegaModel;
use Modules\General\Entities\ColorModel;
use Modules\General\Entities\MonedaModel;
use Modules\General\Entities\TallaModel;

class SayaRawsListaPrecios{
    private static $SCALIA_DB = 'DB_DATABASE';

    public static function listaPrecioFichasTecnicas(array $movimientosId , $idListaPrecio){
        if(count($movimientosId) < 1){
            return [];
        }
        $movimientosId = implode(",", $movimientosId);
        $baseDatosScalia = env(self::$SCALIA_DB);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        return DB::select(
            "SELECT oidFichaTecnica, txNombreFichaTecnica, txReferenciaFichaTecnica
            FROM {$baseDatosScalia}.gen_producto AS  PSC INNER JOIN
                (SELECT cantidadMovimientoDetalle, referenciaProducto
                FROM {$baseDatosExterna}.MovimientoDetalle INNER JOIN {$baseDatosExterna}.Producto ON Producto_idProducto = idProducto
                WHERE Movimiento_idMovimiento IN ({$movimientosId})
                GROUP BY idProducto
            ) PSA ON PSA.referenciaProducto COLLATE utf8_bin = PSC.txReferenciaProducto AND Compania_oidCompania = {$idCompania}
            INNER JOIN {$baseDatosScalia}.ftc_fichatecnica ON FichaTecnica_oidFichaTecnica_1aM = oidFichaTecnica
            LEFT JOIN {$baseDatosScalia}.com_listapreciodetalle ON ListaPrecio_oidListaPrecio_1aM = {$idListaPrecio} AND oidFichaTecnica = FichaTecnica_oidListaPrecioDetalle
            WHERE ListaPrecio_oidListaPrecio_1aM IS NULL
            GROUP BY oidFichaTecnica");
    }

    public static function listaPrecioDetallesQuery(array $movimientosId, $idFichaTecnica , $codificacion = 'R*' , BodegaModel $bodega){
        $idBodegaSaya = self::homologateBodega($bodega);
        $movimientosId = implode("," , $movimientosId);
        $grupo = 'GROUP BY '.str_replace('T*','Talla_oidFichaTecnicaTalla, ',str_replace('C*','Color_oidFichaTecnicaColor, ',str_replace('R*','FT.oidFichaTecnica, ',$codificacion)));
        $grupo = substr($grupo,0, strlen($grupo)-2);
        //$join = str_replace('T*',' AND mov.codigoAlternoTalla <=> GT.txCodigoTalla ',str_replace('C*',' AND mov.codigoAlternoColor <=> GC.txCodigoColor ',str_replace('R*',' ON mov.oidFichaTecnica = FT.oidFichaTecnica ',$codificacion)));
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $baseDatosScalia = env(self::$SCALIA_DB);
        return DB::select(
            "SELECT  
                FT.oidFichaTecnica AS idFichaTecnica, 
                FT.txNombreFichaTecnica,
                IF(POSITION('C*' IN '{$codificacion}') > 0 , Color_oidFichaTecnicaColor, null) AS Color_oidFichaTecnicaColor,
                IF(POSITION('C*' IN '{$codificacion}') > 0 , txNombreColor, null) AS txNombreColor,
                IF(POSITION('T*' IN '{$codificacion}') > 0 , Talla_oidFichaTecnicaTalla , null ) AS Talla_oidFichaTecnicaTalla,
                IF(POSITION('T*' IN '{$codificacion}') > 0 , txNombreTalla, null ) AS txNombreTalla,
                txReferenciaFichaTecnica,
                cantidadFisicaInventario as saldoActual, 
                costoPromedioInventario as promedioPonderadoActual,
                (((cantidadFisicaInventario * costoPromedioInventario) + (cantidadMovimientoDetalle * valorBaseMonedaLocal))/(cantidadFisicaInventario + cantidadMovimientoDetalle)) as promedioPonderadoNuevo,
                (SELECT promedioPonderadoNuevo) - costoPromedioInventario AS diferenciaPonderado,
                0 as TPB,
                valorBaseMonedaOrigen,
                valorBaseMonedaLocal
            FROM {$baseDatosScalia}.ftc_fichatecnica FT 
            LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicacolor FTC ON FT.oidFichaTecnica = FTC.FichaTecnica_oidFichaTecnica_1aM 
            LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicatalla FTT ON FT.oidFichaTecnica = FTT.FichaTecnica_oidFichaTecnica_1aM 
            LEFT JOIN {$baseDatosScalia}.gen_color GC ON FTC.Color_oidFichaTecnicaColor= GC.oidColor
            LEFT JOIN {$baseDatosScalia}.gen_talla GT ON FTT.Talla_oidFichaTecnicaTalla = GT.oidTalla
            LEFT JOIN (
                SELECT PSC.*, referenciaProducto , codigoAlternoColor, codigoAlternoTalla,cantidadMovimientoDetalle,valorBrutoMovimientoDetalle AS valorBaseMonedaOrigen,valorBrutoMovimientoDetalle * IF(tasaCambioMovimiento = 0, 1, tasaCambioMovimiento) AS valorBaseMonedaLocal,IFNULL(0, cantidadFisicaInventario) AS cantidadFisicaInventario, IFNULL(0, costoPromedioInventario) AS costoPromedioInventario, Bodega_idBodegaOrigen
                FROM {$baseDatosExterna}.MovimientoDetalle MD
                INNER JOIN {$baseDatosExterna}.Movimiento M ON idMovimiento = Movimiento_idMovimiento
                INNER JOIN {$baseDatosExterna}.Producto PSA ON MD.Producto_idProducto = idProducto
                INNER JOIN (
                    SELECT FT.oidFichaTecnica,P.txReferenciaProducto,P.FichaTecnica_oidFichaTecnica_1aM
                    FROM {$baseDatosScalia}.gen_producto AS P 
                    INNER JOIN {$baseDatosScalia}.ftc_fichatecnica FT ON P.FichaTecnica_oidFichaTecnica_1aM = FT.oidFichaTecnica
                    WHERE FT.oidFichaTecnica = {$idFichaTecnica} AND P.Compania_oidCompania = {$idCompania}
                ) PSC ON PSA.referenciaProducto = PSC.txReferenciaProducto collate utf8_general_ci AND Movimiento_idMovimiento IN ({$movimientosId})
                LEFT JOIN {$baseDatosExterna}.Inventario I ON MD.Producto_idProducto = I.Producto_idProducto AND {$idBodegaSaya} = I.Bodega_idBodega AND (
                    SELECT idPeriodo FROM {$baseDatosExterna}.Periodo WHERE DATE_FORMAT(fechaInicialPeriodo,'%Y-%m') = DATE_FORMAT(CURDATE(),'%Y-%m')
                ) = I.Periodo_idPeriodo
                LEFT JOIN {$baseDatosExterna}.Color CSA ON Color_idColor <=> CSA.idColor
                LEFT JOIN {$baseDatosExterna}.Talla TSA ON Talla_idTalla <=> TSA.idTalla
            ) mov ON mov.oidFichaTecnica = FT.oidFichaTecnica 
            WHERE FT.oidFichaTecnica = {$idFichaTecnica} 
            {$grupo}"
         );
    }

    /**
     * Metodo encargado de buscar los documentos seleccionables para una lista de precios
     * con determinado tipo de documento y determinado cliente.
     * Los movimientos desde saya deben tener una preinspeccion y tener una fecha 
     * dentro de un rango especificado en la variable min date
     * 
     */
    public static function getMovimientosListaPrecio($idDocumentoSaya , $clienteId){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $baseDatosScalia = env(self::$SCALIA_DB);
        $minDate = Carbon::now()->subMonth(6)->format("Y-m-d");
        /**
         * INNER JOIN A COM_MOVIMIENTOCOMERCIAL PORQUE SE SUPONE QUE DEBE TENER PREINSPECCION Y AL TENER PREINSPECCION TIENE QUE 
         * EXISTIR EN COM_MOVIMIENTOCOMERCIAL
         */
        return DB::select(
            "SELECT 
                idMovimiento,
                numeroReferenciaExternoMovimiento AS numeroFactura,
                fechaElaboracionMovimiento,
                numeroMovimiento,
                Pro2.oidTercero AS idProveedor,
                Pro2.txNombreTercero AS nombreProveedor,
                Cli2.oidTercero AS idCliente,
                Cli2.txNombreTercero AS nombreCliente,
                valorTotalMovimiento,
                totalUnidadesMovimiento,
                Documento_idDocumento,
                tasaCambioMovimiento,
                oidMoneda,
                txNombreMoneda,
                TempS.txnombreTemporada AS nombreTemporada,
                TempS.oidTemporada AS idTemporada
            FROM {$baseDatosExterna}.Movimiento
                INNER JOIN {$baseDatosExterna}.Documento ON Documento_idDocumento = idDocumento
                INNER JOIN {$baseDatosScalia}.com_transaccioncomercial ON codigoAlternoDocumento = txCodigoTransaccionComercial 
                INNER JOIN {$baseDatosExterna}.Tercero Pro1 ON Tercero_idPrincipal = Pro1.idTercero
                INNER JOIN {$baseDatosExterna}.Tercero Cli1 ON Tercero_idEntrega = Cli1.idTercero
                INNER JOIN {$baseDatosScalia}.asn_tercero Pro2 ON Pro1.documentoTercero = Pro2.txDocumentoTercero AND Pro1.tipoTercero NOT LIKE ('%*18*%')
                INNER JOIN {$baseDatosScalia}.asn_tercero Cli2 ON Cli1.documentoTercero = Cli2.txDocumentoTercero AND Cli1.tipoTercero NOT LIKE ('%*18*%')
                INNER JOIN {$baseDatosExterna}.Moneda  ON Movimiento.Moneda_idMoneda = Moneda.idMoneda
                INNER JOIN {$baseDatosScalia}.gen_moneda  ON Moneda.nombreCortoMoneda = gen_moneda.txCodigoMoneda
                LEFT JOIN {$baseDatosExterna}.Temporada  TempY ON Temporada_idTemporada = idTemporada
                LEFT JOIN {$baseDatosScalia}.gen_temporada  TempS ON TempY.codigoAlternoTemporada = TempS.txCodigoTemporada
            WHERE
                Documento_idDocumento = {$idDocumentoSaya} 
                AND fechaElaboracionMovimiento >= '{$minDate}' 
                AND Cli2.oidTercero = {$clienteId} 
            ORDER BY numeroMovimiento ASC"
        );
    }

    public static function getHomologatedTransaccionesComerciales(){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $baseDatosScalia = env(self::$SCALIA_DB);
        return collect(DB::select(
            "SELECT 
                oidTransaccionComercial,
                idDocumento AS idDocumentoSaya,
                txCodigoTransaccionComercial,
                txNombreTransaccionComercial
            FROM {$baseDatosScalia}.com_transaccioncomercial
                INNER JOIN {$baseDatosExterna}.Documento ON codigoAlternoDocumento = txCodigoTransaccionComercial
            WHERE txCodigoTransaccionComercial IN('IM')"
        ));
    }

    public static function getProductoIdForDetalleListaPrecio(FichaTecnicaModel $fichaTecnica, ColorModel $color = null, TallaModel $talla = null , TerceroModel $tercero){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $join = "";
        $and = "";
        if($color){
            $and .= " AND codigoAlternoColor = '$color->txCodigoColor'";
            $join .= " INNER JOIN {$baseDatosExterna}.Color ON idColor = Color_idColor ";
        }else{
            $and = " AND ( Color_idColor IS NULL OR Color_idColor = 0 )";
        }

        if($talla){
            $and .= " AND codigoAlternoTalla = '{$talla->txCodigoTalla}'";
            $join .= " INNER JOIN {$baseDatosExterna}.Talla ON  idTalla = Talla_idTalla";
        }else{
            $and .= " AND ( Talla_idTalla IS NULL OR Talla_idTalla = 0 )";
        }
        $consulta = DB::select("SELECT idProducto FROM {$baseDatosExterna}.Producto 
        {$join}
        WHERE codigoAlternoProducto = '{$fichaTecnica->txReferenciaFichaTecnica}' AND referenciaProducto like '{$tercero->txPrefijoProductoTercero}%' $and");
        return $consulta ? $consulta[0]->idProducto:null;
    }

    public static function getCostosYSaldosProducto($codigoBodega , $idProducto){
        $noResults = (object)["costoPromedioInventario" =>0 , "cantidadFisicaInventario" => 0];
        if(!$idProducto || !$codigoBodega){
            return $noResults;
        }
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $consulta = DB::select("SELECT 
            costoPromedioInventario,
            cantidadFisicaInventario
        FROM
            {$baseDatosExterna}.Producto
                INNER JOIN
            {$baseDatosExterna}.Inventario ON Producto_idProducto = idProducto
                INNER JOIN
            {$baseDatosExterna}.Bodega ON Bodega_idBodega = idBodega 
                INNER JOIN
            {$baseDatosExterna}.Periodo ON Periodo_idPeriodo = idPeriodo
            WHERE idProducto = {$idProducto} AND codigoAlternoBodega = '{$codigoBodega}'
            AND DATE_FORMAT(fechaInicialPeriodo,'%Y-%m') = DATE_FORMAT(CURDATE(),'%Y-%m')");
        return $consulta ? $consulta[0] : $noResults;
    }

    public static function masiveInsertQuery(array $idsMovimiento ,array $idsFichasTecnicas, BodegaModel $bodega){
        //FIXME: eliminar los campos de bodega
        //FIXME: hacer la busqueda de inventario por la bodega de la lista de precios
        $idBodegaSaya = self::homologateBodega($bodega);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $baseDatosScalia = env(self::$SCALIA_DB);
        $idsFichasTecnicas = implode(',' , $idsFichasTecnicas);
        $idsMovimiento = implode(',' , $idsMovimiento );
        return DB::select("SELECT oidBodega, 
            FT.oidFichaTecnica AS idFichaTecnica, 
            FT.txNombreFichaTecnica,
            cantidadFisicaInventario as saldoActual, 
            costoPromedioInventario as promedioPonderadoActual,
            (((cantidadFisicaInventario * costoPromedioInventario) + (cantidadMovimientoDetalle * valorBaseMonedaLocal))/(cantidadFisicaInventario + cantidadMovimientoDetalle)) as promedioPonderadoNuevo,
            (SELECT promedioPonderadoNuevo) - costoPromedioInventario as diferenciaPonderado,
            0 as TPB,
            valorBaseMonedaOrigen,
            valorBaseMonedaLocal
        FROM {$baseDatosScalia}.ftc_fichatecnica FT 
        INNER JOIN (
            SELECT PSC.*, referenciaProducto , cantidadMovimientoDetalle,valorBrutoMovimientoDetalle AS valorBaseMonedaOrigen,valorBrutoMovimientoDetalle * IF(tasaCambioMovimiento = 0, 1, tasaCambioMovimiento) AS valorBaseMonedaLocal,IFNULL(0, cantidadFisicaInventario) AS cantidadFisicaInventario, IFNULL(0, costoPromedioInventario) AS costoPromedioInventario, Bodega_idBodegaOrigen
            FROM {$baseDatosExterna}.MovimientoDetalle MD
            INNER JOIN {$baseDatosExterna}.Movimiento M ON idMovimiento = Movimiento_idMovimiento
            INNER JOIN {$baseDatosExterna}.Producto PSA ON MD.Producto_idProducto = idProducto
            INNER JOIN (
                SELECT FichaTecnica_oidFichaTecnica_1aM AS oidFichaTecnica,P.txReferenciaProducto
                FROM {$baseDatosScalia}.gen_producto AS P 
                WHERE P.Compania_oidCompania = 2 AND FichaTecnica_oidFichaTecnica_1aM in( {$idsFichasTecnicas} )
            ) PSC ON PSA.referenciaProducto = PSC.txReferenciaProducto COLLATE utf8_bin
            LEFT JOIN {$baseDatosExterna}.Inventario I ON MD.Producto_idProducto = I.Producto_idProducto 
                    AND {$idBodegaSaya} = I.Bodega_idBodega 
                    AND (SELECT idPeriodo FROM {$baseDatosExterna}.Periodo WHERE DATE_FORMAT(fechaInicialPeriodo,'%Y-%m') = DATE_FORMAT(CURDATE(),'%Y-%m')) = I.Periodo_idPeriodo
           -- AND MD.Bodega_idBodegaOrigen = I.Bodega_idBodega AND M.Periodo_idPeriodo = I.Periodo_idPeriodo
            WHERE Movimiento_idMovimiento IN ({$idsMovimiento})
        ) mov ON mov.oidFichaTecnica = FT.oidFichaTecnica
        LEFT JOIN {$baseDatosExterna}.Bodega ON Bodega_idBodegaOrigen = idBodega 
        LEFT JOIN {$baseDatosScalia}.gen_bodega ON codigoAlternoBodega = txCodigoBodega 
        GROUP BY FT.oidFichaTecnica");
    }

    public static function homologateBodega(BodegaModel $bodega){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $consulta = DB::select("SELECT idBodega FROM {$baseDatosExterna}.Bodega WHERE codigoAlternoBodega = '{$bodega->txCodigoBodega}'");
        return $consulta ? $consulta[0]->idBodega: null;
    }

    public static function homologateMoneda(MonedaModel $moneda){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $consulta = DB::select("SELECT idMoneda FROM {$baseDatosExterna}.Moneda WHERE codigoAlternoMoneda = '{$moneda->txCodigoMoneda}'");
        return $consulta ? $consulta[0]->idMoneda: null;
    }
}