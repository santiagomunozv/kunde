<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductoTerceroSayaModel extends Model
{

    protected $table = 'ProductoTercero';
    protected $primaryKey = 'idProductoTercero';
    protected $fillable = ['Tercero_idTercero','Producto_idProducto','referenciaProductoTercero','nombreProductoTercero','pluProductoTercero','codigoBarrasProductoTercero',
    'codigoAlternoProductoTercero','UnidadMedida_idUnidadMedidaCompra','UnidadMedida_idUnidadMedidaVenta','factorUnidadMedidaProductoTercero','precioVentaPublicoProductoTercero',
    'operadorUnidadMedidaProductoTercero','compraMinimaProductoTercero','diasEstimadosEntregaProductoTercero'];

    public $timestamps = false;

}
