<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductoImpuestoSayaModel extends Model
{

    protected $primaryKey = 'idProductoImpuesto';
    protected $fillable = ['Producto_idProducto','Impuesto_idImpuesto'];

    public $timestamps = false;

}
