<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class ProductoSayaModel extends Model
{

    protected $table = 'Producto';
    protected $primaryKey = 'idProducto';
    protected $fillable = ['codigoAlternoProducto','referenciaProducto','nombreCortoProducto','nombreLargoProducto','Talla_idTalla','clasificacionProducto','fechaCreacionProducto',
    'estadoProducto','TipoProducto_idTipoProducto','TipoNegocio_idTipoNegocio','conceptoProducto','codigoBarrasProducto','Color_idColor','Temporada_idTemporada',
    'FichaTecnica_idFichaTecnica','Categoria_idCategoria','cantidadContenidaProducto','UnidadMedida_idCompra','UnidadMedida_idVenta','Clima_idClima','Difusion_idDifusion',
    'Estrategia_idEstrategia','Seccion_idSeccion','Evento_idEvento','ClienteObjetivo_idClienteObjetivo','EsquemaProducto_idEsquemaProducto',
    'EstadoConservacion_idEstadoConservacion','Tercero_idCliente','referenciaClienteProducto','Pais_idPaisOrigen','precioProducto','Marca_idMarca','Pais_idPais',
    'nombreColorEspecialProducto','SegLogin_idAdicionar','SegLogin_idModificar','fechaModificarProducto'];

    public $timestamps = false;
}
