<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\General\Entities\ComponenteCostoModel;

class ComponenteCostoSayaModel extends Model
{
    protected $table = 'ComponenteCosto';
    protected $primaryKey = 'idComponenteCosto';
    public $timestamps = false;

    public static function fromComponenteCostoScalia(ComponenteCostoModel $componente)
    {
        $componenteSaya = new self();
        $componenteSaya->idComponenteCosto = $componente->inIdSaya;
        $componenteSaya->exists = $componenteSaya->idComponenteCosto ? true:false;
        $componenteSaya->codigoAlternoComponenteCosto = $componente->inConsecutivoComponenteCosto;
        $componenteSaya->nombreComponenteCosto = $componente->txNombreComponenteCosto;
        return $componenteSaya;
    }
}
