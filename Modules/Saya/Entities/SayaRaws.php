<?php

namespace Modules\Saya\Entities;
use DB;
use Session;
use App\AuthService;
/**
 * Clase dedicada a contener metodos relacionados a la preinspeccion y que interactúa con 
 * las diferentes bases de datos de las diversas compañias
 * 
 */
class SayaRaws{
    private static $SCALIA_DB = 'DB_DATABASE';

    public static function preinspeccionRegistros($movimientoId , $referenciaFichaTecnica){
        $baseDatosScalia = env(self::$SCALIA_DB);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        return DB::select("SELECT 
        FT.oidFichaTecnica, 
        cantidadMovimientoDetalle, 
        valorBrutoMovimientoDetalle, 
        pesoTotalMovimientoDetalle, 
        FT.txReferenciaFichaTecnica, 
        oidFichaTecnicaColor,
        oidFichaTecnicaTalla,
        txNombreEspecialFichaTecnicaColor AS txNombreColor,
        txNombreTalla
        FROM  {$baseDatosExterna}.MovimientoDetalle MD 
        INNER JOIN {$baseDatosExterna}.Producto PSA ON Producto_idProducto = idProducto 
        INNER JOIN {$baseDatosScalia}.gen_producto PSC ON PSA.referenciaProducto = BINARY PSC.txReferenciaProducto AND Compania_oidCompania = {$idCompania} 
        INNER JOIN {$baseDatosScalia}.ftc_fichatecnica FT ON PSC.FichaTecnica_oidFichaTecnica_1aM = FT.oidFichaTecnica 
        LEFT JOIN {$baseDatosExterna}.Color CSA ON PSA.Color_idColor = CSA.idColor 
        LEFT JOIN {$baseDatosExterna}.Talla TSA ON PSA.Talla_idTalla = TSA.idTalla 
        LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicacolor FTC ON FT.oidFichaTecnica = FTC.FichaTecnica_oidFichaTecnica_1aM
        LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicatalla FTT ON FT.oidFichaTecnica = FTT.FichaTecnica_oidFichaTecnica_1aM
        LEFT JOIN {$baseDatosScalia}.gen_color GC ON FTC.Color_oidFichaTecnicaColor = GC.oidColor
        LEFT JOIN {$baseDatosScalia}.gen_talla GT ON FTT.Talla_oidFichaTecnicaTalla = GT.oidTalla
        WHERE txReferenciaFichaTecnica = '{$referenciaFichaTecnica}' AND Movimiento_idMovimiento = {$movimientoId}");
    }

    public static function preinspeccionRegistrosCombo($idMovimiento , $referenciaFichaTecnica, $codificacion = 'R*C*'){
        $baseDatosScalia = env(self::$SCALIA_DB);
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $grupo = 'GROUP BY '.str_replace('T*','FichaTecnicaTalla_oidHija, ',str_replace('C*','FichaTecnicaColor_oidHija, ',str_replace('R*','FichaTecnica_oidHija, ',$codificacion)));
        $grupo2 = 'GROUP BY '.str_replace('T*','FichaTecnicaTalla_oidPreinspeccionDetalle, ',str_replace('C*','FichaTecnicaColor_oidPreinspeccionDetalle, ',str_replace('R*','FichaTecnica_oidPreinspeccionDetalle, ',$codificacion)));
        $grupo = substr($grupo,0, strlen($grupo)-2);
        $grupo2 = substr($grupo2,0, strlen($grupo2)-2);
        return DB::select(
            "SELECT 
                *
            FROM
                (SELECT 
                    FT.txReferenciaFichaTecnica,
                    FT.txNombreFichaTecnica AS nombreLargoProducto,
                    FT.oidFichaTecnica,
                    IF(POSITION('C*' IN '{$codificacion}') > 0, FTC.oidFichaTecnicaColor, NULL) AS oidFichaTecnicaColor,
                    IF(POSITION('T*' IN '{$codificacion}') > 0, FTT.oidFichaTecnicaTalla, NULL) AS oidFichaTecnicaTalla,
                    IF(POSITION('C*' IN '{$codificacion}') > 0, GC.txNombreColor, NULL) AS txNombreColor,
                    IF(POSITION('T*' IN '{$codificacion}') > 0, GT.txNombreTalla, NULL) AS txNombreTalla,
                    SUM(cantidadMovimientoDetalle) AS cantidadMovimientoDetalle,
                    SUM(IFNULL(pesoTotalMovimientoDetalle, 0)) AS pesoTotalMovimientoDetalle,
                    IFNULL(valorBrutoMovimientoDetalle * (dePorcentajeFichaTecnicaCombo/100), 0) AS valorBrutoMovimientoDetalle
                FROM
                    {$baseDatosScalia}.ftc_fichatecnica FTP
                LEFT JOIN ftc_fichatecnicacombo FTCom  ON FTP.oidFichaTecnica = FTCom.FichaTecnica_oidPadre
                LEFT JOIN {$baseDatosScalia}.ftc_fichatecnica FT ON FT.oidFichaTecnica = FTCom.FichaTecnica_oidHija
                LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicacolor FTC ON FTC.oidFichaTecnicaColor = FTCom.FichaTecnicaColor_oidHija
                LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicatalla FTT ON FTT.oidFichaTecnicaTalla = FTCom.FichaTecnicaTalla_oidHija
                LEFT JOIN {$baseDatosScalia}.gen_color GC ON FTC.Color_oidFichaTecnicaColor = GC.oidColor
                LEFT JOIN {$baseDatosScalia}.gen_talla GT ON FTT.Talla_oidFichaTecnicaTalla = GT.oidTalla
                LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicacolor FTCP ON FTCP.oidFichaTecnicaColor = FTCom.FichaTecnicaColor_oidPadre
                LEFT JOIN {$baseDatosScalia}.ftc_fichatecnicatalla FTTP ON FTTP.oidFichaTecnicaTalla = FTCom.FichaTecnicaTalla_oidPadre
                LEFT JOIN {$baseDatosScalia}.gen_color GCP ON FTCP.Color_oidFichaTecnicaColor = GCP.oidColor
                LEFT JOIN {$baseDatosScalia}.gen_talla GTP ON FTTP.Talla_oidFichaTecnicaTalla = GTP.oidTalla
                LEFT JOIN (SELECT 
                    FT.oidFichaTecnica,
                        codigoAlternoColor,
                        codigoAlternoTalla,
                        cantidadMovimientoDetalle,
                        valorBrutoMovimientoDetalle,
                        pesoTotalMovimientoDetalle,
                        PSC.txReferenciaProducto,
                        PSC.FichaTecnica_oidFichaTecnica_1aM
                FROM
                    {$baseDatosExterna}.MovimientoDetalle MD
                INNER JOIN {$baseDatosExterna}.Producto PSA ON Producto_idProducto = idProducto
                INNER JOIN {$baseDatosScalia}.gen_producto PSC ON PSA.referenciaProducto = BINARY PSC.txReferenciaProducto AND PSC.Compania_oidCompania = {$idCompania}
                INNER JOIN {$baseDatosScalia}.ftc_fichatecnica FT ON PSC.FichaTecnica_oidFichaTecnica_1aM = FT.oidFichaTecnica
                LEFT JOIN {$baseDatosExterna}.Color CSA ON PSA.Color_idColor = CSA.idColor
                LEFT JOIN {$baseDatosExterna}.Talla TSA ON PSA.Talla_idTalla = TSA.idTalla
                WHERE
                    Movimiento_idMovimiento = {$idMovimiento}) mov ON mov.oidFichaTecnica = FTP.oidFichaTecnica
                    AND mov.codigoAlternoColor = GCP.txCodigoColor
                    AND mov.codigoAlternoTalla = GTP.txCodigoTalla
                WHERE
                    FTP.txReferenciaFichaTecnica = '{$referenciaFichaTecnica}'
                {$grupo}) saya
                    LEFT JOIN
                (SELECT 
                    FichaTecnica_oidPreinspeccionDetalle,
                        FichaTecnicaColor_oidPreinspeccionDetalle,
                        FichaTecnicaTalla_oidPreinspeccionDetalle
                FROM
                    {$baseDatosScalia}.pre_preinspecciondetalle
                {$grupo2}) scalia ON oidFichaTecnica = FichaTecnica_oidPreinspeccionDetalle
                    AND oidFichaTecnicaColor = FichaTecnicaColor_oidPreinspeccionDetalle
                    AND FichaTecnicaTalla_oidPreinspeccionDetalle = oidFichaTecnicaTalla
            WHERE
                FichaTecnica_oidPreinspeccionDetalle IS NULL");
    }

    public static function fichasByMovimientoId($idMovimiento){
        $baseDatosScalia = env(self::$SCALIA_DB);
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        return DB::select("SELECT cantidadMovimientoDetalle, oidFichaTecnica, txNombreFichaTecnica, txReferenciaFichaTecnica, chComboFichaTecnica 
            FROM {$baseDatosScalia}.gen_producto AS PSC
            INNER JOIN (
                SELECT SUM(cantidadMovimientoDetalle) as cantidadMovimientoDetalle,referenciaProducto
                FROM {$baseDatosExterna}.MovimientoDetalle MD 
                INNER JOIN {$baseDatosExterna}.Producto PSA ON Producto_idProducto = idProducto WHERE MD.Movimiento_idMovimiento = {$idMovimiento} group by idProducto
            ) as PSA ON PSA.referenciaProducto = PSC.txReferenciaProducto COLLATE utf8_bin  AND Compania_oidCompania = {$idCompania}  
            INNER JOIN {$baseDatosScalia}.ftc_fichatecnica ON FichaTecnica_oidFichaTecnica_1aM = oidFichaTecnica"
        );
    }


    public static function seleccionDocumentoQuery($numeroFactura){
        $baseDatosExterna = Session::get(AuthService::$DATABASE_IDENTIFIER);
        $baseDatosScalia = env(self::$SCALIA_DB);
        $idCompania = Session::get(AuthService::$COMPANIA_ID);
        $params = ['numeroFactura' => $numeroFactura];
        return DB::select("SELECT 
            idMovimiento,
            Documento_idDocumento,
            fechaElaboracionMovimiento,
            numeroReferenciaExternoMovimiento,
            numeroMovimiento,
            valorTotalMovimiento,
            totalUnidadesMovimiento,
            Tercero_idEntrega,
            Tercero_idTercero,
            oidTransaccionComercial,
            Pro2.oidTercero AS idProveedor,
            Cli2.oidTercero AS idCliente,
            Pro2.txNombreTercero,
            Cli2.txNombreTercero as txNombreTerceroCliente,
            oidMovimientoComercial,
            oidMoneda,
            tasaCambioMovimiento,
            txCodigoTransaccionComercial
        FROM
            {$baseDatosExterna}.Movimiento 
            INNER JOIN {$baseDatosExterna}.Documento ON idDocumento = Documento_idDocumento AND idDocumento IN (28 , 20, 35)
            INNER JOIN {$baseDatosScalia}.com_transaccioncomercial ON codigoAlternoDocumento = txCodigoTransaccionComercial
            INNER JOIN {$baseDatosExterna}.Tercero AS Pro1 ON Tercero_idPrincipal = Pro1.idTercero 
            INNER JOIN {$baseDatosExterna}.Tercero AS Cli1 ON Tercero_idEntrega = Cli1.idTercero
            LEFT JOIN {$baseDatosExterna}.Moneda ON Moneda_idMoneda = idMoneda
            LEFT JOIN {$baseDatosScalia}.com_movimientocomercial ON Movimiento_oidMovimientoERP = idMovimiento AND Compania_oidCompania = {$idCompania}
            LEFT JOIN {$baseDatosScalia}.pre_preinspeccion ON MovimientoComercial_oidMovimientoComercial = oidMovimientoComercial AND txEstadoPreinspeccion != 'Anulado'
            INNER JOIN {$baseDatosScalia}.asn_tercero AS Pro2 ON Pro1.documentoTercero = Pro2.txDocumentoTercero AND Pro1.tipoTercero NOT LIKE ('%*18*%') 
            LEFT JOIN {$baseDatosScalia}.asn_tercero AS Cli2 ON Cli1.documentoTercero = Cli2.txDocumentoTercero AND Cli1.tipoTercero NOT LIKE ('%*18*%') 
            LEFT JOIN {$baseDatosScalia}.gen_moneda ON Moneda.nombreCortoMoneda = gen_moneda.txCodigoMoneda
            WHERE numeroReferenciaExternoMovimiento = :numeroFactura AND oidPreinspeccion IS NULL
            ORDER BY numeroMovimiento DESC", $params
        );
    }
}