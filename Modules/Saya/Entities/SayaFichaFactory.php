<?php

namespace Modules\Saya\Entities;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SayaFichaFactory
{

    public static function setValuesModel($model,$consulta){

        foreach($consulta as $key => $dato){
            $model[$key] = $dato;
        }

        return $model;
    }

    public static function limpiarConsulta($datos){
        foreach($datos as $key => $campo){
            if(is_null($campo)){
                unset($datos->$key);
            }
        }

        return $datos;
    }

    public static function usuarioSaya($bdCompania){
        $usuario = Auth::user()->txLoginUsuario;
        $consulta = DB::select("SELECT id FROM $bdCompania.SegLogin WHERE usuarioLogin = '$usuario' LIMIT 1");
        $id = $consulta[0]->id ?? 0;

        return $id;
    }

    public static function consultaDinamicos($producto,$bdCompania,$dinamico){
        $consulta = DB::select(self::armarConsultaDinamicos($dinamico,$producto->FichaTecnica_oidFichaTecnica_1aM,$producto->Tercero_oidProducto,$bdCompania));
        $id = "id".$dinamico;
        return $consulta[0]->$id ?? null;
    }

    public static function armarConsultaDinamicos($dinamico,$idFicha,$idTercero,$bdCompania){
        return "SELECT D.id$dinamico FROM kunde.ftc_fichatecnicagrupo FTG
        JOIN kunde.ftc_fichatecnicatercero FTT ON FTG.FichaTecnicaTercero_oidFichaTecnicaTercero_1aM = FTT.oidFichaTecnicaTercero
        JOIN kunde.ftc_grupoproducto GP ON GP.oidGrupoProducto = FTG.GrupoProducto_oidFichaTecnicaGrupo
        JOIN kunde.gen_clasificacionproductogrupo CPG ON CPG.oidClasificacionProductoGrupo = GP.ClasificacionProductoGrupo_oidClasificacionProductoGrupo
        LEFT JOIN $bdCompania.$dinamico D on BINARY D.codigoAlterno$dinamico = BINARY GP.txCodigoGrupoProducto
        WHERE FTT.FichaTecnica_oidFichaTecnica_1aM = $idFicha AND FTT.Tercero_oidFichaTecnicaTercero = $idTercero AND 
        CPG.txRelacionClasificacionProductoGrupo = '$dinamico'";
    }

    public static function consultaProductoImpuesto($bdCompania,$id){
        return "SELECT Impuesto_idImpuesto FROM $bdCompania.TipoNegocioImpuesto WHERE TipoNegocio_idTipoNegocio = $id;";
    }

    public static function consultaProductoRetencion($bdCompania,$id){
        return "SELECT Retencion_idRetencion FROM $bdCompania.TipoNegocioRetencion WHERE TipoNegocio_idTipoNegocio = $id;";
    }

    public static function consultaProductoTercero($bdCompania,$idProducto,$idProductoSaya){
        return "SELECT null as idProductoTercero, 
        IAT.idTercero as Tercero_idTercero, 
        $idProductoSaya as Producto_idProducto, 
        txReferenciaProductoTercero as referenciaProductoTercero, 
        txNombreProductoTercero as nombreProductoTercero, 
        txPLUProductoTercero as pluProductoTercero, 
        txCodigoBarrasProductoTercero as codigoBarrasProductoTercero, 
        txCodigoProductoTercero as codigoAlternoProductoTercero,
        0 as precioVentaPublicoProductoTercero 
        FROM kunde.gen_producto SP 
        INNER JOIN kunde.gen_productotercero SPT on SP.oidProducto = SPT.Producto_oidProducto_1a1 
        LEFT JOIN kunde.asn_tercero SAT ON SAT.oidTercero = SP.Tercero_oidProducto 
        LEFT JOIN $bdCompania.Tercero IAT ON BINARY SAT.txDocumentoTercero = BINARY IAT.documentoTercero and IAT.tipoTercero NOT LIKE '%18%' 
        WHERE SP.oidProducto = $idProducto LIMIT 1";
    }

    public static function consultaProducto($bdCompania,$id){
        return "SELECT 
        SP.txCodigoProducto AS codigoAlternoProducto,
        SP.txReferenciaProducto AS referenciaProducto,
        UPPER(SP.txNombreCortoProducto) AS nombreCortoProducto,
        UPPER(SP.txNombreProducto) AS nombreLargoProducto,
        IT.idTalla AS Talla_idTalla,
        '*01*' AS clasificacionProducto,
        SFT.daFechaCreacionFichaTecnica AS fechaCreacionProducto,
        SP.txEstadoProducto AS estadoProducto,
        ITP.idTipoProducto AS TipoProducto_idTipoProducto,
        ITN.idTipoNegocio AS TipoNegocio_idTipoNegocio,
        'PRODUCTO' AS conceptoProducto,
        SP.txCodigoBarrasProducto AS codigoBarrasProducto,
        IC.idColor AS Color_idColor,
        ITE.idTemporada AS Temporada_idTemporada,
        ICA.idCategoria AS Categoria_idCategoria,
        SFT.inCantidadContenidaFichaTecnica AS cantidadContenidaProducto,
        IUMC.idUnidadMedida AS UnidadMedida_idCompra,
        IUMV.idUnidadMedida AS UnidadMedida_idVenta,
        IEV.idEvento AS Evento_idEvento,
        IAT.idTercero AS Tercero_idCliente,
        SPT.txReferenciaProductoTercero AS referenciaClienteProducto,
        IPO.idPais AS Pais_idPaisOrigen,
        IM.idMarca AS Marca_idMarca,
        IPP.idPais AS Pais_idPais,
        SFTC.txNombreEspecialFichaTecnicaColor AS nombreColorEspecialProducto,
        '' as gastoImportacionProducto 
        FROM kunde.gen_producto SP 
        INNER JOIN kunde.ftc_fichatecnica SFT ON SFT.oidFichaTecnica = SP.FichaTecnica_oidFichaTecnica_1aM 
        INNER JOIN kunde.ftc_fichatecnicatercero SFTT ON SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFichaTecnica_1aM AND SFTT.Tercero_oidFichaTecnicaTercero = SP.Tercero_oidProducto 
        LEFT JOIN kunde.gen_productotercero SPT ON SP.oidProducto = SPT.Producto_oidProducto_1a1 
        LEFT JOIN kunde.ftc_fichatecnicacolor SFTC ON SFT.oidFichaTecnica = SFTC.FichaTecnica_oidFichaTecnica_1aM AND SFTC.Color_oidFichaTecnicaColor = SP.Color_oidProducto 
        LEFT JOIN kunde.gen_talla ST ON ST.oidTalla = SP.Talla_oidProducto 
        LEFT JOIN $bdCompania.Talla IT ON BINARY ST.txCodigoTalla = BINARY IT.codigoAlternoTalla 
        LEFT JOIN kunde.gen_tipoproducto STP ON STP.oidTipoProducto = SPT.TipoProducto_oidTipoProducto 
        LEFT JOIN $bdCompania.TipoProducto ITP ON BINARY STP.txCodigoTipoProducto = BINARY ITP.codigoAlternoTipoProducto 
        LEFT JOIN kunde.gen_tiponegocio STN ON STN.oidTipoNegocio = SFTT.TipoNegocio_oidFichaTecnicaTercero 
        LEFT JOIN $bdCompania.TipoNegocio ITN ON BINARY ITN.codigoAlternoTipoNegocio = BINARY STN.txCodigoTipoNegocio 
        LEFT JOIN kunde.gen_color SC ON SC.oidColor = SP.Color_oidProducto 
        LEFT JOIN $bdCompania.Color IC ON BINARY SC.txCodigoColor = BINARY IC.codigoAlternoColor 
        LEFT JOIN kunde.gen_temporada STE ON STE.oidTemporada = SFTT.Temporada_oidFichaTecnicaTercero 
        LEFT JOIN $bdCompania.Temporada ITE ON BINARY STE.txCodigoTemporada = BINARY ITE.codigoAlternoTemporada 
        LEFT JOIN kunde.gen_evento SEV ON SEV.oidEvento = SPT.Evento_oidEvento 
        LEFT JOIN $bdCompania.Evento IEV ON BINARY SEV.txCodigoEvento = BINARY IEV.codigoAlternoEvento
        LEFT JOIN kunde.gen_categoria SCA ON SCA.oidCategoria = SPT.Categoria_oidCategoria 
        LEFT JOIN $bdCompania.Categoria ICA ON BINARY SCA.txCodigoCategoria = BINARY ICA.codigoAlterno1Categoria 
        LEFT JOIN kunde.gen_unidadmedida SUMC ON SUMC.oidUnidadMedida = SFTT.UnidadMedida_oidCompra 
        LEFT JOIN $bdCompania.UnidadMedida IUMC ON BINARY SUMC.txCodigoUnidadMedida = BINARY IUMC.codigoAlternoUnidadMedida 
        LEFT JOIN kunde.gen_unidadmedida SUMV ON SUMV.oidUnidadMedida = SFTT.UnidadMedida_oidVenta 
        LEFT JOIN $bdCompania.UnidadMedida IUMV ON BINARY SUMV.txCodigoUnidadMedida = BINARY IUMV.codigoAlternoUnidadMedida 
        LEFT JOIN kunde.asn_tercero SAT ON SAT.oidTercero = SP.Tercero_oidProducto 
        LEFT JOIN $bdCompania.Tercero IAT ON BINARY SAT.txDocumentoTercero = BINARY IAT.documentoTercero and IAT.tipoTercero NOT LIKE '%18%' 
        LEFT JOIN kunde.gen_marca SM ON SM.oidMarca = SFTT.Marca_oidFichaTecnicaTercero 
        LEFT JOIN $bdCompania.Marca IM ON BINARY SM.txCodigoMarca = BINARY IM.codigoAlternoMarca 
        LEFT JOIN kunde.gen_pais SPO ON SPO.oidPais = SFTT.Pais_oidOrigen 
        LEFT JOIN $bdCompania.Pais IPO ON BINARY SPO.txCodigoDIANPais = BINARY IPO.codigoAlternoPais 
        LEFT JOIN kunde.gen_pais SPP ON SPP.oidPais = SFTT.Pais_oidProcedencia 
        LEFT JOIN $bdCompania.Pais IPP ON BINARY SPP.txCodigoDIANPais = BINARY IPP.codigoAlternoPais 
        WHERE SP.oidProducto = $id LIMIT 1";
    }
    
    public static function consultaProductoExiste($referenciaProducto){
        return "SELECT idProducto FROM Producto WHERE referenciaProducto = '$referenciaProducto'";
    }

    public static function consultaProductoTerceroExiste($idProducto,$idTercero){
        return "SELECT idProductoTercero FROM ProductoTercero WHERE Producto_idProducto = $idProducto AND Tercero_idTercero = $idTercero";
    }

    public static function consultaTraerReferenciaFicha($referencia){
        return "INSERT INTO kunde.ftc_fichatecnica 
                    SELECT 
                    saya.oidFichaTecnica,
                    saya.txReferenciaFichaTecnica,
                    saya.chComboFichaTecnica,
                    saya.txNombreFichaTecnica,
                    saya.txNombreCortoFichaTecnica,
                    saya.inCantidadContenidaFichaTecnica,
                    saya.txDisenadorFichaTecnica,
                    saya.txMoldeFichaTecnica,
                    SLP.oidLineaProduccion as LineaProduccion_oidFichaTecnica,
                    saya.daFechaCreacionFichaTecnica,
                    saya.daFechaLiberacionFichaTecnica,
                    saya.lsEtapaFichaTecnica,
                    saya.GrupoTalla_idGrupoTalla as GrupoTalla_oidFichaTecnica,
                    saya.dePrecioVentaLocalFichaTecnica,
                    saya.dePrecioVentaExtranjeroFichaTecnica,
                    saya.txCompaniasFichaTecnica,
                    IF(saya.TipoProducto_idTipoProducto IN (46,73,74,75,76,77,78,81,82,110,146,147,150,153,155,156,157,159,160,161,162,163,164,165,166,169,170,171,172,173,174,175,197,207,224,225,230,234,246,254), 4, 
                        IF(saya.TipoProducto_idTipoProducto IN (168,213,275),2,
                        IF(saya.TipoProducto_idTipoProducto IN (113),3,
                        IF(saya.TipoProducto_idTipoProducto IN (186,179,84,187,321,154,185,79,152,281,180,80,158,148,181,240,248,302,219),7,
                        IF(saya.TipoProducto_idTipoProducto IN (301,300,299,298,309,308,285,276,291,290,288,289,307,183,184,297,215,218,190,296,201,216,212 ),8,
                        IF(saya.TipoProducto_idTipoProducto IN (159),9,
                        IF(saya.TipoProducto_idTipoProducto IN (115),6,
                        IF(saya.TipoProducto_idTipoProducto IN (270, 327,328,317, 325),5,
                        IF(saya.TipoProducto_idTipoProducto IN (331),10,
                        IF(saya.TipoProducto_idTipoProducto IN (329,330,282,279),11,
                        IF(saya.TipoProducto_idTipoProducto IN (120,182,210,232,269),12,1))))))))))) as ClasificacionProducto_oidFichaTecnica,
                    SU.oidUnidadMedida as UnidadMedida_oidFichaTecnica,
                    saya.TipoInventario_oidFichaTecnica,
                    saya.dePorcentajeEficiencia1FichaTecnica,
                    saya.dePorcentajeEficiencia2FichaTecnica,
                    oidTipoProducto as FichaTecnica_oidBase,
                    saya.txEstadoFichaTecnica 
                    FROM (
                        SELECT null as oidFichaTecnica,
                            P.codigoAlternoProducto as txReferenciaFichaTecnica,
                            0 as chComboFichaTecnica,
                            CONCAT(UCASE(LEFT(TRIM(nombreLargoProducto), 1)), LCASE(SUBSTRING(TRIM(nombreLargoProducto), 2))) as txNombreFichaTecnica,
                            CONCAT(UCASE(LEFT(TRIM(nombreCortoProducto), 1)), LCASE(SUBSTRING(TRIM(nombreCortoProducto), 2))) as txNombreCortoFichaTecnica,
                            cantidadContenidaProducto as inCantidadContenidaFichaTecnica,
                            'Ficha Técnica Automática' as txDisenadorFichaTecnica,
                            '' as txMoldeFichaTecnica,
                            fechaCreacionProducto as daFechaCreacionFichaTecnica,
                            fechaCreacionProducto as daFechaLiberacionFichaTecnica,
                            'APROBADO' as lsEtapaFichaTecnica,
                            precioProducto as dePrecioVentaLocalFichaTecnica,
                            0 as dePrecioVentaExtranjeroFichaTecnica,
                            '2' as txCompaniasFichaTecnica,
                            11 as  TipoInventario_oidFichaTecnica,
                            0 as dePorcentajeEficiencia1FichaTecnica,
                            0 as dePorcentajeEficiencia2FichaTecnica,
                            'Activo' as txEstadoFichaTecnica,
                            '' as codigoAlternoLineaProduccion,
                            codigoAlternoUnidadMedida,
                            null as GrupoTalla_idGrupoTalla,
                            P.TipoProducto_idTipoProducto ,
                            codigoAlternoTipoProducto 
                        FROM Iblu.Producto P 
                        LEFT JOIN (SELECT idUnidadMedida,codigoAlternoUnidadMedida FROM Iblu.UnidadMedida) IU ON IU.idUnidadMedida = P.UnidadMedida_idCompra 
                        LEFT JOIN (SELECT idTipoProducto,codigoAlternoTipoProducto FROM Iblu.TipoProducto) TP ON TP.idTipoProducto = P.TipoProducto_idTipoProducto 
                        WHERE codigoAlternoProducto = '{$referencia}' 
                        GROUP BY P.codigoAlternoProducto, P.TipoProducto_idTipoProducto 
                    ) saya 
                    LEFT JOIN (select oidTipoProducto, txCodigoTipoProducto from kunde.gen_tipoproducto ) TP ON saya.codigoAlternoTipoProducto= txCodigoTipoProducto 
                    LEFT JOIN (select oidFichaTecnica,txReferenciaFichaTecnica, FichaTecnica_oidBase from kunde.ftc_fichatecnica) FTC ON TRIM(FTC.txReferenciaFichaTecnica) = TRIM(saya.txReferenciaFichaTecnica) AND FichaTecnica_oidBase = TP.oidTipoProducto 
                    LEFT JOIN (select oidLineaProduccion,txCodigoLineaProduccion from kunde.gen_lineaproduccion) SLP ON saya.codigoAlternoLineaProduccion = SLP.txCodigoLineaProduccion 
                    LEFT JOIN (select oidUnidadMedida,txCodigoUnidadMedida from kunde.gen_unidadmedida)SU ON saya.codigoAlternoUnidadMedida = SU.txCodigoUnidadMedida 
                    WHERE saya.txReferenciaFichaTecnica = '{$referencia}'";
    }

    public static function consultaTraerReferenciaFichaTercero($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicatercero 
        select null as oidFichaTecnicaTercero,
            oidFichaTecnica as FichaTecnica_oidFIchaTecnica_1aM,
            oidTercero as Tercero_oidFichaTecnicaTercero,
            txPrefijoProductoTercero as txPrefijoTerceroFichaTecnicaTercero,
            'R' as lsCodificacionFichaTecnicaTercero,
            0 as chGeneraCodigoFichaTecnicaTercero,
            SUMC.oidUnidadMedida as UnidadMedida_oidCompra,
            SUMV.oidUnidadMedida as UnidadMedida_oidVenta,
            SP.oidPais as Pais_oidOrigen,
            SP.oidPais as Pais_oidProcedencia,
            STN.oidTipoNegocio as TipoNegocio_oidFichaTecnicaTercero,
            STEM.oidTemporada as Temporada_oidFichaTecnicaTercero,
            SMAR.oidMarca as Marca_oidFichaTecnicaTercero,
            STP.oidTipoProducto  as TipoProducto_oidFichaTecnicaTercero,
            SCAT.oidCategoria as Categoria_oidFichaTecnicaTercero,
            SEVE.oidEvento as Evento_oidFichaTecnicaTercero
        FROM
        (  
          SELECT codigoAlternoProducto, referenciaProducto, 
                UnidadMedida_idCompra, UnidadMedida_idVenta, Pais_idPaisOrigen, TipoNegocio_idTipoNegocio,
                FT.TipoProducto_idTipoProducto, Temporada_idTemporada, Marca_idMarca, Categoria_idCategoria,
                oidFichaTecnica, UMC.codigoAlternoUnidadMedida as codigoAlternoUnidadMedidaUMC, UMV.codigoAlternoUnidadMedida as codigoAlternoUnidadMedidaUMV,
                codigoAlternoPais, codigoAlternoTipoNegocio, codigoAlternoTipoProducto, codigoAlternoTemporada, 
                codigoAlternoMarca, codigoAlterno1Categoria,codigoAlternoEvento, oidTercero, txPrefijoProductoTercero
            FROM Iblu.Producto FT
            LEFT JOIN Iblu.UnidadMedida UMC ON FT.UnidadMedida_idCompra = UMC.idUnidadMedida
            LEFT JOIN Iblu.UnidadMedida UMV ON FT.UnidadMedida_idVenta = UMV.idUnidadMedida
            LEFT JOIN Iblu.Pais P on FT.Pais_idPaisOrigen = P.idPais
            LEFT JOIN Iblu.TipoNegocio TN on FT.TipoNegocio_idTipoNegocio = TN.idTipoNegocio
            LEFT JOIN Iblu.TipoProducto TP on FT.TipoProducto_idTipoProducto = TP.idTipoProducto
            LEFT JOIN Iblu.Temporada TEM on FT.Temporada_idTemporada = TEM.idTemporada
            LEFT JOIN Iblu.Marca MAR on FT.Marca_idMarca = MAR.idMarca
            LEFT JOIN Iblu.Categoria CAT on FT.Categoria_idCategoria = CAT.idCategoria
            LEFT JOIN Iblu.Evento EVE on FT.Evento_idEvento = EVE.idEvento
            INNER JOIN 
              (
                select oidFichaTecnica,txReferenciaFichaTecnica, SUBSTRING(referenciaProducto, 1, LOCATE('*', referenciaProducto)), P.TipoProducto_idTipoProducto
                from kunde.ftc_fichatecnica SFT
                LEFT JOIN kunde.gen_tipoproducto STP on FichaTecnica_oidBase = oidTipoProducto
                LEFT JOIN Iblu.TipoProducto TP ON codigoAlternoTipoProducto = txCodigoTipoProducto
                LEFT JOIN Iblu.Producto P ON txReferenciaFichaTecnica = codigoAlternoProducto AND P.TipoProducto_idTipoProducto = TP.idTipoProducto
                where P.referenciaProducto != '' AND SFT.txReferenciaFichaTecnica = '{$referencia}' 
                GROUP BY oidFichaTecnica, SUBSTRING(referenciaProducto, 1, LOCATE('*', referenciaProducto)), P.TipoProducto_idTipoProducto
              ) SFT on FT.codigoAlternoProducto = SFT.txReferenciaFichaTecnica and SFT.TipoProducto_idTipoProducto = FT.TipoProducto_idTipoProducto
              INNER JOIN (select oidTercero,txPrefijoProductoTercero from kunde.asn_tercero where txPrefijoProductoTercero is not null and txPrefijoProductoTercero != '') T 
              ON SUBSTRING(referenciaProducto, 1, LOCATE('*', referenciaProducto)) = txPrefijoProductoTercero 
            where FT.referenciaProducto != '' AND SFT.txReferenciaFichaTecnica = '{$referencia}' 
            GROUP BY SFT.oidFichaTecnica, T.oidTercero, FT.TipoProducto_idTipoProducto
        ) temp
        LEFT JOIN (select oidUnidadMedida,txCodigoUnidadMedida from kunde.gen_unidadmedida) SUMC ON temp.codigoAlternoUnidadMedidaUMC = SUMC.txCodigoUnidadMedida
        LEFT JOIN (select oidUnidadMedida,txCodigoUnidadMedida from kunde.gen_unidadmedida) SUMV ON temp.codigoAlternoUnidadMedidaUMV = SUMV.txCodigoUnidadMedida
        LEFT JOIN (select oidPais,txCodigoISOPais from kunde.gen_pais) SP ON temp.codigoAlternoPais = txCodigoISOPais
        LEFT JOIN (select oidTipoNegocio,txCodigoTipoNegocio from kunde.gen_tiponegocio) STN ON temp.codigoAlternoTipoNegocio = txCodigoTipoNegocio
        LEFT JOIN (select oidTipoProducto,txCodigoTipoProducto from kunde.gen_tipoproducto) STP ON temp.codigoAlternoTipoProducto = txCodigoTipoProducto
        LEFT JOIN (select oidTemporada,txCodigoTemporada from kunde.gen_temporada) STEM ON temp.codigoAlternoTemporada = STEM.txCodigoTemporada
        LEFT JOIN (select oidMarca,txCodigoMarca from kunde.gen_marca) SMAR ON temp.codigoAlternoMarca = SMAR.txCodigoMarca
        LEFT JOIN (select oidCategoria,txCodigoCategoria from kunde.gen_categoria) SCAT ON temp.codigoAlterno1Categoria = SCAT.txCodigoCategoria
        left join (select oidEvento,txCodigoEvento from kunde.gen_evento) SEVE on temp.codigoAlternoEvento = SEVE.txCodigoEvento";
    }
    
    public static function consultaTraerReferenciaFichaColor($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicacolor 
        SELECT null as oidFichaTecnicaColor,
            SFT.oidFichaTecnica as FichaTecnica_oidFichaTecnica_1aM,
            oidColor as Color_oidFichaTecnicaColor,
            '' as txCodigoEspecialFichaTecnicaColor,
            nombreEspecialColor as txNombreEspecialFichaTecnicaColor
        FROM
        (
            SELECT nombreEspecialFichaTecnicaColor as nombreEspecialColor,referenciaBaseFichaTecnica as referenciaFichaTecnica,codigoAlternoColor, FT.TipoProducto_idTipoProducto
            FROM Iblu.FichaTecnica FT
            LEFT JOIN Iblu.FichaTecnicaColor FTC ON FT.idFichaTecnica = FTC.FichaTecnica_idFichaTecnica
            INNER JOIN Iblu.Color C ON FTC.Color_idColor = C.idColor
            INNER JOIN 
          (
            select oidFichaTecnica,txReferenciaFichaTecnica , idTipoProducto
            from kunde.ftc_fichatecnica
            LEFT JOIN kunde.gen_tipoproducto STP on FichaTecnica_oidBase = oidTipoProducto
            LEFT JOIN Iblu.TipoProducto TP ON codigoAlternoTipoProducto = txCodigoTipoProducto
          ) SFT ON FT.referenciaBaseFichaTecnica = SFT.txReferenciaFIchaTecnica AND FT.TipoProducto_idTipoProducto = SFT.idTipoProducto
            GROUP BY FT.referenciaBaseFichaTecnica, FT.TipoProducto_idTipoProducto, FTC.Color_idColor
        
            UNION
        
            SELECT nombreColorEspecialProducto as nombreEspecialColor,codigoAlternoProducto as referenciaFichaTecnica,codigoAlternoColor, FT.TipoProducto_idTipoProducto
            FROM Iblu.Producto FT
            INNER JOIN 
          (
            select txReferenciaFichaTecnica , idTipoProducto
            from kunde.ftc_fichatecnica
            LEFT JOIN kunde.gen_tipoproducto STP on FichaTecnica_oidBase = oidTipoProducto
            LEFT JOIN Iblu.TipoProducto TP ON codigoAlternoTipoProducto = txCodigoTipoProducto
          ) SFT ON FT.codigoAlternoProducto = SFT.txReferenciaFichaTecnica AND FT.TipoProducto_idTipoProducto = SFT.idTipoProducto
            INNER JOIN Iblu.Color C ON FT.Color_idColor = C.idColor
            INNER JOIN (select txCodigoColor from kunde.gen_color) SC on C.codigoAlternoColor = SC.txCodigoColor
            GROUP BY codigoAlternoProducto, FT.TipoProducto_idTipoProducto, Color_idColor
        )saya 
        INNER JOIN kunde.ftc_fichatecnica SFT ON saya.referenciaFichaTecnica = SFT.txReferenciaFichaTecnica
        INNER JOIN kunde.gen_color SC on saya.codigoAlternoColor = SC.txCodigoColor
        where oidColor != 9037 AND referenciaFichaTecnica = '{$referencia}' 
        GROUP BY oidFichaTecnica, oidColor";
    }

    public static function consultaTraerReferenciaFichaTalla($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicatalla 
        SELECT null as oidFichaTecnicaTalla, oidFichaTecnica as FichaTecnica_oidFichaTecnica_1aM, oidTalla AS Talla_oidFichaTecnicaTalla, curvaFichaTecnicaTalla as inCurvaFichaTecnicaTalla,
        BaseMedidaFichaTecnicaTalla as chBaseFichaTecnicaTalla
        FROM
        (
            SELECT FTT.curvaFichaTecnicaTalla,IF(FTT.BaseMedidaFichaTecnicaTalla = 'NO', 0, 1) as BaseMedidaFichaTecnicaTalla,FT.referenciaBaseFichaTecnica as referenciaFichaTecnica,T.codigoAlternoTalla, oidTipoProducto
            FROM Iblu.FichaTecnica FT
            INNER JOIN Iblu.FichaTecnicaTalla FTT ON FT.idFichaTecnica = FTT.FichaTecnica_idFichaTecnica
            INNER JOIN Iblu.Talla T ON FTT.Talla_idTalla = T.idTalla
            INNER JOIN 
          (
            select txReferenciaFichaTecnica, idTipoProducto, oidTipoProducto
            from kunde.ftc_fichatecnica
            LEFT JOIN kunde.gen_tipoproducto STP on FichaTecnica_oidBase = oidTipoProducto
            LEFT JOIN Iblu.TipoProducto TP ON codigoAlternoTipoProducto = txCodigoTipoProducto
          ) SFT ON FT.referenciaBaseFichaTecnica = SFT.txReferenciaFichaTecnica AND FT.TipoProducto_idTipoProducto = SFT.idTipoProducto
          GROUP BY FT.idFichaTecnica, FTT.Talla_idTalla, SFT.idTipoProducto
            
            UNION
        
            SELECT '' as curvaFichaTecnicaTalla,0 as BaseMedidaFichaTecnicaTalla,FT.codigoAlternoProducto as referenciaFichaTecnica,T.codigoAlternoTalla, oidTipoProducto
            FROM Iblu.Producto FT
            INNER JOIN Iblu.Talla T ON FT.Talla_idTalla = T.idTalla
            INNER JOIN 
          (
            select txReferenciaFichaTecnica , idTipoProducto, oidTipoProducto
            from kunde.ftc_fichatecnica
            LEFT JOIN kunde.gen_tipoproducto STP on FichaTecnica_oidBase = oidTipoProducto
            LEFT JOIN Iblu.TipoProducto TP ON codigoAlternoTipoProducto = txCodigoTipoProducto
            where txReferenciaFichaTecnica = '{$referencia}' 
          ) SFT ON FT.codigoAlternoProducto = SFT.txReferenciaFichaTecnica AND FT.TipoProducto_idTipoProducto = SFT.idTipoProducto
            WHERE FichaTecnica_idFichaTecnica = 0 or FichaTecnica_idFichaTecnica IS NULL
            GROUP BY FT.codigoAlternoProducto, FT.Talla_idTalla, SFT.idTipoProducto
        )saya 
        INNER JOIN kunde.ftc_fichatecnica SFT ON saya.referenciaFichaTecnica = SFT.txReferenciaFichaTecnica AND saya.oidTipoProducto = SFT.FichaTecnica_oidBase
        INNER JOIN kunde.gen_talla ST on saya.codigoAlternoTalla = ST.txCodigoTalla
        WHERE oidTalla != 9087 AND txReferenciaFichaTecnica = '{$referencia}' 
        GROUP BY oidFichaTecnica, oidTalla";
    }

    public static function consultaTraerReferenciaProducto($referencia){
        return "INSERT INTO kunde.gen_producto 
        SELECT NULL AS oidProducto,
            oidFichaTecnica AS FichaTecnica_oidFichaTecnica_1aM,
            Tercero_oidFichaTecnicaTercero AS Tercero_oidProducto,
            saya.codigoAlternoProducto as txCodigoProducto,
            referenciaProducto AS txReferenciaProducto,
            nombreLargoProducto AS txNombreProducto,
            nombreCortoProducto AS txNombreCortoProducto,
            SC.oidColor AS Color_oidProducto,
            STL.oidTalla AS Talla_oidProducto,
            codigoBarrasProducto as txCodigoBarrasProducto,
            precioProducto AS dePrecioProducto,
            NULL AS PartidaArancelaria_oidProducto,
            idProducto as oidSayaProducto,
            'Activo' AS txEstadoProducto,
            2 as Compania_oidCompania 
        FROM
        (
            SELECT oidFichaTecnica,idProducto,
                IF(oidFichaTecnica IS NULL, codigoAlternoProducto, txReferenciaFichaTecnica) as referenciaBaseFichaTecnica, 
                Tercero_oidFichaTecnicaTercero, codigoAlternoColor, codigoAlternoTalla, codigoAlternoProducto, referenciaProducto, nombreLargoProducto,nombreCortoProducto , codigoBarrasProducto, precioProducto
            FROM Iblu.Producto as IP
          LEFT JOIN Iblu.TipoProducto TP ON IP.TipoProducto_idTipoProducto = idTipoProducto
          LEFT JOIN kunde.gen_tipoproducto STP on TP.codigoAlternoTipoProducto = STP.txCodigoTipoProducto
            INNER JOIN kunde.ftc_fichatecnica FT ON codigoAlternoProducto = txReferenciaFIchaTecnica AND FichaTecnica_oidBase = oidTipoProducto
            LEFT JOIN kunde.ftc_fichatecnicatercero FTT ON FTT.FichaTecnica_oidFichaTecnica_1aM = FT.oidFichaTecnica AND txPrefijoTerceroFichaTecnicaTercero = TRIM(SUBSTRING(referenciaProducto, 1, LOCATE('*', referenciaProducto)))
            -- LEFT JOIN Iblu.FichaTecnica IFT ON idFichaTecnica = FichaTecnica_idFichaTecnica
            -- LEFT JOIN Iblu.Tercero IT on IT.idTercero = IP.Tercero_idCliente AND IT.tipoTercero not like '%18%' AND IT.estadoTercero = 'ACTIVO'
            LEFT JOIN Iblu.Color IC on IC.idColor = Color_idColor
            LEFT JOIN Iblu.Talla ITL on ITL.idTalla = Talla_idTalla
            where  txReferenciaFichaTecnica = '{$referencia}' 
          GROUP BY referenciaProducto
        ) saya 
        -- LEFT JOIN kunde.ftc_fichatecnica SFT ON BINARY saya.referenciaBaseFichaTecnica = BINARY SFT.txReferenciaFichaTecnica
        -- LEFT JOIN kunde.ftc_fichatecnica SFT2 ON BINARY saya.codigoAlternoProducto = BINARY SFT2.txReferenciaFichaTecnica
        -- LEFT JOIN kunde.asn_tercero ST ON BINARY ST.txDocumentoTercero = BINARY saya.documentoTercero
        LEFT JOIN kunde.gen_color SC ON saya.codigoAlternoColor = SC.txCodigoColor
        LEFT JOIN kunde.gen_talla STL ON saya.codigoAlternoTalla = STL.txCodigoTalla
        group by referenciaProducto";
    }

    public static function consultaTraerReferenciaDifusion($referencia){
        return "INSERT into kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
                select P.codigoAlternoProducto,IT.documentoTercero,ID.codigoAlternoDifusion,codigoAlternoTipoProducto 
                FROM Iblu.Producto P 
                join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
                join Iblu.Difusion ID on ID.idDifusion = P.Difusion_idDifusion 
                join Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
                where P.codigoAlternoProducto = '{$referencia}' 
                group by P.codigoAlternoProducto,TipoProducto_idTipoProducto
            ) I 
            JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
            JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
            join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
            join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
            join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
            and txCodigoGrupoProducto = I.codigoAlternoDifusion and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 1";
    }

    public static function consultaTraerReferenciaClienteObjetivo($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
        SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
        SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
            select P.codigoAlternoProducto,IT.documentoTercero,ICO.codigoAlternoClienteObjetivo,codigoAlternoTipoProducto FROM 
            Iblu.Producto P 
            JOIN Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
            JOIN Iblu.ClienteObjetivo ICO on ICO.idClienteObjetivo = P.ClienteObjetivo_idClienteObjetivo 
            JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
            where P.codigoAlternoProducto = '{$referencia}' 
            group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
        ) I 
        JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
        JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
        JOIN kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
        JOIN kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
        JOIN kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
        and txCodigoGrupoProducto = I.codigoAlternoClienteObjetivo and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 2";
    }

    public static function consultaTraerReferenciaEstrategia($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
            select P.codigoAlternoProducto,IT.documentoTercero,IE.codigoAlternoEstrategia,codigoAlternoTipoProducto FROM 
            Iblu.Producto P 
            join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
            join Iblu.Estrategia IE on IE.idEstrategia = P.Estrategia_idEstrategia 
            JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
            where P.codigoAlternoProducto = '{$referencia}' 
            group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
        ) I 
        JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
        JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
        join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
        join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
        join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
        and txCodigoGrupoProducto = I.codigoAlternoEstrategia and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 4"; 
    }

    public static function consultaTraerReferenciaSeccion($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
            select P.codigoAlternoProducto,IT.documentoTercero,ISE.codigoAlternoSeccion,codigoAlternoTipoProducto FROM 
            Iblu.Producto P 
            join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
            join Iblu.Seccion ISE on ISE.idSeccion = P.Seccion_idSeccion 
            JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
            where P.codigoAlternoProducto = '{$referencia}' 
            group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
        ) I 
        JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
        JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
        join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
        join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
        join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
        and txCodigoGrupoProducto = I.codigoAlternoSeccion and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 5";
    }

    public static function consultaTraerReferenciaEsquemaProducto($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo
        FROM(
            select P.codigoAlternoProducto,IT.documentoTercero,EP.codigoAlternoEsquemaProducto,codigoAlternoTipoProducto FROM 
            Iblu.Producto P 
            join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
            join Iblu.EsquemaProducto EP on EP.idEsquemaProducto = P.EsquemaProducto_idEsquemaProducto 
            JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
            where P.codigoAlternoProducto = '{$referencia}' 
            group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
        ) I 
        JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
        JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
        join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
        join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
        join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
        and txCodigoGrupoProducto = I.codigoAlternoEsquemaProducto and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 6";
    }

    public static function consultaTraerReferenciaEstadoConservacion($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
            select P.codigoAlternoProducto,IT.documentoTercero,EP.codigoAlternoEstadoConservacion,codigoAlternoTipoProducto FROM 
            Iblu.Producto P 
            join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
            join Iblu.EstadoConservacion EP on EP.idEstadoConservacion = P.EstadoConservacion_idEstadoConservacion 
            JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
            where P.codigoAlternoProducto = '{$referencia}' 
            group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
        ) I 
        JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
        JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
        join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
        join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
        join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
        and txCodigoGrupoProducto = I.codigoAlternoEstadoConservacion and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 7";
    }

    public static function consultaTraerReferenciaClima($referencia){
        return "INSERT INTO kunde.ftc_fichatecnicagrupo 
        SELECT NULL AS oidFichaTecnicaGrupo,
            SFTT.oidFichaTecnicaTercero as FichaTecnicaTercero_oidFichaTecnicaTercero_1aM,
            SGP.oidGrupoProducto as GrupoProducto_oidFichaTecnicaGrupo 
        FROM(
                select P.codigoAlternoProducto,IT.documentoTercero,IC.codigoAlternoClima,codigoAlternoTipoProducto FROM 
                Iblu.Producto P 
                join Iblu.Tercero IT on P.Tercero_idCliente = IT.idTercero 
                join Iblu.Clima IC on IC.idClima = P.Clima_idClima 
                JOIN Iblu.TipoProducto on TipoProducto_idTipoProducto = idTipoProducto 
                where P.codigoAlternoProducto = '{$referencia}' 
                group by P.codigoAlternoProducto,TipoProducto_idTipoProducto 
            ) I 
            JOIN kunde.gen_tipoproducto TP on TP.txCodigoTipoProducto = I.codigoAlternoTipoProducto 
            JOIN kunde.ftc_fichatecnica SFT ON SFT.txReferenciaFichaTecnica = I.codigoAlternoProducto and SFT.FichaTecnica_oidBase = TP.oidTipoProducto 
            join kunde.ftc_fichatecnicatercero SFTT on SFT.oidFichaTecnica = SFTT.FichaTecnica_oidFIchaTecnica_1aM 
            join kunde.asn_tercero ST on SFTT.Tercero_oidFichaTecnicaTercero = ST.oidTercero and ST.txDocumentoTercero = I.documentoTercero 
            join kunde.ftc_grupoproducto SGP on SGP.Tercero_oidTercero_1aM = ST.oidTercero 
            and txCodigoGrupoProducto = I.codigoAlternoClima and ClasificacionProductoGrupo_oidClasificacionProductoGrupo = 8";
    }
}