<?php

namespace Modules\Saya\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductoRetencionSayaModel extends Model
{

    protected $primaryKey = 'idProductoRetencion';
    protected $fillable = ['Producto_idProducto','Retencion_idRetencion'];

    public $timestamps = false;

}
