<?php

include_once 'nusoap/nusoap.php';

$servicio = new soap_server();
$ns = "urn:miserviciowsdl"; 
$servicio->configureWSDL("Lista precios",$ns);
$servicio->schemaTargetNamespace = $ns;
$servicio->register(
    "llenarPropiedadesListaPrecio", 
    array(
        'path' => 'xsd:string', 
        'usuario' => 'xsd:int', 
        'empresa' => 'xsd:int'
    ), 
    array('return' => 'xsd:Array'), 
    $ns 
);

function llenarPropiedadesListaPrecio($path, $usuario , $empresa) 
{
    session_start();
    $_SESSION['empresa'] = $empresa;
    $_SESSION['SesionUsuario'] = $usuario;

    require_once('interfacedatos.class.php');
    $interfacedatos = new InterfaceDatos();
    return $interfacedatos->ImportarProductoComercialPrecioExcel($path);
}

#Valida lo que esta ingresando por post
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : ''; 
#Se ejecuta lo que se envía
$servicio->service($HTTP_RAW_POST_DATA);