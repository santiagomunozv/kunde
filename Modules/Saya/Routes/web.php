<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function(){
    Route::prefix('saya')->group(function() {
        // Route::get('/', 'SayaController@index');
        // Route::get('/actualizarTercero/{id}', 'SayaController@actualizarTercero');
        // Route::get('/actualizarficha/{id}', 'SayaFichaController@actualizarFichaTecnica');
        // Route::get('/actualizarTercero/{id}/{documento}', 'SayaTerceroController@actualizarTercero');
        // Route::get('/actualizarTerceroContacto/{id}', 'SayaTerceroController@actualizarTerceroContacto');
        // Route::get('/actualizarTerceroBanco/{id}', 'SayaTerceroController@actualizarTerceroBanco');
        // Route::get('/actualizarTerceroReferencia/{id}', 'SayaTerceroController@actualizarTerceroReferencia');
        // Route::get('/actualizarAcote/{id}', 'SayaTerceroController@actualizarAcote');
        // Route::get('/actualizarBodega/{id}', 'SayaTerceroController@actualizarBodega');
        // Route::get('/actualizarColor/{id}', 'SayaTerceroController@actualizarColor');
        // Route::get('/actualizarTalla/{id}', 'SayaTerceroController@actualizarTalla');
        // Route::get('/actualizarCostoAdicional/{id}', 'SayaTerceroController@actualizarCostoAdicional');
        // Route::get('/actualizarCostoIndirecto/{id}', 'SayaTerceroController@actualizarCostoIndirecto');
        // Route::get('/actualizarUnidadMedida/{id}', 'SayaTerceroController@actualizarUnidadMedida');
        // Route::get('/actualizarCentroProduccion/{id}', 'SayaTerceroController@actualizarCentroProduccion');
        // Route::get('/actualizarTemporada/{id}', 'SayaTerceroController@actualizarTemporada');
        // Route::get('/actualizarTipoNegocio/{id}', 'SayaTerceroController@actualizarTipoNegocio');
        // Route::get('/actualizarSucursal/{id}', 'SayaTerceroController@actualizarSucursal');
        // Route::get('/actualizarPartidaArancelaria/{id}', 'SayaTerceroController@actualizarPartidaArancelaria');
        // Route::get('/actualizarfichacolor/{id}', 'SayaFichaController@actualizarFichaTecnicaColor');
        // Route::get('/actualizarfichatalla/{id}', 'SayaFichaController@actualizarFichaTecnicaTalla');
        // Route::get('/actualizarproducto/{id}', 'SayaFichaController@actualizarProducto');
        // Route::get('/actualizarFormaPago/{id}', 'SayaTerceroController@actualizarFormaPago');
        // Route::get('/actualizarNaturalezaJuridica/{id}', 'SayaTerceroController@actualizarNaturalezaJuridica');
        // Route::get('/actualizarTipoAportanteNomina/{id}', 'SayaTerceroController@actualizarTipoAportanteNomina');
        // Route::get('/actualizarTipoIdentificacion/{id}', 'SayaTerceroController@actualizarTipoIdentificacion');
        // Route::get('/actualizarDepartamento/{id}', 'SayaTerceroController@actualizarDepartamento');
        // Route::get('/actualizarDepartamento/{id}', 'SayaTerceroController@actualizarDepartamento');
        // Route::get('/actualizarCiudad/{id}', 'SayaTerceroController@actualizarCiudad');

        // Route::get('respuestacarvajal/{idsDocumento}/{nombreSFTP}/{bd}/{idCompania}','SayaMovimientoFunctions@leerRespuestaCarvajal');
        // Route::get('leerRespuestaCarvajalGeneral','SayaMovimientoFunctions@leerRespuestaCarvajalGeneral');
        // Route::get('descargarArchivosCarvajal/{idsDocumento}/{numeroMovimiento}/{nombreSFTP}/{bd}','SayaMovimientoFunctions@descargarArchivosCarvajal');

        // Route::get('/CambiarColoresSaya/{bd}', 'SayaController@CambiarColoresSaya');
        // Route::get('/CambiarTallasSaya/{bd}', 'SayaController@CambiarTallasSaya');

        
    });
});
