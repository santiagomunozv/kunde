<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('gestionhumana')->group(function () {
    Route::get('/', 'GestionHumanaController@index');
    Route::resource('contratolaboral', 'ContratoLaboralController');
    Route::get('generarcertificadolaboral', 'ContratoLaboralController@indexGenerar');
    Route::get('generarcertificadolaboral/{id}', 'ContratoLaboralController@generarCertificadoLaboral');
    Route::resource('area', 'AreaController');
    Route::resource('publicacion', 'PublicacionController');
    Route::get('consultarimagen/{id}', 'PublicacionController@consultarImagen');
    Route::get('consultarvideo/{id}', 'PublicacionController@consultarVideo');
    Route::post('subirimagen', 'PublicacionController@subirImagen');
    Route::post('subirvideo', 'PublicacionController@subirVideo');
    Route::post('grabarimagenes', 'PublicacionController@storePublicacionImages');
});
