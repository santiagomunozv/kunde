<?php
namespace Modules\GestionHumana\Entities;
use Illuminate\Database\Eloquent\Model;

class PublicacionModel extends Model
{
    protected $table = 'geh_publicacion';
    protected $primaryKey = 'oidPublicacion';
    protected $fillable = ['txTitularPublicacion','dtCreacionPublicacion', 'lsTipoPublicacion', 'dtInicioPublicacion','dtFinPublicacion', 'txContenidoPublicacion'];
    public $timestamps = false;

    public function imagenes(){
        return $this->hasMany('Modules\GestionHumana\Entities\PublicacionImagenModel','Publicacion_oidPublicacion');
    }

}