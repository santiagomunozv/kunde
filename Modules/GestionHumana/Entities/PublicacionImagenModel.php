<?php
namespace Modules\GestionHumana\Entities;
use Illuminate\Database\Eloquent\Model;

class PublicacionImagenModel extends Model
{
    protected $table = 'geh_publicacionimagen';
    protected $primaryKey = 'oidPublicacionImagen';
    protected $fillable = ['Publicacion_oidPublicacion','txRutaPublicacionImagen'];
    public $timestamps = false;

}