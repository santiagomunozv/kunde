<?php
namespace Modules\GestionHumana\Entities;
use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model
{
    protected $table = 'gen_area';
    protected $primaryKey = 'oidArea';
    protected $fillable = ['txCodigoArea','txNombreArea', 'txEstadoArea'];
    public $timestamps = false;

}