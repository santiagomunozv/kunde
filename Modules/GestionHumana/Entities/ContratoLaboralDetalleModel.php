<?php
namespace Modules\GestionHumana\Entities;
use Illuminate\Database\Eloquent\Model;

class ContratoLaboralDetalleModel extends Model
{
    protected $table = 'geh_contratolaboraldetalle';
    protected $primaryKey = 'oidContratoLaboralDetalle';
    protected $fillable = ['ContratoLaboral_oidContratoLaboral','Cargo_oidCargo', 'lsTipoContratoLaboralDetalle', 'inSalarioContratoLaboralDetalle', 'dtInicioContratoLaboralDetalle', 'dtFinContratoLaboralDetalle','lsEstadoContratoLaboralDetalle'];
    public $timestamps = false;

}