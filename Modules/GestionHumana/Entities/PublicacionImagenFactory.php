<?php

namespace Modules\GestionHumana\Entities;

use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Storage;

class PublicacionImagenFactory{

    private static $FINAL_PATH = "publicacion/imagenes/";
    public static $STORAGE = "discoc";
    public static $TMP_DIR = "tmp";

    public static function storeImages(Request $request , $idPublicacion){
        if(isset($request['publicacionImagen'])){
            for($i = 0; $i < count($request['publicacionImagen']); $i++){
                $origen = Storage::disk(self::$TMP_DIR)->path($request['publicacionImagen'][$i]);
                if(file_exists($origen)){
                    $nombre = self::generateFileName($idPublicacion , File::extension($origen));
                    $destino = Storage::disk(self::$STORAGE)->path($nombre);
                    File::move($origen,$destino);
                    $modelo = new PublicacionImagenModel();
                    $modelo->Publicacion_oidPublicacion = $idPublicacion;
                    $modelo->txRutaPublicacionImagen = $nombre;
                    $modelo->save();
                }
            }
        }
    }

    public static function generateFileName($idPublicacion , $extension){
        $uniqId = uniqid();
        return self::$FINAL_PATH."{$idPublicacion}-{$uniqId}.{$extension}";
    }
}