<?php
namespace Modules\GestionHumana\Entities;
use Illuminate\Database\Eloquent\Model;

class ContratoLaboralModel extends Model
{
    protected $table = 'geh_contratolaboral';
    protected $primaryKey = 'oidContratoLaboral';
    protected $fillable = ['Tercero_oidEmpleado','txFuncionesContratoLaboral', 'txClausulasContratoLaboral'];
    public $timestamps = false;

}