@extends("layouts.principal")
@section("nombreModulo")
    Certificado laboral
@endsection
@section('scripts')
    {{Html::script("modules/gestionhumana/js/certificadoLaboralForm.js")}} 
@endsection
@section("contenido")

    {!! Form::model($tercero, ['url' => ["gestionhumana/generarcertificadolaboral"], 'method' => 'POST', 'id' => 'form-generarcertificadolaboral'])
    !!}
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group ">
                        {!!Form::label('txNombreTercero', 'Nombre', array('class' => 'text-md text-primary mb-1')) !!}
                        {!!Form::text('txNombreTercero',null,[ 'class'=>'form-control', 'readonly'])!!}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group ">
                        {!!Form::label('txDocumentoTercero', 'Documento', array('class' => 'text-md text-primary mb-1')) !!}
                        {!!Form::number('txDocumentoTercero',null,['class'=>'form-control', 'readonly'])!!}
                        {!!Form::hidden('oidTercero',Session::get('oidTercero'), array('id' => 'oidTercero'))!!}
                        </div>
                    </div>
                </div>

                {!!Form::button("Generar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"generar()"])!!}
            </div>
        </div>    

    {!! Form::close() !!}
@endsection