@extends("layouts.principal")
@section('nombreModulo')
    Publicación
@endsection
@section('scripts')
    {{ Html::script('modules/gestionhumana/js/publicacionForm.js') }}

@endsection
@section('contenido')
    @php
    ini_set('file_uploads', 'On');
    ini_set('upload_tmp_dir', '/tmp');
    ini_set('upload_max_filesize', 24);
    ini_set('post_max_size', 32);

    @endphp
    @if (isset($publicacion->oidPublicacion))
        {!! Form::model($publicacion, ['route' => ['publicacion.update', $publicacion->oidPublicacion], 'method' => 'PUT', 'id' => 'form-publicacion', 'onsubmit' => 'return false;']) !!}
    @else
        {!! Form::model($publicacion, ['route' => ['publicacion.store', $publicacion->oidPublicacion], 'method' => 'POST', 'id' => 'form-publicacion', 'onsubmit' => 'return false;']) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidPublicacion', null, ['id' => 'oidPublicacion']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txTitularPublicacion', 'Titular', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txTitularPublicacion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el titular']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('lsTipoPublicacion', 'Publicar en:', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select('lsTipoPublicacion', ['Noticia' => 'Noticia', 'Cartelera' => 'Cartelera', 'Sugerencia' => 'Sugerencia'], null, ['class' => 'chosen-select form-control', 'placeholder' => '']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtCreacionPublicacion', 'Fecha de creación', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::date('dtCreacionPublicacion', isset($publicacion->oidPublicacion) ? $publicacion->dtCreacionPublicacion : date('Y-m-d'), ['class' => 'form-control', 'placeholder' => '', 'readonly']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtInicioPublicacion', 'Fecha de inicio', ['class' => 'text-md text-primary mb-1 required']) !!}
                        {!! Form::date('dtInicioPublicacion', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtFinPublicacion', 'Fecha final', ['class' => 'text-md text-primary mb-1 required']) !!}
                        {!! Form::date('dtFinPublicacion', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txContenidoPublicacion', 'Contenido', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::textArea('txContenidoPublicacion', isset($publicacion[0]) ? $publicacion[0]['txContenidoPublicacion'] : null, ['class' => 'form-control', 'style' => 'width:100%', 'placeholder' => '']) !!}
                    </div>
                </div>
            </div>

            <hr>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-header">Imágenes</div>
                <div class="card-body">
                    <div id="imagenes"> {{-- -----DIV DONDE SE AGREGAN LOS INPUT DE LAS IMAGENES------------- --}}
                        <input type="hidden" id="eliminarImagen" name="eliminarImagen" value="">
                    </div>
                    <div class="row">
                        <div class="col-md-12 pb-2">
                            <div id="dropzoneImage" class="dropzone-div">
                                <div class="dz-message">
                                    <div class="col-xs-8">
                                        <div class="message">
                                            <p>Arrastra los elementos aqui o haz click para buscar el archivo.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="fallback">
                                    <input type="file" name="file" multiple style="display: none;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {{ Form::hidden('imagenCatalogo', null, ['id' => 'imagenCatalogo']) }}
                        <div class="col-md-12" style="max-height: 250px;display: flex;overflow: auto;">
                            @if (isset($imagenesContent))
                                @foreach ($imagenesContent as $imagen)
                                    <div class="imagenes m-1" id="imagen_{{ $imagen->oidPublicacionImagen }}">
                                        <span class="btn cerrar"
                                            onclick="eliminarImagen({{ $imagen->oidPublicacionImagen }})">&times;</span>
                                        <img src="/gestionhumana/consultarimagen/{{ $imagen->oidPublicacionImagen }}"
                                            class="rounded" height="200">
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <h1>videos</h1>
                        <div class="col-md-12" style="max-height: 250px;display: flex;overflow: auto;">
                            @if (isset($videosContent))
                                @foreach ($videosContent as $video)
                                    <div class="imagenes m-1" id="imagen_{{ $video->oidPublicacionImagen }}">

                                        <span class="btn cerrar"
                                            onclick="eliminarImagen({{ $video->oidPublicacionImagen }})">
                                            <i class="fas fa-times"></i>
                                        </span>

                                        <video width="320" height="240" controls>
                                            <source
                                                src="/gestionhumana/consultarimagen/{{ $video->oidPublicacionImagen }}"
                                                type=video/ogg>
                                        </video>

                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            @if (isset($publicacion->oidPublicacion))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
