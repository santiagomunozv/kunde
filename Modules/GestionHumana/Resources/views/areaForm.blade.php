@extends("layouts.principal")
@section("nombreModulo")
    Área
@endsection
@section("scripts")
    {{Html::script("modules/gestionhumana/js/areaForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($area->oidArea))
        {!!Form::model($area,["route"=>["area.update",$area->oidArea],"method"=>"PUT", "id"=>"form-area" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($area,["route"=>["area.store",$area->oidArea],"method"=>"POST", "id"=>"form-area", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidArea', null, array('id' => 'oidArea')) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoArea', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoArea',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el código'])!!}
                    </div>
                </div>
		
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreArea', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreArea',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txEstadoArea', 'Estado', array('class' => 'text-md text-primary mb-1  required ')) !!}
                        {!!Form::select('txEstadoArea',['Activo' => 'Activo','Inactivo' => 'Inactivo'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona el estado del área'])!!}                    
                    </div>
                </div>   
			</div>
            @if(isset($area->oidArea))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        