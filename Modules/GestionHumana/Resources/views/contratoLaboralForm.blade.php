@extends('layouts.principal')

@section('nombreModulo')
    Contrato laboral
@endsection

@section('scripts')
<script>
    let contratoLaboralDetalles = '<?php echo json_encode($contratoLaboralDetalle); ?>';
    let idCargo = '<?php echo json_encode($idCargo); ?>';
    let nombreCargo = '<?php echo json_encode($nombreCargo); ?>'; 

</script>
    {{Html::script("modules/gestionhumana/js/contratoLaboralForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($contratoLaboral) --}}
    @if(isset($contratoLaboral->oidContratoLaboral))
        {!!Form::model($contratoLaboral,["route"=>["contratolaboral.update",$contratoLaboral->oidContratoLaboral],"method"=>"PUT", "id"=>"form-contratoLaboral" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($contratoLaboral,["route"=>["contratolaboral.store",$contratoLaboral->oidContratoLaboral],"method"=>"POST", "id"=>"form-contratoLaboral", "onsubmit" => "return false;"])!!}
    @endif

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidEmpleado', 'Empleado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidEmpleado',$asn_tercerolistaEmpleado,(isset($contratoLaboral[0]) ? $contratoLaboral[0]["Tercero_oidEmpleado"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el Empleado'])!!}
                    </div>
                </div>
            </div>   
        </div>  
    </div> 

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarcontratoLaboralDetalle" name="eliminarcontratoLaboralDetalle" value="">
                        <div class="div card-body multi-max" style="overflow: auto">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracioncontratoLaboralDetalle.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Cargo</th>
                                <th>Tipo</th>
                                <th>Salario</th>
                                <th>Inicio</th>
                                <th>Fin</th>
                                <th>Estado</th>
                                <tbody id="contenedorcontratoLaboralDetalle">
                                </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
          
            
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group ">
                            {!!Form::label('txFuncionesContratoLaboral', 'Funciones', array('class' => 'text-md text-primary mb-1')) !!}
                            {!!Form::textArea('txFuncionesContratoLaboral',(isset($contratoLaboral[0]) ? $contratoLaboral[0]["txFuncionesContratoLaboral"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>''])!!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group ">
                            {!!Form::label('txClausulasContratoLaboral', 'Cláusulas', array('class' => 'text-md text-primary mb-1')) !!}
                            {!!Form::textArea('txClausulasContratoLaboral',(isset($contratoLaboral[0]) ? $contratoLaboral[0]["txClausulasContratoLaboral"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>''])!!}
                        </div>
                    </div>
                </div>


            @if(isset($contratoLaboral->oidContratoLaboral))
            {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        @else
            {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
        @endif
        {!! Form::close() !!}
    </div>
</div>    
@endsection