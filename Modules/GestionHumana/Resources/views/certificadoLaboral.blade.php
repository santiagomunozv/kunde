@extends('gestionhumana::layouts.master')

@section('content')
    @php
        setlocale(LC_TIME, 'es_ES.UTF-8');
        if ($contrato->dtFinContratoLaboralDetalle && date('Y-m-d') > $contrato->dtFinContratoLaboralDetalle) {
            $antiguedad =
                'laboró para esta compañía desde el ' .
                strftime('%d de %B del %Y', strtotime($contrato->dtInicioContratoLaboralDetalle)) .
                ' hasta el ' .
                strftime('%d de %B del %Y', strtotime($contrato->dtFinContratoLaboralDetalle));
        } elseif ($contrato->dtFinContratoLaboralDetalle && date('Y-m-d') <= $contrato->dtFinContratoLaboralDetalle) {
            $antiguedad =
                'labora para esta compañía desde el ' .
                strftime('%d de %B del %Y', strtotime($contrato->dtInicioContratoLaboralDetalle)) .
                ' hasta el ' .
                strftime('%d de %B del %Y', strtotime($contrato->dtFinContratoLaboralDetalle));
        } else {
            $antiguedad =
                'labora para esta compañía desde el ' .
                strftime('%d de %B del %Y', strtotime($contrato->dtInicioContratoLaboralDetalle));
        }
    @endphp
    <img src={{ public_path('/images/LogoKunde.jpeg') }} style="height: 4cm; width: 4cm;">

    <p>Medellín, {{ strftime('%B', mktime(0, 0, 0, date('m'), 1, 2000)) }} {{ date('d') }} de {{ date('Y') }}</p>
    <br><br>

    <p style="text-align:center;"><strong>KUNDE S.A.S</p>
    <p style="text-align:center;"><strong>NIT:</strong> 901.093.574-2</p>
    <br>
    <p>CERTIFICA</p>
    <br>

    <p>Que el señor(a) {{ $contrato->txNombreTercero }} identificado(a) con CC
        {{ number_format($contrato->txDocumentoTercero, 0, '', '.') }}
        {{ $antiguedad }}, en el cargo de {{ $contrato->txNombreCargo }} bajo un contrato a término:
        {{ $contrato->lsTipoContratoLaboralDetalle }}.</p>

    <br>
    <p>Se expide este certificación a solicitud del interesado.</p>
    <br><br>

    <p>Atentamente,</p>
    <br>

    <img src={{ public_path('/images/FirmaCartaLaboral.jpg') }} style="height: 2cm; width: 4cm;"><br>
    Billi Joel Gallego Mejía<br>
    Representante legal<br>
    Tel: 3016921080<br>

    <br><br><br><br><br><br>
    <p style="text-align:center;">Kunde S.A.S, CL 53 47 44 - Medellín<br>
        Tel: 3167589829, direccion@consultorakunde.com<br></p>
@stop
