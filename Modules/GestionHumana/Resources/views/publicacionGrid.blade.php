@extends('layouts.principal')
@section('nombreModulo')
    Publicaciones
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("publicacion-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="publicacion-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/gestionhumana/publicacion', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Titular</th>
                            <th>Fecha de creación</th>
                            <th>Publicar en:</th>
                            <th>Fecha de inicio</th>
                            <th>Fecha final</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($publicacion as $publicacion)
                            <tr>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/gestionhumana/publicacion', [$publicacion->oidPublicacion, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button type="button" class="btn btn-danger btn-sm"
                                                onclick="confirmarEliminacion('{{ $publicacion->oidPublicacion }}', 'geh_publicacion', 'Publicacion')"
                                                title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $publicacion->oidPublicacion }}</td>
                                <td>{{ $publicacion->txTitularPublicacion }}</td>
                                <td>{{ $publicacion->dtCreacionPublicacion }}</td>
                                <td>{{ $publicacion->lsTipoPublicacion }}</td>
                                <td>{{ $publicacion->dtInicioPublicacion }}</td>
                                <td>{{ $publicacion->dtFinPublicacion }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Id</th>
                            <th>Titular</th>
                            <th>Fecha de creación</th>
                            <th>Publicar en:</th>
                            <th>Fecha de inicio</th>
                            <th>Fecha final</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
