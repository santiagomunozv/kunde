"use strict";
var errorClass = "is-invalid";


$(function () {
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
})

// ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
let token = $('meta[name="csrf-token"]').attr('content');
var myDropzone = new Dropzone("div#dropzoneImage", {
    headers: { 'X-CSRF-TOKEN': token },
    url: "/gestionhumana/subirimagen",
    addRemoveLinks: true,
    acceptedFiles: "image/*, video/*",
    maxFilesize: 500,
    init: function () {
        this.on("success", function (file) {
            let pathImage = JSON.parse(file.xhr.response);
            $("#imagenes").append("<input type='hidden' name='publicacionImagen[]' value='" + pathImage.tmpPath + "'>");
        });
        this.on("removedfile", function (file) {
            let pathImage = JSON.parse(file.xhr.response);
            $('input[name="publicacionImagen[]"][value="' + pathImage.tmpPath + '"]').remove();
        });
    }
});

function eliminarImagen(id) {
    $("#eliminarImagen").val($("#eliminarImagen").val() + id + ",");
    $("#imagen_" + id).remove();
}

function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-publicacion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                location.href = "/gestionhumana/publicacion";
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {

        let mensajes = [];
        let txTitularPublicacion_Input = $("#txTitularPublicacion");
        let txTitularPublicacion_AE = txTitularPublicacion_Input.val();
        txTitularPublicacion_Input.removeClass(errorClass);
        if (!txTitularPublicacion_AE) {
            txTitularPublicacion_Input.addClass(errorClass)
            mensajes.push("El campo Titular es obligatorio");
        }
        let lsTipoPublicacion_Input = $("#lsTipoPublicacion");
        let lsTipoPublicacion_AE = lsTipoPublicacion_Input.val();
        lsTipoPublicacion_Input.removeClass(errorClass);
        if (!lsTipoPublicacion_AE) {
            lsTipoPublicacion_Input.addClass(errorClass)
            mensajes.push("El campo -Publicar en- es obligatorio");
        }
        let dtInicioPublicacion_Input = $("#dtInicioPublicacion");
        let dtInicioPublicacion_AE = dtInicioPublicacion_Input.val();
        dtInicioPublicacion_Input.removeClass(errorClass);
        if (!dtInicioPublicacion_AE) {
            dtInicioPublicacion_Input.addClass(errorClass)
            mensajes.push("El campo Fecha de inicio es obligatorio");
        }
        let dtFinPublicacion_Input = $("#dtFinPublicacion");
        let dtFinPublicacion_AE = dtFinPublicacion_Input.val();
        dtFinPublicacion_Input.removeClass(errorClass);
        if (!dtFinPublicacion_AE) {
            dtFinPublicacion_Input.addClass(errorClass)
            mensajes.push("El campo Fecha final es obligatorio");
        }
        return mensajes;
    }
}