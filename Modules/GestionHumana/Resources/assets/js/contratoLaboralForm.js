"use strict";
var errorClass = "is-invalid";
var configuracionContratoLaboralDetalles = JSON.parse(contratoLaboralDetalles);
console.log(contratoLaboralDetalles, configuracionContratoLaboralDetalles);
var configuracioncontratoLaboralDetalle = [];


$(function () {
    //generamos la multiregistro de grupo
    configuracioncontratoLaboralDetalle = new GeneradorMultiRegistro('configuracioncontratoLaboralDetalle', 'contenedorcontratoLaboralDetalle', 'configuracioncontratoLaboralDetalle');

    let optionsTipo = [['Indefinido', 'Prestación de servicios', 'Fijo', 'Aprendizaje', 'Temporal'], ['Indefinido', 'Prestación de servicios', 'Fijo', 'Aprendizaje', 'Temporal']];
    let optionsEstado = [['Activo', 'Inactivo'], ['Activo', 'Inactivo']];
    let optionsCargo = [JSON.parse(idCargo), JSON.parse(nombreCargo)];


    configuracioncontratoLaboralDetalle.campoid = 'oidContratoLaboralDetalle';
    configuracioncontratoLaboralDetalle.campoEliminacion = 'eliminarcontratoLaboralDetalle';
    configuracioncontratoLaboralDetalle.botonEliminacion = true;
    configuracioncontratoLaboralDetalle.funcionEliminacion = '';
    configuracioncontratoLaboralDetalle.campos = ['oidContratoLaboralDetalle', 'Cargo_oidCargo', 'lsTipoContratoLaboralDetalle', 'inSalarioContratoLaboralDetalle', 'dtInicioContratoLaboralDetalle', 'dtFinContratoLaboralDetalle', 'lsEstadoContratoLaboralDetalle'];
    configuracioncontratoLaboralDetalle.etiqueta = ['input', 'select', 'select', 'input', 'input', 'input', 'select'];
    configuracioncontratoLaboralDetalle.tipo = ['hidden', 'select', 'select', 'number', 'date', 'date', 'select'];
    configuracioncontratoLaboralDetalle.estilo = ['', '', '', '', '', '', ''];
    configuracioncontratoLaboralDetalle.clase = ['', '', '', '', '', '', ''];
    configuracioncontratoLaboralDetalle.sololectura = [true, false, false, false, false, false, false];
    configuracioncontratoLaboralDetalle.opciones = ['', optionsCargo, optionsTipo, '', '', '', optionsEstado];
    configuracioncontratoLaboralDetalle.funciones = ['', '', '', '', '', '', ''];
    configuracioncontratoLaboralDetalle.otrosAtributos = ['', '', '', '', '', '', ''];

    console.log(configuracionContratoLaboralDetalles)

    configuracionContratoLaboralDetalles.forEach(dato => {
        configuracioncontratoLaboralDetalle.agregarCampos(dato, 'L');
    });

    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-contratoLaboral";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            console.log(resp)
            modal.establecerAccionCerrar(function () {
                location.href = "/gestionhumana/contratolaboral";
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {

        let mensajes = [];
        let Tercero_oidEmpleado_Input = $("#Tercero_oidEmpleado");
        let Tercero_oidEmpleado_AE = Tercero_oidEmpleado_Input.val();
        Tercero_oidEmpleado_Input.removeClass(errorClass);
        if (!Tercero_oidEmpleado_AE) {
            Tercero_oidEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Empleado es obligatorio");
        }
        return mensajes;

    }
}