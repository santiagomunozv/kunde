"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-area";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/gestionhumana/area";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoArea_Input = $("#txCodigoArea");
        let txCodigoArea_AE = txCodigoArea_Input.val();
        txCodigoArea_Input.removeClass(errorClass);
        if(!txCodigoArea_AE){
            txCodigoArea_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreArea_Input = $("#txNombreArea");
        let txNombreArea_AE = txNombreArea_Input.val();
        txNombreArea_Input.removeClass(errorClass);
        if(!txNombreArea_AE){
            txNombreArea_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoArea_Input = $("#txEstadoArea");
        let txEstadoArea_AE = txEstadoArea_Input.val();
        txEstadoArea_Input.removeClass(errorClass);
        if(!txEstadoArea_AE){
            txEstadoArea_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}