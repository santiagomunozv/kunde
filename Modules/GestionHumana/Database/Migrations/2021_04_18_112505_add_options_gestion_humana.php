<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddOptionsGestionHumana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('seg_modulo')->insert([
            "oidModulo" => 22,
            "Paquete_oidPaquete_1aM" => 1,
            "txNombreModulo" => "Gestión humana",
            "txIconoModulo" => "fas fa-id-card"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>22,
            "txNombreOpcion" =>"Contratos laborales",
            "txRutaOpcion" => "/gestionhumana/contratolaboral"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 
    }
}
