<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePublicacionImagen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geh_publicacionimagen', function (Blueprint $table) {
            $table->increments('oidPublicacionImagen');
            $table->integer('Publicacion_oidPublicacion')->comment('Id de publicación');
            $table->string('txRutaPublicacionImagen')->comment('Ruta de imagen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geh_publicacionimagen');
    }
}
