<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableContratosLaboralesDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geh_contratolaboraldetalle', function (Blueprint $table) {
            $table->increments('oidContratoLaboralDetalle');
            $table->integer('ContratoLaboral_oidContratoLaboral')->comment('Id Contrato Laboral');
            $table->integer('Cargo_oidCargo')->comment('Id Cargo');
            $table->string('lsTipoContratoLaboralDetalle')->comment('Tipo contrato');
            $table->decimal('inSalarioContratoLaboralDetalle', 18, 2)->comment('Salario cargo');
            $table->date('dtInicioContratoLaboralDetalle')->comment('Inicio contrato');
            $table->date('dtFinContratoLaboralDetalle')->nullable()->comment('Fin contrato');
            $table->string('lsEstadoContratoLaboralDetalle')->comment('Estado contrato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geh_contratolaboraldetalle');
    }
}
