<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePublicacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geh_publicacion', function (Blueprint $table) {
            $table->increments('oidPublicacion');
            $table->string('txTitularPublicacion')->comment('Id publicacion');
            $table->date('dtCreacionPublicacion')->comment('Fecha creación');
            $table->string('lsTipoPublicacion')->comment('Tipo publicación');
            $table->date('dtInicioPublicacion')->comment('Inicio publicación');
            $table->date('dtFinPublicacion')->nullable()->comment('Fin publicación');
            $table->text('txContenidoPublicacion')->comment('Contenido publicación');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>22,
            "txNombreOpcion" =>"Publicaciones",
            "txRutaOpcion" => "/gestionhumana/publicacion"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geh_publicacion');
    }
}
