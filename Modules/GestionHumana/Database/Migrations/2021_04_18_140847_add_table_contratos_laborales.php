<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableContratosLaborales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geh_contratolaboral', function (Blueprint $table) {
            $table->increments('oidContratoLaboral');
            $table->integer('Tercero_oidEmpleado')->comment('Id empleado');
            $table->text('txFuncionesContratoLaboral')->nullable()->comment('Funciones contrato');
            $table->text('txClausulasContratoLaboral')->nullable()->comment('Clausulas contrato');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geh_contratolaboral');
    }
}
