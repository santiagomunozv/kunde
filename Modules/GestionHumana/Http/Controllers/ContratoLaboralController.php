<?php

namespace Modules\GestionHumana\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\CargoModel;
use Modules\GestionHumana\Entities\ContratoLaboralDetalleModel;
use Modules\GestionHumana\Entities\ContratoLaboralModel;
use Modules\GestionHumana\Http\Requests\ContratoLaboralRequest;

class ContratoLaboralController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $contratoLaboral = ContratoLaboralModel::select('oidContratoLaboral', 'txNombreTercero')->join('asn_tercero','geh_contratolaboral.Tercero_oidEmpleado','asn_tercero.oidTercero')->orderBy('oidContratoLaboral' , 'DESC')->get();
        return view('gestionhumana::contratoLaboralGrid', compact('permisos' , 'contratoLaboral'));
    }

    public function indexGenerar(){
        $tercero = TerceroModel::where("oidTercero", Session::get('oidTercero'))->first();
        return view ('gestionhumana::generarCertificadoLaboralForm', compact('tercero'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $contratoLaboral = new ContratoLaboralModel();
        $contratoLaboralDetalle = new ContratoLaboralDetalleModel();
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
        ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
        ->where('asn_tercero.txEstadoTercero','=', 'Activo')
        ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Emp')
        ->pluck('txNombreTercero','oidTercero');
        $idCargo = CargoModel::pluck('oidCargo');

        $nombreCargo = CargoModel::pluck('txNombreCargo');
        return view('gestionhumana::contratoLaboralForm', compact('contratoLaboral', 'asn_tercerolistaEmpleado', 'contratoLaboralDetalle', 'idCargo', 'nombreCargo'),['contratoLaboral' => $contratoLaboral] );

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ContratoLaboralRequest $request)
    {
        DB::beginTransaction();
        try{
            $contratoLaboral = ContratoLaboralModel::create($request->all());

            $modelo = new ContratoLaboralDetalleModel();
            $total = ($request['oidContratoLaboralDetalle'] !== null ) ? count($request['oidContratoLaboralDetalle']) : 0;
            for($i = 0; $i <  $total; $i++)
            {
                $indice = array('oidContratoLaboralDetalle' => $request['oidContratoLaboralDetalle'][$i]);
                $datos = [
                        'Cargo_oidCargo'=>$request["Cargo_oidCargo"][$i],
                        'lsTipoContratoLaboralDetalle'=>$request["lsTipoContratoLaboralDetalle"][$i],
                        'inSalarioContratoLaboralDetalle'=>$request["inSalarioContratoLaboralDetalle"][$i],
                        'dtInicioContratoLaboralDetalle'=>$request["dtInicioContratoLaboralDetalle"][$i],
                        'dtFinContratoLaboralDetalle'=>$request["dtFinContratoLaboralDetalle"][$i],
                        'lsEstadoContratoLaboralDetalle'=>$request["lsEstadoContratoLaboralDetalle"][$i],
                        'ContratoLaboral_oidContratoLaboral'=> $contratoLaboral->oidContratoLaboral,
                ];
                $modelo::updateOrCreate($indice, $datos);
            }
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidContratoLaboral' => $contratoLaboral->oidContratoLaboral] , 201);
        }
        catch(\Exception $e){
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('contratoLaboral::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $contratoLaboral = ContratoLaboralModel::find($id);
        $contratoLaboralDetalle = ContratoLaboralDetalleModel::where('ContratoLaboral_oidContratoLaboral', '=', $id)->get();
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
        ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
        ->where('asn_tercero.txEstadoTercero','=', 'Activo')
        ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Emp')
        ->pluck('txNombreTercero','oidTercero');
        $idCargo = CargoModel::pluck('oidCargo');

        $nombreCargo = CargoModel::pluck('txNombreCargo');

        
        return view('gestionhumana::contratoLaboralForm', compact('contratoLaboral', 'asn_tercerolistaEmpleado', 'contratoLaboralDetalle', 'idCargo', 'nombreCargo'), ['contratoLaboral' => $contratoLaboral] );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ContratoLaboralRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $contratoLaboral = ContratoLaboralModel::find($id);
            $contratoLaboral->fill($request->all());
            $contratoLaboral->save();

            $modelo = new ContratoLaboralDetalleModel();
            $idsEliminar = explode(',', $request['eliminarcontratoLaboralDetalle']);
            $modelo::whereIn('oidContratoLaboralDetalle',$idsEliminar)->delete();
            $total = ($request['oidContratoLaboralDetalle'] !== null ) ? count($request['oidContratoLaboralDetalle']) : 0;
            for($i = 0; $i <  $total; $i++)
            {
                $indice = array('oidContratoLaboralDetalle' => $request['oidContratoLaboralDetalle'][$i]);
                $datos = [
                    'Cargo_oidCargo'=>$request["Cargo_oidCargo"][$i],
                    'lsTipoContratoLaboralDetalle'=>$request["lsTipoContratoLaboralDetalle"][$i],
                    'inSalarioContratoLaboralDetalle'=>$request["inSalarioContratoLaboralDetalle"][$i],
                    'dtInicioContratoLaboralDetalle'=>$request["dtInicioContratoLaboralDetalle"][$i],
                    'dtFinContratoLaboralDetalle'=>$request["dtFinContratoLaboralDetalle"][$i],
                    'lsEstadoContratoLaboralDetalle'=>$request["lsEstadoContratoLaboralDetalle"][$i],
                    'ContratoLaboral_oidContratoLaboral'=> $id,
                ];
                $modelo::updateOrCreate($indice, $datos);
            }
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidContratoLaboral' => $contratoLaboral->contratoLaboral] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
    
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        ContratoLaboralModel::destroy($id); 
        return redirect('/gestionhumana/contratolaboral');
    }

    public function generarCertificadoLaboral($idTercero, Request $request){
        $contrato = ContratoLaboralModel::from('geh_contratolaboral as cl')
        ->join('geh_contratolaboraldetalle as cld', 'cl.oidContratoLaboral', 'cld.ContratoLaboral_oidContratoLaboral')
        ->join('asn_tercero as t', 'cl.Tercero_oidEmpleado', 't.oidTercero')
        ->join('gen_cargo as c', 'cld.Cargo_oidCargo', 'c.oidCargo')
        ->where('cl.Tercero_oidEmpleado', $idTercero)
        ->where('cld.lsEstadoContratoLaboralDetalle', 'Activo')
        ->first();

        if(!$contrato){
            echo 'No está configurado el contrato laboral para este empleado.';
            return;
        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('gestionhumana::certificadoLaboral', ['contrato' => $contrato]);
        return $pdf->stream();
    }
}
