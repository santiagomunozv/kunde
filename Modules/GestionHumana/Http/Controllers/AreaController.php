<?php

namespace Modules\GestionHumana\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Modules\GestionHumana\Entities\AreaModel;
use Modules\GestionHumana\Http\Requests\AreaRequest;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $area = AreaModel::select("oidArea", "txCodigoArea", "txNombreArea", "txEstadoArea")->orderBy('oidArea' , 'DESC')->get();
        return view('gestionhumana::areaGrid', compact('permisos' , 'area'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $area = new AreaModel();
        return view('gestionhumana::areaForm', ['area' => $area] );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(AreaRequest $request)
    {
        DB::beginTransaction();
        try{
            $area = AreaModel::create($request->all());
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidArea' => $area->oidArea] , 201);
        }
        catch(\Exception $e){
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('area::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $area = AreaModel::find($id);
        
        return view('gestionhumana::areaForm' , ['area' => $area] );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(AreaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $area = AreaModel::find($id);
            $area->fill($request->all());
            $area->save();
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidArea' => $area->oidArea] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
    
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        AreaModel::destroy($id); 
        return redirect('/gestionhumana/area');
    }
}
