<?php

namespace Modules\GestionHumana\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Modules\GestionHumana\Entities\PublicacionImagenFactory;
use Modules\GestionHumana\Entities\PublicacionImagenModel;
use Modules\GestionHumana\Entities\PublicacionModel;
use Modules\GestionHumana\Http\Requests\PublicacionRequest;
use Storage;

class PublicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $publicacion = PublicacionModel::select("oidPublicacion", "txTitularPublicacion", "dtCreacionPublicacion", "lsTipoPublicacion", "dtInicioPublicacion", "dtFinPublicacion", "txContenidoPublicacion")->orderBy('oidPublicacion', 'DESC')->get();
        return view('gestionhumana::publicacionGrid', compact('permisos', 'publicacion'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $publicacion = new PublicacionModel();
        return view('gestionhumana::publicacionForm', ['publicacion' => $publicacion]);
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(PublicacionRequest $request)
    {
        DB::beginTransaction();
        try {
            $publicacion = PublicacionModel::create($request->all());
            $this->grabarImagenes($request, $publicacion->oidPublicacion);
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPublicacion' => $publicacion->oidPublicacion], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('publicacion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $publicacion = PublicacionModel::find($id);
        $imagenes = $publicacion->imagenes;

        // categorizar archivos
        $videosContent = [];
        $imagenesContent = [];
        $extencionesVideosAudios = ['mp3', 'mp4', 'avi', 'flv'];
        foreach ($imagenes as $key => $archivo) {
            //optener la extencion del archivo
            $extension = pathinfo($archivo->txRutaPublicacionImagen, PATHINFO_EXTENSION);
            // validar si es un video/audio o imagen
            for ($i = 0; $i < count($extencionesVideosAudios); $i++) {
                $suiche = false;
                if ($extencionesVideosAudios[$i] === $extension) {
                    array_push($videosContent, $archivo);
                    $suiche = true;
                    break;
                }
            }
            if (!$suiche) {
                array_push($imagenesContent, $archivo);
            }
        }
        return view('gestionhumana::publicacionForm', compact('imagenes', 'videosContent', 'imagenesContent'), ['publicacion' => $publicacion]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(PublicacionRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $publicacion = PublicacionModel::find($id);
            $publicacion->fill($request->all());
            $this->grabarImagenes($request, $publicacion->oidPublicacion);
            $publicacion->save();
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPublicacion' => $publicacion->oidPublicacion], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        PublicacionModel::destroy($id);
        return redirect('/gestionhumana/publicacion');
    }

    public function storePublicacionImages(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->grabarImagenes($request, $request->get("idPublicacion"));
            DB::commit();
            return response([], 200);
        } catch (\Exception $e) {
            DB::rollBack();
            return abort(500, $e->getMessage());
        }
    }

    protected function grabarImagenes($request, $idPadre)
    {

        $datosEliminar = substr($request['eliminarImagen'], 0, strlen($request['eliminarImagen']) - 1);
        if (strlen($datosEliminar) > 0) {
            foreach (explode(',', $datosEliminar) as $idEliminar) {
                $this->eliminarImagen($idEliminar);
                PublicacionImagenModel::where('oidPublicacionImagen', $idEliminar)->delete();
            }
        }

        PublicacionImagenFactory::storeImages($request, $idPadre);
    }

    public function subirImagen(Request $request)
    {
        $imagen = $request->file('file');
        $path = Storage::disk('tmp')->putFile('publicacion/imagenes', $imagen);
        return response(['tmpPath' => $path], 200);
    }

    public function consultarImagen($id)
    {

        $imagen = PublicacionImagenModel::find($id);
        $path = Storage::disk('discoc')->path($imagen->txRutaPublicacionImagen);
        if (file_exists($path)) {
            return response()->file($path);
        } else {
            return response()->file(public_path('images/FichaImagePlaceHolder.png'));
        }
    }

    public function eliminarImagen($id)
    {
        $imagen = PublicacionImagenModel::find($id);
        $path = Storage::disk('discoc')->path($imagen->txRutaPublicacionImagen);
        if (file_exists($path)) {
            Storage::disk('discoc')->delete($imagen->txRutaPublicacionImagen);
        }
    }
}
