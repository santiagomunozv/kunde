<?php

    namespace Modules\GestionHumana\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class PublicacionRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "txTitularPublicacion" => "required",
            "lsTipoPublicacion" => "required",
            "dtInicioPublicacion" => "required",
            "dtFinPublicacion" => "required|after_or_equal:dtInicioPublicacion",
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["txTitularPublicacion.required"] =  "El campo Titular es obligatorio";
                $mensaje["lsTipoPublicacion.required"] = "El campo -Publicar en- es obligatorio";
                $mensaje["dtInicioPublicacion.required"] =  "El campo Fecha de inicio es obligatorio";
                $mensaje["dtFinPublicacion.required"] =  "El campo Fecha final es obligatorio";
                $mensaje["dtFinPublicacion.after_or_equal"] =  "La fecha final no puede ser inferior a la fecha inicial";

            return $mensaje;
        }
    }
        