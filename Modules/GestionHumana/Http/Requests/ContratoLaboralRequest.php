<?php

    namespace Modules\GestionHumana\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ContratoLaboralRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "Tercero_oidEmpleado" => "required|",Rule::unique('geh_contratolaboral')->ignore($this->oidContratoLaboral),
            'Cargo_oidCargo.*' => 'required',
            'lsTipoContratoLaboralDetalle.*' => 'required',
            'inSalarioContratoLaboralDetalle.*' => 'required|numeric',
            'dtInicioContratoLaboralDetalle.*' => 'required|date',
            // 'dtFinContratoLaboralDetalle.*' => 'required|date',
            'lsEstadoContratoLaboralDetalle.*' => 'required',
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
            $mensaje["Tercero_oidEmpleado.required"] =  "El Empleado es obligatorio";
            $mensaje["Tercero_oidEmpleado.unique"] =  "El Empleado ya se encuentra registrado";

            $mensaje = array();
            $total = ($this->get("oidContratoLaboralDetalle") !== null ) ? count($this->get("oidContratoLaboralDetalle")) : 0;
            for($j=0; $j < $total; $j++)
            {
                    $mensaje['Cargo_oidCargo.'.$j.'.required'] = "El campo Cargo es obligatorio en el registro ".($j+1);
                    $mensaje['lsTipoContratoLaboralDetalle.'.$j.'.required'] = "El campo Tipo de contrato es obligatorio en el registro ".($j+1);
                    $mensaje['inSalarioContratoLaboralDetalle.'.$j.'.required'] = "El campo Salario es obligatorio en el registro ".($j+1);
                    $mensaje['dtInicioContratoLaboralDetalle.'.$j.'.date'] = "El campo Inicio debe ser tipo fecha en el registro ".($j+1);
                    // $mensaje['dtFinContratoLaboralDetalle.'.$j.'.date'] = "El campo Fin debe ser tipo fecha en el registro ".($j+1);
                    $mensaje['lsEstadoContratoLaboralDetalle.'.$j.'.required'] = "El campo Estado es obligatorio en el registro ".($j+1);
            }
    

            return $mensaje;
        }
    }
        