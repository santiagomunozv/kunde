<?php

    namespace Modules\GestionHumana\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class AreaRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "txCodigoArea" => "required",
            "txNombreArea" => "required|string|",
            "txEstadoArea" => "required",
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["txCodigoArea.required"] =  "El campo Código es obligatorio";
                $mensaje["txNombreArea.string"] = "El campo Nombre sólo puede contener caracteres afabéticos";
                $mensaje["txNombreArea.required"] =  "El campo Nombre es obligatorio";
                $mensaje["txEstadoArea.required"] =  "El campo Estado es obligatorio";

            return $mensaje;
        }
    }
        