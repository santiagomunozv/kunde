"use strict";
var configuracionCarpetas = JSON.parse(carpetas);
var configuracionCarpeta = [];

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };
  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }
});

document.getElementById("btnGenerar").addEventListener("click", function (event) {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-diseno";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();

    $.post(route, data, function (resp) {
      modal.establecerAccionCerrar(function () {});
      modal.mostrarModal(
        "Informacion",
        '<div class="alert alert-success">El archivo ha sido generado correctamente</div>',
        function () {
          location.reload();
        }
      );
    }).fail(function (resp, estado) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {
    let mensajes = [];
    let errorClass = "is-invalid";

    let documento = $("#Documento_iodDocumento").serialize();
    if (!documento) {
      $("#Documento_iodDocumento_chosen").addClass(errorClass);
      mensajes.push("Debes seleccionar almenos un Documento");
    }

    let Empleado_oidEmpleado_Input = $("#Empleado_oidEmpleado");
    let Empleado_oidEmpleado_AE = Empleado_oidEmpleado_Input.val();
    Empleado_oidEmpleado_Input.removeClass(errorClass);
    if (!Empleado_oidEmpleado_AE) {
      Empleado_oidEmpleado_Input.addClass(errorClass);
      mensajes.push("ebes seleccionar un Empleado");
    }

    let Cliente_oidCliente_Input = $("#Cliente_oidCliente");
    let Cliente_oidCliente_AE = Cliente_oidCliente_Input.val();
    Cliente_oidCliente_Input.removeClass(errorClass);
    if (!Cliente_oidCliente_AE) {
      Cliente_oidCliente_Input.addClass(errorClass);
      mensajes.push("ebes seleccionar un Cliente");
    }

    if (mensajes.length > 0) {
      event.preventDefault();
    }

    return mensajes;
  }
});




$(function(){
    //generamos la multiregistro de grupo
    configuracionCarpeta = new GeneradorMultiRegistro('configuracionCarpeta','contenedorconfiguracionCarpeta','configuracionCarpeta');
  console.log(configuracionCarpetas);
    configuracionCarpeta.campoid = 'carpetaId';
    configuracionCarpeta.campoEliminacion = 'eliminarConfiguracion';
    configuracionCarpeta.botonEliminacion = false;
    configuracionCarpeta.funcionEliminacion = '';  
    configuracionCarpeta.campos = ['carpetaId','carpetaName','carpetaOrden'];
    configuracionCarpeta.etiqueta = ['input','input','input'];
    configuracionCarpeta.tipo = ['hidden','text','number'];
    configuracionCarpeta.estilo = ['','','',''];
    configuracionCarpeta.clase = ['','','',''];
    configuracionCarpeta.sololectura = [true,true,true,false];
    configuracionCarpeta.opciones = ['','','',''];
    configuracionCarpeta.funciones = ['','','',''];
    configuracionCarpeta.otrosAtributos = ['','','',''];


    configuracionCarpetas.forEach( dato => {configuracionCarpeta.agregarCampos(dato , 'L');
    });
});
