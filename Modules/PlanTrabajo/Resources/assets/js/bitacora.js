function grabarBitacora(){
    modal.cargando();
    let mensajes = []; //validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-bitacora";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            modal.mostrarErrores(mensajes);
        });
    }
}

function imprimirBitacora(){
    let oidBitacora = document.getElementById('oidEjecucionPlanTrabajoBitacora').value;
    window.open('http://'+location.host+'/plantrabajo/generarbitacora/'+oidBitacora, '_blank','width=2500px, height=700px, scrollbars=yes');
}