"use strict";
var errorClass = "is-invalid";
var configuracionDetalles = JSON.parse(detalle);
var configuracionDetalle = [];

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };
  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  //generamos la multiregistro de grupo
  configuracionDetalle = new GeneradorMultiRegistro('configuracionDetalle', 'contenedorConfiguracionDetalle', 'configuracionDetalle_');

  let options = [['txNombreResponsableSST', 'txDocumentoResponsableSST'], ['Nombre responsable', 'Documento responsable']]

  configuracionDetalle.campoid = 'oidDetalle';
  configuracionDetalle.campoEliminacion = 'eliminarDetalle';
  configuracionDetalle.botonEliminacion = true;
  configuracionDetalle.funcionEliminacion = '';
  configuracionDetalle.campos = ['oidDetalle', 'txCampoBDDetalle', 'txCampoFormDetalle'];
  configuracionDetalle.etiqueta = ['input', 'select', 'input'];
  configuracionDetalle.tipo = ['hidden', '', 'text'];
  configuracionDetalle.estilo = ['', '', ''];
  configuracionDetalle.clase = ['', '', ''];
  configuracionDetalle.sololectura = [true, false, false];
  configuracionDetalle.opciones = ['', options, ''];
  configuracionDetalle.funciones = ['', '', ''];
  configuracionDetalle.otrosAtributos = ['', '', ''];


  configuracionDetalles.forEach(dato => {
    configuracionDetalle.agregarCampos(dato, 'L');
  });
});

function generardiseño() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-reporteministerio";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(route, data, function (resp) {
      console.log(resp)
      modal.establecerAccionCerrar(function () {
        window.open(location.host + '/' + resp.message, '_blank');
      });
      modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
    }, "json").fail(function (resp) {
      modal.establecerAccionCerrar(function () { });
      modal.mostrarModal("Informacion", "<div class=\"alert alert-warning\">" + resp.responseJSON.message + "</div>");
    });
  }

  function validarForm() {

    let mensajes = [];

    let Documento_iodDocumento_Input = $("#Documento_iodDocumento");
    let Documento_iodDocumento_AE = Documento_iodDocumento_Input.val();
    Documento_iodDocumento_Input.removeClass(errorClass);
    if (!Documento_iodDocumento_AE) {
      Documento_iodDocumento_Input.addClass(errorClass)
      mensajes.push("El campo Documento es obligatorio");
    }

    let Empleado_oidEmpleado_Input = $("#Empleado_oidEmpleado");
    let Empleado_oidEmpleado_AE = Empleado_oidEmpleado_Input.val();
    Empleado_oidEmpleado_Input.removeClass(errorClass);
    if (!Empleado_oidEmpleado_AE) {
      Empleado_oidEmpleado_Input.addClass(errorClass)
      mensajes.push("El campo Empleado de reunión es obligatorio");
    }

    let Cliente_oidCliente_Input = $("#Cliente_oidCliente");
    let Cliente_oidCliente_AE = Cliente_oidCliente_Input.val();
    Cliente_oidCliente_Input.removeClass(errorClass);
    if (!Cliente_oidCliente_AE) {

      Cliente_oidCliente_Input.addClass(errorClass)
      mensajes.push("El campo Cliente es obligatorio");
    }
    return mensajes;
  }
}
