"use strict";
var signaturePad; // Variable global para SignaturePad
var errorClass = "is-invalid";
var configuracionBitacoraManualPendientes = JSON.parse(bitacoraManualPendiente);
var configuracionBitacoraManualPendiente = [];
var configuracionBitacoraManualAsistentes = JSON.parse(bitacoraManualAsistente);
var configuracionBitacoraManualAsistente = [];

$(function () {
  var editor = CKEDITOR.replace('txObservacionBitacoraManual');

  editor.on('change', function (evt) {
    document.getElementById('txObservacionBitacoraManual').value = evt.editor.getData()
  })

  configuracionBitacoraManualPendiente = new GeneradorMultiRegistro(
    "configuracionBitacoraManualPendiente",
    "contenedorBitacoraManualPendiente",
    "configuracionBitacoraManualPendiente"
  );

  let options = [JSON.parse(idResponsable), JSON.parse(nombreResponsable)];

  configuracionBitacoraManualPendiente.campoid = "oidBitacoraManualPendiente";
  configuracionBitacoraManualPendiente.campos = [
    "oidBitacoraManualPendiente",
    "Responsable_oidResponsable",
    "txNombreTareaBitacoraManual",
    "dtFechaVencimientoBitacoraManual",
  ];
  configuracionBitacoraManualPendiente.etiqueta = [
    "input",
    "select",
    "input",
    "input",
  ];
  configuracionBitacoraManualPendiente.tipo = ["hidden", "", "text", "date"];
  configuracionBitacoraManualPendiente.estilo = ["", "", "", ""];
  configuracionBitacoraManualPendiente.clase = ["", "chosen-select", "", ""];
  configuracionBitacoraManualPendiente.sololectura = [
    true,
    false,
    false,
    false,
  ];
  configuracionBitacoraManualPendiente.opciones = ["", options, "", ""];
  configuracionBitacoraManualPendiente.funciones = ["", "", "", ""];
  configuracionBitacoraManualPendiente.otrosAtributos = ["", "", "", ""];

  configuracionBitacoraManualPendientes.forEach((dato) => {
    configuracionBitacoraManualPendiente.agregarCampos(dato, "L");
  });

  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }
});

$(function () {
  configuracionBitacoraManualAsistente = new GeneradorMultiRegistro(
    "configuracionBitacoraManualAsistente",
    "contenedorBitacoraManualAsistente",
    "configuracionBitacoraManualAsistente"
  );

  configuracionBitacoraManualAsistente.campoid = "oidBitacoraManualAsistente";
  configuracionBitacoraManualAsistente.campos = [
    "oidBitacoraManualAsistente",
    "txNombreAsistenteBitacoraManual",
    "shTalleristaAsistente",
  ];
  configuracionBitacoraManualAsistente.etiqueta = [
    "input",
    "input",
    "checkbox",
  ];
  configuracionBitacoraManualAsistente.tipo = ["hidden", "text", ""];
  configuracionBitacoraManualAsistente.estilo = ["", "", ""];
  configuracionBitacoraManualAsistente.clase = ["", "", ""];
  configuracionBitacoraManualAsistente.sololectura = [true, false, false];
  configuracionBitacoraManualAsistente.opciones = ["", "", ""];
  configuracionBitacoraManualAsistente.funciones = ["", "", ""];
  configuracionBitacoraManualAsistente.otrosAtributos = ["", "", ""];

  configuracionBitacoraManualAsistentes.forEach((dato) => {
    configuracionBitacoraManualAsistente.agregarCampos(dato, "L");
  });
});

function generarBitacora() {
  modal.cargando();
  let mensajes = validarForm();
  capturarFirma(mensajes);
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-bitacoramanual";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          console.log(resp);
          downloadFile(resp);
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {
    let mensajes = [];
    let txNombreActividadBitacoraManual_Input = $(
      "#txNombreActividadBitacoraManual"
    );
    let txNombreActividadBitacoraManual_AE =
      txNombreActividadBitacoraManual_Input.val();
    txNombreActividadBitacoraManual_Input.removeClass(errorClass);
    if (!txNombreActividadBitacoraManual_AE) {
      txNombreActividadBitacoraManual_Input.addClass(errorClass);
      mensajes.push("El campo Actividad es obligatorio");
    }

    let Tercero_oidCliente_Input = $("#Tercero_oidCliente");
    let Tercero_oidCliente_AE = Tercero_oidCliente_Input.val();
    Tercero_oidCliente_Input.removeClass(errorClass);
    if (!Tercero_oidCliente_AE) {
      Tercero_oidCliente_Input.addClass(errorClass);
      mensajes.push("El campo Cliente es obligatorio");
    }

    let modalidadBitacora_Input = $("#modalidadBitacora");
    let modalidadBitacora_AE = modalidadBitacora_Input.val();
    modalidadBitacora_Input.removeClass(errorClass);
    if (!modalidadBitacora_AE) {
      modalidadBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Modalidad es obligatorio");
    }
    return mensajes;
  }
}

function downloadFile(response) {
  window.open(
    "http://" + location.host + "/modules/plantrabajo/" + response.archivo,
    "_blank",
    "width=2500px, height=700px, scrollbars=yes"
  );
}

document.addEventListener("DOMContentLoaded", function () {
  const canvas = document.getElementById("signature-pad");
  signaturePad = new SignaturePad(canvas);

  document.getElementById("clear-signature").addEventListener("click", function () {
    signaturePad.clear();
  });
});

function capturarFirma(mensajes) {
  if (!signaturePad.isEmpty()) {
    const signatureData = signaturePad.toDataURL(); // Captura la firma como Base64
    document.getElementById("firma_base64").value = signatureData; // Asigna el valor al campo oculto
  } else {
    let modalidadBitacora = $("#modalidadBitacora").val();
    if (modalidadBitacora === "Presencial") {
      mensajes.push("La firma es obligatoria en modalidad Presencial");
    }
    return mensajes;
  }
}

