"use strict";
var errorClass = "is-invalid";
var configurarTipoInspeccionDetalles = JSON.parse(tipoInspeccionDetalle);
var configuracionTipoInspeccionDetalle = [];

$(function () {
    configuracionTipoInspeccionDetalle = new GeneradorMultiRegistro('configuracionTipoInspeccionDetalle', 'contenedorTipoInspeccion', 'configuracionTipoInspeccionDetalle');

    configuracionTipoInspeccionDetalle.campoid = 'oidTipoInspeccionDetalle';
    configuracionTipoInspeccionDetalle.campoEliminacion = 'eliminarTipoInspeccion';
    configuracionTipoInspeccionDetalle.botonEliminacion = true;
    configuracionTipoInspeccionDetalle.funcionEliminacion = '';

    configuracionTipoInspeccionDetalle.campos = ['oidTipoInspeccionDetalle', 'txNombreTipoInspeccionDetalle'];

    configuracionTipoInspeccionDetalle.etiqueta = ['input', 'input'];

    configuracionTipoInspeccionDetalle.tipo = ['hidden', 'text'];

    configuracionTipoInspeccionDetalle.estilo = ['', ''];
    configuracionTipoInspeccionDetalle.clase = ['', ''];
    configuracionTipoInspeccionDetalle.sololectura = [true, false];
    configuracionTipoInspeccionDetalle.opciones = ['', ''];
    configuracionTipoInspeccionDetalle.funciones = ['', ''];
    configuracionTipoInspeccionDetalle.otrosAtributos = ['', ''];

    configurarTipoInspeccionDetalles.forEach(function (dato, i) {
        configuracionTipoInspeccionDetalle.agregarCampos(dato, 'L');
    });
});

function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-tipoinspeccion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();

        console.log(formularioId);
        console.log(route);
        console.log(data);
        // return;
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                location.href = "/plantrabajo/tipoinspeccion";
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {
        let mensajes = [];

        let txNombreTipoInspeccion_Input = $("#txNombreTipoInspeccion");
        let txNombreTipoInspeccion_AE = txNombreTipoInspeccion_Input.val();
        txNombreTipoInspeccion_Input.removeClass(errorClass);
        if (!txNombreTipoInspeccion_AE) {
            txNombreTipoInspeccion_Input.addClass(errorClass);
            mensajes.push("El campo Nombre es obligatorio");
        }

        let txDescripcionTipoInspeccion_Input = $("#txDescripcionTipoInspeccion");
        let txDescripcionTipoInspeccion_AE = txDescripcionTipoInspeccion_Input.val();
        txDescripcionTipoInspeccion_Input.removeClass(errorClass);
        if (!txDescripcionTipoInspeccion_AE) {
            txDescripcionTipoInspeccion_Input.addClass(errorClass);
            mensajes.push("El campo Descripción es obligatorio");
        }

        return mensajes;
    }
}