"use strict";
var errorClass = "is-invalid";
var registros = JSON.parse(valores);
var actividad = [];
var pendientes = [];
var asistente = [];
var actividadDetalle = [];
// var registrosPendientes = (valoresPendientes != "" ? JSON.parse(valoresPendientes): "");


$(function(){   
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
     
    actividad = new GeneradorMultiRegistro('actividad','contenedorActividad','actividad');
    actividad.campoid = 'oidTerceroVerificacion';
    actividad.campoEliminacion = '';
    actividad.etiqueta = ['input','input','input','input','input','input','input','input','input','input','checkbox', 'button', 'button'];
    actividad.tipo = ['hidden','hidden','text','date','time','text','hidden','hidden','hidden','date','checkbox', 'button', 'button'];
    actividad.campos = [
    'oidActividadPlanTrabajo',
    'ConfiguracionPlanTrabajo_oidConfiguracionPlan',
    'txActividadProgramadaConfiguracionPlanTrabajo',
    'daFechaProgramadaActividadPlanTrabajo',
    'txHorasProgramadasActividadPlanTrabajo',
    'txCantidadHorasActividadPlanTrabajo',
    'oidEjecucionPlanTrabajoDetalle',
    'EjecucionPlanTrabajo_oidEjecucionPlanTrabajo',
    'ActividadPlanTrabajo_oidActividadPlanTrabajo',
    'daFechaEjecucionPlanTrabajoDetalle',
    'chCumplidoEjecucionPlanTrabajoDetalle',
    'bitacoraEjecucionPlanTrabajoDetalle',
    'actividadEjecucionPlanTrabajoDetalle'];
    actividad.opciones = ['','','','','','','','','','','','',''];
    actividad.funciones = ['','','','','','','','','','','','',''];
    actividad.clase = ['','','','','','','','','','','','far fa-folder-open', 'fa fa-folder-open'];
    actividad.sololectura = [true,true,true,true,true,true,false,false,false,false,false,false,false];
    actividad.botonEliminacion = false;

    let reg = 0;
    while (reg < registros.length) {
        actividad.agregarCampos(registros[reg] , 'L')
        var idActividad = registros[reg]['oidActividadPlanTrabajo'];
        var idDetalle = registros[reg]['oidEjecucionPlanTrabajoDetalle'];
        $("#bitacoraEjecucionPlanTrabajoDetalle"+reg).attr('onClick', 'modalBitacora('+idActividad+','+idDetalle+')');
        $("#actividadEjecucionPlanTrabajoDetalle"+reg).attr('onClick', 'modalSubTarea('+idActividad+','+idDetalle+')');
        reg = reg + 1;
    }
});

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-ejecucionplantrabajo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            // location.reload();
            modal.establecerAccionCerrar(function(){
                location.href = "/plantrabajo/ejecucionplantrabajoedit/"+resp.oidEjecucionPlanTrabajo+'/edit';
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let Tercero_oidTerceroEncargado_Input = $("#Tercero_oidTerceroEncargado");
        let Tercero_oidTerceroEncargado_AE = Tercero_oidTerceroEncargado_Input.val();
        Tercero_oidTerceroEncargado_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroEncargado_AE){
            Tercero_oidTerceroEncargado_Input.addClass(errorClass)
            mensajes.push("El campo Encargado es obligatorio");
        }
		let Tercero_oidTerceroCompania_Input = $("#Tercero_oidTerceroCompania");
        let Tercero_oidTerceroCompania_AE = Tercero_oidTerceroCompania_Input.val();
        Tercero_oidTerceroCompania_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroCompania_AE){
            Tercero_oidTerceroCompania_Input.addClass(errorClass)
            mensajes.push("El campo Compañia es obligatorio");
        }
        let Tercero_oidTerceroRealiza_Input = $("#Tercero_oidTerceroRealiza");
        let Tercero_oidTerceroRealiza_AE = Tercero_oidTerceroRealiza_Input.val();
        Tercero_oidTerceroRealiza_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroRealiza_AE){
            Tercero_oidTerceroRealiza_Input.addClass(errorClass)
            mensajes.push("El campo Realizad es obligatorio");
        }
		return mensajes;
    }
}

function modalBitacora(idActividad,idDetalle) {

    modal.cargando()
    $.get('/plantrabajo/planTrabajoEjecucionBitacora/'+idDetalle+'/modal', function (resp) {

        modal.mostrarModal('Bitacora: ', resp.view, function () {
            modal.cargando();
        }).extraGrande().sinPie();

            //MULTIREGISTRO DE PENDIENTES
            var registrosPendientes = resp.data;
            var idTercero =  resp.terceroId;
            var nombreTercero =  resp.terceroNombre;
            let tercero = [JSON.parse(idTercero), JSON.parse(nombreTercero)];
            pendientes = new GeneradorMultiRegistro('pendientes','contenedorPendientes','pendientes');
            pendientes.campoid = 'oidBitacoraPendiente';
            pendientes.campoEliminacion = 'eliminarPendientes';
            pendientes.etiqueta = ['input','input','input','select','input'];
            pendientes.tipo = ['hidden','hidden','text','text','date'];
            pendientes.campos = ['oidBitacoraPendiente','PlanTrabajoBitacora_oidPlanTrabajoBitacora','txBitacoraActividad','Tercero_oidBitacoraResponsable','daBitacoraFehcaEntrega'];
            pendientes.opciones = ['','','',tercero,''];
            pendientes.funciones = ['','','','',''];
            pendientes.clase = ['','','','',''];
            pendientes.sololectura = [true,true,false,false,false];
            let reg = 0;
            while (reg < registrosPendientes.length) {
                pendientes.agregarCampos(registrosPendientes[reg] , 'L')
                reg = reg + 1;
            }

            //MULTIREGISTRO DE ASISTENTES
            var registrosAsistentes = resp.dataAsistente;
            asistente = new GeneradorMultiRegistro('asistente','contenedorAsistente','asistente');
            asistente.campoid = 'oidBitacoraAsistente';
            asistente.campoEliminacion = 'eliminarAsistente';
            asistente.etiqueta = ['input','input','input'];
            asistente.tipo = ['hidden','hidden','text'];
            asistente.campos = ['oidBitacoraAsistente','PlanTrabajoBitacora_oidPlanTrabajoBitacora','txNombreAsistente'];
            asistente.opciones = ['','',''];
            asistente.funciones = ['','',''];
            asistente.clase = ['','',''];
            asistente.sololectura = [true,true,false];
            let regs = 0;
            while (regs < registrosAsistentes.length) {
                asistente.agregarCampos(registrosAsistentes[regs] , 'L')
                regs = regs + 1;
            }
    },'json').fail(function (resp) {
        modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
    });
}

function modalSubTarea(idActividad, idDetalle) {

    modal.cargando()
    $.get('/plantrabajo/ejecucionactividadplantrabajodetalle/'+idActividad+'/'+idDetalle+'/modal', function (resp) {

        modal.mostrarModal('Ejecución de Subtareas ', resp.view, function () {
            modal.cargando();
        }).extraGrande().sinPie();

            var registroActividades = resp.data;
            actividadDetalle = new GeneradorMultiRegistro('actividadDetalle','contenedorActividadDetalle','actividadDetalle');
            actividadDetalle.campoid = 'oidEjecucionActividadPlanTrabajoDetalle';
            actividadDetalle.campoEliminacion = '';
            actividadDetalle.etiqueta = ['input','input','input', 'checkbox'];
            actividadDetalle.tipo = ['hidden','hidden','text','checkbox'];
            actividadDetalle.campos = ['oidEjecucionActividadPlanTrabajoDetalle','oidActividadPlanTrabajoDetalle','txNombreActividadPlanTrabajoDetalle', 'chEjecutadoEjecucionActividadPlanTrabajoDetalle'];
            actividadDetalle.opciones = ['','','', ''];
            actividadDetalle.funciones = ['','','',''];
            actividadDetalle.clase = ['','','',''];
            actividadDetalle.botonEliminacion = false;
            actividadDetalle.sololectura = [true,true,true,false];
            let regs = 0;
            while (regs < registroActividades.length) {
                actividadDetalle.agregarCampos(registroActividades[regs] , 'L')
                regs = regs + 1;
            }
    },'json').fail(function (resp) {
        modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
    });
}