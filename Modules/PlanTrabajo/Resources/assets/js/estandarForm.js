"use strict";
var errorClass = "is-invalid";
var configuracionEstandar = [];

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };
  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  configuracionEstandar = new GeneradorMultiRegistro('configuracionEstandar', 'contenedorEstandar', 'configuracionEstandar_');

  configuracionEstandar.botonEliminacion = false;
  configuracionEstandar.funcionEliminacion = '';
  configuracionEstandar.campos = ['chGeneraEstandar', 'txNombreEstandar'];
  configuracionEstandar.etiqueta = ['checkbox', 'input'];
  configuracionEstandar.tipo = ['checkbox', 'text'];
  configuracionEstandar.clase = ['', ''];
  configuracionEstandar.sololectura = [false, true];
  configuracionEstandar.opciones = ['', ''];
  configuracionEstandar.funciones = ['', ''];
  configuracionEstandar.otrosAtributos = ['', ''];

  configuracionEstandares.forEach(dato => {
    configuracionEstandar.agregarCampos(dato, 'L');
  });

  var checkboxes = document.querySelectorAll("#contenedorEstandar input[type='checkbox']");

  function checkALL(mycheckbox) {
    checkboxes.forEach(function (checkbox, index) {
      if (checkbox !== mycheckbox) {
        checkbox.checked = mycheckbox.checked;
        var idReg = "chGeneraEstandar" + index;
        document.getElementById(idReg).value = mycheckbox.checked;
      }
    });
  }

  document.getElementById('option').addEventListener('change', function () {
    checkALL(this);
  });
});

function asignarIdsCliente() {
  $("#lmClienteEstandar").val($("#idClientes").val());
}

function generarEstandares() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    if (confirm("¿Desea validar la información de la contextualización de este cliente?") == true) {
      let formularioId = "#form-estandar";
      let route = $(formularioId).attr("action");
      let data = $(formularioId).serialize();
      $.post(route, data, function (resp) {
        modal.establecerAccionCerrar(function () {
          var baseUrl = window.location.origin;
          var url = baseUrl + '/' + resp.message;
          window.open(url, '_blank');
          location.reload();
        });
        modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
      }, "json").fail(function (resp) {
        modal.establecerAccionCerrar(function () { });
        modal.mostrarModal("Informacion", "<div class=\"alert alert-warning\">" + resp.responseJSON.message + "</div>");
      });
    }
  }

  function validarForm() {
    let mensajes = [];

    let dtFechaGeneracionEstandar_Input = $("#dtFechaGeneracionEstandar");
    let dtFechaGeneracionEstandar_AE = dtFechaGeneracionEstandar_Input.val();
    dtFechaGeneracionEstandar_Input.removeClass(errorClass);
    if (!dtFechaGeneracionEstandar_AE) {
      dtFechaGeneracionEstandar_Input.addClass(errorClass);
      mensajes.push("El campo Fecha es obligatorio");
    }

    let lsAnioEstandar_Input = $("#lsAnioEstandar");
    let lsAnioEstandar_AE = lsAnioEstandar_Input.val();
    lsAnioEstandar_Input.removeClass(errorClass);
    if (!lsAnioEstandar_AE) {
      lsAnioEstandar_Input.addClass(errorClass);
      mensajes.push("El campo Año es obligatorio");
    }

    let lmClienteEstandar_Input = $("#lmClienteEstandar");
    let lmClienteEstandar_AE = lmClienteEstandar_Input.val();
    lmClienteEstandar_Input.removeClass(errorClass);
    if (!lmClienteEstandar_AE) {
      lmClienteEstandar_Input.addClass(errorClass);
      mensajes.push("El campo Cliente es obligatorio");
    }
    return mensajes;
  }
}