"use strict";
var errorClass = "is-invalid";
var configuracionMatrices = JSON.parse(matrizRiesgos);
var configuracionMatriz = [];

function consultarMatriz(clienteId){
    if(clienteId){
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            dataType: "json",
            url: '/plantrabajo/consultarMatrizByCliente/'+clienteId,
            type: 'GET',
            success: function(resp){
                console.log(resp);
                resp.forEach(function (dato, i) {
                    configuracionMatriz.agregarCampos(dato , 'L');
                    calcularInterpretacionProbabilidad(i);
                });
            },
            error: function(xhr, err) {
                modal.mostrarModal('Error','Ocurrio un problema');
            }
        });
    }else{
        $("#contenedorMatriz").empty();
        configuracionMatriz.contador = 0;
    }
}

$(function(){
    //generamos la multiregistro de grupo
    configuracionMatriz = new GeneradorMultiRegistro('configuracionMatriz','contenedorMatriz','configuracionMatriz');

    let cargo = [JSON.parse(idCargo),JSON.parse(nombreCargo)]
    let rutinarias = [['Si','No'],['Si', 'No']];
    let clasificacion = [['Biológico','Físico', 'Químico', 'Psicosociales', 'Biomecánico', 'Condiciones de seguridad', 'Fenómenos naturales'],['Biológico', 'Físico', 'Químico', 'Psicosociales', 'Biomecánico', 'Condiciones de seguridad', 'Fenómenos naturales']];
    let deficiencia = [['0', '2', '6', '10'],['0',  '2', '6', '10']];
    let exposicion = [['1', '2', '3', '4'],['1',  '2', '3', '4']];
    let consecuencia = [['10', '25', '60', '100'],['10',  '25', '60', '100']];

    let calcularProbabilidadDeficiencia = ['onchange','calcularProbabilidadDeficiencia(this.id)'];
    let calcularProbabilidadExposicion = ['onchange','calcularProbabilidadExposicion(this.id)'];
    let calcularNivelRiesgo = ['onchange','calcularNivelRiesgo(this.id)'];

    configuracionMatriz.campoid = 'oidMatrizRiesgo';
    configuracionMatriz.campoEliminacion = 'eliminarMatriz';
    configuracionMatriz.botonEliminacion = true;
    configuracionMatriz.funcionEliminacion = '';  
    configuracionMatriz.campos = ['oidMatrizRiesgo','Cargo_oidCargo','txActividadCargoMatrizRiesgo','lsRutinariaMatrizRiesgo', 'lsClasificacionMatrizRiesgo', 'txDescripcionClasificacionMatrizRiesgo', 'txEfectosPosiblesMatrizRiesgo', 'txFuenteMatrizRiesgo', 'txMedioMatrizRiesgo', 'txIndividuoMatrizRiesgo', 'lsNivelDeficienciaMatrizRiesgo', 'lsNivelExposicionMatrizRiesgo', 'txNivelProbabilidadMatrzRiesgo', 'txInterpretacionProbabilidadMatrizRiesgo', 'lsNivelConsecuenciaMatrizRiesgo', 'txNivelRiesgoMatrizRiesgo', 'txInterpretacionRiesgoMatrizRiesgo', 'txAceptabilidadRiesgoMatrizRiesgo', 'inExpuestosMatrizRiesgo', 'txPeorConsecuenciaMatrizRiesgo', 'txEliminacionMatrizRiesgo', 'txSustitucionMatrizRiesgo', 'txControlIngenieria', 'txControlAdministrativoMatrizRiesgo', 'txElementosProteccionPersonalMatrizRiesgo'];

    configuracionMatriz.etiqueta = ['input','select','input', 'select', 'select', 'input', 'input', 'input', 'input', 'input', 'select', 'select', 'input', 'input', 'select', 'input', 'input', 'input', 'input', 'input', 'input', 'input', 'input', 'input', 'input'];

    configuracionMatriz.tipo = ['hidden','', 'text', '', '', 'text', 'text', 'text', 'text', 'text', '', '', 'text', 'text', '', 'text', 'text', 'text', 'number', 'text', 'text', 'text', 'text', 'text', 'text'];

    configuracionMatriz.estilo = ['','','','','','','','','','','','','','','','','','','','','','','','',''];
    configuracionMatriz.clase = ['','','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
    configuracionMatriz.sololectura = [true,false,false,false,false,false,false,false,false,false,false,false,true,true,false,true,true,true,false,false,false,false,false,false,false];
    configuracionMatriz.opciones = ['',cargo, '', rutinarias, clasificacion,'','','','','', deficiencia, exposicion,'','', consecuencia,'','','','','','','','','',''];
    configuracionMatriz.funciones = ['','','', '', '', '', '', '', '', '', calcularProbabilidadDeficiencia, calcularProbabilidadExposicion, '', '', calcularNivelRiesgo, '', '', '', '', '', '', '', '', '', ''];
    configuracionMatriz.otrosAtributos = ['','','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

    configuracionMatrices.forEach(function (dato, i) {
        configuracionMatriz.agregarCampos(dato , 'L');
        calcularInterpretacionProbabilidad(i);
    });
});

function calcularProbabilidadDeficiencia(idRegistro){
    let registro = idRegistro.replace('lsNivelDeficienciaMatrizRiesgo', '');
    let deficiencia = $("#lsNivelDeficienciaMatrizRiesgo"+registro).val();
    let exposicion = $("#lsNivelExposicionMatrizRiesgo"+registro).val();

    let resultado = parseInt(deficiencia) * parseInt(exposicion);

    $("#txNivelProbabilidadMatrzRiesgo"+registro).val(isNaN(resultado) ? 0 : resultado);

    calcularInterpretacionProbabilidad(registro);
}

function calcularProbabilidadExposicion(idRegistro){
    let registro = idRegistro.replace('lsNivelExposicionMatrizRiesgo', '');
    let deficiencia = $("#lsNivelDeficienciaMatrizRiesgo"+registro).val();
    let exposicion = $("#lsNivelExposicionMatrizRiesgo"+registro).val();

    let resultado = parseInt(deficiencia) * parseInt(exposicion);

    $("#txNivelProbabilidadMatrzRiesgo"+registro).val(isNaN(resultado) ? 0 : resultado);

    calcularInterpretacionProbabilidad(registro);
}

function calcularInterpretacionProbabilidad(registro){
    let nivelProbabilidad = $("#txNivelProbabilidadMatrzRiesgo"+registro).val();
    $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("color", "white");
    if(nivelProbabilidad >= 2 && nivelProbabilidad <= 4){
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).val("BAJO");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("background-color", "green");
    } else if(nivelProbabilidad >= 6 && nivelProbabilidad <= 8){
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).val("MEDIO");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("background-color", "yellow");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("color", "black");
    } else if(nivelProbabilidad >= 10 && nivelProbabilidad <= 20){
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).val("ALTO");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("background-color", "red");
    } else if(nivelProbabilidad >= 20 && nivelProbabilidad <= 40){
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).val("MUY ALTO");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("background-color", "orange");
    } else{
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).val("");
        $("#txInterpretacionProbabilidadMatrizRiesgo"+registro).css("background-color", "");
    }

    calcularNivelRiesgo('lsNivelConsecuenciaMatrizRiesgo'+registro);
}

function calcularNivelRiesgo(idRegistro){
    let registro = idRegistro.replace('lsNivelConsecuenciaMatrizRiesgo', '');
    let nivelProbabilidad = $("#txNivelProbabilidadMatrzRiesgo"+registro).val();
    let nivelConsecuencia = $("#lsNivelConsecuenciaMatrizRiesgo"+registro).val();

    let resultado = parseInt(nivelProbabilidad) * parseInt(nivelConsecuencia);

    $("#txNivelRiesgoMatrizRiesgo"+registro).val(isNaN(resultado) ? 0 : resultado);
    calcularInterpretacionRiesgo(registro);
}

function calcularInterpretacionRiesgo(registro){
    let nivelRiesgo = $("#txNivelRiesgoMatrizRiesgo"+registro).val();
    $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("color", "white");
    if(nivelRiesgo == 20){
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).val("IV");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("background-color", "green");
        $("#txAceptabilidadRiesgoMatrizRiesgo"+registro).val("Aceptable");
    } else if(nivelRiesgo >= 40 && nivelRiesgo <= 120){
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).val("III");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("background-color", "green");
        $("#txAceptabilidadRiesgoMatrizRiesgo"+registro).val("Mejorable");
    } else if(nivelRiesgo >= 150 && nivelRiesgo <= 500){
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).val("II");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("background-color", "yellow");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("color", "black");
        $("#txAceptabilidadRiesgoMatrizRiesgo"+registro).val("Aceptable con control específico");
    } else if(nivelRiesgo >= 600 && nivelRiesgo <= 4000){
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).val("I");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("background-color", "red");
        $("#txAceptabilidadRiesgoMatrizRiesgo"+registro).val("No aceptable");
    } else{
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).val("");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("background-color", "");
        $("#txInterpretacionRiesgoMatrizRiesgo"+registro).css("color", "");
        $("#txAceptabilidadRiesgoMatrizRiesgo"+registro).val("");
    }
}

function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-matrizriesgoenc";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
               location.href = "/plantrabajo/matrizriesgo";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}