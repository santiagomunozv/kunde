"use strict";
var errorClass = "is-invalid";
var configuracionSistemaGestionInformes = JSON.parse(sistemaGestionInforme);
var configuracionSistemaGestionInforme = [];

$(function(){

    configuracionSistemaGestionInforme = new GeneradorMultiRegistro('configuracionSistemaGestionInforme','contenedorSistemaGestionInforme','configuracionSistemaGestionInforme');

    configuracionSistemaGestionInforme.campoid = 'oidSistemaGestionInforme'; 
    configuracionSistemaGestionInforme.campos = ['oidSistemaGestionInforme','txNombreEmpleado', 'txNombreCargo'];
    configuracionSistemaGestionInforme.etiqueta = ['input', 'input', 'input'];
    configuracionSistemaGestionInforme.tipo = ['hidden','text', 'text'];
    configuracionSistemaGestionInforme.estilo = ['','',''];
    configuracionSistemaGestionInforme.clase = ['','',''];
    configuracionSistemaGestionInforme.sololectura = [true,false,false];
    configuracionSistemaGestionInforme.opciones = ['', '', ''];
    configuracionSistemaGestionInforme.funciones = ['', '', ''];
    configuracionSistemaGestionInforme.otrosAtributos = ['', '', ''];


    configuracionSistemaGestionInformes.forEach( dato => {configuracionSistemaGestionInforme.agregarCampos(dato , 'L');
    });
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
})


function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-sistemagestioninforme";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
               location.href = "/plantrabajo/sistemagestioninforme";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            console.log(resp);
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}