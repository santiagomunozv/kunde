"use strict";
var errorClass = "is-invalid";
var configurarConfiguracionInspeccionDetalles = JSON.parse(configuracionInspeccionTipoInspeccion);
var configuracionConfiguracionInspeccion = [];

$(function () {
    configuracionConfiguracionInspeccion = new GeneradorMultiRegistro('configuracionConfiguracionInspeccion', 'contenedorConfiguracionInspeccion', 'configuracionConfiguracionInspeccion');

    configuracionConfiguracionInspeccion.campoid = 'oidConfiguracionInspeccionTipoInspeccion';
    configuracionConfiguracionInspeccion.campoEliminacion = 'eliminarConfiguracionInspeccion';
    configuracionConfiguracionInspeccion.botonEliminacion = true;
    configuracionConfiguracionInspeccion.funcionEliminacion = '';

    configuracionConfiguracionInspeccion.campos = ['oidConfiguracionInspeccionTipoInspeccion', 'TipoInspeccion_oidTipoInspeccion'];

    configuracionConfiguracionInspeccion.etiqueta = ['input', 'select'];

    configuracionConfiguracionInspeccion.tipo = ['hidden', ''];

    configuracionConfiguracionInspeccion.estilo = ['', ''];
    configuracionConfiguracionInspeccion.clase = ['', ''];
    configuracionConfiguracionInspeccion.sololectura = [true, false];
    configuracionConfiguracionInspeccion.opciones = ['', tipoInspeccionList];
    configuracionConfiguracionInspeccion.funciones = ['', ''];
    configuracionConfiguracionInspeccion.otrosAtributos = ['', ''];

    configurarConfiguracionInspeccionDetalles.forEach(function (dato, i) {
        configuracionConfiguracionInspeccion.agregarCampos(dato, 'L');
    });
});

function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-configuracioninspeccion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                location.href = "/plantrabajo/configuracioninspeccion";
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {
        let mensajes = [];

        let txNombreConfiguracionInspeccion_Input = $("#txNombreConfiguracionInspeccion");
        let txNombreConfiguracionInspeccion_AE = txNombreConfiguracionInspeccion_Input.val();
        txNombreConfiguracionInspeccion_Input.removeClass(errorClass);
        if (!txNombreConfiguracionInspeccion_AE) {
            txNombreConfiguracionInspeccion_Input.addClass(errorClass);
            mensajes.push("El campo Nombre es obligatorio");
        }

        let txDescripcionConfiguracionInspeccion_Input = $("#txDescripcionConfiguracionInspeccion");
        let txDescripcionConfiguracionInspeccion_AE = txDescripcionConfiguracionInspeccion_Input.val();
        txDescripcionConfiguracionInspeccion_Input.removeClass(errorClass);
        if (!txDescripcionConfiguracionInspeccion_AE) {
            txDescripcionConfiguracionInspeccion_Input.addClass(errorClass);
            mensajes.push("El campo Descripción es obligatorio");
        }

        return mensajes;
    }
}