@extends('plantrabajo::layouts.master')

@section('content')
    <img src={{ public_path('/images/LogoKunde.jpeg') }} style="height: 3cm; width: 3cm;">

    <table>
        <tr>
            <th class="titulo">Actividad</th>
            <td colspan="3">{{ $actividad->nombreActividad }}</td>
        </tr>
        <tr>
            <th class="titulo">Fecha</th>
            <td>{{ $actividad->fechaPlanTrabajoBitacora }}</td>
            <th class="titulo">Hora</th>
            <td>{{ $actividad->horaInicioPlanTrabajoBitacora }} a {{ $actividad->horaSalidaPlanTrabajoBitacora }}</td>
        </tr>
        <tr>
            <th class="titulo">Aliado</th>
            <td colspan="3">{{ $actividad->txNombreTercero }}</td>
        </tr>
        <tr>
            <th class="titulo">Modalidad</th>
            <td colspan="3">{{ $actividad->modalidad }}</td>
        </tr>
    </table>

    <!-- Espacio entre Aliado y Participantes -->
    <div class="space"></div>

    <table>
        <tr class="titulo">
            <th colspan="3">PARTICIPANTES</th>
        </tr>
        @foreach ($participantes as $participante)
            <tr>
                <td colspan="3">{{ $participante->txNombreAsistente }}</td>
            </tr>
        @endforeach

        <div class="space"></div>

        <tr class="titulo">
            <th colspan="3">DETALLE</th>
        </tr>
    </table>

    <div class="observation no-margin">
        <div class="detail-container">
            {!! $actividad->observacionPlanTrabajoBitacora !!}
        </div>
    </div>

    <div class="space"></div>

    <table>
        <tr class="titulo">
            <th colspan="3">PENDIENTES</th>
        </tr>
        <tr>
            <th>TAREA</th>
            <th>RESPONSABLE</th>
            <th>FECHA</th>
        </tr>
        @foreach ($tareas as $tarea)
            <tr>
                <td>{{ $tarea->txBitacoraActividad }}</td>
                <td>{{ $tarea->txNombreTercero }}</td>
                <td>{{ $tarea->daBitacoraFehcaEntrega }}</td>
            </tr>
        @endforeach
    </table>

    @if ($actividad->modalidad === 'Presencial')
        <div class="signature-container">
            <p>Firma:</p>
            <img src="{{ $actividad->rutaFirma }}" alt="Firma aliado"
                style="width: 200px; height: auto; border: 1px solid #000;">
        </div>
    @endif


    <style>
        table {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: separate;
            border-spacing: 0;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #ddd;
            padding: 8px;
            border-radius: 8px;
        }

        .titulo {
            background-color: #0F105B;
            color: white;
            border-radius: 8px;
        }

        .observation {
            font-size: 12pt;
            line-height: 1.6;
            text-align: justify;
        }

        .no-margin {
            margin: 0;
        }

        .detail-container {
            border: 1px solid #ddd;
            padding: 10px;
            border-radius: 8px;
        }

        .space {
            height: 20px;
        }

        .signature-container {
            margin-top: 20px;
        }
    </style>
@stop
