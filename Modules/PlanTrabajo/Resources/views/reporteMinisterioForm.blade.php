@extends('layouts.principal')
@section('nombreModulo')
    Diseños
@endsection
@section('scripts')
    <script>
        let detalle = '<?php echo json_encode($detalle); ?>';
    </script>
    {{ Html::script('modules/plantrabajo/js/reporteMinisterioForm.js') }}
@endsection
@section('estilos')
@endsection

@section('contenido')

    {!! Form::open(['url' => ['plantrabajo/reporteministerio'], 'method' => 'POST', 'id' => 'form-reporteministerio']) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('Documento_iodDocumento', 'Tipo de Documento', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('Documento_iodDocumento', ['ActaNombramiento' => 'Acta de nombramiento', 'CapacitacionSST' => 'Capacitación SST', 'AsignacionRecursos'=> 'Asignación de recursos', 'PlanTrabajoAnual' => 'Plan trabajo anual', 'MedidasPrevencion' => 'Medidas de prevención'], null, ['class' => 'chosen-select', 'id' => 'Documento_iodDocumento', 'placeholder' => 'Seleccione un documento']) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('Empleado_oidEmpleado', 'Empleado', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('Empleado_oidEmpleado', $empleado, null, ['class' => 'chosen-select form-control', 'id' => 'Empleado_oidEmpleado', 'placeholder' => 'Selecciona un Empleado']) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('Cliente_oidCliente', 'Cliente', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('Cliente_oidCliente', $cliente, null, ['class' => 'chosen-select form-control', 'id' => 'Cliente_oidCliente', 'placeholder' => 'Selecciona un Cliente']) !!}
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light"
                                onclick="configuracionDetalle.agregarCampos([],'L');">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Campo</th>
                        <th>Valor</th>

                    <tbody id="contenedorConfiguracionDetalle"></tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::button('Generar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'generardiseño()']) !!}


@endsection
