@extends('layouts.principal')
@section('nombreModulo')
    Bitácora manual
@endsection
@section('scripts')
    <script>
        let bitacoraManualAsistente = '<?php echo json_encode($bitacoraManualAsistente); ?>'
        let bitacoraManualPendiente = '<?php echo json_encode($bitacoraManualPendiente); ?>'
        let idResponsable = '<?php echo json_encode($idResponsable); ?>'
        let nombreResponsable = '<?php echo json_encode($nombreResponsable); ?>'
    </script>
    {{ Html::script('modules/plantrabajo/js/bitacoraManualForm.js') }}
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@4.0.0/dist/signature_pad.umd.min.js"></script>
@endsection
@section('contenido')
    {!! Form::model($cliente, [
        'url' => ['plantrabajo/bitacoramanual'],
        'method' => 'POST',
        'id' => 'form-bitacoramanual',
    ]) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txNombreActividadBitacoraManual', 'Actividad', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::text('txNombreActividadBitacoraManual', null, [
                            'class' => 'form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Digita el nombre de la actividad',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_oidCliente', 'Cliente', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select('Tercero_oidCliente', $cliente, null, [
                            'class' => 'chosen-select form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Selecciona el cliente',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('daFechaBitacoraManual', 'Fecha', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::date('daFechaBitacoraManual', date('Y-m-d'), ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('hrHoraInicioBitacoraManual', 'Hora inicial', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::time('hrHoraInicioBitacoraManual', date('h:i:s'), [
                            'class' => 'form-control',
                            'style' => 'width:100%',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('hrHoraFinBitacoraManual', 'Hora final', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::time('hrHoraFinBitacoraManual', null, [
                            'class' => 'form-control',
                            'style' => 'width:100%',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('modalidadBitacora', 'Modalidad', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select(
                            'modalidadBitacora',
                            ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                            null,
                            [
                                'class' => 'form-control',
                                'id' => 'modalidadBitacora',
                                'placeholder' => 'Selecciona la modalidad',
                            ],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txObservacionBitacoraManual', 'Observaciones', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::textArea('txObservacionBitacoraManual', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                    </div>
                </div>
            </div>


            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    Pendientes
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionBitacoraManualPendiente.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Responsable</th>
                                <th>Tarea</th>
                                <th>Fecha de vencimiento</th>
                            <tbody id="contenedorBitacoraManualPendiente">
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <br>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    Asistentes
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionBitacoraManualAsistente.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Nombre</th>
                                <th>Tallerista</th>
                            <tbody id="contenedorBitacoraManualAsistente">
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    Firma
                    <div class="div card-body multi-max">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <!-- Área de Firma -->
                                    <canvas id="signature-pad" class="border" width="600" height="200"
                                        style="border: 1px solid #000; border-radius: 5px;"></canvas>
                                    <br>
                                    <!-- Botón para borrar la firma -->
                                    <button type="button" class="btn btn-secondary btn-sm mt-2" id="clear-signature">Borrar
                                        Firma</button>
                                    <!-- Campo oculto para guardar la firma en Base64 -->
                                    {!! Form::hidden('firma_base64', null, ['id' => 'firma_base64']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {!! Form::button('Generar', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'onclick' => 'generarBitacora()',
            ]) !!}
        </div>
    </div>


    {!! Form::close() !!}
@endsection
