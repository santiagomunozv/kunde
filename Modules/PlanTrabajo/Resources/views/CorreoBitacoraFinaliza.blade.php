
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Se ha finalizado una actividad programada</title>
</head>
    <body>
    <p>Le informamos que se ha finalizado la actividad {{$actividad}} realizada por {{$nombreRealiza}} de Kunde en la compañía {{$nombreCompania}}</p>
    </body>
</html>