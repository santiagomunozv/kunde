@extends('layouts.principal')
@section('nombreModulo')
    Ejecución plan de trabajo
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("programacionplantrabajo-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="programacionplantrabajo-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                {{-- @if ($permisos['chAdicionarRolOpcion'])
                            <a class="btn btn-primary btn-sm text-light" href="{!!URL::to('/general/programacionplantrabajo' , ['create'])!!}">
                                <i class="fa fa-plus"></i>
                            </a>
                            @endif --}}
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Compañia</th>
                            <th>Encargado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($programacionplantrabajo as $programacionplantrabajoreg)
                            <tr
                                class="{{ $programacionplantrabajoreg->txEstadoProgramacionPlanTrabajo == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/plantrabajo/ejecucionplantrabajoedit', [
                                                $programacionplantrabajoreg->oidProgramacionPlanTrabajo,
                                                'edit',
                                            ]) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $programacionplantrabajoreg->oidProgramacionPlanTrabajo }}', 'gen_programacionplantrabajo', 'ProgramacionPlanTrabajo')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $programacionplantrabajoreg->oidProgramacionPlanTrabajo }}', 'gen_programacionplantrabajo', 'ProgramacionPlanTrabajo','{{ $programacionplantrabajoreg->txEstadoProgramacionPlanTrabajo }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/programacionplantrabajo', [$programacionplantrabajoreg->oidProgramacionPlanTrabajo]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $programacionplantrabajoreg->oidProgramacionPlanTrabajo }}</td>
                                <td>{{ $programacionplantrabajoreg->Compania }}</td>
                                <td>{{ $programacionplantrabajoreg->Encargado }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Asociados de Negocio</th>
                            <th>Asociados de Negocio</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
