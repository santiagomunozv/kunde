@extends('layouts.principal')

@section('nombreModulo')
    Informes sistema gestión
@endsection

@section('scripts')
<script>
    let sistemaGestionInforme = '<?php echo json_encode($sistemaGestionInforme); ?>';
    
</script>
    {{Html::script("modules/plantrabajo/js/sistemagestioninforme.js")}} 
@endsection

@section('contenido')
  {!! Form::model($cliente, ['url' => ["plantrabajo/sistemagestioninforme"], 'method' => 'POST', 'id' => 'form-sistemagestioninforme'])!!}    
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group ">
                {!!Form::label('Tercero_oidCliente', 'Cliente', array('class' => 'text-md text-primary mb-1 required')) !!}
                {!!Form::select('Tercero_oidCliente',$cliente,null,['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el cliente'])!!}
                </div>
            </div>
        </div>
    
        <div class="div card border-left-primary shadow h-100 py-2">
            <div class="div card-body">
            
                <div class="div card-body multi-max">
                    <table class="table multiregistro table-sm table-hover table-borderless">
                        <thead class="bg-primary text-light">
                            <th width="50px">
                                <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionSistemaGestionInforme.agregarCampos([],'L');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </th>
                            <th>Empleado</th>
                            <th>Cargo</th>
                            <tbody id="contenedorSistemaGestionInforme">
                            </tbody>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    <br>
        {!!Form::button("Generar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"generar()"])!!}
    </div>
</div>    

{!! Form::close() !!}
@endsection

