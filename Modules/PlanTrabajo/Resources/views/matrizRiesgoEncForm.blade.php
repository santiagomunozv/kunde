@extends("layouts.principal")
@section("nombreModulo")
    Matriz riesgo
@endsection
@section("scripts")
    <script>
        let matrizRiesgos = '<?php echo json_encode($matrizRiesgo); ?>';
        let idCargo = '<?php echo json_encode($idCargo); ?>';
        let nombreCargo = '<?php echo json_encode($nombreCargo); ?>';
    </script>
    {{Html::script("modules/plantrabajo/js/matrizRiesgoEnc.js")}}
    
@endsection
@section("contenido")
    @if(isset($matrizRiesgoEnc->oidMatrizRiesgoEnc))
        {!!Form::model($matrizRiesgoEnc,["route"=>["matrizriesgo.update",$matrizRiesgoEnc->oidMatrizRiesgoEnc],"method"=>"PUT", "id"=>"form-matrizriesgoenc" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($matrizRiesgoEnc,["route"=>["matrizriesgo.store",$matrizRiesgoEnc->oidMatrizRiesgoEnc],"method"=>"POST", "id"=>"form-matrizriesgoenc", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidMatrizRiesgoEnc', null, array('id' => 'oidMatrizRiesgoEnc')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('daFechaMatrizRiesgoEnc', 'Fecha', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::date('daFechaMatrizRiesgoEnc', (isset($matrizRiesgoEnc->oidMatrizRiesgoEnc) ? $matrizRiesgoEnc->daFechaMatrizRiesgoEnc : date('Y-m-d')), ['class'=>'form-control', 'readonly'])!!}
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidCliente', 'Cliente', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidCliente', $cliente, null, ['class'=>'chosen-select form-control', 'placeholder'=>'Selecciona el cliente', 'onchange'=> 'consultarMatriz(this.value)', (isset($matrizRiesgoEnc->oidMatrizRiesgoEnc) ? 'disabled' : '')])!!}
                    </div>
                </div>
            </div>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarMatriz" name="eliminarMatriz" value="">
                    <div class="div card-body multi-max" style="overflow: auto">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <tr text-align="center">
                                    <th colspan="4">&nbsp;</th>
                                    <th text-align="center" colspan="2">PELIGROS</th>
                                    <th>&nbsp;</th>
                                    <th text-align="center" colspan="3">CONTROLES EXISTENTES</th>
                                    <th text-align="center" colspan="7">EVALUACIÓN DEL RIESGO</th>
                                    <th text-align="center" colspan="1">VALORACIÓN DEL RIESGO</th>
                                    <th text-align="center" colspan="2">CRITERIOS PARA CONTROLES</th>
                                    <th text-align="center" colspan="5">MEDIDAS DE INTERVENCIÓN</th>
                                </tr>
                                <th width="50px">
                                    {{-- <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionMatriz.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button> --}}
                                </th>
                                <th>Proceso</th>
                                <th>Actividad y/o tarea</th>
                                <th>Rutinaria</th>
                                <th>Clasificación</th>
                                <th>Descripción</th>
                                <th>Efectos posibles</th>
                                <th>Fuente</th>
                                <th>Medio</th>
                                <th>Individuo</th>
                                <th>Nivel de deficiencia</th>
                                <th>Nivel de exposición</th>
                                <th>Nivel de probabilidad</th>
                                <th>Interpretación nivel de probabilidad</th>
                                <th>Nivel de consecuencia</th>
                                <th>Nivel de riesgo e invervención</th>
                                <th>Interpretación nivel de riesgo</th>
                                <th>Aceptabilidad de riesgo</th>
                                <th>N de expuestos</th>
                                <th>Peor consecuencia</th>
                                <th>Eliminación</th>
                                <th>Sustitución</th>
                                <th>Controles de ingeniería</th>
                                <th>Controles administrativos</th>
                                <th>Elementos de protección personal</th>
                                <tbody id="contenedorMatriz">

                                </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            @if(isset($matrizRiesgoEnc->oidMatrizRiesgoEnc))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        