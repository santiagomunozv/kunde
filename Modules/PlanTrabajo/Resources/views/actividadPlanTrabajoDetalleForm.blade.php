
    {!!Form::model($actividadPlanTrabajoDetalle,['url'=>['plantrabajo/ejecucionactividadupdate',$idDetalleEjecucion],'method'=>'PUT', 'id'=>'form-actividad'])!!}
    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
    {!!Form::hidden('ActividadPlanTrabajo_oidActividadPlanTrabajo',$idActividad, array('id' => 'ActividadPlanTrabajo_oidActividadPlanTrabajo')) !!}
    {!!Form::hidden('EjecucionPlanTrabajoDetalle_oidEjecucionPlanTrabajoDetalle',$idDetalleEjecucion, array('id' => 'EjecucionPlanTrabajoDetalle_oidEjecucionPlanTrabajoDetalle')) !!}

    <div class="card-body multi-max">
        <input type="hidden" id="eliminarActividad" name="eliminarActividad" value="">
        <table id="tabla-asistente" class="table multiregistro table-sm table-hover table-borderless">
            <thead>
                <tr>
                    <th class="bg-primary text-light"></th>
                    <th class="bg-primary text-light">Actividad</th>
                    <th class="bg-primary text-light">Cumplido</th>
                </tr>
            </thead>
            <tbody id="contenedorActividadDetalle"></tbody>
        </table>
    </div>
    
    {!!Form::button("Actualizar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabarActividad()"])!!}
