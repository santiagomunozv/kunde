@extends('layouts.principal')
@section('nombreModulo')
    Configuración de inspección
@endsection

@section('scripts')
    <script>
        let configuracionInspeccionTipoInspeccion = '<?php echo json_encode($configuracionInspeccionTipoInspeccion); ?>';
        var oidTipoInspeccion = '<?php echo isset($oidTipoInspeccion) ? $oidTipoInspeccion : ''; ?>';
        var txNombreTipoInspeccion = '<?php echo isset($txNombreTipoInspeccion) ? $txNombreTipoInspeccion : ''; ?>';
        var tipoInspeccionList = [JSON.parse(oidTipoInspeccion), JSON.parse(txNombreTipoInspeccion)];
    </script>
    {{ Html::script('modules/plantrabajo/js/configuracionInspeccionForm.js') }}
@endsection

@section('contenido')
    @if (isset($configuracionInspeccion->oidConfiguracionInspeccion))
        {!! Form::model($configuracionInspeccion, [
            'route' => ['configuracioninspeccion.update', $configuracionInspeccion->oidConfiguracionInspeccion],
            'method' => 'PUT',
            'id' => 'form-configuracioninspeccion',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($configuracionInspeccion, [
            'route' => ['configuracioninspeccion.store', $configuracionInspeccion->oidConfiguracionInspeccion],
            'method' => 'POST',
            'id' => 'form-configuracioninspeccion',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidConfiguracionInspeccion', null, ['id' => 'oidConfiguracionInspeccion']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txNombreConfiguracionInspeccion', 'Nombre', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txNombreConfiguracionInspeccion', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionConfiguracionInspeccion', 'Descripción', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::textarea('txDescripcionConfiguracionInspeccion', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarConfiguracionInspeccion" name="eliminarConfiguracionInspeccion"
                        value="">
                    <div class="div card-body multi-max" style="overflow: auto">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionConfiguracionInspeccion.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Tipo de inspección</th>
                            <tbody id="contenedorConfiguracionInspeccion">

                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            @if (isset($configuracionInspeccion->oidConfiguracionInspeccion))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
