<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe actividades</title>
    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        p.normal {
          font-style: normal;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #3A64AE;
          color: white;
        }
        </style>
        <link rel="stylesheet" href="css/fontawesome.css">
</head>
<body>
    <table id="customers">
        @php
            $reg = 0;
        @endphp
        @while ($reg < count($consulta))
            @php
                $nombreCompania = $consulta[$reg]->txNombreTercero; 
            @endphp
            <tr>
                <th colspan="13">{{$nombreCompania}}</th>
            </tr>
            <tr>
                <th>Actividad/Mes</th>
                <th>Enero</th>
                <th>Febrero</th>
                <th>Marzo</th>
                <th>Abril</th>
                <th>Mayo</th>
                <th>Junio</th>
                <th>Julio</th>
                <th>Agosto</th>
                <th>Septiembre</th>
                <th>Octubre</th>
                <th>Noviembre</th>
                <th>Diciembre</th>
            </tr>
            @while ($reg < count($consulta) && $nombreCompania == $consulta[$reg]->txNombreTercero)
                <tr>
                    <td>{{$consulta[$reg]->txActividadProgramadaConfiguracionPlanTrabajo}}</td>
                    <td>@if($consulta[$reg]->ENERO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->ENERO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->FEBRERO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->FEBRERO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->MARZO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->MARZO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->ABRIL == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->ABRIL == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->MAYO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->MAYO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->JUNIO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->JUNIO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->JULIO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->JULIO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->AGOSTO == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->AGOSTO == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->SEPTIEMBRE == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->SEPTIEMBRE == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->OCTUBRE == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->OCTUBRE == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->NOVIEMBRE == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->NOVIEMBRE == "0")<a></a>@else{{""}} @endif</td>
                    <td>@if($consulta[$reg]->DICIEMBRE == 1)<a><i class="fa fa-check"></i></a>@elseif($consulta[$reg]->DICIEMBRE == "0")<a></a>@else{{""}} @endif</td>
                </tr>
                @php
                    $reg ++;
                @endphp
            @endwhile
        @endwhile
    </table>
</body>
</html>