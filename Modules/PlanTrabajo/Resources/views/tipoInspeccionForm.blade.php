@extends('layouts.principal')
@section('nombreModulo')
    Tipo de inspección
@endsection

@section('scripts')
    <script>
        let tipoInspeccionDetalle = '<?php echo json_encode($tipoInspeccionDetalle); ?>';
    </script>
    {{ Html::script('modules/plantrabajo/js/tipoInspeccionForm.js') }}
@endsection

@section('contenido')
    @if (isset($tipoInspeccion->oidtipoInspeccion))
        {!! Form::model($tipoInspeccion, [
            'route' => ['tipoinspeccion.update', $tipoInspeccion->oidTipoInspeccion],
            'method' => 'PUT',
            'id' => 'form-tipoinspeccion',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($tipoInspeccion, [
            'route' => ['tipoinspeccion.store', $tipoInspeccion->oidTipoInspeccion],
            'method' => 'POST',
            'id' => 'form-tipoinspeccion',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidTipoInspeccion', null, ['id' => 'oidTipoInspeccion']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txNombreTipoInspeccion', 'Nombre', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txNombreTipoInspeccion', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionTipoInspeccion', 'Descripción', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::textarea('txDescripcionTipoInspeccion', null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarTipoInspeccion" name="eliminarTipoInspeccion" value="">
                    <div class="div card-body multi-max" style="overflow: auto">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionTipoInspeccionDetalle.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Nombre</th>
                            <tbody id="contenedorTipoInspeccion">

                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            @if (isset($tipoInspeccion->oidTipoInspeccion))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
