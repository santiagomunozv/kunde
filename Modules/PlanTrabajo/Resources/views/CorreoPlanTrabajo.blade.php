
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Recordatorio Actividad Kunde</title>
</head>
    <body>
    <p>Recuerda que el día {{$fecha}} tienes una cita con {{$encargado}} de Kunde para realizar la actividad {{$actividad}} a las {{$hora}}, durante {{$cantidadHora}} horas</p>
    </body>
</html>