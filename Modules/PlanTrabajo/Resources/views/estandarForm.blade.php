@extends('layouts.principal')
@section('nombreModulo')
    Estándares
@endsection
@section('scripts')
    <script>
        var configuracionEstandares = {!! $detalle !!};
    </script>
    {{ Html::script('modules/plantrabajo/js/estandarForm.js') }}
@endsection
@section('estilos')
@endsection

@section('contenido')
    {!! Form::open(['url' => ['plantrabajo/estandares'], 'method' => 'POST', 'id' => 'form-estandar']) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('dtFechaGeneracionEstandar', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaGeneracionEstandar', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de generación',
                    ]) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('dtFechaConvocatoriaEstandar', 'Fecha convocatoria', ['class' => 'text-md text-primary mb-1 ']) !!}
                    {!! Form::date('dtFechaConvocatoriaEstandar', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de convocatoria',
                    ]) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('lsAnioEstandar', 'Año', [
                        'class' => 'text-sm text-primary mb-1 required',
                    ]) !!}
                    {!! Form::select(
                        'lsAnioEstandar',
                        [
                            '2023' => '2023',
                            '2024' => '2024',
                        ],
                        null,
                        ['class' => 'chosen-select', 'id' => 'lsAnioEstandar', 'placeholder' => 'Seleccione un año'],
                    ) !!}
                </div>

                <div class="col-sm-12">
                    {!! Form::label('lmClienteEstandar', 'Cliente', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('idClientes', $cliente, null, [
                        'class' => 'chosen-select form-control',
                        'id' => 'idClientes',
                        'multiple',
                        'onchange' => 'asignarIdsCliente()',
                    ]) !!}
                    {!! Form::hidden('lmClienteEstandar', null, [
                        'id' => 'lmClienteEstandar',
                    ]) !!}

                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                        </th>
                        <th>
                            <label class="custom-checkbox-container-title">
                                <input class="form-control" type="checkbox" id="option" onchange="checkALL(this)">
                                <span class="checkmark"></span>
                            </label>
                        </th>
                        <th>Estándar</th>

                    <tbody id="contenedorEstandar"></tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::button('Generar', [
        'type' => 'button',
        'class' => 'btn btn-primary',
        'onclick' => 'generarEstandares()',
    ]) !!}
@endsection
