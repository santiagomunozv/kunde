@extends("layouts.principal")
@section("nombreModulo")
    Ejecución plan de trabajo
@endsection
@section("scripts")
<script type="text/javascript">   
    let valores = '<?php echo (isset($ejecucion) ? json_encode($ejecucion) : "");?>';
    // let valoresDetalle = '<?php echo (isset($preguntaDetalle) ? json_encode($preguntaDetalle) : "");?>';
    
    </script>
    {{Html::script("modules/plantrabajo/js/ejecucionPlanTrabajoForm.js")}}
    {{Html::script("modules/plantrabajo/js/bitacora.js")}}
    {{Html::script("modules/plantrabajo/js/ejecucionActividadPlanTrabajo.js")}}
@endsection
@section("contenido")
    {!!Form::model($ejecucionplantrabajo,['url'=>['plantrabajo/ejecucionplantrabajoupdate',$id],'method'=>'PUT', 'id'=>'form-ejecucionplantrabajo'])!!}
    
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidProgramacionPlanTrabajo', null, array('id' => 'oidProgramacionPlanTrabajo')) !!}
            {!!Form::hidden('oidEjecucionPlanTrabajo',(isset($ejecucionplantrabajo[0]) ? $ejecucionplantrabajo[0]["oidEjecucionPlanTrabajo"] : null), array('id' => 'oidEjecucionPlanTrabajo')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroEncargado', 'Encargado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroEncargado',$asn_tercerolista,(isset($ejecucionplantrabajo[0]) ? $ejecucionplantrabajo[0]["Tercero_oidTerceroEncargado"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Encargado', 'disabled'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroRealiza', 'Realiza', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroRealiza',$asn_tercerolistaEmpleado,(isset($ejecucionplantrabajo[0]) ? $ejecucionplantrabajo[0]["Tercero_oidTerceroRealiza"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Realizador'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroCompania', 'Compañia', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroCompania',$asn_tercerolista,(isset($ejecucionplantrabajo[0]) ? $ejecucionplantrabajo[0]["Tercero_oidTerceroCompania"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Compañia', 'disabled'])!!}
                    </div>
                </div>
            </div><br>

            @if($ejecucionplantrabajo[0]['Tercero_oidTerceroRealiza'])
                <div class="card-body multi-max">
                    <table id="tabla-verificacion" class="table multiregistro table-sm table-hover table-borderless">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="bg-primary text-light">Actividad Programada</th>
                                <th class="bg-primary text-light">Fecha Programada</th>
                                <th class="bg-primary text-light">Hora Programada</th>
                                <th class="bg-primary text-light">Cantidad de horas</th>
                                <th class="bg-primary text-light">Fecha Ejecutada</th>
                                <th class="bg-primary text-light">Cumplido</th>
                                <th class="bg-primary text-light">Bitácora</th>
                                <th class="bg-primary text-light">Subtareas</th>
                            </tr>
                        </thead>
                        <tbody id="contenedorActividad"></tbody>
                    </table>
                </div>
            @endif

            {!!Form::button("Guardar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
        </div>
    </div>
@endsection
        