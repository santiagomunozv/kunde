@extends('layouts.principal')
@section('nombreModulo')
    Diseños
@endsection
@section('scripts')
    <script>
        let carpetas = '<?php echo json_encode($carpetas); ?>';
    </script>
    {{ Html::script('modules/plantrabajo/js/disenoForm.js') }}
@endsection
@section('estilos')
@endsection

@section('contenido')

    {!! Form::open(['url' => ['plantrabajo/diseno'], 'method' => 'POST', 'id' => 'form-diseno']) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('Documento_iodDocumento', 'Tipo de Documento', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::select('Documento_iodDocumento[]', $documento, null, ['class' => 'chosen-select', 'id' => 'Documento_iodDocumento']) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('Empleado_oidEmpleado', 'Empleado', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::select('Empleado_oidEmpleado', $empleado, null, ['class' => 'form-control', 'id' => 'Empleado_oidEmpleado', 'placeholder' => 'Selecciona un Empleado']) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('Cliente_oidCliente', 'Cliente', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::select('Cliente_oidCliente', $cliente, null, ['class' => 'form-control', 'id' => 'Cliente_oidCliente', 'placeholder' => 'Selecciona un Cliente']) !!}
                </div>
            </div>
        </div>
        <div class="card-body">
            {{-- <input type="hidden" id="eliminarConfiguracion" name="eliminarConfiguracion" value=""> --}}
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            {{-- <button type="button" class="btn btn-primary btn-sm text-light"
                                onclick="configuracionCarpetas.agregarCampos([],'L');">
                                <i class="fa fa-plus"></i>
                            </button> --}}
                        </th>
                        <th>Nombre de carpeta</th>
                        <th>Orden</th>

                    <tbody id="contenedorconfiguracionCarpeta"></tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {{-- {!! Form::button('Generar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'generardiseño()']) !!} --}}
    {!! Form::submit('Generar', ['type' => 'button', 'class' => 'btn btn-primary', 'id' => 'btnGenerar']) !!}


@endsection
