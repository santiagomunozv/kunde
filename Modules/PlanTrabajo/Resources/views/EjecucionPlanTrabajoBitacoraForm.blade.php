
    {!!Form::model($bitacora,['url'=>['plantrabajo/bitacoraupdate',$id],'method'=>'PUT', 'id'=>'form-bitacora'])!!}
    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
    {!!Form::hidden('oidEjecucionPlanTrabajoBitacora',(isset($bitacora[0]) ? $bitacora[0]["oidEjecucionPlanTrabajoBitacora"] : null), array('id' => 'oidEjecucionPlanTrabajoBitacora')) !!}
    {!!Form::hidden('EjecucionPlanTrabajoDetalle_oidEjecucionDetalle',$id, array('id' => 'EjecucionPlanTrabajoDetalle_oidEjecucionDetalle')) !!}
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group ">
            {!!Form::label('fechaPlanTrabajoBitacora', 'Fecha', array('class' => 'text-md text-primary mb-1 required ')) !!}
            {!!Form::date('fechaPlanTrabajoBitacora',(isset($bitacora[0]) ? $bitacora[0]["fechaPlanTrabajoBitacora"] : date('Y-m-d')),['class'=>'form-control','style'=>'width:100%','placeholder'=>'Selecciona Encargado'])!!}
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group ">
            {!!Form::label('horaInicioPlanTrabajoBitacora', 'Hora Inicio', array('class' => 'text-md text-primary mb-1 required ')) !!}
            {!!Form::time('horaInicioPlanTrabajoBitacora',(isset($bitacora[0]) ? $bitacora[0]["horaInicioPlanTrabajoBitacora"] : date('h:i:s')),['class'=>'form-control','style'=>'width:100%','placeholder'=>'Selecciona Encargado'])!!}
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group ">
            {!!Form::label('horaSalidaPlanTrabajoBitacora', 'Hora salida', array('class' => 'text-md text-primary mb-1 required ')) !!}
            {!!Form::time('horaSalidaPlanTrabajoBitacora',(isset($bitacora[0]) ? $bitacora[0]["horaSalidaPlanTrabajoBitacora"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>'Selecciona Encargado'])!!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group ">
            {!!Form::label('observacionPlanTrabajoBitacora', 'Observación', array('class' => 'text-md text-primary mb-1 required ')) !!}
            {!!Form::textArea('observacionPlanTrabajoBitacora',(isset($bitacora[0]) ? $bitacora[0]["observacionPlanTrabajoBitacora"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>'Digite una observación'])!!}
            </div>
        </div>
    </div>

    
    <div class="card-body multi-max">
        <input type="hidden" id="eliminarPendientes" name="eliminarPendientes" value="">
        Pendientes
        <table id="tabla-pendientes" class="table multiregistro table-sm table-hover table-borderless">
            <thead>
                <tr>
                    <th class="bg-primary text-light">
                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="pendientes.agregarCampos([],'A');">
                            <i  class="fa fa-plus"></i>
                        </button>
                    </th>
                    <th class="bg-primary text-light">Actividad</th>
                    <th class="bg-primary text-light">Responsable</th>
                    <th class="bg-primary text-light">Fecha de entrega</th>
                </tr>
            </thead>
            <tbody id="contenedorPendientes"></tbody>
        </table>
    </div><br>

    <div class="card-body multi-max">
        <input type="hidden" id="eliminarAsistente" name="eliminarAsistente" value="">
        Asistentes
        <table id="tabla-asistente" class="table multiregistro table-sm table-hover table-borderless">
            <thead>
                <tr>
                    <th class="bg-primary text-light">
                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="asistente.agregarCampos([],'M');">
                            <i  class="fa fa-plus"></i>
                        </button>
                    </th>
                    <th class="bg-primary text-light">Nombre</th>
                </tr>
            </thead>
            <tbody id="contenedorAsistente"></tbody>
        </table>
    </div>
    
    {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabarBitacora()"])!!}
    {!!Form::button("Imprimir",["type" => "button" ,"class"=>"btn btn-info", "onclick"=>"imprimirBitacora()"])!!}

