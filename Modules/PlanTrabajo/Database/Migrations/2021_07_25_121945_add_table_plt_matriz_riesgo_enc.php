<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddTablePltMatrizRiesgoEnc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plt_matrizriesgoenc', function (Blueprint $table) {
            $table->increments('oidMatrizRiesgoEnc');
            $table->integer('Tercero_oidCliente')->comment('Id del cliente');
            $table->integer('Tercero_oidEmpleado')->comment('id del usuario');
            $table->date('daFechaMatrizRiesgoEnc')->comment('Fecha de la matriz');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>19,
            "txNombreOpcion" =>"Matriz de riesgo",
            "txRutaOpcion" => "/plantrabajo/matrizriesgo"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plt_matrizriesgoenc');
    }
}
