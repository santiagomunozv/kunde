<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionBitacoraManual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>19,
            "txNombreOpcion" =>"Bitácora manual",
            "txRutaOpcion" => "/plantrabajo/bitacoramanual"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>19,
            "txNombreOpcion" =>"Sistema gestión",
            "txRutaOpcion" => "/plantrabajo/sistemagestioninforme"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
