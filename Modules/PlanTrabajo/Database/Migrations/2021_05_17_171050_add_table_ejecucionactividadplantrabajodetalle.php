<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableEjecucionactividadplantrabajodetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plt_ejecucionactividadplantrabajodetalle', function (Blueprint $table) {
            $table->increments('oidEjecucionActividadPlanTrabajoDetalle');
            $table->integer('EjecucionPlanTrabajoDetalle_oidEjecucionPlanTrabajoDetalle')->comment('Id plan trabajo detalle');
            $table->integer('ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')->comment('Id actividad trabajo detalle');
            $table->boolean('chEjecutadoEjecucionActividadPlanTrabajoDetalle')->comment('Ejecutado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plt_ejecucionactividadplantrabajodetalle');
    }
}
