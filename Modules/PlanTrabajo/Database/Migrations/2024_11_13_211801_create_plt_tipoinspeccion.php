<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePltTipoinspeccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('seg_opcion')->insert([
            "Modulo_oidModulo_1aM" => 19,
            "txNombreOpcion" => "Tipo de inspección",
            "txRutaOpcion" => "/plantrabajo/tipoinspeccion"
        ]);

        DB::table('seg_opcion')->insert([
            "Modulo_oidModulo_1aM" => 19,
            "txNombreOpcion" => "Configuración inspección",
            "txRutaOpcion" => "/plantrabajo/configuracioninspeccion"
        ]);

        DB::table('seg_opcion')->insert([
            "Modulo_oidModulo_1aM" => 19,
            "txNombreOpcion" => "Inspección",
            "txRutaOpcion" => "/plantrabajo/inspeccion"
        ]);

        Schema::create('plt_tipoinspeccion', function (Blueprint $table) {
            $table->increments('oidTipoInspeccion');
            $table->string('txNombreTipoInspeccion')->comment('Nombre tipo de inspección');
            $table->string('txDescripcionTipoInspeccion')->comment('Descripción tipo de inspección');
        });

        Schema::create('plt_tipoinspecciondetalle', function (Blueprint $table) {
            $table->increments('oidTipoInspeccionDetalle');
            $table->unsignedInteger('TipoInspeccion_oidTipoInspeccion')->comment('Id del tipo de inspección');
            $table->foreign('TipoInspeccion_oidTipoInspeccion', 'FK_TipoInspeccion_oid_tid')->references('oidTipoInspeccion')->on('plt_tipoinspeccion');
            $table->string('txNombreTipoInspeccionDetalle')->comment('Nombre tipo de inspección detalle');
        });

        Schema::create('plt_configuracioninspeccion', function (Blueprint $table) {
            $table->increments('oidConfiguracionInspeccion');
            $table->string('txNombreConfiguracionInspeccion')->comment('Nombre configuración inspección');
            $table->string('txDescripcionConfiguracionInspeccion')->comment('Descripción configuración inspección');
        });

        Schema::create('plt_configuracioninspecciontipoinspeccion', function (Blueprint $table) {
            $table->increments('oidConfiguracionInspeccionTipoInspeccion');
            $table->unsignedInteger('ConfiguracionTipoInspeccion_oidConfiguracionTipoInspeccion')->comment('Id de la configuración de inspección');
            $table->foreign('ConfiguracionTipoInspeccion_oidConfiguracionTipoInspeccion', 'FK_ConfiguracionTipoInspeccion_oid_citi')->references('oidConfiguracionInspeccion')->on('plt_configuracioninspeccion');
            $table->unsignedInteger('TipoInspeccion_oidTipoInspeccion')->comment('Id del tipo de inspección');
            $table->foreign('TipoInspeccion_oidTipoInspeccion', 'FK_TipoInspeccion_oid_citi')->references('oidTipoInspeccion')->on('plt_tipoinspeccion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plt_tipoinspecciondetalle');
        Schema::dropIfExists('plt_tipoinspeccion');

        Schema::dropIfExists('plt_configuracioninspeccion');
        Schema::dropIfExists('plt_configuracioninspecciontipoinspeccion');
    }
}
