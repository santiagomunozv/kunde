<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePltMatrizRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plt_matrizriesgo', function (Blueprint $table) {
            $table->increments('oidMatrizRiesgo');
            $table->integer('MatrizRiesgoEnc_oidMatrizRiesgoEnc')->comment('Id de la matriz encabezado');
            $table->integer('Cargo_oidCargo')->comment('Id del cargo');
            $table->string('txActividadCargoMatrizRiesgo')->comment('Actividad del cargo');
            $table->string('lsRutinariaMatrizRiesgo')->comment('Es rutinaria');
            $table->string('lsClasificacionMatrizRiesgo')->comment('Clasificazión de peligros');
            $table->string('txDescripcionClasificacionMatrizRiesgo')->comment('Descripci´n clasificación de peligros');
            $table->string('txEfectosPosiblesMatrizRiesgo')->comment('Efectos posibles');
            $table->string('txFuenteMatrizRiesgo')->comment('Fuente');
            $table->string('txMedioMatrizRiesgo')->comment('Medio');
            $table->string('txIndividuoMatrizRiesgo')->comment('Individuo');
            $table->string('lsNivelDeficienciaMatrizRiesgo')->comment('Nivel deficiencia');
            $table->string('lsNivelExposicionMatrizRiesgo')->comment('Nivel exposición');
            $table->string('txNivelProbabilidadMatrzRiesgo')->comment('Nivel probabilidad');
            $table->string('txInterpretacionProbabilidadMatrizRiesgo')->comment('Interpretación probabilidad');
            $table->string('lsNivelConsecuenciaMatrizRiesgo')->comment('Nivel consecuencia');
            $table->string('txNivelRiesgoMatrizRiesgo')->comment('Nivel riesgo');
            $table->string('txInterpretacionRiesgoMatrizRiesgo')->comment('Interpretación riesgo');
            $table->string('txAceptabilidadRiesgoMatrizRiesgo')->comment('Aceptabilidad');
            $table->integer('inExpuestosMatrizRiesgo')->comment('Expuestos');
            $table->string('txPeorConsecuenciaMatrizRiesgo')->comment('Peor consecuencia');
            $table->string('txEliminacionMatrizRiesgo')->comment('Eliminación');
            $table->string('txSustitucionMatrizRiesgo')->comment('Sustitución');
            $table->string('txControlIngenieria')->comment('Control ingeniería');
            $table->string('txControlAdministrativoMatrizRiesgo')->comment('Control administrativo');
            $table->string('txElementosProteccionPersonalMatrizRiesgo')->comment('Elementos de protección personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plt_matrizriesgo');
    }
}
