<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableSistemaGestionInforme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plt_sistemagestioninforme', function (Blueprint $table) {
            $table->increments('oidSistemaGestionInforme');
            $table->string('nombreSistemaGestionInforme', 255)->comment("Nombre del informe plan de gestión");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plt_sistemagestioninforme');
    }
}
