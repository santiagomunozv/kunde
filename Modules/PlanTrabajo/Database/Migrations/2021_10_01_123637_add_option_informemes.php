<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionInformemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('seg_opcion')->insert([
            "Modulo_oidModulo_1aM" => 19,
            "txNombreOpcion" => "Informe plan trabajo cliente",
            "txRutaOpcion" => "plantrabajo/informePlanTrabajoCliente"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {
        });
    }
}
