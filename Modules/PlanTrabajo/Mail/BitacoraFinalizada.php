<?php

namespace Modules\PlanTrabajo\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BitacoraFinalizada extends Mailable
{
    use Queueable, SerializesModels;

    public $correoRealiza;
    public $nombreRealiza;
    public $correoCompania;
    public $nombreCompania;
    public $actividad;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($correoRealiza,$nombreRealiza,$correoCompania,$nombreCompania,$actividad)
    {
        $this->encargcorreoRealizaado = $correoRealiza;
        $this->nombreRealiza = $nombreRealiza;
        $this->correoCompania = $correoCompania;
        $this->nombreCompania = $nombreCompania;
        $this->actividad = $actividad;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('plantrabajo::CorreoBitacoraFinaliza');
    }
}
