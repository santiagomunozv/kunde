<?php

namespace Modules\PlanTrabajo\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EjecucionActividad extends Mailable
{
    use Queueable, SerializesModels;

    public $actividad;
    public $encargado;
    public $compania;
    public $fecha;
    public $hora;
    public $cantidadHora;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($actividad,$encargado,$compania,$fecha,$hora,$cantidadHora)
    {
        $this->actividad = $actividad;
        $this->encargado = $encargado;
        $this->compania = $compania;
        $this->fecha = $fecha;
        $this->hora = $hora;
        $this->cantidadHora = $cantidadHora;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('plantrabajo::CorreoPlanTrabajo');
    }
}
