<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class MatrizRiesgoEncModel extends Model
{
    protected $table = 'plt_matrizriesgoenc';
    protected $primaryKey = 'oidMatrizRiesgoEnc';
    protected $fillable = ['Tercero_oidCliente', 'Tercero_oidEmpleado', 'daFechaMatrizRiesgoEnc'];
    public $timestamps = false;

}