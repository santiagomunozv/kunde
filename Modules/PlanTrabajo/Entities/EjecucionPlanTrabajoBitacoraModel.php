<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class EjecucionPlanTrabajoBitacoraModel extends Model
{
    protected $table = 'plt_ejecucionplantrabajobitacora';
    protected $primaryKey = 'oidEjecucionPlanTrabajoBitacora';
    protected $fillable = ['EjecucionPlanTrabajoDetalle_oidEjecucionDetalle','fechaPlanTrabajoBitacora','horaInicioPlanTrabajoBitacora','horaSalidaPlanTrabajoBitacora','observacionPlanTrabajoBitacora'];
    public $timestamps = false;

}