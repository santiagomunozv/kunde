<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class BitacoraAsistenteModel extends Model
{
    protected $table = 'plt_bitacoraasistente';
    protected $primaryKey = 'oidBitacoraAsistente';
    protected $fillable = ['PlanTrabajoBitacora_oidPlanTrabajoBitacora','txNombreAsistente'];
    public $timestamps = false;

}