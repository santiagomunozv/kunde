<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class MatrizRiesgoModel extends Model
{
    protected $table = 'plt_matrizriesgo';
    protected $primaryKey = 'oidMatrizRiesgo';
    protected $fillable = ['MatrizRiesgoEnc_oidMatrizRiesgoEnc', 'Cargo_oidCargo', 'txActividadCargoMatrizRiesgo','lsRutinariaMatrizRiesgo', 'lsClasificacionMatrizRiesgo', 'txDescripcionClasificacionMatrizRiesgo', 'txEfectosPosiblesMatrizRiesgo', 'txFuenteMatrizRiesgo', 'txMedioMatrizRiesgo', 'txIndividuoMatrizRiesgo', 'lsNivelDeficienciaMatrizRiesgo', 'lsNivelExposicionMatrizRiesgo', 'txNivelProbabilidadMatrzRiesgo', 'txInterpretacionProbabilidadMatrizRiesgo', 'lsNivelConsecuenciaMatrizRiesgo', 'txNivelRiesgoMatrizRiesgo', 'txInterpretacionRiesgoMatrizRiesgo', 'txAceptabilidadRiesgoMatrizRiesgo', 'inExpuestosMatrizRiesgo', 'txPeorConsecuenciaMatrizRiesgo', 'txEliminacionMatrizRiesgo', 'txSustitucionMatrizRiesgo', 'txControlIngenieria', 'txControlAdministrativoMatrizRiesgo', 'txElementosProteccionPersonalMatrizRiesgo'];
    public $timestamps = false;

}