<?php

namespace Modules\PlanTrabajo\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoInspeccionDetalleModel extends Model
{
    protected $table = 'plt_tipoinspecciondetalle';

    protected $primaryKey = 'oidTipoInspeccionDetalle';

    protected $fillable = [
        'TipoInspeccion_oidTipoInspeccion',
        'txNombreTipoInspeccionDetalle',
    ];

    public $timestamps = false;
}
