<?php

namespace Modules\PlanTrabajo\Entities;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionInspeccionTipoInspeccionModel extends Model
{
    protected $table = 'plt_configuracioninspecciontipoinspeccion';

    protected $primaryKey = 'oidConfiguracionInspeccionTipoInspeccion';

    protected $fillable = [
        'ConfiguracionTipoInspeccion_oidConfiguracionTipoInspeccion',
        'TipoInspeccion_oidTipoInspeccion',
    ];

    public $timestamps = false;
}
