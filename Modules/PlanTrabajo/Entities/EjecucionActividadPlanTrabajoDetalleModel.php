<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class EjecucionActividadPlanTrabajoDetalleModel extends Model
{
    protected $table = 'plt_ejecucionactividadplantrabajodetalle';
    protected $primaryKey = 'oidEjecucionActividadPlanTrabajoDetalle';
    protected $fillable = ['EjecucionPlanTrabajoDetalle_oidEjecucionPlanTrabajoDetalle','ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle', 'chEjecutadoEjecucionActividadPlanTrabajoDetalle'];
    public $timestamps = false;

}