<?php

namespace Modules\PlanTrabajo\Entities;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionInspeccionModel extends Model
{
    protected $table = 'plt_configuracioninspeccion';
    protected $primaryKey = 'oidConfiguracionInspeccion';
    protected $fillable = ['txNombreConfiguracionInspeccion', 'txDescripcionConfiguracionInspeccion'];
    public $timestamps = false;
}
