<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class EjecucionPlanTrabajoDetalleModel extends Model
{
    protected $table = 'plt_ejecucionplantrabajodetalle';
    protected $primaryKey = 'oidEjecucionPlanTrabajoDetalle';
    protected $fillable = ['EjecucionPlanTrabajo_oidEjecucionPlanTrabajo','ActividadPlanTrabajo_oidActividadPlanTrabajo','daFechaEjecucionPlanTrabajoDetalle','chCumplidoEjecucionPlanTrabajoDetalle'];
    public $timestamps = false;

}