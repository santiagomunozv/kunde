<?php

namespace Modules\PlanTrabajo\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoInspeccionModel extends Model
{
    protected $table = 'plt_tipoinspeccion';
    protected $primaryKey = 'oidTipoInspeccion';
    protected $fillable = ['txNombreTipoInspeccion', 'txDescripcionTipoInspeccion'];
    public $timestamps = false;
}
