<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class BitacoraPendienteModel extends Model
{
    protected $table = 'plt_bitacorapendiente';
    protected $primaryKey = 'oidBitacoraPendiente';
    protected $fillable = ['PlanTrabajoBitacora_oidPlanTrabajoBitacora','txBitacoraActividad','Tercero_oidBitacoraResponsable','daBitacoraFehcaEntrega'];
    public $timestamps = false;

}