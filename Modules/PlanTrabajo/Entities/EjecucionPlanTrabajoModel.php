<?php
namespace Modules\PlanTrabajo\Entities;
use Illuminate\Database\Eloquent\Model;

class EjecucionPlanTrabajoModel extends Model
{
    protected $table = 'plt_ejecucionplantrabajo';
    protected $primaryKey = 'oidEjecucionPlanTrabajo';
    protected $fillable = ['ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo','Tercero_oidTerceroRealiza'];
    public $timestamps = false;

}