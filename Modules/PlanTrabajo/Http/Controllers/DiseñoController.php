<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use PhpOffice\PhpWord\TemplateProcessor;
use ZipArchive;
use File;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Zipper;


// necesario para obtener fechas en español
setlocale(LC_ALL, "es_Es");

class DiseñoController extends Controller
{
    // AL MODIFICAR LOS NOMBRES SE DEBE MODIFICAR LA PLANTILLA
    protected $documentosLista = [
        'TODOS LOS DOCUMENTOS',
        'ACTA DE NOMBRAMIENTO DEL RS-GST',
        'ACTA CONOCIMIENTO FUNCIONES Y RESPONSABILIDADES ADMINISTRATIVO',
        'ASIGNACIÓN DE RECURSO DEL SG-SST',
        'EN BLANCO',
        'EN BLANCO',
        'ACTA DE NOMBRAMIENTO MIEMBRO DEL COMITÉ PARITARIO DE SST',
        'FUNCIONES MIEMBRO DEL COMITÉ PARITARIO DE SST',
        'CONVOCATORIA COMITÉ DE CONVIVENCIA LABORAL',
        'ACUERDO DE CONFIDENCIALIDAD COMITÉ DE CONVIVENCIA LABORAL',
    ];

    public function index()
    {
        $cliente = TerceroModel::getTerceroAcoordingTypeAndState('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $empleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        $documento = $this->documentosLista;
        $carpetas = [
            [
                "carpetaId" => "0",
                "carpetaName" => "Nombre del cliente",
                "carpetaOrden" => "0"
            ],
            [
                "carpetaId" => "1",
                "carpetaName" => "1.1.1 Responsable del sistema de gestión de seguridad y salud en el trabajo",
                "carpetaOrden" => "1"
            ],
            [
                "carpetaId" => "2",
                "carpetaName" => "1.1.2 Responsabilidades en el sistema de gestión de seguridad y salud en el trabajo",
                "carpetaOrden" => "2"
            ],
            [
                "carpetaId" => "3",
                "carpetaName" => " 1.1.3 Asignacion de recursos para el sistema de gestion de seguridad y salud en el trabajo",
                "carpetaOrden" => "3"
            ],
            [
                "carpetaId" => "4",
                "carpetaName" => " 1.1.4 Afiliación al sistema General",
                "carpetaOrden" => "4"
            ],
            [
                "carpetaId" => "5",
                "carpetaName" => "1.1.5 Pago de pesión trabajadores ",
                "carpetaOrden" => "5"
            ],
            [
                "carpetaId" => "6",
                "carpetaName" => " 1.1.6 - 1.1.7 Acta de nombramiento del COPASST",
                "carpetaOrden" => "6"
            ],
            [
                "carpetaId" => "7",
                "carpetaName" => "1.1.8 Acta de nombramiento del COCOLA ",
                "carpetaOrden" => "7"
            ],
            [
                "carpetaId" => "8",
                "carpetaName" => " 1.2.1 Programa capacitación prom...",
                "carpetaOrden" => "8"
            ],
            [
                "carpetaId" => "9",
                "carpetaName" => "1.2.2 Capacitación, inducción y rei... ",
                "carpetaOrden" => "9"
            ],
            [
                "carpetaId" => "10",
                "carpetaName" => "1.2.3 Responsables del sistema de... ",
                "carpetaOrden" => "10"
            ],
            [
                "carpetaId" => "11",
                "carpetaName" => "",
                "carpetaOrden" => "11"
            ]

        ];

        return view('plantrabajo::disenoForm', compact('cliente', 'empleado', 'documento', 'carpetas'));
    }

    public function store(Request $request)
    {
        $idCliente = $request->get('Cliente_oidCliente');
        $idEmpleado = $request->get('Empleado_oidEmpleado');
        $documentos = $request->get('Documento_iodDocumento');
        $carpetasOrden = $request->get('carpetaOrden');
        $carpetasNombre = $request->get('carpetaName');

        $cliente = TerceroModel::find($idCliente);
        $empleado = TerceroModel::find($idEmpleado);

        $numeroDocumentos =  count($this->documentosLista);

        if (!file_exists(public_path('documentosGenerados/' . $cliente->txNombreTercero))) {
            $path = public_path('documentosGenerados/' . $cliente->txNombreTercero);
            mkdir($path, 0777, true);
        } else {
            $path = public_path('documentosGenerados/' . $cliente->txNombreTercero);
        }

        foreach ($documentos as $orden) {

            switch ($orden) {
                case '0':   //carpeta padre
                    // if (!file_exists(public_path('documentosGenerados/' . $cliente->txNombreTercero))) {
                    //     // $path = public_path('documentosGenerados/' . '/' . $cliente->txNombreTercero);
                    //     // mkdir($path, 0777, true);
                    // }
                    $suiche = false;
                    for ($i = 1; $i < $numeroDocumentos + 1; $i++) {
                        $ruta = $path . '/' . $carpetasNombre[$i];

                        if (!file_exists($ruta)) {
                            mkdir($ruta, 0777, true);
                        }

                        $allDocumentos[$i] = "$i";

                        if ($allDocumentos[$i] == 7) {
                            $ruta = $path . '/' . $carpetasNombre[$i - 1];
                        }
                        if ($allDocumentos[$i] == 9) {
                            $ruta = $path . '/' . $carpetasNombre[$i - 1];
                            $suiche = true;
                        }

                        $this->documentoSeleccionado(
                            $allDocumentos,
                            $suiche ? $i - 2 : $i,
                            $ruta,
                            $cliente,
                            $empleado
                        );
                    }

                    break;

                case '1':   //1.1.1 Responsable del sistema de gestión de seguridad y salud en el trabajo
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }

                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;

                case '2':   //1.1.2 Responsabilidades en el sistema de gestión de seguridad y salud en el trabajo
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '3':   //1.1.3 Asignacion de recursos para el sistema de gestion de seguridad y salud en el trabajo
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }

                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '4':   //1.1.4 Afiliación al sistema General
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '5':   //1.1.5 Pago de pesión trabajadores
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '6':   //1.1.6 - 1.1.7 Acta de nombramiento del COPASST
                    $ruta = $path . '/' . $carpetasNombre[$orden];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '7':   //1.1.6 - 1.1.7 Acta de nombramiento del COPASST
                    $ruta = $path . '/' . $carpetasNombre[$orden - 1]; //Este archivo va en la misma carpeta que el anterior
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    // dd($carpetasNombre[$orden - 1]);
                    break;
                case '8':   //1.1.8 Acta de nombramiento del COCOLA"
                    $ruta = $path . '/' . $carpetasNombre[$orden - 1];

                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '9':   //1.1.8 Acta de nombramiento del COCOLA"
                    $ruta = $path . '/' . $carpetasNombre[$orden - 2];  //Este archivo va en la misma carpeta que el anterior
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '10':   //1.2.2"
                    $ruta = $path . '/' . $carpetasNombre[$orden - 2];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;
                case '11':   //1.2.3"
                    $ruta = $path . '/' . $carpetasNombre[$orden - 2];
                    if (!file_exists($ruta)) {
                        mkdir($ruta, 0777, true);
                    }
                    $this->documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado);
                    break;

                default:
                    # code...
                    break;
            }
        }

        $nombreZip = str_replace(' ', '', rtrim($cliente->txNombreTercero, ".") . '.zip');
        // dd($nombreZip);

        // comprimir archivos
        $this->comprimirArchivos($cliente, $nombreZip);

        //eliminar archivos
        $this->limpiarArchivos(public_path('documentosGenerados/' . $cliente->txNombreTercero));

        // return response([$cliente->txNombreTercero], 200);
        return response()->download(public_path('zipGenerados/' . $nombreZip))->deleteFileAfterSend(true);
    }

    // public function downloadZip($folder)
    // {
    //     return response()->download(public_path('zipGenerados/' . $folder . '.zip'));
    // }


    public function guardarDocumento($documento, $empleado, $cliente, $ruta)
    {
        $documentos = $this->documentosLista;

        foreach ($documentos as $conteo => $nombre) {

            if ($conteo == $documento) {
                $rutaPlantilla = public_path('plantillasDiseño/' . $nombre . '.docx');

                $templateProcessor = new TemplateProcessor($rutaPlantilla);
                // convertimos la fecha en un array donde la posición 0 es el dia, etc
                $fecha = explode('-', strftime("%d-%B-%Y-%r"));

                // asignar valores a la plantilla
                $templateProcessor->setValues(array(
                    'txDocumentoTerceroEmpleado' => $this->quitarEspacios($empleado->txDocumentoTercero),
                    'txDocumentoTerceroCliente' => $this->quitarEspacios($cliente->txDocumentoTercero),
                    'txNombreTerceroEmpleado' => $this->quitarEspacios($empleado->txNombreTercero),
                    'txNombreTerceroCliente' => $this->quitarEspacios($cliente->txNombreTercero),
                    'txFechaDia' => $fecha[0],
                    'txFechaMes' => $fecha[1],
                    'txFechaAño' => $fecha[2],
                    'txFechaHora' => $fecha[3],
                    'txLoquesea' 
                ));

                $templateProcessor->saveAs($ruta . '/' . $nombre . '.docx');
            }
        }
    }

    function quitarEspacios($cadena)
    {
        // Quita los espacios de sobra en las cadenas
        return preg_replace(['/\s+/', '/^\s|\s$/'], [' ', ''], $cadena);
    }

    public function documentoSeleccionado($documentos, $orden, $ruta, $cliente, $empleado)
    {
        // busca la posicion  y el valor del documento en el array
        if (in_array($orden, (array) $documentos)) {

            $posicion = array_search($orden, $documentos);
            // dd($documentos[$posicion]);
            $this->guardarDocumento($documentos[$posicion], $empleado, $cliente, $ruta);
        }
    }

    public function limpiarArchivos($carpeta)
    {
        function rmDir_rf($carpeta)
        {
            foreach (glob($carpeta . "/*") as $archivos_carpeta) {
                if (is_dir($archivos_carpeta)) {
                    rmDir_rf($archivos_carpeta);
                } else {
                    unlink($archivos_carpeta);
                }
            }
            rmdir($carpeta);
        }

        rmDir_rf($carpeta);
    }

    public function comprimirArchivos($cliente, $nombreZip)
    {
        // $zipper = new Zipper;
        // $rutaZip = public_path('zipGenerados' . DIRECTORY_SEPARATOR . 'LITOGRAFÍA, TIPOGRAFÍA Y PAPELERÍA MEDELLÍN S.A.S.' . '.zip');

        // dd('zipGenerados' . DIRECTORY_SEPARATOR . $nombreZip);
        $files = glob(public_path('documentosGenerados/'));
        Zipper::make(public_path('zipGenerados' . DIRECTORY_SEPARATOR . $nombreZip))->add($files)->close();

        // $resultado = $zip->close();
        // if ($resultado) {
        //     echo "Archivo creado";
        // } else {
        //     echo "Error creando archivo";
        // }


        // if (headers_sent()) die("**Error: headers sent");
        // $nombre = basename($nombreArchivoZip);
        // header("Content-length: " . filesize($nombreArchivoZip));
        // header('Content-Type:  application/zip');
        // header("Content-disposition: attachment; filename=$nombre");
        // header("Content-Transfer-Encoding: Binary");
        // ob_clean();
        // flush();
        // // Leer el contenido binario del zip y enviarlo
        // readfile($nombreArchivoZip);

        // // eliminar zip:
        // unlink($nombreArchivoZip);





        // Pruba 1 _________________________________________________________________________________________________________
        // $zip = new ZipArchive();
        // $rutaDelDirectorio = public_path('documentosGenerados/');
        // $nombreArchivoZip = public_path('zipGenerados' . DIRECTORY_SEPARATOR . $cliente->txNombreTercero . '.zip');

        // // dd($nombreArchivoZip);
        // if (!$zip->open($nombreArchivoZip, ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
        //     exit("Error abriendo ZIP en $nombreArchivoZip");
        // }

        // $archivos = new RecursiveIteratorIterator(
        //     new RecursiveDirectoryIterator($rutaDelDirectorio),
        //     RecursiveIteratorIterator::LEAVES_ONLY
        // );

        // foreach ($archivos as $archivo) {
        //     if ($archivo->isDir()) {
        //         continue;
        //     }
        //     $rutaAbsoluta = $archivo->getRealPath();
        //     $nombreArchivo = substr($rutaAbsoluta, strlen($rutaDelDirectorio) + 1);
        //     $zip->addFile($rutaAbsoluta, $nombreArchivo);
        // }
        // $resultado = $zip->close();
        // if ($resultado) {
        //     echo "Archivo creado";
        // } else {
        //     echo "Error creando archivo";
        // }


        // if (headers_sent()) die("**Error: headers sent");
        // $nombre = basename($nombreArchivoZip);
        // header("Content-length: " . filesize($nombreArchivoZip));
        // header('Content-Type:  application/zip');
        // header("Content-disposition: attachment; filename=$nombre");
        // header("Content-Transfer-Encoding: Binary");
        // ob_clean();
        // flush();
        // // Leer el contenido binario del zip y enviarlo
        // readfile($nombreArchivoZip);

        // // eliminar zip:
        // unlink($nombreArchivoZip);

        // Prueba 2_________________________________________________________________________________________
        // // Get real path for our folder
        // $rootPath = realpath(public_path('documentosGenerados/'));
        // $nombreRutaZip = public_path('zipGenerados' . DIRECTORY_SEPARATOR . $cliente->txNombreTercero . '.zip');

        // // Initialize archive object
        // $zip = new ZipArchive();
        // $zip->open($nombreRutaZip, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // // Create recursive directory iterator
        // /** @var SplFileInfo[] $files */
        // $files = new RecursiveIteratorIterator(
        //     new RecursiveDirectoryIterator($rootPath),
        //     RecursiveIteratorIterator::LEAVES_ONLY
        // );

        // foreach ($files as $name => $file) {
        //     // Skip directories (they would be added automatically)
        //     if (!$file->isDir()) {
        //         // Get real and relative path for current file
        //         $filePath = $file->getRealPath();
        //         $relativePath = substr($filePath, strlen($rootPath) + 1);

        //         // Add current file to archive
        //         $zip->addFile($filePath, $relativePath);
        //     }
        // }

        // // Zip archive will be created only after closing object
        // $zip->close();
    }
}
