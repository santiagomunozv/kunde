<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use PhpOffice\PhpWord\TemplateProcessor;
use ZipArchive;
use File;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Zipper;


class ReporteMinisterioController extends Controller
{
    public function index()
    {
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $empleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        $detalle = [];

        return view('plantrabajo::reporteMinisterioForm', compact('cliente', 'empleado', 'detalle'));
    }

    public function store(Request $request)
    {
        $clienteId = $request->get('Cliente_oidCliente');
        $empleadoId = $request->get('Empleado_oidEmpleado');
        $documentoNombre = $request->get('Documento_iodDocumento');

        $cliente = TerceroModel::find($clienteId);
        $empleado = TerceroModel::find($empleadoId);

        if (!file_exists(public_path('reportesMinisterio/reportesMinisterioGenerados/' . $cliente->txNombreTercero))) {
            $path = public_path('reportesMinisterio/reportesMinisterioGenerados/' . $cliente->txNombreTercero);
            mkdir($path, 0777, true);
        } else {
            abort(409, 'Este documento ya ha sido generado para el cliente seleccionado');
        }

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $this->guardarDocumento($documentoNombre, $empleado, $cliente, $path, $request);
        return response(['message' => 'reportesMinisterio/reportesMinisterioGenerados/' . $cliente->txNombreTercero . "/$documentoNombre.docx"], 200);
    }

    public function guardarDocumento($documentoNombre, $empleado, $cliente, $ruta, $request)
    {
        $rutaPlantilla = public_path('reportesMinisterio/' . $documentoNombre . '.docx');

        $templateProcessor = new TemplateProcessor($rutaPlantilla);
        $fecha = explode('-', strftime("%d-%B-%Y-%r"));

        $templateProcessor->setValues(array(
            'txDocumentoTerceroEmpleado' => $this->quitarEspacios($empleado->txDocumentoTercero),
            'txDocumentoTerceroCliente' => $this->quitarEspacios($cliente->txDocumentoTercero),
            'txNombreTerceroEmpleado' => $this->quitarEspacios($empleado->txNombreTercero),
            'txNombreTerceroCliente' => $this->quitarEspacios($cliente->txNombreTercero),
            'txFechaDia' => $fecha[0],
            'txFechaMes' => $fecha[1],
            'txFechaAño' => $fecha[2],
            'txFechaHora' => $fecha[3],
        ));

        $total = ($request['txCampoBDDetalle'] !== null) ? count($request['txCampoBDDetalle']) : 0;
        for ($i = 0; $i <  $total; $i++) {
            $templateProcessor->setValues(array(
                $request['txCampoBDDetalle'][$i] => $this->quitarEspacios($request['txCampoFormDetalle'][$i]),
            ));
        }

        $templateProcessor->saveAs($ruta . '/' . $documentoNombre . '.docx');
    }

    function quitarEspacios($cadena)
    {
        return preg_replace(['/\s+/', '/^\s|\s$/'], [' ', ''], $cadena);
    }

    function base64($archivo)
    {

        $logo = '&nbsp;';
        $fp = fopen($archivo, "r", 0);
        if ($archivo != '' and $fp) {
            $imagen = fread($fp, filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  '<img src="data:image/png;base64,' . $base64 . '" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }

    public function limpiarArchivos($carpeta)
    {
        function rmDir_rf($carpeta)
        {
            foreach (glob($carpeta . "/*") as $archivos_carpeta) {
                if (is_dir($archivos_carpeta)) {
                    rmDir_rf($archivos_carpeta);
                } else {
                    unlink($archivos_carpeta);
                }
            }
            rmdir($carpeta);
        }

        rmDir_rf($carpeta);
    }
}
