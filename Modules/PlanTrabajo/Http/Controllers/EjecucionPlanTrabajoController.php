<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\General\Entities\ProgramacionPlanTrabajoModel;

use Modules\AsociadoNegocio\Entities\TerceroModel;
use DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Modules\General\Entities\ActividadPlanTrabajoDetalleModel;
use Modules\PlanTrabajo\Entities\BitacoraAsistenteModel;
use Modules\PlanTrabajo\Entities\BitacoraPendienteModel;
use Modules\PlanTrabajo\Entities\EjecucionActividadPlanTrabajoDetalleModel;
use Modules\PlanTrabajo\Entities\EjecucionPlanTrabajoBitacoraModel;
use Modules\PlanTrabajo\Entities\EjecucionPlanTrabajoDetalleModel;
use Modules\PlanTrabajo\Entities\EjecucionPlanTrabajoModel;
use Modules\PlanTrabajo\Mail\BitacoraFinalizada;
use Modules\RegistroSeguimiento\Entities\TareaModel;

class EjecucionPlanTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $programacionplantrabajo = ProgramacionPlanTrabajoModel::orderBy('oidProgramacionPlanTrabajo', 'DESC')
            ->selectRaw('TE.txNombreTercero as Encargado, TC.txNombreTercero as Compania, oidProgramacionPlanTrabajo')
            ->leftjoin('asn_tercero as TE', 'Tercero_oidTerceroEncargado', '=', 'TE.oidTercero')
            ->leftjoin('asn_tercero as TC', 'Tercero_oidTerceroCompania', '=', 'TC.oidTercero');


        if (Session::get('rolUsuario') != 1) {
            $programacionplantrabajo->where("Tercero_oidTerceroEncargado", Session::get('oidTercero'));
        }

        $programacionplantrabajo = $programacionplantrabajo->get();

        return view('plantrabajo::EjecucionPlanTrabajoGrid', compact('programacionplantrabajo', 'permisos'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('plantrabajo::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('plantrabajo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $ejecucion = DB::select(
            "SELECT
                oidEjecucionPlanTrabajoDetalle,
                oidActividadPlanTrabajo,
                oidConfiguracionPlanTrabajo AS ConfiguracionPlanTrabajo_oidConfiguracionPlan,
                txActividadProgramadaConfiguracionPlanTrabajo,
                daFechaProgramadaActividadPlanTrabajo,
                txHorasProgramadasActividadPlanTrabajo,
                txCantidadHorasActividadPlanTrabajo,
                EjecucionPlanTrabajo_oidEjecucionPlanTrabajo,
                ActividadPlanTrabajo_oidActividadPlanTrabajo,
                daFechaEjecucionPlanTrabajoDetalle,
                chCumplidoEjecucionPlanTrabajoDetalle
            FROM
                kunde.gen_actividadplantrabajo
                    LEFT JOIN
                gen_configuracionplantrabajo ON ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo
                    LEFT JOIN
                plt_ejecucionplantrabajodetalle ON oidActividadPlanTrabajo = ActividadPlanTrabajo_oidActividadPlanTrabajo
            WHERE
                ProgramacionPlanTrabajo_oidPlanTrabajo_1aM = $id"
        );

        $ejecucionplantrabajo = ProgramacionPlanTrabajoModel::select('Tercero_oidTerceroEncargado', 'Tercero_oidTerceroCompania', 'Tercero_oidTerceroRealiza', 'oidEjecucionPlanTrabajo')
            ->leftjoin('plt_ejecucionplantrabajo', 'ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo', '=', 'oidProgramacionPlanTrabajo')
            ->where('oidProgramacionPlanTrabajo', $id)
            ->get();

        $asn_tercerolista = TerceroModel::where('asn_tercero.txEstadoTercero', '=', 'Activo')->pluck('txNombreTercero', 'oidTercero');

        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');


        return view('plantrabajo::EjecucionPlanTrabajoForm', compact('ejecucion', 'ejecucionplantrabajo', 'asn_tercerolista', 'id', 'asn_tercerolistaEmpleado'));
    }

    public function UpdateEjecucionPlanTrabajo(Request $request, $id)
    {
        //GUARDADO DE ENCABEZADO EJECUCION PLAN TRABAJO
        $indice = array('oidEjecucionPlanTrabajo' => $request['oidEjecucionPlanTrabajo']);
        $datos = array(
            'ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo' => $id,
            'Tercero_oidTerceroRealiza' => $request['Tercero_oidTerceroRealiza'],
        );
        $guardar = EjecucionPlanTrabajoModel::updateOrCreate($indice, $datos);
        $idEjecucion = $guardar->oidEjecucionPlanTrabajo;

        $total = ($request['oidEjecucionPlanTrabajoDetalle'] !== null) ? count($request['oidEjecucionPlanTrabajoDetalle']) : 0;

        if ($total > 0) {
            for ($i = 0; $i < $total; $i++) {
                //GUARDADO DE DETALLE EJECUCION PLAN TRABAJO
                $indice = array('oidEjecucionPlanTrabajoDetalle' => $request['oidEjecucionPlanTrabajoDetalle'][$i]);
                $datos = array(
                    'EjecucionPlanTrabajo_oidEjecucionPlanTrabajo' => $idEjecucion,
                    'ActividadPlanTrabajo_oidActividadPlanTrabajo' => $request['oidActividadPlanTrabajo'][$i],
                    'daFechaEjecucionPlanTrabajoDetalle' => $request['daFechaEjecucionPlanTrabajoDetalle'][$i],
                    'chCumplidoEjecucionPlanTrabajoDetalle' => $request['chCumplidoEjecucionPlanTrabajoDetalle'][$i],
                );

                if ($request['chCumplidoEjecucionPlanTrabajoDetalle'][$i] == 1) {
                    $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                        ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $request['oidActividadPlanTrabajo'][$i])
                        ->get();

                    if ($actividadPlanTrabajoDetalle->count()) {
                        if ($actividadPlanTrabajoDetalle[0]['oidEjecucionActividadPlanTrabajoDetalle']) {
                            $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                                ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $request['oidActividadPlanTrabajo'][$i])
                                ->where('chEjecutadoEjecucionActividadPlanTrabajoDetalle', 0)
                                ->groupBy('plt_ejecucionactividadplantrabajodetalle.oidEjecucionActividadPlanTrabajoDetalle')
                                ->get();
                        } else {
                            $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                                ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $request['oidActividadPlanTrabajo'][$i])
                                ->get();
                        }
                    }

                    if ($actividadPlanTrabajoDetalle->count()) {
                        return response(['oidEjecucionPlanTrabajo' => $id, 'errors' => ['Todas las subtareas deben estar cumplidas']], 409);
                    }
                }

                EjecucionPlanTrabajoDetalleModel::updateOrCreate($indice, $datos);
            }
        } else {
            $data = DB::select(
                "SELECT
                    oidEjecucionPlanTrabajoDetalle,
                    oidActividadPlanTrabajo,
                    daFechaEjecucionPlanTrabajoDetalle,
                    chCumplidoEjecucionPlanTrabajoDetalle
                FROM
                    kunde.gen_actividadplantrabajo
                        LEFT JOIN
                    gen_configuracionplantrabajo ON ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo
                        LEFT JOIN
                    plt_ejecucionplantrabajodetalle ON oidActividadPlanTrabajo = ActividadPlanTrabajo_oidActividadPlanTrabajo
                WHERE
                    ProgramacionPlanTrabajo_oidPlanTrabajo_1aM = $id"
            );

            foreach ($data as $value) {
                //GUARDADO DE DETALLE EJECUCION PLAN TRABAJO
                $indice = array('oidEjecucionPlanTrabajoDetalle' => $value->oidEjecucionPlanTrabajoDetalle);
                $datos = array(
                    'EjecucionPlanTrabajo_oidEjecucionPlanTrabajo' => $idEjecucion,
                    'ActividadPlanTrabajo_oidActividadPlanTrabajo' => $value->oidActividadPlanTrabajo,
                    'daFechaEjecucionPlanTrabajoDetalle' => $value->daFechaEjecucionPlanTrabajoDetalle,
                    'chCumplidoEjecucionPlanTrabajoDetalle' => $value->chCumplidoEjecucionPlanTrabajoDetalle,
                );

                if ($value->chCumplidoEjecucionPlanTrabajoDetalle) {
                    $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::join('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                        ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $value->oidActividadPlanTrabajo)
                        ->get();

                    if (!$actividadPlanTrabajoDetalle->count()) {
                        return response(['oidEjecucionPlanTrabajo' => $id, 'errors' => ['Todas las subtareas deben estar cumplidas']], 409);
                    }

                    if ($value->chCumplidoEjecucionPlanTrabajoDetalle == 1) {
                        $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                            ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $value->oidActividadPlanTrabajo)
                            ->get();

                        if ($actividadPlanTrabajoDetalle->count()) {
                            if ($actividadPlanTrabajoDetalle[0]['oidEjecucionActividadPlanTrabajoDetalle']) {
                                $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                                    ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $value->oidActividadPlanTrabajo)
                                    ->where('chEjecutadoEjecucionActividadPlanTrabajoDetalle', 0)
                                    ->groupBy('plt_ejecucionactividadplantrabajodetalle.oidEjecucionActividadPlanTrabajoDetalle')
                                    ->get();
                            } else {
                                $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                                    ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $value->oidActividadPlanTrabajo)
                                    ->get();
                            }
                        }

                        if ($actividadPlanTrabajoDetalle->count()) {
                            return response(['oidEjecucionPlanTrabajo' => $id, 'errors' => ['Todas las subtareas deben estar cumplidas']], 409);
                        }
                    }
                }

                EjecucionPlanTrabajoDetalleModel::updateOrCreate($indice, $datos);
            }
        }

        return response(['oidEjecucionPlanTrabajo' => $id], 201);
    }

    public function planTrabajoEjecucionBitacoraModal($id)
    {
        $ejecucionDetalle = EjecucionPlanTrabajoDetalleModel::find($id);

        $ejecucion = EjecucionPlanTrabajoModel::join("gen_programacionplantrabajo", "plt_ejecucionplantrabajo.ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo", '=', "gen_programacionplantrabajo.oidProgramacionPlanTrabajo")
            ->where("oidEjecucionPlanTrabajo", "=", $ejecucionDetalle->EjecucionPlanTrabajo_oidEjecucionPlanTrabajo)
            ->first();

        $bitacora = EjecucionPlanTrabajoBitacoraModel::where('EjecucionPlanTrabajoDetalle_oidEjecucionDetalle', $id)->get();
        $idBitacora = (count($bitacora) > 0 ? $bitacora[0]['oidEjecucionPlanTrabajoBitacora'] : "");
        $pendienteBitacora = BitacoraPendienteModel::where('PlanTrabajoBitacora_oidPlanTrabajoBitacora', $idBitacora)
            ->get();
        $asistenteBitacora = BitacoraAsistenteModel::where('PlanTrabajoBitacora_oidPlanTrabajoBitacora', $idBitacora)
            ->get();

        $terceroResponsableID = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->orWhere(function ($query) use ($ejecucion) {
                $query->where('oidTercero', '=', $ejecucion->Tercero_oidTerceroCompania);
                $query->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli');
            })
            ->pluck('oidTercero');

        $terceroResponsableNombre = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->orWhere(function ($query) use ($ejecucion) {
                $query->where('oidTercero', '=', $ejecucion->Tercero_oidTerceroCompania);
                $query->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli');
            })
            ->pluck('txNombreTercero');

        $idT = (isset($terceroResponsableID) ? json_encode($terceroResponsableID) : "");
        $NombreT = (isset($terceroResponsableNombre) ? json_encode($terceroResponsableNombre) : "");

        $view = view('plantrabajo::EjecucionPlanTrabajoBitacoraForm', compact('id', 'bitacora'))->render();
        return response(['view' => $view, 'data' => $pendienteBitacora, 'terceroId' => $idT, 'terceroNombre' => $NombreT, 'dataAsistente' => $asistenteBitacora], 200);
    }

    public function updateBitacora(Request $request, $id)
    {
        //GUARDADO DE ENCABEZADO DE BITACORA
        $indice = array('oidEjecucionPlanTrabajoBitacora' => $request['oidEjecucionPlanTrabajoBitacora']);
        $datos = array(
            'EjecucionPlanTrabajoDetalle_oidEjecucionDetalle' => $request['EjecucionPlanTrabajoDetalle_oidEjecucionDetalle'],
            'fechaPlanTrabajoBitacora' => $request['fechaPlanTrabajoBitacora'],
            'horaInicioPlanTrabajoBitacora' => $request['horaInicioPlanTrabajoBitacora'],
            'horaSalidaPlanTrabajoBitacora' => $request['horaSalidaPlanTrabajoBitacora'],
            'observacionPlanTrabajoBitacora' => $request['observacionPlanTrabajoBitacora'],
        );
        $guardar = EjecucionPlanTrabajoBitacoraModel::updateOrCreate($indice, $datos);
        $idBitacora = $guardar->oidEjecucionPlanTrabajoBitacora;

        $cliente = EjecucionPlanTrabajoDetalleModel::join('plt_ejecucionplantrabajo', 'plt_ejecucionplantrabajodetalle.EjecucionPlanTrabajo_oidEjecucionPlanTrabajo', 'plt_ejecucionplantrabajo.oidEjecucionPlanTrabajo')
            ->join('gen_programacionplantrabajo', 'plt_ejecucionplantrabajo.ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo', 'gen_programacionplantrabajo.oidProgramacionPlanTrabajo')
            ->join('asn_tercero', 'gen_programacionplantrabajo.Tercero_oidTerceroCompania', 'asn_tercero.oidTercero')
            ->where('plt_ejecucionplantrabajodetalle.oidEjecucionPlanTrabajoDetalle', $request['EjecucionPlanTrabajoDetalle_oidEjecucionDetalle'])
            ->first();

        //GUARDADO DE DETALLLE DE BITACORA PENDIENTES
        $idsEliminar = explode(',', $request['eliminarPendientes']);
        BitacoraPendienteModel::whereIn('oidBitacoraPendiente', $idsEliminar)->delete();
        $total = ($request['oidBitacoraPendiente'] !== null) ? count($request['oidBitacoraPendiente']) : 0;
        for ($i = 0; $i < $total; $i++) {
            $indice = array('oidBitacoraPendiente' => $request['oidBitacoraPendiente'][$i]);
            $datos = array(
                'PlanTrabajoBitacora_oidPlanTrabajoBitacora' => $idBitacora,
                'txBitacoraActividad' => $request['txBitacoraActividad'][$i],
                'Tercero_oidBitacoraResponsable' => $request['Tercero_oidBitacoraResponsable'][$i],
                'daBitacoraFehcaEntrega' => $request['daBitacoraFehcaEntrega'][$i],
            );
            $guardar = BitacoraPendienteModel::updateOrCreate($indice, $datos);

            $dataTarea = TareaModel::where('txAsuntoTarea', $request['txBitacoraActividad'][$i])
                ->where('txResponsableTarea', $request['Tercero_oidBitacoraResponsable'][$i])
                ->where('txEstadoTarea', 'Nuevo')
                ->first();

            if (!$dataTarea) {
                $tarea = new TareaModel();
                $tarea->dtFechaElaboracionTarea = date('Y-m-d');
                $tarea->txAsuntoTarea = $request['txBitacoraActividad'][$i] . ' - ' . $cliente->txNombreTercero;
                $tarea->txPrioridadTarea = 'Alta';
                $tarea->txAreaTarea = 8;
                $tarea->txResponsableTarea = $request['Tercero_oidBitacoraResponsable'][$i];
                $tarea->txEstadoTarea = 'Nuevo';
                $tarea->dtFechaVencimientoTarea = $request['daBitacoraFehcaEntrega'][$i];
                $tarea->save();
            } else {
                $tarea = TareaModel::find($dataTarea->oidTarea);
                $tarea->dtFechaElaboracionTarea = date('Y-m-d');
                $tarea->txAsuntoTarea = $request['txBitacoraActividad'][$i] . ' - ' . $cliente->txNombreTercero;
                $tarea->txPrioridadTarea = 'Alta';
                $tarea->txAreaTarea = 8;
                $tarea->txResponsableTarea = $request['Tercero_oidBitacoraResponsable'][$i];
                $tarea->txEstadoTarea = 'Nuevo';
                $tarea->dtFechaVencimientoTarea = $request['daBitacoraFehcaEntrega'][$i];
                $tarea->save();
            }
        }

        //GUARDADO DE DETALLLE DE BITACORA ASISTENTES
        $idsEliminarA = explode(',', $request['eliminarAsistente']);
        BitacoraAsistenteModel::whereIn('oidBitacoraAsistente', $idsEliminarA)->delete();
        $totalAsistente = ($request['oidBitacoraAsistente'] !== null) ? count($request['oidBitacoraAsistente']) : 0;
        for ($i = 0; $i < $totalAsistente; $i++) {
            $indice = array('oidBitacoraAsistente' => $request['oidBitacoraAsistente'][$i]);
            $datos = array(
                'PlanTrabajoBitacora_oidPlanTrabajoBitacora' => $idBitacora,
                'txNombreAsistente' => $request['txNombreAsistente'][$i],
            );
            $guardar = BitacoraAsistenteModel::updateOrCreate($indice, $datos);
        }
        $idDetalleEjecucion = $request['EjecucionPlanTrabajoDetalle_oidEjecucionDetalle'];
        //CONSULTA PARA EL ENVIO DE CORREOS A LOS IMPLICADOS EN LA EJECUCION.
        // $consulta = DB::select("SELECT
        //     TR.txCorreoElectronicoTercero as CorreoRealiza,
        //     TR.txNombreTercero as nombreRealiza,
        //     TC.txCorreoElectronicoTercero as correoCompania,
        //     TC.txNombreTercero as nombreCompania,
        //     txActividadProgramadaConfiguracionPlanTrabajo as nombreActividad,
        //     oidEjecucionPlanTrabajo,
        //     oidProgramacionPlanTrabajo

        // FROM
        //     kunde.plt_ejecucionplantrabajodetalle
        //         JOIN
        //     plt_ejecucionplantrabajo ON EjecucionPlanTrabajo_oidEjecucionPlanTrabajo = oidEjecucionPlanTrabajo
        //         JOIN
        //     gen_programacionplantrabajo ON ProgramacionPlanTrabajo_oidEjecucionPlanTrabajo = oidProgramacionPlanTrabajo
        //         JOIN
        //     asn_tercero TR on Tercero_oidTerceroRealiza = TR.oidTercero
        //         JOIN
        //     asn_tercero TC on Tercero_oidTerceroCompania = TC.oidTercero
        //         JOIN
        //     gen_actividadplantrabajo on ActividadPlanTrabajo_oidActividadPlanTrabajo = oidActividadPlanTrabajo
        //         JOIN
        //     gen_configuracionplantrabajo on ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo

        // WHERE
        //     oidEjecucionPlanTrabajoDetalle = $idDetalleEjecucion");

        // if(!empty($consulta)){
        //     $cita = new BitacoraFinalizada($consulta[0]->CorreoRealiza,$consulta[0]->nombreRealiza,$consulta[0]->correoCompania,$consulta[0]->nombreCompania,$consulta[0]->nombreActividad);
        //     Mail::to($consulta[0]->CorreoRealiza)->send($cita);
        //     Mail::to($consulta[0]->correoCompania)->send($cita);
        // }


        return response(200);
    }

    public function getEjecucionActividadPlantrabajoDetalle($idActividad, $idDetalleEjecucion)
    {
        $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
            ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $idActividad)
            ->get();

        if ($actividadPlanTrabajoDetalle->count()) {
            if ($actividadPlanTrabajoDetalle[0]['oidEjecucionActividadPlanTrabajoDetalle']) {
                $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                    ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $idActividad)
                    ->groupBy('plt_ejecucionactividadplantrabajodetalle.oidEjecucionActividadPlanTrabajoDetalle')
                    ->get();
            } else {
                $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::leftJoin('plt_ejecucionactividadplantrabajodetalle', 'gen_actividadplantrabajodetalle.oidActividadPlanTrabajoDetalle', 'plt_ejecucionactividadplantrabajodetalle.ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle')
                    ->where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $idActividad)
                    ->get();
            }
        }

        $view = view('plantrabajo::actividadPlanTrabajoDetalleForm', compact('idActividad', 'idDetalleEjecucion', 'actividadPlanTrabajoDetalle'))->render();
        return response(['view' => $view, 'data' => $actividadPlanTrabajoDetalle], 200);
    }

    public function updateEjecucionActividad(Request $request, $id)
    {
        $total = ($request['oidEjecucionActividadPlanTrabajoDetalle'] !== null) ? count($request['oidEjecucionActividadPlanTrabajoDetalle']) : 0;
        for ($i = 0; $i < $total; $i++) {
            $indice = array('oidEjecucionActividadPlanTrabajoDetalle' => $request['oidEjecucionActividadPlanTrabajoDetalle'][$i]);
            $datos = array(
                'EjecucionPlanTrabajoDetalle_oidEjecucionPlanTrabajoDetalle' => $id,
                'ActividadPlanTrabajoDetalle_oidActividadPlanTrabajoDetalle' => $request['oidActividadPlanTrabajoDetalle'][$i],
                'chEjecutadoEjecucionActividadPlanTrabajoDetalle' => $request['chEjecutadoEjecucionActividadPlanTrabajoDetalle'][$i],
            );
            EjecucionActividadPlanTrabajoDetalleModel::updateOrCreate($indice, $datos);
        }

        return response(200);
    }

    public function informePlanTrabajo()
    {
        $consulta = DB::select("SELECT
            txNombreTercero,
            txActividadProgramadaConfiguracionPlanTrabajo,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-01'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS ENERO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-02'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS FEBRERO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-03'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS MARZO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-04'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS ABRIL,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-05'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS MAYO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-06'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS JUNIO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-07'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS JULIO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-08'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS AGOSTO,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-09'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS SEPTIEMBRE,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-10'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS OCTUBRE,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-11'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS NOVIEMBRE,
            IF(DATE_FORMAT(daFechaProgramadaActividadPlanTrabajo, '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-12'), IFNULL(chCumplidoEjecucionPlanTrabajoDetalle, 0), '') AS DICIEMBRE
        FROM
            kunde.gen_programacionplantrabajo
                INNER JOIN
            gen_actividadplantrabajo ON ProgramacionPlanTrabajo_oidPlanTrabajo_1aM = oidProgramacionPlanTrabajo
                INNER JOIN
            gen_configuracionplantrabajo ON ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo
                INNER JOIN
            asn_tercero ON Tercero_oidTerceroCompania = oidTercero
                LEFT JOIN
            plt_ejecucionplantrabajodetalle ON ActividadPlanTrabajo_oidActividadPlanTrabajo = oidActividadPlanTrabajo
        GROUP BY oidTercero , oidConfiguracionPlanTrabajo
        ORDER BY txNombreTercero");

        return view('plantrabajo::InformePlanTrabajo', compact('consulta'));
    }

    public function generarBitacora($id)
    {
        /*$contrato = ContratoLaboralModel::from('geh_contratolaboral as cl')
        ->join('geh_contratolaboraldetalle as cld', 'cl.oidContratoLaboral', 'cld.ContratoLaboral_oidContratoLaboral')
        ->join('asn_tercero as t', 'cl.Tercero_oidEmpleado', 't.oidTercero')
        ->join('gen_cargo as c', 'cld.Cargo_oidCargo', 'c.oidCargo')
        ->where('cl.Tercero_oidEmpleado', $idTercero)
        ->where('cld.lsEstadoContratoLaboralDetalle', 'Activo')
        ->first();

        if(!$contrato){
            echo 'No está configurado el contrato laboral para este empleado.';
            return;
        }
    */

        $bitacora = EjecucionPlanTrabajoBitacoraModel::find($id);

        $actividad = EjecucionPlanTrabajoBitacoraModel::from("plt_ejecucionplantrabajobitacora as eptb")
            ->join("plt_ejecucionplantrabajodetalle as eptd", "eptb.EjecucionPlanTrabajoDetalle_oidEjecucionDetalle", "eptd.oidEjecucionPlanTrabajoDetalle")
            ->join("gen_actividadplantrabajo as apt", "eptd.ActividadPlanTrabajo_oidActividadPlanTrabajo", "apt.oidActividadPlanTrabajo")
            ->join("gen_configuracionplantrabajo as cpt", "apt.ConfiguracionPlanTrabajo_oidConfiguracionPlan", "cpt.oidConfiguracionPlanTrabajo")
            ->join("gen_programacionplantrabajo as ppt", "apt.ProgramacionPlanTrabajo_oidPlanTrabajo_1aM", "ppt.oidProgramacionPlanTrabajo")
            ->join("asn_tercero as t", "ppt.Tercero_oidTerceroCompania", "t.oidTercero")
            ->selectRaw("txActividadProgramadaConfiguracionPlanTrabajo as nombreActividad,
        fechaPlanTrabajoBitacora,
        horaInicioPlanTrabajoBitacora,
        horaSalidaPlanTrabajoBitacora,
        observacionPlanTrabajoBitacora,
        txNombreTercero
        ")
            ->where("oidEjecucionPlanTrabajoBitacora", $id)
            ->first();

        $participantes = BitacoraAsistenteModel::where("PlanTrabajoBitacora_oidPlanTrabajoBitacora", $id)->get();

        $tareas = BitacoraPendienteModel::from('plt_bitacorapendiente as bp')
            ->join("asn_tercero as t", "bp.Tercero_oidBitacoraResponsable", "t.oidTercero")
            ->selectRaw("txNombreTercero, txBitacoraActividad, daBitacoraFehcaEntrega")
            ->where("PlanTrabajoBitacora_oidPlanTrabajoBitacora", $id)
            ->get();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('plantrabajo::impresionBitacora', ['actividad' => $actividad, "participantes" => $participantes, "tareas" => $tareas]);
        return $pdf->stream();
    }
}
