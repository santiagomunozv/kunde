<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\RegistroSeguimiento\Entities\TareaModel;

class BitacoraManualController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    private static $FINAL_PATH = "plantrabajo/";
    public function index()
    {
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $bitacoraManualAsistente = [];
        $bitacoraManualPendiente = [];
        $idResponsable = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where(function ($query) {
                $query->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
                    ->orWhere('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli');
            })
            ->pluck('oidTercero');
        $nombreResponsable = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where(function ($query) {
                $query->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
                    ->orWhere('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli');
            })
            ->pluck('txNombreTercero');
        return view('plantrabajo::bitacoraManualForm', compact('cliente', 'bitacoraManualPendiente', 'bitacoraManualAsistente', 'idResponsable', 'nombreResponsable'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $cliente = TerceroModel::find($request['Tercero_oidCliente']);
            $modalidad = $request->input('modalidadBitacora', 'Virtual');

            $actividad = [
                "nombreActividad" => $request['txNombreActividadBitacoraManual'],
                "fechaPlanTrabajoBitacora" => $request['daFechaBitacoraManual'],
                "horaInicioPlanTrabajoBitacora" => $request['hrHoraInicioBitacoraManual'],
                "horaSalidaPlanTrabajoBitacora" => $request['hrHoraFinBitacoraManual'],
                "observacionPlanTrabajoBitacora" => $request['txObservacionBitacoraManual'],
                "modalidad" => $modalidad,
                "txNombreTercero" => $cliente->txNombreTercero,
            ];

            $participantes = [];
            $total = ($request['oidBitacoraManualAsistente'] !== null) ? count($request['oidBitacoraManualAsistente']) : 0;
            for ($i = 0; $i < $total; $i++) {
                $participantes[$i]['txNombreAsistente'] = $request['txNombreAsistenteBitacoraManual'][$i];
            }

            $tareas = [];
            $total = ($request['oidBitacoraManualPendiente'] !== null) ? count($request['oidBitacoraManualPendiente']) : 0;
            for ($i = 0; $i < $total; $i++) {
                $responsable = TerceroModel::find($request['Responsable_oidResponsable'][$i]);
                $tareas[$i]['txNombreTercero'] = $responsable->txNombreTercero;
                $tareas[$i]['txBitacoraActividad'] = $request['txNombreTareaBitacoraManual'][$i];
                $tareas[$i]['daBitacoraFehcaEntrega'] = $request['dtFechaVencimientoBitacoraManual'][$i];

                $tarea = new TareaModel();
                $tarea->dtFechaElaboracionTarea = date('Y-m-d');
                $tarea->txAsuntoTarea = $request['txNombreTareaBitacoraManual'][$i] . ' - ' . $cliente->txNombreTercero;
                $tarea->txPrioridadTarea = 'Alta';
                $tarea->txAreaTarea = 8;
                $tarea->txResponsableTarea = $request['Responsable_oidResponsable'][$i];
                $tarea->txEstadoTarea = 'Nuevo';
                $tarea->dtFechaVencimientoTarea = $request['dtFechaVencimientoBitacoraManual'][$i];
                $tarea->save();
            }

            $firmaPath = null;
            if ($request->has('firma_base64') && $modalidad === 'Presencial') {
                $firmaData = $request->input('firma_base64');
                $firmaData = str_replace('data:image/png;base64,', '', $firmaData);
                $firmaData = str_replace(' ', '+', $firmaData);
                $firma = base64_decode($firmaData);

                $carpetaFirmas = 'firmas';
                Storage::disk('discoc')->makeDirectory($carpetaFirmas);

                $firmaFileName = 'firma_' . time() . '.png';
                $firmaPath = $carpetaFirmas . '/' . $firmaFileName;

                Storage::disk('discoc')->put($firmaPath, $firma);
            }

            $actividad["rutaFirma"] = Storage::disk('discoc')->path($firmaPath);

            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('plantrabajo::impresionBitacora', ['actividad' => (object)$actividad, "participantes" => json_decode(json_encode($participantes), FALSE), "tareas" => json_decode(json_encode($tareas), FALSE)]);

            $fileName = time() . '.' . 'pdf';
            $pdf->save(public_path() . '/' . $fileName);
            $pdf = public_path($fileName);

            $nombre = self::generateFileName($fileName);
            $destino = Storage::disk('discoc')->path($nombre);
            File::move($pdf, $destino);
            DB::commit();
            return response(['archivo' => $fileName], 201);
        } catch (Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public static function generateFileName($fileName)
    {
        return self::$FINAL_PATH . "{$fileName}";
    }
}
