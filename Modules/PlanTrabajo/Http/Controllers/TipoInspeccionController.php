<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\PlanTrabajo\Entities\TipoInspeccionDetalleModel;
use Modules\PlanTrabajo\Entities\TipoInspeccionModel;
use Modules\PlanTrabajo\Http\Requests\TipoInspeccionRequest;

class TipoInspeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $tipoInspeccion = TipoInspeccionModel::selectRaw('oidTipoInspeccion, txNombreTipoInspeccion, txDescripcionTipoInspeccion')
            ->get();

        return view('plantrabajo::tipoInspeccionGrid', compact('permisos', 'tipoInspeccion'), ['tipoInspeccion' => $tipoInspeccion]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tipoInspeccion = new TipoInspeccionModel();
        $tipoInspeccionDetalle = new TipoInspeccionDetalleModel();

        return view('plantrabajo::tipoInspeccionForm', compact("tipoInspeccion", "tipoInspeccionDetalle"), ['tipoInspeccion' => $tipoInspeccion]);
    }

    /**
     * Store a newly created resource in storage.
     * @param TipoInspeccionRequest $request
     * @return Response
     */
    public function store(TipoInspeccionRequest $request)
    {
        DB::beginTransaction();
        try {
            $tipoInspeccion = new TipoInspeccionModel();
            $tipoInspeccion->fill($request->all());
            $tipoInspeccion->save();
            $this->grabarDetalle($request, $tipoInspeccion->oidTipoInspeccion);
            DB::commit();
            return response(['oidTipoInspeccion' => $tipoInspeccion->oidTipoInspeccion], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('plantrabajo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $tipoInspeccion = TipoInspeccionModel::find($id);
        $tipoInspeccionDetalle = TipoInspeccionDetalleModel::where("TipoInspeccion_oidTipoInspeccion", '=', $id)->get();

        return view('plantrabajo::tipoInspeccionForm', compact("tipoInspeccion", "tipoInspeccionDetalle"), ['tipoInspeccion' => $tipoInspeccion]);
    }

    /**
     * Update the specified resource in storage.
     * @param TipoInspeccionRequest $request
     * @param int $id
     * @return Response
     */
    public function update(TipoInspeccionRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $tipoInspeccion = TipoInspeccionModel::find($id);
            $tipoInspeccion->fill($request->all());
            $tipoInspeccion->save();
            $this->grabarDetalle($request, $tipoInspeccion->oidTipoInspeccion);
            DB::commit();
            return response(['oidTipoInspeccion' => $tipoInspeccion->oidTipoInspeccion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    private function grabarDetalle($request, $id)
    {
        $modelo = new TipoInspeccionDetalleModel();
        $idsEliminar = explode(',', $request['eliminarTipoInspeccion']);
        $modelo::whereIn('oidTipoInspeccionDetalle', $idsEliminar)->delete();
        $total = ($request['oidTipoInspeccionDetalle'] !== null) ? count($request['oidTipoInspeccionDetalle']) : 0;
        for ($i = 0; $i <  $total; $i++) {
            $indice = array('oidTipoInspeccionDetalle' => $request['oidTipoInspeccionDetalle'][$i]);
            $datos = [
                'TipoInspeccion_oidTipoInspeccion' => $id,
                'txNombreTipoInspeccionDetalle' => $request["txNombreTipoInspeccionDetalle"][$i],
            ];
            $modelo::updateOrCreate($indice, $datos);
        }
    }
}
