<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroCargoModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\CargoModel;
use Modules\PlanTrabajo\Entities\MatrizRiesgoEncModel;
use Modules\PlanTrabajo\Entities\MatrizRiesgoModel;
use Modules\PlanTrabajo\Http\Requests\MatrizRiesgoEncRequest;

class MatrizRiesgoEncController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $matrizRiesgoEnc = MatrizRiesgoEncModel::join('asn_tercero as TC','Tercero_oidCliente','=','TC.oidTercero')
        ->leftjoin('asn_tercero as TE','Tercero_oidEmpleado','=','TE.oidTercero')
        ->selectRaw('oidMatrizRiesgoEnc, TE.txNombreTercero as empleado, TC.txNombreTercero as cliente, daFechaMatrizRiesgoEnc')
        ->get();

        return view('plantrabajo::matrizRiesgoEncGrid', compact('permisos' , 'matrizRiesgoEnc'), ['matrizRiesgoEnc' => $matrizRiesgoEnc] );
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $matrizRiesgoEnc = new MatrizRiesgoEncModel();
        $matrizRiesgo = new MatrizRiesgoModel();
        $nombreCargo = CargoModel::pluck('txNombreCargo');
        $idCargo = CargoModel::pluck('oidCargo');
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
                            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
                            ->where('asn_tercero.txEstadoTercero','=', 'Activo')
                            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Cli')
                            ->pluck('txNombreTercero','oidTercero');
                            
        return view('plantrabajo::matrizRiesgoEncForm', compact("matrizRiesgoEnc", "matrizRiesgo", "nombreCargo", "idCargo", "cliente"), ['matri$matrizRiesgoEnc' => $matrizRiesgoEnc] );
    }

    /**
     * Store a newly created resource in storage.
     * @param MatrizRiesgoEncRequest $request
     * @return Response
     */
    public function store(MatrizRiesgoEncRequest $request)
    {
        DB::beginTransaction();
        try{
            $matrizRiesgoEnc = new MatrizRiesgoEncModel();
            $matrizRiesgoEnc->fill($request->all());
            $matrizRiesgoEnc->Tercero_oidEmpleado = Session::get('oidTercero');
            $matrizRiesgoEnc->save();
            $this->grabarDetalle($request, $matrizRiesgoEnc->oidMatrizRiesgoEnc);
            DB::commit();
            return response(['oidMatrizRiesgoEnc' => $matrizRiesgoEnc->oidMatrizRiesgoEnc] , 201);
        }
        catch(\Exception $e){
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('plantrabajo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $matrizRiesgoEnc = MatrizRiesgoEncModel::find($id);
        $matrizRiesgo = MatrizRiesgoModel::where("MatrizRiesgoEnc_oidMatrizRiesgoEnc", '=', $id)->get();
        $nombreCargo = CargoModel::pluck('txNombreCargo');
        $idCargo = CargoModel::pluck('oidCargo');
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
                            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
                            ->where('asn_tercero.txEstadoTercero','=', 'Activo')
                            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Cli')
                            ->pluck('txNombreTercero','oidTercero');
        
        return view('plantrabajo::matrizRiesgoEncForm',compact("matrizRiesgoEnc", "matrizRiesgo", "nombreCargo", "idCargo", "cliente"), ['matrizRiesgoEnc' => $matrizRiesgoEnc] );
    }

    /**
     * Update the specified resource in storage.
     * @param MatrizRiesgoEncRequest $request
     * @param int $id
     * @return Response
     */
    public function update(MatrizRiesgoEncRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $matrizRiesgoEnc = MatrizRiesgoEncModel::find($id);
            $matrizRiesgoEnc->fill($request->all());
            $matrizRiesgoEnc->save();
            $this->grabarDetalle($request, $matrizRiesgoEnc->oidMatrizRiesgoEnc);
            DB::commit();
            return response(['oidMatrizRiesgoEnc' => $matrizRiesgoEnc->oidMatrizRiesgoEnc] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }   

    private function grabarDetalle($request, $id){
        $modelo = new MatrizRiesgoModel();
        $idsEliminar = explode(',', $request['eliminarMatriz']);
        $modelo::whereIn('oidMatrizRiesgo',$idsEliminar)->delete();
        $total = ($request['oidMatrizRiesgo'] !== null ) ? count($request['oidMatrizRiesgo']) : 0;
        for($i = 0; $i <  $total; $i++)
        {
            $indice = array('oidMatrizRiesgo' => $request['oidMatrizRiesgo'][$i]);
            $datos = [
                    'MatrizRiesgoEnc_oidMatrizRiesgoEnc' => $id,
                    'Cargo_oidCargo'=>$request["Cargo_oidCargo"][$i],
                    'txActividadCargoMatrizRiesgo'=>$request["txActividadCargoMatrizRiesgo"][$i],
                    'lsRutinariaMatrizRiesgo'=>$request["lsRutinariaMatrizRiesgo"][$i],
                    'lsClasificacionMatrizRiesgo'=>$request["lsClasificacionMatrizRiesgo"][$i],
                    'txDescripcionClasificacionMatrizRiesgo'=>$request["txDescripcionClasificacionMatrizRiesgo"][$i],
                    'txEfectosPosiblesMatrizRiesgo'=>$request["txEfectosPosiblesMatrizRiesgo"][$i],
                    'txFuenteMatrizRiesgo'=>$request["txFuenteMatrizRiesgo"][$i],
                    'txMedioMatrizRiesgo'=>$request["txMedioMatrizRiesgo"][$i],
                    'txIndividuoMatrizRiesgo'=>$request["txIndividuoMatrizRiesgo"][$i],
                    'lsNivelDeficienciaMatrizRiesgo'=>$request["lsNivelDeficienciaMatrizRiesgo"][$i],
                    'lsNivelExposicionMatrizRiesgo'=>$request["lsNivelExposicionMatrizRiesgo"][$i],
                    'txNivelProbabilidadMatrzRiesgo'=>$request["txNivelProbabilidadMatrzRiesgo"][$i],
                    'txInterpretacionProbabilidadMatrizRiesgo'=>$request["txInterpretacionProbabilidadMatrizRiesgo"][$i],
                    'lsNivelConsecuenciaMatrizRiesgo'=>$request["lsNivelConsecuenciaMatrizRiesgo"][$i],
                    'txNivelRiesgoMatrizRiesgo'=>$request["txNivelRiesgoMatrizRiesgo"][$i],
                    'txInterpretacionRiesgoMatrizRiesgo'=>$request["txInterpretacionRiesgoMatrizRiesgo"][$i],
                    'txAceptabilidadRiesgoMatrizRiesgo'=>$request["txAceptabilidadRiesgoMatrizRiesgo"][$i],
                    'inExpuestosMatrizRiesgo'=>$request["inExpuestosMatrizRiesgo"][$i],
                    'txPeorConsecuenciaMatrizRiesgo'=>$request["txPeorConsecuenciaMatrizRiesgo"][$i],
                    'txEliminacionMatrizRiesgo'=>$request["txEliminacionMatrizRiesgo"][$i],
                    'txSustitucionMatrizRiesgo'=>$request["txSustitucionMatrizRiesgo"][$i],
                    'txControlIngenieria'=>$request["txControlIngenieria"][$i],
                    'txControlAdministrativoMatrizRiesgo'=>$request["txControlAdministrativoMatrizRiesgo"][$i],
                    'txElementosProteccionPersonalMatrizRiesgo'=>$request["txElementosProteccionPersonalMatrizRiesgo"][$i],
                ];
            $modelo::updateOrCreate($indice, $datos);
        }
    }

    public function consultarMatrizByCliente($clienteId){
        $matriz = TerceroCargoModel::join('gen_matrizriesgo', 'asn_tercerocargo.Cargo_oidTerceroCargo', 'gen_matrizriesgo.Cargo_oidCargo')
        ->where('asn_tercerocargo.Tercero_oidTercero_1aM', '=', $clienteId)
        ->select('Cargo_oidCargo', 'txActividadCargoMatrizRiesgo','lsRutinariaMatrizRiesgo', 'lsClasificacionMatrizRiesgo', 'txDescripcionClasificacionMatrizRiesgo', 'txEfectosPosiblesMatrizRiesgo', 'txFuenteMatrizRiesgo', 'txMedioMatrizRiesgo', 'txIndividuoMatrizRiesgo', 'lsNivelDeficienciaMatrizRiesgo', 'lsNivelExposicionMatrizRiesgo', 'txNivelProbabilidadMatrzRiesgo', 'txInterpretacionProbabilidadMatrizRiesgo', 'lsNivelConsecuenciaMatrizRiesgo', 'txNivelRiesgoMatrizRiesgo', 'txInterpretacionRiesgoMatrizRiesgo', 'txAceptabilidadRiesgoMatrizRiesgo', 'inExpuestosMatrizRiesgo', 'txPeorConsecuenciaMatrizRiesgo', 'txEliminacionMatrizRiesgo', 'txSustitucionMatrizRiesgo', 'txControlIngenieria', 'txControlAdministrativoMatrizRiesgo', 'txElementosProteccionPersonalMatrizRiesgo')
        ->get();

        return response($matriz, 200);
    }
}
