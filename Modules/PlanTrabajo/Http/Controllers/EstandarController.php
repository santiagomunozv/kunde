<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use ZipArchive;
use Illuminate\Support\Facades\Storage;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;
use Modules\AsociadoNegocio\Entities\TerceroIntegranteComiteModel;
use Modules\AsociadoNegocio\Entities\TerceroLaboralModel;
use Modules\AsociadoNegocio\Entities\TerceroSucursalModel;
use PhpOffice\PhpWord\TemplateProcessor;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Zipper;
use PhpOffice\PhpSpreadSheet\SpreadSheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

setlocale(LC_TIME, 'es_ES.utf8');

class EstandarController extends Controller
{
    private $array = [];
    public function index()
    {
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $detalle = [
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.1 Responsable del Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.2 Responsabilidades en el Sistema de Gestión de Seguridad y Salud en el Trabajo – SG-SST',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.3 Asignación de recursos para el Sistema de Gestión en Seguridad y Salud en el Trabajo – SG-SST',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.4 -1.1.5 Afiliación al Sistema General de Riesgos Laborales,Pago de pensión trabajadores alto riesgo',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.6 - 1.1.7 Conformación y Capacitación COPASST - Vigía',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.1.8 Conformación Comité de Convivencia',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.2.1 - 1.2.2  Programa de  Capacitación, Inducción y Reinducción en Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST, actividades de Promoción y Prevención PyP',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '1.2.3 Responsables del Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST con curso (50 horas)',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.1.1 - 2.2.1 Política del Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST firmada, fechada y comunicada al COPASST-Vigía',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.3.1 Evaluación e identificación de prioridades',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.4.1 Plan que identifica objetivos, metas, responsabilidad, recursos con cronograma y firmado',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.5.1 Archivo o retención documental del Sistema de Gestión en Seguridad y Salud en el Trabajo SG-SST',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.6.1 Rendición sobre el desempeño',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.7.1 Matriz legal',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.8.1 Mecanismos de comunicación, auto reporte en Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.9.1 - 2.10.1 Identificación, evaluación, para adquisición de productos y servicios, selecion de provedores y contratistas',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '2.11.1 Gestión del cambio',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '3.1.1 - 3.1.2 - 3.1.3 - 3.1.4 - 3.1.5 - 3.1.6 Gestion de las evaluaciones y recomendaciones medicolaborles y descripcione sociodemografica',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '3.1.7 Estilos de vida y entornos saludables (controles tabaquismo, alcoholismo, farmacodependencia y otros)',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '3.1.8  - 3.1.9 Agua potable, servicios sanitarios y disposición de basuras. Eliminación adecuada de residuos sólidos, líquidos o gaseosos',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '3.2.1 - 3.2.2 - 3.2.3  Reporte, investigación, registro y análisis de AT, IT, EL',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '3.3.1 - 3.3.2 - 3.3.3 - 3.3.4 -3.3.5 - 3.3.6 Gestion de indicadores de AT y EL',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.1.1 - 4.1.2 - 4.1.3 - 4.1.4 Metodología para la identificación, evaluación y valoración de peligros. Identificación de peligros con participación de todos los niveles de la emp',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.2.1 - 4.2.2 Se implementan las medidas de prevención y control de peligros',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.2.3 Hay procedimientos, instructivos, fichas, protocolos',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.2.4 Inspección con el COPASST o Vigía',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.2.5 Mantenimiento periódico de instalaciones, equipos, máquinas, herramientas',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '4.2.6 Entrega de Elementos de Protección Persona EPP, se verifica con contratistas y subcontratistas',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '5.1.1 - 5.1.2 Plan de Prevención y Preparación ante emergencias y brigada conformada, capacitada y dotada',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '6.1.1 Indicadores estructura, proceso y resultado',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '6.1.2 - 6.1.3 - 6.1.4 Las empresa adelanta auditoría por lo menos una vez al año',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => '7.1.1 -7.1.2 - 7.1.3 - 7.1.4 Definir acciones de Promoción y Prevención con base en resultados del Sistema de Gestión de Seguridad y Salud en el Trabajo SG-SST_',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => 'Bitacoras',
            ],
            [
                "chGeneraEstandar" => false,
                "txNombreEstandar" => 'Kundeteca',
            ],
        ];

        $detalle = json_encode($detalle);

        return view('plantrabajo::estandarForm', compact('cliente', 'detalle'));
    }

    public function store(Request $request)
    {
        ini_set('memory_limit', '-1');

        $clientesId = explode(',', $request->get('lmClienteEstandar'));
        $anio = $request->get('lsAnioEstandar');
        $fecha = $request->get('dtFechaGeneracionEstandar');
        $fechaConvocatoria = $request->get('dtFechaConvocatoriaEstandar');
        $chGeneraEstandarArray = $request->get('chGeneraEstandar');
        $this->eliminarArchivosPrevios();

        foreach ($clientesId as $clienteId) {
            $cliente = TerceroModel::find($clienteId);

            $folderName = "estandares/$anio";
            $estandares = public_path($folderName);


            //if (count(glob($estandares  . "/*")) > 1){
            foreach (glob($estandares . "/*") as $index => $estandar) {
                // if (!isset($chGeneraEstandarArray[$index]) || $chGeneraEstandarArray[$index] == 'false' || !$chGeneraEstandarArray[$index]) {
                //     continue;
                // }

                // if ($chGeneraEstandarArray[$index] == true) {
                //     dd($estandar);
                // }

                for ($i = 0; $i < count($request->get('txNombreEstandar')); $i++) {
                    if (
                        $request['chGeneraEstandar'][$i] == true &&
                        normalizer_normalize($request['txNombreEstandar'][$i], \Normalizer::FORM_C) == normalizer_normalize(basename($estandar), \Normalizer::FORM_C)
                    ) {
                        if (count(glob($estandar . "/*")) > 0) {
                            foreach (glob($estandar . "/*") as $concepto) {
                                if (is_dir($concepto)) {
                                    if (count(glob($concepto . "/*")) > 0) {
                                        foreach (glob($concepto . "/*") as $archivo) {
                                            $path = public_path("estandares_generados/$anio/$cliente->txNombreTercero/" . basename($estandar) . "/" . basename($concepto));
                                            if (!file_exists($path)) {
                                                mkdir($path, 0777, true);
                                            }
                                            self::resolveDocumento($path, $archivo, $cliente, $fecha, $fechaConvocatoria);
                                        }
                                    } else {
                                        $path = public_path("estandares_generados/$anio/$cliente->txNombreTercero/" . basename($estandar) . "/" . basename($concepto));
                                        if (!file_exists($path)) {
                                            mkdir($path, 0777, true);
                                        }
                                    }
                                } else {
                                    $path = public_path("estandares_generados/$anio/$cliente->txNombreTercero/" . basename($estandar));
                                    if (!file_exists($path)) {
                                        mkdir($path, 0777, true);
                                    }
                                    self::resolveDocumento($path, $concepto, $cliente, $fecha, $fechaConvocatoria);
                                }
                            }
                        } else {
                            $path = public_path("estandares_generados/$anio/$cliente->txNombreTercero/" . basename($estandar));
                            if (!file_exists($path)) {
                                mkdir($path, 0777, true);
                            }
                        }
                    }
                }
            }
            //}
            /*else {
                $path = public_path("estandares_generados/$anio/$cliente->txNombreTercero/" . basename($estandar));
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
            }  */
        }

        self::generarZip();

        return response(['message' => 'estandares_generados.zip'], 200);
    }

    private function eliminarArchivosPrevios()
    {
        if (File::exists(public_path("estandares_generados"))) {
            File::deleteDirectory(public_path("estandares_generados"));
        }
        if (file_exists(public_path("estandares_generados.zip"))) {
            unlink(public_path("estandares_generados.zip"));
        }
    }

    public function resolveDocumento($path, $archivo, $cliente, $fecha, $fechaConvocatoria)
    {
        switch (pathinfo($archivo)['extension']) {
            case 'docx':
                $this->guardarDocumentoDocx($path, $archivo, $cliente, $fecha, $fechaConvocatoria);
                break;

            case 'xlsx':
                $this->guardarDocumentoXlsx($path, $archivo, $cliente, $fecha);
                break;

            default:
                $nombreArchivo = basename($archivo);
                $destinoArchivo = $path . "/" . $nombreArchivo;
                if (!file_exists($destinoArchivo)) {
                    copy($archivo, $destinoArchivo);
                }
                break;
        }
    }

    public function guardarDocumentoDocx($path, $archivo, $cliente, $fecha, $fechaConvocatoria)
    {
        try {
            if (!file_exists($archivo)) {
                throw new \Exception("El archivo $archivo no existe.");
            }
            setlocale(LC_TIME, 'es_ES.UTF-8');

            $rutaPlantilla = $archivo;

            $fechaCarbon = Carbon::parse($fecha);
            $dia = $fechaCarbon->day;
            $nombreMes = $fechaCarbon->formatLocalized('%B');
            $anio = $fechaCarbon->year;
            $hora = Carbon::now()->format('H:i');

            $fechaCarbonConvocatoria = Carbon::parse($fechaConvocatoria);
            $diaConvocatoria = $fechaCarbon->day;
            $nombreMesConvocatoria = $fechaCarbon->formatLocalized('%B');
            $anioConvocatoria = $fechaCarbon->year;
            $horaConvocatoria = Carbon::now()->format('H:i');

            $fechaConvocatoriaDosDias = $fechaCarbonConvocatoria->addDays(2);
            $diaConvocatoriaDos = $fechaConvocatoriaDosDias->day;
            $mesConvocatoriaDos = $fechaConvocatoriaDosDias->formatLocalized('%B');

            $fechaConvocatoriaTresDias = $fechaCarbonConvocatoria->addDays(3);
            $diaConvocatoriaTres = $fechaConvocatoriaTresDias->day;
            $mesConvocatoriaTres = $fechaConvocatoriaTresDias->formatLocalized('%B');

            $fechaConvocatoriaCuatroDias = $fechaCarbonConvocatoria->addDays(4);
            $diaConvocatoriaCuatro = $fechaConvocatoriaCuatroDias->day;
            $mesConvocatoriaCuatro = $fechaConvocatoriaCuatroDias->formatLocalized('%B');

            $fechaConvocatoriaCincoDias = $fechaCarbonConvocatoria->addDays(5);
            $diaConvocatoriaCinco = $fechaConvocatoriaCincoDias->day;
            $mesConvocatoriaCinco = $fechaConvocatoriaCincoDias->formatLocalized('%B');

            $fechaConvocatoriaSeisDias = $fechaCarbonConvocatoria->addDays(6);
            $diaConvocatoriaSeis = $fechaConvocatoriaSeisDias->day;
            $mesConvocatoriaSeis = $fechaConvocatoriaSeisDias->formatLocalized('%B');

            $sedes = TerceroSucursalModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)->count();
            $empleados = TerceroEmpleadoModel::where('Tercero_oidTercero', $cliente->oidTercero)->count();
            $arl = TerceroLaboralModel::from('asn_tercerolaboral as tl')
                ->join('asn_tercero as t', 'tl.Tercero_oidSalud', 't.oidTercero')
                ->where('Tercero_oidTercero_1a1', $cliente->oidTercero)
                ->first();

            $this->array[] = basename($archivo);

            if (
                normalizer_normalize(basename($archivo), \Normalizer::FORM_C) ==
                normalizer_normalize('1.1.6.3 Formato Acta de Nombramiento y de Funciones de Personas Vigías o Integrantes del Comité Paritario de Seguridad y Seguridad en el Trabajo.docx', \Normalizer::FORM_C)
            ) {
                $integrantesCopasst = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
                    ->select('txNombreTerceroIntegranteComite', 'inDocumentoTerceroIntegrantesComite')
                    ->where('txTipoTerceroIntegranteComite', 'Copasst')
                    ->get();

                $this->generarDocumentoCopasst($integrantesCopasst, $archivo, $cliente, $path, $dia, $nombreMes, $anio);
            } else if (
                normalizer_normalize(basename($archivo), \Normalizer::FORM_C) ==
                normalizer_normalize('1.1.8.3 Acta de Conformación, Funciones y Confidencialidad Comité de Convivencia Laboral.docx', \Normalizer::FORM_C)
            ) {
                $integrantesConvivencia = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
                    ->select('txNombreTerceroIntegranteComite', 'inDocumentoTerceroIntegrantesComite')
                    ->where('txTipoTerceroIntegranteComite', 'Convivencia')
                    ->get();

                $this->generarDocumentoConvivencia($integrantesConvivencia, $archivo, $cliente, $path, $dia, $nombreMes, $anio);
            } else if (
                normalizer_normalize(basename($archivo), \Normalizer::FORM_C) ==
                normalizer_normalize('5.1.2.1 Acta de Nombramiento de Brigadista.docx', \Normalizer::FORM_C)
            ) {
                $integrantesBrigada = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
                    ->select('txNombreTerceroIntegranteComite', 'inDocumentoTerceroIntegrantesComite')
                    ->where('txTipoTerceroIntegranteComite', 'Cocola')
                    ->get();

                $this->generarDocumentoBrigada($integrantesBrigada, $archivo, $cliente, $path, $dia, $nombreMes, $anio);
            } else {
                $templateProcessor = new TemplateProcessor($rutaPlantilla);

                $templateProcessor->setValues(array(
                    'documentoCliente' => $this->quitarEspacios(number_format($cliente->txDocumentoTercero, 0, ',', '.')),
                    'nombreCliente' => $this->quitarEspacios($cliente->txNombreTercero),
                    'ciudadCliente' => $this->quitarEspacios($cliente->ciudad->txNombreCiudad),
                    'departamentoCliente' => $this->quitarEspacios($cliente->ciudad->departamento->txNombreDepartamento),
                    'direccionCliente' => $cliente->txDireccionTercero,
                    'telefonoCliente' => $cliente->txTelefonoTercero,
                    'sedesCliente' => $sedes + 1,
                    'empleadosCliente' => $empleados,
                    'arlCliente' => $arl->txNombreTercero ?? '',
                    'fechaDia' => $dia,
                    'fechaMes' => $nombreMes,
                    'fechaAnio' => $anio,
                    'fechaHora' => $hora,
                    'fechaDiaConvocatoria' => $diaConvocatoria,
                    'fechaMesConvocatoria' => $nombreMesConvocatoria,
                    'fechaHoraConvocatoria' => $horaConvocatoria,
                    'fechaDiaConvocatoriaDos' => $diaConvocatoriaDos,
                    'fechaMesConvocatoriaDos' => $mesConvocatoriaDos,
                    'fechaDiaConvocatoriaTres' => $diaConvocatoriaTres,
                    'fechaMesConvocatoriaTres' => $mesConvocatoriaTres,
                    'fechaDiaConvocatoriaCuatro' => $diaConvocatoriaCuatro,
                    'fechaMesConvocatoriaCuatro' => $mesConvocatoriaCuatro,
                    'fechaDiaConvocatoriaCinco' => $diaConvocatoriaCinco,
                    'fechaMesConvocatoriaCinco' => $mesConvocatoriaCinco,
                    'fechaDiaConvocatoriaSeis' => $diaConvocatoriaSeis,
                    'fechaMesConvocatoriaSeis' => $mesConvocatoriaSeis,
                    'anioConvocatoria' => $anioConvocatoria,
                    'responsableSST' => $this->quitarEspacios($cliente->txNombreResponsableSSTTercero),
                    'documentoResponsableSST' => $this->quitarEspacios(number_format($cliente->inDocumentoResponsableSSTTercero, 0, ',', '.')),
                    'representanteLegal' => $this->quitarEspacios($cliente->txNombreRepresentanteLegalTercero),
                    'documentoRepresentante' => $this->quitarEspacios(number_format($cliente->inDocumentoRepresentanteLegalTercero, 0, ',', '.')),
                    'nombreIntegranteCopasst' => $this->quitarEspacios($cliente->txNombreIntegranteComiteCopassTercero),
                    'documentoIntegranteCopasst' => $this->quitarEspacios(number_format($cliente->inDocumentoIntegranteComiteCopasstTercero, 0, ',', '.')),
                    'nombreIntegranteConvivencia' => $this->quitarEspacios($cliente->txNombreIntegranteComiteConvivenciaTercero),
                    'documentoIntegranteConvivencia' => $this->quitarEspacios(number_format($cliente->inDocumentoIntegranteComiteConvivenciaTerce, 0, ',', '.')),
                    'nombreIntegranteBrigada' => $this->quitarEspacios($cliente->txNombreIntegranteBrigadaTercero),
                    'documentoIntegranteBrigada' => $this->quitarEspacios(number_format($cliente->inDocumentoIntegranteBrigadaTercero, 0, ',', '.')),
                    'actividadEconomicaCliente' => $cliente->actividadEconomica->txNombreActividadEconomica ?? '',
                    'misionCliente' => $this->quitarEspacios($cliente->txMisionTercero),
                    'visionCliente' => $this->quitarEspacios($cliente->txVisionTercero),
                    'valoresCorporativosCliente' => $this->quitarEspacios($cliente->txValoresCorporativoTercero),
                ));

                $imagen =  Storage::disk('discoc')->path("asociadonegocio/tercero/" . $cliente->imImagenTercero);

                $imgBase64 = self::base64($imagen);
                if ($imgBase64 != '') {
                    $templateProcessor->setImageValue('imLogoTerceroCliente', [
                        'path' => $imgBase64,
                        'width' => 100,
                        'height' => 100,
                    ]);
                }

                $templateProcessor->saveAs("$path/" . basename($archivo));
            }
        } catch (\Exception $e) {
            throw new \Exception("Error al generar el archivo " . basename($archivo) . ": " . $e->getMessage());
        }
    }

    public function generarDocumentoCopasst($integrantesCopasst, $archivo, $cliente, $path, $dia, $mes, $anio)
    {
        foreach ($integrantesCopasst as $integrante) {
            $templateProcessor = new TemplateProcessor($archivo);

            $templateProcessor->setValues([
                'documentoCliente' => $this->quitarEspacios(number_format($cliente->txDocumentoTercero, 0, ',', '.')),
                'nombreCliente' => $this->quitarEspacios($cliente->txNombreTercero),
                'fechaDia' => $dia,
                'fechaMes' => $mes,
                'fechaAnio' => $anio,
                'nombreIntegranteCopasstReg' => $integrante->txNombreTerceroIntegranteComite,
                'documentoIntegranteCopasstReg' => number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.'),
                'nombreIntegranteCopasst' => $integrante->txNombreTerceroIntegranteComite,
                'documentoIntegranteCopasst' => number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.'),
            ]);

            $imagen =  Storage::disk('discoc')->path("asociadonegocio/tercero/" . $cliente->imImagenTercero);

            $imgBase64 = self::base64($imagen);
            if ($imgBase64 != '') {
                $templateProcessor->setImageValue('imLogoTerceroCliente', [
                    'path' => $imgBase64,
                    'width' => 100,
                    'height' => 100,
                ]);
            }

            $nombreArchivo = $integrante->txNombreTerceroIntegranteComite . "_" . basename($archivo);
            $templateProcessor->saveAs("$path/$nombreArchivo");
        }
    }

    public function generarDocumentoConvivencia($integrantesConvivencia, $archivo, $cliente, $path, $dia, $mes, $anio)
    {
        foreach ($integrantesConvivencia as $integrante) {
            $templateProcessor = new TemplateProcessor($archivo);

            $templateProcessor->setValues([
                'documentoCliente' => $this->quitarEspacios(number_format($cliente->txDocumentoTercero, 0, ',', '.')),
                'nombreCliente' => $this->quitarEspacios($cliente->txNombreTercero),
                'nombreIntegranteConvivenciaReg' => $integrante->txNombreTerceroIntegranteComite,
                'documentoIntegranteConvivenciaReg' => number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.'),
                'nombreIntegranteConvivencia' => $this->quitarEspacios($integrante->txNombreTerceroIntegranteComite),
                'documentoIntegranteConvivencia' => $this->quitarEspacios(number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.')),
                'fechaDia' => $dia,
                'fechaMes' => $mes,
                'fechaAnio' => $anio,
            ]);

            $imagen =  Storage::disk('discoc')->path("asociadonegocio/tercero/" . $cliente->imImagenTercero);

            $imgBase64 = self::base64($imagen);
            if ($imgBase64 != '') {
                $templateProcessor->setImageValue('imLogoTerceroCliente', [
                    'path' => $imgBase64,
                    'width' => 100,
                    'height' => 100,
                ]);
            }

            $nombreArchivo = $integrante->txNombreTerceroIntegranteComite . "_" . basename($archivo);
            $templateProcessor->saveAs("$path/$nombreArchivo");
        }
    }

    public function generarDocumentoBrigada($integrantesBrigada, $archivo, $cliente, $path, $dia, $mes, $anio)
    {
        foreach ($integrantesBrigada as $integrante) {
            $templateProcessor = new TemplateProcessor($archivo);

            $templateProcessor->setValues([
                'documentoCliente' => $this->quitarEspacios(number_format($cliente->txDocumentoTercero, 0, ',', '.')),
                'nombreCliente' => $this->quitarEspacios($cliente->txNombreTercero),
                'arlCliente' => $this->quitarEspacios($cliente->arl->txNombreArl ?? ''),
                'fechaDia' => $dia,
                'fechaMes' => $mes,
                'fechaAnio' => $anio,
                'nombreIntegranteBrigadaReg' => $integrante->txNombreTerceroIntegranteComite,
                'documentoIntegranteBrigadaReg' => number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.'),
                'nombreIntegranteBrigada' => $integrante->txNombreTerceroIntegranteComite,
                'documentoIntegranteBrigada' => number_format($integrante->inDocumentoTerceroIntegrantesComite, 0, ',', '.'),
            ]);

            $imagen =  Storage::disk('discoc')->path("asociadonegocio/tercero/" . $cliente->imImagenTercero);

            $imgBase64 = self::base64($imagen);
            if ($imgBase64 != '') {
                $templateProcessor->setImageValue('imLogoTerceroCliente', [
                    'path' => $imgBase64,
                    'width' => 100,
                    'height' => 100,
                ]);
            }

            $nombreArchivo = $integrante->txNombreTerceroIntegranteComite . "_" . basename($archivo);
            $templateProcessor->saveAs("$path/$nombreArchivo");
        }
    }
    public function guardarDocumentoXlsx($path, $archivo, $cliente, $fecha)
    {
        setlocale(LC_TIME, 'es_ES.UTF-8');

        $rutaExcel = ($archivo);

        $lectorExcel = IOFactory::createReader('Xlsx');
        $archivoExcel = $lectorExcel->load($rutaExcel);

        $hojaActiva = $archivoExcel->getActiveSheet();

        $integrantesComiteCopasst = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
            ->select(DB::raw('GROUP_CONCAT(txNombreTerceroIntegranteComite) as txNombreTerceroIntegranteComite'))
            ->where('txTipoTerceroIntegranteComite', 'Copasst')
            ->first();

        $integrantesComiteConvivencia = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
            ->select(DB::raw('GROUP_CONCAT(txNombreTerceroIntegranteComite) as txNombreTerceroIntegranteComite'))
            ->where('txTipoTerceroIntegranteComite', 'Convivencia')
            ->first();

        $integrantesComiteBrigada = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', $cliente->oidTercero)
            ->select(DB::raw('GROUP_CONCAT(txNombreTerceroIntegranteComite) as txNombreTerceroIntegranteComite'))
            ->where('txTipoTerceroIntegranteComite', 'Cocola')
            ->first();

        $celdas = [
            'A3' => ['C3', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'A4' => ['B4', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'A5' => [
                ['B5', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
                ['B5', 'NIT:', number_format($cliente->txDocumentoTercero, 0, ',', '.')],
                ['C5', 'FECHA DE ELABORACIÓN:', $fecha],
            ],
            'A6' => [
                ['B6', 'RESPONSABLE SG-SST:', $cliente->txNombreResponsableSSTTercero],
                ['B6', 'RESPONSABLE:', $cliente->txNombreResponsableSSTTercero],
                ['B6', 'REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
                ['B6', 'MIEMBROS DEL COPASST O VIGÍA:', $integrantesComiteCopasst->txNombreTerceroIntegranteComite ?? ''],
                ['B6', 'MIEMBROS DEL COCOLA:', $integrantesComiteConvivencia->txNombreTerceroIntegranteComite],
                ['B6', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
                ['B6', 'NIT:', number_format($cliente->txDocumentoTercero, 0, ',', '.')],
                ['C6', 'MIEMBROS DE LA BRIGADA:', $integrantesComiteBrigada->txNombreTerceroIntegranteComite],
            ],
            'A7' => [
                ['B7', 'REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
                ['B7', 'MIEMBROS DEL COCOLA:', $integrantesComiteConvivencia->txNombreTerceroIntegranteComite],
                ['B7', 'NIT:', number_format($cliente->txDocumentoTercero, 0, ',', '.')]
            ],
            'A8' => [
                ['B8', 'RESPONSABLE DEL SG-SST:', $cliente->txNombreResponsableSSTTercero],
            ],
            'A9' => [
                ['B9', 'REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
            ],
            'B1' => ['B1', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'B2' => ['B2', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'B81' => ['B81', 'NOMBRE RESPONSABLE SST', $cliente->txNombreResponsableSSTTercero],
            'B82' => ['B82', 'CC', number_format($cliente->inDocumentoResponsableSSTTercero, 0, ',', '.')],
            'C1' => ['C1', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'C2' => ['C2', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'D1' => ['D1', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'E1' => ['E1', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'E2' => ['E2', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'E3' => ['E3', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'F65' => ['F65', 'NOMBRE REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
            'F66' => ['F66', 'NOMBRE REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
            'F66' => ['F66', 'NOMBRE REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
            'F66' => ['F66', 'CC', number_format($cliente->inDocumentoRepresentanteLegalTercero, 0, ',', '.')],
            'F77' => ['F77', 'CC', number_format($cliente->inDocumentoRepresentanteLegalTercero, 0, ',', '.')],
            'J1' => ['J1', 'NOMBRE DE LA EMPRESA:', $cliente->txNombreTercero],
            'M81' => ['M81', 'NOMBRE REPRESENTANTE LEGAL:', $cliente->txNombreRepresentanteLegalTercero],
            'M82' => ['M82', 'CC', number_format($cliente->inDocumentoRepresentanteLegalTercero, 0, ',', '.')],
        ];

        foreach ($celdas as $celda => $valor) {
            $valorCelda = $hojaActiva->getCell($celda)->getValue();

            if (is_array(reset($valor))) {
                foreach ($valor as $v) {
                    if ($valorCelda == $v[1]) {
                        $hojaActiva->setCellValue($v[0], $v[2]);
                    }
                }
            } else {
                if ($valorCelda == $valor[1]) {
                    $hojaActiva->setCellValue($valor[0], $valor[2]);
                }
            }
        }


        $rutaImagen = Storage::disk('discoc')->path("asociadonegocio/tercero/" . $cliente->imImagenTercero);
        $imgBase64 = self::base64($rutaImagen);
        if ($imgBase64 != '') {
            $imagen = new Drawing();
            $imagen->setName('Logo');
            $imagen->setDescription('Logo de la empresa');
            $imagen->setPath($rutaImagen);
            $imagen->setCoordinates('A1');
            $imagen->setWidth(100);
            $imagen->setHeight(100);
            $imagen->setWorksheet($hojaActiva);
        }

        $guardarExcel = IOFactory::createWriter($archivoExcel, 'Xlsx');
        $guardarExcel->save("$path/" . basename($archivo));
    }

    function quitarEspacios($cadena)
    {
        return preg_replace(['/\s+/', '/^\s|\s$/'], [' ', ''], $cadena);
    }

    function base64($archivo)
    {
        $logo = '';
        if (!is_file($archivo)) {
            return $logo;
        }

        $fp = fopen($archivo, "r");
        if ($fp) {
            $imagen = fread($fp, filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  "data:image/png;base64,$base64";
        }
        return $logo;
    }

    public function generarZip()
    {
        $folderName = 'estandares_generados';
        $folderPath = public_path($folderName);

        if (!is_dir($folderPath)) {
            mkdir($folderPath, 0777, true);
        }

        $zipFileName = $folderName . '.zip';

        $zipFilePath = public_path($zipFileName);

        $zip = new ZipArchive();

        if ($zip->open($zipFilePath, ZipArchive::CREATE) === true) {
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($folderPath),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file) {
                if (!$file->isDir()) {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($folderPath) + 1);
                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
        } else {
            return response()->json(['message' => 'No se pudo crear el archivo zip.'], 500);
        }
    }

    public function limpiarArchivos($carpeta)
    {
        function rmDir_rf($carpeta)
        {
            foreach (glob($carpeta . "/*") as $archivos_carpeta) {
                if (is_dir($archivos_carpeta)) {
                    rmDir_rf($archivos_carpeta);
                } else {
                    unlink($archivos_carpeta);
                }
            }
            rmdir($carpeta);
        }

        if (file_exists($carpeta)) {
            rmDir_rf($carpeta);
        }
    }
}
