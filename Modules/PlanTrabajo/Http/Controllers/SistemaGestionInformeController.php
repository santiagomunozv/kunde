<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\PlanTrabajo\Entities\SistemaGestionInformeModel;
use DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\PlanTrabajo\Http\Requests\SistemaGestionInformeRequest;

class SistemaGestionInformeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');
        $sistemaGestionInforme = [];
        
            return view('plantrabajo::sistemaGestionInformeForm', compact('cliente', 'sistemaGestionInforme'));
    }   
}
