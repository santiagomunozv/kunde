<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;

class PlanTrabajoClienteController extends Controller
{

    public function planTrabajoClienteMes()
    {
        $consulta = DB::select("SELECT
            txNombreTercero,
            txActividadProgramadaConfiguracionPlanTrabajo,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-01'), 1, 0) AS ENERO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-02'), 1, 0) AS FEBRERO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-03'), 1, 0) AS MARZO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-04'), 1, 0) AS ABRIL,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-05'), 1, 0) AS MAYO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-06'), 1, 0) AS JUNIO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-07'), 1, 0) AS JULIO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-08'), 1, 0) AS AGOSTO,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-09'), 1, 0) AS SEPTIEMBRE,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-10'), 1, 0) AS OCTUBRE,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-11'), 1, 0) AS NOVIEMBRE,
            IF(DATE_FORMAT(DATE_ADD(daFechaIngresoTerceroLaboral, interval txMesConfiguracionPlanTrabajo MONTH), '%Y-%m') = CONCAT(DATE_FORMAT(CURDATE(), '%Y'),'-12'), 1, 0) AS DICIEMBRE
        FROM
        asn_tercero t
            JOIN asn_tercerolaboral tl ON t.oidTercero = tl.Tercero_oidTercero_1a1
            JOIN gen_programacionplantrabajo ppt ON t.oidTercero = ppt.Tercero_oidTerceroCompania
            join gen_actividadplantrabajo apt on ppt.oidProgramacionPlanTrabajo = apt.ProgramacionPlanTrabajo_oidPlanTrabajo_1aM
            join gen_configuracionplantrabajo cpt on apt.ConfiguracionPlanTrabajo_oidConfiguracionPlan = cpt.oidConfiguracionPlanTrabajo

        ");

        if (count($consulta) == 0) {
            echo 'No se encontraron registros';
            return;
        }

        return view('plantrabajo::InformePlanTrabajoCliente', compact('consulta'));
    }
}
