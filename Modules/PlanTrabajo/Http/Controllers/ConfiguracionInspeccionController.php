<?php

namespace Modules\PlanTrabajo\Http\Controllers;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\PlanTrabajo\Entities\ConfiguracionInspeccionModel;
use Modules\PlanTrabajo\Entities\ConfiguracionInspeccionTipoInspeccionModel;
use Modules\PlanTrabajo\Entities\TipoInspeccionModel;
use Modules\PlanTrabajo\Http\Requests\ConfiguracionInspeccionRequest;

class ConfiguracionInspeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $configuracionInspeccion = ConfiguracionInspeccionModel::selectRaw('oidConfiguracionInspeccion, txNombreConfiguracionInspeccion, txDescripcionConfiguracionInspeccion')
            ->get();

        return view('plantrabajo::configuracionInspeccionGrid', compact('permisos', 'configuracionInspeccion'), ['configuracionInspeccion' => $configuracionInspeccion]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $configuracionInspeccion = new ConfiguracionInspeccionModel();
        $configuracionInspeccionTipoInspeccion = new ConfiguracionInspeccionTipoInspeccionModel();
        $oidTipoInspeccion = TipoInspeccionModel::All()->pluck('oidTipoInspeccion');
        $txNombreTipoInspeccion = TipoInspeccionModel::All()->pluck('txNombreTipoInspeccion');

        return view('plantrabajo::configuracionInspeccionForm', compact("configuracionInspeccion", "configuracionInspeccionTipoInspeccion", "oidTipoInspeccion", "txNombreTipoInspeccion"), ['configuracionInspeccion' => $configuracionInspeccion]);
    }

    /**
     * Store a newly created resource in storage.
     * @param ConfiguracionInspeccionRequest $request
     * @return Response
     */
    public function store(ConfiguracionInspeccionRequest $request)
    {
        DB::beginTransaction();
        try {
            $configuracionInspeccion = new ConfiguracionInspeccionModel();
            $configuracionInspeccion->fill($request->all());
            $configuracionInspeccion->save();
            $this->grabarDetalle($request, $configuracionInspeccion->oidConfiguracionInspeccion);
            DB::commit();
            return response(['oidConfiguracionInspeccion' => $configuracionInspeccion->oidConfiguracionInspeccion], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('plantrabajo::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $configuracionInspeccion = ConfiguracionInspeccionModel::find($id);
        $configuracionInspeccionTipoInspeccion = ConfiguracionInspeccionTipoInspeccionModel::where("ConfiguracionTipoInspeccion_oidConfiguracionTipoInspeccion", '=', $id)->get();
        $oidTipoInspeccion = TipoInspeccionModel::All()->pluck('oidTipoInspeccion');
        $txNombreTipoInspeccion = TipoInspeccionModel::All()->pluck('txNombreTipoInspeccion');

        return view('plantrabajo::configuracionInspeccionForm', compact("configuracionInspeccion", "configuracionInspeccionTipoInspeccion", "oidTipoInspeccion", "txNombreTipoInspeccion"), ['configuracionInspeccion' => $configuracionInspeccion]);
    }

    /**
     * Update the specified resource in storage.
     * @param ConfiguracionInspeccionRequest $request
     * @param int $id
     * @return Response
     */
    public function update(ConfiguracionInspeccionRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $configuracionInspeccion = ConfiguracionInspeccionModel::find($id);
            $configuracionInspeccion->fill($request->all());
            $configuracionInspeccion->save();
            $this->grabarDetalle($request, $configuracionInspeccion->oidConfiguracionInspeccion);
            DB::commit();
            return response(['oidConfiguracionInspeccion' => $configuracionInspeccion->oidConfiguracionInspeccion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    private function grabarDetalle($request, $id)
    {
        $modelo = new ConfiguracionInspeccionTipoInspeccionModel();
        $idsEliminar = explode(',', $request['eliminarConfiguracionInspeccion']);
        $modelo::whereIn('oidConfiguracionInspeccionTipoInspeccion', $idsEliminar)->delete();
        $total = ($request['oidConfiguracionInspeccionTipoInspeccion'] !== null) ? count($request['oidConfiguracionInspeccionTipoInspeccion']) : 0;
        for ($i = 0; $i <  $total; $i++) {
            $indice = array('oidConfiguracionInspeccionTipoInspeccion' => $request['oidConfiguracionInspeccionTipoInspeccion'][$i]);
            $datos = [
                'ConfiguracionTipoInspeccion_oidConfiguracionTipoInspeccion' => $id,
                'TipoInspeccion_oidTipoInspeccion' => $request["TipoInspeccion_oidTipoInspeccion"][$i],
            ];
            $modelo::updateOrCreate($indice, $datos);
        }
    }
}
