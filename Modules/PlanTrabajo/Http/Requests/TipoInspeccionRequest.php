<?php

namespace Modules\PlanTrabajo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TipoInspeccionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            'txNombreTipoInspeccion' => 'required',
            'txDescripcionTipoInspeccion' => 'required',
            'txNombreTipoInspeccionDetalle.*' => 'required',
        );

        return $validacion;
    }



    public function messages()
    {
        $mensaje = array();
        $mensaje['txNombreTipoInspeccion.required'] = "El nombre es obligatorio";
        $mensaje['txDescripcionTipoInspeccion.required'] = "La descripción es obligatoria";

        $total = ($this->get("oidTipoInspeccionDetalle") !== null) ? count($this->get("oidTipoInspeccionDetalle")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['txNombreTipoInspeccionDetalle.' . $j . '.required'] = "El nombre de la lista de chequeo es obligatorio en el registro " . ($j + 1);
        }

        return $mensaje;
    }
}
