<?php

namespace Modules\PlanTrabajo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfiguracionInspeccionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            'txNombreConfiguracionInspeccion' => 'required',
            'txDescripcionConfiguracionInspeccion' => 'required',
            'TipoInspeccion_oidTipoInspeccion.*' => 'required',
        );

        return $validacion;
    }



    public function messages()
    {
        $mensaje = array();
        $mensaje['txNombreConfiguracionInspeccion.required'] = "El nombre es obligatorio";
        $mensaje['txDescripcionConfiguracionInspeccion.required'] = "La descripción es obligatoria";

        $total = ($this->get("oidConfiguracionInspeccionTipoInspeccion") !== null) ? count($this->get("oidConfiguracionInspeccionTipoInspeccion")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['TipoInspeccion_oidTipoInspeccion.' . $j . '.required'] = "El tipo de inspección es obligatorio en el registro " . ($j + 1);
        }

        return $mensaje;
    }
}
