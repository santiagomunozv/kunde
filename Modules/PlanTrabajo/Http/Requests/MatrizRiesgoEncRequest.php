<?php

    namespace Modules\PlanTrabajo\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

    class MatrizRiesgoEncRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
        $validacion = array(
            'Tercero_oidCliente' => 'required',
            'daFechaMatrizRiesgoEnc' => 'required',
            'Cargo_oidCargo.*' => 'required',
            'txActividadCargoMatrizRiesgo.*' => 'required',
            'lsRutinariaMatrizRiesgo.*' => 'required',
            'lsClasificacionMatrizRiesgo.*' => 'required',
            'txDescripcionClasificacionMatrizRiesgo.*' => 'required',
            'txEfectosPosiblesMatrizRiesgo.*' => 'required',
            'txFuenteMatrizRiesgo.*' => 'required',
            'txMedioMatrizRiesgo.*' => 'required',
            'txIndividuoMatrizRiesgo.*' => 'required',
            'lsNivelDeficienciaMatrizRiesgo.*' => 'required',
            'lsNivelExposicionMatrizRiesgo.*' => 'required',
            'txNivelProbabilidadMatrzRiesgo.*' => 'required',
            'txInterpretacionProbabilidadMatrizRiesgo.*' => 'required',
            'lsNivelConsecuenciaMatrizRiesgo.*' => 'required',
            'txNivelRiesgoMatrizRiesgo.*' => 'required',
            'txInterpretacionRiesgoMatrizRiesgo.*' => 'required',
            'txAceptabilidadRiesgoMatrizRiesgo.*' => 'required',
            'inExpuestosMatrizRiesgo.*' => 'required',
            'txPeorConsecuenciaMatrizRiesgo.*' => 'required',
            'txEliminacionMatrizRiesgo.*' => 'required',
            'txSustitucionMatrizRiesgo.*' => 'required',
            'txControlIngenieria.*' => 'required',
            'txControlAdministrativoMatrizRiesgo.*' => 'required',
            'txElementosProteccionPersonalMatrizRiesgo.*' => 'required',
          );
    
          return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
            $mensaje['Tercero_oidCliente.required'] = "El cliente es obligatorio";
            $mensaje['daFechaMatrizRiesgoEnc.required'] = "La fecha es obligatoria";
       
            $total = ($this->get("oidMatrizRiesgo") !== null ) ? count($this->get("oidMatrizRiesgo")) : 0;
            for($j=0; $j < $total; $j++)
            {
                 $mensaje['Cargo_oidCargo.'.$j.'.required'] = "El proceso es obligatorio en el registro ".($j+1);
                 $mensaje['txActividadCargoMatrizRiesgo.'.$j.'.required'] = "La actividad es obligatoria en el registro ".($j+1);
                 $mensaje['lsRutinariaMatrizRiesgo.'.$j.'.required'] = "Es rutinaria es obligatorio en el registro ".($j+1);
                 $mensaje['lsClasificacionMatrizRiesgo.'.$j.'.required'] = "La clasificación es obligatorio en el registro ".($j+1);
                 $mensaje['txDescripcionClasificacionMatrizRiesgo.'.$j.'.required'] = "La descripción es obigatoria en el registro ".($j+1);
                 $mensaje['txEfectosPosiblesMatrizRiesgo.'.$j.'.required'] = "Los efectos posibles son obligatorios en el registro ".($j+1);
                 $mensaje['txFuenteMatrizRiesgo.'.$j.'.required'] = "La fuente es obligatoria en el registro ".($j+1);
                 $mensaje['txMedioMatrizRiesgo.'.$j.'.required'] = "El medio es obligatorio en el registro ".($j+1);
                 $mensaje['txIndividuoMatrizRiesgo.'.$j.'.required'] = "El individuo es obligatorio en el registro ".($j+1);
                 $mensaje['lsNivelDeficienciaMatrizRiesgo.'.$j.'.required'] = "El nivel de deficiencia es obligatorio en el registro ".($j+1);
                 $mensaje['lsNivelExposicionMatrizRiesgo.'.$j.'.required'] = "El nivel de exposición es obligatorio en el registro ".($j+1);
                 $mensaje['txNivelProbabilidadMatrzRiesgo.'.$j.'.required'] = "El nivel de probabilidad es obligatorio en el registro ".($j+1);
                 $mensaje['txInterpretacionProbabilidadMatrizRiesgo.'.$j.'.required'] = "La interpretación de probabilidad es obligatoria en el registro ".($j+1);
                 $mensaje['lsNivelConsecuenciaMatrizRiesgo.'.$j.'.required'] = "El nivel de consecuencia es obligatorio en el registro ".($j+1);
                 $mensaje['txNivelRiesgoMatrizRiesgo.'.$j.'.required'] = "El nivel de riesgo es obligatorio en el registro ".($j+1);
                 $mensaje['txInterpretacionRiesgoMatrizRiesgo.'.$j.'.required'] = "La interpretación de probabilidad es obligatoria en el registro ".($j+1);
                 $mensaje['txAceptabilidadRiesgoMatrizRiesgo.'.$j.'.required'] = "La aceptabilidad es obligatoria en el registro ".($j+1);
                 $mensaje['inExpuestosMatrizRiesgo.'.$j.'.required'] = "El N de expuestos es obligatorio en el registro ".($j+1);
                 $mensaje['txPeorConsecuenciaMatrizRiesgo.'.$j.'.required'] = "La peor consecuencia es obligatoria en el registro ".($j+1);
                 $mensaje['txEliminacionMatrizRiesgo.'.$j.'.required'] = "La eliminación es obligatoria en el registro ".($j+1);
                 $mensaje['txSustitucionMatrizRiesgo.'.$j.'.required'] = "La sustitución es obligatoria en el registro ".($j+1);
                 $mensaje['txControlIngenieria.'.$j.'.required'] = "El control de ingeniería es obligatorio en el registro ".($j+1);
                 $mensaje['txControlAdministrativoMatrizRiesgo.'.$j.'.required'] = "El control administrativo es obligatorio en el registro ".($j+1);
                 $mensaje['txElementosProteccionPersonalMatrizRiesgo.'.$j.'.required'] = "Los elementos de protección personal son obligatorios en el registro ".($j+1);
            }
  
            return $mensaje;
        }
    }