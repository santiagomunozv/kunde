<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('plantrabajo')->group(function () {
        Route::resource('/ejecucionplantrabajo', 'EjecucionPlanTrabajoController');
        Route::get('/ejecucionplantrabajoedit/{id}/{metodo}', 'EjecucionPlanTrabajoController@edit');
        Route::put('ejecucionplantrabajoupdate/{id}', 'EjecucionPlanTrabajoController@UpdateEjecucionPlanTrabajo');
        Route::get('/planTrabajoEjecucionBitacora/{id}', 'EjecucionPlanTrabajoController@planTrabajoEjecucionBitacora');
        Route::get('/planTrabajoEjecucionBitacora/{id}/modal', 'EjecucionPlanTrabajoController@planTrabajoEjecucionBitacoraModal');
        Route::put('bitacoraupdate/{id}', 'EjecucionPlanTrabajoController@updateBitacora');
        Route::get('/ejecucionactividadplantrabajodetalle/{idActividad}/{idDetalle}/modal', 'EjecucionPlanTrabajoController@getEjecucionActividadPlantrabajoDetalle');
        Route::put('ejecucionactividadupdate/{id}', 'EjecucionPlanTrabajoController@updateEjecucionActividad');
        Route::get('/informePlanTrabajo', 'EjecucionPlanTrabajoController@informePlanTrabajo');
        Route::get('generarbitacora/{id}', 'EjecucionPlanTrabajoController@generarBitacora');
        Route::get('sistemagestioninforme', 'SistemaGestionInformeController@index');
        Route::put('sistemagestioninforme', 'SistemaGestionInformeController@update');
        Route::get('bitacoramanual', 'BitacoraManualController@index');
        Route::post('bitacoramanual', 'BitacoraManualController@store');
        Route::resource('/matrizriesgo', 'MatrizRiesgoEncController');
        Route::get('consultarMatrizByCliente/{id}', 'MatrizRiesgoEncController@consultarMatrizByCliente');
        // plan trabajo mes a mes
        Route::get('informePlanTrabajoCliente', 'PlanTrabajoClienteController@planTrabajoClienteMes');
        // Route::get('diseno', 'DiseñoController@index');
        // Route::post('diseno', 'DiseñoController@store');
        Route::get('reporteministerio', 'ReporteMinisterioController@index');
        Route::post('reporteministerio', 'ReporteMinisterioController@store');


        Route::get('estandares', 'EstandarController@index');
        Route::post('estandares', 'EstandarController@store');


        Route::resource('/tipoinspeccion', 'TipoInspeccionController');
        Route::resource('/configuracioninspeccion', 'ConfiguracionInspeccionController');
    });
});
