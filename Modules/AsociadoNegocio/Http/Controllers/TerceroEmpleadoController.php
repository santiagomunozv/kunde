<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use App\Exports\TerceroEmpleadosExport;
use App\Imports\EmpleadoImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\TipoIdentificacionModel;
use DB;
use Exception;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Modules\AsociadoNegocio\Entities\GrupoFamiliarEmpleadoModel;
use Modules\AsociadoNegocio\Http\Requests\TerceroEmpleadoRequest;
use Modules\General\Entities\ArlModel;
use Modules\General\Entities\CargoModel;
use Modules\General\Entities\CiudadModel;
use Modules\General\Entities\PaisModel;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class TerceroEmpleadoController extends Controller
{

    public function edit($id, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la vista
        $empleados = TerceroEmpleadoModel::where('Tercero_oidTercero', '=', $id)->get();
        // tipo de documentos
        $tipoDocumentoId = TipoIdentificacionModel::where('gen_tipoidentificacion.txEstadoTipoIdentificacion', '=', 'Activo')->pluck('oidTipoIdentificacion');
        $tipoDcoumentoNombre = TipoIdentificacionModel::where('gen_tipoidentificacion.txEstadoTipoIdentificacion', '=', 'Activo')->pluck('txNombreTipoIdentificacion');
        // terceros
        $terceroNombre = TerceroModel::where('oidTercero', $id)->pluck('txNombreTercero');


        return view('asociadonegocio::TerceroEmpleadosForm', compact('empleados', 'tipoDocumentoId', 'tipoDcoumentoNombre', 'id', 'terceroNombre'));
    }

    public function update(TerceroEmpleadoRequest $request, $id)
    {
        DB::beginTransaction();

        try {
            $modelo = new TerceroEmpleadoModel();
            $idsEliminar = explode(',', $request['eliminarEmpleado']);
            $modelo::whereIn('oidTerceroEmpleado', $idsEliminar)->delete();

            if ($request['oidTerceroEmpleado'] != null) {
                for ($i = 0; $i < count($request['oidTerceroEmpleado']); $i++) {

                    $indice = array('oidTerceroEmpleado' => $request['oidTerceroEmpleado'][$i]);
                    $datos = [
                        'TipoIdentificacion_oidTerceroEmpleado' => $request['TipoIdentificacion_oidTerceroEmpleado'][$i],
                        'txDocumentoEmpleado' => $request['txDocumentoEmpleado'][$i],
                        'txNombreEmpleado' => $request['txNombreEmpleado'][$i],
                        'txApellidoEmpleado' => $request['txApellidoEmpleado'][$i],
                        'Tercero_oidTercero' => $id,
                    ];
                    $guardar = $modelo::updateOrCreate($indice, $datos);
                }
            }

            DB::commit();
            return response([], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function importarEmpleadosIndex($id)
    {
        $file = '/docs/';
        $this->eliminarArchivosPublic([$file]);
        return view('asociadonegocio::EmpleadoImportForm', compact('id'));
    }

    public function cargarArchivoEmpleado()
    {
        $destinationPath = public_path() . '/docs/';
        
        $fileName = Input::file('file')->getClientOriginalName();

        $upload_success = Input::file('file')->move($destinationPath, $fileName);

        if ($upload_success) {
            return response(["nombreArchivo" => $fileName], 200);
        } else {
            return abort(500, 'error');
        }
    }

    public function importarEmpleados(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $file = public_path() . "/docs/" . $request['nombreArchivo'];
            
            $datos = Excel::toArray(new EmpleadoImport, $file);

            if (!$datos) {
                throw new Exception('No se encontraron datos en la plantilla importada');
            }

            foreach ($datos[0] as $pos => $dato) {
                // dd($dato);
                if ($pos !== 0) {
                    // cualquier modificación de google doc se debe modificar estos cmapos tambien
                    $TipoIdentificacion_oidTerceroEmpleado = 1; //todos los empleados se importan con cedula
                    $txNombreEmpleado = $dato['2_nombres'];
                    $txApellidoEmpleado = $dato['3_apellidos'];
                    $txDocumentoEmpleado = $dato['4_documento_de_identidad'];
                    $txGeneroEmpleado = $dato['5_sexo'];
                    $dtFechaNacimientoEmpleado = Carbon::instance(Date::excelToDateTimeObject($dato['6_fecha_de_nacimiento']))->format('y/m/d'); // convierte en fecha reconocible para php
                    $Pais_oidTerceroEmpleadoN = 51;
                    $Ciudad_oidTerceroEmpleadoN = CiudadModel::where('txNombreCiudad', '=', $dato['7_lugar_de_nacimiento'])->pluck('oidCiudad');
                    $txEdadEmpleado = $dato['8_edad'];
                    $txEstadoCivilEmpleado = $dato['9_estado_civil'];
                    $txDireccionEmpleado = $dato['10_direccion_de_residencia'];
                    $txBarrioViviendaEmpleado = $dato['11_barrio'];
                    // $Ciudad_oidTerceroEmpleadoR = $dato[''];
                    $Cargo_oidTerceroEmpleado =  $dato['13_cargo'];
                    $txCelularEmpleado = $dato['14_celular'];
                    $txCorreoEmpleado = $dato['15_correo_electronico'];
                    $lsEstratoEmpleado = $dato['16_estrato'];
                    $txTiempoEmpresaEmpleado = $dato['17_tiempo_que_lleva_en_la_empresa'];
                    $txEpsEmpleado = $dato['18_indique_cual_es_su_eps'];
                    $Arl_oidEmpleado = ArlModel::where('txNombreArl', '=', $dato['19_indique_cual_es_su_arl'])->pluck('oidArl');
                    $txFondoPensionEmpleado = $dato['20_indique_cual_es_su_arp_administradoras_de_fondos_de_pension'];
                    $txHorarioTrabajoEmpleado = $dato['21_horario_de_trabajo'];
                    $txTipoContratoEmpleado = $dato['22_tipo_de_contrato'];
                    $intPersonasCargoEmpleado = $dato['23_numero_personas_a_cargo'];
                    $lsTipoViviendaEmpleado = $dato['24_tipo_de_vivienda'];
                    $lsEscolaridadEmpleado = $dato['25_nivel_de_escolaridad'];
                    $lsIngresosEmpleado = $dato['26_promedio_de_ingresos_smlv'];
                    $txTiempoLibreEmpleado = $dato['27_uso_del_tiempo_libre'];
                    $txDeporteEmpleado = $dato['28_practica_algun_deporte_en_caso_de_que_su_respuesta_sea_si_indique_cual'];
                    $txSustanciasEmpleado = $dato['29_consume_alguna_sustancia_psicoactiva_en_caso_de_que_su_respuesta_sea_si_indique_cual'];
                    $txEnfermedadesEmpleado = $dato['30_tiene_alguna_enfermedad_en_caso_de_que_su_respuesta_sea_si_indique_cual'];

                    // grupo familiar
                    $txNombreFamiliar = $dato['31_en_caso_de_accidente_con_quien_nos_podemos_comunicar_nombres_completos'];
                    $lsParentescoFamiliar = $dato['32_parentesco']; //en el doc cambiarlo po obciones
                    $txCelularFamiliar = $dato['33_numero_de_celular'];


                    $this->buscarTerceroEmpleado($txDocumentoEmpleado);

                    $terceroEmpleado = new TerceroEmpleadoModel();
                    $familiarEmpleado = new  GrupoFamiliarEmpleadoModel;

                    $terceroEmpleado->Tercero_oidTercero = $id;
                    $terceroEmpleado->TipoIdentificacion_oidTerceroEmpleado = $TipoIdentificacion_oidTerceroEmpleado;
                    $terceroEmpleado->txDocumentoEmpleado = $txDocumentoEmpleado;
                    $terceroEmpleado->txNombreEmpleado = $txNombreEmpleado;
                    $terceroEmpleado->txApellidoEmpleado = $txApellidoEmpleado;

                    // perfil sociodemografico
                    $terceroEmpleado->Cargo_oidTerceroEmpleado = $Cargo_oidTerceroEmpleado;
                    $terceroEmpleado->Pais_oidTerceroEmpleadoN = $Pais_oidTerceroEmpleadoN;
                    json_decode($Ciudad_oidTerceroEmpleadoN) ?  $terceroEmpleado->Ciudad_oidTerceroEmpleadoN = $Ciudad_oidTerceroEmpleadoN[0] : null;
                    $terceroEmpleado->dtFechaNacimientoEmpleado = $dtFechaNacimientoEmpleado;
                    $terceroEmpleado->txDireccionEmpleado = $txDireccionEmpleado;
                    $terceroEmpleado->txCelularEmpleado = $txCelularEmpleado;
                    $terceroEmpleado->txCorreoEmpleado = $txCorreoEmpleado;
                    $terceroEmpleado->lsEstratoEmpleado = $lsEstratoEmpleado;
                    $terceroEmpleado->txTiempoEmpresaEmpleado = $txTiempoEmpresaEmpleado;
                    $terceroEmpleado->txEpsEmpleado = $txEpsEmpleado;
                    json_decode($Arl_oidEmpleado) ?  $terceroEmpleado->Arl_oidEmpleado = $Arl_oidEmpleado[0] : null;
                    $terceroEmpleado->txHorarioTrabajoEmpleado = $txHorarioTrabajoEmpleado;
                    $terceroEmpleado->txTipoContratoEmpleado = $txTipoContratoEmpleado;
                    $terceroEmpleado->txGeneroEmpleado = $txGeneroEmpleado;
                    $terceroEmpleado->txEstadoCivilEmpleado = $txEstadoCivilEmpleado;
                    $terceroEmpleado->intPersonasCargoEmpleado = $intPersonasCargoEmpleado;
                    $terceroEmpleado->lsTipoViviendaEmpleado = $lsTipoViviendaEmpleado;
                    $terceroEmpleado->lsEscolaridadEmpleado = $lsEscolaridadEmpleado;
                    $terceroEmpleado->lsIngresosEmpleado = $lsIngresosEmpleado;
                    $terceroEmpleado->txEdadEmpleado = $txEdadEmpleado;
                    $terceroEmpleado->txTiempoLibreEmpleado = $txTiempoLibreEmpleado;
                    $terceroEmpleado->txDeporteEmpleado = $txDeporteEmpleado;
                    $terceroEmpleado->txSustanciasEmpleado = $txSustanciasEmpleado;
                    $terceroEmpleado->txEnfermedadesEmpleado = $txEnfermedadesEmpleado;
                    $terceroEmpleado->txBarrioViviendaEmpleado = $txBarrioViviendaEmpleado;
                    $terceroEmpleado->txFondoPensionEmpleado = $txFondoPensionEmpleado;


                    $terceroEmpleado->save();

                    $idEmpleadoTercero = TerceroEmpleadoModel::where('txDocumentoEmpleado', $txDocumentoEmpleado)->pluck('oidTerceroEmpleado');

                    // grupo familiar
                    $familiarEmpleado->Empleado_oidFamiliarEmpleado = $idEmpleadoTercero[0];
                    $familiarEmpleado->txNombreFamiliar = $txNombreFamiliar;
                    $familiarEmpleado->lsParentescoFamiliar = $lsParentescoFamiliar;
                    $familiarEmpleado->txCelularFamiliar = $txCelularFamiliar;
                    $familiarEmpleado->save();
                }
            }

            DB::commit();
            return response(["message" => 'Empleados cargados'], Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollBack();
            return response(["message" => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function exportarEmpleados($id)
    {
        return Excel::download(new TerceroEmpleadosExport($id), 'Kunde-TerceroEmpleados.xlsx');
    }

    public function buscarTerceroEmpleado($txDocumentoEmpleado)
    {
        $empleado = TerceroEmpleadoModel::where('txDocumentoEmpleado', $txDocumentoEmpleado)->first();

        if ($empleado) {
            throw new Exception("El Empleado con número de cédula $txDocumentoEmpleado ya se encuentra registrado o esta repetido");
        }
    }

    public function descargarPlantillaEmpleado()
    {
        $fileName = basename('PlantillaTerceroEmpleado.xlsx');
        $filePath =  public_path() . '/docs/plantillas/' . $fileName;
        if (!empty($fileName) && file_exists($filePath)) {
            // Define headers
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$fileName");
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: binary");

            // Read the file
            readfile($filePath);
            exit;
        } else {
            echo 'The file does not exist.';
        }
    }

    public function eliminarArchivosPublic($arrayDirectorios, $tipoArchivo = "*")
    {

        if (is_array($arrayDirectorios) || count($arrayDirectorios) != 0) {
            foreach ($arrayDirectorios as $key => $path) {
                //obtenemos todos los nombres de los ficheros
                $files = glob(public_path() . $path . $tipoArchivo);
                // si no hay ficheros, termina
                if (count($files) == 0) {
                    return;
                }
                if (is_file($files[$key])) {
                    //elimino el fichero
                    unlink($files[$key]);
                }
            }
        } else {
            return abort(500, 'El directorio a eliminar no es un array');
        }
    }
}
