<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\General\Entities\ClasificacionTerceroGrupoModel;
use Modules\General\Entities\ClasificacionTerceroModel;
use Modules\AsociadoNegocio\Entities\GrupoTerceroModel;
use DB;


use App\Http\Controllers\Controller;


class GrupoTerceroController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        
        $clasificacionTercero = ClasificacionTerceroModel::select('txNombreClasificacionTerceroGrupo','oidClasificacionTercero','txNombreClasificacionTercero','oidClasificacionTerceroGrupo')
        ->leftjoin('gen_clasificaciontercerogrupo','oidClasificacionTercero','=','ClasificacionTercero_oidClasificacionTercero_1aM')
        ->groupby('oidClasificacionTerceroGrupo','oidClasificacionTercero','txNombreClasificacionTercero','txNombreClasificacionTerceroGrupo','chObligatorioClasificacionTerceroGrupo','chCorreoClasificacionTerceroGrupo')
        ->get();


        return view('asociadonegocio::GrupoTerceroGrid', compact('clasificacionTercero','permisos'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('asociadonegocio::GrupoTerceroForm');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asociadonegocio::GrupoTerceroForm');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $grupoTercero = GrupoTerceroModel::where('ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo','=', $id)->get();

        $clasificacionTercero = ClasificacionTerceroModel::select('txNombreClasificacionTercero','txNombreClasificacionTerceroGrupo','oidClasificacionTercero','oidClasificacionTerceroGrupo','chObligatorioClasificacionTerceroGrupo','chCorreoClasificacionTerceroGrupo')
        ->leftjoin('gen_clasificaciontercerogrupo','oidClasificacionTercero','=','ClasificacionTercero_oidClasificacionTercero_1aM')
        ->where('oidClasificacionTerceroGrupo','=',$id)
        ->first();

        if(isset($clasificacionTercero->chCorreoClasificacionTerceroGrupo) && $clasificacionTercero->chCorreoClasificacionTerceroGrupo == 1){
            $correo = '"'.'text'.'"';
            $barraCorreo = 'si';
        }else{
            $correo = '"'.'hidden'.'"';
            $barraCorreo = 'no';
        }


        return view('asociadonegocio::GrupoTerceroForm', compact('clasificacionTercero','grupoTercero','id','obligatorio','correo','barraCorreo'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $this->grabarDetalleGrupoTercero($request, $id, 'eliminarGrupoTercero');
            DB::commit();
            return response(200);
        }catch(\Exception $e){
            DB::rollback();
            return abort(500, $e->getMessage());
        }
        
    }

    protected function grabarDetalleGrupoTercero($request, $idPadre, $datosEliminar)
    {
        $modelo = new GrupoTerceroModel();
        $modelo::whereIn($modelo->getKeyName(),explode(',', $request[$datosEliminar]))->delete();
        $datosConteo = isset($request[$modelo->getKeyName()]) ? count($request[$modelo->getKeyName()]) : 0;
    
        for($i = 0; $i <  $datosConteo; $i++)
        {
            $indice = array($modelo->getKeyName() => $request[$modelo->getKeyName()][$i]);
            $datos = ['ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo'=> $idPadre,
                    'txCodigoGrupoTercero'=>$request["txCodigoGrupoTercero"][$i],
                    'txNombreGrupoTercero'=>$request["txNombreGrupoTercero"][$i],
                    'txCorreoGrupoTercero'=>$request["txCorreoGrupoTercero"][$i],
                    'txMensajeGrupoTercero'=>$request["txMensajeGrupoTercero"][$i],
                    'chObservacionGrupoTercero'=>$request["chObservacionGrupoTercero"][$i],
                ];
            $guardar = $modelo::updateOrCreate($indice, $datos);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
