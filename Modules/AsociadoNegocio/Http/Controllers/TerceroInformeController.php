<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\General\Entities\ClasificacionTerceroGrupoModel;
use Modules\General\Entities\ClasificacionTerceroModel;
use Modules\Seguridad\Entities\CompaniaModel;

class TerceroInformeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $accion = $request->get('accion');
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');
        $clasificacion = ClasificacionTerceroModel::pluck('txNombreClasificacionTercero','oidClasificacionTercero');
        $clasificacionTercero = ClasificacionTerceroGrupoModel::selectRaw('oidClasificacionTerceroGrupo,txNombreClasificacionTercero,txNombreClasificacionTerceroGrupo,oidClasificacionTercero')
        ->leftjoin('gen_clasificaciontercero','ClasificacionTercero_oidClasificacionTercero_1aM','=','oidClasificacionTercero')
        ->orderby('txNombreClasificacionTercero')
        ->get();
        return view('asociadonegocio::TerceroParametro', compact('accion','compania','clasificacion','clasificacionTercero'));
    }

    public function generarCondicion(Request $request,$parametro){
        //Condicion para los grupos de tercero dinamicos
        $condicion = '';
        foreach($request['GrupoTercero_oidTerceroGrupo'] as $clasificacion => $grupo)
        {
            $temp = '(ClasificacionTercero_oidTerceroClasificacion_1aM = '.$clasificacion.' AND ';
            $sw = false;
            foreach($grupo as $grupos => $valor)
            {
                if(!empty($valor))
                {
                    $temp .= '(ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = '. $grupos.' AND oidGrupoTercero = '.$valor.') OR ';
                    $sw = true;
                }
            }
            $temp = substr($temp,0, -3);
            $temp .= ') OR ';
            if($sw)
                $condicion .= $temp;
        }
        $condicion = substr($condicion,0, -3);
        
        //Condicion para los datos de encabezado
        $fechaInicial = $request['fechaInicialCreacion'];
        $fechaFinal = $request['fechaFinalCreacion'];
        $clasificacionTercero = (isset($request['clasificacionTerceroTipo']) ? implode(',',$request['clasificacionTerceroTipo']): '');
        $clasificacion = (isset($request['clasificacionTercero']) ? implode("%' OR lmClasificacionTercero LIKE '%",$request['clasificacionTercero']) : '');
        $tipoServicio = (isset($request['tipoServicio']) ? implode("%' OR lmTipoServicioTercero LIKE '%",$request['tipoServicio']) : '');
        $compañia = (isset($request['CompaniaTercero']) ? implode("%' OR TCN.Compania_oidCompania LIKE '%",$request['CompaniaTercero']) : '');
        $estadoSG = $request['txEstadoSistemaGestionTercero'];
        $estado = $request['EstadoTercero'];

        $condicion .= ($condicion == false && $clasificacionTercero != '' ? "TC.ClasificacionTercero_oidTerceroClasificacion_1aM IN ($clasificacionTercero)" : "");
        $condicion .= ($fechaInicial != '' ? " AND daFechaCreacionTercero > '$fechaInicial' " : "");
        $condicion .= ($fechaFinal != '' ? " AND daFechaCreacionTercero < '$fechaFinal'" : "");
        $condicion .= ($clasificacion != '' ? " AND (lmClasificacionTercero like '%$clasificacion%')" : "");
        $condicion .= ($tipoServicio != '' ? " AND (lmTipoServicioTercero like '%$tipoServicio%')" : "");
        $condicion .= ($compañia != '' ? " AND (TCN.Compania_oidCompania = '$compañia')" : "");
        $condicion .= ($estadoSG != '' ? " AND txEstadoSistemaGestionTercero = '$estadoSG'" : "");
        $condicion .= ($estado != '' ? " AND txEstadoTercero = '$estado'" : "");

        if($parametro == 'informe'){
            $consulta = $this->generarInforme($condicion);
            $datos = array();
            for($i=0; $i < count($consulta[0]); $i++)
            {
                $datos[$i] = get_object_vars($consulta[0][$i]);
            }
            $preguntas= $consulta[1];
            $certificados = $consulta[2];
            $nombreGrupoEncab = $consulta[3];
            $nombreGrupo = $consulta[4];
            
            return view('asociadonegocio::TerceroInforme', compact('datos','preguntas','certificados','nombreGrupoEncab','nombreGrupo'));
        }elseif($parametro == 'calificacion'){
            $this->generarCalificacion($condicion);
        }
    }

    public function generarInforme($condicion)
    {
        //Consulta de maestro de preguntas OEA
        $preguntas = DB::select("SELECT 
        oidPreguntaOea,
        txNombrePreguntaOea,
        Compania_oidCompania
        FROM
        gen_preguntaoea
        order by Compania_oidCompania");
        $camposOEA = '';
        for ($i=0; $i < count($preguntas); $i++) { 
            $camposOEA .= " MAX(IF(".$preguntas[$i]->oidPreguntaOea." = Preguntas_oidTerceroVerificacion,lsImpactoTerceroVerificacion AND ".$preguntas[$i]->Compania_oidCompania." = TCN.Compania_oidCompania,'')) as `".$preguntas[$i]->txNombrePreguntaOea."`,";
        }
        $campos = substr($camposOEA, 0,-1);

        //Consulta de los certificados de tercero
        $certificadoTercero = DB::select("SELECT 
        oidTipoCertificado,
        txNombreTipoCertificado,
        Compania_oidCompania
        FROM
        kunde.gen_tipocertificado
        order by Compania_oidCompania");
        $camposCerti = '';
        for ($i=0; $i < count($certificadoTercero); $i++) { 
            $camposCerti .= " MAX(IF(".$certificadoTercero[$i]->oidTipoCertificado." = TipoCertificado_oidTerceroCertificado AND ".$certificadoTercero[$i]->Compania_oidCompania." = TCN.Compania_oidCompania,lsVerificacionTerceroCertificado,'')) as `".$certificadoTercero[$i]->txNombreTipoCertificado."`,";
        }
        $camposCertificado = substr($camposCerti, 0,-1);


        $nombresGrupos = DB::select("SELECT 
            txNombreClasificacionTerceroGrupo,
            CONCAT(txNombreClasificacionTerceroGrupo,'_',ClasificacionTercero_oidClasificacionTercero_1aM) as nombreClasificacion,
            ClasificacionTercero_oidClasificacionTercero_1aM
        FROM
            gen_clasificaciontercerogrupo
        order BY ClasificacionTercero_oidClasificacionTercero_1aM");

        //Consulta de los maestros de grupo
        $TerceroGrupo = DB::select(
            "SELECT 
                oidClasificacionTerceroGrupo,
                -- oidGrupoTercero,
                CONCAT(txNombreClasificacionTerceroGrupo,'_',ClasificacionTercero_oidClasificacionTercero_1aM) as nombreClasificacion,
                txNombreClasificacionTerceroGrupo,
                -- txNombreGrupoTercero,
                ClasificacionTercero_oidClasificacionTercero_1aM,
                oidCompania
            FROM
                gen_clasificaciontercerogrupo,seg_compania
            order BY ClasificacionTercero_oidClasificacionTercero_1aM");

        $camposGrupo = '';
        for ($i=0; $i < count($TerceroGrupo); $i++) { 
            $camposGrupo .= " MAX(IF(".$TerceroGrupo[$i]->oidClasificacionTerceroGrupo." = oidClasificacionTerceroGrupo AND ".$TerceroGrupo[$i]->ClasificacionTercero_oidClasificacionTercero_1aM." = ClasificacionTercero_oidClasificacionTercero_1aM ,txNombreGrupoTercero,'')) as `".$TerceroGrupo[$i]->txNombreClasificacionTerceroGrupo."_".$TerceroGrupo[$i]->ClasificacionTercero_oidClasificacionTercero_1aM."`,";
        }
        $camposGrupos = substr($camposGrupo, 0,-1);

        //Consulta de terceros
        $consulta = DB::select("SELECT 
            TCN.Compania_oidCompania,
            txNombreCompania,
            txDocumentoTercero,
            txNombreTercero,
            txNombreComercialTercero,
            txDireccionTercero,
            txNombreCiudad,
            txTelefonoTercero,
            txTelefonoTercero,
            txMovilTercero,
            txCorreoElectronicoTercero,
            lmTipoServicioTercero,
            lmClasificacionTercero,
            daFechaCreacionTercero,
            txEstadoTercero,
            txImpactoTerceroVerificacionOea,
            dePonderacionTerceroVerificacionOea,
            ClasificacionTercero_oidTerceroClasificacion_1aM,
            txEstadoSistemaGestionTercero,
            $campos,
            $camposCertificado,
            $camposGrupos
            FROM
            asn_tercero
                LEFT JOIN
            asn_terceroclasificacion TC ON TC.Tercero_oidTercero_1aM = oidTercero
                LEFT JOIN
            gen_ciudad ON Ciudad_oidTercero = oidCiudad
                LEFT JOIN 
            asn_tercerocompania TCN on  TCN.Tercero_oidTercero_1aM = oidTercero
                LEFT JOIN 
            seg_compania COM on TCN.Compania_oidCompania = COM.oidCompania
                LEFT JOIN
            asn_terceroverificacion TV on TV.Tercero_oidTercero_1aM = oidTercero AND TV.Compania_oidCompania = TCN.Compania_oidCompania
                LEFT JOIN 
            gen_preguntaoea OEA on Preguntas_oidTerceroVerificacion = oidPreguntaOea AND OEA.Compania_oidCompania = TCN.Compania_oidCompania
                LEFT JOIN
            asn_terceroverificacionoea TVO ON TVO.Tercero_oidTercero_1a1 = oidTercero AND TVO.Compania_oidCompania = TCN.Compania_oidCompania
                LEFT JOIN
            asn_tercerocertificado TCA ON TCA.Tercero_oidTercero_1aM = oidTercero
                LEFT JOIN 
            asn_tercerogrupo TG on TG.Tercero_oidTercero_1aM = oidTercero 
                                AND TG.Compania_oidCompania = TCN.Compania_oidCompania
                LEFT JOIN 
            asn_grupotercero GT ON GrupoTercero_oidTerceroGrupo = oidGrupoTercero
                LEFT JOIN
            gen_clasificaciontercerogrupo CTG on GT.ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = CTG.oidClasificacionTerceroGrupo  
                                        AND TG.ClasificacionTercero_oidTerceroGrupo = CTG.ClasificacionTercero_oidClasificacionTercero_1aM
        WHERE 
            $condicion
            group by oidTercero,TCN.Compania_oidCompania");
            // dd($condicion);

        return [$consulta,$preguntas,$certificadoTercero,$TerceroGrupo,$nombresGrupos];
    }

    public function generarCalificacion($condicion){
        dd($condicion);
    }
}
