<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;

class EstadisticaTerceroController extends Controller
{

    public function index($id)
    {
        $terceroEmpleado = TerceroEmpleadoModel::where('Tercero_oidTercero', $id)->get();

        /* Edades */
        $edades = DB::table('asn_terceroempleado')
            ->select('txEdadEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('txEdadEmpleado');

        $rangoEdadesContent = ['18 - 25 años' => 0, '26- 35 años' => 0, '36- 45 años' => 0, '45- 55 años' => 0, '56 en adelante' => 0];

        foreach ($edades as $key => $value) {
            $edadesNormalizado = $this->normalizarTexto($value);

            switch ($edadesNormalizado) {
                case '18___25_a_nos':
                    $rangoEdadesContent['18 - 25 años'] += 1;
                    break;
                case '26__35_a_nos':
                    $rangoEdadesContent['26- 35 años'] += 1;
                    break;
                case '36__45_a_nos':
                    $rangoEdadesContent['36- 45 años'] += 1;
                    break;
                case '45__55_a_nos':
                    $rangoEdadesContent['45- 55 años'] += 1;
                    break;
                case '56_en_adelante':
                    $rangoEdadesContent['56 en adelante'] += 1;
                    break;
                default:

                    break;
            }
        }

        /* Sexo */
        $sexo = DB::table('asn_terceroempleado')
            ->select('txGeneroEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('txGeneroEmpleado');

        // posición 0 = Hombre 1 = mujer
        $sexoContent = ['Hombre' => 0, 'Mujer' => 0];

        foreach ($sexo as $key => $value) {
            $comparacion = strcmp("hombre", $this->normalizarTexto($value));
            if ($comparacion !== 0) {
                $sexoContent['Hombre'] += 1;
                continue;
            }

            $comparacion = strcmp("mujer", $this->normalizarTexto($value));
            if ($comparacion !== 0) {
                $sexoContent['Mujer'] += 1;
                continue;
            }
        }

        /* Estado civil */
        $estadoCivil = DB::table('asn_terceroempleado')
            ->select('txEstadoCivilEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('txEstadoCivilEmpleado');
        $estadoCivilContent = ['Soltero/a' => 0, 'Casado/a' => 0, 'Unión libre' => 0, 'separado/a' => 0, 'Divorciado/a' => 0, 'Viudo/a' => 0];

        foreach ($estadoCivil as $key => $value) {
            // $valorNormalizado = $this->normalizarTexto($value);

            switch ($value) {
                case 'Soltero/a':
                    $estadoCivilContent['Soltero/a'] += 1;
                    break;
                case 'Casado/a':
                    $estadoCivilContent['Casado/a'] += 1;
                    break;
                case 'Unión libre':
                    $estadoCivilContent['Unión libre'] += 1;
                    break;
                case 'separado/a':
                    $estadoCivilContent['separado/a'] += 1;
                    break;
                case 'Divorciado/a':
                    $estadoCivilContent['Divorciado/a'] += 1;
                    break;
                case 'Viudo/a':
                    $estadoCivilContent['Viudo/a'] += 1;
                    break;

                default:

                    break;
            }
        }

        /* Estrato*/
        $estrato = DB::table('asn_terceroempleado')
            ->select('lsEstratoEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('lsEstratoEmpleado');

        $estratoContent = ['1' => 0, '2' => 0, '3' => 0, '4' => 0, 'otro' => 0];

        foreach ($estrato as $key => $value) {

            switch ($value) {
                case 1:
                    $estratoContent['1'] += 1;
                    break;
                case 2:
                    $estratoContent['2'] += 1;
                    break;
                case 3:
                    $estratoContent['3'] += 1;
                    break;
                case 4:
                    $estratoContent['4'] += 1;
                    break;
                default:
                    $estratoContent['otro'] += 1;
                    break;
            }
        }

        /* Personas a cargo */
        $numeropersonascargo = DB::table('asn_terceroempleado')
            ->select('intPersonasCargoEmpleado')
            ->orderBy('intPersonasCargoEmpleado', 'DEC')
            // ->take(10)
            ->where('Tercero_oidTercero', $id)
            ->pluck('intPersonasCargoEmpleado');


        $numeropersonascargoContent = ['0' => 0, '1' => 0, '2' => 0, '3' => 0, '4' => 0, 'mas_de_5' => 0];

        foreach ($numeropersonascargo as $key => $value) {

            switch ($value) {
                case '0':
                    $numeropersonascargoContent['0'] += 1;
                    break;
                case '1':
                    $numeropersonascargoContent['1'] += 1;
                    break;
                case '2':
                    $numeropersonascargoContent['2'] += 1;
                    break;
                case '3':
                    $numeropersonascargoContent['3'] += 1;
                    break;
                case '4':
                    $numeropersonascargoContent['4'] += 1;
                    break;
                default:
                    $numeropersonascargoContent['mas_de_5'] += 1;
                    break;
            }
        }

        /* ETipo de vivienda */
        $vivienda = DB::table('asn_terceroempleado')
            ->select('lsTipoViviendaEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('lsTipoViviendaEmpleado');


        $tipoViviendaContent = ['Propia' => 0, 'Arrendada' => 0, 'Familiar' => 0, 'Otra' => 0];

        foreach ($vivienda as $key => $value) {
            $viviendaNormalizado = $this->normalizarTexto($value);

            switch ($viviendaNormalizado) {
                case 'propia':
                    $tipoViviendaContent['Propia'] += 1;
                    break;
                case 'arrendada':
                    $tipoViviendaContent['Arrendada'] += 1;
                    break;
                case 'familiar':
                    $tipoViviendaContent['Familiar'] += 1;
                    break;
                default:
                    $tipoViviendaContent['Otra'] += 1;
                    break;
            }
        }

        /* Escolaridad */
        $escolaridad = DB::table('asn_terceroempleado')
            ->select('lsEscolaridadEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('lsEscolaridadEmpleado');

        $escolaridadContent = ['Primaria' => 0, 'Secundaria' => 0, 'Técnico/Tecnólogo' => 0, 'Profesional' => 0, 'Posgrado' => 0, 'Otra' => 0,];

        foreach ($escolaridad as $key => $value) {
            $escolaridadNormalizado = $this->normalizarTexto($value);

            switch ($escolaridadNormalizado) {
                case 'primaria':
                    $escolaridadContent['Primaria'] += 1;
                    break;
                case 'secundaria':
                    $escolaridadContent['Secundaria'] += 1;
                    break;
                case 't_ecnico_tecn_ologo':
                    $escolaridadContent['Técnico/Tecnólogo'] += 1;
                    break;
                case 'profesional':
                    $escolaridadContent['Profesional'] += 1;
                    break;
                case 'posgrado':
                    $escolaridadContent['Posgrado'] += 1;
                    break;
                default:
                    $escolaridadContent['Otra'] += 1;
                    break;
            }
        }

        /* Salario */
        $salario = DB::table('asn_terceroempleado')
            ->select('lsIngresosEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('lsIngresosEmpleado');

        $salarioContent = ['Menos de 1 SMLV' => 0, '1 SMLV' => 0, '2 - 3 SMLV' => 0, '4 - 5 SMLV' => 0, '6 - 7 SMLV' => 0, 'Más de 7 SMLV' => 0];

        foreach ($salario as $key => $value) {
            $salarioNormalizado = $this->normalizarTexto($value);

            switch ($salarioNormalizado) {
                case 'menos_de_1_smlv':
                    $salarioContent['Menos de 1 SMLV'] += 1;
                    break;
                case '1_smlv':
                    $salarioContent['1 SMLV'] += 1;
                    break;
                case '2___3_smlv':
                    $salarioContent['2 - 3 SMLV'] += 1;
                    break;
                case '4___5_smlv':
                    $salarioContent['4 - 5 SMLV'] += 1;
                    break;
                case '6___7_smlv':
                    $salarioContent['6 - 7 SMLV'] += 1;
                    break;
                case 'mass_de_7_smlv':
                    $salarioContent['Más de 7 SMLV'] += 1;
                    break;
                default:
                    break;
            }
        }

        /* Enfermedades */
        $tieneEnfermedad = DB::table('asn_terceroempleado')
            ->select('chEnfermedadesEmpleado')
            ->where('Tercero_oidTercero', $id)
            ->pluck('chEnfermedadesEmpleado');


        $tieneEnfermedadContent = ['0' => 0, '1' => 0, 'sin_respuesta' => 0];

        foreach ($tieneEnfermedad as $key => $value) {
            switch ($value) {
                case 0:
                    $tieneEnfermedadContent['0'] += 1;
                    break;
                case 1:
                    $tieneEnfermedadContent['1'] += 1;
                    break;

                default:
                    $tieneEnfermedadContent['sin_respuesta'] += 1;
                    break;
            }
        }



        return view('asociadonegocio::EstadisticaTerceroForm', compact('rangoEdadesContent', 'sexoContent', 'estadoCivilContent', 'estratoContent', 'numeropersonascargoContent', 'tipoViviendaContent', 'escolaridadContent', 'terceroEmpleado', 'tieneEnfermedadContent', 'salarioContent'));
    }

    public function normalizarTexto($input)
    {
        $input = iconv('UTF-8', 'ASCII//TRANSLIT', $input);
        $input = preg_replace('/[^a-zA-Z0-9]/', '_', $input);
        $input = strtolower($input);
        return $input;
    }
}
