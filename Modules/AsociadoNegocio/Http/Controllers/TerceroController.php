<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use App\AuthService;
use Session;
use Mail;
use Modules\Saya\Http\Controllers\SayaTerceroController;
use App\Http\Requests;
use App\Imports\TareaImport;
use App\Seg_CompaniaModel;
use App\Seg_RolModel;
use Illuminate\Http\Request;
use Modules\General\Entities\CiudadModel;
use Modules\General\Entities\GrupoNominaModel;
use Modules\General\Entities\CargoModel;
use Modules\General\Entities\ClasificacionTerceroModel;
use Modules\General\Entities\FormaPagoModel;
use Modules\General\Entities\TipoCertificadoModel;
use Modules\General\Entities\ClasificacionRentaModel;
use Modules\General\Entities\NaturalezaJuridicaModel;
use Modules\General\Entities\ActividadEconomicaModel;
use Modules\General\Entities\ClasificacionProductoGrupoModel;
use Modules\General\Entities\TipoIdentificacionModel;
use Modules\General\Entities\ClasificacionTerceroGrupoModel;
use Modules\General\Entities\ClasificacionTerceroConfigModel;
use Modules\General\Entities\CentroTrabajoModel;
use Modules\AsociadoNegocio\Entities\TerceroReferenciaModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\AsociadoNegocio\Entities\TerceroComercialModel;
use Modules\AsociadoNegocio\Entities\TerceroPersonalModel;
use Modules\AsociadoNegocio\Entities\TerceroTributarioModel;
use Modules\AsociadoNegocio\Entities\TerceroContactoModel;
use Modules\AsociadoNegocio\Entities\TerceroLaboralModel;
use Modules\AsociadoNegocio\Entities\TerceroSucursalModel;
use Modules\AsociadoNegocio\Entities\TerceroImpuestoModel;
use Modules\AsociadoNegocio\Entities\TerceroBancoModel;
use Modules\AsociadoNegocio\Entities\TerceroGrupoModel;
use Modules\AsociadoNegocio\Entities\GrupoProductoModel;
use Modules\AsociadoNegocio\Entities\TerceroLogisticaModel;
use Modules\AsociadoNegocio\Entities\TerceroVerificacionModel;
use Modules\AsociadoNegocio\Entities\TerceroVerificacionOeaModel;
use Modules\AsociadoNegocio\Entities\TerceroAdjuntoModel;
use Modules\AsociadoNegocio\Entities\TerceroCentroCostoModel;
use Modules\AsociadoNegocio\Http\Requests\TerceroContactoRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroPersonalRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroLaboralRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroReferenciaRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroSucursalRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroRequest;
use Modules\AsociadoNegocio\Entities\GrupoTerceroModel;
use Modules\AsociadoNegocio\Entities\TerceroClasificacionModel;
use Modules\AsociadoNegocio\Entities\TerceroCentroTrabajoModel;
use Modules\General\Entities\PreguntaOeaModel;
use Modules\General\Entities\CentroCostoModel;
use Intervention\Image\ImageManager;
use function GuzzleHttp\json_decode;
use Input;
use DB;
use File;
use DataTables;
use Storage;
use Illuminate\Support\Facades\URL;
use Modules\AsociadoNegocio\Entities\TerceroCertificadoModel;

use Modules\AsociadoNegocio\Mail\CertificadoTerceroVencido;
use Modules\AsociadoNegocio\Mail\NotificacionCreacionTercero;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input as FacadesInput;
use Maatwebsite\Excel\Facades\Excel;
use Modules\AsociadoNegocio\Entities\TerceroActividadEconomicaModel;
use Modules\AsociadoNegocio\Entities\TerceroCargoModel;
use Modules\AsociadoNegocio\Entities\TerceroCompaniaModel;
use Modules\AsociadoNegocio\Entities\TerceroEventoPeriodicidadModel;
use Modules\AsociadoNegocio\Entities\TerceroFacturaElectronicaModel;
use Modules\AsociadoNegocio\Entities\TerceroIntegranteComiteModel;
use Modules\AsociadoNegocio\Entities\TerceroPeriodicidadModel;
use Modules\AsociadoNegocio\Entities\TerceroRazonSocialModel;
use Modules\AsociadoNegocio\Entities\TerceroResponsabilidadFiscalModel;
use Modules\AsociadoNegocio\Http\Requests\TerceroIntegranteComiteRequest;
use Modules\General\Entities\ArlModel;
use Modules\General\Entities\CanalDistribucionTerceroModel;
use Modules\General\Entities\ProcesoLogisticoTerceroModel;
use Modules\General\Entities\RegimenContableModel;
use Modules\General\Entities\ResponsabilidadFiscalModel;
use Modules\General\Entities\Servicio;
use Modules\General\Entities\TipoContactoTerceroModel;
use Modules\General\Entities\TipoEtiquetaTerceroModel;
use Modules\General\Entities\TipoProductoTerceroModel;
use Modules\General\Http\Controllers\CanalDistribucionTerceroController;
use Modules\General\Http\Controllers\ProcesoLogisticoTerceroController;
use Modules\General\Http\Controllers\TipoEtiquetaTerceroController;
use Modules\General\Http\Controllers\TipoProductoTerceroController;
use Modules\Reunion\Entities\CronogramaEspacioModel;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionModel;
use Modules\Seguridad\Entities\CompaniaModel;

class terceroController extends SayaTerceroController
{
    public function index(Request $request)
    {
        if (isset($_GET['ct'])) {
            $ct = $_GET['ct'];
        } else {
            $ct = 1;
        }

        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $clasificacion = ClasificacionTerceroModel::select('txNombreClasificacionTercero')
            ->where('oidClasificacionTercero', '=', $ct)
            ->get();

        $Compania = Seg_CompaniaModel::pluck('txNombreCompania', 'oidCompania');

        $tipoServicios = Servicio::all();


        return view('asociadonegocio::TerceroGrid', compact('permisos', 'ct', 'clasificacion', 'tipoServicios'));
    }

    public function consultaAjax(Request $request)
    {

        $permisos = $this->consultarPermisosUrl("/asociadonegocio/tercerogrid");
        if (!$permisos) {
            abort(401);
        }
        $ct = $request->get('ct');

        $terceros = TerceroModel::gridQuery($ct);
        return DataTables::of($terceros)
            ->addColumn('edit', function ($tercero) use ($permisos, $ct) {
                $editUrl = "/asociadonegocio/terceromenu/$tercero->oidTercero/$ct";
                $edit = $permisos['chModificarRolOpcion'] ? "<a class=\"btn btn-sm btn-success\" href=\"{$editUrl}\" title=\"Modificar\"><i class=\"fas fa-pencil-alt\"></i></a>" : "";
                $power = $permisos['chModificarRolOpcion'] ? "<button title='Estados' class=\"btn btn-warning btn-sm\" onclick=\"cambiarEstado('$tercero->oidTercero','asn_tercero','Tercero','$tercero->txEstadoTercero')\"><i class=\"fas fa-power-off\"></i></button >" : "";
                $buttonDelete = $permisos['chEliminarRolOpcion'] ? '<button type="button" class="btn btn-danger btn-sm" onclick="confirmarEliminacion(\'' . $tercero->oidTercero . '\', \'asn_tercero\', \'Tercero\')" title = "Eliminar" >
                                    <i class="fas fa-trash-alt"></i>
                                </button>' : '';
                //$buttonPrint = $permisos['chConsultarRolOpcion'] ? '<a class="btn btn-info btn-sm" href="'.URL::to('/asociadonegocio/terceroimpresion' , [$tercero->oidTercero]).'?ct='.$ct.'">
                //<i class="fas fa-print"></i>
                //</a>' : '';
                $buttonPrint = '';
                $buttonStadoSG = $permisos['chInactivarRolOpcion'] ? '<button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Estado sistema de gestión" onclick="cambiarEstadoSG(\'' . $tercero->oidTercero . '\')"><i class="fas fa-toggle-on"></i></button>' : '';

                return "<div class=\"btn-group\" role=\"group\">{$edit}{$power}{$buttonDelete}{$buttonPrint}{$buttonStadoSG}</div>";
            })->escapeColumns('')->make(true);
    }

    public function create(Request $request)
    {
        $ct = $request->get('ct');
        $asn_tercero = new TerceroModel();
        $asn_tercero->txEstadoTercero = 'Activo';
        $asn_tercero->daFechaCreacionTercero = Carbon::now();
        $clasificacion = ClasificacionTerceroModel::select('txNombreClasificacionTercero')
            ->where('oidClasificacionTercero', '=', $ct)
            ->get();
        $clasificacionTerceroEdit = '';
        $companiaEdit = '';

        $tipoServicios = Servicio::all()->pluck('txNombre', 'id');

        $clasificacionTercero = ClasificacionTerceroModel::pluck('txNombreClasificacionTercero', 'oidClasificacionTercero');

        $gen_tipoidentificacionlista = TipoIdentificacionModel::Where('gen_tipoidentificacion.txEstadoTipoIdentificacion', '=', 'Activo')->pluck('txNombreTipoIdentificacion', 'oidTipoIdentificacion');

        $tercerociudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");
        $Compania = Seg_CompaniaModel::pluck('txNombreCompania', 'oidCompania');

        $actividadEconomica = ActividadEconomicaModel::select(DB::raw("CONCAT_WS(' - ', txCodigoActividadEconomica, txNombreActividadEconomica) as txNombreActividadEconomica"), 'oidActividadEconomica')
            ->pluck("txNombreActividadEconomica", "oidActividadEconomica");

        $arl = ArlModel::pluck('txNombreArl', 'oidArl');


        return view('asociadonegocio::TerceroForm', compact('gen_tipoidentificacionlista', 'tercerociudad', 'asn_tercero', 'ct', 'clasificacion', 'Compania', 'clasificacionTercero', 'clasificacionTerceroEdit', 'companiaEdit', 'tipoServicios', 'actividadEconomica', 'arl'));
    }

    public function store(TerceroRequest $request)
    {
        DB::beginTransaction();
        try {
            $asn_tercero = TerceroModel::create([
                'TipoIdentificacion_oidTercero' => $request['TipoIdentificacion_oidTercero'],
                'txDocumentoTercero' => $request['txDocumentoTercero'],
                'txDigitoVerificacionTercero' => ($request['txDigitoVerificacionTercero'] == null ? '' : $request['txDigitoVerificacionTercero']),
                'Ciudad_oidTercero' => $request['Ciudad_oidTercero'],
                'txCodigoTercero' => $request['txCodigoTercero'],
                'txCodigoBarrasTercero' => ($request['txCodigoBarrasTercero'] == '' ? null : $request['txCodigoBarrasTercero']),
                'txPrimerNombreTercero' => ($request['txPrimerNombreTercero'] == null ? '' : $request['txPrimerNombreTercero']),
                'txSegundoNombreTercero' => ($request['txSegundoNombreTercero'] == '' ? null : $request['txSegundoNombreTercero']),
                'txPrimerApellidoTercero' => ($request['txPrimerApellidoTercero'] == null ? '' : $request['txPrimerApellidoTercero']),
                'txSegundoApellidoTercero' => ($request['txSegundoApellidoTercero'] == '' ? null : $request['txSegundoApellidoTercero']),
                'txNombreTercero' => $request['txNombreTercero'],
                'txNombreComercialTercero' => ($request['txNombreComercialTercero'] == '' ? null : $request['txNombreComercialTercero']),
                'txDireccionTercero' => $request['txDireccionTercero'],
                'txTelefonoTercero' => ($request['txTelefonoTercero'] == '' ? null : $request['txTelefonoTercero']),
                'txMovilTercero' => $request['txMovilTercero'],
                'txCorreoElectronicoTercero' => ($request['txCorreoElectronicoTercero'] == '' ? null : $request['txCorreoElectronicoTercero']),
                'txGrupoSizfraTercero' => $request['txGrupoSizfraTercero'],
                'txEstadoTercero' => $request['txEstadoTercero'],
                'txResponsableTercero' => $request['txResponsableTercero'],
                'txObservacionTercero' => $request['txObservacionTercero'],
                'txPrefijoProductoTercero' => $request['txPrefijoProductoTercero'],
                'daFechaCreacionTercero' => $request['daFechaCreacionTercero'],
                'lmTipoServicioTercero' => implode(',', $request->get("lmTipoServicioTercero", [])),
                'lmClasificacionTercero' => implode(',', $request["lmClasificacionTercero"]),
                'Usuario_oidCreador' => Auth::id(),
                'txUrl' => $request["txUrl"],
                'txNombreResponsableSSTTercero' => $request["txNombreResponsableSSTTercero"],
                'inDocumentoResponsableSSTTercero' => $request["inDocumentoResponsableSSTTercero"],
                'txNombreRepresentanteLegalTercero' => $request["txNombreRepresentanteLegalTercero"],
                'inDocumentoRepresentanteLegalTercero' => $request["inDocumentoRepresentanteLegalTercero"],
                'txNombreIntegranteComiteCopassTercero' => $request["txNombreIntegranteComiteCopassTercero"],
                'inDocumentoIntegranteComiteCopasstTercero' => $request["inDocumentoIntegranteComiteCopasstTercero"],
                'txNombreIntegranteComiteConvivenciaTercero' => $request["txNombreIntegranteComiteConvivenciaTercero"],
                'inDocumentoIntegranteComiteConvivenciaTerce' => $request["inDocumentoIntegranteComiteConvivenciaTerce"],
                'txNombreIntegranteBrigadaTercero' => $request["txNombreIntegranteBrigadaTercero"],
                'inDocumentoIntegranteBrigadaTercero' => $request["inDocumentoIntegranteBrigadaTercero"],
                'ActividadEconomica_oidActividadEconomica' => $request["ActividadEconomica_oidActividadEconomica"],
                'Arl_oidArl' => $request["Arl_oidArl"],
                'txMisionTercero' => $request["txMisionTercero"],
                'txVisionTercero' => $request["txVisionTercero"],
                'txValoresCorporativoTercero' => $request["txValoresCorporativoTercero"],
            ]);
            if ($request['imagenTercero']) {
                $origen = Storage::disk('tmp')->path($request['imagenTercero']);
                $extension = File::extension($origen);
                $nombre = uniqid() . "." . $extension;
                $destino = Storage::disk('discoc')->path('asociadonegocio/tercero/' . $nombre);
                // GUARDAMOS LA IMAGEN TAMBIEN EN SAYA
                // for ($i=0; $i < count($request["Compania_oidCompania"]); $i++) {
                //     $origenScalia = Storage::disk('tmp')->get($request['imagenTercero']);
                //     $destinoSaya = '/foto/'.$nombre;
                //     Storage::disk('sftp_'.$Compania)->put($destinoSaya, $origenScalia);
                // }
                File::move($origen, $destino);
                $asn_tercero->imImagenTercero = $nombre;
                $asn_tercero->save();
            }
            TerceroClasificacionModel::where('Tercero_oidTercero_1aM', '=', $asn_tercero->oidTercero)->delete();
            for ($i = 0; $i < count($request["oidClasificacionTercero"]); $i++) {
                $idClasificacion = $request["oidClasificacionTercero"][$i];
                $indice = ['oidTerceroClasificacion' => null];
                $datos = [
                    'Tercero_oidTercero_1aM' => $asn_tercero->oidTercero,
                    'ClasificacionTercero_oidTerceroClasificacion_1aM' => $request["oidClasificacionTercero"][$i]
                ];
                TerceroClasificacionModel::updateOrCreate($indice, $datos);
            }

            TerceroCompaniaModel::where('Tercero_oidTercero_1aM', '=', $asn_tercero->oidTercero)->delete();

            $indice = ['oidTerceroCompania' => null];
            TerceroCompaniaModel::updateOrCreate($indice, [
                'Tercero_oidTercero_1aM' => $asn_tercero->oidTercero,
                'Compania_oidCompania' => 2
            ]);

            // for ($i=0; $i < count($request["Compania_oidCompania"]); $i++) {
            //     $indice = ['oidTerceroCompania'=>null];
            //     $datos = [
            //         'Tercero_oidTercero_1aM'=>$asn_tercero->oidTercero,
            //         'Compania_oidCompania'=>$request["Compania_oidCompania"][$i]
            //     ];
            //     TerceroCompaniaModel::updateOrCreate($indice,$datos);
            // }

            DB::commit();
            $this->actualizarTercero($asn_tercero->oidTercero, '');
            return response(['ct' => $idClasificacion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        if (isset($_GET['ct'])) {
            $ct = $_GET['ct'];
        } else {
            $ct = 1;
        }
        $terceroPrint = TerceroModel::select(
            'oidTercero',
            'imImagenTercero',
            'txNombreTercero',
            'txDocumentoTercero',
            'txTelefonoTercero',
            'txCorreoElectronicoTercero',
            'txNombreCiudad',
            'txDireccionTercero',
            'txNombreFormaPago',
            'lsRegimenVentaTerceroTributario',
            'txNombreNaturalezaJuridica',
            'txNombreActividadEconomica',
            'txNombreClasificacionRenta',
            'inCierreContableTerceroLogistica',
            'lsCierreContableTerceroLogistica',
            'txTiempoPermanenciaTerceroLogistica',
            'lsManejoMuestrasTerceroLogistica',
            'lsMontajePedidoTerceroLogistica',
            'lmSitioEntregaTerceroLogistica',
            'txHorarioEntregaTerceroLogistica',
            'txNombreEntregaTerceroLogistica',
            'txDireccionEntregaTerceroLogistica',
            'txTelefonoEntregaTerceroLogistica',
            'lmRequisitoEntregaTerceroLogistica',
            'txManejoDevolucionesTerceroLogistica',
            'txObservacionTerceroLogistica',
            'txNombreGrupoTercero',
            'lmTipoServicioTercero',
            'lmClasificacionTercero',
            'inFrecuenciaPedidoTerceroLogistica',
            'lsFrecuenciaPedidoTerceroLogistica'
        )
            ->leftjoin('asn_tercerocomercial', 'asn_tercerocomercial.Tercero_oidTercero_1a1', '=', 'oidTercero')
            ->leftjoin('asn_tercerotributario', 'asn_tercerotributario.Tercero_oidTercero_1a1', '=', 'oidTercero')
            ->leftjoin('asn_tercerogrupo', 'asn_tercerogrupo.Tercero_oidTercero_1aM', '=', 'oidTercero')
            ->leftjoin('asn_grupotercero', 'asn_tercerogrupo.GrupoTercero_oidTerceroGrupo', '=', 'oidGrupoTercero')
            ->leftjoin('asn_tercerologistica', 'asn_tercerologistica.Tercero_oidTercero_1aM', '=', 'oidTercero')
            ->leftjoin('gen_ciudad', 'Ciudad_oidTercero', '=', 'oidCiudad')
            ->leftjoin('gen_formapago', 'FormaPago_oidCompra', '=', 'oidFormaPago')
            ->leftjoin('gen_naturalezajuridica', 'NaturalezaJuridica_oidTerceroTributario', '=', 'oidNaturalezaJuridica')
            ->leftjoin('gen_actividadeconomica', 'ActividadEconomica_oidTerceroTributario', '=', 'oidActividadEconomica')
            ->leftjoin('gen_clasificacionrenta', 'ClasificacionRenta_oidTerceroTributario', '=', 'oidClasificacionRenta')
            ->where('oidTercero', $id)
            ->first();

        $terceroGrupo = TerceroGrupoModel::select(
            'oidTerceroGrupo',
            'oidGrupoTercero',
            'oidClasificacionTerceroGrupo',
            'txNombreClasificacionTerceroGrupo',
            'txNombreGrupoTercero',
            'txObservacionTerceroGrupo'
        )
            ->leftjoin('asn_grupotercero', 'GrupoTercero_oidTerceroGrupo', '=', 'asn_grupotercero.oidGrupoTercero')
            ->leftjoin('gen_clasificaciontercerogrupo', 'ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo', '=', 'oidClasificacionTerceroGrupo')
            ->where('Tercero_oidTercero_1aM', $id)
            ->orderBy('txNombreClasificacionTerceroGrupo')
            ->get();

        $terceroCertificado = TerceroCertificadoModel::select('txNombreTipoCertificado', 'lsVerificacionTerceroCertificado')
            ->leftjoin('gen_tipocertificado', 'TipoCertificado_oidTerceroCertificado', '=', 'oidTipoCertificado')
            ->where('Tercero_oidTercero_1aM', $id)
            ->get();

        $terceroVerificacion = TerceroVerificacionModel::select('txNombrePreguntaOea', 'lsImpactoTerceroVerificacion', 'dePonderacionTerceroVerificacionOea', 'txImpactoTerceroVerificacionOea')
            ->leftjoin('asn_terceroverificacionoea', 'asn_terceroverificacionoea.Tercero_oidTercero_1a1', '=', 'asn_terceroverificacion.Tercero_oidTercero_1aM')
            ->leftjoin('gen_preguntaoea', 'Preguntas_oidTerceroVerificacion', '=', 'oidPreguntaOea')
            ->where('Tercero_oidTercero_1aM', $id)
            ->where('txTipoTereceroVerificacion', 'Confeccion')
            ->where('txTipoTereceroVerificacionOea', 'Confeccion')
            ->get();

        $terceroVerificacionComercializacion = TerceroVerificacionModel::select('txNombrePreguntaOea', 'lsImpactoTerceroVerificacion', 'dePonderacionTerceroVerificacionOea', 'txImpactoTerceroVerificacionOea')
            ->leftjoin('asn_terceroverificacionoea', 'asn_terceroverificacionoea.Tercero_oidTercero_1a1', '=', 'asn_terceroverificacion.Tercero_oidTercero_1aM')
            ->leftjoin('gen_preguntaoea', 'Preguntas_oidTerceroVerificacion', '=', 'oidPreguntaOea')
            ->where('Tercero_oidTercero_1aM', $id)
            ->where('txTipoTereceroVerificacion', 'Comercializacion')
            ->where('txTipoTereceroVerificacionOea', 'Comercializacion')
            ->get();

        $terceroContacto = TerceroContactoModel::select('txNombreTerceroContacto', 'txCorreoTerceroContacto', 'txMovilTerceroContacto', 'txTipoTerceroContacto')
            ->where('Tercero_oidTercero_1aM', $id)
            ->get();

        return view('asociadonegocio::TerceroPrint', compact('terceroPrint', 'terceroGrupo', 'terceroCertificado', 'terceroVerificacion', 'terceroContacto', 'ct', 'terceroVerificacionComercializacion'));
    }

    public function edit($id)
    {
        //consultamos los datos de la tabla.
        $consulta = TerceroModel::find($id);
        $clasificacionTercero = ClasificacionTerceroModel::pluck('txNombreClasificacionTercero', 'oidClasificacionTercero');
        $clasificacionTerceroEdit = TerceroClasificacionModel::where('Tercero_oidTercero_1aM', $id)->pluck('ClasificacionTercero_oidTerceroClasificacion_1aM');

        $companiaEdit = TerceroCompaniaModel::where('Tercero_oidTercero_1aM', $id)->pluck('Compania_oidCompania');

        //consultamos los datos de la listas de seleccion.
        $gen_tipoidentificacionlista = TipoIdentificacionModel::Where('gen_tipoidentificacion.txEstadoTipoIdentificacion', '=', 'Activo')->pluck('txNombreTipoIdentificacion', 'oidTipoIdentificacion');
        $tercerociudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");
        $Compania = Seg_CompaniaModel::pluck('txNombreCompania', 'oidCompania');
        $actividadEconomica = ActividadEconomicaModel::select(DB::raw("CONCAT_WS(' - ', txCodigoActividadEconomica, txNombreActividadEconomica) as txNombreActividadEconomica"), 'oidActividadEconomica')
            ->pluck("txNombreActividadEconomica", "oidActividadEconomica");
        $arl = ArlModel::pluck('txNombreArl', 'oidArl');

        $tipoServicios = Servicio::all();

        return view('asociadonegocio::TerceroForm', ['asn_tercero' => $consulta], compact('gen_tipoidentificacionlista', 'tercerociudad', 'Compania', 'clasificacionTercero', 'clasificacionTerceroEdit', 'companiaEdit', 'tipoServicios', 'actividadEconomica', 'arl'));
    }

    public function update(Request $request, $id)
    {
        $documentoAnterior = ($request["txDocumentoTerceroAnterior"] ? $request["txDocumentoTerceroAnterior"] : '');
        $idEliminar = $request['eliminarImagen'];
        DB::beginTransaction();
        try {

            $asn_tercero = TerceroModel::find($id);
            $asn_tercero->fill($request->all());
            $asn_tercero->lmTipoServicioTercero = implode(',', $request->get("lmTipoServicioTercero", []));
            $asn_tercero->lmClasificacionTercero = implode(',', $request["lmClasificacionTercero"]);
            $asn_tercero->txPrimerNombreTercero = ($request["txPrimerNombreTercero"] == null ? '' : $request["txPrimerNombreTercero"]);
            $asn_tercero->txPrimerApellidoTercero = ($request["txPrimerApellidoTercero"] == null ? '' : $request["txPrimerApellidoTercero"]);
            $asn_tercero->txDigitoVerificacionTercero = ($request["txDigitoVerificacionTercero"] == null ? '' : $request["txDigitoVerificacionTercero"]);
            $asn_tercero->Usuario_oidModificador = Auth::id();
            $asn_tercero->daFechaModificacionTercero = Carbon::now()->toDateString();

            if ($idEliminar) {
                $asn_tercero->imImagenTercero = "";
                $rutaImagenEliminar = TerceroModel::find($idEliminar)->imImagenTercero;
                $this->eliminarImagen($rutaImagenEliminar);
            }
            if ($request['imagenTercero']) {
                $Compania = Session::get('txBaseDatosCompania');
                $origen = Storage::disk('tmp')->path($request['imagenTercero']);
                $extension = File::extension($origen);
                $nombre = uniqid() . "." . $extension;
                $destino = Storage::disk('discoc')->path('asociadonegocio/tercero/' . $nombre);
                // GUARDAMOS LA IMAGEN TAMBIEN EN SAYA
                // for ($i=0; $i < count($request["Compania_oidCompania"]); $i++) {
                //     $origenScalia = Storage::disk('tmp')->get($request['imagenTercero']);
                //     $destinoSaya = '/foto/'.$nombre;
                //     Storage::disk('sftp_'.$Compania)->put($destinoSaya, $origenScalia);
                // }
                File::move($origen, $destino);
                $asn_tercero->imImagenTercero = $nombre;
            }
            $asn_tercero->save();
            //guardamos las clasificaciones de tercero
            TerceroClasificacionModel::where('Tercero_oidTercero_1aM', '=', $asn_tercero->oidTercero)->delete();
            for ($i = 0; $i < count($request["oidClasificacionTercero"]); $i++) {
                $idClasificacion = $request["oidClasificacionTercero"][$i];
                $indice = ['oidTerceroClasificacion' => null];
                $datos = [
                    'Tercero_oidTercero_1aM' => $asn_tercero->oidTercero,
                    'ClasificacionTercero_oidTerceroClasificacion_1aM' => $request["oidClasificacionTercero"][$i]
                ];
                TerceroClasificacionModel::updateOrCreate($indice, $datos);
            }

            // TerceroCompaniaModel::where('Tercero_oidTercero_1aM','=',$asn_tercero->oidTercero)->delete();
            // for ($i=0; $i < count($request["Compania_oidCompania"]); $i++) {
            //     $indice = ['oidTerceroCompania'=>null];
            //     $datos = [
            //         'Tercero_oidTercero_1aM'=>$asn_tercero->oidTercero,
            //         'Compania_oidCompania'=>2
            //     ];
            //     TerceroCompaniaModel::updateOrCreate($indice,$datos);
            // }

            $this->actualizarTercero($id, $documentoAnterior);
            DB::commit();
            return response(['id' => $id, 'ct' => $idClasificacion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $tercero = TerceroModel::find($id);
        $this->eliminarImagen($tercero);
        TerceroModel::destroy($id);
        return redirect('/asociadonegocio/tercero');
    }

    //Funcion para subir imagenes al disco temporal
    public function subirTercero(Request $request)
    {
        $imagen = $request->file('file');
        $path = Storage::disk('tmp')->putFile('asociadonegocio/tercero', $imagen);
        return response(['tmpPath' => $path], 200);
    }

    //Funcion para consultar imagenes mediante id
    public function consultaImagen($id)
    {
        $tercero = TerceroModel::find($id);
        $path = Storage::disk('discoc')->path("asociadonegocio/tercero/" . $tercero->imImagenTercero);
        if (file_exists($path)) {
            return response()->file($path);
        }
    }

    //Funcion para eliminar imagenes mediante id
    public function eliminarImagen($imagen)
    {
        $path = Storage::disk('discoc')->path("asociadonegocio/tercero/" . $imagen);
        if (file_exists($path)) {
            Storage::disk('discoc')->delete("asociadonegocio/tercero/" . $imagen);
        }
    }

    // Funcion para abrir el menu de los terceros
    public function terceroMenu($id, $idClasificacion)
    {

        $tercero = TerceroModel::find($id);
        $rol = Session::get(AuthService::$USER_ROLE);
        $clasificacion = ClasificacionTerceroModel::select('txNombreClasificacionTercero')
            ->where('oidClasificacionTercero', '=', $idClasificacion)
            ->get();
        $consulta = DB::select("SELECT
            Tercero_oidTercero_1aM as oidTercero,
            txRutaAccionModuloTercero,
            txNombreModuloTercero,
            txRutaImagenModuloTercero,
            oidClasificacionTercero
        FROM
            asn_terceroclasificacion TC
                LEFT JOIN
            gen_clasificaciontercero CT ON TC.ClasificacionTercero_oidTerceroClasificacion_1aM = oidClasificacionTercero
                LEFT JOIN
            gen_clasificacionterceroconfig CTC ON CTC.ClasificacionTercero_oidClasificacionTercero_1aM = oidClasificacionTercero
                LEFT JOIN
            gen_modulotercero ON ModuloTercero_ClasificacionTerceroConfig = oidModuloTercero
                left join
            gen_clasificaciontercerorol on ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM = oidClasificacionTerceroConfig

        WHERE
            oidClasificacionTercero = $idClasificacion
                AND Tercero_oidTercero_1aM = $id
                and Rol_oidClasificacionTerceroRol = $rol
                order by txNombreModuloTercero");

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 29)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        $terceroVerificacion = TerceroModel::select('lmTipoServicioTercero', 'Tercero_oidTercero_1aM', 'lmClasificacionTercero')
            ->leftjoin('asn_terceroverificacion', 'Tercero_oidTercero_1aM', '=', 'oidTercero')
            ->where('oidTercero', $id)
            ->get();

        $tipoServicios = Servicio::all();

        return view('asociadonegocio::TerceroMenu', compact('consulta', 'id', 'clasificacion', 'terceroVerificacion', 'tercero', 'idClasificacion', 'permisoModificar', 'tipoServicios'));
    }

    //FUNCIONES DEL CONTROLADOR DE CONTACTO---------------------------------------------------
    public function editTerceroContacto($idPadre, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la vista
        $contactos = TerceroContactoModel::where('Tercero_oidTercero_1aM', '=', $idPadre)->where('ClasificacionTercero_oidTerceroContacto', $idClasificacion)->get();

        $tipoContactoId = TipoContactoTerceroModel::where('ClasificacionTercero_oidTipoContactoTercero', $idClasificacion)->pluck('oidTipoContactoTercero');
        $tipoContactoNombre = TipoContactoTerceroModel::where('ClasificacionTercero_oidTipoContactoTercero', $idClasificacion)->pluck('txNombreTipoContactoTercero');

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 17)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroContactoForm', compact('contactos', 'idPadre', 'permisoModificar', 'tipoContactoId', 'tipoContactoNombre', 'idClasificacion'));
    }

    public function updateTerceroContacto(TerceroContactoRequest $request, $idPadre)
    {
        DB::beginTransaction();
        try {
            $this->grabarDetalleContacto($request, $idPadre, 'eliminarAsn_TerceroContacto');
            DB::commit();
            $this->actualizarTerceroContacto($idPadre);
        } catch (\Exception $e) {
            DB::rollback();
        }
        return response(200);
    }

    public function grabarDetalleContacto($request, $idPadre, $datosEliminar)
    {
        $idClasificacion = $request['ClasificacionTercero_oidTerceroContacto'];
        $modelo = new TerceroContactoModel();
        // $datoFactura = $modelo::select("txTipoTerceroContacto")->where('oidTerceroContacto',explode(',', $request[$datosEliminar]))->first();
        // if($datoFactura['txTipoTerceroContacto'] != "FEL"){
        //     $modelo::whereIn($modelo->getKeyName(),explode(',', $request[$datosEliminar]))->delete();
        // }

        $datosConteo = isset($request[$modelo->getKeyName()]) ? count($request[$modelo->getKeyName()]) : 0;

        for ($i = 0; $i <  $datosConteo; $i++) {
            $indice = array($modelo->getKeyName() => $request[$modelo->getKeyName()][$i]);
            $datos = [
                'Tercero_oidTercero_1aM' => $idPadre,
                'txNombreTerceroContacto' => $request["txNombreTerceroContacto"][$i],
                'txCargoTerceroContacto' => $request["txCargoTerceroContacto"][$i],
                'txCorreoTerceroContacto' => $request["txCorreoTerceroContacto"][$i],
                'txMovilTerceroContacto' => $request["txMovilTerceroContacto"][$i],
                'txFormularioTerceroContacto' => $request["txFormularioTerceroContacto"][$i],
                'TipoContactoTercero_oidContactoTercero' => $request["TipoContactoTercero_oidContactoTercero"][$i],
                'ClasificacionTercero_oidTerceroContacto' => $request["ClasificacionTercero_oidTerceroContacto"],
            ];
            $guardar = $modelo::updateOrCreate($indice, $datos);
        }
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $idPadre)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
    }

    //METODO PARA TERCERO PERSONAL------------------------------------------------------------------
    public function editTerceroPersonal($id, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la vista
        $asn_terceropersonal = TerceroPersonalModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->select('oidTerceroPersonal', 'lsSexoTerceroPersonal', 'lsGrupoSanguineoTerceroPersonal', 'lsRHTerceroPersonal', 'daFechaExpedicionTerceroPersonal', 'Ciudad_oidExpedicion', 'daFechaNacimientoTerceroPersonal', 'Ciudad_oidNacimiento', 'lsEstadoCivilTerceroPersonal', 'inEstratoTerceroPersonal', 'lsEscolaridadTerceroPersonal', 'txTituloTerceroPersonal', 'lsViviendaTerceroPersonal', 'lsCaracteristicaViviendaTerceroPersonal', 'lsZonaViviendaTerceroPersonal', 'lsTransporteTerceroPersonal', 'inEstratoServiciosPublicosTerceroPersonal', 'lsEnergiaTerceroPersonal', 'lsAlcantarilladoTerceroPersonal', 'lsAcueductoTerceroPersonal', 'lsGasTerceroPersonal', 'lsBasuraTerceroPersonal', 'lsTelefonoTerceroPersonal', 'lsConsumeAlcoholTerceroPersonal', 'lsPracticaActividadTerceroPersonal', 'lsFumadorTerceroPersonal', 'lsExfumadorTerceroPersonal', 'lsDeudaTerceroPersonal', 'lsRefinanciarDeudaTerceroPersonal', 'lsComputadorTerceroPersonal', 'lsInternetTerceroPersonal')
            ->get();
        // Consultamos los maestros necesarios para las listas de seleccion.
        $tercerociudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");
        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 18)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();
        return view('asociadonegocio::TerceroPersonalForm', compact('asn_terceropersonal', 'id', 'tercerociudad', 'permisoModificar'));
    }

    public function updateTerceroPersonal(TerceroPersonalRequest $request, $id)
    {
        $indice = array('oidTerceroPersonal' => $request['oidTerceroPersonal']);

        $datos = array(
            'Tercero_oidTercero_1aM' => $id,
            'lsSexoTerceroPersonal' => $request['lsSexoTerceroPersonal'],
            'lsGrupoSanguineoTerceroPersonal' => $request['lsGrupoSanguineoTerceroPersonal'],
            'lsRHTerceroPersonal' => $request['lsRHTerceroPersonal'],
            'daFechaExpedicionTerceroPersonal' => $request['daFechaExpedicionTerceroPersonal'],
            'Ciudad_oidExpedicion' => $request['Ciudad_oidExpedicion'],
            'daFechaNacimientoTerceroPersonal' => $request['daFechaNacimientoTerceroPersonal'],
            'Ciudad_oidNacimiento' => $request['Ciudad_oidNacimiento'],
            'lsEstadoCivilTerceroPersonal' => $request['lsEstadoCivilTerceroPersonal'],
            'inEstratoTerceroPersonal' => $request['inEstratoTerceroPersonal'],
            'lsEscolaridadTerceroPersonal' => $request['lsEscolaridadTerceroPersonal'],
            'txTituloTerceroPersonal' => $request['txTituloTerceroPersonal'],
            'lsViviendaTerceroPersonal' => $request['lsViviendaTerceroPersonal'],
            'lsCaracteristicaViviendaTerceroPersonal' => $request['lsCaracteristicaViviendaTerceroPersonal'],
            'lsZonaViviendaTerceroPersonal' => $request['lsZonaViviendaTerceroPersonal'],
            'lsTransporteTerceroPersonal' => $request['lsTransporteTerceroPersonal'],
            'inEstratoServiciosPublicosTerceroPersonal' => $request['inEstratoServiciosPublicosTerceroPersonal'],
            'lsEnergiaTerceroPersonal' => $request['lsEnergiaTerceroPersonal'],
            'lsAlcantarilladoTerceroPersonal' => $request['lsAlcantarilladoTerceroPersonal'],
            'lsAcueductoTerceroPersonal' => $request['lsAcueductoTerceroPersonal'],
            'lsGasTerceroPersonal' => $request['lsGasTerceroPersonal'],
            'lsBasuraTerceroPersonal' => $request['lsBasuraTerceroPersonal'],
            'lsTelefonoTerceroPersonal' => $request['lsTelefonoTerceroPersonal'],
            'lsConsumeAlcoholTerceroPersonal' => $request['lsConsumeAlcoholTerceroPersonal'],
            'lsPracticaActividadTerceroPersonal' => $request['lsPracticaActividadTerceroPersonal'],
            'lsFumadorTerceroPersonal' => $request['lsFumadorTerceroPersonal'],
            'lsExfumadorTerceroPersonal' => $request['lsExfumadorTerceroPersonal'],
            'lsDeudaTerceroPersonal' => $request['lsDeudaTerceroPersonal'],
            'lsRefinanciarDeudaTerceroPersonal' => $request['lsRefinanciarDeudaTerceroPersonal'],
            'lsComputadorTerceroPersonal' => $request['lsComputadorTerceroPersonal'],
            'lsInternetTerceroPersonal' => $request['lsInternetTerceroPersonal'],
        );

        $guardar = TerceroPersonalModel::updateOrCreate($indice, $datos);
        $this->actualizarTercero($id, '');
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $id)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
        return response(['id' => $guardar->oidTerceroPersonal], 200);
    }

    // METODO PARA TERCERO LABORAL--------------------------------------------------------
    public function editTerceroLaboral($id, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la vista
        $asn_tercerolaboral = TerceroLaboralModel::where('Tercero_oidTercero_1a1', '=', $id)
            ->select('oidTerceroLaboral', 'Tercero_oidSalud', 'Tercero_oidPension', 'Tercero_oidCesantias', 'Tercero_oidCaja', 'inPersonasACargoTerceroLaboral', 'inNumeroHijosTerceroLaboral', 'GrupoNomina_oidTerceroLaboral', 'deIngresosFamiliares', 'daFechaRetiroPension', 'chExtranjeroPension', 'chColombianoExterior', 'daFechaIngresoTerceroLaboral', 'lsHorarioTerceroLaboral', 'lsVinculacionTerceroLaboral', 'deSalarioTerceroLaboral', 'deBonoTerceroLaboral', 'lsCabezaFamiliaTerceroLaboral', 'inPersonasHogarTerceroLaboral', 'inPersonasDiscapacidadTerceroLaboral')
            ->get();

        // Consultamos los maestros necesarios para las listas de seleccion.
        $gruponominalista = GrupoNominaModel::where('txEstadoGrupoNomina', '=', 'Activo')->pluck('txNombreGrupoNomina', 'oidGrupoNomina');
        $cargolista = CargoModel::where('txEstadoCargo', '=', 'Activo')->pluck('txNombreCargo', 'oidCargo');

        $terceroSalud = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('asn_tercero.lmTipoServicioTercero', 'like', '%AdministradorSalud%')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Pro')
            ->pluck('txNombreTercero', 'oidTercero');

        $terceroPension = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('asn_tercero.lmTipoServicioTercero', 'like', '%AdministradorPension%')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Pro')
            ->pluck('txNombreTercero', 'oidTercero');

        $terceroCesantia = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('asn_tercero.lmTipoServicioTercero', 'like', '%AdministradorCesantias%')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Pro')
            ->pluck('txNombreTercero', 'oidTercero');

        $terceroCompensacion = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('asn_tercero.lmTipoServicioTercero', 'like', '%AdministradorCompensacion%')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Pro')
            ->pluck('txNombreTercero', 'oidTercero');

        $idCentroTrabajo = CentroTrabajoModel::where('txEstadoCentroTrabajo', '=', 'Activo')->pluck('oidCentroTrabajo');

        $nombreCentroCosto = CentroCostoModel::selectRaw('CONCAT(txNombreCentroCosto," - ",txNombreCompania) as nombre')->leftjoin('seg_compania', 'Compania_oidCompania', '=', 'oidCompania')->pluck('nombre');
        $idCentroCosto = CentroCostoModel::pluck('oidCentroCosto');
        $terceroCentroCosto = TerceroCentroCostoModel::where('Tercero_oidTercero_1aM', $id)->get();

        $nombreCentroTrabajo = CentroTrabajoModel::selectRaw('CONCAT(txNombreCentroTrabajo," - ",txNombreCompania) as nombre')->leftjoin('seg_compania', 'Compania_oidCompania', '=', 'oidCompania')->pluck('nombre');
        $idCentroTrabajo = CentroTrabajoModel::pluck('oidCentroTrabajo');
        $terceroCentroTrabajo = TerceroCentroTrabajoModel::where('Tercero_oidTercero_1aM', $id)->get();

        $nombreCargo = CargoModel::pluck('txNombreCargo');
        $idCargo = CargoModel::pluck('oidCargo');
        $terceroCargo = TerceroCargoModel::where('Tercero_oidTercero_1aM', $id)->get();


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 19)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view(
            'asociadonegocio::TerceroLaboralForm',
            compact(
                'asn_tercerolaboral',
                'id',
                'gruponominalista',
                'cargolista',
                'terceroSalud',
                'terceroPension',
                'terceroCesantia',
                'terceroCompensacion',
                'permisoModificar',
                'nombreCentroCosto',
                'idCentroCosto',
                'terceroCentroCosto',
                'terceroCentroTrabajo',
                'idCentroTrabajo',
                'nombreCentroTrabajo',
                'nombreCargo',
                'idCargo',
                'terceroCargo'
            )
        );
    }

    public function updateTerceroLaboral(TerceroLaboralRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $indice = array('oidTerceroLaboral' => $request['oidTerceroLaboral']);

            $datos = array(
                'Tercero_oidTercero_1a1' => $id,
                'Tercero_oidSalud' => $request['Tercero_oidSalud'],
                'Tercero_oidPension' => $request['Tercero_oidPension'],
                'Tercero_oidCesantias' => $request['Tercero_oidCesantias'],
                'Tercero_oidCaja' => $request['Tercero_oidCaja'],
                'Ciudad_oidExpedicion' => $request['Ciudad_oidExpedicion'],
                'inPersonasACargoTerceroLaboral' => $request['inPersonasACargoTerceroLaboral'],
                'inNumeroHijosTerceroLaboral' => $request['inNumeroHijosTerceroLaboral'],
                'GrupoNomina_oidTerceroLaboral' => $request['GrupoNomina_oidTerceroLaboral'],
                'deIngresosFamiliares' => $request['deIngresosFamiliares'],
                'daFechaRetiroPension' => $request['daFechaRetiroPension'],
                'chExtranjeroPension' => isset($request['chExtranjeroPension']) ? 1 : 0,
                'chColombianoExterior' => isset($request['chColombianoExterior']) ? 1 : 0,
                'daFechaIngresoTerceroLaboral' => $request['daFechaIngresoTerceroLaboral'],
                'lsHorarioTerceroLaboral' => $request['lsHorarioTerceroLaboral'],
                'lsVinculacionTerceroLaboral' => $request['lsVinculacionTerceroLaboral'],
                'deSalarioTerceroLaboral' => $request['deSalarioTerceroLaboral'],
                'deBonoTerceroLaboral' => $request['deBonoTerceroLaboral'],
                'lsCabezaFamiliaTerceroLaboral' => $request['lsCabezaFamiliaTerceroLaboral'],
                'inPersonasHogarTerceroLaboral' => $request['inPersonasHogarTerceroLaboral'],
                'inPersonasDiscapacidadTerceroLaboral' => $request['inPersonasDiscapacidadTerceroLaboral'],
            );
            $guardar = TerceroLaboralModel::updateOrCreate($indice, $datos);
            $this->guardarCentrosCostos($request, $id);
            $this->guardarCentrosTrabajo($request, $id);
            $this->guardarCargos($request, $id);
            DB::commit();
            $this->actualizarTercero($id, '');
            //Actualizamos quien modifico el tercero en los datos de encabezado
            TerceroModel::where('oidTercero', $id)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
            return response(['id' => $id], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
        return response(200);
    }

    public function guardarCentrosCostos($request, $id)
    {
        TerceroCentroCostoModel::where('Tercero_oidTercero_1aM', $id)->delete();
        $totalConteo = ($request['CentroCosto_oidTerceroCentroCosto'] ? count($request['CentroCosto_oidTerceroCentroCosto']) : 0);
        for ($i = 0; $i < $totalConteo; $i++) {
            $indice = ['oidTerceroCentroCosto' => null];
            $datos = [
                'Tercero_oidTercero_1aM' => $id,
                'CentroCosto_oidTerceroCentroCosto' => $request['CentroCosto_oidTerceroCentroCosto'][$i]
            ];
            TerceroCentroCostoModel::updateOrCreate($indice, $datos);
        }
    }

    public function guardarCentrosTrabajo($request, $id)
    {
        TerceroCentroTrabajoModel::where('Tercero_oidTercero_1aM', $id)->delete();
        $totalConteo = ($request['CentroTrabajo_oidTerceroCentroTrabajo'] ? count($request['CentroTrabajo_oidTerceroCentroTrabajo']) : 0);
        for ($i = 0; $i < $totalConteo; $i++) {
            $indice = ['oidTerceroCentroTrabajo' => null];
            $datos = [
                'Tercero_oidTercero_1aM' => $id,
                'CentroTrabajo_oidTerceroCentroTrabajo' => $request['CentroTrabajo_oidTerceroCentroTrabajo'][$i]
            ];
            TerceroCentroTrabajoModel::updateOrCreate($indice, $datos);
        }
    }

    public function guardarCargos($request, $id)
    {
        TerceroCargoModel::where('Tercero_oidTercero_1aM', $id)->delete();
        $totalConteo = ($request['Cargo_oidTerceroCargo'] ? count($request['Cargo_oidTerceroCargo']) : 0);
        for ($i = 0; $i < $totalConteo; $i++) {
            $indice = ['oidTerceroCargo' => null];
            $datos = [
                'Tercero_oidTercero_1aM' => $id,
                'Cargo_oidTerceroCargo' => $request['Cargo_oidTerceroCargo'][$i]
            ];
            TerceroCargoModel::updateOrCreate($indice, $datos);
        }
    }

    // METODO PARA TERERO COMERCIAL ---------------------------------------
    public function editTerceroComercial($id, $idClasificacion)
    {

        // consultar si ya existen datos para mostrarlos en la vista
        $asn_tercerocomercial = TerceroComercialModel::where('Tercero_oidTercero_1a1', '=', $id)
            ->where('Compania_oidCompania', Session::get('oidCompania'))
            ->select('oidTerceroComercial', 'Tercero_oidVendedor', 'FormaPago_oidCompra', 'FormaPago_oidVenta')
            ->get();

        // Consultamos los maestros necesarios para las listas de seleccion
        $tercerovendedor = TerceroModel::findByTipoCliente(['Proveedor'], ['oidTercero', 'txNombreTercero'])->pluck('txNombreTercero', 'oidTercero');
        $terceroformapago = FormaPagoModel::where('gen_formapago.txEstadoFormaPago', '=', 'Activo')->pluck('txNombreFormaPago', 'oidFormaPago');

        // consultar si ya existen datos para mostrarlos en la vista
        $asn_tercerotributario = TerceroTributarioModel::where('Tercero_oidTercero_1a1', $id)->get();

        // Consultamos los maestros necesarios para las listas de seleccion de la segunda tabla
        $terceroactividadeconomica = ActividadEconomicaModel::select(DB::raw("CONCAT(txCodigoActividadEconomica,'-',txNombreActividadEconomica) AS txNombreActividadEconomica"), 'oidActividadEconomica')
            ->where('txEstadoActividadEconomica', 'Activo')
            ->pluck('txNombreActividadEconomica', 'oidActividadEconomica');
        $valorActividadEconomica = TerceroActividadEconomicaModel::where('Tercero_oidTercero_1aM', $id)->pluck('ActividadEconomica_oidActividadEconomica_1aM');

        $terceroclasificacionrenta = ClasificacionRentaModel::pluck('txNombreClasificacionRenta', 'oidClasificacionRenta');
        $terceronaturalezajuridica = NaturalezaJuridicaModel::pluck('txNombreNaturalezaJuridica', 'oidNaturalezaJuridica');

        // Consultamos si ya existen datos de Calculos tributarios
        $terceroimpuesto = TerceroImpuestoModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->select('oidTerceroImpuesto', 'lsConceptoTerceroImpuesto', 'chProveedorAplicaTerceroImpuesto', 'chProveedorBase0TerceroImpuesto', 'chClienteAplicaTerceroImpuesto', 'chClienteBase0TerceroImpuesto')
            ->get();



        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 22)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view(
            'asociadonegocio::TerceroComercialForm',
            compact(
                'asn_tercerocomercial',
                'asn_tercerotributario',
                'id',
                'tercerovendedor',
                'terceroformapago',
                'terceroactividadeconomica',
                'terceroclasificacionrenta',
                'terceronaturalezajuridica',
                'terceroimpuesto',
                'permisoModificar',
                'valorActividadEconomica'
            )
        );
    }

    public function updateTerceroComercial(Request $request, $idPadre)
    {
        DB::beginTransaction();
        try {
            $this->grabarDetalleComercial($request, $idPadre, 'eliminarTerceroImpuesto');
            DB::commit();
            $this->actualizarTercero($idPadre, '');
            return response(['id' => $idPadre], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function grabarDetalleComercial($request, $idPadre, $datosEliminar)
    {
        //Guardado de datos Comerciales
        $indice = array('oidTerceroComercial' => $request['oidTerceroComercial']);
        $datos = [
            'Tercero_oidTercero_1a1' => $idPadre,
            'Tercero_oidVendedor' => $request['Tercero_oidVendedor'],
            'FormaPago_oidCompra' => $request['FormaPago_oidCompra'],
            'FormaPago_oidVenta' => $request['FormaPago_oidVenta'],
            'Compania_oidCompania' => Session::get('oidCompania'),

        ];
        TerceroComercialModel::updateOrCreate($indice, $datos);

        //Guardado de datos Tributario
        $indice = array('oidTerceroTributario' => $request['oidTerceroTributario']);
        $datos = [
            'Tercero_oidTercero_1a1' => $idPadre,
            'chPracticaRetencionTerceroTributario' => isset($request['chPracticaRetencionTerceroTributario']) ? 1 : 0,
            'lsRegimenVentaTerceroTributario' => $request['lsRegimenVentaTerceroTributario'],
            'chAutoretenedorTerceroTributario' => isset($request['chAutoretenedorTerceroTributario']) ? 1 : 0,
            'NaturalezaJuridica_oidTerceroTributario' => $request['NaturalezaJuridica_oidTerceroTributario'],
            'chAutoreenedorCREETerceroTributario' => isset($request['chAutoreenedorCREETerceroTributario']) ? 1 : 0,
            'chGranContribuyenteTerceroTributario' => isset($request['chGranContribuyenteTerceroTributario']) ? 1 : 0,
            'ClasificacionRenta_oidTerceroTributario' => $request['ClasificacionRenta_oidTerceroTributario'],
            'chEntidadEstadoTerceroTributario' => isset($request['chEntidadEstadoTerceroTributario']) ? 1 : 0,
            'chNoResponsableIVATerceroTributario' => isset($request['chNoResponsableIVATerceroTributario']) ? 1 : 0,
        ];
        TerceroTributarioModel::updateOrCreate($indice, $datos);

        //GUARDAMOS LAS ACTIVIDADES ECONOMICAS EN UNA TABLA INTERMEDIA, YA QUE SE PUEDE TENER VARIAS
        $actividad = ($request['ActividadEconomica_oidActividadEconomica_1aM'] ? count($request['ActividadEconomica_oidActividadEconomica_1aM']) : 0);
        TerceroActividadEconomicaModel::where('Tercero_oidTercero_1aM', $idPadre)->delete();
        for ($i = 0; $i < $actividad; $i++) {
            $indice = array('oidTerceroActividadEconomica' => null);
            $datos = array(
                'Tercero_oidTercero_1aM' => $idPadre,
                'ActividadEconomica_oidActividadEconomica_1aM' => $request['ActividadEconomica_oidActividadEconomica_1aM'][$i]
            );
            TerceroActividadEconomicaModel::updateOrCreate($indice, $datos);
        }

        //Guardado de datos de impuestos
        $modelo = new TerceroImpuestoModel();
        $modelo::whereIn($modelo->getKeyName(), explode(',', $request[$datosEliminar]))->delete();
        $totalReg = isset($request[$modelo->getKeyName()]) ? count($request[$modelo->getKeyName()]) : 0;
        for ($i = 0; $i < $totalReg; $i++) {
            $indice = array($modelo->getKeyName() => $request[$modelo->getKeyName()][$i]);

            $datos = [
                'Tercero_oidTercero_1aM' => $idPadre,
                'lsConceptoTerceroImpuesto' => $request['lsConceptoTerceroImpuesto'][$i],
                'chProveedorAplicaTerceroImpuesto' => $request['chProveedorAplicaTerceroImpuesto'][$i],
                'chProveedorBase0TerceroImpuesto' => $request['chProveedorBase0TerceroImpuesto'][$i],
                'chClienteAplicaTerceroImpuesto' => $request['chClienteAplicaTerceroImpuesto'][$i],
                'chClienteBase0TerceroImpuesto' => $request['chClienteBase0TerceroImpuesto'][$i],
            ];

            $guardar = $modelo::updateOrCreate($indice, $datos);
        }
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $idPadre)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
    }

    // METODO PARA TERCERO REFERENCIA----------------------------------------------------------
    public function editTerceroReferencia($id, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la multi registro de personal
        $asn_terceropersonal = TerceroReferenciaModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroReferencia', '=', 'Personal')
            ->select(DB::raw('oidTerceroReferencia as oidTerceroPersonal, txNombreTerceroReferencia as txNombreTerceroPersonal, txTipoTerceroReferencia as txTipoTerceroPersonal, txOcupacionTerceroReferencia as txOcupacionTerceroPersonal, txTelefonoTerceroReferencia as txTelefonoTerceroPersonal, txDireccionTerceroReferencia as txDireccionTerceroPersonal, txObservacionTerceroReferencia as txObservacionTerceroPersonal'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Comercial
        $asn_tercerocomercial = TerceroReferenciaModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroReferencia', '=', 'Comercial')
            ->select(DB::raw('oidTerceroReferencia as oidTerceroComercial, txNombreTerceroReferencia as txNombreTerceroComercial, txTipoTerceroReferencia as txTipoTerceroComercial, txParentescoTerceroReferencia as txParentescoTerceroComercial, txOcupacionTerceroReferencia as txOcupacionTerceroComercial, txTelefonoTerceroReferencia as txTelefonoTerceroComercial, txDireccionTerceroReferencia as txDireccionTerceroComercial, txObservacionTerceroReferencia as txObservacionTerceroComercial'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Convivencia
        $asn_terceroconvivencia = TerceroReferenciaModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroReferencia', '=', 'Convivencia')
            ->select(DB::raw('oidTerceroReferencia as oidTerceroConvivencia, txNombreTerceroReferencia as txNombreTerceroConvivencia, txTipoTerceroReferencia as txTipoTerceroConvivencia, txParentescoTerceroReferencia as txParentescoTerceroConvivencia, daFechaNacimientoTerceroReferencia as daFechaNacimientoTerceroConvivencia, txNivelEducativoTerceroReferencia as txNivelEducativoTerceroConvivencia, txOcupacionTerceroReferencia as txOcupacionTerceroConvivencia, txObservacionTerceroReferencia as txObservacionTerceroConvivencia, txTelefonoTerceroReferencia as txTelefonoTerceroConvivencia'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Convivencia
        $asn_tercerofamiliar = TerceroReferenciaModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroReferencia', '=', 'Familiar')
            ->select(DB::raw('oidTerceroReferencia as oidTerceroFamiliar, txNombreTerceroReferencia as txNombreTerceroFamiliar, txTipoTerceroReferencia as txTipoTerceroFamiliar, txParentescoTerceroReferencia as txParentescoTerceroFamiliar, txOcupacionTerceroReferencia as txOcupacionTerceroFamiliar, txTelefonoTerceroReferencia as txTelefonoTerceroFamiliar, txDireccionTerceroReferencia as txDireccionTerceroFamiliar, txObservacionTerceroReferencia as txObservacionTerceroFamiliar'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Financieras
        $asn_tercerofinancieras = TerceroReferenciaModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroReferencia', '=', 'Financiera')
            ->select(DB::raw('oidTerceroReferencia as oidTerceroFinancieras, txNombreTerceroReferencia as txNombreTerceroFinancieras, txTipoTerceroReferencia as txTipoTerceroFinancieras, txTelefonoTerceroReferencia as txTelefonoTerceroFinancieras, txDireccionTerceroReferencia as txDireccionTerceroFinancieras, txObservacionTerceroReferencia as txObservacionTerceroFinancieras'))
            ->get();

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 20)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroReferenciaForm', compact('asn_terceropersonal', 'id', 'asn_tercerocomercial', 'asn_terceroconvivencia', 'asn_tercerofamiliar', 'asn_tercerofinancieras', 'permisoModificar'));
    }

    public function updateTerceroReferencia(TerceroReferenciaRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $sufijos = ['Personal', 'Comercial', 'Convivencia', 'Familiar'];
            foreach ($sufijos as $sufijo) {
                $this->grabarReferencias($request, $id, $sufijo);
            }
            DB::commit();
            $this->actualizarTerceroReferencia($id);
        } catch (\Exception $e) {
            DB::rollback();
        }
        return response(200);
    }

    public function grabarReferencias($request, $idPadre, $sufijo)
    {

        $idsEliminar = explode(',', $request['eliminarAsn_Tercero' . $sufijo]);
        TerceroReferenciaModel::whereIn('oidTerceroReferencia', $idsEliminar)->delete();
        $ids = $request['oidTercero' . $sufijo];
        if ($ids) {
            for ($i = 0; $i < count($request['oidTercero' . $sufijo]); $i++) {
                $indice = array('oidTerceroReferencia' => $request['oidTercero' . $sufijo][$i]);
                $datos = array(
                    'Tercero_oidTercero_1aM' => $idPadre,
                    'txNombreTerceroReferencia' => $request['txNombreTercero' . $sufijo][$i],
                    'txTipoTerceroReferencia' => $request['txTipoTercero' . $sufijo][$i],
                    'txParentescoTerceroReferencia' => $request['txParentescoTercero' . $sufijo][$i],
                    'daFechaNacimientoTerceroReferencia' => $request['daFechaNacimientoTercero' . $sufijo][$i],
                    'txNivelEducativoTerceroReferencia' => $request['txNivelEducativoTercero' . $sufijo][$i],
                    'txOcupacionTerceroReferencia' => $request['txOcupacionTercero' . $sufijo][$i],
                    'txObservacionTerceroReferencia' => $request['txObservacionTercero' . $sufijo][$i],
                    'txDireccionTerceroReferencia' => $request['txDireccionTercero' . $sufijo][$i],
                    'txTelefonoTerceroReferencia' => $request['txTelefonoTercero' . $sufijo][$i],
                );
                TerceroReferenciaModel::updateOrCreate($indice, $datos);
            }
        }
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $idPadre)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
    }

    public function terceroSucursalGrid($idPadre, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la vista
        $tercerosucursal = TerceroSucursalModel::where('Tercero_oidTercero_1aM', '=', $idPadre)
            ->select(
                'oidTerceroSucursal',
                'txCodigoTerceroSucursal',
                'txNombreComercialTerceroSucursal',
                'txNombreCiudad',
                'txCodigoBarrasTerceroSucursal',
                'txTelefono1TerceroSucursal',
                'txCorreoElectronicoTerceroSucursal',
                'txDireccionTerceroSucursal'
            )
            ->leftjoin('gen_ciudad', 'Ciudad_oidTerceroSucursal', '=', 'oidCiudad')
            ->get();
        return view('asociadonegocio::TerceroSucursalGrid', compact('tercerosucursal', 'idPadre'));
    }

    // TERCERO SUCURSAL--------------------------------------------------------------
    // Funsion para abrir modal de suscursales en modo de crear
    public function crearTerceroSucursal($idPadre)
    {
        $tercerosucursal = TerceroSucursalModel::where('Tercero_oidTercero_1aM', '=', $idPadre)
            ->get();
        $ciudad = CiudadModel::pluck('txNombreCiudad', 'oidCiudad');
        return view('asociadonegocio::TerceroSucursalForm', compact('idPadre', 'ciudad', 'tercerosucursal'));
    }

    // TERCERO SUCURSAL--------------------------------------------------------------
    //Funsion para abrir el modal de tercero sucursal en modo de edit
    public function editTerceroSucursal($idSucursal)
    {
        // Consultamos los datos para mostrar en el modal para edición
        $tercerosucursal = TerceroSucursalModel::where('oidTerceroSucursal', $idSucursal)
            ->first();
        $idPadre = $tercerosucursal->Tercero_oidTercero_1aM;
        //consulta las listas desplegables
        $ciudad = CiudadModel::pluck('txNombreCiudad', 'oidCiudad');
        return view('asociadonegocio::TerceroSucursalForm', compact('idSucursal', 'idPadre', 'ciudad', 'tercerosucursal'));
    }

    public function updateTerceroSucursal(TerceroSucursalRequest $request)
    {

        DB::beginTransaction();
        try {
            $indice = array('oidTerceroSucursal' => $request['oidTerceroSucursal']);
            $datos = array(
                'Tercero_oidTercero_1aM' => $request['Tercero_oidTercero_1aM'],
                'txCodigoTerceroSucursal' => $request['txCodigoTerceroSucursal'],
                'txNombreComercialTerceroSucursal' => $request['txNombreComercialTerceroSucursal'],
                'Ciudad_oidTerceroSucursal' => $request['Ciudad_oidTerceroSucursal'],
                'txCodigoBarrasTerceroSucursal' => $request['txCodigoBarrasTerceroSucursal'],
                'txTelefono1TerceroSucursal' => $request['txTelefono1TerceroSucursal'],
                'txCorreoElectronicoTerceroSucursal' => $request['txCorreoElectronicoTerceroSucursal'],
                'txDireccionTerceroSucursal' => $request['txDireccionTerceroSucursal'],
            );


            $guardar = TerceroSucursalModel::updateOrCreate($indice, $datos);
            $id = $guardar->oidTerceroSucursal;
            $this->actualizarSucursal($id);
            DB::commit();
            return response(['id' => $id], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    // ----------------------------------------------------------------------------------------------------
    // --------------------------------------------ASOCIADO NEGOCIO GRUPO----------------------------------
    // ----------------------------------------------------------------------------------------------------

    //Funcion para cargar vista en modo edicion
    public function editGrupoProducto($id, $idClasificacion)
    {

        $clasificacionGrupos = GrupoProductoModel::select('oidClasificacionProductoGrupo', 'txNombreClasificacionProductoGrupo')
            ->leftjoin('gen_clasificacionproductogrupo', 'ClasificacionProductoGrupo_oidClasificacionProductoGrupo', 'oidClasificacionProductoGrupo')
            ->groupby('oidClasificacionProductoGrupo', 'txNombreClasificacionProductoGrupo')->where('Tercero_oidTercero_1aM', $id)->get();

        $rolUsuario = Session::get('rolUsuario');

        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', 24)->Where('oidRol', $rolUsuario)->Where('ClasificacionTercero_oidClasificacionTercero_1aM', $idClasificacion);
            })->first();

        return view('asociadonegocio::GrupoProducto', compact('id', 'clasificacionGrupos', 'permisoModificar'));
    }

    //TODO: Revisar actualizacion a saya
    public function updateGrupoProducto(Request $request, $id)
    {
        $companias = CompaniaModel::all();

        DB::beginTransaction();
        $this->beginSayaTransaction($companias);
        try {
            $grupoProductos = GrupoProductoModel::whereIn('oidGrupoProducto', explode(',', $request["eliminarGrupoProducto"]))->get();
            foreach ($grupoProductos as $grupo) {
                $grupo->delete();
            }

            $clasificacionGrupos = GrupoProductoModel::whereIn("ClasificacionProductoGrupo_oidClasificacionProductoGrupo", explode(",", $request["eliminarClasificaciones"]))
                ->where('Tercero_oidTercero_1aM', $id)->get();
            foreach ($clasificacionGrupos as $clasificacion) {
                $clasificacion->delete();
            }

            if (isset($request['oidGrupoProducto'])) {
                for ($i = 0; $i < count($request['oidGrupoProducto']); $i++) {
                    $idGrupo = $request['oidGrupoProducto'][$i];
                    $ClasificacionProductoGrupo = $request["ClasificacionProductoGrupo_oidClasificacionProductoGrupo"][$i];
                    $codigo = $request["txCodigoGrupoProducto"][$i];
                    $nombre = $request['txNombreGrupoProducto'][$i];
                    $saveSaya = false;

                    $grupoProducto = $idGrupo ? GrupoProductoModel::find($idGrupo) : new GrupoProductoModel();
                    $codigoAnterior = $grupoProducto->txCodigoGrupoProducto ?? null;
                    $grupoProducto->Tercero_oidTercero_1aM = $id;
                    $grupoProducto->ClasificacionProductoGrupo_oidClasificacionProductoGrupo = $ClasificacionProductoGrupo;
                    $grupoProducto->txCodigoGrupoProducto = $codigo;
                    $grupoProducto->txNombreGrupoProducto = $nombre;

                    if ($grupoProducto->exists) {
                        if ($grupoProducto->getOriginal('txCodigoGrupoProducto') != $codigo || $grupoProducto->getOriginal('txNombreGrupoProducto') != $nombre) {
                            $saveSaya = true;
                        }
                    } else {
                        $saveSaya = true;
                    }
                    $grupoProducto->save();

                    if ($saveSaya) {
                        $this->sayaActualizarDinamicas($grupoProducto, $codigoAnterior, $companias);
                    }
                }
            }
            DB::commit();
            $this->commitSayaTransaction($companias);
            return response(['oidTercero' => $id], 200);
        } catch (\Exception $e) {
            DB::rollback();
            $this->rollbackSayaTransaction($companias);
            return response(['message' => $e], 500);
        }
    }

    //Funcion para cargar centros de produccion en multi
    public function listaClasificacionesProducto()
    {
        $grupos = ClasificacionProductoGrupoModel::select('oidClasificacionProductoGrupo', 'txNombreClasificacionProductoGrupo')->get();
        return view('asociadonegocio::listaClasificacionProductoGrupo', compact('grupos'));
    }

    public function listaGrupos(Request $request)
    {
        $idTercero = $request->get("idTercero");
        $idClasificacion = $request->get("idClasificacion");

        $datos = GrupoProductoModel::select('oidGrupoProducto', 'ClasificacionProductoGrupo_oidClasificacionProductoGrupo', 'txCodigoGrupoProducto', 'txNombreGrupoProducto')
            ->where('ClasificacionProductoGrupo_oidClasificacionProductoGrupo', '=', $idClasificacion)->where('Tercero_oidTercero_1aM', '=', $idTercero)->get();

        return json_encode($datos);
    }

    //FUNCION PARA GUARDAR LOS BANCOS
    public function editTerceroBanco($id, $idClasificacion)
    {
        //consulta si hay cuentas bancarias almacenadas
        $bancos = TerceroBancoModel::where('Tercero_oidTercero_1aM', '=', $id)->get();
        //consulta las listas desplegables
        $terceros = TerceroModel::findByTipoCliente(["Banco"], ["txNombreTercero", 'oidTercero']);
        $nombreTercero = $terceros->pluck('txNombreTercero');
        $idTercero = $terceros->pluck('oidTercero');


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 25)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroBancoForm', compact('id', 'bancos', 'nombreTercero', 'idTercero', 'permisoModificar'));
    }

    public function updateTerceroBanco(Request $request, $idPadre)
    {

        DB::beginTransaction();
        try {
            $this->grabarDetalleBanco($request, $idPadre, 'eliminarBanco');
            DB::commit();
            $this->actualizarTerceroBanco($idPadre);
            return response(200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function grabarDetalleBanco($request, $idPadre, $datosEliminar)
    {
        $modelo = new TerceroBancoModel();
        $modelo::whereIn($modelo->getKeyName(), explode(',', $request[$datosEliminar]))->delete();

        $datosConteo = isset($request[$modelo->getKeyName()]) ? count($request[$modelo->getKeyName()]) : 0;

        for ($i = 0; $i < $datosConteo; $i++) {
            $indice = array($modelo->getKeyName() => $request[$modelo->getKeyName()][$i]);

            $datos = [
                'Tercero_oidTercero_1aM' => $idPadre,
                'Tercero_oidTerceroBanco' => $request['Tercero_oidTerceroBanco'][$i],
                'txNombreCuentaTerceroBanco' => $request['txNombreCuentaTerceroBanco'][$i],
                'txTitularTerceroBanco' => $request['txTitularTerceroBanco'][$i],
                'lsTipoTerceroBanco' => $request['lsTipoTerceroBanco'][$i],
                'txNumeroCuentaTerceroBanco' => $request['txNumeroCuentaTerceroBanco'][$i],
            ];

            $guardar = $modelo::updateOrCreate($indice, $datos);
        }
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $idPadre)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
        return response(200);
    }
    //consulta de ajax
    public function consultarIdentificacion($id)
    {

        $consultaIdentificacion = tipoIdentificacionModel::select('chDocumentoVerificacionTipoIdentificacion', 'lsTipoNombreTipoIdentificacion', 'chDocumentoAutomaticoTipoIdentificacion', 'txDocumentoPrefijoTipoIdentificacion')
            ->where('oidTipoIdentificacion', '=', $id)
            ->get();

        if ($id == 3) {
            $numero = $consultaIdentificacion[0]['txDocumentoPrefijoTipoIdentificacion'];
            $documentoMayorTercero = TerceroModel::select(DB::raw("CONCAT($numero,MAX(CAST(SUBSTRING(txDocumentoTercero, 6) AS UNSIGNED))+1) AS nuevo"))
                ->where('TipoIdentificacion_oidTercero', $id)
                ->where(\DB::raw('SUBSTRING(txDocumentoTercero, 1, 5)'), '=', $consultaIdentificacion[0]['txDocumentoPrefijoTipoIdentificacion'])
                ->get();
        } else {
            $documentoMayorTercero = '';
        }
        return response(["datos" => $consultaIdentificacion, "numeroAutomatico" => $documentoMayorTercero], 200);
    }

    public function editTerceroGrupo($id, $idClasificacion)
    {
        $clasificacionTercero = ClasificacionTerceroGrupoModel::selectRaw('oidClasificacionTerceroGrupo, max(txNombreClasificacionTerceroGrupo) as txNombreClasificacionTerceroGrupo,max(GrupoTercero_oidTerceroGrupo) as grupo, max(oidTerceroGrupo) as oidTerceroGrupo, chMultipleClasificacionTerceroGrupo,chObligatorioClasificacionTerceroGrupo,MAX(txObservacionTerceroGrupo) as observaciones')
            ->leftjoin('asn_terceroclasificacion', 'gen_clasificaciontercerogrupo.ClasificacionTercero_oidClasificacionTercero_1aM', '=', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM')
            ->leftjoin('asn_tercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM', '=', 'oidTercero')
            ->leftjoin('asn_grupotercero', 'oidClasificacionTerceroGrupo', '=', 'ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo')
            ->leftjoin('asn_tercerogrupo', function ($union) {
                $union->on('oidGrupoTercero', '=', 'GrupoTercero_oidTerceroGrupo')
                    ->on('oidTercero', '=', 'asn_tercerogrupo.Tercero_oidTercero_1aM');
            })
            ->where('oidTercero', $id)
            ->where('asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', $idClasificacion)
            ->groupby('oidClasificacionTerceroGrupo')
            ->get();


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 26)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroGrupoForm', compact('clasificacionTercero', 'id', 'permisoModificar', 'idClasificacion'));
    }

    public function updateTerceroGrupo(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $idClasificacion = $request["ClasificacionTercero_oidTerceroGrupo"];
            $nombreClasificacio = ClasificacionTerceroModel::where('oidClasificacionTercero', $idClasificacion)->pluck('txNombreClasificacionTercero');
            $grupo = '';
            //Buscamos los ids de los grupos que seleccionamos en el formulario.
            for ($i = 0; $i < count($request['oidTerceroGrupo']); $i++) {
                $total = (isset($request["GrupoTercero_oidTerceroGrupo"][$i]) ? count($request["GrupoTercero_oidTerceroGrupo"][$i]) : 0);
                for ($j = 0; $j < $total; $j++) {
                    if (isset($request["GrupoTercero_oidTerceroGrupo"][$i][$j])) {

                        $grupo .= $request["GrupoTercero_oidTerceroGrupo"][$i][$j] . ',';
                        // $grupos = substr($grupo, 0, -1);
                        $grupos = trim($grupo, ',');
                    }
                }
            }
            //consultamos cuales de las opciones que se seleccionaron o se quieren modificar
            $consulta = DB::select("SELECT
                oidClasificacionTerceroGrupo,
                ClasificacionTercero_oidClasificacionTercero_1aM,
                txNombreClasificacionTerceroGrupo,
                chCorreoClasificacionTerceroGrupo,
                nuevo.txNombreGrupoTercero,
                nuevo.txCorreoGrupoTercero AS NuevoCorreo,
                nuevo.txMensajeGrupoTercero,
                txNombreTercero,
                txDocumentoTercero
            FROM
                kunde.gen_clasificaciontercerogrupo
                    left JOIN
                asn_grupotercero viejo ON ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = oidClasificacionTerceroGrupo
                    left JOIN
                asn_tercerogrupo ON GrupoTercero_oidTerceroGrupo = oidGrupoTercero
                    INNER JOIN
                asn_tercero ON Tercero_oidTercero_1aM = oidTercero
                    LEFT JOIN
                (SELECT
                    txNombreGrupoTercero,
                    txCorreoGrupoTercero,
                    txMensajeGrupoTercero,
                    oidGrupoTercero,
                    ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo
                FROM
                    asn_grupotercero
                WHERE
                    oidGrupoTercero IN ($grupos)
                    AND txCorreoGrupoTercero != '') nuevo ON nuevo.ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = oidClasificacionTerceroGrupo
            WHERE
                ClasificacionTercero_oidClasificacionTercero_1aM = $idClasificacion
                AND Tercero_oidTercero_1aM = $id
                AND chCorreoClasificacionTerceroGrupo = 1
                AND viejo.txCorreoGrupoTercero != ''
                AND viejo.oidGrupoTercero != nuevo.oidGrupoTercero
                AND lmClasificacionTercero like 'Seguimiento'");

            $consultaCreacionNuevo = DB::select("SELECT
                    txNombreGrupoTercero,
                    txCorreoGrupoTercero,
                    txMensajeGrupoTercero,
                    oidGrupoTercero,
                    ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo,
                    txNombreTercero,
                    txDocumentoTercero
                FROM
                    asn_grupotercero
                    LEFT JOIN
                    gen_clasificaciontercerogrupo ON ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = oidClasificacionTerceroGrupo
                    LEFT JOIN
                    asn_tercero on oidTercero = $id
                WHERE
                    oidGrupoTercero IN ($grupos)
                    AND txCorreoGrupoTercero != ''
                    AND chCorreoClasificacionTerceroGrupo = 1
                    AND lmClasificacionTercero like 'Seguimiento'");

            if (count($consulta)) {
                // dd(count($consulta).'primera');
                //Recorremos la consulta para enviar los correos a los campos que se modificaron.
                for ($i = 0; $i < count($consulta); $i++) {
                    $correo = new NotificacionCreacionTercero($consulta[$i]->txMensajeGrupoTercero, $nombreClasificacio[0], $consulta[$i]->txNombreTercero, $consulta[$i]->txDocumentoTercero);
                    Mail::to($consulta[$i]->NuevoCorreo)->send($correo);
                }
            }
            //Funcion para enviar el correo cuando se agrega un grupo por primera vez
            if (count($consultaCreacionNuevo)) {
                //Recorremos la consulta para enviar los correos correspondientes.
                for ($i = 0; $i < count($consultaCreacionNuevo); $i++) {
                    // dd($consultaCreacionNuevo[$i]->txCorreoGrupoTercero);
                    $registro = TerceroGrupoModel::where('GrupoTercero_oidTerceroGrupo', $consultaCreacionNuevo[$i]->oidGrupoTercero)->where('Tercero_oidTercero_1aM', $id)->select()->get();

                    if ($registro->isEmpty()) {
                        $correo = new NotificacionCreacionTercero($consultaCreacionNuevo[$i]->txMensajeGrupoTercero, $nombreClasificacio[0], $consultaCreacionNuevo[$i]->txNombreTercero, $consultaCreacionNuevo[$i]->txDocumentoTercero);
                        Mail::to($consultaCreacionNuevo[$i]->txCorreoGrupoTercero)->send($correo);
                    }
                }
            }

            //Guardamos los grupos dinamicos
            TerceroGrupoModel::where('Tercero_oidTercero_1aM', '=', $id)->where('ClasificacionTercero_oidTerceroGrupo', '=', $idClasificacion)->where('Compania_oidCompania', '=', Session::get('oidCompania'))->delete();
            for ($i = 0; $i < count($request['oidTerceroGrupo']); $i++) {
                $total = (isset($request["GrupoTercero_oidTerceroGrupo"][$i]) ? count($request["GrupoTercero_oidTerceroGrupo"][$i]) : 0);
                for ($j = 0; $j < $total; $j++) {
                    if (isset($request["GrupoTercero_oidTerceroGrupo"][$i][$j])) {
                        $indice = ['oidTerceroGrupo' => null];
                        $datosReg = [
                            'Tercero_oidTercero_1aM' => $id,
                            'GrupoTercero_oidTerceroGrupo' => $request["GrupoTercero_oidTerceroGrupo"][$i][$j],
                            'ClasificacionTercero_oidTerceroGrupo' => $idClasificacion,
                            'Compania_oidCompania' => Session::get('oidCompania'),
                            'txObservacionTerceroGrupo' => (isset($request["txObservacionTerceroGrupo"][$i]) ? $request["txObservacionTerceroGrupo"][$i] : null),
                        ];
                        TerceroGrupoModel::updateOrCreate($indice, $datosReg);
                    }
                }
            }
            //Actualizamos quien modifico el tercero en los datos de encabezado
            TerceroModel::where('oidTercero', $id)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);

            DB::commit();
            $this->actualizarTercero($id, '');
            return response(['id' => $id], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function validacionTerceroGrupo(Request $request)
    {


        $resultado = '';
        for ($i = 0; $i < count($request['oidTerceroGrupo']); $i++) {
            $campo = (isset($request["GrupoTercero_oidTerceroGrupo"][$i][0]) ? $request["GrupoTercero_oidTerceroGrupo"][$i][0] : 'Error');

            $obligatorio = substr($request['obligatorio'][$i], 0, 1);
            if ($obligatorio == 1 && $campo == 'Error') {
                $NombreObligatorio = substr($request['obligatorio'][$i], 1);
                $resultado .= $NombreObligatorio . ', ';
            }
        }
        if ($resultado != '') {
            return abort(400, 'Los siguientes campos son obligatorios:  ' . $resultado);
        }

        return response(200);
    }

    public function editTerceroLogistica($id, $idClasificacion)
    {
        $tercero = TerceroModel::where('oidTercero', $id)->select('txPrefijoProductoTercero', 'txGrupoSizfraTercero')->get();
        $terceroLogistica = TerceroLogisticaModel::where('Tercero_oidTercero_1aM', '=', $id)->first();
        if ($terceroLogistica) {
            $terceroLogistica->lmSitioEntregaTerceroLogistica = explode(',', $terceroLogistica->lmSitioEntregaTerceroLogistica);
            $terceroLogistica->lmRequisitoEntregaTerceroLogistica = explode(',', $terceroLogistica->lmRequisitoEntregaTerceroLogistica);
        }
        $tipoProducto = TipoProductoTerceroModel::pluck('txNombreTipoProductoTercero', 'oidTipoProductoTercero');
        $canalDistribucion = CanalDistribucionTerceroModel::pluck('txNombreCanalDistribucionTercero', 'oidCanalDistribucionTercero');
        $tipoEtiqueta = TipoEtiquetaTerceroModel::pluck('txNombreTipoEtiquetaTercero', 'oidTipoEtiquetaTercero');
        $procesoLogistico = ProcesoLogisticoTerceroModel::pluck('txNombreProcesoLogisticoTercero', 'oidProcesoLogisticoTercero');


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 27)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroLogisticaForm', compact('terceroLogistica', 'id', 'permisoModificar', 'tercero', 'tipoProducto', 'canalDistribucion', 'tipoEtiqueta', 'procesoLogistico'));
    }

    public function updateTerceroLogistica(Request $request, $id)
    {
        $indice = array('oidTerceroLogistica' => $request['oidTerceroLogistica']);

        $datos = array(
            'Tercero_oidTercero_1aM' => $id,
            'inCierreContableTerceroLogistica' => $request['inCierreContableTerceroLogistica'],
            'lsCierreContableTerceroLogistica' => $request['lsCierreContableTerceroLogistica'],
            'txTiempoPermanenciaTerceroLogistica' => $request['txTiempoPermanenciaTerceroLogistica'],
            'chRequiereShowRoomTerceroLogistica' => isset($request['chRequiereShowRoomTerceroLogistica']) ? 1 : 0,
            'lsManejoMuestrasTerceroLogistica' => $request['lsManejoMuestrasTerceroLogistica'],
            'lsMontajePedidoTerceroLogistica' => $request['lsMontajePedidoTerceroLogistica'],
            'inFrecuenciaPedidoTerceroLogistica' => $request['inFrecuenciaPedidoTerceroLogistica'],
            'lsFrecuenciaPedidoTerceroLogistica' => $request['lsFrecuenciaPedidoTerceroLogistica'],
            'lmSitioEntregaTerceroLogistica' => implode(',', $request['lmSitioEntregaTerceroLogistica']),
            'txHorarioEntregaTerceroLogistica' => $request['txHorarioEntregaTerceroLogistica'],
            'txNombreEntregaTerceroLogistica' => $request['txNombreEntregaTerceroLogistica'],
            'txDireccionEntregaTerceroLogistica' => $request['txDireccionEntregaTerceroLogistica'],
            'txTelefonoEntregaTerceroLogistica' => $request['txTelefonoEntregaTerceroLogistica'],
            'lmRequisitoEntregaTerceroLogistica' => implode(',', $request['lmRequisitoEntregaTerceroLogistica']),
            'txOtrosRequisitosTerceroLogistica' => $request['txOtrosRequisitosTerceroLogistica'],
            'txManejoDevolucionesTerceroLogistica' => $request['txManejoDevolucionesTerceroLogistica'],
            'txObservacionTerceroLogistica' => $request['txObservacionTerceroLogistica'],
            'TipoProducto_oidTerceroLogistica' => $request['TipoProducto_oidTerceroLogistica'],
            'CanalDistribucion_oidTerceroLogistica' => $request['CanalDistribucion_oidTerceroLogistica'],
            'TipoEtiqueta_oidTerceroLogistica' => $request['TipoEtiqueta_oidTerceroLogistica'],
            'ProcesoLogistico_oidTerceroLogistica' => $request['ProcesoLogistico_oidTerceroLogistica'],

        );

        $guardar = TerceroLogisticaModel::updateOrCreate($indice, $datos);
        TerceroModel::where('oidTercero', $id)->update(['txPrefijoProductoTercero' => $request['txPrefijoProductoTercero'], 'txGrupoSizfraTercero' => $request['txGrupoSizfraTercero'], 'Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
        return response(['id' => $guardar], 200);
    }

    public function editTerceroCertificado($id, $idClasificacion)
    {
        $idCompania = Session::get('oidCompania');

        $terceroCertificado = DB::select("SELECT
            oidTipoCertificado,
            txNombreTipoCertificado,
            lsTipoTipoCertificado,
            oidTerceroCertificado,
            txVigenciaTipoCertificado,
            TC.Tercero_oidTercero_1aM,
            TC.TipoCertificado_oidTerceroCertificado,
            lsVerificacionTerceroCertificado,
            oidTerceroAdjunto,
            txRutaTerceroAdjunto,
            daFechaCreacionTerceroAdjunto,
            daFechaVencimientoTerceroAdjunto,
            txDescripcionTerceroAdjunto
            FROM
        gen_tipocertificado TCF
            LEFT JOIN
        asn_tercerocertificado TC ON oidTipoCertificado = TipoCertificado_oidTerceroCertificado and TC.Tercero_oidTercero_1aM = $id and TC.Compania_oidCompania = $idCompania
            LEFT JOIN
        asn_terceroadjunto TA ON TC.TipoCertificado_oidTerceroCertificado = TA.TipoCertificado_oidTerceroCertificado and TA.Tercero_oidTercero_1aM = $id and TA.Compania_oidCompania = $idCompania
        where TCF.Compania_oidCompania = $idCompania
        order by txNombreTipoCertificado");


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 23)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroCertificadoForm', compact('terceroCertificado', 'id', 'permisoModificar'));
    }

    public function updateTerceroCertificado(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            //Eliminamos los archivos que dieron eliminar en la vista
            if (isset($request['eliminarArchivos'])) {
                for ($i = 0; $i < count($request['eliminarArchivos']); $i++) {
                    TerceroAdjuntoModel::where('oidTerceroAdjunto', '=', $request['eliminarArchivos'][$i])->delete();
                }
            }
            //Guardamos o actualizamos los registros que eligieron en la vista
            for ($i = 0; $i < count($request['TipoCertificado_oidTerceroCertificado']); $i++) {
                $registro = $request['TipoCertificado_oidTerceroCertificado'][$i];

                $indice = ['oidTerceroCertificado' => $request['oidTerceroCertificado'][$i]];
                $datos = [
                    'Tercero_oidTercero_1aM' => $id,
                    'TipoCertificado_oidTerceroCertificado' => $request['TipoCertificado_oidTerceroCertificado'][$i],
                    'lsVerificacionTerceroCertificado' => $request['lsVerificacionTerceroCertificado'][$i],
                    'Compania_oidCompania' => Session::get('oidCompania')
                ];
                TerceroCertificadoModel::updateOrCreate($indice, $datos);
                //Guardamos los archivos que insertaron en la vista recorriendo los adjuntos
                if (isset($request['txRutaTerceroAdjunto-' . $registro][0])) {
                    for ($j = 0; $j < count($request['txRutaTerceroAdjunto-' . $registro]); $j++) {
                        //Guardamos el archivo en el disco C
                        $origen = Storage::disk('tmp')->path($request['txRutaTerceroAdjunto-' . $registro][$j]);
                        $extension = File::extension($origen);
                        $nombre = uniqid() . "." . $extension;
                        $destino = Storage::disk('discoc')->path('asociadonegocio/terceroCertificados/' . $nombre);
                        File::move($origen, $destino);

                        //vefificamos que en el request biene un registro perteneciente al principla para obtener su valor y guardarlo  juntos
                        //en este caso la fecha de vencimiento
                        $fechaVencimiento = null;
                        if (isset($request['daFechaVencimientoTerceroAdjunto' . $registro])) {
                            for ($y = 0; $y < count($request['daFechaVencimientoTerceroAdjunto' . $registro]); $y++) {
                                $fecha = explode('#', $request['daFechaVencimientoTerceroAdjunto' . $registro][$y]);
                                if ($fecha[1] == $request['txRutaTerceroAdjunto-' . $registro][$j]) {
                                    $fechaVencimiento = $fecha[0];
                                    break;
                                }
                            }
                        }
                        //vefificamos que en el reques biene un registro perteneciente al principla para obtener su valor y guardarlo  juntos
                        //en este caso la
                        $descripcion = null;
                        if (isset($request['txDescripcionTerceroAdjunto' . $registro])) {
                            for ($y = 0; $y < count($request['txDescripcionTerceroAdjunto' . $registro]); $y++) {
                                $descrip = explode('#', $request['txDescripcionTerceroAdjunto' . $registro][$y]);
                                if ($descrip[1] == $request['txRutaTerceroAdjunto-' . $registro][$j]) {
                                    $descripcion = $descrip[0];
                                    break;
                                }
                            }
                        }
                        $indice = ['oidTerceroAdjunto' => null];
                        $datos = [
                            'Tercero_oidTercero_1aM' => $id,
                            'TipoCertificado_oidTerceroCertificado' => $registro,
                            'daFechaCreacionTerceroAdjunto' => Carbon::now(),
                            'daFechaVencimientoTerceroAdjunto' => $fechaVencimiento,
                            'txRutaTerceroAdjunto' => $nombre,
                            'txDescripcionTerceroAdjunto' => $descripcion,
                            'Compania_oidCompania' => Session::get('oidCompania')

                        ];
                        TerceroAdjuntoModel::updateOrCreate($indice, $datos);
                    }
                }
            }
            //Actualizamos quien modifico el tercero en los datos de encabezado
            TerceroModel::where('oidTercero', $id)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
            DB::commit();
            return response(['id' => $id], 200);
        } catch (\Exeption $e) {
            DB::rollback();
            return abort(500, $e->getMessaje());
        }
    }

    public function imprimirCertificado($nombreArchivo)
    {
        $destino = "/asociadonegocio/terceroCertificados/" . $nombreArchivo;
        $archivo = Storage::disk('discoc')->get($destino);
        return response($archivo)->header("Content-Type", "application/pdf/xml/xlsx/pptx");
    }

    public function editTerceroVerificacionOEA($id, $idClasificacion, $tipo)
    {
        $idCompania = Session::get('oidCompania');
        $preguntas = DB::select("SELECT
            oidPreguntaOea as Preguntas_oidTerceroVerificacion,
            inPorcentajePreguntaOea,
            txNombrePreguntaOea,
            Tercero_oidTercero_1aM,
            lsImpactoTerceroVerificacion,
            lsTipoPreguntaOea
        FROM
            asn_tercero T
                LEFT JOIN
            gen_preguntaoea P ON lsTipoPreguntaOea = SUBSTRING(lmTipoServicioTercero,
                1,
                LOCATE(',', lmTipoServicioTercero) - 1)
                OR lsTipoPreguntaOea = SUBSTRING(lmTipoServicioTercero,
                LOCATE(',', lmTipoServicioTercero) + 1)
                left join asn_terceroverificacion TV on TV.Preguntas_oidTerceroVerificacion = oidPreguntaOea and TV.Tercero_oidTercero_1aM = oidTercero and TV.Compania_oidCompania = $idCompania
        WHERE
            oidTercero = $id
        AND lmClasificacionTercero = 'Seguimiento'
        AND lsTipoPreguntaOea = '$tipo'
        and P.Compania_oidCompania = $idCompania");

        $fecha = date("Y-m-d");
        $preguntaDetalle = DB::select("SELECT * FROM gen_preguntaoeadetalle where daVigenciaPreguntaOeaDetalle >= $fecha and daVigenciaPreguntaOeaDetalle is not null");


        $verificacionOea = TerceroVerificacionOeaModel::select('dePonderacionTerceroVerificacionOea', 'txImpactoTerceroVerificacionOea')
            ->where('Tercero_oidTercero_1a1', $id)
            ->where('Compania_oidCompania', Session::get('oidCompania'))
            ->where('txTipoTereceroVerificacionOea', $tipo)
            ->first();


        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 28)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroVerificacionOeaForm', compact('verificacionOea', 'id', 'preguntas', 'permisoModificar', 'preguntaDetalle', 'tipo'));
    }

    public function updateTerceroVerificacionOEA(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            TerceroVerificacionModel::where('Tercero_oidTercero_1aM', '=', $id)
                ->where('Compania_oidCompania', '=', Session::get('oidCompania'))
                ->where('txTipoTereceroVerificacion', '=', $request["txTipoTereceroVerificacion"])
                ->delete();
            for ($i = 0; $i < count($request['Preguntas_oidTerceroVerificacion']); $i++) {
                $indice = ['oidTerceroVerificacion' => null];
                $datosReg = [
                    'Tercero_oidTercero_1aM' => $id,
                    'Preguntas_oidTerceroVerificacion' => $request["Preguntas_oidTerceroVerificacion"][$i],
                    'lsImpactoTerceroVerificacion' => $request["lsImpactoTerceroVerificacion"][$i],
                    'txTipoTereceroVerificacion' => $request["txTipoTereceroVerificacion"],
                    'Compania_oidCompania' => Session::get('oidCompania'),
                ];
                TerceroVerificacionModel::updateOrCreate($indice, $datosReg);
            }
            TerceroVerificacionOeaModel::where('Tercero_oidTercero_1a1', '=', $id)
                ->where('Compania_oidCompania', '=', Session::get('oidCompania'))
                ->where('txTipoTereceroVerificacionOea', '=', $request["txTipoTereceroVerificacion"])
                ->delete();
            TerceroVerificacionOeaModel::create([
                'oidTerceroVerificacionOea' => null,
                'Tercero_oidTercero_1a1' => $id,
                'dePonderacionTerceroVerificacionOea' => $request["dePonderacionTerceroVerificacionOea"],
                'txImpactoTerceroVerificacionOea' => $request["txImpactoTerceroVerificacionOea"],
                'txImpactoTerceroVerificacionOea' => $request["txImpactoTerceroVerificacionOea"],
                'txTipoTereceroVerificacionOea' => $request["txTipoTereceroVerificacion"], //es el mismo tipo del encabezado
                'Compania_oidCompania' => Session::get('oidCompania'),
            ]);

            //Actualizamos quien modifico el tercero en los datos de encabezado
            TerceroModel::where('oidTercero', $id)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
            DB::commit();
            return response(['id' => $id], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function consultaDocumento($documentoTercero)
    {
        $tercero = TerceroModel::select('txDocumentoTercero', 'txNombreClasificacionTercero')
            ->leftjoin('asn_terceroclasificacion', 'Tercero_oidTercero_1aM', '=', 'oidTercero')
            ->leftjoin('gen_clasificaciontercero', 'ClasificacionTercero_oidTerceroClasificacion_1aM', '=', 'oidClasificacionTercero')
            ->where('txDocumentoTercero', $documentoTercero)->get();
        if ($tercero->isNotEmpty()) {
            $nombreClasificacion = '';
            for ($i = 0; $i < count($tercero); $i++) {
                $nombreClasificacion .= $tercero[$i]->txNombreClasificacionTercero . ' , ';
            }
            return abort(400, 'El documento ingresado ya existe en el sistema con la clasificación: ' . $nombreClasificacion);
        } else {
            return response(200);
        }
    }

    //Funcion para subir imagenes al disco temporal
    public function subirCertificado(Request $request)
    {
        $imagen = $request->file('file');
        $path = Storage::disk('tmp')->putFile('asociadonegocio/terceroCertificado', $imagen);
        return response(['tmpPath' => $path], 200);
    }

    public function consultaSelectOea($idPregunta)
    {
        $fecha = date("Y-m-d");
        $preguntaDetalle = DB::select("SELECT * FROM gen_preguntaoeadetalle where PreguntaOea_oidPreguntaOea_1aM = $idPregunta AND daVigenciaPreguntaOeaDetalle >= '$fecha'");
        return $preguntaDetalle;
    }

    //CRON CORREO DE CERTIFICADOS VENCIDOS (SOLO ES DE PRUEBA PARA EJECUTARLOS EN LA RUTA)
    //EL REAL ESTA EN EL ARCHIVO (cronTercero.php)
    public function certificadoVencimiento()
    {

        $consulta = DB::select("SELECT
        daFechaVencimientoTerceroAdjunto,
        txNombreTercero,
        txNombreTipoCertificado,
        oidTercero,
        txDocumentoTercero
        FROM
        kunde.asn_terceroadjunto as  TA
        left join
        asn_tercero on TA.Tercero_oidTercero_1aM  = oidTercero
        left join
        gen_tipocertificado on TipoCertificado_oidTerceroCertificado = oidTipoCertificado
        WHERE
        daFechaVencimientoTerceroAdjunto is not null");

        Storage::disk("discoc")->put("/asociadonegocio/cronCertificado/" . uniqid() . ".txt", 'casaa');
        for ($i = 0; $i < count($consulta); $i++) {
            $tiempoActual = Carbon::now();
            $fecha = $tiempoActual->toDateString();
            $idTercero = $consulta[$i]->oidTercero;

            $correo = DB::select("SELECT
                txCorreoGrupoTercero
            FROM
                kunde.asn_tercerogrupo
                    LEFT JOIN
                asn_grupotercero ON GrupoTercero_oidTerceroGrupo = oidGrupoTercero
                    LEFT JOIN
                gen_clasificaciontercerogrupo ON ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = oidClasificacionTerceroGrupo
            WHERE
                Tercero_oidTercero_1aM = $idTercero
                AND txNombreClasificacionTerceroGrupo = 'Lider'
                AND txCorreoGrupoTercero != ''
            GROUP BY Tercero_oidTercero_1aM");

            if ($consulta[$i]->daFechaVencimientoTerceroAdjunto > $fecha && $consulta[$i]->daFechaVencimientoTerceroAdjunto <= date("Y-m-d", strtotime("+ 10 DAYS", strtotime($fecha)))) {
                $vencido = new CertificadoTerceroVencido($consulta[$i]->txNombreTercero, $consulta[$i]->daFechaVencimientoTerceroAdjunto, $consulta[$i]->txNombreTipoCertificado, $consulta[$i]->txDocumentoTercero);
                Mail::to($correo[0]->txCorreoGrupoTercero)->send($vencido);
            }
        }
    }

    public function consultaObservacionTerceroGrupo($id)
    {
        $campo = DB::select("SELECT
            chObservacionGrupoTercero
        FROM
            kunde.asn_grupotercero
        WHERE
            oidGrupoTercero = $id");
        if ($campo[0]->chObservacionGrupoTercero === 1) {
            return response(200);
        } else {
            return abort(400);
        }
    }

    public function cambiarEstadoSG($idTercero)
    {
        $terceroSG = TerceroModel::select('txEstadoSistemaGestionTercero')->where('oidTercero', $idTercero)->get();
        if ($terceroSG[0]->txEstadoSistemaGestionTercero == null) {
            TerceroModel::where('oidTercero', $idTercero)->update(['txEstadoSistemaGestionTercero' => 'Activo']);
            return response(200);
        } elseif ($terceroSG[0]->txEstadoSistemaGestionTercero == 'Inactivo') {
            TerceroModel::where('oidTercero', $idTercero)->update(['txEstadoSistemaGestionTercero' => 'Activo']);
            return response(200);
        } elseif ($terceroSG[0]->txEstadoSistemaGestionTercero == 'Activo') {
            TerceroModel::where('oidTercero', $idTercero)->update(['txEstadoSistemaGestionTercero' => 'Inactivo']);
            return response(200);
        } else {
            return response(400);
        }
    }

    public function editTerceroRazonSocial($id, $idClasificacion)
    {
        $NombreTercero = TerceroModel::where('oidTercero', $id)->select('txNombreTercero')->get();
        $fechaActual = Carbon::now()->toDateString();
        $razonSocial = TerceroRazonSocialModel::where('Tercero_oidTercero_1aM', $id)->get();

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 30)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();
        return view('asociadonegocio::TerceroRazonSocialForm', compact('id', 'NombreTercero', 'fechaActual', 'razonSocial', 'permisoModificar'));
    }

    public function updateTerceroRazonSocial(Request $request, $idPadre)
    {
        $Fecha = TerceroRazonSocialModel::where('Tercero_oidTercero_1aM', $idPadre)->select('daFechaFinTerceroRazonSocial')->orderby('oidTerceroRazonSocial', 'desc')->get();
        if ($Fecha->isEmpty()) {
            $fechaCreacion = TerceroModel::where('oidTercero', $idPadre)->select('daFechaCreacionTercero')->get();
            $fechaInicio = $fechaCreacion[0]->daFechaCreacionTercero;
        } else {
            $fechaInicio = $Fecha[0]->daFechaFinTerceroRazonSocial;
        }
        $razonSocial = TerceroRazonSocialModel::create([
            'Tercero_oidTercero_1aM' => $idPadre,
            'txNombreTerceroRazonSocial' => $request['txNombreTerceroRazonSocial-encabezado'],
            'daFechaInicioTerceroRazonSocial' => $fechaInicio,
            'daFechaFinTerceroRazonSocial' => $request['daFechaFinTerceroRazonSocial-encabezado']
        ]);
        $razonSocial->save();
        $NombreTercero = TerceroModel::where('oidTercero', $idPadre)
            ->update([
                'txNombreTercero' => $request['txNombreTercero'],
                'txPrimerNombreTercero' => $request['txPrimerNombreTercero-encabezado'],
                'txSegundoNombreTercero' => $request['txSegundoNombreTercero-encabezado'],
                'txPrimerApellidoTercero' => $request['txPrimerApellidoTercero-encabezado'],
                'txSegundoApellidoTercero' => $request['txSegundoApellidoTercero-encabezado']
            ]);
        $this->actualizarTercero($idPadre, '');
        return response(['id' => $idPadre], 200);
    }

    public function editTerceroFacturaElectronica($id, $idClasificacion)
    {

        $terceroFacturacionElectronica = TerceroFacturaElectronicaModel::where('Tercero_oidTercero_1aM', $id)->first();
        $idFactura = (isset($terceroFacturacionElectronica->oidTerceroFacturaElectronica) ? $terceroFacturacionElectronica->oidTerceroFacturaElectronica : 0);

        $responsabilidadFiscal = ResponsabilidadFiscalModel::select(DB::raw("CONCAT(txCodigoResponsabilidadFiscal,' - ',txNombreResponsabilidadFiscal) AS txNombreResponsabilidadFiscal"), 'oidResponsabilidadFiscal')
            ->pluck('txNombreResponsabilidadFiscal', 'oidResponsabilidadFiscal');
        $valorResponsabilidadFiscal = TerceroResponsabilidadFiscalModel::where('FacturaElectronica_oidTerceroFacturaElectronica_1aM', $idFactura)->pluck('ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM');

        $regimenCotable = RegimenContableModel::pluck('txNombreRegimenContable', 'oidRegimenContable');

        $cuentasBancarias = TerceroBancoModel::where('Tercero_oidTercero_1aM', $id)->pluck('txNombreCuentaTerceroBanco', 'oidTerceroBanco');

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 31)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view(
            'asociadonegocio::TerceroFacturacionElectronicaForm',
            compact('id', 'responsabilidadFiscal', 'valorResponsabilidadFiscal', 'terceroFacturacionElectronica', 'permisoModificar', 'regimenCotable', 'cuentasBancarias')
        );
    }

    public function updateTerceroFacturaElectronica(Request $request, $idPadre)
    {
        $indice = array('oidTerceroFacturaElectronica' => $request['oidTerceroFacturaElectronica']);
        $datos = array(
            'Tercero_oidTercero_1aM' => $idPadre,
            'txMatriculaMercantilTerceroFactura' => $request['txMatriculaMercantilTerceroFactura'],
            'TerceroBanco_oidFacturaElectronica' => $request['TerceroBanco_oidFacturaElectronica'],
            'RegimenContable_oidTerceroFacturaElectronica' => $request['RegimenContable_oidTerceroFacturaElectronica'],
        );
        $razonSocial = TerceroFacturaElectronicaModel::updateOrCreate($indice, $datos);

        $idFacturaElect = $razonSocial->oidTerceroFacturaElectronica;

        //GUARDAMOS LAS RESPONSABILIDADES FISCALES EN LA TABLA INTERMEDIA
        $actividad = ($request['ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM'] ? count($request['ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM']) : 0);
        TerceroResponsabilidadFiscalModel::where('FacturaElectronica_oidTerceroFacturaElectronica_1aM', $idFacturaElect)->delete();
        for ($i = 0; $i < $actividad; $i++) {
            $indice = array('oidTerceroResponsabilidadFiscal' => null);
            $datos = array(
                'FacturaElectronica_oidTerceroFacturaElectronica_1aM' => $idFacturaElect,
                'ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM' => $request['ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM'][$i]
            );
            TerceroResponsabilidadFiscalModel::updateOrCreate($indice, $datos);
        }

        return response(200);
    }

    public function generarResponsabilidad($id)
    {

        $empleado = TerceroModel::leftJoin('asn_tercerocargo as tc', 'asn_tercero.oidTercero', 'tc.Tercero_oidTercero_1aM')
            ->leftJoin('gen_cargo as c', 'tc.Cargo_oidTerceroCargo', 'c.oidCargo')
            ->where('oidTercero', $id)
            ->first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('asociadonegocio::impresionResponsabilidad', ['empleado' => $empleado]);
        return $pdf->stream();
    }

    public function indexImportarTareas()
    {
        return view('asociadonegocio::tareaImportForm');
    }

    public function cargarArchivoTareas()
    {
        $destinationPath = public_path() . '/images';
        $fileName = FacadesInput::file('file')->getClientOriginalName();
        $upload_success = FacadesInput::file('file')->move($destinationPath, $fileName);

        if ($upload_success) {
            return response(["nombreArchivo" => $fileName], 200);
        } else {
            return abort(500, 'error');
        }
    }

    public function importarTareas(Request $request)
    {
        DB::beginTransaction();
        try {
            $file = public_path() . '/images/' . $request['nombreArchivo'];
            $datos = Excel::toArray(new TareaImport, $file);
            if (!$datos) {
                throw new Exception('No se encontraron datos en la plantilla importada');
            }

            $documentoClienteActual = 0;
            foreach ($datos[0] as $pos => $dato) {
                if ($pos !== 0) {
                    $orden = $dato[0];
                    $nombreEvento = $dato[1];
                    $modalidad = $dato[2];
                    $fecha = $dato[3];
                    $hora = $dato[4];
                    $documentoCliente = $dato[5];

                    if ($documentoClienteActual != $documentoCliente) {
                        $terceroCliente = $this->buscarCliente($documentoCliente);
                        $terceroPeriodicidad = new TerceroPeriodicidadModel();
                        $terceroPeriodicidad->lsPeriodicidadTerceroPeriodicidad = 'semanal';
                        $terceroPeriodicidad->dtFechaInicioTerceroPeriodicidad = $fecha;
                        $terceroPeriodicidad->hrHoraInicioPeriodicidadTerceroPeriodicidad = $hora;
                        $terceroPeriodicidad->inHorasTerceroPeriodicidad = '8';
                        $terceroPeriodicidad->Tercero_oidTercero = $terceroCliente->oidTercero;
                        $terceroPeriodicidad->inHorasConsumidasTerceroPeriodicidad = '8';
                        $terceroPeriodicidad->save();
                    }

                    if ($modalidad == '') {
                        throw new Exception("La modalidad de la reunión es obligatoria");
                    }

                    $evento = $this->buscarEvento($nombreEvento);

                    $terceroEventoPeriodicidad = new TerceroEventoPeriodicidadModel();
                    $terceroEventoPeriodicidad->Evento_oidEvento = $evento->oidEvento;
                    $terceroEventoPeriodicidad->TerPerio_oidTerPerio = $terceroPeriodicidad->oidTerceroPeriodicidad;
                    $terceroEventoPeriodicidad->inOrdernTerceroEventoPeriodicidad = $orden;
                    $terceroEventoPeriodicidad->dtFechaEstimadaTerceroEventoPeriodicidad = $fecha;
                    $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad = $hora;
                    $terceroEventoPeriodicidad->lsModalidadTerceroPeriodicidad = $modalidad;
                    $terceroEventoPeriodicidad->save();

                    $documentoClienteActual = $documentoCliente;
                    $this->crearTareaCalendario($terceroEventoPeriodicidad, $terceroCliente, $evento);
                }
            }
            DB::commit();
            return response(["message" => 'Tareas cargadas'], Response::HTTP_CREATED);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["message" => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function buscarEvento($nombreEvento)
    {
        $evento = EventosModel::where('txNombreEvento', $nombreEvento)->first();

        if (!$evento) {
            throw new Exception("No se encontró el evento $nombreEvento");
        }
        return $evento;
    }

    public static function buscarCliente($documentoCliente)
    {
        $terceroCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->where('txDocumentoTercero', $documentoCliente)
            ->first();

        if (!$terceroCliente) {
            throw new Exception("No se encontró un cliente con el documento $documentoCliente");
        }
        return $terceroCliente;
    }

    public static function crearTareaCalendario($terceroEventoPeriodicidad, $tercero, $evento)
    {
        $fecha = $terceroEventoPeriodicidad->dtFechaEstimadaTerceroEventoPeriodicidad . " " . $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad;
        $fechaJuliana = strtotime($fecha) * 1000;

        $fechaFinal = strtotime("+ 60 minute", strtotime($fecha));
        $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
        $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

        $terceroResponsable = TerceroModel::where('txDocumentoTercero', '1036653146')->first();

        $asunto = "$evento->txNombreEvento - $tercero->txNombreTercero";

        $cronograma = new CronogramaEspacioModel();
        $cronograma->txAsuntoCronogramaEspacios = $asunto;
        $cronograma->dtFechaCronogramaEspacios = $terceroEventoPeriodicidad->dtFechaEstimadaTerceroEventoPeriodicidad;
        $cronograma->hrHoraCronogramaEspacios = $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad;
        $cronograma->txFechaJulianoCronogramaEspacios = $fechaJuliana;
        $cronograma->txFechaFinalJulianoCronogramaEspacios = $fechaFinalJuliana;
        $cronograma->Evento_oidEvento = $terceroEventoPeriodicidad->Evento_oidEvento;
        $cronograma->Tercero_idCliente = $tercero->oidTercero;
        $cronograma->chProgramadoCronogramaEspacios = 1;
        $cronograma->lsModalidadTerceroPeriodicidad = $terceroEventoPeriodicidad->lsModalidadTerceroPeriodicidad;
        $cronograma->dtHoraEstimadaTerceroEventoPeriodicidad = $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad;
        $cronograma->save();

        $f = $terceroEventoPeriodicidad->dtFechaEstimadaTerceroEventoPeriodicidad;
        $h = $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad;
        $d = 60;
        // $id = $terceroResponsable->oidTercero;

        $intInitial = strtotime($f . " " . $h);
        $horai = date('H:i:s', $intInitial);

        $intFinal = strtotime("+" . $d . " minute", strtotime($horai));
        $hFinal = date('H:i:s', $intFinal);

        // $task_exists = ReunionModel::valitdateTimeTask($id, $horai, $hFinal, $f);
        // if (!empty($task_exists)) {
        //     return abort(500, 'Ya tienes la tarea ' .  $task_exists[0]->txAsuntoReunion . ' a las ' . $task_exists[0]->hrHoraReunion . ' para esta fecha.');
        // }

        if ($tercero->txUrl == '') {
            throw new Exception("El cliente $tercero->txNombreTercero no tiene un link asociado por defecto");
        }

        $reunion = new ReunionModel();
        $reunion->txAsuntoReunion = $asunto;
        $reunion->txTipoReunion = $terceroEventoPeriodicidad->lsModalidadTerceroPeriodicidad;
        $reunion->dtFechaReunion = $terceroEventoPeriodicidad->dtFechaEstimadaTerceroEventoPeriodicidad;
        $reunion->hrHoraReunion = $terceroEventoPeriodicidad->dtHoraEstimadaTerceroEventoPeriodicidad;
        $reunion->txFechaJulianoReunion = $fechaJuliana;
        $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
        $reunion->txUbicacionReunion = $tercero->txUrl;
        $reunion->Tercero_oidResponsable = $terceroResponsable->oidTercero;
        $reunion->txDescripcionReunion = '';
        $reunion->Evento_oidEvento = $terceroEventoPeriodicidad->Evento_oidEvento;
        $reunion->chEstadoReunion = 0;
        $reunion->txDescripcionEstadoReunion = '';
        $reunion->intDuracionReunion = 60;
        $reunion->Tercero_idCliente = $tercero->oidTercero;
        $reunion->txEstadoReunion = "Terminado";
        $reunion->hrHoraFinalReunion = $hFinal;

        $reunion->save();
    }

    // METODO PARA TERCERO INTEGRANTE COMITE----------------------------------------------------------
    public function editTerceroIntegranteComite($id, $idClasificacion)
    {
        // consultar si ya existen datos para mostrarlos en la multi registro Copasst
        $asn_tercerocopasst = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroIntegranteComite', '=', 'Copasst')
            ->select(DB::raw('oidTerceroIntegrantesComite as oidTerceroCopasst, txNombreTerceroIntegranteComite as txNombreTerceroCopasst, txTipoTerceroIntegranteComite as txTipoTerceroCopasst, inDocumentoTerceroIntegrantesComite as inDocumentoTerceroCopasst'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Cocola
        $asn_tercerococola = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroIntegranteComite', '=', 'Cocola')
            ->select(DB::raw('oidTerceroIntegrantesComite as oidTerceroCocola, txNombreTerceroIntegranteComite as txNombreTerceroCocola, txTipoTerceroIntegranteComite as txTipoTerceroCocola, inDocumentoTerceroIntegrantesComite as inDocumentoTerceroCocola'))
            ->get();

        // consultar si ya existen datos para mostrarlos en la multi registro de Convivencia
        $asn_terceroconvivencia = TerceroIntegranteComiteModel::where('Tercero_oidTercero_1aM', '=', $id)
            ->where('txTipoTerceroIntegranteComite', '=', 'Convivencia')
            ->select(DB::raw('oidTerceroIntegrantesComite as oidTerceroConvivencia, txNombreTerceroIntegranteComite as txNombreTerceroConvivencia, txTipoTerceroIntegranteComite as txTipoTerceroConvivencia, inDocumentoTerceroIntegrantesComite as inDocumentoTerceroConvivencia'))
            ->get();

        $rolUsuario = Session::get('rolUsuario');
        $permisoModificar = ClasificacionTerceroConfigModel::select('chModificarClasificacionTerceroRol')
            ->leftjoin('gen_modulotercero', 'ModuloTercero_ClasificacionTerceroConfig', '=', 'oidModuloTercero')
            ->leftjoin('gen_clasificaciontercerorol', 'ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM', '=', 'oidClasificacionTerceroConfig')
            ->leftjoin('seg_rol', 'Rol_oidClasificacionTerceroRol', '=', 'oidRol')
            ->where(function ($opcion) use ($rolUsuario, $idClasificacion) {
                $opcion->where('oidModuloTercero', '=', 20)
                    ->Where('oidRol', '=', $rolUsuario)
                    ->Where('ClasificacionTercero_oidClasificacionTercero_1aM', '=', $idClasificacion);
            })
            ->first();

        return view('asociadonegocio::TerceroIntegranteComiteForm', compact('asn_tercerocopasst', 'id', 'asn_tercerococola', 'asn_terceroconvivencia', 'permisoModificar'));
    }

    public function updateTerceroIntegranteComite(TerceroIntegranteComiteRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $sufijos = ['Copasst', 'Convivencia', 'Cocola'];
            foreach ($sufijos as $sufijo) {
                $this->grabarIntegrantesComites($request, $id, $sufijo);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
        return response(200);
    }

    public function grabarIntegrantesComites($request, $idPadre, $sufijo)
    {
        $idsEliminar = explode(',', $request['eliminarAsn_Tercero' . $sufijo]);
        TerceroIntegranteComiteModel::whereIn('oidTerceroIntegrantesComite', $idsEliminar)->delete();
        $ids = $request['oidTercero' . $sufijo];
        if ($ids) {
            for ($i = 0; $i < count($request['oidTercero' . $sufijo]); $i++) {
                $indice = array('oidTerceroIntegrantesComite' => $request['oidTercero' . $sufijo][$i]);
                $datos = array(
                    'Tercero_oidTercero_1aM' => $idPadre,
                    'txNombreTerceroIntegranteComite' => $request['txNombreTercero' . $sufijo][$i],
                    'txTipoTerceroIntegranteComite' => $request['txTipoTercero' . $sufijo][$i],
                    'inDocumentoTerceroIntegrantesComite' => $request['inDocumentoTercero' . $sufijo][$i]
                );
                TerceroIntegranteComiteModel::updateOrCreate($indice, $datos);
            }
        }
        //Actualizamos quien modifico el tercero en los datos de encabezado
        TerceroModel::where('oidTercero', $idPadre)->update(['Usuario_oidModificador' => Auth::id(), 'daFechaModificacionTercero' => Carbon::now()->toDateString()]);
    }
}
