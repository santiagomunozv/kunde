<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsociadoNegocio\Entities\TerceroAdjuntoDocumentoModel;
use DB;
use Modules\AsociadoNegocio\Http\Requests\TerceroAdjuntoDocumentoRequest;
use Modules\General\Entities\TipoDocumentoModel;

class TerceroAdjuntoDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $terceroadjuntodocumento = TerceroAdjuntoDocumentoModel::select("oidTerceroAdjuntoDocumento", "Tercero_oidTercero", "TipoDocumento_oidTipoDocumento", "txDescripcionTerceroAdjuntoDocumento", "daFechaTerceroAdjuntoDocumento", "txRutaTerceroAdjuntoDocumento")->get();
        $oidTipoDocumento = TipoDocumentoModel::pluck('oidTipoDocumento');
        $txNombreTipoDocumento = TipoDocumentoModel::pluck('txNombreTipoDocumento');
        return view('asociadonegocio::terceroAdjuntoDocumentoForm', compact('terceroadjuntodocumento', 'oidTipoDocumento', 'txNombreTipoDocumento'));
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(TerceroAdjuntoDocumentoRequest $request)
    {
        DB::beginTransaction();
        try{
            $modelo = new TerceroAdjuntoDocumentoModel;
            $idsEliminar = explode(',', $request['eliminarTerceroAdjuntoDocumento']);
            $modelo::whereIn('oidTerceroAdjuntoDocumento',$idsEliminar)->delete();
            for($i = 0; $i <  count($request['oidTerceroAdjuntoDocumento']); $i++)
            {
                $indice = array('oidTerceroAdjuntoDocumento' => $request['oidTerceroAdjuntoDocumento'][$i]);
                $datos = [
                        'Tercero_oidTercero'=>$request["Tercero_oidTercero"][$i],
                        'TipoDocumento_oidTipoDocumento'=>$request["TipoDocumento_oidTipoDocumento"][$i],
                        'txDescripcionTerceroAdjuntoDocumento'=>$request["txDescripcionTerceroAdjuntoDocumento"][$i],
                        'daFechaTerceroAdjuntoDocumento'=>$request["daFechaTerceroAdjuntoDocumento"][$i],
                        'txRutaTerceroAdjuntoDocumento'=>$request["txRutaTerceroAdjuntoDocumento"][$i],
                    ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }
            DB::commit();
            return response(['oidTerceroAdjuntoDocumento' => $guardar->oidTerceroAdjuntoDocumento] , 200);
        }catch(\Exception $e){

            DB::rollback();
            return response($e->getMessage());
        }
    }
   
}
