<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use App\Services\EmailService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Modules\AsociadoNegocio\Entities\TerceroEventoPeriodicidadModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\AsociadoNegocio\Entities\TerceroPeriodicidadModel;
use Modules\AsociadoNegocio\Http\Requests\ConfiguracionEvento\StoreRequest;
use Modules\AsociadoNegocio\Http\Requests\ConfiguracionEvento\UpdateRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroEventoPeriodicidadRequest;
use Modules\AsociadoNegocio\Http\Requests\TerceroPeriodicidadRequest;
use Modules\Reunion\Entities\CronogramaEspacioModel;
use Modules\Reunion\Entities\EventosModel;

class ConfiguracionEventoController extends Controller
{
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $id = TerceroPeriodicidadModel::create($data)->oidTerceroPeriodicidad;

        if (isset($data['oidTerceroEventoPeriodicidad'])) {
            for ($i = 0; $i <  count($request['oidTerceroEventoPeriodicidad']); $i++) {
                TerceroEventoPeriodicidadModel::create([
                    'TerPerio_oidTerPerio' => $id,
                    'Evento_oidEvento' => $data['Evento_oidEvento'][$i],
                    'inOrdernTerceroEventoPeriodicidad' => $data['inOrdernTerceroEventoPeriodicidad'][$i],
                    'dtFechaEstimadaTerceroEventoPeriodicidad' => $data['dtFechaEstimadaTerceroEventoPeriodicidad'][$i],
                    'dtHoraEstimadaTerceroEventoPeriodicidad' => $data['dtHoraEstimadaTerceroEventoPeriodicidad'][$i],
                    'lsModalidadTerceroPeriodicidad' => $data['lsModalidadTerceroPeriodicidad'][$i],
                ]);
            }
        }
        if ($data['grabar']) {
            $this->generateCronograma($request['Tercero_oidTercero']);
        } else {
            $this->simulateCronograma($request);
        }

        return response(['msn' => 'se creo correctamente'], 200);
    }

    public function edit($idTercero, $id)
    {
        $optionsPeriodicidad = ['diaria' => 'Diaria', 'semanal' => 'Semanal', 'quincenal' => 'Quincenal', 'mensual' => 'Mensual', 'bimestral' => 'Bimestral', 'trimestral' => 'Timestral', 'semestral' => 'Semestral', 'anual' => 'Anual'];
        $optionsDiaInicio = ['lunes' => 'Lunes', 'martes' => 'Martes', 'miercoles' => 'Miércoles', 'jueves' => 'Jueves', 'viernes' => 'Viernes'];

        $tercero = TerceroModel::getById($idTercero);
        $terceroPeriodicidad = TerceroPeriodicidadModel::where('Tercero_oidTercero', $idTercero)->first();
        $eventos = EventosModel::where('txAnioEvento', Carbon::now()->format('Y'))->get();
        $eventosId = $eventos->pluck('oidEvento');
        $eventosNombre = $eventos->pluck('txNombreEvento');

        if (isset($terceroPeriodicidad->oidTerceroPeriodicidad)) {
            $terceroEvtPeriodicidad = TerceroEventoPeriodicidadModel::where('TerPerio_oidTerPerio', $terceroPeriodicidad->oidTerceroPeriodicidad)->orderBy('inOrdernTerceroEventoPeriodicidad', 'ASC')->get();
        } else {
            $terceroEvtPeriodicidad = [];
        }

        return view('asociadonegocio::ConfigEventoForm', compact('idTercero', 'tercero', 'optionsPeriodicidad', 'optionsDiaInicio', 'terceroPeriodicidad', 'terceroEvtPeriodicidad', 'eventosId', 'eventosNombre'));
    }

    public function update(UpdateRequest $request, TerceroPeriodicidadModel $evento)
    {
        $data = $request->validated();
        $periodicidad = new TerceroEventoPeriodicidadModel();

        $evento->fill($data)->save();

        if ($data['eliminarEvento']) {
            $idsEliminar = explode(',', $data['eliminarEvento']);
            $periodicidad::whereIn('oidTerceroEventoPeriodicidad', $idsEliminar)->delete();
        }

        if (array_key_exists('oidTerceroEventoPeriodicidad', $data)) {
            $total = count($data['oidTerceroEventoPeriodicidad']);
            for ($i = 0; $i < $total; $i++) {
                $periodicidad::updateOrCreate(
                    [
                        'oidTerceroEventoPeriodicidad' => $data['oidTerceroEventoPeriodicidad'][$i]
                    ],
                    [
                        'Evento_oidEvento' => $data['Evento_oidEvento'][$i],
                        'TerPerio_oidTerPerio' => $evento->oidTerceroPeriodicidad,
                        'inOrdernTerceroEventoPeriodicidad' => $data['inOrdernTerceroEventoPeriodicidad'][$i],
                        'dtFechaEstimadaTerceroEventoPeriodicidad' => $data['dtFechaEstimadaTerceroEventoPeriodicidad'][$i],
                        'dtHoraEstimadaTerceroEventoPeriodicidad' => $data['dtHoraEstimadaTerceroEventoPeriodicidad'][$i],
                        'lsModalidadTerceroPeriodicidad' => $data['lsModalidadTerceroPeriodicidad'][$i],
                    ]
                );
            }
        }

        if ($data['grabar']) {
            $this->generateCronograma($request['Tercero_oidTercero']);
        } else {
            $this->simulateCronograma($request);
        }

        return response(['msn' => 'se creo correctamente'], 200);
    }

    private function simulateCronograma($request)
    {
        $cronogramas = TerceroPeriodicidadModel::join('asn_terceroeventoperiodicidad', 'asn_terceroperiodicidad.oidTerceroPeriodicidad', 'asn_terceroeventoperiodicidad.TerPerio_oidTerPerio')
            ->join('reu_evento', 'asn_terceroeventoperiodicidad.Evento_oidEvento', 'reu_evento.oidEvento')
            ->where('Tercero_oidTercero', $request['Tercero_oidTercero'])
            ->orderBy('inOrdernTerceroEventoPeriodicidad', 'ASC')
            ->get();

        $fechaPeriodicidad = '';
        foreach ($cronogramas as $pos => $dataCronograma) {
            if ($pos == 0) {
                $fechaReunion = $dataCronograma->dtFechaInicioTerceroPeriodicidad;
            } else {
                $addDate = $this->resolvePeriocidad($dataCronograma->lsPeriodicidadTerceroPeriodicidad);
                $fechaReunion =  strtotime("+ $addDate", strtotime($fechaPeriodicidad));
                $fechaReunion = date('Y-m-d', $fechaReunion);
            }

            $reg = TerceroEventoPeriodicidadModel::where('oidTerceroEventoPeriodicidad', $dataCronograma->oidTerceroEventoPeriodicidad)->first();
            $reg->dtFechaEstimadaTerceroEventoPeriodicidad = $fechaReunion;
            $reg->dtHoraEstimadaTerceroEventoPeriodicidad = $dataCronograma->hrHoraInicioPeriodicidadTerceroPeriodicidad;
            $reg->save();
            $fechaPeriodicidad = $fechaReunion;
        }
    }

    private function generateCronograma($terceroId)
    {
        $cronogramas = TerceroPeriodicidadModel::join('asn_terceroeventoperiodicidad', 'asn_terceroperiodicidad.oidTerceroPeriodicidad', 'asn_terceroeventoperiodicidad.TerPerio_oidTerPerio')
            ->join('reu_evento', 'asn_terceroeventoperiodicidad.Evento_oidEvento', 'reu_evento.oidEvento')
            ->where('Tercero_oidTercero', $terceroId)
            ->orderBy('inOrdernTerceroEventoPeriodicidad', 'ASC')
            ->get();

        DB::delete("DELETE FROM reu_cronogramaespacios WHERE Tercero_idCliente = $terceroId");

        $fechaPeriodicidad = '';
        foreach ($cronogramas as $pos => $dataCronograma) {
            if ($pos == 0) {
                if ($dataCronograma->dtFechaEstimadaTerceroEventoPeriodicidad != "") {
                    $fecha = $dataCronograma->dtFechaEstimadaTerceroEventoPeriodicidad . " " . $dataCronograma->dtHoraEstimadaTerceroEventoPeriodicidad;
                    $dataCronograma->dtFechaInicioTerceroPeriodicidad = $dataCronograma->dtFechaEstimadaTerceroEventoPeriodicidad;
                    $dataCronograma->save();
                } else {
                    $fecha = $dataCronograma->dtFechaInicioTerceroPeriodicidad . " " . $dataCronograma->dtHoraEstimadaTerceroEventoPeriodicidad;
                }
                $fechaJuliana = strtotime($fecha) * 1000;
                $fechaFinal = strtotime("+ 60 minute", strtotime($fecha));
                $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
                $fechaFinalJuliana = strtotime($fechaFinal) * 1000;
                $fechaReunion = $dataCronograma->dtFechaInicioTerceroPeriodicidad;
            } else {
                $addDate = $this->resolvePeriocidad($dataCronograma->lsPeriodicidadTerceroPeriodicidad);
                if ($dataCronograma->dtFechaEstimadaTerceroEventoPeriodicidad != "") {
                    $fechaReunion = $dataCronograma->dtFechaEstimadaTerceroEventoPeriodicidad;
                } else {
                    $fechaReunion =  strtotime("+ $addDate", strtotime($fechaPeriodicidad));
                    $fechaReunion = date('Y-m-d', $fechaReunion);
                }

                $fecha = $fechaReunion . " " . $dataCronograma->dtHoraEstimadaTerceroEventoPeriodicidad;
                $fechaJuliana = strtotime($fecha) * 1000;
                $fechaFinal = strtotime("+ 60 minute", strtotime($fecha));
                $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
                $fechaFinalJuliana = strtotime($fechaFinal) * 1000;
            }

            $reg = TerceroEventoPeriodicidadModel::where('oidTerceroEventoPeriodicidad', $dataCronograma->oidTerceroEventoPeriodicidad)->first();
            $reg->dtFechaEstimadaTerceroEventoPeriodicidad = $fechaReunion;
            $reg->save();

            $tercero = TerceroModel::find($terceroId);
            $evento = EventosModel::find($dataCronograma->Evento_oidEvento);

            $cronograma = new CronogramaEspacioModel();
            $cronograma->txAsuntoCronogramaEspacios = "$evento->txNombreEvento - $tercero->txNombreTercero";
            $cronograma->dtFechaCronogramaEspacios = $fechaReunion;
            $cronograma->hrHoraCronogramaEspacios = $dataCronograma->dtHoraEstimadaTerceroEventoPeriodicidad;
            $cronograma->txFechaJulianoCronogramaEspacios = $fechaJuliana;
            $cronograma->txFechaFinalJulianoCronogramaEspacios = $fechaFinalJuliana;
            $cronograma->Evento_oidEvento = $dataCronograma->Evento_oidEvento;
            $cronograma->Tercero_idCliente = $terceroId;
            $cronograma->chProgramadoCronogramaEspacios = 0;
            $cronograma->lsModalidadTerceroPeriodicidad = $dataCronograma->lsModalidadTerceroPeriodicidad;
            $cronograma->dtHoraEstimadaTerceroEventoPeriodicidad = $dataCronograma->dtHoraEstimadaTerceroEventoPeriodicidad;
            $cronograma->save();
            $fechaPeriodicidad = $fechaReunion;
        }
    }

    private function resolvePeriocidad($periodicidad)
    {
        switch ($periodicidad) {
            case 'diaria':
                return ' 1 day';
                break;

            case 'semanal':
                return ' 7 day';
                break;

            case 'quincenal':
                return ' 14 day';
                break;

            case 'mensual':
                return ' 1 months';
                break;

            case 'bimestral':
                return ' 2 months';
                break;

            case 'trimestral':
                return ' 3 months';
                break;

            case 'semestral':
                return ' 6 months';
                break;

            case 'anual':
                return ' 12 months';
                break;

            default:
                return ' 1 day';
                break;
        }
    }

    public function generarPlanTrabajoTercero($idTercero)
    {
        $planTrabajo = DB::table('asn_terceroeventoperiodicidad as tep')
            ->join('asn_terceroperiodicidad as tp', 'tep.TerPerio_oidTerPerio', '=', 'tp.oidTerceroPeriodicidad')
            ->join('reu_evento as re', 'tep.Evento_oidEvento', '=', 're.oidEvento')
            ->leftJoin('reu_evento_asistente as rea', 're.oidEvento', '=', 'rea.Evento_oidEvento')
            ->select(
                're.txNombreEvento as tema',
                DB::raw('GROUP_CONCAT(DISTINCT rea.txAsistenteEventoAsistente) as participantes'),
                'tep.lsModalidadTerceroPeriodicidad as modalidad',
                'tep.dtFechaEstimadaTerceroEventoPeriodicidad as fecha',
                'tep.dtHoraEstimadaTerceroEventoPeriodicidad as hora',
                're.txColorEvento'
            )
            ->where('tp.Tercero_oidTercero', $idTercero)
            ->groupBy('tep.oidTerceroEventoPeriodicidad')
            ->get();

        // $pdf = App::make('dompdf.wrapper');
        return view('asociadonegocio::planTrabajo', ['planTrabajo' => $planTrabajo]);
        // return $pdf->stream();
    }

    public function enviarNotificacionPlanTrabajoTercero($idTercero, EmailService $emailService)
    {
        $tercero = TerceroModel::find($idTercero);

        $planTrabajo = DB::table('asn_terceroeventoperiodicidad as tep')
            ->join('asn_terceroperiodicidad as tp', 'tep.TerPerio_oidTerPerio', '=', 'tp.oidTerceroPeriodicidad')
            ->join('reu_evento as re', 'tep.Evento_oidEvento', '=', 're.oidEvento')
            ->leftJoin('reu_evento_asistente as rea', 're.oidEvento', '=', 'rea.Evento_oidEvento')
            ->select(
                're.txNombreEvento as tema',
                DB::raw('GROUP_CONCAT(DISTINCT rea.txAsistenteEventoAsistente) as participantes'),
                'tep.lsModalidadTerceroPeriodicidad as modalidad',
                'tep.dtFechaEstimadaTerceroEventoPeriodicidad as fecha',
                'tep.dtHoraEstimadaTerceroEventoPeriodicidad as hora',
                're.txColorEvento'
            )
            ->where('tp.Tercero_oidTercero', $idTercero)
            ->groupBy('tep.oidTerceroEventoPeriodicidad')
            ->get();

        $mensaje = "Buen día $tercero->txNombreTercero, a continuación  te enviamos el plan de trabajo programado con Kunde <br><br>";

        $mensaje .= view('asociadonegocio::planTrabajo', ['planTrabajo' => $planTrabajo])->render();
        $destinatarios = $tercero->txCorreoElectronicoTercero;
        $asunto = 'Plan de trabajo Kunde';

        $emailService->sendEmail($destinatarios, $asunto, $mensaje);

        return $idTercero;
    }
}
