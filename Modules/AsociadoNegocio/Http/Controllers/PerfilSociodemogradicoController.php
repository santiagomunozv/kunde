<?php

namespace Modules\AsociadoNegocio\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\GrupoFamiliarEmpleadoModel;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;
use Modules\AsociadoNegocio\Http\Requests\TerceroEmpleadoRequest;
use Modules\General\Entities\ArlModel;
use Modules\General\Entities\CargoModel;
use Modules\General\Entities\CiudadModel;
use Modules\General\Entities\DepartamentoModel;
use Modules\General\Entities\PaisModel;

class PerfilSociodemogradicoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($idEmpleado)
    {
        // $permisos = $this->consultarPermisos();
        // if(!$permisos){
        //     abort(401);
        // }

        $empleado = DB::table('asn_terceroempleado')
            ->join('asn_tercero', 'asn_tercero.oidTercero', '=', 'asn_terceroempleado.Tercero_oidTercero')
            ->where('oidTerceroEmpleado', $idEmpleado)->get()->first();
        // dd($empleado->lsIngresosEmpleado);
        // Pais
        $Pais_empleadoTercero = PaisModel::orderBy("txNombrePais")->pluck("txNombrePais", "oidPais");
        // Ciudad
        $Ciudad_empleadoTercero = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");
        // ARL
        $EmpleadoARL = ArlModel::orderBy('txNombreArl')->pluck('txNombreArl', 'oidArl');
        // Cargo
        $terceroCargo = CargoModel::orderBy("txNombreCargo")->pluck("txNombreCargo", "oidCargo");

        $gruposFamiliares = GrupoFamiliarEmpleadoModel::where('Empleado_oidFamiliarEmpleado', '=', $idEmpleado)->get();
            
        // ingresos
        $ingresos = [
            0 => '1 SMLV', 1 => '2-3 SMLV', 2 => '6-7 SMLV', 3 => 'Más de 7 SMLV'
        ];

        return view('asociadonegocio::PerfilSociodemograficoForm', compact('empleado', 'idEmpleado',  'Pais_empleadoTercero', 'Ciudad_empleadoTercero', 'EmpleadoARL', 'terceroCargo', 'gruposFamiliares', 'ingresos'));
    }

    public function update(TerceroEmpleadoRequest $request, $id)
    {
        // dd($request->all());
        DB::beginTransaction();

        try {
            $empleado = TerceroEmpleadoModel::find($id);
            $empleado->fill($request->all());
            $empleado->save();






            $this->GuardarGrupoFamiliar($request, $id, 'eliminarComFamiliar');
            DB::commit();
            return response(['oidTerceroEmpleado' => $empleado->oidTerceroEmpleado], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function GuardarGrupoFamiliar($request, $id, $datosEliminar)
    {
        $idFamiliar = $request['ClasificacionTercero_oidTerceroContacto'];
        $modelo = new GrupoFamiliarEmpleadoModel();

        $datosConteo = isset($request[$modelo->getKeyName()]) ? count($request[$modelo->getKeyName()]) : 0;

        for ($i = 0; $i < $datosConteo; $i++) {
            $indice = array($modelo->getKeyName() => $request[$modelo->getKeyName()][$i]);

            $datos = [
                'Empleado_oidFamiliarEmpleado' => $id,
                'lsParentescoFamiliar' => $request['lsParentescoFamiliar'][$i],
                'txCelularFamiliar' => $request['txCelularFamiliar'][$i],
                'txNombreFamiliar' => $request['txNombreFamiliar'][$i],
            ];
            $guardar = $modelo::updateOrCreate($indice, $datos);
        }
    }

    public function consultarDepartamento($idPais)
    {
    }

    public function consultarCiudadEmpleado($id)
    {
        $terceroCiudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', $id)->pluck("txNombreCiudad", "oidCiudad");
        return response($terceroCiudad, 200);

        // $terceroCiudad = DepartamentoModel::where('Pais_oidDepartamento', '=', $id)->pluck("txNombreDepartamento", "oidDepartamento");

        return response($terceroCiudad, 200);
    }
}
