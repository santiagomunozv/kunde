<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class TerceroPeriodicidadRequest extends FormRequest
{
    public function rules()
    {
        return [
            "oidTerceroPeriodicidad" => "nullable",
            "Tercero_oidTercero" => "nullable",
            "lsPeriodicidadTerceroPeriodicidad" => "required",
            "dtFechaInicioTerceroPeriodicidad" => "required",
            "inHorasTerceroPeriodicidad" => "required",
            "hrHoraInicioPeriodicidadTerceroPeriodicidad" => "required",
            "oidTerceroEventoPeriodicidad" => "nullable",
            "Evento_oidEvento" => "nullable",
            "eliminarEvento" => "nullable"
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'lsPeriodicidadTerceroPeriodicidad.required' => 'El campo Periodicidad es obligatorio.',
            'dtFechaInicioTerceroPeriodicidad.required' => 'El campo Fecha inicio es obligatorio.',
            'hrHoraInicioPeriodicidadTerceroPeriodicidad.required' => 'El campo Hora estimada inicio es obligatorio.',
            'inHorasTerceroPeriodicidad.required' => 'El campo Horas es obligatorio.',
        ];
    }
}
