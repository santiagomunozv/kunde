<?php

        namespace Modules\AsociadoNegocio\Http\Requests;
        
        use Illuminate\Foundation\Http\FormRequest;
        
        class TerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
         
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                         "TipoIdentificacion_oidTercero" => "required",
                         "txDocumentoTercero" => "numeric|required|unique:asn_tercero,txDocumentoTercero,".$this->get('oidTercero').",oidTercero",  
                         "txCodigoTercero" => "required|unique:asn_tercero,txCodigoTercero,".$this->get('oidTercero') .",oidTercero",
                        //  "txPrimerNombreTercero" => "string|required",
                        //  "txPrimerApellidoTercero" => "string|required",
                         "txNombreTercero" => "string|required",
                         "txDireccionTercero" => "required",
                         "txMovilTercero" => "required",
                         "txEstadoTercero" => "required",
                         "Ciudad_oidTercero" => "required",
                         "txTelefonoTercero" => "required",

                        
                );
                return $validacion;
            }
    
    
            public function messages()
            {
                $mensaje = array();
                $mensaje["TipoIdentificacion_oidTercero.required"] = "El campo tipo Identificación es obligatorio.";
				$mensaje["txDocumentoTercero.required"] =  "El campo Documento es obligatorio.";
                $mensaje["txDocumentoTercero.unique"] =  "El valor ingresado en el campo Documento ya existe, éste debe ser único";
                $mensaje["txDocumentoTercero.numeric"] =  "El campo Documento solo puede contener numeros";
                $mensaje["txCodigoTercero.required"] =  "El campo Código o contrato es obligatorio";
                $mensaje["txCodigoTercero.unique"] = "El valor ingresado en el campo Código ya existe, éste debe ser único";
                $mensaje["txCodigoTercero.numeric"] = "El valor ingresado en el campo Código debe ser un dato numerico";
                // $mensaje["txPrimerNombreTercero.string"] = "El campo Primer Nombre Solo puede contener caracteres afabéticos";
                // $mensaje["txPrimerNombreTercero.required"] = "El campo Primer Nombre es obligatorio";
                // $mensaje["txPrimerApellidoTercero.string"] =  "El campo Primer Apellido Solo puede contener caracteres afabéticos";
                // $mensaje["txPrimerApellidoTercero.required"] =  "El campo Primer Apellido es obligatorio";
                $mensaje["txNombreTercero.string"] =  "El campo Razón Social Solo puede contener caracteres afabéticos";
                $mensaje["txNombreTercero.required"] =  "El campo Razón Social es obligatorio";
                $mensaje["txDireccionTercero.required"] =  "El campo Dirección es obligatorio";
                $mensaje["txMovilTercero.required"] =  "El campo Móvil es obligatorio";
                $mensaje["txEstadoTercero.required"] =  "El campo Estado es obligatorio";
                $mensaje["Ciudad_oidTercero.required"] =  "El campo Ciudad es obligatorio";
                $mensaje["txTelefonoTercero.required"] =  "El campo teléfono es obligatorio";
                return $mensaje;
            }
        }
        