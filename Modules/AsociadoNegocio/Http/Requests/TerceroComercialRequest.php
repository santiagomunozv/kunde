<?php

        namespace Modules\AsociadoNegocio\Http\Requests;
        
        use Illuminate\Foundation\Http\FormRequest;
        
        class TerceroComercialRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
         
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                         "lsRegimenVentaTerceroTributario" => "required",
                         "ClasificacionRenta_oidTerceroTributario" => "required",
                        

                        
                );
                return $validacion;
            }
    
    
            public function messages()
            {
                $mensaje = array();
                $mensaje["lsRegimenVentaTerceroTributario.required"] = "El campo Régimen es obligatorio.";
				$mensaje["ClasificacionRenta_oidTerceroTributario.required"] =  "El campo Clasificación es obligatorio.";
                return $mensaje;
            }
        }
        