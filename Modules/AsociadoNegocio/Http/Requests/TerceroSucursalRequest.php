<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TerceroSucursalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
          'txCodigoTerceroSucursal.*' => 'required',
          'txNombreComercialTerceroSucursal.*' => 'required|string',
          'Ciudad_oidTerceroSucursal.*' => 'required',
          'txTelefono1TerceroSucursal.*' => 'required',
          'txDireccionTerceroSucursal.*' => 'required',
        );
    
        return $validacion;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
      $mensaje = array();
      $mensaje["txCodigoTerceroSucursal.required"] = "El campo código es obligatorio.";
      $mensaje["txNombreComercialTerceroSucursal.required"] =  "El campo nombre es obligatorio.";
      $mensaje["txNombreComercialTerceroSucursal.string"] =  "El campo nombre debe ser de tipo texto.";
      $mensaje["Ciudad_oidTerceroSucursal.required"] =  "El campo de la ciudad es obligatorio obligatorio.";
      $mensaje["txTelefono1TerceroSucursal.required"] = "El teléfono es obligatorio.";
      $mensaje["txDireccionTerceroSucursal.required"] =  "La dirección es obligatorio.";
      return $mensaje;
    }
}
