<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerfilSociodemograficoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lsParentescoFamiliar' => 'nullable',
            'txCedulaFamiliar' => 'nullable',
            'txNombreFamiliar' => 'nullable',
            'txApellidoFamiliar' => 'nullable',
            'Empleado_oidFamiliarEmpleado' => 'nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
