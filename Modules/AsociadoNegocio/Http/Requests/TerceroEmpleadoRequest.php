<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TerceroEmpleadoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $start_date = date('d-m-y');
        $validacion = array(
            'TipoIdentificacion_oidTerceroEmpleado' => 'nullable',
            'txDocumentoEmpleado' => 'nullable',
            'txNombreEmpleado' => 'nullable',
            'txApellidoEmpleado' => 'nullable',
            'txTerceroNombreTercero' => 'nullable',

            'Cargo_oidTerceroEmpleado' => 'nullable',
            'Pais_oidTerceroEmpleadoN' => 'nullable',
            'Ciudad_oidTerceroEmpleadoN' => 'nullable',
            'dtFechaNacimientoEmpleado' => 'nullable|date',
            'txDireccionEmpleado' => 'nullable',
            'txCelularEmpleado' => 'nullable|numeric|min:10',
            'txCorreoEmpleado' => 'nullable|string|email|max:100',
            'lsEstratoEmpleado' => 'nullable',
            'txTiempoEmpresaEmpleado' => 'nullable|max:100',
            'txEpsEmpleado' => 'nullable',
            'Arl_oidEmpleado' => 'nullable',
            'txHorarioTrabajoEmpleado' => 'nullable',
            'txTipoContratoEmpleado' => 'nullable',
            'txGeneroEmpleado' => 'nullable',
            'txEstadoCivilEmpleado' => 'nullable',
            'intPersonasCargoEmpleado' => 'nullable|numeric',
            'lsTipoViviendaEmpleado' => 'nullable',
            'lsEscolaridadEmpleado' => 'nullable',
            'lsIngresosEmpleado' => 'nullable',
            'txEdadEmpleado' => 'nullable',
            'ComFamiliar_oidEmpleadoTercero_1aM' => 'nullable',
            'txTiempoLibreEmpleado' => 'nullable',
            'txDeporteEmpleado' => 'nullable',
            'chSustanciasEmpleado' => 'nullable',
            'txSustanciasEmpleado' => 'nullable',
            'chEnfermedadesEmpleado' => 'nullable',
            'txEnfermedadesEmpleado' => 'nullable',
            'txBarrioViviendaEmpleado' => 'nullable',
            'txFondoPensionEmpleado' => 'nullable',
            // grupo familiar
            'lsParentescoFamiliar' => 'nullable',
            'txCelularFamiliar' => 'nullable',
            'txNombreFamiliar' => 'nullable',
        );

        return $validacion;
    }

    public function messages()
    {
        // ELIMINAR LOS REQUIRED YA Q NINGUN CAMPO ES REQUIRE
        $mensaje = array();
        $mensaje['Cargo_oidTerceroEmpleado' . 'required'] = "El campo Cargo es obligatorio.";
        $mensaje['Pais_oidTerceroEmpleadoN' . 'required'] = "";
        $mensaje['Ciudad_oidTerceroEmpleadoN' . 'required'] = "";
        $mensaje['dtFechaNacimientoEmpleado' . 'before'] = "El campo Fecha de nacimiento debe ser una fecha anterior a la actual";
        $mensaje['txDireccionEmpleado' . 'required'] = "";
        $mensaje['txCelularEmpleado' . 'required'] = "El campo Celular es obligatorio";
        $mensaje['txCelularEmpleado' . 'numeric'] = "El campo Celular solo puede contener numeros";
        $mensaje['txCorreoEmpleado' . 'required'] = "El campo Correo es obligatorio";
        $mensaje['txCorreoEmpleado' . 'email'] = "El campo Correo debe contener un correo valido";
        $mensaje['lsEstratoEmpleado' . 'required'] = "";
        $mensaje['txTiempoEmpresaEmpleado' . 'required'] = "El campo Tiempo en la empres es obligatorio";
        $mensaje['txEpsEmpleado' . 'required'] = "";
        $mensaje['Arl_oidEmpleado' . 'required'] = "";
        $mensaje['txHorarioTrabajoEmpleado' . 'required'] = "";
        $mensaje['txTipoContratoEmpleado' . 'required'] = "";
        $mensaje['txGeneroEmpleado' . 'required'] = "";
        $mensaje['txEstadoCivilEmpleado' . 'required'] = "";
        $mensaje['intPersonasCargoEmpleado' . 'required'] = "El campo Personas a cargo es obligatorio";
        $mensaje['intPersonasCargoEmpleado' . 'numeric'] = "El campo Personas solo puede contener numeros";
        $mensaje['lsTipoViviendaEmpleado' . 'required'] = "";
        $mensaje['lsEscolaridadEmpleado' . 'required'] = "";
        $mensaje['lsIngresosEmpleado' . 'required'] = "";
        $mensaje['txEdadEmpleado' . 'required'] = "El campo Edad es obligatorio";
        $mensaje['ComFamiliar_oidEmpleadoTercero_1aM' . 'required'] = "";
        $mensaje['txTiempoLibreEmpleado' . 'required'] = "";
        $mensaje['txDeporteEmpleado' . 'required'] = "";
        $mensaje['chSustanciasEmpleado' . 'required'] = "";
        $mensaje['txSustanciasEmpleado' . 'required'] = "";








        // $mensaje["TipoIdentificacion_oidTercero.required"] = "El campo tipo Identificación es obligatorio.";
        // $mensaje["txDocumentoTercero.required"] =  "El campo Documento es obligatorio.";
        // $mensaje["txDocumentoTercero.unique"] =  "El valor ingresado en el campo Documento ya existe, éste debe ser único";
        // $mensaje["txDocumentoTercero.numeric"] =  "El campo Documento solo puede contener numeros";
        // $mensaje["txCodigoTercero.required"] =  "El campo Código o contrato es obligatorio";
        // $mensaje["txCodigoTercero.unique"] = "El valor ingresado en el campo Código ya existe, éste debe ser único";
        // $mensaje["txCodigoTercero.numeric"] = "El valor ingresado en el campo Código debe ser un dato numerico";
        // // $mensaje["txPrimerNombreTercero.string"] = "El campo Primer Nombre Solo puede contener caracteres afabéticos";
        // // $mensaje["txPrimerNombreTercero.required"] = "El campo Primer Nombre es obligatorio";
        // // $mensaje["txPrimerApellidoTercero.string"] =  "El campo Primer Apellido Solo puede contener caracteres afabéticos";
        // // $mensaje["txPrimerApellidoTercero.required"] =  "El campo Primer Apellido es obligatorio";
        // $mensaje["txNombreTercero.string"] =  "El campo Razón Social Solo puede contener caracteres afabéticos";
        // $mensaje["txNombreTercero.required"] =  "El campo Razón Social es obligatorio";
        // $mensaje["txDireccionTercero.required"] =  "El campo Dirección es obligatorio";
        // $mensaje["txMovilTercero.required"] =  "El campo Móvil es obligatorio";
        // $mensaje["txEstadoTercero.required"] =  "El campo Estado es obligatorio";
        // $mensaje["Ciudad_oidTercero.required"] =  "El campo Ciudad es obligatorio";
        // $mensaje["txTelefonoTercero.required"] =  "El campo teléfono es obligatorio";
        return $mensaje;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
