<?php

namespace Modules\AsociadoNegocio\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class GrupoProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $validacion = array(
        'txCodigoGrupoProducto.*' => 'required|string',
        'txNombreGrupoProducto.*' => 'required|string',
      );

      return $validacion;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $mensaje = array();
       
        $total = ($this->get("txCodigoGrupoProducto") !== null ) ? count($this->get("txCodigoGrupoProducto")) : 0;
        for($j=0; $j < $total; $j++)
        {
          $mensaje['txCodigoGrupoProducto.'.$j.'.required'] = "El Nombre es obligatorio en el registro N. ".($j+1);
          $mensaje['txCodigoGrupoProducto.'.$j.'.string'] = "El Nombre debe se de tipo texto en el registro N. ".($j+1);
          $mensaje['txNombreGrupoProducto.'.$j.'.required'] = "El Correo es obligatorio en el registro N. ".($j+1);
          $mensaje['txNombreGrupoProducto.'.$j.'.string'] = "El Correo no es valido en el registro N. ".($j+1);
        }
        //print_r($mensaje);
        return $mensaje;
    }
}