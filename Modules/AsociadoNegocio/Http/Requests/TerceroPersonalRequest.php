<?php

    namespace Modules\AsociadoNegocio\Http\Requests;
        
    use Illuminate\Foundation\Http\FormRequest;
        
        class TerceroPersonalRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
         
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                         "lsSexoTerceroPersonal" => "required",
                         "lsGrupoSanguineoTerceroPersonal" => "required",
                         "lsRHTerceroPersonal" => "required",
                         "daFechaExpedicionTerceroPersonal" => "required",
                         "Ciudad_oidExpedicion" => "required",
                         "daFechaNacimientoTerceroPersonal" => "required",
                         "Ciudad_oidNacimiento" => "required",
                         "lsEstadoCivilTerceroPersonal" => "required",

                        
                );
                return $validacion;
            }
    
    
            public function messages()
            {
                $mensaje = array();
                $mensaje["lsSexoTerceroPersonal.required"] = "El campo Sexo es obligatorio.";
				$mensaje["lsGrupoSanguineoTerceroPersonal.required"] =  "El campo Grupo Sanguineo es obligatorio.";
                $mensaje["lsRHTerceroPersonal.required"] =  "El campo RH es obligatorio.";
                $mensaje["daFechaExpedicionTerceroPersonal.required"] = "La fecha de Exp. es obligatorio.";
                $mensaje["Ciudad_oidExpedicion.required"] =  "La Ciudad de Exp. es obligatorio.";
                $mensaje["daFechaNacimientoTerceroPersonal.required"] =  "La Fecha de Nacimiento es obligatorio.";
                $mensaje["Ciudad_oidNacimiento.required"] =  "La Ciudad de Nacimiento es obligatorio.";
                $mensaje["lsEstadoCivilTerceroPersonal.required"] =  "El Estado Civil es obligatorio";
                return $mensaje;
            }
        }
        