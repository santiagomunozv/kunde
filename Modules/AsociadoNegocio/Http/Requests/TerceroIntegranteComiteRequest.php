<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TerceroIntegranteComiteRequest extends FormRequest
{
     /**
      * Determine if the user is authorized to make this request.
      *
      * @return bool
      */
     public function authorize()
     {
          return true;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
     public function rules()
     {
          // Personal
          $validacion = array(
               'txNombreTerceroCopasst.*' => 'required|string',

               'txNombreTerceroCocola.*' => 'required|string',

               'txNombreTerceroConvivencia.*' => 'required|string',
          );

          return $validacion;
     }

     /**
      * Get the validation rules that apply to the request.
      *
      * @return array
      */
     public function messages()
     {
          $mensaje = array();

          $total = ($this->get("oidTerceroCopasst") !== null) ? count($this->get("oidTerceroCopasst")) : 0;
          for ($j = 0; $j < $total; $j++) {
               $mensaje['txNombreTerceroCopasst.' . $j . '.required'] = "El Nombre del integrante copasst es obligatorio en el registro N. " . ($j + 1);
          }

          $total = ($this->get("oidTerceroCocola") !== null) ? count($this->get("oidTerceroCocola")) : 0;
          for ($j = 0; $j < $total; $j++) {
               $mensaje['txNombreTerceroCocola.' . $j . '.string'] = "El Nombre del integrante cocola es de tipo texto en el registro N. " . ($j + 1);
          }

          $total = ($this->get("oidTerceroConvivencia") !== null) ? count($this->get("oidTerceroConvivencia")) : 0;
          for ($j = 0; $j < $total; $j++) {
               $mensaje['txNombreTerceroConvivencia.' . $j . '.string'] = "El Nombre del integrante Convivencia es de tipo Texto  en el registro N. " . ($j + 1);
          }

          return $mensaje;
     }
}
