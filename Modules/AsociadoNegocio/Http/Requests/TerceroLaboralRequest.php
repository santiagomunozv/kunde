<?php

namespace Modules\AsociadoNegocio\Http\Requests;
        
    use Illuminate\Foundation\Http\FormRequest;
        
        class TerceroLaboralRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
         
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                         "GrupoNomina_oidTerceroLaboral" => "required",
                         "inPersonasACargoTerceroLaboral" => "required|numeric",
                         "inNumeroHijosTerceroLaboral" => "required|numeric",
                         "Tercero_oidSalud" => "required",
                         "Tercero_oidCesantias" => "required",
                         "Tercero_oidCaja" => "required",                        
                );
                return $validacion;
            }
    
    
            public function messages()
            {
                $mensaje = array();
                $mensaje["GrupoNomina_oidTerceroLaboral.required"] = "El Grupo Nómina es obligatorio.";
                $mensaje["inPersonasACargoTerceroLaboral.required"] =  "Las Personas a Cargo es obligatorio.";
                $mensaje["inPersonasACargoTerceroLaboral.numeric"] =  "El campo Personas a Cargo debe ser numerico.";
                $mensaje["inNumeroHijosTerceroLaboral.required"] = "El Número de Hijos es obligatorio.";
                $mensaje["inNumeroHijosTerceroLaboral.numeric"] = "El Número de Hijos debe ser numerico.";
                $mensaje["Tercero_oidSalud.required"] =  "Administradora de Salud es obligatoria.";
                $mensaje["Tercero_oidCesantias.required"] =  "Administradora de Cesantías es obligatoria.";
                $mensaje["Tercero_oidCaja.required"] =  "La Caja de Compensación Familiar es obligatoria.";
                return $mensaje;
            }
        }
        