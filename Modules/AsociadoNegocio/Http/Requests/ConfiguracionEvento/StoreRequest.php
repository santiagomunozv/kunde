<?php

namespace Modules\AsociadoNegocio\Http\Requests\ConfiguracionEvento;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    public function rules()
    {
        return [
            "lsPeriodicidadTerceroPeriodicidad" => "required",
            "dtFechaInicioTerceroPeriodicidad" => "required",
            "hrHoraInicioPeriodicidadTerceroPeriodicidad" => "required",
            "inHorasTerceroPeriodicidad" => "required|numeric",
            "inOrdernTerceroEventoPeriodicidad.*" => "required|numeric",
            "Evento_oidEvento.*" => "required|numeric",
            "lsModalidadTerceroPeriodicidad.*" => "required",
            "dtFechaEstimadaTerceroEventoPeriodicidad.*" => "required",
            "dtHoraEstimadaTerceroEventoPeriodicidad.*" => "required",

            "Tercero_oidTercero" => "nullable",
            "oidTerceroEventoPeriodicidad" => "nullable",
            "grabar" => "required"
        ];
    }

    public function messages()
    {
        return [
            'lsPeriodicidadTerceroPeriodicidad.required' => 'El campo Periodicidad es obligatorio.',
            'dtFechaInicioTerceroPeriodicidad.required' => 'El campo Fecha inicio es obligatorio.',
            'hrHoraInicioPeriodicidadTerceroPeriodicidad.required' => 'El campo Hora estimada inicio es obligatorio.',
            'inHorasTerceroPeriodicidad.required' => 'El campo Horas es obligatorio.',
            'dtHoraEstimadaTerceroEventoPeriodicidad.required' => 'El campo Hora estimada es obligatorio.',
            'dtFechaEstimadaTerceroEventoPeriodicidad.required' => 'El campo Fecha estimada obligatorio.',
        ];
    }

    public function authorize()
    {
        return true;
    }
}
