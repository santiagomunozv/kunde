<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TerceroContactoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $validacion = array(
        'txNombreTerceroContacto.*' => 'required|string',
        'txCorreoTerceroContacto.*' => 'required|string',
        'txMovilTerceroContacto.*' => 'required'
      );

      return $validacion;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        $mensaje = array();
       
        $total = ($this->get("oidTerceroContacto") !== null ) ? count($this->get("oidTerceroContacto")) : 0;
        for($j=0; $j < $total; $j++)
        {
          $mensaje['txNombreTerceroContacto.'.$j.'.required'] = "El Nombre es obligatorio en el registro N. ".($j+1);
          $mensaje['txNombreTerceroContacto.'.$j.'.string'] = "El Nombre debe se de tipo texto en el registro N. ".($j+1);
          $mensaje['txCorreoTerceroContacto.'.$j.'.required'] = "El Correo es obligatorio en el registro N. ".($j+1);
          $mensaje['txCorreoTerceroContacto.'.$j.'.string'] = "El Correo no es valido en el registro N. ".($j+1);
          $mensaje['txMovilTerceroContacto.'.$j.'.required'] = "El Móvil es obligatorio en el registro N. ".($j+1);
        }
        //print_r($mensaje);
        return $mensaje;
    }
}