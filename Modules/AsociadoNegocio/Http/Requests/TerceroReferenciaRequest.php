<?php

namespace Modules\AsociadoNegocio\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class TerceroReferenciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Personal
        $validacion = array(
          'txNombreTerceroPersonal.*' => 'required|string',
          'txTelefonoTerceroPersonal.*' => 'required',

          'txNombreTerceroComercial.*' => 'required|string',
          'txTelefonoTerceroComercial.*' => 'required',

          'txNombreTerceroConvivencia.*' => 'required|string',

          'txNombreTerceroFamiliar.*' => 'required|string',
          'txTelefonoTerceroFamiliar.*' => 'required',

          'txNombreTerceroFinancieras.*' => 'required|string',
          'txTelefonoTerceroFinancieras.*' => 'required',
        );
  
        return $validacion;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
          $mensaje = array();
       
          $total = ($this->get("oidTerceroPersonal") !== null ) ? count($this->get("oidTerceroPersonal")) : 0;
          for($j=0; $j < $total; $j++)
          {
               $mensaje['txNombreTerceroPersonal.'.$j.'.required'] = "El Nombre de las referencias Personales es obligatorio en el registro N. ".($j+1);
               $mensaje['txNombreTerceroPersonal.'.$j.'.string'] = "El Nombre de las referencias Personales es de tipo texto en el registro N. ".($j+1);
               $mensaje['txTelefonoTerceroPersonal.'.$j.'.required'] = "El Teléfono de las referencias Personales es obligatorio en el registro N. ".($j+1);
          }

          $total = ($this->get("oidTerceroComercial") !== null ) ? count($this->get("oidTerceroComercial")) : 0;
          for($j=0; $j < $total; $j++)
          {
               $mensaje['txNombreTerceroComercial.'.$j.'.string'] = "El Nombre de las referencias Comerciales es de tipo texto en el registro N. ".($j+1);
               $mensaje['txNombreTerceroComercial.'.$j.'.required'] = "El Nombre de las referencias Comerciales es obligatorio en el registro N. ".($j+1);
               $mensaje['txTelefonoTerceroComercial.'.$j.'.required'] = "El Teléfono de las referencias Comerciales es obligatorio en el registro N. ".($j+1);
          }

          $total = ($this->get("oidTerceroConvivencia") !== null ) ? count($this->get("oidTerceroConvivencia")) : 0;
          for($j=0; $j < $total; $j++)
          {
               $mensaje['txNombreTerceroConvivencia.'.$j.'.string'] = "El Nombre de las Referencias de Convivencia es de tipo Texto  en el registro N. ".($j+1);
               $mensaje['txNombreTerceroConvivencia.'.$j.'.required'] = "El Nombre de las Referencias de Convivencia es obligatorio en el registro N. ".($j+1);
          }

          $total = ($this->get("oidTerceroFamiliar") !== null ) ? count($this->get("oidTerceroFamiliar")) : 0;
          for($j=0; $j < $total; $j++)
          {
               $mensaje['txNombreTerceroFamiliar.'.$j.'.string'] = "El Nombre de las Referencias Familiares es de tipo Texto  en el registro N. ".($j+1);
               $mensaje['txNombreTerceroFamiliar.'.$j.'.required'] = "El Nombre de las Referencias Familiares es obligatorio en el registro N. ".($j+1);
               $mensaje['txTelefonoTerceroFamiliar.'.$j.'.required'] = "El Teléfono de las Referencias Familiares es obligatorio en el registro N. ".($j+1);
          }

          $total = ($this->get("oidTerceroFinancieras") !== null ) ? count($this->get("oidTerceroFinancieras")) : 0;
          for($j=0; $j < $total; $j++)
          {
               $mensaje['txNombreTerceroFinancieras.'.$j.'.string'] = "El Nombre de las Referencias Financieras es de tipo Texto  en el registro N. ".($j+1);
               $mensaje['txNombreTerceroFinancieras.'.$j.'.required'] = "El Nombre de las Referencias Financieras es obligatorio en el registro N. ".($j+1);
               $mensaje['txTelefonoTerceroFinancieras.'.$j.'.required'] = "El Teléfono de las Referencias Financieras es obligatorio en el registro N.  ".($j+1);
          }

          return $mensaje; 
     }
}
