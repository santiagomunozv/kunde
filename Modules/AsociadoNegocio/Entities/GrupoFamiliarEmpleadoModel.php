<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;

class GrupoFamiliarEmpleadoModel extends Model
{
    protected $table = 'asn_grupofamiliarempleado';
    protected $primaryKey = 'oidFamiliarEmpleado';
    protected $fillable = [
        'lsParentescoFamiliar',
        'txCelularFamiliar',
        'txNombreFamiliar',
        'Empleado_oidFamiliarEmpleado',
    ];
}
