<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;


        class GrupoTerceroModel extends Model
        {

            protected $table = 'asn_grupotercero';
            protected $primaryKey = 'oidGrupoTercero';
            protected $fillable = ['ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo','txCodigoGrupoTercero','txNombreGrupoTercero','txCorreoGrupoTercero','txMensajeGrupoTercero','chObservacionGrupoTercero'];
            public $timestamps = false;
        
        }