<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroPersonalModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceropersonal';
            protected $primaryKey = 'oidTerceroPersonal';
            protected $fillable = ['Tercero_oidTercero_1aM','lsSexoTerceroPersonal','lsGrupoSanguineoTerceroPersonal','lsRHTerceroPersonal','daFechaExpedicionTerceroPersonal','Ciudad_oidExpedicion','daFechaNacimientoTerceroPersonal','Ciudad_oidNacimiento','lsEstadoCivilTerceroPersonal', 'inEstratoTerceroPersonal', 'lsEscolaridadTerceroPersonal', 'txTituloTerceroPersonal', 'lsViviendaTerceroPersonal', 'lsCaracteristicaViviendaTerceroPersonal', 'lsZonaViviendaTerceroPersonal', 'lsTransporteTerceroPersonal', 'inEstratoServiciosPublicosTerceroPersonal', 'lsEnergiaTerceroPersonal', 'lsAlcantarilladoTerceroPersonal', 'lsAcueductoTerceroPersonal', 'lsGasTerceroPersonal', 'lsBasuraTerceroPersonal', 'lsTelefonoTerceroPersonal', 'lsConsumeAlcoholTerceroPersonal', 'lsPracticaActividadTerceroPersonal', 'lsFumadorTerceroPersonal', 'lsExfumadorTerceroPersonal', 'lsDeudaTerceroPersonal', 'lsRefinanciarDeudaTerceroPersonal', 'lsComputadorTerceroPersonal', 'lsInternetTerceroPersonal'];
            public $timestamps = false;
        
        }
        