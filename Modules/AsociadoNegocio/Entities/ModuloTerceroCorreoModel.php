<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;

        class ModuloTerceroCorreoModel extends Model
        {
            protected $table = 'gen_modulotercerocorreo';
            protected $primaryKey = 'oidModuloTerceroCorreo';
            protected $fillable = ['ClasificacionTercero_oidClasificacionTercero_1aM','ModuloTercero_oidModuloTercero_1a1','txParaModuloTerceroCorreo','txCopiaModuloTerceroCorreo','txAsuntoModuloTerceroCorreo','txMensajeModuloTerceroCorreo',];
            public $timestamps = false;
        
        }