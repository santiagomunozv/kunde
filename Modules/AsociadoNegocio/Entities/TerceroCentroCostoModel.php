<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroCentroCostoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocentrocosto';
            protected $primaryKey = 'oidTerceroCentroCosto';
            protected $fillable = ['Tercero_oidTercero_1aM','CentroCosto_oidTerceroCentroCosto'];
            public $timestamps = false;
        
        }