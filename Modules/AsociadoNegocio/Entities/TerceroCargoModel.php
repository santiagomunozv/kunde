<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroCargoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocargo';
            protected $primaryKey = 'oidTerceroCargo';
            protected $fillable = ['Tercero_oidTercero_1aM','Cargo_oidTerceroCargo'];
            public $timestamps = false;
        
        }