<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TerceroEmpleadoModel extends Model
{
    protected $table = 'asn_terceroempleado';
    protected $primaryKey = 'oidTerceroEmpleado';
    protected $fillable = [
        'TipoIdentificacion_oidTerceroEmpleado',
        'txDocumentoEmpleado',
        'txNombreEmpleado',
        'txApellidoEmpleado',
        'Tercero_oidTercero',

        'Cargo_oidTerceroEmpleado',
        'Pais_oidTerceroEmpleadoN',
        'Ciudad_oidTerceroEmpleadoN',
        'dtFechaNacimientoEmpleado',
        'Pais_oidTerceroEmpleadoR',
        // 'Ciudad_oidTerceroEmpleadoR',
        'txDireccionEmpleado',
        'txCelularEmpleado',
        'txCorreoEmpleado',
        'lsEstratoEmpleado',
        'txTiempoEmpresaEmpleado',
        'txEpsEmpleado',
        'Arl_oidEmpleado',
        'txHorarioTrabajoEmpleado',
        'txTipoContratoEmpleado',
        'txGeneroEmpleado',
        'txEstadoCivilEmpleado',
        'intPersonasCargoEmpleado',
        'lsTipoViviendaEmpleado',
        'lsEscolaridadEmpleado',
        'lsIngresosEmpleado',
        'txEdadEmpleado',
        'txBarrioViviendaEmpleado',
        'txFondoPensionEmpleado',

        'txTiempoLibreEmpleado',
        'txDeporteEmpleado',
        'chSustanciasEmpleado',
        'txSustanciasEmpleado',
        'chEnfermedadesEmpleado',
        'txEnfermedadesEmpleado'
        // 'chTratamientoDatosEmpleado',
    ];
    public $timestamps = false;

    public function tercero()
    {
        return $this->belongsTo('Modules\AsociadoNegocio\Entities\TerceroModel', 'Tercero_oidTercero_1aM');
    }

    static public function obtenerEmpleados($idEmpresa)
    {


        $registros = DB::table('asn_terceroempleado as te')
            ->join('gen_tipoidentificacion as ti', 'te.TipoIdentificacion_oidTerceroEmpleado', 'ti.oidTipoIdentificacion')
            ->join('gen_pais as pi', 'te.Pais_oidTerceroEmpleadoN', 'pi.oidPais')
            ->join('gen_ciudad as ci', 'te.Ciudad_oidTerceroEmpleadoN', 'ci.oidCiudad')
            ->where('Tercero_oidTercero', $idEmpresa)
            ->select(
                'txNombreEmpleado',
                'txApellidoEmpleado',
                'txDocumentoEmpleado',
                'txGeneroEmpleado',
                'dtFechaNacimientoEmpleado',
                'pi.txNombrePais as nombrePaisNacimiento',
                'ci.txNombreCiudad as nombreCiudadNacimiento',
                'txEdadEmpleado',
                'txEstadoCivilEmpleado',
                // 'Ciudad_oidTerceroEmpleadoR',
                'txDireccionEmpleado',
                'txBarrioViviendaEmpleado',
                'Cargo_oidTerceroEmpleado',
                'txCelularEmpleado',
                'txCorreoEmpleado',
                'lsEstratoEmpleado',
                'txTiempoEmpresaEmpleado',
                'txEpsEmpleado',
                'Arl_oidEmpleado',
                'txFondoPensionEmpleado',
                'txHorarioTrabajoEmpleado',
                'txTipoContratoEmpleado',
                'intPersonasCargoEmpleado',
                'lsTipoViviendaEmpleado',
                'lsEscolaridadEmpleado',
                'lsIngresosEmpleado',
                'txTiempoLibreEmpleado',
                'txDeporteEmpleado',
                'txSustanciasEmpleado',
                'txEnfermedadesEmpleado'

            )
            ->get();
        // dd($registros);
        return $registros;
    }
}
