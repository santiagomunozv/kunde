<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;

class TerceroPeriodicidadModel extends Model
{
    protected $table = 'asn_terceroperiodicidad';
    protected $primaryKey = 'oidTerceroPeriodicidad';
    protected $fillable = [
        'lsPeriodicidadTerceroPeriodicidad',
        'dtFechaInicioTerceroPeriodicidad',
        'hrHoraInicioPeriodicidadTerceroPeriodicidad',
        'inHorasTerceroPeriodicidad',
        'Tercero_oidTercero',
        'inHorasConsumidasTerceroPeriodicidad'
    ];
    public $timestamps = false;

    public static function getByIdTercero($idTercero)
    {
        return self::where('Tercero_oidTercero', $idTercero)->first();
    }
}
