<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;

class TerceroAdjuntoDocumentoModel extends Model
{
    protected $table = 'asn_terceroadjuntodocumento';
    protected $primaryKey = 'oidTerceroAdjuntoDocumento';
    protected $fillable = ['Tercero_oidTercero', 'TipoDocumento_oidTipoDocumento','txDescripcionTerceroAdjuntoDocumento', 'daFechaTerceroAdjuntoDocumento','txRutaTerceroAdjuntoDocumento'];
    public $timestamps = false;
}
