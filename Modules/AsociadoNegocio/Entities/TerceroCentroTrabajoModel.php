<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroCentroTrabajoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocentrotrabajo';
            protected $primaryKey = 'oidTerceroCentroTrabajo';
            protected $fillable = ['Tercero_oidTercero_1aM','CentroTrabajo_oidTerceroCentroTrabajo'];
            public $timestamps = false;
        
        }