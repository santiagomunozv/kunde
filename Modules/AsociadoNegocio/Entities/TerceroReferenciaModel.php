<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroReferenciaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroreferencia';
            protected $primaryKey = 'oidTerceroReferencia';
            protected $fillable = ['Tercero_oidTercero_1aM','txNombreTerceroReferencia','txTipoTerceroReferencia','txParentescoTerceroReferencia','daFechaNacimientoTerceroReferencia','txNivelEducativoTerceroReferencia','txOcupacionTerceroReferencia','txTelefonoTerceroReferencia','txDireccionTerceroReferencia','txObservacionTerceroReferencia',];
            public $timestamps = false;
        
        }