<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroLogisticaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerologistica';
            protected $primaryKey = 'oidTerceroLogistica';
            protected $fillable = [
                'Tercero_oidTercero_1aM',
                'inCierreContableTerceroLogistica',
                'lsCierreContableTerceroLogistica',
                'txTiempoPermanenciaTerceroLogistica',
                'chRequiereShowRoomTerceroLogistica',
                'lsManejoMuestrasTerceroLogistica',
                'lsMontajePedidoTerceroLogistica',
                'inFrecuenciaPedidoTerceroLogistica',
                'lsFrecuenciaPedidoTerceroLogistica',
                'lmSitioEntregaTerceroLogistica',
                'txHorarioEntregaTerceroLogistica',
                'txNombreEntregaTerceroLogistica',
                'txDireccionEntregaTerceroLogistica', 
                'txTelefonoEntregaTerceroLogistica',
                'lmRequisitoEntregaTerceroLogistica',
                'txOtrosRequisitosTerceroLogistica',
                'txManejoDevolucionesTerceroLogistica',
                'txObservacionTerceroLogistica',
                'TipoProducto_oidTerceroLogistica',
                'CanalDistribucion_oidTerceroLogistica',
                'TipoEtiqueta_oidTerceroLogistica',
                'ProcesoLogistico_oidTerceroLogistica',
            ];
            public $timestamps = false;

            public static function findByTerceroId($idTercero){
                $consulta = self::where("Tercero_oidTercero_1aM" , $idTercero)->get();
                return $consulta->first();
            }
        }