<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroVerificacionModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroverificacion';
            protected $primaryKey = 'oidTerceroVerificacion';
            protected $fillable = [
                'Tercero_oidTercero_1aM',
                'Preguntas_oidTerceroVerificacion',
                'lsImpactoTerceroVerificacion',
                'txTipoTereceroVerificacion',
                'Compania_oidCompania',
            ];
            public $timestamps = false;
        
        }