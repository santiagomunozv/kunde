<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;

        class ModuloTerceroModel extends Model
        {
            protected $table = 'gen_modulotercero';
            protected $primaryKey = 'oidModuloTercero';
            protected $fillable = ['txNombreModuloTercero','txRutaImagenModuloTercero','txRutaAccionModuloTercero',];
            public $timestamps = false;
        
            public function correos(){
                return $this->hasMany('App\Gen_ModuloTerceroCorreoModel','ModuloTercero_oidModuloTercero_1a1');
            }
        }