<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroGrupoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;


            protected $table = 'asn_tercerogrupo';
            protected $primaryKey = 'oidTerceroGrupo';
            protected $fillable = [
                'Tercero_oidTercero_1aM',
                'GrupoTercero_oidTerceroGrupo',
                'ClasificacionTercero_oidTerceroGrupo',
                'txObservacionTerceroGrupo',
                'Compania_oidCompania'
            ];
            public $timestamps = false;
        
        }