<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class TerceroIntegranteComiteModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'asn_tercerointegrantescomite';
    protected $primaryKey = 'oidTerceroIntegrantesComite';
    protected $fillable = ['Tercero_oidTercero_1aM', 'txNombreTerceroIntegranteComite', 'txTipoTerceroIntegranteComite', 'inDocumentoTerceroIntegrantesComite'];
    public $timestamps = false;
}
