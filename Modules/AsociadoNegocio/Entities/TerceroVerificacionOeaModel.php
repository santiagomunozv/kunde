<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroVerificacionOeaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroverificacionoea';
            protected $primaryKey = 'oidTerceroVerificacionOea';
            protected $fillable = [
                'Tercero_oidTercero_1a1',
                'dePonderacionTerceroVerificacionOea',
                'txImpactoTerceroVerificacionOea',
                'txTipoTereceroVerificacionOea',
                'Compania_oidCompania',
            ];
            public $timestamps = false;
        
        }