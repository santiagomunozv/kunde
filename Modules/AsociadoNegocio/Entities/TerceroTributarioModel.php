<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class TerceroTributarioModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'asn_tercerotributario';
    protected $primaryKey = 'oidTerceroTributario';
    protected $fillable = [
        'Tercero_oidTercero_1a1',
        'chPracticaRetencionTerceroTributario',
        'lsRegimenVentaTerceroTributario',
        'chAutoretenedorTerceroTributario',
        'NaturalezaJuridica_oidTerceroTributario',
        'chAutoreenedorCREETerceroTributario',
        'ActividadEconomica_oidTerceroTributario',
        'chGranContribuyenteTerceroTributario',
        'ClasificacionRenta_oidTerceroTributario',
        'chEntidadEstadoTerceroTributario',
        'chNoResponsableIVATerceroTributario',
        'txCodigoMatriculaMercantilTerceroTributario'
    ];
    public $timestamps = false;
}
