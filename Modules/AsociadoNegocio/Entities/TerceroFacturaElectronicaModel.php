<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroFacturaElectronicaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerofacturaelectronica';
            protected $primaryKey = 'oidTerceroFacturaElectronica';
            protected $fillable = ['Tercero_oidTercero_1aM','txMatriculaMercantilTerceroFactura','TerceroBanco_oidFacturaElectronica','RegimenContable_oidTerceroFacturaElectronica'];
            public $timestamps = false;
        
        }