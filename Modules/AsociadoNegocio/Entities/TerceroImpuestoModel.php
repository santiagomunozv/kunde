<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroImpuestoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroimpuesto';
            protected $primaryKey = 'oidTerceroImpuesto';
            protected $fillable = [
                'Tercero_oidTercero_1aM',
                'lsConceptoTerceroImpuesto',
                'chProveedorAplicaTerceroImpuesto',
                'chProveedorBase0TerceroImpuesto', 
                'chClienteAplicaTerceroImpuesto',
                'chClienteBase0TerceroImpuesto',  
            ];
            public $timestamps = false;
        
        }