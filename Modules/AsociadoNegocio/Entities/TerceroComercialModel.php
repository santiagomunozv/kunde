<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroComercialModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocomercial';
            protected $primaryKey = 'oidTerceroComercial';
            protected $fillable = ['Tercero_oidTercero_1a1','Tercero_oidVendedor','FormaPago_oidCompra','FormaPago_oidVenta','Compania_oidCompania'];
            public $timestamps = false;
        
        }