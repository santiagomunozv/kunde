<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroCompaniaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocompania';
            protected $primaryKey = 'oidTerceroCompania';
            protected $fillable = ['Tercero_oidTercero_1aM','Compania_oidCompania'];
            public $timestamps = false;
        
        }