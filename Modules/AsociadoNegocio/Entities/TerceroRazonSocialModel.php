<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroRazonSocialModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerorazonsocial';
            protected $primaryKey = 'oidTerceroRazonSocial';
            protected $fillable = ['Tercero_oidTercero_1aM','txNombreTerceroRazonSocial','daFechaInicioTerceroRazonSocial','daFechaFinTerceroRazonSocial'];
            public $timestamps = false;
        
        }