<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroLaboralModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerolaboral';
            protected $primaryKey = 'oidTerceroLaboral';
            protected $fillable = [
                'Tercero_oidTercero_1a1',
                'GrupoNomina_oidTerceroLaboral',
                'inNumeroHijosTerceroLaboral', 
                'deIngresosFamiliares',
                'inPersonasACargoTerceroLaboral', 
                'daFechaRetiroPension',
                'chExtranjeroPension', 
                'chColombianoExterior',              
                'Tercero_oidSalud', 
                'Tercero_oidPension', 
                'Tercero_oidCesantias', 
                'Tercero_oidCaja',
                'daFechaIngresoTerceroLaboral',
                'lsHorarioTerceroLaboral',
                'lsVinculacionTerceroLaboral',
                'deSalarioTerceroLaboral',
                'deBonoTerceroLaboral',
                'lsCabezaFamiliaTerceroLaboral',
                'inPersonasHogarTerceroLaboral',
                'inPersonasDiscapacidadTerceroLaboral'
            ];
            public $timestamps = false;
        
        }