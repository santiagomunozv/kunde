<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroClasificacionModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroclasificacion';
            protected $primaryKey = 'oidTerceroClasificacion';
            protected $fillable = ['Tercero_oidTercero_1aM','ClasificacionTercero_oidTerceroClasificacion_1aM'];
            public $timestamps = false;
        
            public function clasificacion()
            {
                return $this->belongsTo('Modules\General\Entities\ClasificacionTerceroModel','ClasificacionTercero_oidTerceroClasificacion_1aM');
            }
        }
