<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroActividadEconomicaModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_terceroactividadeconomica';
            protected $primaryKey = 'oidTerceroActividadEconomica';
            protected $fillable = ['Tercero_oidTercero_1aM','ActividadEconomica_oidActividadEconomica_1aM'];
            public $timestamps = false;
        
        }