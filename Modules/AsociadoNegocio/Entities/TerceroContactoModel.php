<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroContactoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocontacto';
            protected $primaryKey = 'oidTerceroContacto';
            protected $fillable = ['Tercero_oidTercero_1aM','txNombreTerceroContacto','txCargoTerceroContacto','txCorreoTerceroContacto','txMovilTerceroContacto','txFormularioTerceroContacto','TipoContactoTercero_oidContactoTercero','ClasificacionTercero_oidTerceroContacto'];
            public $timestamps = false;
            
        
            public function tercero()
            {
                return $this->belongsTo('Modules\AsociadoNegocio\Entities\TerceroModel','Tercero_oidTercero_1aM');
            }
    
        }