<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroBancoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerobanco';
            protected $primaryKey = 'oidTerceroBanco';
            protected $fillable = ['Tercero_oidTercero_1aM','Tercero_oidTerceroBanco','txNombreCuentaTerceroBanco','txTitularTerceroBanco','lsTipoTerceroBanco','txNumeroCuentaTerceroBanco'];
            public $timestamps = false;
        
        }