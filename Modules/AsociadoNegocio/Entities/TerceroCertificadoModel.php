<?php 
        namespace Modules\AsociadoNegocio\Entities;
        use Illuminate\Database\Eloquent\Model;
        use OwenIt\Auditing\Contracts\Auditable;


        class TerceroCertificadoModel extends Model implements Auditable
        {
            use \OwenIt\Auditing\Auditable;

            protected $table = 'asn_tercerocertificado';
            protected $primaryKey = 'oidTerceroCertificado';
            protected $fillable = ['Tercero_oidTercero_1aM','TipoCertificado_oidTerceroCertificado','lsVerificacionTerceroCertificado','Compania_oidCompania'];
            public $timestamps = false;
        
        }