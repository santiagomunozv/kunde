<?php 
    namespace Modules\AsociadoNegocio\Entities;
    use Illuminate\Database\Eloquent\Model;
    use OwenIt\Auditing\Contracts\Auditable;


    class GrupoProductoModel extends Model implements Auditable
    {
        use \OwenIt\Auditing\Auditable;

        protected $table = 'ftc_grupoproducto';
        protected $primaryKey = 'oidGrupoProducto';
        protected $fillable = ['Tercero_oidTercero_1aM','ClasificacionProductoGrupo_oidClasificacionProductoGrupo','txCodigoGrupoProducto','txNombreGrupoProducto'];
        public $timestamps = false;

        public static function getGruposByClasificacionProducto($id,$idTercero){
            return self::where('ClasificacionProductoGrupo_oidClasificacionProductoGrupo',$id)
            ->where('Tercero_oidTercero_1aM',$idTercero)
            ->orderBy('txNombreGrupoProducto','Asc')
            ->select('txNombreGrupoProducto','oidGrupoProducto')
            ->pluck('txNombreGrupoProducto','oidGrupoProducto');
        }
    }