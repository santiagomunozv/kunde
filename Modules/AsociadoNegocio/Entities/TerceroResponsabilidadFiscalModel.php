<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class TerceroResponsabilidadFiscalModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'asn_terceroresponsabilidadfiscal';
    protected $primaryKey = 'oidTerceroResponsabilidadFiscal';
    protected $fillable = [
        'FacturaElectronica_oidTerceroFacturaElectronica_1aM',
        'ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM',
    ];
    public $timestamps = false;
}
