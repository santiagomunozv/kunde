<?php 
namespace Modules\AsociadoNegocio\Entities;
use Illuminate\Database\Eloquent\Model;
use Modules\Comercial\Entities\ListaPrecioModel;
use OwenIt\Auditing\Contracts\Auditable;


class TerceroSucursalModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'asn_tercerosucursal';
    protected $primaryKey = 'oidTerceroSucursal';
    protected $fillable = ['Tercero_oidTercero_1aM','txCodigoTerceroSucursal','txNombreComercialTerceroSucursal','Ciudad_oidTerceroSucursal','txCodigoBarrasTerceroSucursal','txTelefono1TerceroSucursal','txCorreoElectronicoTerceroSucursal','txDireccionTerceroSucursal',];
    public $timestamps = false;

    public function tercero(){
        return $this->belongsTo('Modules\AsociadoNegocio\Entities\TerceroModel','Tercero_oidTercero_1aM');
    }

    public function ciudad(){
        return $this->belongsTo('Modules\General\Entities\CiudadModel','Ciudad_oidTerceroSucursal');
    }

    public static function sucursalesNoExcluidasListaPrecio(ListaPrecioModel $listaPrecio){
        $idCliente = $listaPrecio->Tercero_oidListaPrecio;
        return self::leftJoin("com_listaprecioexclusion" ,function($join) use ($listaPrecio){
            $join->on('oidTerceroSucursal','TerceroSucursal_oidListaPrecioExclusion')
                ->where('ListaPrecio_oidListaPrecio_1aM' , $listaPrecio->oidListaPrecio);
        })
        ->join('gen_ciudad' , 'Ciudad_oidTerceroSucursal' , 'oidCiudad')
        ->where('Tercero_oidTercero_1aM' , $idCliente)
        ->whereNull('oidListaPrecioExclusion')
        ->orderBy('txNombreCiudad', 'ASC')
        ->select('oidTerceroSucursal','txCodigoTerceroSucursal','txNombreComercialTerceroSucursal', 'txNombreCiudad')
        ->orderBy('txNombreComercialTerceroSucursal' , 'ASC')
        ->get();
    }

    public static function listSelectByCliente($idCliente , $orId = 0){
        return self::where('Tercero_oidTercero_1aM' ,$idCliente)
        ->orWhere("oidTerceroSucursal" , $orId)
        ->select("oidTerceroSucursal" , "txNombreComercialTerceroSucursal")
        ->pluck( "txNombreComercialTerceroSucursal","oidTerceroSucursal");
    }
}