<?php 
    namespace Modules\AsociadoNegocio\Entities;
    use Illuminate\Database\Eloquent\Model;
    use OwenIt\Auditing\Contracts\Auditable;


    class TerceroAdjuntoModel extends Model implements Auditable
    {
        use \OwenIt\Auditing\Auditable;

        protected $table = 'asn_terceroadjunto';
        protected $primaryKey = 'oidTerceroAdjunto';
        protected $fillable = ['Tercero_oidTercero_1aM','TipoCertificado_oidTerceroCertificado','daFechaCreacionTerceroAdjunto','daFechaVencimientoTerceroAdjunto','txRutaTerceroAdjunto','txDescripcionTerceroAdjunto','Compania_oidCompania'];
        public $timestamps = false;
    
    }