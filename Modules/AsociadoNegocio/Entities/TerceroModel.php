<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use OwenIt\Auditing\Contracts\Auditable;

class TerceroModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'asn_tercero';
    protected $primaryKey = 'oidTercero';
    protected $fillable = [
        'Ciudad_oidTercero',
        'TipoIdentificacion_oidTercero',
        'txDocumentoTercero',
        'txDigitoVerificacionTercero',
        'txCodigoTercero',
        'txCodigoBarrasTercero',
        'txPrimerNombreTercero',
        'txSegundoNombreTercero',
        'txPrimerApellidoTercero',
        'txSegundoApellidoTercero',
        'txNombreTercero',
        'txNombreComercialTercero',
        'txDireccionTercero',
        'txTelefonoTercero',
        'txMovilTercero',
        'txCorreoElectronicoTercero',
        'lmTipoServicioTercero',
        'lmClasificacionTercero',
        'txPrefijoProductoTercero',
        'txGrupoSizfraTercero',
        'Usuario_oidCreador',
        'Usuario_oidModificador',
        'daFechaModificacionTercero',
        'imImagenTercero',
        'daFechaCreacionTercero',
        'txEstadoSistemaGestionTercero',
        'txEstadoTercero',
        'txResponsableTercero',
        'txObservacionTercero',
        'txUrl',
        'txNombreResponsableSSTTercero',
        'inDocumentoResponsableSSTTercero',
        'txNombreRepresentanteLegalTercero',
        'inDocumentoRepresentanteLegalTercero',
        'txNombreIntegranteComiteCopassTercero',
        'inDocumentoIntegranteComiteCopasstTercero',
        'txNombreIntegranteComiteConvivenciaTercero',
        'inDocumentoIntegranteComiteConvivenciaTerce',
        'txNombreIntegranteBrigadaTercero',
        'inDocumentoIntegranteBrigadaTercero',
        'ActividadEconomica_oidActividadEconomica',
        'Arl_oidArl',
        'txMisionTercero',
        'txVisionTercero',
        'txValoresCorporativoTercero'
    ];

    public $timestamps = false;

    public static function getById($id)
    {
        return self::where('oidTercero', $id)->first();
    }

    public function ciudad()
    {
        return $this->belongsTo('Modules\General\Entities\CiudadModel', 'Ciudad_oidTercero');
    }

    public function actividadEconomica()
    {
        return $this->belongsTo('Modules\General\Entities\ActividadEconomicaModel', 'ActividadEconomica_oidActividadEconomica');
    }

    public function arl()
    {
        return $this->belongsTo('Modules\General\Entities\ArlModel', 'Arl_oidArl');
    }

    public function terceroClasificacion()
    {
        return $this->hasMany('Modules\AsociadoNegocio\Entities\TerceroClasificacionModel', 'Tercero_oidTercero_1aM');
    }

    public function contactos()
    {
        return $this->hasMany('Modules\AsociadoNegocio\Entities\TerceroContactoModel', 'Tercero_oidTercero_1aM');
    }

    public static function findByTipoCliente(array $tiposCliente, array $columnsToSelect = null, $orId = 0)
    {
        if (!$tiposCliente) {
            throw new \Exception("Se debe especificar el tipo de tercero");
        }
        $query =  self::join('asn_terceroclasificacion', 'Tercero_oidTercero_1aM', 'oidTercero')
            ->join('gen_clasificaciontercero', 'ClasificacionTercero_oidTerceroClasificacion_1aM', 'oidClasificacionTercero')
            ->where('txEstadoTercero', '=', 'Activo');
        if (count($tiposCliente) == 1) {
            $query->where('lmTipoClasificacionTercero', 'like', "%{$tiposCliente[0]}%");
        } else {
            $query->where(function ($query) use ($tiposCliente) {
                $query->where('lmTipoClasificacionTercero', 'like', "%{$tiposCliente[0]}%");
                for ($i = 1; $i < count($tiposCliente); $i++) {
                    $query->orWhere('lmTipoClasificacionTercero', 'like', "%{$tiposCliente[$i]}%");
                }
            });
        }
        $query->orWhere('oidTercero', $orId)->orderBy('txNombreTercero', 'ASC');
        if ($columnsToSelect) {
            $query->select($columnsToSelect);
        }
        return $query->get();
    }

    public static function findClienteProveedorBySearch($busqueda)
    {
        return self::join('asn_terceroclasificacion', 'Tercero_oidTercero_1aM', 'oidTercero')
            ->join('gen_clasificaciontercero', 'ClasificacionTercero_oidTerceroClasificacion_1aM', 'oidClasificacionTercero')
            ->where('txEstadoTercero', 'Activo')
            ->where(function ($query) {
                $query->where('lmTipoClasificacionTercero', 'like', '%Proveedor%')
                    ->orWhere('lmTipoClasificacionTercero', 'like', '%Cliente%');
            })->where(function ($query) use ($busqueda) {
                $query->where('txDocumentoTercero', 'like', '%' . $busqueda . '%')
                    ->orWhere('txNombreTercero', 'like', '%' . $busqueda . '%')
                    ->orWhere('txNombreComercialTercero', 'like', '%' . $busqueda . '%');
            })->orderBy('txNombreTercero', 'asc')->groupBy('txDocumentoTercero')->get();
    }

    public static function gridQuery($ct)
    {
        return self::select([
            'T.oidTercero',
            'TI.txNombreTipoIdentificacion',
            'T.txDocumentoTercero',
            'T.txNombreTercero AS Tercero',
            'T.txNombreComercialTercero',
            'T.txTelefonoTercero',
            'T.txMovilTercero',
            'T.txCorreoElectronicoTercero',
            'TCR.txNombreTercero AS creador',
            'T.daFechaCreacionTercero',
            'TMD.txNombreTercero AS modificador',
            'T.daFechaModificacionTercero',
            'T.txEstadoSistemaGestionTercero',
            'T.txEstadoTercero'
        ])
            ->from('asn_tercero AS T')
            ->leftjoin('gen_tipoidentificacion AS TI', 'T.TipoIdentificacion_oidTercero', '=', 'TI.oidTipoIdentificacion')
            ->leftjoin('asn_terceroclasificacion AS TCS', 'T.oidTercero', '=', 'TCS.Tercero_oidTercero_1aM')
            ->leftjoin('asn_tercerocompania AS TC', 'TC.Tercero_oidTercero_1aM', '=', 'T.oidTercero')
            ->leftjoin('seg_usuario AS UC', 'T.Usuario_oidCreador', '=', 'UC.oidUsuario')
            ->leftjoin('asn_tercero AS TCR', 'UC.Tercero_oidUsuario', '=', 'TCR.oidTercero')
            ->leftjoin('seg_usuario AS UM', 'T.Usuario_oidModificador', '=', 'UM.oidUsuario')
            ->leftjoin('asn_tercero AS TMD', 'UM.Tercero_oidUsuario', '=', 'TMD.oidTercero')
            ->where('TC.Compania_oidCompania', '=', Session::get('oidCompania'))
            ->where('ClasificacionTercero_oidTerceroClasificacion_1aM', $ct);
    }

    //método encargado de obtener nombre y Id de todos los Tercero
    public function getNameAndIdTercero()
    {
        $tercero = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        return $tercero;
    }

    //método encargado de obtener lista de tercero según su estado y tipo
    public function getTerceroAcoordingTypeAndState($tipo, $status = 'Activo')
    {
        $terceros = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', $status)
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', $tipo)
            ->pluck('txNombreTercero', 'oidTercero');

        return $terceros;
    }

    //método encargado de obtener propiedad recibida de todos los terceros 
    public function getAllPropertysTerceroByName($propiedad)
    {
        $propiedad = self::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where(function ($query) {
                $query->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
                    ->orWhere('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli');
            })
            ->pluck($propiedad);

        return $propiedad;
    }
}
