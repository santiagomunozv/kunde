<?php

namespace Modules\AsociadoNegocio\Entities;

use Illuminate\Database\Eloquent\Model;

class TerceroEventoPeriodicidadModel extends Model
{

    protected $table = 'asn_terceroeventoperiodicidad';
    protected $primaryKey = 'oidTerceroEventoPeriodicidad';
    protected $fillable = [
        'Evento_oidEvento',
        'TerPerio_oidTerPerio',
        'inOrdernTerceroEventoPeriodicidad',
        'dtFechaEstimadaTerceroEventoPeriodicidad',
        'dtHoraEstimadaTerceroEventoPeriodicidad',
        'lsModalidadTerceroPeriodicidad'
    ];
    public $timestamps = false;

    public function eventos()
    {
        return $this->belongsToMany('Modules/Reunion/Entities/EventosModel');
    }

    public static function getByIdTerPeriodicidad($idTerPerio)
    {
        return self::where('TerPerio_oidTerPerio', $idTerPerio)->get();
    }
}
