<?php

namespace Modules\AsociadoNegocio\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CertificadoTerceroVencido extends Mailable
{
    use Queueable, SerializesModels;

    public $nombreTercero;
    public $fechaCertificado;
    public $nombreTipoCertificado;
    public $documentoTercero;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombreTercero,$fechaCertificado,$nombreTipoCertificado,$documentoTercero)
    {
        $this->nombreTercero = $nombreTercero;
        $this->fechaCertificado = $fechaCertificado;
        $this->nombreTipoCertificado = $nombreTipoCertificado;
        $this->nombreTipoCertificado = $nombreTipoCertificado;
        $this->documentoTercero = $documentoTercero;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('asociadonegocio::correoCertificadoVencido');
    }
}
