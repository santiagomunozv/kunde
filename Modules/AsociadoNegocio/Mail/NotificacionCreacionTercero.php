<?php

namespace Modules\AsociadoNegocio\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificacionCreacionTercero extends Mailable
{
    use Queueable, SerializesModels;

    public $mensaje;
    public $clasificacion;
    public $nombreTercero;
    public $documentoTercero;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mensaje,$clasificacion,$nombreTercero,$documentoTercero)
    {
        $this->mensaje = $mensaje;
        $this->clasificacion = $clasificacion;
        $this->nombreTercero = $nombreTercero;
        $this->documentoTercero = $documentoTercero;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('asociadonegocio::CorreoCreacionTercero');
    }
}
