<?php

namespace Modules\AsociadoNegocio\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AsociadoNegocioDatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(
            ServiciosTableSeeder::class
        );
    }
}
