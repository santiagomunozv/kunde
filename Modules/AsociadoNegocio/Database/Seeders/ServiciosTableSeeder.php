<?php

namespace Modules\AsociadoNegocio\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\General\Entities\Servicio;

class ServiciosTableSeeder extends Seeder
{

    public function run()
    {
        $items = [
            ['id' => 1, 'txNombre' => 'Asesoria', 'chEstado' => 1],
            ['id' => 2, 'txNombre' => 'AdministradorSalud', 'chEstado' => 1],
            ['id' => 3, 'txNombre' => 'AdministradorPension', 'chEstado' => 1],
            ['id' => 4, 'txNombre' => 'AdministradorCesantias', 'chEstado' => 1],
            ['id' => 5, 'txNombre' => 'AdministradorCompensacion', 'chEstado' => 1],
        ];

        foreach ($items as $item) {
            Servicio::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
