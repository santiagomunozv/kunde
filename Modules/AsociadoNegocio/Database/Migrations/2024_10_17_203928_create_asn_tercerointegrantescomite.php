<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsnTercerointegrantescomite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_tercerointegrantescomite', function (Blueprint $table) {
            $table->increments('oidTerceroIntegrantesComite');
            $table->integer('Tercero_oidTercero_1aM');
            $table->string('txNombreTerceroIntegranteComite', 255);
            $table->string('txTipoTerceroIntegranteComite');
            $table->integer('inDocumentoTerceroIntegrantesComite');
        });

        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->integer('ActividadEconomica_oidActividadEconomica')->nullable()->comment('Actividad económica');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_tercerointegrantescomite');

        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->dropColumn('ActividadEconomica_oidActividadEconomica');
        });
    }
}
