<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTerceroadjuntodocumento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_terceroadjuntodocumento', function (Blueprint $table) {
            $table->increments('oidTerceroAdjuntoDocumento');
            $table->integer('Tercero_oidTercero');
            $table->integer('TipoDocumento_oidTipoDocumento');
            $table->string('txDescripcionTerceroAdjuntoDocumento', 80)->comment("Descricpión del adjunto");
            $table->date('daFechaTerceroAdjuntoDocumento', 20)->comment("Fecha del adjunto");
            $table->string('txRutaTerceroAdjuntoDocumento', 80)->comment("Ruta del adjunto");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_terceroadjuntodocumento');
    }
}
