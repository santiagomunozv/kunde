<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddValorescorporativosTercero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->string('txMisionTercero')->nullable()->comment('Misión');
            $table->string('txVisionTercero')->nullable()->comment('Visión');
            $table->string('txValoresCorporativoTercero')->nullable()->comment('Valores corporativos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->dropColumn('txMisionTercero');
            $table->dropColumn('txVisionTercero');
            $table->dropColumn('txValoresCorporativoTercero');
        });
    }
}
