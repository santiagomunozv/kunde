<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsObservacionandresponsableTerceros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->string('txResponsableTercero')->nullable()->comment('Responsable tercero');
            $table->text('txObservacionTercero')->nullable()->comment('Observacion tercero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('txResponsableTercero');
            $table->dropColumn('txObservacionTercero');
        });
    }
}
