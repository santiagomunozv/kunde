<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsnTerceroeventoperiodicidadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_terceroeventoperiodicidad', function (Blueprint $table) {
            $table->bigIncrements('oidTerceroEventoPeriodicidad');
            $table->unsignedInteger('Evento_oidEvento');
            $table->unsignedBigInteger('TerPerio_oidTerPerio');

            $table->foreign('Evento_oidEvento')->references('oidEvento')->on('reu_evento');
            $table->foreign('TerPerio_oidTerPerio')->references('oidTerceroPeriodicidad')->on('asn_terceroperiodicidad')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_terceroeventoperiodicidad');
    }
}
