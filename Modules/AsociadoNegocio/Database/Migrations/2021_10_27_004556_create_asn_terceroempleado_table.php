<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsnTerceroempleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_terceroempleado', function (Blueprint $table) {
            $table->bigIncrements('oidTerceroEmpleado');
            $table->integer('TipoIdentificacion_oidTerceroEmpleado')->comment('Tipo de documento');
            $table->string('txDocumentoEmpleado')->comment('Cedula Empleado');
            $table->string('txNombreEmpleado')->comment('Nombre Empleado');
            $table->string('txApellidoEmpleado')->comment('Apellido Empleado');
            // ENCUESTA PERFIL SOCIODEMOGRÁFICO
            $table->string('txCelularEmpleado')->comment('Numero telefonico Empleado')->nullable();
            $table->string('txEdadEmpleado')->comment('Edad Empleado')->nullable();
            $table->string('txGeneroEmpleado')->comment('Genero Empleado')->nullable();
            $table->string('txCorreoEmpleado')->comment('Correo Empleado')->nullable();
            $table->string('txEpsEmpleado')->comment('EPS Empleado')->nullable();
            $table->integer('Arl_oidEmpleado')->comment('ARL Empleado')->nullable();
            $table->string('txEstadoCivilEmpleado')->comment('Estado civil Empleado')->nullable();
            $table->string('lsEstratoEmpleado')->comment('Estrato empleado Empleado')->nullable();
            $table->integer('intPersonasCargoEmpleado')->comment('Numero de personas a cargo Empleado')->nullable();
            $table->string('lsEscolaridadEmpleado')->comment('Nivel de escolaridad Empleado')->nullable();
            $table->integer('Pais_oidTerceroEmpleadoN')->comment('Pais de nacimiento Empleado')->nullable();
            $table->integer('Ciudad_oidTerceroEmpleadoN')->comment('Ciudad de nacimiento Empleado')->nullable();
            $table->date('dtFechaNacimientoEmpleado')->comment('Fecha nacimiento Empleado')->nullable();
            // $table->integer('Ciudad_oidTerceroEmpleadoR')->comment('Ciudad recidencia Empleado')->nullable();
            $table->string('txDireccionEmpleado')->comment('Direccion residencia Empleado')->nullable();
            $table->string('lsTipoViviendaEmpleado')->comment('Tipo vivienda Empleado')->nullable();
            $table->string('txBarrioViviendaEmpleado')->comment('Barrio vivienda Empleado')->nullable();
            $table->string('txFondoPensionEmpleado')->comment('Fondo de pension')->nullable();
            $table->string('txTipoContratoEmpleado')->comment('Tipo contrato Empleado')->nullable();
            $table->string('Cargo_oidTerceroEmpleado')->comment('Cargo Empleado')->nullable();
            $table->string('txHorarioTrabajoEmpleado')->comment('Horario Trabajo Empleado')->nullable();
            $table->text('txTiempoEmpresaEmpleado')->comment('Tiempo en la empresa Empleado')->nullable();
            $table->string('lsIngresosEmpleado')->comment('Promedio de ingresos Empleado')->nullable();
            $table->string('txTiempoLibreEmpleado', 100)->comment('Actividades de tiempo libre Empleado')->nullable();
            $table->string('txDeporteEmpleado', 100)->comment('Deportes Empleado')->nullable();
            $table->boolean('chSustanciasEmpleado')->comment('Consumo de sustancias Empleado')->nullable();
            $table->string('txSustanciasEmpleado', 100)->comment('Sustancias Empleado')->nullable();
            $table->boolean('chEnfermedadesEmpleado')->comment('¿el empleado tiene enfermedades?')->nullable();
            $table->string('txEnfermedadesEmpleado', 50)->comment('enfermedades del empleado')->nullable();

            $table->integer('Tercero_oidTercero');

            // departamento y ciudad de nacimiento
            $table->foreign('Pais_oidTerceroEmpleadoN')->references('oidPais')->on('gen_pais');
            $table->foreign('Ciudad_oidTerceroEmpleadoN')->references('oidCiudad')->on('gen_ciudad');
            // ciudad de recidencia
            // $table->foreign('Ciudad_oidTerceroEmpleadoR')->references('oidCiudad')->on('gen_ciudad');
            // arl
            $table->foreign('Arl_oidEmpleado')->references('oidArl')->on('gen_arl');
            $table->foreign('TipoIdentificacion_oidTerceroEmpleado', 'TipoIdTerceroEmp_foreign')->references('oidTipoIdentificacion')->on('gen_tipoidentificacion');
            $table->foreign('Tercero_oidTercero')->references('oidTercero')->on('asn_tercero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_terceroempleado');
    }
}
