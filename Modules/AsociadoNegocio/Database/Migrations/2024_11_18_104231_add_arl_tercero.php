<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArlTercero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->integer('Arl_oidArl')->nullable()->comment('Arl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->dropColumn('Arl_oidArl');
        });
    }
}
