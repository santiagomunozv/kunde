<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsnGrupofamiliarempleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn_grupofamiliarempleado', function (Blueprint $table) {
            $table->bigIncrements('oidFamiliarEmpleado');
            $table->string('lsParentescoFamiliar')->comment('Parentesco con el empleado')->nullable();
            $table->string('txCelularFamiliar')->comment('Celular Familiar')->nullable();
            $table->string('txNombreFamiliar')->comment('Nombre familiar')->nullable();
            $table->unsignedBigInteger('Empleado_oidFamiliarEmpleado')->comment('Empleado');

            $table->foreign('Empleado_oidFamiliarEmpleado')->references('oidTerceroEmpleado')->on('asn_terceroempleado')->onDelete('cascade');

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn_grupofamiliarempleado');
    }
}
