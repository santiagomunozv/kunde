<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTxUrlToAsnTerceroTable extends Migration
{

    public function up()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->string('txUrl')->nullable();
        });
    }

    public function down()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {

        });
    }
}
