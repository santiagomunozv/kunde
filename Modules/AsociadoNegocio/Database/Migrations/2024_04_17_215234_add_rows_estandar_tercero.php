<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowsEstandarTercero extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->string('txNombreResponsableSSTTercero')->nullable()->comment('Responsable SST');
            $table->integer('inDocumentoResponsableSSTTercero')->nullable()->comment('Documento Responsable SST');
            $table->string('txNombreRepresentanteLegalTercero')->nullable()->comment('Representante legal');
            $table->integer('inDocumentoRepresentanteLegalTercero')->nullable()->comment('Documento representante legal');
            $table->string('txNombreIntegranteComiteCopassTercero')->nullable()->comment('Integrante comité copasst');
            $table->integer('inDocumentoIntegranteComiteCopasstTercero')->nullable()->comment('Documento integrante comité copasst');
            $table->string('txNombreIntegranteComiteConvivenciaTercero')->nullable()->comment('Integrante comité convivencia');
            $table->integer('inDocumentoIntegranteComiteConvivenciaTerce')->nullable()->comment('Documento integrante comité convivencia');
            $table->string('txNombreIntegranteBrigadaTercero')->nullable()->comment('Ingegrante brigada');
            $table->integer('inDocumentoIntegranteBrigadaTercero')->nullable()->comment('Documento integrante brigada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asn_tercero', function (Blueprint $table) {
            $table->dropColumn('txNombreResponsableSSTTercero');
            $table->dropColumn('inDocumentoResponsableSSTTercero');
            $table->dropColumn('txNombreRepresentanteLegalTercero');
            $table->dropColumn('inDocumentoRepresentanteLegalTercero');
            $table->dropColumn('txNombreIntegranteComiteCopassTercero');
            $table->dropColumn('inDocumentoIntegranteComiteCopasstTercero');
            $table->dropColumn('txNombreIntegranteComiteConvivenciaTercero');
            $table->dropColumn('inDocumentoIntegranteComiteConvivenciaTerce');
            $table->dropColumn('txNombreIntegranteBrigadaTercero');
            $table->dropColumn('inDocumentoIntegranteBrigadaTercero');
        });
    }
}
