<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsnTerceroperiodicidadTable extends Migration
{

    public function up()
    {
        Schema::create('asn_terceroperiodicidad', function (Blueprint $table) {
            $table->bigIncrements('oidTerceroPeriodicidad');
            $table->string('lsPeriodicidadTerceroPeriodicidad');
            $table->date('dtFechaInicioTerceroPeriodicidad');
            $table->string('hrHoraInicioPeriodicidadTerceroPeriodicidad');
            $table->string('inHorasTerceroPeriodicidad');
            $table->integer('Tercero_oidTercero');

            $table->foreign('Tercero_oidTercero')->references('oidTercero')->on('asn_tercero');
        });
    }

    public function down()
    {
        Schema::dropIfExists('asn_terceroperiodicidad');
    }
}
