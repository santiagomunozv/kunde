<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowTerceroeventoperiocididad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asn_terceroeventoperiodicidad', function (Blueprint $table) {
            $table->integer('inOrdernTerceroEventoPeriodicidad')->default(0)->nullable()->comment('Orden del evento');
            $table->date('dtFechaEstimadaTerceroEventoPeriodicidad')->nullable()->comment('Fecha estimada evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asn_terceroeventoperiodicidad', function (Blueprint $table) {
            $table->dropColumn('inOrdernTerceroEventoPeriodicidad');
            $table->dropColumn('dtFechaEstimadaTerceroEventoPeriodicidad');
        });
    }
}
