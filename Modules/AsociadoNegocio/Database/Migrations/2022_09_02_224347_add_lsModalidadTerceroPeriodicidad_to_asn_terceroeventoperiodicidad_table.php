<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLsModalidadTerceroPeriodicidadToAsnTerceroeventoperiodicidadTable extends Migration
{

    public function up()
    {
        Schema::table('asn_terceroeventoperiodicidad', function (Blueprint $table) {
            $table->time('dtHoraEstimadaTerceroEventoPeriodicidad')->nullable();
            $table->text('lsModalidadTerceroPeriodicidad')->nullable();
        });
    }

    public function down()
    {
        Schema::table('asn_terceroeventoperiodicidad', function (Blueprint $table) {
            $table->dropColumn('dtHoraEstimadaTerceroEventoPeriodicidad');
            $table->dropColumn('lsModalidadTerceroPeriodicidad');
        });
    }
}
