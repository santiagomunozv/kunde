@extends('layouts.principal')
@section('nombreModulo')
Verificación Documental
@endsection
@section('scripts')
    {{Html::script('modules/asociadonegocio/js/TerceroCertificado.js')}}
@endsection
@section('estilos')
<style>

</style>
    
@endsection

@section('contenido')
    {!!Form::open(['url'=>['/asociadonegocio/tercerocertificado',$id],'method'=>'PUT', 'id'=>'terceroCertificado','onsubmit' => 'return false'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {{-- Gurdamos los hidden de los archivos que eliminaron --}}
            <div id="eliminarArchivos">
                <input type="hidden" id="eliminarArchivos">
            </div>
            <?php $reg = 0;  ?>
            @while ($reg < count($terceroCertificado))
                <?php $nombreAnt = $terceroCertificado[$reg]->txNombreTipoCertificado;  ?>
                <?php $contador = $reg;  ?>

                    {!!Form::hidden('oidTerceroCertificado[]',$terceroCertificado[$reg]->oidTerceroCertificado, array('id' => 'oidTerceroCertificado')) !!}
                    {!!Form::hidden('Tercero_oidTercero_1aM',null, array('id' => 'Tercero_oidTercero_1aM')) !!}
                    {!!Form::hidden('TipoCertificado_oidTerceroCertificado[]',$terceroCertificado[$reg]->oidTipoCertificado, array('id' => 'TipoCertificado_oidTerceroCertificado')) !!}

                    <div class="row">  
                        <div class="col-sm-8">
                            <a data-toggle="collapse" href="#collapseExample{{$reg}}" role="button" aria-expanded="false" aria-controls="collapseExample" onclick ="generarDropzone({{$reg}},{{$terceroCertificado[$reg]->oidTipoCertificado}})">
                                <div @if ($terceroCertificado[$reg]->lsVerificacionTerceroCertificado == 'No') class = "alert alert-danger" @elseif($terceroCertificado[$reg]->lsVerificacionTerceroCertificado == 'Si') class = "alert alert-success"   @else class = "alert alert-dark" @endif role="alert" style="height: 40px" id="txNombreTipoCertificado{{$reg}}">
                                    {!!Form::label('txNombreTipoCertificado',$nombreAnt, array('id'=>"txNombre")) !!}
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            {!!Form::select('lsVerificacionTerceroCertificado[]',['No'=>'No','Si'=>'Si','N/A'=>'N/A'],isset($terceroCertificado[$reg]->lsVerificacionTerceroCertificado) ? $terceroCertificado[$reg]->lsVerificacionTerceroCertificado : 'N/A',[ 'class'=>'form-control','onchange'=>'cambiarClase(this.id,this.value)','id'=>"lsVerificacionTerceroCertificado$reg",'placeholder'=>'Seleccione'])!!}
                        </div>
                        <div id="archivos{{$reg}}"></div>
                        <div class="collapse col-md-9 contenedores" id="collapseExample{{$reg}}">
                            <div class="col-md-9">
                                <div id="dropzoneImage{{$reg}}" class="dropzone-div"style="border: 2px dashed #0087F7;">
                                    <div class="row">
                                        {{-- Aca hacemos el rompimiento de control para mostrar los archivos de cada tipo de certificados --}}
                                        @while ($reg < count($terceroCertificado) and $nombreAnt == $terceroCertificado[$reg]->txNombreTipoCertificado)
                                            @if ($terceroCertificado[$reg]->txRutaTerceroAdjunto)
                                                {!!Form::hidden("contador$contador",$reg, array('id' => "contador$contador")) !!}

                                                <div class="col-md-3" id ="div{{$terceroCertificado[$reg]->oidTerceroAdjunto}}">
                                                    <i class="fas fa-times" onclick="eliminarArchivo('{{$terceroCertificado[$reg]->oidTerceroAdjunto}}')" style="width: 50px;"></i>
                                                    <a href="{{URL::to('/asociadonegocio/imprimirCertificado',$terceroCertificado[$reg]->txRutaTerceroAdjunto)}}">
                                                        <i class="fas fa-file-download"  style="font-size: 80px;" data-toggle="tooltip" title="Descripción: {{$terceroCertificado[$reg]->txDescripcionTerceroAdjunto}}"></i>
                                                    </a>
                                                    @if ($terceroCertificado[$reg]->TipoCertificado_oidTerceroCertificado == '6')
                                                        {!!Form::label('NombreAdjunto', 'Nombre Adjunto', array('class' => 'text-sm text-primary mb-1')) !!}
                                                        {!!Form::text('nombreArchivo',$terceroCertificado[$reg]->txRutaTerceroAdjunto,[ 'class'=>'form-control','readonly', 'style'=>'height:25px'])!!} 
                                                    @endif

                                                    {!!Form::label('fechaCreacion-{{$reg}}', 'Fecha Creación', array('class' => 'text-sm text-primary mb-1')) !!}
                                                    {!!Form::text('fechaCreacion-{{$reg}}',$terceroCertificado[$reg]->daFechaCreacionTerceroAdjunto,[ 'class'=>'form-control','readonly','style'=>'height:25px'])!!}

                                                    {!!Form::label('fechaVencimiento-{{$reg}}', 'Fecha Vencimiento', array('class' => 'text-sm text-primary mb-1')) !!}
                                                    {!!Form::text('fechaVencimiento-{{$reg}}',$terceroCertificado[$reg]->daFechaVencimientoTerceroAdjunto,[ 'class'=>'form-control','readonly','style'=>'height:25px'])!!}
                                                </div>
                                            @endif
                                            <?php $reg ++; ?> 
                                        @endwhile
                                    </div>
                                    <div class="dz-message">
                                        <div class="col-xs-8">
                                            <div class="message">
                                                <p>haz click para buscar los archivos.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fallback">
                                        <input type="file" name="file" multiple style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br> 
            @endwhile
        </div>
    </div><br>

    @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
    {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
    @endif
@endsection


