{!!Html::script('js/gridselect.js'); !!}

@extends('layouts.modal') 
@section('content')

  <?php
        $valor = $_GET["valor"];
        $reg = $_GET["reg"];
        $tipo = $_GET["tipo"];
        $tabla = $_GET["tabla"];

  ?>

  <style>
  tfoot input {
    width: 100%;
    padding: 3px;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  </style>

  <script type="text/javascript">
    $(document).ready( function () {
        var valor = "<?php echo $valor; ?>";
        var tipo = "<?php echo $tipo; ?>";
        var tabla = "<?php echo $tabla; ?>";
        
        configurarGridSelect('ttercero',"{!! URL::to ('Asn_TerceroDataSelect?valor="+valor+"&tipo="+tipo+"')!!}");
        var table = $('#ttercero').DataTable();

        $('#ttercero tbody').on( 'dblclick', 'tr', function () {
          $(this).toggleClass('selected');

          var datos = table.rows('.selected').data();
          var reg = "<?php echo $reg; ?>";
          window.parent.adicionarRegistros("ttercero", datos,reg,tabla);

        } );
    });
  </script>

  <div class="container">
    <div class="row">
      <div class="container">
        <br>
        <div class="btn-group" style="margin-left: 94%;margin-bottom:4px" title="Columns">
          <button type="button" class="btn btn-default dropdown-toggle"data-toggle="dropdown">
            <i class="glyphicon glyphicon-th icon-th"></i>
            <span class="caret"></span>
          </button>
          
          <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a class="toggle-vis" data-column="0"><label> Iconos</label></a></li>
            <li><a class="toggle-vis" data-column="1"><label> Id</label></a></li>
            <li><a class="toggle-vis" data-column="2"><label> Documento</label></a></li>
            <li><a class="toggle-vis" data-column="3"><label> Nombre</label></a></li>
            <li><a class="toggle-vis" data-column="4"><label> Nombre Comercial</label></a></li>
          </ul>
        </div>


        <table id="ttercero" name="ttercero" class="display table-bordered" width="100%">
          <thead>
            <tr class="btn-success active">
                <th><b>Id</b></th>
                <th><b>Documento</b></th>
                <th><b>Nombre</b></th>
                <th><b>Nombre Comercial</b></th>
            </tr>
          </thead>
          <tfoot>
            <tr class="btn-default active">
                <th><b>Id</b></th>
                <th><b>Documento</b></th>
                <th><b>Nombre</b></th>
                <th><b>Nombre Comercial</b></th>
            </tr>
          </tfoot>
        </table>
        
      </div>
    </div>
  </div>
@stop
