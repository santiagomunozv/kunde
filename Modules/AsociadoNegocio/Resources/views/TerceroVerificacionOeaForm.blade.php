
@extends('layouts.principal')
@section('nombreModulo')
Verificacion OEA
@endsection
@section('scripts')
<script type="text/javascript">   
    let valores = '<?php echo (isset($preguntas) ? json_encode($preguntas) : "");?>';
    let valoresDetalle = '<?php echo (isset($preguntaDetalle) ? json_encode($preguntaDetalle) : "");?>';
    
    </script>
    {{Html::script('modules/asociadonegocio/js/TerceroVerificacionOea.js')}}
@endsection
@section('contenido')
{!!Form::model($preguntas,['url'=>['/asociadonegocio/terceroverificacion',$id],'method'=>'PUT', 'id'=>'terceroverificacionoea','onsubmit' => 'return false'])!!}
{!!Form::hidden('txTipoTereceroVerificacion',$tipo,array('id' => 'txTipoTereceroVerificacion'))!!}
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
        <input type="hidden" id="Preguntas_oidTerceroVerificacion" name="Preguntas_oidTerceroVerificacion" value="">
        <div class="card-body multi-max">
            <table id="tabla-verificacion" class="table multiregistro table-sm table-hover table-borderless">
                <thead>
                    <tr>
                        <th></th>
                        <th class="bg-primary text-light">Tipo</th>
                        <th class="bg-primary text-light">Preguntas</th>
                        <th class="bg-primary text-light">Porcentaje</th>
                        <th class="bg-primary text-light">Clasificación</th>
                    </tr>
                </thead>
                <tbody id="contenedorterceroverificacion"></tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                {!!Form::label('dePonderacionTerceroVerificacionOea', 'Ponderación', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::text('dePonderacionTerceroVerificacionOea',(isset($verificacionOea->dePonderacionTerceroVerificacionOea) ? $verificacionOea->dePonderacionTerceroVerificacionOea : null),[ 'class'=>'form-control','readonly'])!!}
            </div>
            <div class="col-md-6">
                {!!Form::label('txImpactoTerceroVerificacionOea', 'Impacto', array('class' => 'text-sm text-primary mb-1')) !!}
                {!!Form::text('txImpactoTerceroVerificacionOea',(isset($verificacionOea->txImpactoTerceroVerificacionOea) ? $verificacionOea->txImpactoTerceroVerificacionOea : null),[ 'class'=>'form-control','readonly'])!!}
            </div>
        </div><br>
        @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
        {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        @endif
    </div>
</div>

@endsection