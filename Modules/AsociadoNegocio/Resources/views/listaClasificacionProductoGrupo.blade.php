<div class="table-responsive">
    <table id="grupo-table" class="table table-hover table-sm scalia-grid">
        <thead class="bg-primary text-light">
            <tr>
                <th>Grupo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($grupos as $grupo)
                <tr data-id-grupo="{{$grupo->oidClasificacionProductoGrupo}}" data-nombre-grupo="{{$grupo->txNombreClasificacionProductoGrupo}}">
                    <td>{{$grupo->txNombreClasificacionProductoGrupo}}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>