@extends('layouts.principal')
@section('nombreModulo')
Grupos de Terceros
@endsection
@section('scripts')
<script>
    let GrupoTercero = {!! $grupoTercero !!}
    let correo = {!! $correo !!}
</script>
{{Html::script('modules/asociadonegocio/js/GrupoTercero.js')}}
@endsection
@section('contenido')
{!!Form::model($clasificacionTercero,['url'=>'asociadonegocio/grupotercero/'.$id,'method'=>'PUT', 'id'=>'GrupoTercero', 'onsubmit' => 'return false;'])!!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txNombreClasificacionTercero', 'Clasificacion Tercero', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txNombreClasificacionTercero',null,[ 'class'=>'form-control','readonly'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txNombreClasificacionTerceroGrupo', 'Grupo', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txNombreClasificacionTerceroGrupo',null,[ 'class'=>'form-control','readonly'])!!}
                </div>
            </div>
        </div>
        <input type="hidden" id="eliminarGrupoTercero" name="eliminarGrupoTercero" value="">
            <input type="hidden" id="ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo" name="ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo" value="1">                                       
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="grupoTercero.agregarCampos([],'L');">
                                <i  class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th class="required">Código</th>
                        <th class="required">Nombre</th>
                        @if($barraCorreo == 'si')
                        <th class="required">Correo</th>
                        <th class="required">Mensaje</th>
                        @endif
                        <th>Observación</th>
                        <tbody id="contenedorGrupoTercero"></tbody>
                    </thead>
                </table>
            </div>
        
    </div>
    {!!Form::button('Guardar',['type'=>'button',"class"=>"btn btn-primary", "onclick"=>"grabar();"])!!}
    {!! Form::close() !!}

@endsection
