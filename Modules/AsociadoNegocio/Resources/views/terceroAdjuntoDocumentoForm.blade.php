@extends('layouts.principal')

@section('nombreModulo')
    Adjunto
@endsection

@section('scripts')
<script>
    let terceroadjuntodocumento = '<?php echo json_encode($terceroadjuntodocumento); ?>';
    let oidTipoDocumento = '<?php echo json_encode($oidTipoDocumento); ?>';
    let txNombreTipoDocumento = '<?php echo json_encode($txNombreTipoDocumento); ?>';

</script>
    {{Html::script("modules/asociadonegocio/js/terceroAdjuntoDocumentoForm.js")}} 
@endsection

@section('contenido')

    {!! Form::model($terceroadjuntodocumento, ['url' => ['asociadonegocio/terceroadjuntodocumento'], 'method' => 'PUT', 'id' => 'form-terceroadjuntodocumento'])
    !!}

    <div class="div card border-left-primary shadow h-100 py-2">
        <div class="div card-body">
            <input type="hidden" id="eliminarTerceroAdjuntoDocumento" name="eliminarTerceroAdjuntoDocumento" value="">
            <div class="div card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionTerceroAdjuntoDocumento.agregarCampos([],'L');">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Descripción</th>
                        <th>Tipo de documento</th>
                        <th>Fecha</th>
                        <th>Adjunto</th>
                        <tbody id="contenedorTerceroAdjuntoDocumento">
                        </tbody>
                    </thead>
                </table>
                {!! Form::button('Modificar', ['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"]) !!}
            </div>
        </div>
    </div>

@endsection
