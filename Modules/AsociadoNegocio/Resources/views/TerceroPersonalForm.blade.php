@extends('layouts.principal')
    @section('nombreModulo')
    Información Personal
    @endsection

    @section('scripts')
    {{Html::script('modules/asociadonegocio/js/TerceroPersonal.js')}}
    @endsection
@section('contenido')
{!!Form::model($asn_terceropersonal,['url'=>['asociadonegocio/terceropersonal',$id],'method'=>'PUT', 'id'=>'form-asn_terceropersonal', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {!!Form::hidden('oidTerceroPersonal', (isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["oidTerceroPersonal"] : null), array('id' => 'oidTerceroPersonal')) !!}
            {!!Form::hidden('Tercero_oidTercero_1a1', (isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["Tercero_oidTercero_1a1"] : null), array('id' => 'Tercero_oidTercero_1a1')) !!}
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            {!!Form::label('lsSexoTerceroPersonal', 'Sexo', array('class' => 'required text-sm text-primary mb-1')) !!}
                            {!!Form::select('lsSexoTerceroPersonal',['Masculino'=>'Masculino','Femenino'=>'Femenino'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsSexoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el Sexo.'])!!}
                        </div>
                    </div>
                </div> 
                
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            {!!Form::label('lsGrupoSanguineoTerceroPersonal', 'Grupo Sanguíneo', array('class' => 'required text-sm text-primary mb-1')) !!}
                            {!!Form::select('lsGrupoSanguineoTerceroPersonal',['A'=>'A','B'=>'B','AB'=>'AB','O'=>'O'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsGrupoSanguineoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>''])!!}
                        </div>
                        <div class="col-sm-6">
                            {!!Form::label('lsRHTerceroPersonal', 'RH', array('class' => 'required text-sm text-primary mb-1')) !!}
                            {!!Form::select('lsRHTerceroPersonal',['+'=>'+','-'=>'-'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsRHTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>''])!!}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('daFechaExpedicionTerceroPersonal', 'Fecha Exp. Documento', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('daFechaExpedicionTerceroPersonal',(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["daFechaExpedicionTerceroPersonal"] : null),[ 'class'=>'date-picker form-control','placeholder'=>'Seleccione la Fecha de Exp..','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('Ciudad_oidExpedicion','Ciudad Exp. Documento', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Ciudad_oidExpedicion',$tercerociudad,(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["Ciudad_oidExpedicion"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Ciudad.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('daFechaNacimientoTerceroPersonal', 'Fecha de Nacimineto', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('daFechaNacimientoTerceroPersonal',(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["daFechaNacimientoTerceroPersonal"] : null),[ 'class'=>'date-picker form-control', 'placeholder'=>'Seleccione la fecha de Nacimiento.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('Ciudad_oidNacimiento', 'Ciudad de Nacimiento', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Ciudad_oidNacimiento',$tercerociudad,(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["Ciudad_oidNacimiento"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la ciudad de Nacimiento.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsEstadoCivilTerceroPersonal', 'Estado Civil', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsEstadoCivilTerceroPersonal',['Soltero'=>'Soltero','Casado'=>'Casado','Divorciado'=>'Divorciado','Viudo'=>'Viudo','Union Libre'=>'Union Libre','Separado'=>'Separado'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsEstadoCivilTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el Estado Civil.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('inEstratoTerceroPersonal', 'Estrato', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('inEstratoTerceroPersonal',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["inEstratoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el Estrato.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsEscolaridadTerceroPersonal', 'Escolaridad', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsEscolaridadTerceroPersonal',['Primara'=>'Primara','Secundaria'=>'Secundaria','Técnica'=>'Técnica','Tecnología'=>'Tecnología','Pregrado'=>'Pregrado' ,'Posgrado'=>'Posgrado','Maestría'=>'Maestría','Doctorado'=>'Doctorado'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsEscolaridadTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Escolaridad.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txTituloTerceroPersonal', 'Título', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('txTituloTerceroPersonal',(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["txTituloTerceroPersonal"] : null),[ 'class'=>'date-picker form-control', 'placeholder'=>'Ingrese el título.','autocomplete'=>'off'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsViviendaTerceroPersonal', 'Tipo de vivienda', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsViviendaTerceroPersonal',['Casa'=>'Casa','Apartamento'=>'Apartamento','Finca'=>'Finca','Casa finca'=>'Casa finca'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsViviendaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el Tipo de Vivienda.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsCaracteristicaViviendaTerceroPersonal', 'Característica de vivienda', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsCaracteristicaViviendaTerceroPersonal',['Propia'=>'Propia','Arrendada'=>'Arrendada','Familiar'=>'Familiar'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsCaracteristicaViviendaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la característica de la vivienda.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsZonaViviendaTerceroPersonal', 'Zona de vivienda', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsZonaViviendaTerceroPersonal',['Urbana'=>'Urbana','Rural'=>'Rural'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsZonaViviendaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Zona de Vivienda.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsTransporteTerceroPersonal', 'Transporte para llegar al trabajo', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsTransporteTerceroPersonal',['Caminar'=>'Caminar','Bicileta'=>'Bicileta','Moto'=>'Moto','Particular'=>'Particular','Transporte público'=>'Transporte público','Otro'=>'Otro'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsTransporteTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el transporte.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('inEstratoServiciosPublicosTerceroPersonal', 'Estrato de servicios públicos', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('inEstratoServiciosPublicosTerceroPersonal',['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["inEstratoServiciosPublicosTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el Estrato de Servicios Públicos.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsEnergiaTerceroPersonal', 'Energía', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsEnergiaTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsEnergiaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsAlcantarilladoTerceroPersonal', 'Alcantarillado', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsAlcantarilladoTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsAlcantarilladoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsAcueductoTerceroPersonal', 'Acueducto', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsAcueductoTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsAcueductoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsGasTerceroPersonal', 'Gas', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsGasTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsGasTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsBasuraTerceroPersonal', 'Recolección de basura', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsBasuraTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsBasuraTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsTelefonoTerceroPersonal', 'Teléfono fijo', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsTelefonoTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsTelefonoTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsConsumeAlcoholTerceroPersonal', 'Consume alcohol', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsConsumeAlcoholTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsConsumeAlcoholTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsPracticaActividadTerceroPersonal', 'Practica actividad física', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsPracticaActividadTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsPracticaActividadTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsFumadorTerceroPersonal', 'Fumador', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsFumadorTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsFumadorTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsExfumadorTerceroPersonal', 'Ex fumador', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsExfumadorTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsExfumadorTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsDeudaTerceroPersonal', 'Cuenta con deudas', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsDeudaTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsDeudaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsRefinanciarDeudaTerceroPersonal', 'Desea consolidar las deudas', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsRefinanciarDeudaTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsRefinanciarDeudaTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsComputadorTerceroPersonal', 'Tiene computador', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsComputadorTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsComputadorTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsInternetTerceroPersonal', 'Acceso a internet', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsInternetTerceroPersonal',['Si'=>'Si','No'=>'No'],(isset($asn_terceropersonal[0]) ? $asn_terceropersonal[0]["lsInternetTerceroPersonal"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione una opción.'])!!}
                </div>
            </div>
            
            
            <br>
            @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
            {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @endif 
        </div>
    </div>
    
@endsection
        


