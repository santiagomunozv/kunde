@extends('layouts.formatoImpresion')
@section('estilos')
    <style>
    td{
        color: #000;
    }
    .titulo{
        font-size:20px;
        font-weight:bold;
    }
    label{
        font-size:16px;
        font-weight:bold;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #202FC4;
        color: white;
    }
    </style>
@endsection
@section('contenido')
<div class="row">
    <div class="col-md-6">
        <table width="100%" height="100%" style="border-bottom:1px solid #000000">
            <tr>
                <td>
                    <tr>
                        <td class="titulo" width="60%" height="60%"> {{$terceroPrint->txNombreTercero}}</td>
                    </tr>
                    <tr>
                        <td>Nit:  {{$terceroPrint->txDocumentoTercero}}</td>
                    </tr>
                    <tr>
                        <td>Teléfono:  {{$terceroPrint->txTelefonoTercero}}</td>
                    </tr>
                    <tr>
                        <td>Correo Electrónico:  {{$terceroPrint->txCorreoElectronicoTercero}}</td>
                    </tr>
                    <tr>
                        <td>Ciudad:  {{$terceroPrint->txNombreCiudad}}</td>
                    </tr>  
                    <tr>
                        <td>Direccion:  {{$terceroPrint->txDireccionTercero}}</td>
                    </tr>
                    <tr>
                        <td>Tipo Servicio:  {{$terceroPrint->lmTipoServicioTercero}}</td>
                    </tr>
                    <tr>
                        <td>Clasificación:  {{$terceroPrint->lmClasificacionTercero}}</td>
                    </tr>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <img class="img-fluid" style="max-height:200px" src="{{$terceroPrint->imImagenTercero ? '/asociadonegocio/consultaimagen/'.$terceroPrint->oidTercero : '/asociadonegocio/camera-512.png'}}"> 
    </div>
</div>
    <div class="table-responsive">
        <table id="hojavida-table" class="table table-hover table-sm">
            <thead class="bg-primary text-light" align="center">
                <tr>
                    <th colspan="2">información Comercial y Tributaria</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="w-50">Forma de pago compra:</th>
                    <td class="w-50">{{$terceroPrint->txNombreFormaPago}}</td>
                </tr>
                <tr>
                    <th>Régimen:</th>
                    <td>{{$terceroPrint->lsRegimenVentaTerceroTributario}}</td>
                </tr>
                <tr>
                    <th>Naturaleza jurídica:</th>
                    <td>{{$terceroPrint->txNombreNaturalezaJuridica}}</td>
                </tr>
                <tr>
                    <th>Actividad Económica:</th>
                    <td>{{$terceroPrint->txNombreActividadEconomica}}</td>
                </tr>
                <tr>
                    <th>Clasificación Renta:</th>
                    <td>{{$terceroPrint->txNombreClasificacionRenta}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    @if ($ct == 1)
        <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light" align="center">
                    <tr>
                        <th colspan="2">información Logística</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="w-50">Cierre contable:</th>
                        <td class="w-50">{{$terceroPrint->inCierreContableTerceroLogistica}} {{$terceroPrint->lsCierreContableTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Tiempo permanencia en bodega:</th>
                        <td>{{$terceroPrint->txTiempoPermanenciaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Manejo de muestras:</th>
                        <td>{{$terceroPrint->lsManejoMuestrasTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Montaje de pedidos:</th>
                        <td>{{$terceroPrint->lsMontajePedidoTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Frecuencia pedidos:</th>
                        <td>{{$terceroPrint->inFrecuenciaPedidoTerceroLogistica}}  {{$terceroPrint->lsFrecuenciaPedidoTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Sitio de entrega:</th>
                        <td>{{$terceroPrint->lmSitioEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Horario de entrega:</th>
                        <td>{{$terceroPrint->txHorarioEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Nombre de entrega:</th>
                        <td>{{$terceroPrint->txNombreEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Direccion de entrega:</th>
                        <td>{{$terceroPrint->txDireccionEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Telefono de entrega:</th>
                        <td>{{$terceroPrint->txTelefonoEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Requisitos de entrega:</th>
                        <td>{{$terceroPrint->lmRequisitoEntregaTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>Manejo de devoluciones:</th>
                        <td>{{$terceroPrint->txManejoDevolucionesTerceroLogistica}}</td>
                    </tr>
                    <tr>
                        <th>observaciones:</th>
                        <td>{{$terceroPrint->txObservacionTerceroLogistica}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif
    <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light" align="center">
                    <tr>
                        <th colspan="2">Grupos</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 0;
                        $totalReg = count($terceroGrupo);
                     
                        while($i < $totalReg)
                        {
                            $nombreClasificacionTerceroGrupo = $terceroGrupo[$i]->txNombreClasificacionTerceroGrupo;
                            echo 
                            "<tr>
                                <th class='w-50'>$nombreClasificacionTerceroGrupo :</th><td class='w-50'>";
                                while ($i < $totalReg && $nombreClasificacionTerceroGrupo == $terceroGrupo[$i]->txNombreClasificacionTerceroGrupo) 
                            { 
                                $NombreGrupo = $terceroGrupo[$i]['txNombreGrupoTercero'];
                                $observacion = (isset($terceroGrupo[$i]['txObservacionTerceroGrupo']) ? 'Observación: ('.$terceroGrupo[$i]['txObservacionTerceroGrupo'].')' : '');
                                echo" $NombreGrupo, $observacion";
                                    $i++;      
                            }
                            echo "</td></tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light" align="center">
                    <tr>
                        <th colspan="2">Certificados</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terceroCertificado as $certificado)
                        <tr>
                        <th class="w-50">{{$certificado->txNombreTipoCertificado}}:</th>
                            <td class="w-50">{{$certificado->lsVerificacionTerceroCertificado}}</td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light" align="center">
                    <tr>
                        <th colspan="2">Verificación OEA Confección</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terceroVerificacion as $verificacion)
                        <tr>
                        <th class="w-50">{{$verificacion->txNombrePreguntaOea}}:</th>
                            <td class="w-50">{{$verificacion->lsImpactoTerceroVerificacion}}</td>
                        </tr>
                    @endforeach
                    @if (isset($terceroVerificacion[0]->dePonderacionTerceroVerificacionOea))
                        <tr style="color: #fff;background-color: #000;">
                            <th></th>
                            <td></td>
                        </tr>
                        <tr>
                            <th class="w-50">PONDERACIÓN:</th>
                            <td class="w-50">{{$terceroVerificacion[0]->dePonderacionTerceroVerificacionOea}}</td>
                        </tr>
                        <tr>
                            <th class="w-50">IMPACTO:</th>
                            <td class="w-50">{{$terceroVerificacion[0]->txImpactoTerceroVerificacionOea}}</td>
                        </tr>
                    @endif
 
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light" align="center">
                    <tr>
                        <th colspan="2">Verificación OEA Comercializacion</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terceroVerificacionComercializacion as $verificacion)
                        <tr>
                        <th class="w-50">{{$verificacion->txNombrePreguntaOea}}:</th>
                            <td class="w-50">{{$verificacion->lsImpactoTerceroVerificacion}}</td>
                        </tr>
                    @endforeach
                    @if (isset($terceroVerificacionComercializacion[0]->dePonderacionTerceroVerificacionOea))
                        <tr style="color: #fff;background-color: #000;">
                            <th></th>
                            <td></td>
                        </tr>
                        <tr>
                            <th class="w-50">PONDERACIÓN:</th>
                            <td class="w-50">{{$terceroVerificacionComercializacion[0]->dePonderacionTerceroVerificacionOea}}</td>
                        </tr>
                        <tr>
                            <th class="w-50">IMPACTO:</th>
                            <td class="w-50">{{$terceroVerificacionComercializacion[0]->txImpactoTerceroVerificacionOea}}</td>
                        </tr>
                    @endif
 
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table id="hojavida-table" class="table table-hover table-sm">
                <thead class="bg-primary text-light">
                    <tr>
                        <th>Nombre Contactos</th>
                        <th>Movil Contacto</th>
                        <th>Correo Contacto</th>
                        <th>Tipo Contacto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($terceroContacto as $contacto)
                        <tr>
                        <td >{{$contacto->txNombreTerceroContacto}}</td>
                            <td >{{$contacto->txMovilTerceroContacto}}</td>
                            <td >{{$contacto->txCorreoTerceroContacto}}</td>
                            <td >{{$contacto->txTipoTerceroContacto}}</td>
                        </tr>
                    @endforeach
                    
                </tbody>
            </table>
        </div>

@endsection