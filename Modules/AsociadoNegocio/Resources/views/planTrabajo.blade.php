@extends('plantrabajo::layouts.master')

@section('content')

    <table>
        <thead>
            <tr class="titulo">
                <th>TEMAS</th>
                <th>PARTICIPANTES</th>
                <th>MODALIDAD</th>
                <th>FECHA</th>
                <th>HORA</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($planTrabajo as $plan)
                <tr>
                    <td style="background-color: {{ $plan->txColorEvento }}; color: #fff;">
                        {{ $plan->tema }}
                    </td>
                    <td>{{ $plan->participantes }}</td>
                    <td>{{ $plan->modalidad }}</td>
                    <td>{{ \Carbon\Carbon::parse($plan->fecha)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($plan->hora)->format('g:i a') }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <style>
        table {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 10px;
            text-align: center;
        }

        .titulo {
            background-color: #0F105B;
            color: white;
            font-weight: bold;
        }

        tbody tr:nth-child(even) {
            background-color: #f9f9f9;
        }

        tbody tr:hover {
            background-color: #f1f1f1;
        }

        td:first-child {
            font-size: 12px;
            text-transform: uppercase;
        }

        img {
            display: block;
            margin: 0 auto;
        }
    </style>
@stop
