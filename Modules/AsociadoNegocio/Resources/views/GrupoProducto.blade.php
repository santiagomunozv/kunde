@extends('layouts.principal')

@section('nombreModulo')Grupos de Productos @endsection
@section('scripts')
    <script>
        let idTercero = {{ $id }};
        let clasificacionGrupos = {!! $clasificacionGrupos !!};
    </script>
    {{Html::script('modules/asociadonegocio/js/GrupoProducto.js')}}
@endsection
@section('contenido')
    {!!Form::open(['url' => ['/asociadonegocio/grupoproducto',$id],'method' => 'PUT', 'id' => 'GrupoProducto', 'onsubmit' => 'return false'])!!}
        <div class="card border-left-primary shadow h-100 py-2">
            <input type="hidden" id="eliminarGrupoProducto" name="eliminarGrupoProducto" value="">
            <input type="hidden" id="eliminarClasificaciones" name="eliminarClasificaciones" value="">
            <div class="row">
                <div class="col-md-2 m-1 ">
                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="modalClasificaciones()">
                        <i class="fa fa-plus"></i>Agregar Clasificaciones
                    </button>
                </div>
                <nav class="col">
                    <div class="nav nav-tabs" id="navClasificaciones" role="tablist"></div>
                </nav>
            </div>
            <div class="card-body">
                <div class="tab-content" id="tabClasificaciones">
                </div>
            </div>
        </div>
        <br>
        @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
            {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        @endif
@endsection
