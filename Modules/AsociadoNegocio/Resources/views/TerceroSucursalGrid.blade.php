@extends('layouts.principal')
@section('nombreModulo')
    Tercero Sucursal
@stop

@section('scripts')
<script type="text/javascript">
    $(function(){
        configurarGrid('tercerosucursal-table');
    });
    var funcionDireccion = ["ondblclick","abrirModalDireccion(this.id)"];

</script>
    {!!Html::script('modules/asociadonegocio/js/TerceroSucursal.js'); !!}
@endsection

@section('estilos')
<style type="text/css">
    .claseBoton{
      text-decoration: none;
      padding: 10px;
      font-weight: 600;
      font-size: 20px;
      color: #ffffff;
      background-color: #1883ba;
      border-radius: 6px;
      border: 2px solid #0016b0;
    }
  </style>
@endsection

@section('contenido')
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div class="table-responsive">
            <table id="tercerosucursal-table" class="table table-hover table-sm scalia-grid">
                <thead class="bg-primary text-light">
                    <tr>
                        <th style="width: 150px;" data-orderable="false">
                        <a class="btn btn-primary btn-sm text-light" onclick="modalSucursalCrear('{{$idPadre}}')">
                                <i class="fa fa-plus"></i>
                            </a>
                            <button type="button" id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light" >
                                <i class="fas fa-broom"></i>
                            </button>
                            <button type="button" id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                        </th>
                        <th>Código</th>
                        <th>GLN (EAN)</th>
                        <th>Nombre Comercial</th>
                        <th>Ciudad</th>
                        <th>Teléfono</th>
                        <th>Correo Electrónico</th>
                        <th>Dirección</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tercerosucursal as $sucursal)
                        <tr>
                            <td>
                                <div class="btn-group" role="group" aria-label="Acciones">
                                <a class="btn btn-success btn-sm text-light" onclick="modalSucursalEdit('{{$sucursal->oidTerceroSucursal}}')">
                                        <i class="fas fa-pencil-alt"></i>
                                    </a>
                                    <button type="button" onclick="confirmarEliminacion('{{$sucursal->oidTerceroSucursal}}', 'asn_tercerosucursal', 'TerceroSucursal')" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </div>
                            </td>
                            <td>{{$sucursal->txCodigoTerceroSucursal}}</td>
                            <td>{{$sucursal->txCodigoBarrasTerceroSucursal}}</td>
                            <td>{{$sucursal->txNombreComercialTerceroSucursal}}</td>
                            <td>{{$sucursal->txNombreCiudad}}</td>
                            <td>{{$sucursal->txTelefono1TerceroSucursal}}</td>
                            <td>{{$sucursal->txCorreoElectronicoTerceroSucursal}}</td>
                            <td>{{$sucursal->txDireccionTerceroSucursal}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
