@extends('layouts.principal')
@section('nombreModulo')
    Asociados de Negocio: {{ $clasificacion[0]['txNombreClasificacionTercero'] }}
@endsection

@section('estilos')
    {!! Html::style('modules/asociadonegocio/css/Tercero.css') !!}
@endsection
@section('scripts')
    {!! Html::script('modules/asociadonegocio/js/Tercero.js') !!}
    <script type="text/javascript">
        var valorTipoServicio = {!! $tipoServicios !!};
        var valorClasificacion = null;
        $(function() {
            // configurarGridAjax("tercero-table","/asociadonegocio/productoDataAjax?ct={{ $ct }}");
            let columns = [{
                    data: 'edit',
                    name: 'edit',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'oidTercero',
                    name: 'T.oidTercero'
                },
                {
                    data: 'txNombreTipoIdentificacion',
                    name: 'TI.txNombreTipoIdentificacion'
                },
                {
                    data: 'txDocumentoTercero',
                    name: 'T.txDocumentoTercero'
                },
                {
                    data: 'Tercero',
                    name: 'T.txNombreTercero'
                },
                {
                    data: 'txNombreComercialTercero',
                    name: 'T.txNombreComercialTercero'
                },
                {
                    data: 'txTelefonoTercero',
                    name: 'T.txTelefonoTercero'
                },
                {
                    data: 'txMovilTercero',
                    name: 'T.txMovilTercero'
                },
                {
                    data: 'txCorreoElectronicoTercero',
                    name: 'T.txCorreoElectronicoTercero'
                },
                {
                    data: 'creador',
                    name: 'TCR.txNombreTercero'
                },
                {
                    data: 'daFechaCreacionTercero',
                    name: 'T.daFechaCreacionTercero'
                },
                {
                    data: 'modificador',
                    name: 'TMD.txNombreTercero'
                },
                {
                    data: 'daFechaModificacionTercero',
                    name: 'T.daFechaModificacionTercero'
                },
                {
                    data: 'txEstadoSistemaGestionTercero',
                    name: 'T.txEstadoSistemaGestionTercero'
                },
                {
                    data: 'txEstadoTercero',
                    name: 'T.txEstadoTercero'
                }
            ];
            laravelDatatable('/asociadonegocio/terceroDataAjax?ct={{ $ct }}', columns, 'tercero-table', [
                [1, 'desc']
            ]);
        });

        function modalTercero(url_) {
            var clasif = "{{ $clasificacion[0]['txNombreClasificacionTercero'] }}";
            modal.cargando()
            $.get(url_, function(resp) {
                modal.mostrarModal('Asociados de negocio: ' + clasif, resp, function() {
                    //esta es la accion del boton aceptar
                    modal.cargando();
                }).extraGrande().sinPie();
                llenarValoresChosen(valorTipoServicio, valorClasificacion)
                iniciarChosenSelect();
                generarDropzone();
            }).fail(function(resp) {
                modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
            });
        }
    </script>
@endsection
@section('estilos')
    <style>
        .errspan {
            float: left;
            margin-left: 6px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
            color: gray;
        }

        .searchGrid {
            padding-left: 23px;
        }
    </style>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="tercero-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <button class="btn btn-primary btn-sm text-light"
                                        onclick="modalTercero('/asociadonegocio/tercero/create?ct={{ $ct }}')">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                @endif

                                <a id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </a>
                                <a id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </a>

                                <button id="btnUpload" class="btn btn-primary btn-sm text-light" onclick="cargarTareas()">
                                    <i class="fas fa-upload"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Tipo Documento</th>
                            <th>Documento</th>
                            <th>Nombre</th>
                            <th>Nombre Comercial</th>
                            <th>Telefono</th>
                            <th>Movil</th>
                            <th>Correo</th>
                            <th>Usuario Creador</th>
                            <th>Fecha creación</th>
                            <th>Usuario Modificador</th>
                            <th>Fecha Modificador</th>
                            <th>Estado Sistema Gestión</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
