
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Certificado Vencido</title>
</head>
    <body>
        <p>El sistema encontro un certificado vencido de un tercero con los siguientes datos:</p>
        <p>nombre del tercero: {{$nombreTercero}} </p>
        <p>Documento del tercero: {{$documentoTercero}} </p>
        <p>fecha del vencimiento del certificado: {{$fechaCertificado}}</p>
        <p>Tipo de certificado: {{$nombreTipoCertificado}} </p>
        <p>Por favor revisar si es necesario actualizar este certificado en el modulo de Asociados de Negocio en la opción de certificados.</p>
    </body>
</html>