@extends('layouts.principal')
@section('nombreModulo')
Cuentas Bancarias
@endsection 
@section('scripts')
<script>
    let idTercero = {{ $id }};
    let bancos = {!! $bancos !!}
    let idTerceroL = '<?php echo (isset($idTercero) ? json_encode($idTercero) : "");?>';
    let nombreTercero = '<?php echo (isset($nombreTercero) ? json_encode($nombreTercero) : "");?>';
</script>
{{Html::script('modules/asociadonegocio/js/TerceroBancoForm.js')}}  
@endsection 
@section('contenido')
    {!!Form::open(['url'=>['/asociadonegocio/tercerobanco',$id],'method'=>'PUT', 'id' => 'tercerobanco', 'onsubmit' => 'return false'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" id="eliminarCentroCosto" name="eliminarCentroCosto" value="">
            <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="bancoTercero.agregarCampos([],'L');">
                                <i  class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Banco</th>
                        <th class="required">Nombre Cuenta</th>
                        <th class="required">Titular</th>
                        <th class="required">Tipo</th>
                        <th class="required">Numero Cuenta</th>
                        <tbody id="contenedorBanco"></tbody>
                    </thead>
                </table>
            </div>
            @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
            {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @endif
        </div>
    </div>
@endsection
