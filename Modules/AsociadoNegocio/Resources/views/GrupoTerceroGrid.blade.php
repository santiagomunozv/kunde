@extends('layouts.principal')
@section('nombreModulo')
    Grupo de Terceros
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("grupotercero-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="grupotercero-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Clasificación</th>
                            <th>Grupo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clasificacionTercero as $clasificacionTerceroreg)
                            <tr>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/asociadonegocio/grupotercero', [$clasificacionTerceroreg->oidClasificacionTerceroGrupo, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $clasificacionTerceroreg->txNombreClasificacionTercero }}</td>
                                <td>{{ $clasificacionTerceroreg->txNombreClasificacionTerceroGrupo }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Clasificación</th>
                            <th>Grupo</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
