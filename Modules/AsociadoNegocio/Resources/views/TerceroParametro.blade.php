@extends('layouts.principal')
@section('nombreModulo')
Parametros Tercero
@endsection
@section('scripts')
{{Html::script('modules/asociadonegocio/js/TerceroParametroInforme.js')}}
@endsection
@section('estilos')  
@endsection

@section('contenido')
    {!!Form::open(['url'=>["/asociadonegocio/generarCondicion/$accion"],'target'=>'_blank','method'=>'GET', 'id'=>'terceroParametroInforme'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('fechaInicialCreacion', 'Fecha inicial de creación', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::date('fechaInicialCreacion',null,['class'=>'form-control','readonly'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('fechaFinalCreacion','Fecha Final de creación', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::date('fechaFinalCreacion',null,[ 'class'=>'form-control','readonly'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('clasificacionTerceroTipo', 'Clasificación Tercero (Tipo Tercero)', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('clasificacionTerceroTipo[]',$clasificacion,null,['class'=>'chosen-select','id'=>'clasificacionTerceroTipo','multiple','onchange'=>'mostrarGrupos()'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('clasificacionTercero','Clasificación', array('class' =>'text-sm text-primary mb-1')) !!}
                    {!!Form::select('clasificacionTercero[]',['Excluido'=>'Excluido','Eventual'=>'Eventual','Consumo'=>'Consumo','Seguimiento'=>'Seguimiento'],null,['class'=>'chosen-select','multiple'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('CompaniaTercero', 'Compañia Tercero', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('CompaniaTercero[]',$compania,null,['class'=>'chosen-select','id'=>'CompaniaTercero','multiple'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('tipoServicio', 'Tipo servicio', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('tipoServicio[]',['Confeccion'=>'Confección','Comercializacion'=>'Comercialización','Logistica'=>'Logística'],null,['class'=>'chosen-select','id'=>'tipoServicio','multiple'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txEstadoSistemaGestionTercero','Estado Sistemas de Gestión', array('class' =>'text-sm text-primary mb-1')) !!}
                    {!!Form::select('txEstadoSistemaGestionTercero',['Activo'=>'Activo','Inactivo'=>'Inactivo',],null,['class'=>'form-control','placeholder'=>'Seleccione un estado de gestión'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('EstadoTercero','Estado Tercero', array('class' =>'text-sm text-primary mb-1')) !!}
                    {!!Form::select('EstadoTercero',['Activo'=>'Activo','Inactivo'=>'Inactivo',],null,['class'=>'form-control','placeholder'=>'Seleccione un estado'])!!}
                </div>
            </div>
        </div>
    </div>
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="accordion" id="accordionExample">
                <?php $reg = 0; ?>
                @while ($reg < count($clasificacionTercero))
                    <?php $nombreClasificacion = $clasificacionTercero[$reg]->txNombreClasificacionTercero ?>
                    <div class="d-none" id="{{str_replace(' ','',$nombreClasificacion)}}">
                        <div class="card-header" id="headingOne{{$reg}} ">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$reg}}" aria-expanded="true" aria-controls="collapseOne{{$reg}}">
                                    {{$nombreClasificacion}}
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne{{$reg}}" class="collapse hidde" aria-labelledby="headingOne{{$reg}}" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    @while ($reg < count($clasificacionTercero) AND $nombreClasificacion == $clasificacionTercero[$reg]->txNombreClasificacionTercero )
                                        <div class="col-sm-6" id="grupo" >
                                            {!!Form::label('GrupoTercero_oidTerceroGrupo',$clasificacionTercero[$reg]->txNombreClasificacionTerceroGrupo, array('class' => 'text-sm text-primary mb-1')) !!}
                                            {!!Form::select('GrupoTercero_oidTerceroGrupo['.$clasificacionTercero[$reg]->oidClasificacionTercero.']['.$clasificacionTercero[$reg]->oidClasificacionTerceroGrupo.']',$clasificacionTercero[$reg]->grupoTercero($clasificacionTercero[$reg]->oidClasificacionTerceroGrupo)->pluck('txNombreGrupoTercero','oidGrupoTercero'),null,[ 'class'=>'form-control','placeholder'=>'Seleccione'])!!}
                                        </div>
                                        <?php $reg ++ ?>
                                    @endwhile
                                </div>
                            </div>
                        </div>
                    </div>
                @endwhile
            </div>
        </div>
    </div>

    {!!Form::submit('Consultar',["class"=>"btn btn-primary"])!!}
    {{-- {!!Form::button('Modificar',['type' => 'button' ,"class"=>"btn btn-primary", "onclick"=>"validaciones()"])!!} --}}

@endsection


