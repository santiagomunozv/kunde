@extends('layouts.principal')
@section('nombreModulo')
Información Comercial
@endsection
@section('scripts')
    {{Html::script('modules/asociadonegocio/js/TerceroComercial.js')}}
    <script type="text/javascript">
    var valorAsn_TerceroImpuesto = {!! $terceroimpuesto !!};
    $(function(){
        valorConcepto =  Array("IVA","Impoconsumo","Ret. Fuente","Ret. Iva","Ret. Ica","Ret. CREE");
        nombreConcepto =  Array("IVA","Impoconsumo", "Ret. Fuente", "Ret. Iva","Ret. Ica","Ret. CREE");
        let Concepto = [valorConcepto,nombreConcepto];

        terceroimpuesto = new GeneradorMultiRegistro('terceroimpuesto','contenedorTerceroImpuesto','terceroimpuesto');
        terceroimpuesto.campoid = 'oidTerceroImpuesto';
        terceroimpuesto.campoEliminacion = 'eliminarTerceroImpuesto';

        terceroimpuesto.campos = ['oidTerceroImpuesto','lsConceptoTerceroImpuesto','chProveedorAplicaTerceroImpuesto','chProveedorBase0TerceroImpuesto','chClienteAplicaTerceroImpuesto','chClienteBase0TerceroImpuesto']

        terceroimpuesto.etiqueta = ['input','select','checkbox','checkbox','checkbox','checkbox'];
        terceroimpuesto.tipo = ['hidden','text','checkbox','checkbox','checkbox'];
        terceroimpuesto.opciones = ['',Concepto,'','',''];
        terceroimpuesto.clase = ['','','','',''];
        terceroimpuesto.sololectura = [true,false,false,false,false];
        terceroimpuesto.estilo = ['','','','',''];

        valorAsn_TerceroImpuesto.forEach( dato => {terceroimpuesto.agregarCampos(dato , 'L');
        });
    });
                        
    </script>
@endsection
@section('contenido')
        {!!Form::model($asn_tercerocomercial,['url'=>['asociadonegocio/tercerocomercial',$id],'method'=>'PUT', 'id'=>'form-asn_tercerocomercial', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                {!!Form::hidden('oidTerceroComercial', (isset($asn_tercerocomercial[0]) ? $asn_tercerocomercial[0]["oidTerceroComercial"] : null), array('id' => 'oidTerceroComercial')) !!}
                {!!Form::hidden('Tercero_oidTercero_1a1', (isset($asn_tercerocomercial[0]) ? $asn_tercerocomercial[0]["Tercero_oidTercero_1a1"] : null), array('id' => 'Tercero_oidTercero_1a1')) !!}
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('Tercero_oidVendedor', 'Vendedor / Comprador', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('Tercero_oidVendedor',$tercerovendedor,(isset($asn_tercerocomercial[0]) ? $asn_tercerocomercial[0]["Tercero_oidVendedor"] : null),['class'=>'select form-control','placeholder'=>'Seleccione el vendedor.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('FormaPago_oidCompra', 'Forma de pago Compra', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('FormaPago_oidCompra',$terceroformapago,(isset($asn_tercerocomercial[0]) ? $asn_tercerocomercial[0]["FormaPago_oidCompra"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese la forma de pago Compra.'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('FormaPago_oidVenta', 'Forma de pago Venta', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('FormaPago_oidVenta',$terceroformapago,(isset($asn_tercerocomercial[0]) ? $asn_tercerocomercial[0]["FormaPago_oidVenta"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese la forma de pago Venta.'])!!}
                </div>
            </div>
        </div>
    </div><br>
        
        <h3>Información tributaria</h3>
        
        <!-- Información tributaria -->
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
                {!!Form::hidden('oidTerceroTributario', (isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["oidTerceroTributario"] : null), array('id' => 'oidTerceroTributario')) !!}
                <div class="col-sm-12"> 
                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chPracticaRetencionTerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chPracticaRetencionTerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chPracticaRetencionTerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    {!!Form::label('chPracticaRetencionTerceroTributario', 'Practica retención en la fuente', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            {!!Form::label('lsRegimenVentaTerceroTributario', 'Régimen de ventas', array('class' => 'required text-sm text-primary mb-1')) !!}
                            {!!Form::select('lsRegimenVentaTerceroTributario',['COMUN'=>'Comun','SIMPLIFICADO'=>'Simplificado','SIN REGIMEN'=>'Sin Régimen'],(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["lsRegimenVentaTerceroTributario"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese el Régimen de ventas.'])!!}
                        </div>
                    </div>

                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chAutoretenedorTerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chAutoretenedorTerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chAutoretenedorTerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    {!!Form::label('chAutoretenedorTerceroTributario', 'Es autoretenedor', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            {!!Form::label('NaturalezaJuridica_oidTerceroTributario', 'Naturaleza jurídica', array('class' => 'text-sm text-primary mb-1')) !!}
                            {!!Form::select('NaturalezaJuridica_oidTerceroTributario',$terceronaturalezajuridica,(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["NaturalezaJuridica_oidTerceroTributario"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Naturaleza Jurídica.'])!!}
                        </div>
                    </div>

                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chAutoreenedorCREETerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chAutoreenedorCREETerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chAutoreenedorCREETerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>      
                                <div class="col-sm-10">
                                    {!!Form::label('chAutoreenedorCREETerceroTributario', 'Autoretenedor Cree', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            {!!Form::label('ActividadEconomica_oidActividadEconomica_1aM[]', 'Actividad Económica', array('class' => 'text-sm text-primary mb-1')) !!}
                            {!!Form::select('ActividadEconomica_oidActividadEconomica_1aM[]',$terceroactividadeconomica,$valorActividadEconomica,['class'=>'chosen-select','multiple','id'=>'ActividadEconomica_oidActividadEconomica_1aM'])!!}
                        </div>
                    </div>

                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chGranContribuyenteTerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chGranContribuyenteTerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chGranContribuyenteTerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    {!!Form::label('chGranContribuyenteTerceroTributario', 'Es gran contribuyente', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-6">
                            {!!Form::label('ClasificacionRenta_oidTerceroTributario', 'Clasificación Renta', array('class' => 'required text-sm text-primary mb-1')) !!}
                            {!!Form::select('ClasificacionRenta_oidTerceroTributario',$terceroclasificacionrenta,(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["ClasificacionRenta_oidTerceroTributario"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Clasificación Renta.'])!!}
                        </div>
                    </div>

                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chEntidadEstadoTerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chEntidadEstadoTerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chEntidadEstadoTerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}     
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    {!!Form::label('chEntidadEstadoTerceroTributario', 'Es entidad estatal', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div><br>

                    <div class="row">                       
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="custom-checkbox-container">
                                        {!!Form::checkbox('chNoResponsableIVATerceroTributario',(isset($asn_tercerotributario[0]) ? $asn_tercerotributario[0]["chNoResponsableIVATerceroTributario"] : null),((isset($asn_tercerotributario[0]) and $asn_tercerotributario[0]["chNoResponsableIVATerceroTributario"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-sm-10">
                                    {!!Form::label('chNoResponsableIVATerceroTributario', 'No es responsable de Iva ', array('class' => 'text-sm text-primary mb-1')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
        </div><br>

        <h3>Cálculos tributarios</h3>
        
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                <input type="hidden" id="eliminarTerceroImpuesto" name="eliminarTerceroImpuesto" value="">
                <div class="card-body multi-max">
                    <table id="tabla-contactos" class="table multiregistro table-sm table-hover table-borderless">
                        <thead class="bg-primary text-light">
                            <tr>     
                                <th colspan="2"></th>
                                <th colspan="2" class="text-center">Proveedor</th>
                                <th colspan="2" class="text-center">Cliente</th>
                            </tr>
                            <tr>
                                <th class="bg-primary text-light" style="width: 31px;">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="terceroimpuesto.agregarCampos([],'A');">
                                        <i  class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th class="required">Impuestos</th>
                                <th class="text-center">Aplica</th>
                                <th class="text-center">Base 0</th>
                                <th class="text-center">Aplica</th>
                                <th class="text-center">Base 0</th>
                            </tr>
                        </thead>
                        <tbody id="contenedorTerceroImpuesto"></tbody>
                    </table>
                </div>
            </div>
        </div><br>
        @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
        {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        @endif 
@endsection
        


