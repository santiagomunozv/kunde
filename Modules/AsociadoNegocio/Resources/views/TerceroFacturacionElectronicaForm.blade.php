@extends('layouts.principal')
    @section('nombreModulo')
    Tercero Factura Eléctronica
    @endsection

    @section('scripts')
    {{Html::script('modules/asociadonegocio/js/TerceroFacturaElectronica.js')}}
    @endsection
@section('contenido')
{!!Form::model($terceroFacturacionElectronica,['url'=>['asociadonegocio/terceroFacturaElectronica',$id],'method'=>'PUT', 'id'=>'form-terceroFacturaElectronica', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {!!Form::hidden('oidTerceroFacturaElectronica',null, array('id' => 'oidTerceroFacturaElectronica')) !!}
            {!!Form::hidden('Tercero_oidTercero_1aM',$id, array('id' => 'Tercero_oidTercero_1a1')) !!}
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM[]', 'Responsabilidades Fiscales', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM[]',$responsabilidadFiscal,$valorResponsabilidadFiscal,['class'=>'chosen-select','multiple','id'=>'ResponsabilidadFiscal_oidResponsabilidadFiscal_1aM'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txMatriculaMercantilTerceroFactura', 'Matrícula mercantil', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txMatriculaMercantilTerceroFactura',null,[ 'class'=>'form-control','id'=>'txMatriculaMercantilTerceroFactura','placeholder'=>'Ingrese el código mercantil'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('TerceroBanco_oidFacturaElectronica', 'Cuenta Bancaria', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('TerceroBanco_oidFacturaElectronica',$cuentasBancarias,null,['class'=>'form-control','id'=>'TerceroBanco_oidFacturaElectronica','placeholder'=>'Seleccione la cuenta'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('RegimenContable_oidTerceroFacturaElectronica', 'Régimen contable', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('RegimenContable_oidTerceroFacturaElectronica',$regimenCotable,null,['class'=>'form-control','id'=>'txCuentaTerceroFacturaElectronica','placeholder'=>'Seleccione un régimen contable'])!!}
                </div>
            </div>
        </div> 
    </div>

    @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
    {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
    @endif 
    
@endsection
        


