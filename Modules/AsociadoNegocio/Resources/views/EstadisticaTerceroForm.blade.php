@extends('layouts.principal')
@section('nombreModulo')
    Estadistica
@endsection
@Section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("enfermedad-table");
        });
        let randoEdades = @json($rangoEdadesContent);
        let sexo = @json($sexoContent);
        let estadoCivil = @json($estadoCivilContent);
        let estrato = @json($estratoContent);
        let personasCargo = @json($numeropersonascargoContent);
        let tipoVivienda = @json($tipoViviendaContent);
        let escolaridad = @json($escolaridadContent);
        let tieneEnfermedad = @json($tieneEnfermedadContent);
        let salario = @json($salarioContent);

        
    </script>
    {{ Html::script('modules/asociadonegocio/js/EstadisticaTercero.js') }}
    {{ Html::script('https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js') }}

@endsection

@section('contenido')


    <div class="row">

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Edades</h6>
                </div>
                <div class="card-body">
                    <canvas id="edades"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Sexo</h6>
                </div>
                <div class="card-body">
                    <canvas id="sexo"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Estado civil</h6>
                </div>
                <div class="card-body">
                    <canvas id="estadoCivil"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Estrato</h6>
                </div>
                <div class="card-body">
                    <canvas id="estrato"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Número personas a cargo</h6>
                </div>
                <div class="card-body">
                    <canvas id="personasacargo"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Tipo de vivienda</h6>
                </div>
                <div class="card-body">
                    <canvas id="vivienda"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Escolaridad</h6>
                </div>
                <div class="card-body">
                    <canvas id="escolaridad"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Salario</h6>
                </div>
                <div class="card-body">
                    <canvas id="salario"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Enfermedades</h6>
                </div>
                <div class="card-body">

                    <table id="enfermedad-table" class="table table-hover table-sm scalia-grid">
                        <thead class="bg-primary text-light">
                            <tr>
                                <th>Enfermedades</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($terceroEmpleado as $terceroEmpleado)
                                @if ($terceroEmpleado->txEnfermedadesEmpleado != null)
                                    <tr>
                                        <td>
                                            {{ $terceroEmpleado->txEnfermedadesEmpleado }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Enfermedades</h6>
                </div>
                <div class="card-body">
                    <canvas id="tieneEnfermedad"></canvas>
                </div>
            </div>
        </div>


        
    </div>












@endsection
