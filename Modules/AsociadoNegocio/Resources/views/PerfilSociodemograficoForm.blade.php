@section('scripts')
    {{ Html::script('modules/asociadonegocio/js/TerceroEmpleado.js') }}
@endsection
{!! Form::model($empleado, ['url' => ['asociadonegocio/perfilsociodemografico', $idEmpleado], 'method' => 'PUT', 'id' => 'form-perfilsociodemografico', 'files' => true]) !!}

<div>
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-perfil-tab" data-toggle="tab" href="#nav-perfil" role="tab"
                aria-controls="nav-perfil" aria-selected="true">PÁGINA 1</a>
            <a class="nav-item nav-link" id="nav-seguridad-tab" data-toggle="tab" href="#nav-seguridad" role="tab"
                aria-controls="nav-seguridad" aria-selected="false">PÁGINA 2</a>
            <a class="nav-item nav-link" id="nav-entretenimiento-tab" data-toggle="tab" href="#nav-entretenimiento"
                role="tab" aria-controls="nav-entretenimiento" aria-selected="false">PÁGINA 3</a>
        </div>
    </nav>

    <div class="tab-content" id="nav-tabContent">
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

        <!-- mostramos los errores de insercion -->
        <div id="errores"></div>
        <!-- CONTACTO NAV -->
        <div class="tab-pane fade show active" id="nav-perfil" role="tabpanel" aria-labelledby="nav-perfil-tab">
            <div class="row gx-3 mb-3">
                <div class="col-md-6">
                    <!-- Form Group (empresa)-->
                    {!! Form::label('txTerceroNombreTercero', 'Empresa', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txTerceroNombreTercero', $empleado->txNombreTercero, ['class' => 'form-control', 'placeholder' => 'Ingrese nombre de la empresa', 'disabled']) !!}
                </div>
                <div class="col-md-6">
                    <!-- Form Group (cargo)-->
                    {!! Form::label('Cargo_oidTerceroEmpleado', 'Cargo', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('Cargo_oidTerceroEmpleado', $empleado->Cargo_oidTerceroEmpleado, ['class' => 'form-control chosen-select', 'placeholder' => 'Ingrese Cargo']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (nombre)-->
                <div class="col-md-4">
                    {!! Form::label('txNombreEmpleado', 'Nombre', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txNombreEmpleado', $empleado->txNombreEmpleado, ['class' => 'form-control', 'placeholder' => 'Ingrese nombre del empleado', 'disabled']) !!}
                </div>
                <!-- Form Group (apellido)-->
                <div class="col-md-4">
                    {!! Form::label('txApellidoEmpleado', 'Apellido', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txApellidoEmpleado', $empleado->txApellidoEmpleado, ['class' => 'form-control', 'placeholder' => 'Ingrese apellido del empleado', 'disabled']) !!}
                </div>
                <!-- Form Group (Cédula)-->
                <div class="col-md-4">
                    {!! Form::label('txDocumentoEmpleado', 'Documento', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txDocumentoEmpleado', $empleado->txDocumentoEmpleado, ['class' => 'form-control', 'placeholder' => 'Ingrese documento del empleado', 'disabled']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (Pais nacimiento)-->
                <div class="col-md-4">
                    {!! Form::label('Pais_oidTerceroEmpleadoN', 'Pais de nacimiento', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('Pais_oidTerceroEmpleadoN', $Pais_empleadoTercero, null, ['class' => 'form-control chosen-select', 'placeholder' => 'Selecciona el departamento.']) !!}
                    {{-- 'onchange' => "consultarCiudad(this.value, 'Ciudad_oidTerceroEmpleadoN')" --}}
                </div>
                <!-- Form Group (ciudad de nacimiento)-->
                <div class="col-md-4">
                    {!! Form::label('Ciudad_oidTerceroEmpleadoN', 'Ciudad de nacimiento', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('Ciudad_oidTerceroEmpleadoN', $Ciudad_empleadoTercero, null, ['class' => 'form-control', 'placeholder' => 'Selecciona la ciudad.']) !!}
                </div>

                <!-- Form Group (Fecha de nacimiento)-->
                <div class="col-md-4">
                    {!! Form::label('dtFechaNacimientoEmpleado', 'Fecha de nacimiento', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaNacimientoEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingresa de nacimiento']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (fondo de pensión)-->
                <div class="col-md-6">
                    {!! Form::label('txFondoPensionEmpleado', 'Fondo de pensión', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txFondoPensionEmpleado', null, ['class' => 'form-control chosen-select', 'placeholder' => 'Ingrese fonde de pensión (ARP).']) !!}
                </div> 
                <!-- Form Group (ciudad residencia)-->
                {{-- <div class="col-md-6">
                    {!! Form::label('Ciudad_oidTerceroEmpleadoR', 'Ciudad de residencia', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('Ciudad_oidTerceroEmpleadoR', $Ciudad_empleadoTercero, null, ['class' => 'form-control chosen-select', 'placeholder' => 'Selecciona la ciudad.']) !!}
                </div> --}}
            </div>
            <div class="row gx-3 mb-3">
                <div class="col-md-6">
                    {!! Form::label('txBarrioViviendaEmpleado', 'Barrio de residencia', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txBarrioViviendaEmpleado', null, ['class' => 'form-control chosen-select', 'placeholder' => 'Ingresa el barrio.']) !!}
                </div>
                <!-- Form Group (Dirección residencia)-->
                <div class="col-md-6">
                    {!! Form::label('txDireccionEmpleado', 'Dirección de residencia', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txDireccionEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la Dirección.', 'readonly', 'data-toggle' => 'collapse', 'href' => '#ModalDireccion', 'role' => 'button', 'autocomplete' => 'off']) !!}
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="collapse" id="ModalDireccion">
                            @include('Direccion')
                        </div>
                    </div>
                </div>
            </div>

            <div class="row gx-3 mb-3">
                <!-- Form Group (celular)-->
                <div class="col-md-6">
                    {!! Form::label('txCelularEmpleado', 'Celular', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txCelularEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingrese celular del empleado']) !!}
                </div>
                <!-- Form Group (correo)-->
                <div class="col-md-6">
                    {!! Form::label('txCorreoEmpleado', 'Correo Electrónico', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txCorreoEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingrese correo electrónico del empleado']) !!}
                </div>

            </div>

            <div class="row gx-3 mb-3">
                    <!-- Form Group (Estrato)-->
                <div class="col-md-6">
                    {!! Form::label('lsEstratoEmpleado', 'Estrato', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('lsEstratoEmpleado', ['1', '2', '3', '4', '5', '6'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona']) !!}
                </div>


                <!-- Form Group (tiempo en la empresa)-->
                <div class="col-md-6">
                    {!! Form::label('txTiempoEmpresaEmpleado', 'Tiempo en la empresa (en años)', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('txTiempoEmpresaEmpleado', ['Menos de 1 año' => 'Menos de 1 año', '1 a 3 años' => '1 a 3 años', '3 a 6 años' => '3 a 6 años', '6 a 10 años' => '6 a 10 años', '10 a 15 años' => '10 a 15 años', '16 a más años' => '16 a más años'], null, ['class' => 'form-control', 'placeholder' => 'Ingresa tienpo en la empresa en años']) !!}
                </div>
            </div>

            <!-- END PERFIL 1 -->
        </div>

        <!-- PERFIL 2 NAV-->
        <div class="tab-pane fade" id="nav-seguridad" role="tabpanel" aria-labelledby="nav-seguridad-tab">

            <div class="row gx-3 mb-3">
                <!-- Form Group (EPS)-->
                <div class="col-md-6">
                    {!! Form::label('txEpsEmpleado', 'EPS', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txEpsEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingrese EPS empleado']) !!}
                </div>
                <!-- Form Group (ARL)-->
                <div class="col-md-6">
                    {!! Form::label('Arl_oidEmpleado', 'ARL', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('Arl_oidEmpleado', $EmpleadoARL, null, ['class' => 'form-control', 'placeholder' => 'Selecciona ARL.']) !!}
                </div>
            </div>

            <div class="row gx-3 mb-3">
                <!-- Form Group (horario de trabajo)-->
                <div class="col-md-6">
                    {!! Form::label('txHorarioTrabajoEmpleado', 'Horario de trabajo', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txHorarioTrabajoEmpleado', null, ['class' => 'form-control', 'placeholder' => 'Ingrese horario de trabajo']) !!}
                </div>
                <!-- Form Group (tipo contrato)-->
                <div class="col-md-6">
                    {!! Form::label('txTipoContratoEmpleado', 'Tipo de contrato', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('txTipoContratoEmpleado', ['Fijo' => 'Fijo', 'Indefinido' => 'Indefinido', 'Contrato de aprendizaje' => 'Contrato de aprendizaje', 'Pasantías' => 'Pasantías', 'Prestación de servicios' => 'Prestación de servicios', 'Obra labor' => 'Obra labor'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona Tipo de contrato.']) !!}
                </div>
            </div>

            <div class="row gx-3 mb-3">
                <!-- Form Group (genero)-->
                <div class="col-md-6">
                    {!! Form::label('txGeneroEmpleado', 'Genero', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('txGeneroEmpleado', ['Hombre' => 'Hombre', 'Mujer' => 'Mujer'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona genero.']) !!}
                </div>
                <!-- Form Group (estado civil)-->
                <div class="col-md-6">
                    {!! Form::label('txEstadoCivilEmpleado', 'Estado civil', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('txEstadoCivilEmpleado', ['Soltero/a' => 'Soltero/a', 'Casado/a' => 'Casado/a', 'Unión libre' => 'Unión libre o unión de hecho', 'separado/a' => 'Separado/a', 'Divorciado/a' => 'Divorciado/a', 'Viudo/a' => 'Viudo/a'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona estado civil.']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (edad)-->
                <div class="col-md-4">
                    {!! Form::label('txEdadEmpleado', 'Edad', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('txEdadEmpleado', ['18 - 25 años' => '18 - 25 años', '26- 35 años' => '26- 35 años', '36- 45 años' => '36- 45 años', '45- 55 años' => '45- 55 años', '56 en adelante' => '56 en adelante'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona rango de edad']) !!}
                </div>
                <!-- Form Group (personas a cargo)-->
                <div class="col-md-4">
                    {!! Form::label('intPersonasCargoEmpleado', 'Personas a cargo', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::number('intPersonasCargoEmpleado', null, ['min' => '0', 'max' => '100', 'class' => 'form-control', 'placeholder' => 'Ingresa numero de personas a cargo']) !!}
                </div>
                <!-- Form Group (tipo de vivienda)-->
                <div class="col-md-4">
                    {!! Form::label('lsTipoViviendaEmpleado', 'Tipo de vivienda', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('lsTipoViviendaEmpleado', ['Propia' => 'Propia', 'Arrendada' => 'Arrendada', 'Familiar' => 'Familiar', 'Otra' => 'Otra'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona tipo de vivienda.']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (escolaridad)-->
                <div class="col-md-6">
                    {!! Form::label('lsEscolaridadEmpleado', 'Nivel de escolaridad', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('lsEscolaridadEmpleado', ['Primaria' => 'Primaria', 'Secundaria' => 'Secundaria', 'Técnico/Tecnólogo' => 'Técnico / Tecnólogo', 'Profesional' => 'Profesional', 'Posgrado' => 'Posgrado'], null, ['class' => 'form-control', 'placeholder' => 'Selecciona nivel de escolaridad.']) !!}
                </div>
                <!-- Form Group (Ingresos)-->
                <div class="col-md-6">
                    {!! Form::label('lsIngresosEmpleado', 'Promedio de ingresos (SMLV)', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::select('lsIngresosEmpleado', ['menos de 1 SMLV' => 'menos de 1 SMLV', '1 SMLV' => '1 SMLV', '2 - 3 SMLV' => '2 - 3 SMLV', '6 - 7 SMLV' => '6 - 7 SMLV', 'mas de 7 SMLV' => 'Más de 7 SMLV'], null, ['class' => 'form-control', 'placeholder' => 'Seleccionapromedio de ingresos.']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <!-- Form Group (composición familiar)-->
                <div class="col-md-6">
                    {!! Form::label('ComFamiliarEmpleado', 'Composición familiar', ['class' => 'required text-sm text-primary mb-1']) !!}
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary" type="button" id="btnConsultarFamilia"
                        onclick="consultarFammilia('{{ $gruposFamiliares }}')">
                        <i class="fas fa-download"></i>
                        Cargar Familiares
                    </button>
                </div>
                <div class="col-md-3">
                    <div id="mensajeFamilair" class="alert alert-light" role="alert" style="visibility: hidden">

                    </div>
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <div class="col-md-12">

                    <input type="hidden" id="ComFamiliar_oidEmpleadoTercero_1aM"
                        name="ComFamiliar_oidEmpleadoTercero_1aM" value="1">
                    <input type="hidden" id="eliminarComFamiliar" name="eliminarComFamiliar" value="">

                    <table id="tabla-ComFamiliarEmpleado" name="tabla-ComFamiliarEmpleado"
                        class="table multiregistro table-sm table-hover table-borderless ocultarTabla"
                        style="visibility: hidden">
                        <thead>
                            <tr>
                                <th class="bg-primary text-light size">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="composicionfamiliar.agregarCampos([],'A');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th class="required bg-primary text-light">Parentesco</th>
                                <th class="required bg-primary text-light">Celular</th>
                                <th class="required bg-primary text-light">Nombre Completo</th>
                            </tr>
                        </thead>
                        <tbody id="contenedorcomposicionfamiliar"></tbody>
                    </table>
                </div>
            </div>
            <!-- END PERFIL 2 -->
        </div>


        <!-- PERFIL 3 NAV-->
        <div class="tab-pane fade" id="nav-entretenimiento" role="tabpanel" aria-labelledby="nav-entretenimiento-tab">
            <div class="row gx-3 mb-3">
                <div class="col-md-12">
                    <!-- Form Group (tiepo libre)-->
                    {!! Form::label('txTiempoLibreEmpleado', 'Uso del tiempo libre', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::textarea('txTiempoLibreEmpleado', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Hacer lista de las actividades más recurrentes']) !!}
                </div>
            </div>
            <div class="row gx-3 mb-3">
                <div class="col-md-6">
                    <div class="text-center">
                        <!-- Form Group (deporte)-->
                        {!! Form::checkbox('chDeporteEmpleado', null, false, ['class' => 'form-check-input', 'onclick' => "document.getElementById('txDeporteEmpleado').disabled = !this.checked"]) !!}
                        {!! Form::label('chDeporteEmpleado', '¿Práctica algún deporte?', ['class' => 'text-sm text-primary mb-1']) !!}
                    </div>
                    <!-- Form Group (deporte)-->
                    {!! Form::label('txDeporteEmpleado', '¿Cuales?', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::textarea('txDeporteEmpleado', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Hacer lista de los deportes más recurrentes', 'disabled']) !!}
                </div>
                <div class="col-md-6">
                    <div class="text-center">
                        <!-- Form Group (consumo de sustacias)-->
                        {!! Form::checkbox('chSustanciasEmpleado', 0, false, ['class' => 'form-check-input', 'onclick' => "document.getElementById('txSustanciasEmpleado').disabled = !this.checked"]) !!}
                        {!! Form::label('chSustanciasEmpleado', '¿Consume sustancias alucinógenas?', ['class' => 'text-sm text-primary mb-1']) !!}
                    </div>
                    <!-- Form Group (sustancias)-->
                    {!! Form::label('txSustanciasEmpleado', '¿Cuales?', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::textarea('txSustanciasEmpleado', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Hacer lista de las sustancias alucinógenas que consume', 'disabled']) !!}
                </div>

                <div class="col-md-12">
                    <div class="text-center">
                        <br>
                        <!-- Form Group (tiene enfermedades)-->
                        {!! Form::checkbox('chEnfermedadesEmpleado', 1, false, ['class' => 'form-check-input', 'onclick' => "document.getElementById('txEnfermedadesEmpleado').disabled = !this.checked"]) !!}
                        {!! Form::label('chEnfermedadesEmpleado', '¿Tiene alguna enfermedad?', ['class' => 'text-sm text-primary mb-1']) !!}
                    </div>
                    <!-- Form Group (enfermedades)-->
                    {!! Form::label('txEnfermedadesEmpleado', '¿Cuales?', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::textarea('txEnfermedadesEmpleado', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Hacer lista de las enfermedades', 'disabled']) !!}
                    <br>
                </div>
                {!! Form::button('Guardar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabarPerfil()']) !!}
            </div>
        </div>
    </div>

</div>

{!! Form::close() !!}
