@extends('layouts.principal')
@section('nombreModulo')
    Tercero Grupo
@endsection
@section('scripts')
{{Html::script('modules/asociadonegocio/js/TerceroGrupo.js')}}    
@endsection
@section('contenido')
    {!!Form::model($clasificacionTercero,['url'=>'asociadonegocio/tercerogrupo/'.$id,'method'=>'PUT', 'id'=>'terceroGrupo', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row">
                {!!Form::hidden('ClasificacionTercero_oidTerceroGrupo',$idClasificacion, array('id' => 'ClasificacionTercero_oidTerceroGrupo')) !!}
                @foreach ($clasificacionTercero as $reg => $valor)
                        {!!Form::hidden('oidTerceroGrupo[]',$valor->oidTerceroGrupo, array('id' => 'oidTerceroGrupo')) !!}
                    <div class="col-sm-6" id="grupo{{$reg}}" >
                        {!!Form::label('GrupoTercero_oidTerceroGrupo',$valor->txNombreClasificacionTerceroGrupo, array('class' => 'text-sm text-primary mb-1')) !!}
                        @if($valor->txNombreClasificacionTerceroGrupo == 'Clasificacion')
                        {!!Form::label('clasificacion','(Este campo es para pago de clientes o proveedores)', array('class' => 'text-sm text-danger mb-1')) !!}
                        @endif
                        {!!Form::hidden('obligatorio[]',$valor->chObligatorioClasificacionTerceroGrupo.$valor->txNombreClasificacionTerceroGrupo, array('id' => 'obligatorio')) !!}
                        {!!Form::select('GrupoTercero_oidTerceroGrupo['.$reg.'][]',$valor->grupoTercero($valor->oidClasificacionTerceroGrupo)->pluck('txNombreGrupoTercero','oidGrupoTercero'),$valor->valoresGrupo($id),[ 'class'=>$valor->chMultipleClasificacionTerceroGrupo == 1 ? 'chosen-select':'form-control',$valor->chMultipleClasificacionTerceroGrupo == 1 ? 'multiple':'','placeholder'=>'Seleccione','onchange'=>"validarObservacion(this.value,$reg)"])!!}
                        @if ($valor->observaciones != null)
                        {!!Form::label('nombreObservacion','Observacion de '.$valor->txNombreClasificacionTerceroGrupo, array('class' => 'text-sm text-primary mb-1','id'=>"nombreObservacion$reg")) !!}
                        {!!Form::text('txObservacionTerceroGrupo['.$reg.']',$valor->observaciones, array('class'=>'form-control col-sm-12','id'=>"txObservacionTerceroGrupo$reg",'readonly')) !!}
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
    {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"validarFormulario()"])!!}
    @endif
@endsection