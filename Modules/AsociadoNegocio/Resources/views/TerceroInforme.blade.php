{!!Html::style('css/sb-admin-2.min.css')!!}

    <style>
        #informeTercero {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #informeTercero td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #informeTercero tr:nth-child(even){
            background-color: #f2f2f2;
        }

        #informeTercero tr:hover {
            background-color: #ddd;
        }

        #informeTercero th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #3A64AE;
            color: white;
        }

        .table-responsive {
            border-radius: 10px;
        }

        html, body{
            padding: 0;
            margin: 0;
            height: 100vh;
            width: 100%;
            overflow: auto;
        }

        #informeTercero-table thead .row1 th{
            top: 0px;
            position: sticky;
            z-index: 100;
        }

        #informeTercero-table thead .row2 th{
            top: 47px;
            position: sticky;
            z-index: 100;
        }
        .table-container{
            height: 100%;
            width:100%;
        }
         
    </style>

<div class="table-container">
    <table id="informeTercero-table" class="table table-hover">
        <thead>
            <tr class="row1">
                <th class="bg-primary text-light" colspan="12"> Datos Generales</th> 
                <th class="bg-primary text-light" colspan="{{count($preguntas)+2}}">Datos Verificación OEA</th> 
                <th class="bg-primary text-light" colspan="{{count($certificados)+2}}">Certificados</th> 
                <th class="bg-primary text-light" colspan="{{count($nombreGrupo)+2}}">Grupos</th> 
            </tr>
            <tr class="row2">
                <th class="bg-primary text-light">Compañía</th>
                <th class="bg-primary text-light">Nombre Tercero</th>
                <th class="bg-primary text-light">Nombre Comercial</th>
                <th class="bg-primary text-light">Documento Tercero</th>
                <th class="bg-primary text-light">Correo Tercero</th>
                <th class="bg-primary text-light">Teléfono Tercero</th>
                <th class="bg-primary text-light">Movil Tercero</th>
                <th class="bg-primary text-light">Dirección Tercero</th>
                <th class="bg-primary text-light">Fecha de creación</th>
                <th class="bg-primary text-light">Tipo servicio</th>
                <th class="bg-primary text-light">Clasificación</th>
                <th class="bg-primary text-light">Estado sistema de gestión</th>
                <th class="bg-primary text-light">Estado</th>
                @foreach ($preguntas as $valor)
                    <th class="bg-Secondary text-light">{{$valor->txNombrePreguntaOea}}</th>
                @endforeach
                <th class="bg-Secondary text-light">Impacto</th>
                <th class="bg-Secondary text-light">Ponderación</th>
                @foreach ($certificados as $valor)
                    <th class="bg-Success text-light">{{$valor->txNombreTipoCertificado}}</th>
                @endforeach
                @foreach ($nombreGrupo as $valor)
                    <th class="bg-info text-light">{{$valor->txNombreClasificacionTerceroGrupo}}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @for($i=0; $i < count($datos); $i++)
                <tr>
                    <td >{{$datos[$i]["txNombreCompania"]}}</td>
                    <td >{{$datos[$i]["txNombreTercero"]}}</td>
                    <td >{{$datos[$i]["txNombreComercialTercero"]}}</td>
                    <td >{{$datos[$i]["txDocumentoTercero"]}}</td>
                    <td >{{$datos[$i]["txCorreoElectronicoTercero"]}}</td>
                    <td >{{$datos[$i]["txTelefonoTercero"]}}</td>
                    <td >{{$datos[$i]["txMovilTercero"]}}</td>
                    <td >{{$datos[$i]["txDireccionTercero"]}}</td>
                    <td >{{$datos[$i]["daFechaCreacionTercero"]}}</td>
                    <td >{{$datos[$i]["lmTipoServicioTercero"]}}</td>
                    <td >{{$datos[$i]["lmClasificacionTercero"]}}</td>
                    <td >{{$datos[$i]["txEstadoSistemaGestionTercero"]}}</td>
                    <td >{{$datos[$i]["txEstadoTercero"]}}</td>
                    @foreach ($preguntas as $titulo)
                        <td>{{$datos[$i][$titulo->txNombrePreguntaOea]}}</td>
                    @endforeach
                    <td >{{$datos[$i]["txImpactoTerceroVerificacionOea"]}}</td>
                    <td >{{$datos[$i]["dePonderacionTerceroVerificacionOea"]}}</td>
                    @foreach ($certificados as $titulo)
                        <td>{{$datos[$i][$titulo->txNombreTipoCertificado]}}</td>
                    @endforeach
                    @foreach ($nombreGrupo as $titulo)
                        <td>{{$datos[$i][$titulo->nombreClasificacion]}}</td>
                    @endforeach
                </tr> 
            @endfor 
        </tbody>
    </table>
</div>
