@extends('plantrabajo::layouts.master')

@section('content')
    <img src={{ public_path('/images/LogoKunde.jpeg') }} style="height: 3cm; width: 3cm;">

    <table>
        <tr class="titulo">
            <th colspan="2">RESPONSABILIDADES DEL COLABORADOR</th>
        </tr>
        <tr>
            <th class="titulo">NOMBRE</th>
            <td>{{ $empleado->txNombreTercero }}</td>
        </tr>
        <tr>
            <th class="titulo">DOCUMENTO</th>
            <td>{{ $empleado->txDocumentoTercero }}</td>
        </tr>
        <tr>
            <th class="titulo">CARGO</th>
            <td>{{ $empleado->txNombreCargo }}</td>
        </tr>
        <tr class="titulo">
            <th>N°</th>
            <th>OBLIGACIONES DENTRO DEL SG-SST</th>
        </tr>

        <tr>
            <th class="titulo">1</th>
            <th>Procurar el cuidado integral de su salud. Es decir, seguir las recomendaciones emitidas con respecto a la
                salud y al cuidado de la misma. Además de acogerse a las actividades de los planes y programas de prevención
                y promoción llevados a cabo por la empresa que incluyen actividades como las pausas activas y el porte
                adecuado de elementos y equipos de protección personal.</th>
        </tr>

        <tr>
            <th class="titulo">2</th>
            <th>Suministrar información clara, veraz y completa sobre su estado de salud. Es decir, dar a conocer cualquier
                afección física o mental que pueda poner en riesgo la integridad de la persona y evitar omitir información
                sobre su estado de salud que deba ser considerada en el ejercicio de su labor.</th>
        </tr>

        <tr>
            <th class="titulo">3</th>
            <th>Cumplir las normas, reglamentos, protocolos e instrucciones del Sistema de Gestión de la Seguridad y Salud
                en el Trabajo de la empresa. Es decir, participar de las actividades propuestas y seguir los instructivos e
                instrucciones que se emitan con el fin de tener prácticas seguras y sanas como utilizar de forma adecuada
                los elementos y equipos de protección personal.</th>
        </tr>

        <tr>
            <th class="titulo">4</th>
            <th>Informar oportunamente al empleador o contratante acerca de los peligros y riesgos latentes en su sitio de
                trabajo. Es decir, reportar siempre que identifique alguna situación que pueda poner en riesgo a cualquier
                persona de la empresa y velar por la solución oportuna de la misma.</th>
        </tr>

        <tr>
            <th class="titulo">5</th>
            <th>Participar en las actividades de capacitación en seguridad y salud en el trabajo definido en el plan de
                capacitación del SG-SST de forma activa y procurando el aprendizaje en las mismas.</th>
        </tr>

        <tr>
            <th class="titulo">6</th>
            <th>Participar y contribuir al cumplimiento de los objetivos del Sistema de Gestión de la Seguridad y Salud en
                el Trabajo SG-SST ya que son responsabilidad de cada integrante de la empresa.</th>
        </tr>

        <tr>
            <th class="titulo">7</th>
            <th>Participar y dejar registro fotográfico de las actividades que sirvan como evidencia de la ejecución del
                SG-SST, como lo son las pausas activas, las capacitaciones y las reuniones programadas.</th>
        </tr>

        <tr>
            <th colspan="2">Una vez comprendidas y firmadas, estas responsabilidades pasan a ser parte del manual de
                funciones del empleado, de ser el caso, y su incumplimiento será sancionado según el Código Sustantivo de
                Trabajo estando dentro de las sanciones la posible terminación del contrato con justa causa por parte del
                empleador.</th>
        </tr>
    </table>

    <br><br><br><br>

    <div class="col md-12" style="display: inline-block;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        ___________________________
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        ________________________________
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        FIRMA COLABORADOR
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        FIRMA DEL JEFE INMEDIATO
    </div>

    <br><br>

    * Con la firma del presente, se autoriza a KUNDE S.A.S. a hacer uso de evidencias con el fin de promover la seguridad y
    la salud en el trabajo por el medio que determine, bien sea en redes sociales o a nivel interno.

    <style>
        table {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        td,
        th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        .titulo {
            background-color: #0F105B;
            color: white;
        }
    </style>
@stop
