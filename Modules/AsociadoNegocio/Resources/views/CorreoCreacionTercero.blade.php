
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Creacion de Tercero</title>
    <style>
      #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
      }

      #customers tr:nth-child(even){background-color: #f2f2f2;}

      #customers tr:hover {background-color: #ddd;}

      #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #3a64ae;
        color: white;
      }
    </style>
</head>
    <body>
        <p>Se creo un nuevo tercero en Scalia.</p>
        <p>{{$mensaje}}</p>
        <p>Muchas gracias.</p>
    </body>

    <body>
        <table id="customers">
            <tr>
              <th>Clasificación</th>
              <th>Nombre del Tercero</th>
              <th>Documento del Tercero</th>
            </tr>
            <tr>
              <td>{{$clasificacion}}</td>
              <td>{{$nombreTercero}}</td>
              <td>{{$documentoTercero}}</td>
            </tr>
          </table>
    </body>
</html>