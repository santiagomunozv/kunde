@section('scripts')
    <script type="text/javascript">
        let idTercero = {{ $id }};
    </script>
    {{ Html::script('modules/asociadonegocio/js/TerceroEmpleado.js') }}
@endsection
{!! Form::open(['url' => ['asociadonegocio/importarempleados', $id], 'method' => 'POST', 'id' => 'form-importarempleados', 'files' => true]) !!}

<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">

        <div class="alert alert-warning">
            <strong>Tener en cuenta:</strong> <br>
            1. Los <strong>encabezados</strong> generados por la encuesta de Google no se deben modificar. <br>
            2. No debe haber filas <strong>vacías</strong> entre los registros. <br>
            3. El escaneo de registros inicia desde la fila 2 del documento.
        </div>

        <div class="container">
            <a onclick="plantillaEmpleado()" class="btn">
                <i class="fas fa-file-excel">
                    Descargar Plantilla
                </i>
            </a>
        </div>

        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

        <input type="hidden" name="nombreArchivo" id="nombreArchivo" value="">

        <div class="col-md-12 pb-2">
            <div id="dropzoneImage" class="dropzone-div">
                <div class="dz-message">
                    <div class="col-xs-8">
                        <div class="message">
                            <p>haz click para buscar cargar un archivo.</p>
                        </div>
                    </div>
                </div>
                <div class="fallback">
                    <input type="file" name="file" multiple style="display: none;">
                </div>
            </div>
        </div>

        {!! Form::button('Importar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'importarEmpleados()']) !!}

    </div>
</div>
{!! Form::close() !!}
