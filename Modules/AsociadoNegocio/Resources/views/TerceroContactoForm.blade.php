@extends('layouts.principal')
@section('nombreModulo')
Tercero Contacto
@endsection
@Section('scripts')
    <script type="text/javascript">   
       var valorAsn_TerceroContacto = {!! $contactos !!};
       var idTipoContacto = '<?php echo (isset($tipoContactoId) ? json_encode($tipoContactoId) : "");?>';
       var nombreTipoContacto = '<?php echo (isset($tipoContactoNombre) ? json_encode($tipoContactoNombre) : "");?>';
       
    </script>
    {{Html::script('modules/asociadonegocio/js/TerceroContacto.js')}}
@endsection
@section('contenido')
    {!!Form::model($contactos,['url'=>['asociadonegocio/tercerocontacto',$idPadre],'method'=>'PUT', 'id'=>'form-asn_tercerocontacto'])!!}
    {!!Form::hidden('ClasificacionTercero_oidTerceroContacto',$idClasificacion, array('id' => 'ClasificacionTercero_oidTerceroContacto')) !!}

            <!-- Miltiregistro Contacto -->
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
            <input type="hidden" id="eliminarAsn_TerceroContacto" name="eliminarAsn_TerceroContacto" value="">
            <div class="card-body multi-max">
                    <table id="tabla-contactos" class="table multiregistro table-sm table-hover table-borderless">
                        <thead>
                            <tr>
                                <th class="bg-primary text-light">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="tercerocontacto.agregarCampos([],'A');">
                                        <i  class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th class="required bg-primary text-light">Nombre Contacto</th>
                                <th class="bg-primary text-light">Cargo</th>
                                <th class="required bg-primary text-light">Correo</th>
                                <th class="required bg-primary text-light">Móvil</th>
                                <th class="bg-primary text-light">Formulario</th>
                                <th class="required bg-primary text-light" style="width:300px;">Tipo de Contacto</th>
                            </tr>
                        </thead>
                        <tbody id="contenedortercerocontacto"></tbody>
                    </table>
                </div>
                @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
                {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
                @endif 
        </div>
    </div>
@endsection
