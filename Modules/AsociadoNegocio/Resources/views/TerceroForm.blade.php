@section('scripts')
    <script>
        let servicios = {!! $tipoServicios !!}
    </script>

    {{ Html::script('modules/asociadonegocio/js/Tercero.js') }}
@endsection
@if (isset($asn_tercero->oidTercero))
    {!! Form::model($asn_tercero, [
        'route' => ['tercero.update', $asn_tercero->oidTercero],
        'method' => 'PUT',
        'id' => 'form-asn_tercero',
        'onsubmit' => 'return false;',
        'files' => 'true',
    ]) !!}
@else
    {!! Form::model($asn_tercero, [
        'route' => ['tercero.store'],
        'method' => 'POST',
        'id' => 'form-asn_tercero',
        'onsubmit' => 'return false;',
        'files' => 'true',
    ]) !!}
@endif
<!-- Maestro Tercero -->
{!! Form::hidden('oidTercero', null, ['id' => 'oidTercero']) !!}
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        {{-- mostramos los errores de insercion --}}
        <div id="errores"></div>
        {!! Form::hidden('numeroVerificacion', null, ['id' => 'numeroVerificacion']) !!}
        {!! Form::hidden('nombreVerificacion', null, ['id' => 'nombreVerificacion']) !!}
        {!! Form::hidden(
            'txDocumentoTerceroAnterior',
            $asn_tercero->txDocumentoTercero ? $asn_tercero->txDocumentoTercero : null,
            ['id' => 'txDocumentoTerceroAnterior'],
        ) !!}
        <div class="row">
            <div class="col-md-6">
                {!! Form::label('TipoIdentificacion_oidTercero', 'Tipo Identificación', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}
                {!! Form::select('TipoIdentificacion_oidTercero', $gen_tipoidentificacionlista, null, [
                    'class' => 'select form-control',
                    'onchange' => 'consultarIdentificacion(this.value,0)',
                    'placeholder' => 'Selecciona Tipo Identificación.',
                ]) !!}
            </div>
            <div class="col-md-5">
                {!! Form::label('txDocumentoTercero', 'Documento', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txDocumentoTercero', null, [
                    'class' => 'form-control',
                    'onblur' => 'documentoTercero()',
                    'placeholder' => 'Ingresa el Documento.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
            <div class="col-md-1">
                {!! Form::label('', '', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txDigitoVerificacionTercero', null, [
                    'class' => 'form-control',
                    'id' => 'txDigitoVerificacionTercero',
                    'readonly',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('txCodigoTercero', 'Código o contrato', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txCodigoTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el código o el contrato.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>

            <div class="col-sm-6">
                {!! Form::label('txCorreoElectronicoTercero', 'Correo Electrónico', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}
                {!! Form::text('txCorreoElectronicoTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el Correo Electrónico.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
        </div>

        <div class="collapse" id="divNombres">
            <div class="row">
                <div class="col-sm-6" id="prueba1">
                    {!! Form::label('txPrimerNombreTercero', 'Primer Nombre', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txPrimerNombreTercero', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el Primer Nombre.',
                        'onblur' => 'concatenarNombre()',
                        'autocomplete' => 'off',
                    ]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('txSegundoNombreTercero', 'Segundo Nombre', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::text('txSegundoNombreTercero', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el Segundo Nombre.',
                        'onblur' => 'concatenarNombre()',
                        'autocomplete' => 'off',
                    ]) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('txPrimerApellidoTercero', 'Primer Apellido', ['class' => 'required text-sm text-primary mb-1']) !!}
                    {!! Form::text('txPrimerApellidoTercero', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el Primer Apellido.',
                        'onblur' => 'concatenarNombre()',
                        'autocomplete' => 'off',
                    ]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('txSegundoApellidoTercero', 'Segundo Apellido', ['class' => 'text-sm text-primary mb-1']) !!}
                    {!! Form::text('txSegundoApellidoTercero', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el Segundo Apellido.',
                        'onblur' => 'concatenarNombre()',
                        'autocomplete' => 'off',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('txNombreTercero', 'Razón Social', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txNombreTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa la Razón Social.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
            <div class="col-sm-6">
                {!! Form::label('txNombreComercialTercero', 'Nombre Comercial', ['class' => 'text-sm text-primary mb-1']) !!}
                {!! Form::text('txNombreComercialTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el Nombre Comercial.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('txDireccionTercero', 'Dirección', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txDireccionTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa la Dirección.',
                    'readonly',
                    'data-toggle' => 'collapse',
                    'href' => '#ModalDireccion',
                    'role' => 'button',
                    'autocomplete' => 'off',
                ]) !!}
            </div>

            <div class="col-sm-6">
                {!! Form::label('Ciudad_oidTercero', 'Ciudad', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::select('Ciudad_oidTercero', $tercerociudad, null, [
                    'class' => 'chosen-select',
                    'placeholder' => 'Selecciona la ciudad.',
                ]) !!}
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="collapse" id="ModalDireccion">
                    @include('Direccion')
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('txTelefonoTercero', 'Teléfono', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txTelefonoTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el Teléfono.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
            <div class="col-sm-6">
                {!! Form::label('txMovilTercero', 'Móvil', ['class' => 'required text-sm text-primary mb-1l']) !!}
                {!! Form::text('txMovilTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el Móvil.',
                    'autocomplete' => 'off',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('lmTipoServicioTercero', 'Tipo Servicio', ['class' => 'text-sm text-primary mb-1']) !!}
                {!! Form::select('lmTipoServicioTercero[]', $tipoServicios, null, [
                    'class' => 'chosen-select',
                    'multiple',
                    'id' => 'lmTipoServicioTercero',
                    'style' => 'width:450px;',
                ]) !!}
            </div>
            <div class="col-sm-6 ">
                {!! Form::label('lmClasificacionTercero', 'Tipo de factura', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::select('lmClasificacionTercero[]', [], null, [
                    'class' => 'chosen-select',
                    'multiple',
                    'id' => 'lmClasificacionTercero',
                    'style' => 'width:450px;',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('oidClasificacionTercero[]', 'Clasificaciones (Tipo Tercero)', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}<br>
                {!! Form::select('oidClasificacionTercero[]', $clasificacionTercero, $clasificacionTerceroEdit, [
                    'class' => 'chosen-select',
                    'multiple',
                    'id' => 'oidClasificacionTercero',
                    'style' => 'width:450px;',
                ]) !!}
            </div>
            <div class="col-sm-6 mb-1">
                {!! Form::label('txEstadoTercero', 'Estado', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txEstadoTercero', isset($asn_tercero) ? $asn_tercero->txEstadoTercero : 'Activo', [
                    'class' => 'form-control',
                    'readonly' => 'readonly',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 mb-1">
                {!! Form::label('daFechaCreacionTercero', 'Fecha de creación', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}
                {!! Form::text('daFechaCreacionTercero', null, ['class' => 'form-control', 'readonly']) !!}
            </div>
            <div class="col-sm-6 mb-1">
                {!! Form::label('txResponsableTercero', 'Responsable', ['class' => 'required text-sm text-primary mb-1']) !!}
                {!! Form::text('txResponsableTercero', null, [
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el responsable',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 mb-1">
                {!! Form::label('txUrl', 'URL de la reunión', ['class' => 'text-sm text-primary mb-1']) !!}
                {!! Form::text('txUrl', null, ['class' => 'form-control', 'placeholder' => 'Ingrese url de la reunión']) !!}
            </div>

            <div class="col-sm-6">
                {!! Form::label('ActividadEconomica_oidActividadEconomica', 'Actividad económica', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}
                {!! Form::select('ActividadEconomica_oidActividadEconomica', $actividadEconomica, null, [
                    'class' => 'chosen-select',
                    'placeholder' => 'Selecciona la actividad económica.',
                ]) !!}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                {!! Form::label('Arl_oidArl', 'Arl', [
                    'class' => 'required text-sm text-primary mb-1',
                ]) !!}
                {!! Form::select('Arl_oidArl', $arl, null, [
                    'class' => 'chosen-select',
                    'placeholder' => 'Selecciona la Arl.',
                ]) !!}
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('txObservacionTercero', 'Descripción', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::textArea('txObservacionTercero', isset($reunion[0]) ? $reunion[0]['txObservacionTercero'] : null, [
                        'class' => 'form-control',
                        'style' => 'width:100%',
                        'rows' => 3,
                        'placeholder' => '',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Datos estándares</h6>
            </div>
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-6 mb-1">
                        {!! Form::label('txNombreRepresentanteLegalTercero', 'Representante legal', [
                            'class' => 'text-sm text-primary mb-1',
                        ]) !!}
                        {!! Form::text('txNombreRepresentanteLegalTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Representante legal',
                        ]) !!}
                    </div>

                    <div class="col-sm-6 mb-1">
                        {!! Form::label('inDocumentoRepresentanteLegalTercero', 'Documento representante legal', [
                            'class' => 'text-sm text-primary mb-1',
                        ]) !!}
                        {!! Form::text('inDocumentoRepresentanteLegalTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Documento representante legal',
                        ]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 mb-1">
                        {!! Form::label('txNombreResponsableSSTTercero', 'Responsable SST', ['class' => 'text-sm text-primary mb-1']) !!}
                        {!! Form::text('txNombreResponsableSSTTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Responsable SST',
                        ]) !!}
                    </div>

                    <div class="col-sm-6 mb-1">
                        {!! Form::label('inDocumentoResponsableSSTTercero', 'Documento responsable SST', [
                            'class' => 'text-sm text-primary mb-1',
                        ]) !!}
                        {!! Form::text('inDocumentoResponsableSSTTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Documento responsable SST',
                        ]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 mb-1">
                        {!! Form::label('txMisionTercero', 'Misión', ['class' => 'text-sm text-primary mb-1']) !!}
                        {!! Form::textarea('txMisionTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Misión',
                        ]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 mb-1">
                        {!! Form::label('txVisionTercero', 'Visión', ['class' => 'text-sm text-primary mb-1']) !!}
                        {!! Form::textarea('txVisionTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Visión',
                        ]) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 mb-1">
                        {!! Form::label('txValoresCorporativoTercero', 'Valores corporativos', [
                            'class' => 'text-sm text-primary mb-1',
                        ]) !!}
                        {!! Form::textarea('txValoresCorporativoTercero', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Valores corporativos',
                        ]) !!}
                    </div>
                </div>

            </div>
        </div>

        <br>
        <br>

        <div class="row">
            @if ($asn_tercero->imImagenTercero)
                <div class="cat-image m-5" id="imagen_{{ $asn_tercero->oidTercero }}">
                    <span class="close-preview" onclick="eliminarImagen({{ $asn_tercero->oidTercero }})">&times;</span>
                    <img src="/asociadonegocio/consultaimagen/{{ $asn_tercero->oidTercero }}" class="rounded"
                        height="200">
                </div>
            @endif

            <div id="imagenes"> {{-- -----DIV DONDE SE AGREGAN LOS INPUT DE LAS IMAGENES------------- --}}
                <input type="hidden" id="eliminarImagen" name="eliminarImagen" value="">
            </div>

            <div class="col-sm-12 pb-2">
                <div id="dropzoneImageTercero" class="dropzone-div">
                    <div class="dz-message">
                        <div class="col-xs-8">
                            <div class="message">
                                <p>haz click para agregar una imagen.</p>
                            </div>
                        </div>
                    </div>
                    <div class="fallback">
                        <input type="file" name="file" multiple style="display: none;">
                    </div>
                </div>
            </div>
        </div>

        @if (isset($asn_tercero->oidTercero))
            {!! Form::button('Modificar', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'onclick' => "grabar($asn_tercero->oidTercero)",
            ]) !!}
        @else
            {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
        @endif
        {!! Form::close() !!}
    </div>
</div>
