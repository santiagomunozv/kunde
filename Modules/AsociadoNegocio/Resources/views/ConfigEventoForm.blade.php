@extends('layouts.principal')
@section('nombreModulo')
    Configuración de eventos - {{ $tercero->txNombreTercero }}
@endsection
@Section('scripts')
    <script type="text/javascript">
        let TerceroEventos = '<?php echo isset($terceroEvtPeriodicidad) ? json_encode($terceroEvtPeriodicidad) : ''; ?> ';
        let eventosId = '<?php echo json_encode($eventosId); ?>';
        let eventosNombre = '<?php echo json_encode($eventosNombre); ?>';
    </script>

    {{ Html::script('modules/asociadonegocio/js/ConfiguracionEventoForm.js') }}
@endsection
@section('contenido')
    @if (isset($terceroPeriodicidad))
        {!! Form::model($terceroPeriodicidad, [
            'route' => ['configuracioneventos.update', $terceroPeriodicidad->oidTerceroPeriodicidad],
            'method' => 'PUT',
            'id' => 'form-confevento',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($terceroPeriodicidad, [
            'route' => ['configuracioneventos.store'],
            'method' => 'POST',
            'id' => 'form-confevento',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <!-- Miltiregistro Contacto -->
    <div class="card shadow h-100 py-2">
        <div class="card-body">
            {!! Form::hidden(
                'oidTerceroPeriodicidad',
                $terceroPeriodicidad ? $terceroPeriodicidad->oidTerceroPeriodicidad : null,
                ['id' => 'oidTerceroPeriodicidad'],
            ) !!}
            {!! Form::hidden('Tercero_oidTercero', $idTercero, ['id' => 'Tercero_oidTercero']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('lsPeriodicidadTerceroPeriodicidad', 'Periodicidad', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::select('lsPeriodicidadTerceroPeriodicidad', $optionsPeriodicidad, null, [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Seleccione la periodicidad',
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtFechaInicioTerceroPeriodicidad', 'Fecha de inicio', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::date('dtFechaInicioTerceroPeriodicidad', null, ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('hrHoraInicioPeriodicidadTerceroPeriodicidad', 'Hora estimada inicio', [
                            'class' => 'text-md text-primary mb-1  required ',
                        ]) !!}
                        {!! Form::time('hrHoraInicioPeriodicidadTerceroPeriodicidad', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Cantidad de horas',
                            'autocomplete' => 'off',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('inHorasTerceroPeriodicidad', 'Horas', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::number('inHorasTerceroPeriodicidad', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Cantidad de horas',
                            'autocomplete' => 'off',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Eventos</div>
                <div class="card-body">

                    <input type="hidden" id="eliminarEvento" name="eliminarEvento" value="">
                    <table class="table multiregistro table-sm table-hover table-borderless">
                        <thead class="bg-primary text-light">
                            <th width="50px">
                                <button type="button" class="btn btn-primary btn-sm text-light"
                                    onclick="configuracionEvento.agregarCampos([],'L');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </th>
                            <th>Orden</th>
                            <th>Evento</th>
                            <th>Modalidad</th>
                            <th>Fecha estimada</th>
                            <th>Hora estimada</th>
                        <tbody id="contenedorEvento"></tbody>
                        </thead>
                    </table>

                </div>
            </div>

            {!! Form::button('Simular', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar(0)']) !!}
            {!! Form::button('Guardar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar(1)']) !!}
            {!! Form::button('Generar reporte', [
                'type' => 'button',
                'class' => 'btn btn-info',
                'onclick' => 'generarReporte()',
            ]) !!}
            {!! Form::button('Enviar notficación', [
                'type' => 'button',
                'class' => 'btn btn-success',
                'onclick' => 'enviarNotificacion()',
            ]) !!}
        </div>
    </div>
@endsection
