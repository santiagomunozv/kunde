@extends('layouts.principal')
@section('nombreModulo')
Información Laboral
@endsection
@section('scripts')
<script>
    var idCentroCostos = '<?php echo (isset($idCentroCosto) ? json_encode($idCentroCosto) : "");?>';
    var nombreCentroCostos = '<?php echo (isset($nombreCentroCosto) ? json_encode($nombreCentroCosto) : "");?>';

    var idCentroTrabajos = '<?php echo (isset($idCentroTrabajo) ? json_encode($idCentroTrabajo) : "");?>';
    var nombreCentroTrabajos = '<?php echo (isset($nombreCentroTrabajo) ? json_encode($nombreCentroTrabajo) : "");?>';

    var idCargos = '<?php echo (isset($idCargo) ? json_encode($idCargo) : "");?>';
    var nombreCargos = '<?php echo (isset($nombreCargo) ? json_encode($nombreCargo) : "");?>';

    let terceroCentroCosto = {!! $terceroCentroCosto !!}
    let terceroCentroTrabajo = {!! $terceroCentroTrabajo !!}
    let terceroCargo = {!! $terceroCargo !!}

    </script>
    {{Html::script('modules/asociadonegocio/js/TerceroLaboral.js')}}
@endsection
@section('contenido')
    {!!Form::model($asn_tercerolaboral,['url'=>['asociadonegocio/tercerolaboral',$id],'method'=>'PUT', 'id'=>'form-Asn_TerceroLaboral', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            {!!Form::hidden('oidTerceroLaboral', (isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["oidTerceroLaboral"] : null), array('id' => 'oidTerceroLaboral')) !!}
            {!!Form::hidden('Tercero_oidTercero_1a1', (isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["Tercero_oidTercero_1a1"] : null), array('id' => 'Tercero_oidTercero_1a1')) !!}
            
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsHorarioTerceroLaboral', 'Horario', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsHorarioTerceroLaboral',['8-5' => '8-5', 'Horas' => 'Horas', '8-4' => '8-4', 'Por horas de consultoría y los pedidos y reuniones adicionales' => 'Por horas de consultoría y los pedidos y reuniones adicionales', 'Rotativo' => 'Rotativo', 'Sin horario' => 'Sin horario', 'Extra laboral' => 'Extra laboral', 'Indefinido' => 'Indefinido'],(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["lsHorarioTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione el horario'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('daFechaIngresoTerceroLaboral','Fecha ingreso', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('daFechaIngresoTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["daFechaIngresoTerceroLaboral"] : null))!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsVinculacionTerceroLaboral', 'Vinculación', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsVinculacionTerceroLaboral',['Prestación de servicios' => 'Prestación de servicios', 'Horas' => 'Horas', 'Fijo' => 'Fijo', 'Por definir' => 'Por definir', 'Contrato' => 'Contrato', 'N/A' => 'N/A', 'Indefinido' => 'Indefinido'],(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["lsVinculacionTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la vinculación'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('deSalarioTerceroLaboral','Salario', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('deSalarioTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["deSalarioTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese el salario','autocomplete'=>'off'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('deBonoTerceroLaboral', 'Bono', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('deBonoTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["deBonoTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese el bono de nómina','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsCabezaFamiliaTerceroLaboral','Cabeza de familia', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsCabezaFamiliaTerceroLaboral',['Si' => 'Si', 'No' => 'No'],(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["lsCabezaFamiliaTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione si es cabeza de familia'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('inPersonasHogarTerceroLaboral','Personas en el hogar', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('inPersonasHogarTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["inPersonasHogarTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese las personas en el hogar.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('inPersonasDiscapacidadTerceroLaboral','Personas a Cargo', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('inPersonasDiscapacidadTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["inPersonasDiscapacidadTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese las con discapacidad en el hogar.','autocomplete'=>'off'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('GrupoNomina_oidTerceroLaboral', 'Grupo Nómina', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('GrupoNomina_oidTerceroLaboral',$gruponominalista,(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["GrupoNomina_oidTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione Grupo Nómina.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('inPersonasACargoTerceroLaboral','Personas a Cargo', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('inPersonasACargoTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["inPersonasACargoTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese las personas a cargo.','autocomplete'=>'off'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('inNumeroHijosTerceroLaboral','Número de Hijos', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('inNumeroHijosTerceroLaboral',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["inNumeroHijosTerceroLaboral"] : null),[ 'class'=>'form-control','placeholder'=>'Ingrese el Número de Hijos.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('deIngresosFamiliares','Ingresos Familiares', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('deIngresosFamiliares',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["deIngresosFamiliares"] : null),[ 'class'=>'form-control','placeholder'=>'Digite los Ingresos Familiares.','autocomplete'=>'off'])!!}
                </div>
            </div>

            <div class="row">

                <div class="col-sm-6">
                    {!!Form::label('daFechaRetiroPension','Fecha Retiro Pensión', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('daFechaRetiroPension',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["daFechaRetiroPension"] : null))!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('Tercero_oidSalud', 'Administradora de Salud', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Tercero_oidSalud',$terceroSalud,(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["Tercero_oidSalud"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Salud.'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('Tercero_oidPension', 'Administradora de Pensión', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Tercero_oidPension',$terceroPension,(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["Tercero_oidPension"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la pensión..'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('Tercero_oidCesantias', 'Administradora de Cesantías', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Tercero_oidCesantias',$terceroCesantia,(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["Tercero_oidCesantias"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione las Cesantías.'])!!}
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('Tercero_oidCaja', 'Caja Compensación Familiar', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('Tercero_oidCaja',$terceroCompensacion,(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["Tercero_oidCaja"] : null),[ 'class'=>'form-control','placeholder'=>'Seleccione la Caja de Compensación.'])!!}
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-1">
                            <label class="custom-checkbox-container">
                                {!!Form::checkbox('chExtranjeroPension',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["chExtranjeroPension"] : null),((isset($asn_tercerolaboral[0]) and $asn_tercerolaboral[0]["chExtranjeroPension"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-sm-11">
                            {!!Form::label('chExtranjeroPension', 'Extranjero no obligado a cotizar pensiones', array('class' => 'text-sm text-primary mb-1')) !!}
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-sm-1">
                            <label class="custom-checkbox-container">
                                {!!Form::checkbox('chColombianoExterior',(isset($asn_tercerolaboral[0]) ? $asn_tercerolaboral[0]["chColombianoExterior"] : null),((isset($asn_tercerolaboral[0]) and $asn_tercerolaboral[0]["chColombianoExterior"] == 1) ? true : null),['class'=>'form-control','style'=>'height:30px;'])!!}
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-sm-11">
                            {!!Form::label('chColombianoExterior', 'Colombiano residente en el exterior', array('class' => 'text-sm text-primary mb-1')) !!}
                        </div>
                    </div>
                </div>
            </div><br>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <input type="hidden" id="eliminarCargos" name="eliminarCargos" value="">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <div class="card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="cargo.agregarCampos([],'L');">
                                        <i  class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Cargos</th>
                                <tbody id="contenedorCargo"></tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div><br>

            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <input type="hidden" id="eliminarCentroCosto" name="eliminarCentroCosto" value="">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <div class="card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="centroCosto.agregarCampos([],'L');">
                                        <i  class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Centro costo</th>
                                <tbody id="contenedorCentroCosto"></tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div><br>

            
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <input type="hidden" id="eliminarCentroTrabajo" name="eliminarCentroTrabajo" value="">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <div class="card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="centroTrabajo.agregarCampos([],'L');">
                                        <i  class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Centro trabajo</th>
                                <tbody id="contenedorCentroTrabajo"></tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div><br>





            @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
            {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @endif 
        </div>
    </div>
 @endsection
        


