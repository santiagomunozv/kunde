
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div id="imagenes"> {{-- -----DIV DONDE SE AGREGAN LOS INPUT DE LAS IMAGENES------------- --}}
            <input type="hidden" id="eliminarAdjunto" name="eliminarAdjunto" value="">
        </div> 
        <div class="row">
            <div class="col-md-12 pb-2">
                <div id="dropzoneImage" class="dropzone-div">
                    <div class="dz-message">
                        <div class="col-xs-8">
                            <div class="message">
                                <p>Arrastra los elementos aqui o haz click para buscar el archivo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="fallback">
                        <input type="file" name="file" multiple style="display:">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>