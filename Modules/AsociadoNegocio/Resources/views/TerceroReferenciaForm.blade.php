@extends('layouts.principal')
@section('nombreModulo')
Referencias
@endsection
@Section('scripts')
{!!Html::script('js/general.js'); !!}
{{Html::script('modules/asociadonegocio/js/TerceroReferencia.js')}}

<script type="text/javascript">
       
    // Multi de tercero personal
    var valorAsn_TerceroPersonal =  {!! $asn_terceropersonal !!};
    var nuevoTerceroPersonal = ['','Personal','','','','',''];

    // Multi Tercero Comercial
    var valorAsn_TerceroComercial = {!! $asn_tercerocomercial !!};
    var nuevoTerceroComercial = ['','Comercial','','','','',''];

    //Multi Tercero Convivencia
    var valorAsn_TerceroConvivencia = {!! $asn_terceroconvivencia !!};
    var nuevoTerceroConviviencia = ['','Convivencia','','','','','',''];

    // Multi de Tercero Familiares
    var valorAsn_TerceroFamiliar = {!! $asn_tercerofamiliar !!};
    var nuevoTerceroFamiliares = ['','Familiar','','','','',''];

    // Tercero Financieras
    var valorAsn_TerceroFinancieras = {!! $asn_tercerofinancieras !!};
    var nuevoTerceroFinancieras = ['','Financiera','','','','',''];

</script>
@endsection

@section('contenido')

	{!!Form::model($asn_terceropersonal,['url'=>['/asociadonegocio/terceroreferencia',$id],'method'=>'PUT', 'id'=>'form-asn_terceroreferencia', 'onsubmit' => 'return false;'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#referenciasPersonales" role="tab" aria-controls="pills-home" aria-selected="true">Personales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-comercial-tab" data-toggle="pill" href="#referenciasComerciales" role="tab" aria-controls="pills-profile" aria-selected="false">Comerciales</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-convivencia-tab" data-toggle="pill" href="#referenciasConvivencia" role="tab" aria-controls="pills-contact" aria-selected="false">Convivencia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-Familiares-tab" data-toggle="pill" href="#referenciasFamiliares" role="tab" aria-controls="pills-contact" aria-selected="false">Familiares</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-Financieras-tab" data-toggle="pill" href="#referenciasFinancieras" role="tab" aria-controls="pills-contact" aria-selected="false">Financieras</a>
                </li>
            </ul>

            <!-- Referencias Personales -->
            
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="referenciasPersonales" role="tabpanel" aria-labelledby="pills-home-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <input type="hidden" id="eliminarAsn_TerceroPersonal" name="eliminarAsn_TerceroPersonal" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>    
                                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="terceropersonal.agregarCampos(nuevoTerceroPersonal,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                            <th class="required">Nombre</th>
                                            <th>Ocupación</th>
                                            <th class="required">Teléfono</th>
                                            <th>Dirección</th>
                                            <th>Observación</th>
                                </thead>
                                    <tbody id="contenedorasn_terceropersonal"  style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Referencias Comerciales -->
                <div class="tab-pane fade" id="referenciasComerciales" role="tabpanel" aria-labelledby="pills-comercial-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <input type="hidden" id="eliminarAsn_TerceroComercial" name="eliminarAsn_TerceroComercial" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>    
                                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="tercerocomercial.agregarCampos(nuevoTerceroComercial,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                        <th class="required">Nombre</th>
                                        <th>Parentesco</th>
                                        <th>Ocupación</th>
                                        <th class="required">Teléfono</th>
                                        <th>Dirección</th>
                                        <th>Observación</th>
                                </thead>
                                    <tbody id="contenedorasn_tercerocomercial"  style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <!-- Referencias Convivencia -->
                <div class="tab-pane fade" id="referenciasConvivencia" role="tabpanel" aria-labelledby="pills-convivencia-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <input type="hidden" id="eliminarAsn_TerceroConvivencia" name="eliminarAsn_TerceroConvivencia" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>    
                                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="terceroconvivencia.agregarCampos(nuevoTerceroConviviencia,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                        <th class="required">Nombre</th>
                                        <th>Parentesco</th>
                                        <th>Fecha Nacimiento</th>
                                        <th>Nivel Educativo</th>
                                        <th>Ocupación</th>
                                        <th>Observación</th>
                                        <th>Teléfono</th>
                                </thead>
                                    <tbody id="contenedorasn_terceroconvivencia"  style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Referencias Familiares -->
                <div class="tab-pane fade" id="referenciasFamiliares" role="tabpanel" aria-labelledby="pills-familiares-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <input type="hidden" id="eliminarAsn_TerceroFamiliar" name="eliminarAsn_TerceroFamiliar" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>    
                                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="tercerofamiliar.agregarCampos(nuevoTerceroFamiliares,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                        <th class="required">Nombre</th>
                                        <th>Parentesco</th>
                                        <th>Ocupación</th>
                                        <th class="required">Teléfono</th>
                                        <th>Dirección</th>
                                        <th>Observación</th>
                                </thead>
                                    <tbody id="contenedorasn_tercerofamiliar"  style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Referencias Financieras -->
                <div class="tab-pane fade" id="referenciasFinancieras" role="tabpanel" aria-labelledby="pills-financieras-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">                                       
                    <input type="hidden" id="eliminarAsn_TerceroFinancieras" name="eliminarAsn_TerceroFinancieras" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>    
                                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="tercerofinanciero.agregarCampos(nuevoTerceroFinancieras,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                        <th class="required">Nombre</th>
                                        <th class="required">Teléfono</th>
                                        <th>Dirección</th>
                                        <th>Observación</th>
                                </thead>
                                    <tbody id="contenedorasn_tercerofinancieras"  style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    
            @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
            {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @endif
@endsection
