
@extends('layouts.principal')
@section('nombreModulo')
Información Logística
@endsection
@section('scripts')
{{Html::script('modules/asociadonegocio/js/TerceroLogistica.js')}}
@endsection
@section('contenido')
    {!!Form::model($terceroLogistica,['url'=>['/asociadonegocio/tercerologistica',$id],'method'=>'PUT', 'id'=>'terceroLogistica','onsubmit' => 'return false'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {!!Form::hidden('oidTerceroLogistica',null, array('id' => 'oidTerceroLogistica')) !!}
            {!!Form::hidden('Tercero_oidTercero_1aM',$id, array('id' => 'Tercero_oidTercero_1aM')) !!}

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txCierreContableTerceroLogistica', 'Cierre Contable', array('class' => 'text-sm text-primary mb-1')) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            {!!Form::text('inCierreContableTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el cierre contable.','autocomplete'=>'off'])!!}
                        </div>
                        <div class="col-sm-6">
                            {!!Form::select('lsCierreContableTerceroLogistica',['Dia del mes'=>'Dia del mes','Antes de fin de mes'=>'Antes de fin de mes'],null,[ 'class'=>'form-control','placeholder'=>'Seleccione un tipo de cierre','autocomplete'=>'off'])!!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txTiempoPermanenciaTerceroLogistica', 'Tiempo Permanencia en bodega (Dias)', array('class' => 'required text-sm text-primary mb-1l')) !!}
                    {!!Form::number('txTiempoPermanenciaTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa tiempo de permanecia.','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsManejoMuestrasTerceroLogistica', 'Manejo de Muestras', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('lsManejoMuestrasTerceroLogistica',['Factura'=>'Factura','Remision'=>'Remision'],null,[ 'class'=>'form-control','placeholder'=>'Seleccione el manejo de muestras','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lsMontajePedidoTerceroLogistica', 'Montaje de pedidos', array('class' => 'text-sm text-primary mb-1l')) !!}
                    {!!Form::select('lsMontajePedidoTerceroLogistica',['EDI'=>'Via EDI','PDF'=>'PDF','Excel'=>'Excel formato Iblu','Correo'=>'Correo electrónico'],null,[ 'class'=>'form-control','placeholder'=>'Seleccione tiempo de permanencia','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lsFrecuenciaPedidoTerceroLogistica', 'Frecuencia de Pedidos', array('class' => 'text-sm text-primary mb-1')) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            {!!Form::text('inFrecuenciaPedidoTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la frecuencia.','autocomplete'=>'off'])!!}
                        </div>
                        <div class="col-sm-6">
                            {!!Form::select('lsFrecuenciaPedidoTerceroLogistica',['Dias'=>'Dias','Meses'=>'Meses','Años'=>'Años'],null,[ 'class'=>'form-control','placeholder'=>'Seleccione la frecuencia','autocomplete'=>'off'])!!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    {!!Form::label('chRequiereShowRoomTerceroLogistica', 'Requiere ShowRoom', array('class' => 'text-sm text-primary mb-1l')) !!}
                    <label class="custom-checkbox-container">
                        {!!Form::checkbox('chRequiereShowRoomTerceroLogistica',null,null,['class'=>'form-control'])!!}
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txPrefijoProductoTercero', 'Prefijo Ref:', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txPrefijoProductoTercero',($tercero[0]->txPrefijoProductoTercero ? $tercero[0]->txPrefijoProductoTercero : null),[ 'class'=>'form-control','placeholder'=>'Ingresa el prefijo del producto.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6 mb-1">
                    {!!Form::label('txGrupoSizfraTercero', 'Grupo Sizfra', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txGrupoSizfraTercero',($tercero[0]->txGrupoSizfraTercero ? $tercero[0]->txGrupoSizfraTercero : null),[ 'class'=>'form-control','placeholder'=>'Ingresa el Grupo Sizfra.'])!!}   
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('TipoProducto_oidTerceroLogistica', 'Tipo de Producto', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('TipoProducto_oidTerceroLogistica',$tipoProducto,null,[ 'class'=>'form-control','placeholder'=>'Seleccione.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6 mb-1">
                    {!!Form::label('CanalDistribucion_oidTerceroLogistica', 'Canal de Distribución', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('CanalDistribucion_oidTerceroLogistica',$canalDistribucion,null,[ 'class'=>'form-control','placeholder'=>'Seleccione.'])!!}   
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('TipoEtiqueta_oidTerceroLogistica', 'Tipo de Etiqueta', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('TipoEtiqueta_oidTerceroLogistica',$tipoEtiqueta,null,[ 'class'=>'form-control','placeholder'=>'Seleccione.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6 mb-1">
                    {!!Form::label('ProcesoLogistico_oidTerceroLogistica', 'Proceso Logístico', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::select('ProcesoLogistico_oidTerceroLogistica',$procesoLogistico,null,[ 'class'=>'form-control','placeholder'=>'Seleccione.'])!!}   
                </div>
            </div>
        </div>
    </div>

    <h3>Información de Entrega</h3>
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lmSitioEntregaTerceroLogistica', 'Sitio de Entrega', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::select('lmSitioEntregaTerceroLogistica[]',['Almacen'=>'Almacen','CEDI'=>'CEDI'],null,[ 'class'=>'chosen-select','id'=>'lmSitioEntregaTerceroLogistica','multiple','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txHorarioEntregaTerceroLogistica', 'Horario de Atención', array('class' => 'text-sm text-primary mb-1l')) !!}
                    {!!Form::text('txHorarioEntregaTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese los horarios de atención','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txNombreEntregaTerceroLogistica', 'Nombre', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txNombreEntregaTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese el nombre.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txDireccionEntregaTerceroLogistica', 'Dirección de entrega', array('class' => 'text-sm text-primary mb-1l')) !!}
                    {!!Form::text('txDireccionEntregaTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese la dirección de entrega.','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txTelefonoEntregaTerceroLogistica', 'Teléfono', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txTelefonoEntregaTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese el teléfono','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('lmRequisitoEntregaTerceroLogistica', 'Requisitos de entrega', array('class' => 'required text-sm text-primary mb-1l')) !!}
                    {!!Form::select('lmRequisitoEntregaTerceroLogistica[]',['Factura'=>'Factura','Remisión'=>'Remisión','Lista Empaque'=>'Lista Empaque','Aviso despacho'=>'Aviso despacho'],null,[ 'class'=>'chosen-select','id'=>'lmRequisitoEntregaTerceroLogistica','multiple','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txOtrosRequisitosTerceroLogistica', 'Otros', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txOtrosRequisitosTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese otros requisitos.','autocomplete'=>'off'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txManejoDevolucionesTerceroLogistica', 'Manejo de devoluciones', array('class' => 'text-sm text-primary mb-1l')) !!}
                    {!!Form::text('txManejoDevolucionesTerceroLogistica',null,[ 'class'=>'form-control','placeholder'=>'Ingrese los manejos de devoluciones.','autocomplete'=>'off'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {!!Form::label('txObservacionTerceroLogistica', 'Observaciones Especiales', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::textarea('txObservacionTerceroLogistica',null,['class'=>'editorTexto form-control','id'=>'txObservacionTerceroLogistica','style'=>'width:100%', 'placeholder'=>'Ingresa Observaciones'])!!}

                </div>
            </div>
        </div>
    </div><br>
    @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
    {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
    @endif
@endsection