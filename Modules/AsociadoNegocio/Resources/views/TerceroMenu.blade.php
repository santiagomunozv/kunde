@extends('layouts.principal')
@section('nombreModulo')
    Asociados de Negocio: {{ $clasificacion[0]['txNombreClasificacionTercero'] }}
@endsection
@section('estilos')
    {!! Html::style('modules/asociadonegocio/css/Tercero.css') !!}
    <style>
        /* Animación con keyframe llamada "latidos" */
        @keyframes latidos {
            from {
                transform: none;
            }

            50% {
                transform: scale(1.4);
            }

            to {
                transform: none;
            }
        }

        /* En la clase corazon vamos a llamar latidos en la animación  */
        .corazon {
            display: inline-block;
            font-size: 25px;
            text-shadow: 0 0 10px #222, 1px 1px 0 #450505;
            color: red;
            animation: latidos .5s infinite;
            transform-origin: center;
        }
    </style>
@endsection

@section('scripts')
    <script></script>
    {!! Html::script('modules/asociadonegocio/js/Tercero.js') !!}

    <script type="text/javascript">
        var valorTipoServicio = {!! $tipoServicios !!};
        // var valorTipoServicio = '<?php echo isset($tercero->lmTipoServicioTercero) ? $tercero->lmTipoServicioTercero : null; ?>'
        var valorClasificacion = '<?php echo isset($tercero->lmClasificacionTercero) ? $tercero->lmClasificacionTercero : null; ?>'
        let idTipoIdentificacion = '<?php echo $tercero->TipoIdentificacion_oidTercero; ?>'

        function modalMenu(url_) {
            var clasif = "{{ $clasificacion[0]['txNombreClasificacionTercero'] }}";
            modal.cargando()
            $.get(url_, function(resp) {
                modal.mostrarModal('Asociados de Negocio: ' + clasif, resp, function() {
                    //esta es la accion del boton aceptar
                    modal.cargando();
                }).extraGrande().sinPie();
                llenarValoresChosen(valorTipoServicio, valorClasificacion)
                iniciarChosenSelect()
                // llenarSelectCompania('<?php echo $tercero->txCompaniasTercero; ?>');
                consultarIdentificacion(idTipoIdentificacion, 1);
                generarDropzoneTercero();

            }).fail(function(resp) {
                modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
            });
        }
    </script>
@endsection
@section('contenido')
    <input type="hidden" name="ClasificacionActual" id="ClasificacionActual" value="{{ $idClasificacion }}">
    <div class="row">
        <div class="col-md-6 my-2">
            <div class="card border-left-info shadow w-100">
                <div class="card-body text-center">
                    <img class="img-fluid" style="max-height:500px"
                        src="{{ $tercero->imImagenTercero ? '/asociadonegocio/consultaimagen/' . $tercero->oidTercero : '/asociadonegocio/camera-512.png' }}">
                </div>
            </div>
        </div>

        <div class="col-md-6 my-2">
            <div class="card border-left-info shadow w-100">
                <div class="card-body">
                    <h4 class="text-sm text-primary mb-1"><b>Nombre: </b>{{ $tercero->txNombreTercero }}</h4><br>
                    <h4 class="text-sm text-primary mb-1"><b>Documento: </b>{{ $tercero->txDocumentoTercero }}</h4><br>
                    <h4 class="text-sm text-primary mb-1"><b>Correo Electrónico:
                        </b>{{ $tercero->txCorreoElectronicoTercero }}</h4><br>
                    <h4 class="text-sm text-primary mb-1"><b>Teléfono: </b>{{ $tercero->txTelefonoTercero }}</h4><br>
                    <h4 class="text-sm text-primary mb-1"><b>Dirección: </b>{{ $tercero->txDireccionTercero }}</h4><br>
                    <h4 class="text-sm text-primary mb-1"><b>Estado: </b>{{ $tercero->txEstadoTercero }}</h4><br>
                    @if ($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
                        <button id="terceroBoton" class="btn btn-warning"
                            onclick="modalMenu('/asociadonegocio/tercero/{{ $id }}/edit')"
                            style="width: 200px; height:40px;">
                            <span class="far fa-edit" style="margin-right: 5px;"></span>
                            Modificar Tercero
                        </button>
                    @endif
                </div>
                <div class="col-md-12">
                    @foreach ($consulta as $config)
                        @if ($config->txRutaAccionModuloTercero)
                            @if ($config->txNombreModuloTercero != 'Verificacion OEA')
                                <a target="_blank"
                                    href="{{ url('asociadonegocio/' . $config->txRutaAccionModuloTercero, [$config->oidTercero, $config->oidClasificacionTercero, 'edit']) }}"
                                    class="btn btn-light btn-lg" data-toggle="tooltip" data-placement="top"
                                    title="{{ $config->txNombreModuloTercero }}">
                                    <i id="iconoBoton" class="{{ $config->txRutaImagenModuloTercero }}"></i>
                                </a>
                            @else
                                @if ($terceroVerificacion[0]->lmTipoServicioTercero != null)
                                    @php
                                        $tipo = explode(',', $terceroVerificacion[0]->lmTipoServicioTercero);
                                    @endphp
                                    @foreach ($tipo as $item)
                                        @if ($item != 'Logistica')
                                            <a target="_blank"
                                                href="{{ url('asociadonegocio/' . $config->txRutaAccionModuloTercero, [$config->oidTercero, $config->oidClasificacionTercero, $item, 'edit']) }}"
                                                class="btn btn-light btn-lg" data-toggle="tooltip" data-placement="top"
                                                title="{{ $config->txNombreModuloTercero }} - {{ $item }}">
                                                <i id="iconoBoton"
                                                    class="{{ $config->txRutaImagenModuloTercero }}
                                                @if (
                                                    $terceroVerificacion[0]->lmTipoServicioTercero != null &&
                                                        $terceroVerificacion[0]->Tercero_oidTercero_1aM == null &&
                                                        $terceroVerificacion[0]->lmClasificacionTercero == 'Seguimiento') @if ($config->txRutaImagenModuloTercero == 'fas fa-indent')
                                                        text-danger corazon @endif
                                                @endif">
                                                </i>
                                            </a>
                                        @endif
                                    @endforeach
                                @endif

                            @endif

                        @endif
                    @endforeach
                    @if ($idClasificacion == '3')
                        <a onclick="descargarResponsabilidad(@php echo $id @endphp)" class="btn btn-light btn-lg"
                            data-toggle="tooltip" data-placement="top" title="Descargar responsabilidad">
                            <i id="iconoBoton" class="fa fa-download">
                            </i>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
