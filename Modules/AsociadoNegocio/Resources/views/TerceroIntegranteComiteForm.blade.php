@extends('layouts.principal')
@section('nombreModulo')
    Integrantes comité
@endsection
@Section('scripts')
    {!! Html::script('js/general.js') !!}
    {{ Html::script('modules/asociadonegocio/js/TerceroIntegranteComite.js') }}

    <script type="text/javascript">
        // Multi de tercero Copast
        var valorAsn_TerceroCopasst = {!! $asn_tercerocopasst !!};
        var nuevoTerceroCopasst = ['', 'Copasst', '', ''];

        // Multi Tercero Cocola
        var valorAsn_TerceroCocola = {!! $asn_tercerococola !!};
        var nuevoTerceroCocola = ['', 'Cocola', '', ''];

        //Multi Tercero Convivencia
        var valorAsn_TerceroConvivencia = {!! $asn_terceroconvivencia !!};
        var nuevoTerceroConviviencia = ['', 'Convivencia', '', ''];
    </script>
@endsection

@section('contenido')
    {!! Form::model($asn_tercerocopasst, [
        'url' => ['/asociadonegocio/integrantescomites', $id],
        'method' => 'PUT',
        'id' => 'form-asn_integrantescomite',
        'onsubmit' => 'return false;',
    ]) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#integrantesCopasst" role="tab"
                        aria-controls="pills-home" aria-selected="true">Copasst</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-comercial-tab" data-toggle="pill" href="#integrantesCocola" role="tab"
                        aria-controls="pills-profile" aria-selected="false">Brigada</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-convivencia-tab" data-toggle="pill" href="#integrantesConvivencia"
                        role="tab" aria-controls="pills-contact" aria-selected="false">Convivencia</a>
                </li>
            </ul>

            <!-- Copasst -->

            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="integrantesCopasst" role="tabpanel"
                    aria-labelledby="pills-home-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">
                    <input type="hidden" id="eliminarAsn_TerceroCopasst" name="eliminarAsn_TerceroCopasst" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>
                                        <button type="button" class="btn btn-primary btn-sm text-light"
                                            onclick="tercerocopasst.agregarCampos(nuevoTerceroCopasst,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                    <th class="required">Nombre</th>
                                    <th>Identificación</th>
                                </thead>
                                <tbody id="contenedorasn_copasst" style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- Cocola -->

                <div class="tab-pane fade" id="integrantesCocola" role="tabpanel" aria-labelledby="pills-comercial-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">
                    <input type="hidden" id="eliminarAsn_TerceroCocola" name="eliminarAsn_TerceroCocola" value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>
                                        <button type="button" class="btn btn-primary btn-sm text-light"
                                            onclick="tercerococola.agregarCampos(nuevoTerceroCocola,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                    <th class="required">Nombre</th>
                                    <th>Identificación</th>
                                </thead>
                                <tbody id="contenedorasn_cocola" style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <!-- Convivencia -->

                <div class="tab-pane fade" id="integrantesConvivencia" role="tabpanel"
                    aria-labelledby="pills-convivencia-tab">
                    <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">
                    <input type="hidden" id="eliminarAsn_TerceroConvivencia" name="eliminarAsn_TerceroConvivencia"
                        value="">
                    <div class="card-body multi-max">
                        <div class="table-responsive">
                            <table id="tabla-referencia" class="table multiregistro table-sm table-hover table-borderless">
                                <thead class="bg-primary text-light">
                                    <th>
                                        <button type="button" class="btn btn-primary btn-sm text-light"
                                            onclick="terceroconvivencia.agregarCampos(nuevoTerceroConviviencia,'A');">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </th>
                                    <th class="required">Nombre</th>
                                    <th>Identificación</th>
                                </thead>
                                <tbody id="contenedorasn_convivencia" style="width:3000px;"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if ($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @endif
        @endsection
