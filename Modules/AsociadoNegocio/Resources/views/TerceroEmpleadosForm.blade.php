@extends('layouts.principal')
@section('nombreModulo')
    {{ $terceroNombre[0] }} Empleados
@endsection
@Section('scripts')
    <script type="text/javascript">
        let idTercero = {{ $id }};
        let empleados = {!! $empleados !!}
        var idTipoDocumento = '<?php echo isset($tipoDocumentoId) ? json_encode($tipoDocumentoId) : ''; ?> ';
        var nombreTipoDocumento = '<?php echo isset($tipoDcoumentoNombre) ? json_encode($tipoDcoumentoNombre) : ''; ?>'
    </script>
    <style>
        .size {
            width: 15px;
        }

    </style>
    {{ Html::script('modules/asociadonegocio/js/TerceroEmpleado.js') }}
@endsection
@section('contenido')

    {!! Form::model($empleados, ['url' => ['asociadonegocio/terceroempleado', $id], 'method' => 'PUT', 'id' => 'form-terceroempleado', 'onsubmit' => 'return false']) !!}

    <!-- Miltiregistro Contacto -->
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <a class="primary" href="{{ url('asociadonegocio/estadisticastercero/' . $id) }}" title="Estadisticas">
                <i class="fas fa-chart-bar fa-2x"></i>
            </a>
            <input type="hidden" id="Tercero_oidTercero_1aM" name="Tercero_oidTercero_1aM" value="1">
            <input type="hidden" id="eliminarEmpleado" name="eliminarEmpleado" value="">
            <div class="card-body multi-max">
                <table id="tabla-empleados" class="table multiregistro table-sm table-hover table-borderless">
                    <thead>
                        <tr>
                            <th class="bg-primary text-light size">
                                <button type="button" class="btn btn-primary btn-sm text-light"
                                    onclick="terceroempleado.agregarCampos([],'A');">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Subir archivo excel"
                                    onclick="cargarEmpleados({{ $id }})">
                                    <i class="fas fa-upload"></i>
                                </button>
                                <button id="btnDescargar" class="btn btn-primary btn-sm text-light" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="Descargar archivo excel"
                                    onclick="descargarEmpleados({{ $id }})">
                                    <i class="fas fa-download"></i>
                                </button>
                            </th>
                            <th class="bg-primary text-light size"></th>
                            <th class="required bg-primary text-light">Tipo Dcocumento</th>
                            <th class="required bg-primary text-light">Cedula</th>
                            <th class="required bg-primary text-light">Nombre</th>
                            <th class="required bg-primary text-light">Apellido</th>
                        </tr>
                    </thead>
                    <tbody id="contenedorterceroempleado"></tbody>
                </table>
            </div>
            {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
        </div>
    </div>
@endsection
