@extends('layouts.principal')
@section('nombreModulo')
Cambio razón social
@endsection 
@section('scripts')
<script>
    let terceroRazonSocial = {!! $razonSocial !!}
</script>
{{Html::script('modules/asociadonegocio/js/TerceroRazonSocial.js')}}
@endsection 
@section('contenido')
    {!!Form::open(['url'=>['/asociadonegocio/terceroRazonSocial',$id],'method'=>'PUT', 'id' => 'tercerorazonsocial', 'onsubmit' => 'return false'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txNombreTerceroRazonSocial-encabezado', 'Razon Social Actual', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txNombreTerceroRazonSocial-encabezado',(isset($NombreTercero) ? $NombreTercero[0]->txNombreTercero : null),[ 'class'=>'form-control','readonly'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txPrimerNombreTercero-encabezado', 'Primer Nombre', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txPrimerNombreTercero-encabezado',null,[ 'class'=>'form-control','onblur'=>'concatenarNombre()'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txSegundoNombreTercero-encabezado', 'Segundo Nombre', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txSegundoNombreTercero-encabezado',null,[ 'class'=>'form-control','onblur'=>'concatenarNombre()'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txPrimerApellidoTercero-encabezado', 'Primer Apellido', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txPrimerApellidoTercero-encabezado',null,[ 'class'=>'form-control','onblur'=>'concatenarNombre()'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txSegundoApellidoTercero-encabezado', 'Segundo Apellido', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('txSegundoApellidoTercero-encabezado',null,[ 'class'=>'form-control','onblur'=>'concatenarNombre()'])!!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txNombreTercero', 'Nueva Razon Social', array('class' => 'required text-sm text-primary mb-1')) !!}
                    {!!Form::text('txNombreTercero',null,[ 'class'=>'form-control'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('daFechaFinTerceroRazonSocial-encabezado', 'Fecha de Cambio', array('class' => 'text-sm text-primary mb-1')) !!}
                    {!!Form::text('daFechaFinTerceroRazonSocial-encabezado',$fechaActual,[ 'class'=>'form-control','readonly'])!!}
                </div>
            </div>
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th></th>
                        <th>Razon social</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <tbody id="contenedorRazonSocial"></tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
        @if($permisoModificar['chModificarClasificacionTerceroRol'] == 1)
        {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        @endif
@endsection
