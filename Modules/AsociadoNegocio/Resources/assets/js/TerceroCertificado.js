
let errorClass = 'is-invalid';
function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#terceroCertificado';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyNumero = 'txNumeroTerceroTipoCertificado';
            let keyFecha = 'daFechaVencimientoTerceroTipoCertificado';
            let numeros = $('input[name="'+keyNumero+'[]"]');
            let fecha = $('input[name="'+keyFecha+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyFecha)){
                    setErroresMultiregistro(index.substr(keyFecha.length , index.length) , fecha);
                }else if(index.startsWith(keyNumero)){
                    setErroresMultiregistro(index.substr(keyNumero.length , index.length) , numeros);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }
}


function validarForm(){
    let impacto = $('select[name="lsVerificacionTerceroCertificado[]"]');
    let mensajes =[];
    $('.'+errorClass).removeClass(errorClass);
    requerido(impacto,mensajes,'Las listas de verificacion son obligatorias.');
    for(let i = 0 ; i < impacto.length; i++){
        requerido(impacto[i],mensajes,'La lista de verificación es obligatoria en el registro :  '+[i]);
    }
    return mensajes;
}
function requerido( input, messages , message){
    if(!$(input).val()){
        $(input).addClass( errorClass );
        messages.push(message);
    }
}

function cambiarClase(registro,valor){
    contador = registro.replace('lsVerificacionTerceroCertificado', '');
    cantidadReg = $('#contador'+contador).val();
    dropzone = $("#archivos"+contador).html();
    if(valor == 'Si' && dropzone == ""){
        valor ="";
        $('#lsVerificacionTerceroCertificado'+contador).val('N/A')
        modal.mostrarModal('Información','<div class="alert alert-info">No puede seleccionar la opción "Si", primero debe adjuntar un archivo para que ésta se pueda seleccionar.</div>');
    }
    //hacemos este if para que el sistema no deje cambiar la clase ni el  valor de la lista select si tiene adjuntos guardados ya en base de datos
        if(cantidadReg >= 0){
            valor = 'Si';
        }
        if(valor == 'Si'){
            reg = registro.replace('lsVerificacionTerceroCertificado', '');
            $("#txNombreTipoCertificado"+reg).removeClass();
            $("#txNombreTipoCertificado"+reg).addClass('alert alert-success')
            $('#lsVerificacionTerceroCertificado'+reg).val('Si')
        }else if(valor == 'No'){
            reg = registro.replace('lsVerificacionTerceroCertificado', '');
            $("#txNombreTipoCertificado"+reg).removeClass();
            $("#txNombreTipoCertificado"+reg).removeClass().addClass('alert alert-danger')
        }else{
            reg = registro.replace('lsVerificacionTerceroCertificado', '');
            $("#txNombreTipoCertificado"+reg).removeClass();
            $("#txNombreTipoCertificado"+reg).removeClass().addClass('alert alert-dark')
        }
}

function generarDropzone(currentKey,idCertificado){

    //Cerramos colapse cuando inicien otro nuevo
    $(".contenedores").removeClass("show");

    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    myDropzone = new Dropzone("div#dropzoneImage"+currentKey, {
        headers: { 'X-CSRF-TOKEN': token},
        url: "/asociadonegocio/subirCertificado",
        addRemoveLinks: true,
        init: function(){
            //Funcion para crear el input hidden con la rut del adjunto
            this.on("success",function(file){
                $('#lsVerificacionTerceroCertificado'+currentKey).val("Si");
                var pathImage = JSON.parse(file.xhr.response);
                $("#archivos"+currentKey).append("<input type='hidden' name='txRutaTerceroAdjunto-"+idCertificado+"[]' value='"+pathImage.tmpPath+"'>");
                cambiarClase('lsVerificacionTerceroCertificado'+currentKey,"Si");
                    var NombreArchivo = file.name;
                    modal.mostrarModal('Fecha De Archivo','<label>Fecha de Vencimiento del archivo: '+NombreArchivo+'</label><input type="text" class="form-control" id="daFechaVencimientoTerceroAdjunto" readonly><label>Descripción</label></input><input type="text" class="form-control" id="txDescripcionTerceroAdjunto" value="'+NombreArchivo+'"></input>',function(){
                    FechaVencimientoArchivo = document.getElementById("daFechaVencimientoTerceroAdjunto").value;
                    descripcionAdjunto = document.getElementById("txDescripcionTerceroAdjunto").value;
                    //concatenamos la fecha con el value del adjunto separado por (#) para saber en el controlador de que adjunto pertenece la fecha que mandaremos.
                    $("#archivos"+currentKey).append("<input type='hidden' name='daFechaVencimientoTerceroAdjunto"+idCertificado+"[]' value='"+FechaVencimientoArchivo+"#"+pathImage.tmpPath+"'>");
                    $("#archivos"+currentKey).append("<input type='hidden' name='txDescripcionTerceroAdjunto"+idCertificado+"[]' value='"+descripcionAdjunto+"#"+pathImage.tmpPath+"'>");
                    modal.cerrarModal();
                });
                    let datePickerConfig = {
                    format: 'yyyy-mm-dd',
                    locale: 'es-es',
                    uiLibrary: 'bootstrap4',
                    };
                    $("#daFechaVencimientoTerceroAdjunto").datepicker(datePickerConfig)
            });
            this.on("addedfile", function(file) {
                file.previewElement.addEventListener("click", function() {
                    var pathImage = JSON.parse(file.xhr.response);
                    var NombreArchivo = file.name;
                    modal.mostrarModal('Fecha De Archivo','<label>Fecha de Vencimiento del archivo: '+NombreArchivo+'</label><input type="date" class="form-control" id="daFechaVencimientoTerceroAdjunto"><label>Descripción</label></input><input type="text" class="form-control" id="txDescripcionTerceroAdjunto" value="'+NombreArchivo+'"></input>',function(){
                    FechaVencimientoArchivo = document.getElementById("daFechaVencimientoTerceroAdjunto").value;
                    descripcionAdjunto = document.getElementById("txDescripcionTerceroAdjunto").value;
                    //concatenamos la fecha con el value del adjunto separado por (#) para saber en el controlador de que adjunto pertenece la fecha que mandaremos.
                    $("#archivos"+currentKey).append("<input type='hidden' name='daFechaVencimientoTerceroAdjunto"+idCertificado+"[]' value='"+FechaVencimientoArchivo+"#"+pathImage.tmpPath+"'>");
                    $("#archivos"+currentKey).append("<input type='hidden' name='txDescripcionTerceroAdjunto"+idCertificado+"[]' value='"+descripcionAdjunto+"#"+pathImage.tmpPath+"'>");
                    modal.cerrarModal();
                    });
                });
            });

            //Funsion para remover los adjuntos. El hidden que se creo al adjuntarlo
            this.on("removedfile",function(file){
                let pathImage = JSON.parse(file.xhr.response);
                $("input[name='txRutaTerceroAdjunto-"+idCertificado+"[]'][value='"+pathImage.tmpPath+"']").remove();
            });
        }
    });
}

//Funsion para eliminar los adjuntos que ya estan guardados en base de datos
function eliminarArchivo(idAdjunto){
    $("#eliminarArchivos").val(idAdjunto);
    $("#eliminarArchivos").append("<input type='hidden' name='eliminarArchivos[]' value='"+idAdjunto+"'>");
    $("#div"+idAdjunto).remove();
}