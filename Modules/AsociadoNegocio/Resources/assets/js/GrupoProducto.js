"use strict";

// ----------DEFINIMOS VARIABLES---------------
var claseSeleccion = 'table-info';
var errorClass = 'is-invalid';
var GrupoProducto = [];
var arrayClasificacion =[];
let contador =0;

$(function(){
    clasificacionGrupos.forEach((clasificacion,key) => {
        let id = clasificacion.oidClasificacionProductoGrupo;
        let nombre = clasificacion.txNombreClasificacionProductoGrupo;
        if (!arrayClasificacion.includes(id)){
            if(key == 0){
                $("#navClasificaciones").append('<a class="nav-item nav-link active" id="nav'+id+'" data-toggle="tab" href="#clasificacion'+id+'" role="tab">'+nombre+
                '<span class="text-danger" onclick="eliminarClasificaciones('+id+');"><i class="ml-2 fas fa-times"></i></span></a>');
                crearTabsClasificaciones(id,'active');
            }else{
                $("#navClasificaciones").append('<a class="nav-item nav-link" id="nav'+id+'" data-toggle="tab" href="#clasificacion'+id+'" role="tab">'+nombre+
                '<span class="text-danger" onclick="eliminarClasificaciones('+id+');"><i class="ml-2 fas fa-times"></i></span></a>');
                crearTabsClasificaciones(id,'');
            }
            arrayClasificacion.push(String(id));
        }
    });
});

// ----------- FUNCION PARA AGREGAR GRUPOS A MULTI----------------
function adicionarGrupos(trGrupos){
    if(trGrupos && trGrupos.length){
        trGrupos.forEach((tr,key) => {
            let id = tr.dataset.idGrupo;
            let nombre = tr.dataset.nombreGrupo;
            if (!arrayClasificacion.includes(id)){
                if(key == 0 && arrayClasificacion.length == 0){
                    $("#navClasificaciones").append('<a class="nav-item nav-link active" id="nav'+id+'" data-toggle="tab" href="#clasificacion'+id+'" role="tab">'+nombre+
                    '<span class="text-danger" onclick="eliminarClasificaciones('+id+');"><i class="ml-2 fas fa-times"></i></span></a>');
                    crearTabsClasificaciones(id,'active');
                }else{
                    $("#navClasificaciones").append('<a class="nav-item nav-link" id="nav'+id+'" data-toggle="tab" href="#clasificacion'+id+'" role="tab">'+nombre+
                    '<span class="text-danger" onclick="eliminarClasificaciones('+id+');"><i class="ml-2 fas fa-times"></i></span></a>');
                    crearTabsClasificaciones(id,'');
                }
                arrayClasificacion.push(String(id));
            }
        });
    }
}

function crearTabsClasificaciones(id,activo){
    let div= '<div class="tab-pane fade show '+activo+'" id="clasificacion'+id+'" role="tabpanel">'+
        '   <div class="table-responsive">'+
        '       <table class="table multiregistro table-sm table-hover table-borderless">'+
        '           <thead class="bg-primary text-light">'+
        '               <tr>'+
        '                   <th>'+
        '                      <button type="button" class="btn btn-primary btn-sm text-light" onclick="GrupoProducto['+id+'].agregarCampos([0,'+id+',\'\',\'\'],\'A\');"><i class="fa fa-plus"></i></button>'+
        '                   </th>'+
        '                   <th class="required">Codigo</th>'+
        '                   <th class="required">Nombre</th>'+
        '               </tr>'+
        '           </thead>'+
        '           <tbody id="tClasificaciones'+id+'"></tbody>'+
        '       </table>'+
        '   </div>'+
        '</div>';
    $("#tabClasificaciones").append(div);
    configurarMulti(id);
}

function configurarMulti(id){

    // -------------GENERAMOS MULTI DE MATERIA PRIMA-----------------
    GrupoProducto[id] = new GeneradorMultiRegistro('GrupoProducto['+id+']','tClasificaciones'+id,'GrupoProducto'+id);
    GrupoProducto[id].campoid = 'oidGrupoProducto';
    GrupoProducto[id].campoEliminacion = 'eliminarGrupoProducto';
    GrupoProducto[id].etiqueta = ['input','input','input','input'];
    GrupoProducto[id].tipo = ['hidden','hidden','text','text'];
    GrupoProducto[id].campos = ['oidGrupoProducto','ClasificacionProductoGrupo_oidClasificacionProductoGrupo','txCodigoGrupoProducto','txNombreGrupoProducto']
    GrupoProducto[id].sololectura = [true,true,false,false];
    GrupoProducto[id].opciones = ['','','',''];
    GrupoProducto[id].clase = ['','','',''];

    $.get('/asociadonegocio/listargrupos',{'idTercero':idTercero,'idClasificacion':id},function( data ) {
        let datos = JSON.parse(data);
        let registro = new Array();
        let terceroDatos = new Array();

        for(let j in datos){
            for (var i in datos[j]){
                registro.push(datos[j][i]);
            }
            terceroDatos.push(registro);
            registro = [];
        }
        GrupoProducto[id].contador = contador;
        terceroDatos.forEach(dato => {
            GrupoProducto[id].agregarCampos(dato , 'A');
            contador++;
        });
    }).fail(function(resp){

    });
}

function modalClasificaciones(id){
    modal.cargando();
    $.get('/asociadonegocio/listarclasificacionesproducto',function( data ) {
        modal.mostrarModal('Lista de Clasificaciones de producto grupo',data,function(){
            let trs = document.querySelectorAll('.'+claseSeleccion);
            adicionarGrupos(trs);
            modal.cerrarModal();
        }).extraGrande();
        configurarGridSelect('grupo-table');
    }).fail(function(resp){
        modal.cerrarModal();
    });
}

//Funcion para eliminar centros de produccion -tabs
function eliminarClasificaciones(id){
    modal.cargando();
    modal.mostrarModal('Eliminar','Esta Seguro de eliminar esta clasificación de productos?',function(){
        $("#clasificacion"+id).remove();
        $("#nav"+id).remove();
        $("#eliminarClasificaciones").val($("#eliminarClasificaciones").val() + id+',');

        let index = arrayClasificacion.indexOf(String(id));
        if (index > -1) {
            arrayClasificacion.splice(index, 1);
        }
        
        modal.cerrarModal();
    });
}

//  FUNCION PARA GRABAR REGISTROS ------
function grabar(){
    modal.cargando();
    $('.'+errorClass).removeClass(errorClass);
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#GrupoProducto';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                // location.href = '/asociadonegocio/terceromenu/'+resp.oidTercero;
                window.close();
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyCodigo = 'txCodigoGrupoProducto';
            let keyNombre = 'txNombreGrupoProducto';
            
            let codigos = $('input[name="'+keyCodigo+'[]"]')
            let nombres = $('input[name="'+keyNombre+'[]"]');
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace(".","");
                mensajes.push( value );

                if( index.startsWith(keyCodigo)){
                    setErroresMultiregistro(index.substr(keyCodigo.length , index.length) , codigos);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes, route ,resp);
        });
    }
}

// FUNCION PARA ENVIAR ERROES DE MULTIREGISTRO
function setErroresMultiregistro( index , array){
    console.log(index,array);
    if(array && array.length){
        array[index].classList.add(errorClass);
    }
}

// Funcion para validar formulario en parte del cliente
function validarForm(){

    let mensajes =[];
    let codigo = document.querySelectorAll('input[name="txCodigoGrupoProducto[]"]');
    let nombre = document.querySelectorAll('input[name="txNombreGrupoProducto[]"]');
    
    let validar = [codigo,nombre];
    let mensajesArray = ["el codigo",'el nombre']

    validar.forEach((valida,j) => {
        if(valida){
            valida.forEach((valor , i) => {
                if (valor && !valor.value){
                    valor.classList.add( errorClass );
                    mensajes.push(mensajesArray[j]+' en la linea '+(i + 1)+' es requerida');
                }
            })
        }
    });

    return mensajes;
}
