
let razonSocial = [];
$(function(){
    // -------------GENERAMOS MULTI DE COLORES-----------------
    razonSocial = new GeneradorMultiRegistro('razonSocial','contenedorRazonSocial','razonSocial');
    razonSocial.campoid = 'oidTerceroBanco';
    razonSocial.campoEliminacion = '';
    razonSocial.etiqueta = ['input','input','input','input'];
    razonSocial.tipo = ['hidden','text','text','text'];
    razonSocial.campos = ['oidTerceroRazonSocial','txNombreTerceroRazonSocial','daFechaInicioTerceroRazonSocial','daFechaFinTerceroRazonSocial']
    razonSocial.opciones = ['','','',''];
    razonSocial.clase = ['','','',''];
    razonSocial.sololectura = [true,true,true,true];
    razonSocial.botonEliminacion = false;


    terceroRazonSocial.forEach( dato => {razonSocial.agregarCampos(dato , 'L');
        });

});

let errorClass = 'is-invalid';
function grabar(idPadre){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#tercerorazonsocial';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let errors = resp.responseJSON.errors;
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}

function validarForm(){
    let messages = [];
    let validaciones = [
        {'input' :'txNombreTercero' , 'message' : 'El campo de Nueva Razón Social es obligatorio si deseas guardar.'},
    ];
    validaciones.forEach( validacion => {
        let input = $('#'+validacion.input);
        let value = input.val();
        if(!value){
            input.addClass( errorClass );
            messages.push( validacion.message);
        }
    });
    return messages;
}

function concatenarNombre(){
    $("#txNombreTercero").val($("#txPrimerNombreTercero-encabezado").val()+" "+$("#txSegundoNombreTercero-encabezado").val()+" "+$("#txPrimerApellidoTercero-encabezado").val()+" "+$("#txSegundoApellidoTercero-encabezado").val()+" ")
}