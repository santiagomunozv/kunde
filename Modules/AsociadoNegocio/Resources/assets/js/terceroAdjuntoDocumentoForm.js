"use strict";
var errorClass = "is-invalid";
var configuracionTerceroAdjuntoDocumentos = JSON.parse(terceroadjuntodocumento);
var configuracionTerceroAdjuntoDocumento = [];


$(function(){
    
    configuracionTerceroAdjuntoDocumento = new GeneradorMultiRegistro('configuracionTerceroAdjuntoDocumento','contenedorTerceroAdjuntoDocumento','configuracionTerceroAdjuntoDocumento');

    let options=[JSON.parse(oidTipoDocumento),JSON.parse(txNombreTipoDocumento)]

    configuracionTerceroAdjuntoDocumento.campoid = 'oidTerceroAdjuntoDocumento';
    configuracionTerceroAdjuntoDocumento.campoEliminacion = 'eliminarTerceroAdjuntoDocumento';
    configuracionTerceroAdjuntoDocumento.botonEliminacion = true;
    configuracionTerceroAdjuntoDocumento.funcionEliminacion = '';  
    configuracionTerceroAdjuntoDocumento.campos = ['oidTerceroAdjuntoDocumento','txDescripcionTerceroAdjuntoDocumento','TipoDocumento_oidTipoDocumento','daFechaTerceroAdjuntoDocumento', 'txRutaTerceroAdjuntoDocumento'];
    configuracionTerceroAdjuntoDocumento.etiqueta = ['input', 'input', 'select', 'date', 'file'];
    configuracionTerceroAdjuntoDocumento.tipo = ['hidden', 'text', '', '', ''];
    configuracionTerceroAdjuntoDocumento.estilo = ['', '', '', '', ''];
    configuracionTerceroAdjuntoDocumento.clase = ['', '', 'chosen-select', '', ''];
    configuracionTerceroAdjuntoDocumento.sololectura = [true,false,false,false,false];
    configuracionTerceroAdjuntoDocumento.opciones = ['', '', options, '', ''];
    configuracionTerceroAdjuntoDocumento.funciones = ['', '', '', '', ''];
    configuracionTerceroAdjuntoDocumento.otrosAtributos = ['', '', '', '', ''];


    configuracionTerceroAdjuntoDocumentos.forEach( dato => {configuracionTerceroAdjuntoDocumento.agregarCampos(dato , 'L');
    });
});


function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-terceroadjuntodocumento";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
               location.href = "/asociadonegocio/terceroadjuntodocumento";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}