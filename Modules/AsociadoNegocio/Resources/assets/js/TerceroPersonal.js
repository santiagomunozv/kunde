"use strict";

$(document).ready( function () {
    let datePickerConfig = {
        format: 'yyyy-mm-dd',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
    };

    $("#daFechaExpedicionTerceroPersonal").datepicker(datePickerConfig)
    $("#daFechaNacimientoTerceroPersonal").datepicker(datePickerConfig)

    //iniciando chosen select de ciudades
    $('#Ciudad_oidNacimiento').chosen();
    $('#Ciudad_oidExpedicion').chosen();


});



let errorClass = 'is-invalid';
function grabar(idPadre){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-asn_terceropersonal';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '/asociadonegocio/tercerogrid?ct=1';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let errors = resp.responseJSON.errors;
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
		let messages = [];
		let validaciones = [
			{'input' :'lsSexoTerceroPersonal' , 'message' : 'El campo Sexo es obligatorio.'},
			{'input' :'lsGrupoSanguineoTerceroPersonal' , 'message' : 'El campo Grupo Sanguíneo es obligatorio.'},
			{'input' :'lsRHTerceroPersonal' , 'message' : 'El campo RH es obligatorio.'},
			{'input' :'daFechaExpedicionTerceroPersonal' , 'message' : 'El campo Fecha Expedición es obligatorio.'},
			{'input' :'Ciudad_oidExpedicion' , 'message' : 'El campo Ciudad de Expedición es obligatorio.'},
			{'input' :'daFechaNacimientoTerceroPersonal' , 'message' : 'El campo Fecha Nacimiento es obligatorio.'},
			{'input' :'Ciudad_oidNacimiento' , 'message' : 'El campo Ciudad de Nacimiento es obligatorio.'},
            {'input' :'lsEstadoCivilTerceroPersonal' , 'message' : 'El campo Estado es obligatorio.'},
            {'input' :'inEstratoTerceroPersonal' , 'message' : 'El campo Estrato es obligatorio.'},
            {'input' :'lsEscolaridadTerceroPersonal' , 'message' : 'El campo Escolaridad es obligatorio.'},
            {'input' :'lsViviendaTerceroPersonal' , 'message' : 'El campo Tipo de vivienda es obligatorio.'},
            {'input' :'lsCaracteristicaViviendaTerceroPersonal' , 'message' : 'El campo Característica de vivienda es obligatorio.'},
            {'input' :'lsZonaViviendaTerceroPersonal' , 'message' : 'El campo Zona de vivienda es obligatorio.'},

		];
		validaciones.forEach( validacion => {
			let input = $('#'+validacion.input);
			let value = input.val();
			if(!value){
				input.addClass( errorClass );
				messages.push( validacion.message);
			}
		});
		
		return messages;
	}
	
}







// function validarForm(){
	// 	let sexo = $('#lsSexoTerceroPersonal');
	// 	let grupo = $('#lsGrupoSanguineoTerceroPersonal');
	// 	let rh = $('#lsRHTerceroPersonal');
	// 	let fechaEx = $('#daFechaExpedicionTerceroPersonal');
	// 	let ciudadEx = $('#Ciudad_oidExpedicion');
	// 	let fechaNa = $('#daFechaNacimientoTerceroPersonal');
	// 	let ciudadNa = $('#Ciudad_oidNacimiento');
	// 	let estado = $('#lsEstadoCivilTerceroPersonal');
		
   
	// 	let mensajes =[];
		
    //     let regimenTributario = regimen.val();
    //     regimen.removeClass(errorClass);
    //     clasificacionRenta.removeClass(errorClass);
    //     if(!regimenTributario){
    //         regimen.addClass(errorClass);
    //         mensajes.push('El campo Régimen es obligatorio.');
    //     }
    //     if(!clasificacionTributario){
    //         clasificacionRenta.addClass(errorClass)
    //         mensajes.push('El campo Clasificación es obligatorio.');
    //     }
    //     return mensajes;
    // }




// function grabar(){
//     modal.cargando();
//     $('.'+errorClass).removeClass(errorClass);
//     let mensajes = validarDepartamento();
//     if( mensajes && mensajes.length ){
//         modal.mostrarErrores( mensajes );
//     }else{
//         let formularioId = '#form-departamento';
//         let url = $(formularioId).attr('action');
//         let data = $(formularioId).serialize();
//         $.post(url,data, function( resp ){
//             modal.establecerAccionCerrar(function(){
//                 location.href = '/general/departamento';
//             });
//             modal.mostrarModal('Información' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
//         },'json').fail( function(resp){
//             let keyCodigo = 'txCodigoCiudad';
//             let keyNombre = 'txNombreCiudad';
//             let nombres = $('input[name="'+keyNombre+'[]"]');
//             let codigos = $('input[name="'+keyCodigo+'[]"]')
//             $.each(resp.responseJSON.errors, function(index, value) {
//                 mensajes.push( value );
//                 if( index.startsWith(keyCodigo)){
//                     setErroresMultiregistro(index.substr(keyCodigo.length , index.length) , codigos);
//                 }else if(index.startsWith(keyNombre)){
//                     setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
//                 }else{
//                     $('#'+index).addClass(errorClass);
//                 }
//             });
//             modal.mostrarErrores(mensajes, url ,resp);
//         });
//     }

//     function setErroresMultiregistro( index , array){
//         if(array && array.length){
//             array[index].classList.add(errorClass);
//         }
//     }
// }