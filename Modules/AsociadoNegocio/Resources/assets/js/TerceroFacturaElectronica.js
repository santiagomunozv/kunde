"use strict";

$(function () {
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

});

let errorClass = 'is-invalid';
function grabar(idPadre){
    modal.cargando();
    let mensajes = []; //validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-terceroFacturaElectronica';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '/asociadonegocio/tercerogrid?ct=1';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            // let errors = resp.responseJSON.errors;
            // $.each(resp.responseJSON.errors, function(index, value) {
            //     mensajes.push( value );
            //     $('#'+index).addClass(errorClass);
            // });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
		let messages = [];
		let validaciones = [
			// {'input' :'lsSexoTerceroPersonal' , 'message' : 'El campo Sexo es obligatorio.'},
			// {'input' :'lsGrupoSanguineoTerceroPersonal' , 'message' : 'El campo Grupo Sanguíneo es obligatorio.'},
			// {'input' :'lsRHTerceroPersonal' , 'message' : 'El campo RH es obligatorio.'},
			// {'input' :'daFechaExpedicionTerceroPersonal' , 'message' : 'El campo Fecha Expedición es obligatorio.'},
			// {'input' :'Ciudad_oidExpedicion' , 'message' : 'El campo Ciudad de Expedición es obligatorio.'},
			// {'input' :'daFechaNacimientoTerceroPersonal' , 'message' : 'El campo Fecha Nacimiento es obligatorio.'},
			// {'input' :'Ciudad_oidNacimiento' , 'message' : 'El campo Ciudad de Nacimiento es obligatorio.'},
			// {'input' :'lsEstadoCivilTerceroPersonal' , 'message' : 'El campo Estado es obligatorio.'},
		];
		validaciones.forEach( validacion => {
			let input = $('#'+validacion.input);
			let value = input.val();
			if(!value){
				input.addClass( errorClass );
				messages.push( validacion.message);
			}
		});
		
		return messages;
	}
	
}