"use strict";
var errorClass = "is-invalid";

var configuracionEventos = JSON.parse(TerceroEventos);
var configuracionEvento = [];

let evento = [
  JSON.parse(eventosId),
  JSON.parse(eventosNombre),
];

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" }
  }

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  configuracionEvento = new GeneradorMultiRegistro('configuracionEvento', 'contenedorEvento', 'configuracionEvento');

  configuracionEvento.campoid = 'oidTerceroEventoPeriodicidad';
  configuracionEvento.campoEliminacion = 'eliminarEvento';
  configuracionEvento.botonEliminacion = true;
  configuracionEvento.funcionEliminacion = '';
  configuracionEvento.campos = [
    'oidTerceroEventoPeriodicidad',
    'inOrdernTerceroEventoPeriodicidad',
    'Evento_oidEvento',
    'lsModalidadTerceroPeriodicidad',
    'dtFechaEstimadaTerceroEventoPeriodicidad',
    'dtHoraEstimadaTerceroEventoPeriodicidad'
  ];
  configuracionEvento.etiqueta = ['input', 'input', 'select', 'select', 'input', 'input'];
  configuracionEvento.tipo = ['hidden', 'number', 'text', 'text', 'date', 'time'];
  configuracionEvento.estilo = ['', '', '', '', '', ''];
  configuracionEvento.clase = ['', '', 'chosen-select', '', '', ''];
  configuracionEvento.sololectura = [true, false, false, false, false, false];
  configuracionEvento.opciones = ['', '', evento, [['Virtual', 'Presencial', 'Virtual/Grupal'], ['Virtual', 'Presencial', 'Virtual/Grupal']], '', ''];
  configuracionEvento.funciones = ['', '', '', '', '', ''];
  configuracionEvento.otrosAtributos = ['', '', '', '', '', ''];

  configuracionEventos.forEach(dato => {
    configuracionEvento.agregarCampos(dato, 'L');
  });
});

function grabar(grabar) {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-confevento";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    data = data + '&grabar=' + grabar;

    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          if (grabar) {
            window.close();
            location.href = '';
          }
          location.reload();
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {

      $.each(resp.responseJSON.errors, function (index, value) {
        index = index.replace(".", "");
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);

    });
  }
}

function setErroresMultiregistro(index, array) {
  if (array && array.length) {
    array[index].classList.add(errorClass);
  }
}

function validarForm() {
  let mensajes = [];

  let lsPeriodicidadTerceroPeriodicidad_Input = $("#lsPeriodicidadTerceroPeriodicidad");
  let lsPeriodicidadTerceroPeriodicidad_AE = lsPeriodicidadTerceroPeriodicidad_Input.val();
  lsPeriodicidadTerceroPeriodicidad_Input.removeClass(errorClass);
  if (!lsPeriodicidadTerceroPeriodicidad_AE) {
    lsPeriodicidadTerceroPeriodicidad_Input.addClass(errorClass);
    mensajes.push("El campo Periodicidad es obligatorio");
  }

  let hrHoraInicioPeriodicidadTerceroPeriodicidad_Input = $("#hrHoraInicioPeriodicidadTerceroPeriodicidad");
  let hrHoraInicioPeriodicidadTerceroPeriodicidad_AE = hrHoraInicioPeriodicidadTerceroPeriodicidad_Input.val();
  hrHoraInicioPeriodicidadTerceroPeriodicidad_Input.removeClass(errorClass);
  if (!hrHoraInicioPeriodicidadTerceroPeriodicidad_AE) {
    hrHoraInicioPeriodicidadTerceroPeriodicidad_Input.addClass(errorClass);
    mensajes.push("El campo Hora estimada inicio es obligatoria");
  }

  let inHorasTerceroPeriodicidad_Input = $("#inHorasTerceroPeriodicidad");
  let inHorasTerceroPeriodicidad_AE = inHorasTerceroPeriodicidad_Input.val();
  inHorasTerceroPeriodicidad_Input.removeClass(errorClass);
  if (!inHorasTerceroPeriodicidad_AE) {
    inHorasTerceroPeriodicidad_Input.addClass(errorClass);
    mensajes.push("El campo hora es obligatorio");
  }

  let dtFechaInicioTerceroPeriodicidad_Input = $("#dtFechaInicioTerceroPeriodicidad");
  let dtFechaInicioTerceroPeriodicidad_AE = dtFechaInicioTerceroPeriodicidad_Input.val();
  dtFechaInicioTerceroPeriodicidad_Input.removeClass(errorClass);
  if (!dtFechaInicioTerceroPeriodicidad_AE) {
    dtFechaInicioTerceroPeriodicidad_Input.addClass(errorClass);
    mensajes.push("El campo fecha de inicio es obligatorio");
  }

  return mensajes;
}

function generarReporte() {
  let oidTercero = document.getElementById('Tercero_oidTercero').value;
  let baseUrl = window.location.origin;
  window.open(baseUrl + '/asociadonegocio/generarplantrabajotercero/' + oidTercero, '_blank', 'width=2500px, height=700px, scrollbars=yes');
}

function enviarNotificacion() {
  let oidTercero = document.getElementById('Tercero_oidTercero').value;
  modal.cargando();
  $.get(
    '/asociadonegocio/enviarnotificacionplantrabajotercero/' + oidTercero,
    {},
    function () {
      modal.establecerAccionCerrar(function () {
        location.reload();
      });
      modal.mostrarModal(
        "Informacion",
        '<div class="alert alert-success">Notificacion enviada correctamente</div>'
      );
    },
    "json"
  ).fail(function (resp) {
    modal.mostrarErrores([], '', resp);

  });
}
