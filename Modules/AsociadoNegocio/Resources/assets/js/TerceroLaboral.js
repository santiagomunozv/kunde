"use strict";

let errorClass = 'is-invalid';
let cargo = [];
let centroCosto = [];
let centroTrabajo = [];
let centroCostoLista = [JSON.parse(idCentroCostos), JSON.parse(nombreCentroCostos)];
let centroTrabajoLista = [JSON.parse(idCentroTrabajos), JSON.parse(nombreCentroTrabajos)];
let cargoLista = [JSON.parse(idCargos), JSON.parse(nombreCargos)];

$(function() {

    
    // -------------GENERAMOS MULTI CENTRO COSTO-----------------
    cargo = new GeneradorMultiRegistro('cargo','contenedorCargo','cargo');
    cargo.campoid = 'oidCargo';
    cargo.campoEliminacion = 'eliminarCargo';
    cargo.etiqueta = ['input','select'];
    cargo.tipo = ['hidden','text'];
    cargo.campos = ['oidTerceroCargo','Cargo_oidTerceroCargo']
    cargo.opciones = ['',cargoLista];
    cargo.clase = ['','chosen-select'];
    cargo.sololectura = [true,false];

    terceroCargo.forEach( dato => {cargo.agregarCampos(dato , 'L');
    });

    // -------------GENERAMOS MULTI CENTRO COSTO-----------------
    centroCosto = new GeneradorMultiRegistro('centroCosto','contenedorCentroCosto','centroCosto');
    centroCosto.campoid = 'oidCentroCosto';
    centroCosto.campoEliminacion = 'eliminarCentroCosto';
    centroCosto.etiqueta = ['input','select'];
    centroCosto.tipo = ['hidden','text'];
    centroCosto.campos = ['oidTerceroCentroCosto','CentroCosto_oidTerceroCentroCosto']
    centroCosto.opciones = ['',centroCostoLista];
    centroCosto.clase = ['','chosen-select'];
    centroCosto.sololectura = [true,false];

    terceroCentroCosto.forEach( dato => {centroCosto.agregarCampos(dato , 'L');
    });

    // -------------GENERAMOS MULTI CENTRO TRABAJP-----------------
    centroTrabajo = new GeneradorMultiRegistro('centroTrabajo','contenedorCentroTrabajo','centroTrabajo');
    centroTrabajo.campoid = 'oidCentroTrabajo';
    centroTrabajo.campoEliminacion = 'eliminarCentroTrabajo';
    centroTrabajo.etiqueta = ['input','select'];
    centroTrabajo.tipo = ['hidden','text'];
    centroTrabajo.campos = ['oidTercerocentroTrabajo','CentroTrabajo_oidTerceroCentroTrabajo']
    centroTrabajo.opciones = ['',centroTrabajoLista];
    centroTrabajo.clase = ['','chosen-select'];
    centroTrabajo.sololectura = [true,false];

    terceroCentroTrabajo.forEach( dato => {centroTrabajo.agregarCampos(dato , 'L');
    });


    let datePickerConfig = {
        format: 'yyyy-mm-dd',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
    };

    $("#daFechaRetiroPension").datepicker(datePickerConfig);
    $("#daFechaIngresoTerceroLaboral").datepicker(datePickerConfig);
    
    //iniciamos chosen select
    $('#Tercero_oidSalud').chosen();
    $('#Tercero_oidCesantias').chosen();
    $('#Tercero_oidPension').chosen();
    $('#Tercero_oidCaja').chosen();
});


function grabar(idPadre){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-Asn_TerceroLaboral';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let errors = resp.responseJSON.errors;
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $('#'+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        let messages = [];
		let validaciones = [
			{'input' :'GrupoNomina_oidTerceroLaboral' , 'message' : 'El campo Grupo Nómina es obligatorio.'},
			{'input' :'inPersonasACargoTerceroLaboral' , 'message' : 'El campo de Personas a Cargo es obligatorio.'},
			{'input' :'inNumeroHijosTerceroLaboral' , 'message' : 'El campo Número de Hijos es obligatorio.'},
			{'input' :'Tercero_oidSalud' , 'message' : 'El campo Administradora de salud es obligatorio.'},
			{'input' :'Tercero_oidCesantias' , 'message' : 'El campo Administradora de Cesantías es obligatorio.'},
            {'input' :'Tercero_oidCaja' , 'message' : 'El campo Caja de Compensación es obligatorio.'},
            {'input' :'daFechaIngresoTerceroLaboral' , 'message' : 'El campo Fecha ingreso es obligatorio.'},
            {'input' :'lsHorarioTerceroLaboral' , 'message' : 'El campo Horario es obligatorio.'},
            {'input' :'lsVinculacionTerceroLaboral' , 'message' : 'El campo Vinculación es obligatorio.'},
            {'input' :'deSalarioTerceroLaboral' , 'message' : 'El campo Salario es obligatorio.'},
            {'input' :'lsCabezaFamiliaTerceroLaboral' , 'message' : 'El campo Cabeza de familia es obligatorio.'},
		];
		validaciones.forEach( validacion => {
			let input = $('#'+validacion.input);
			let value = input.val();
			if(!value){
				input.addClass( errorClass );
				messages.push( validacion.message);
			}
		});
        return messages;
    }
}