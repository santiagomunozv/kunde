// convertir la clasificación de edades a array
let clasificacionEdades = Object.values(randoEdades);
let clasificacionSexo = Object.values(sexo);
let clasificacionEstadoCivil = Object.values(estadoCivil);
let clasificacionEstrato = Object.values(estrato);
let clasificacionPersonasCargo = Object.values(personasCargo);
let clasificacionTipoVivienda = Object.values(tipoVivienda);
let clasificacionEscolaridad = Object.values(escolaridad);
let clasificacionTieneEnfermedad = Object.values(tieneEnfermedad);
let clasificacionSalario = Object.values(salario);

// edades _______________________________________________________________
rangoText = [
  "18 - 25 años",
  "26- 35 años",
  "36- 45 años",
  "45- 55 años",
  "56 en adelante",
];

var oilCanvas = document.getElementById("edades");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var oilData = {
  labels: rangoText,
  datasets: [
    {
      data: clasificacionEdades,
      backgroundColor: [
        "#FF6384",
        "#63FF84",
        "#84FF63",
        "#8463FF",
        "#6384FF",
        "#DAF7A6",
        "#FF5733",
        "#900C3F",
      ],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

// sexo _________________________________________________________________

var oilCanvas = document.getElementById("sexo");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var oilData = {
  labels: ["Hombre", "Mujer"],
  datasets: [
    {
      data: clasificacionSexo,
      backgroundColor: ["#FF6384", "#63FF84", "#84FF63", "#8463FF", "#6384FF"],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//estado civil_____________________________________________________________

var oilCanvas = document.getElementById("estadoCivil");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var oilData = {
  labels: [
    "Soltero/a",
    "Casado/a",
    "Unión libre",
    "separado/a",
    "Divorciado/a",
    "Viudo/a",
  ],
  datasets: [
    {
      data: clasificacionEstadoCivil,
      backgroundColor: [
        "#FF6384",
        "#63FF84",
        "#84FF63",
        "#8463FF",
        "#6384FF",
        "#783851",
      ],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//Estrato_____________________________________________________________

var oilCanvas = document.getElementById("estrato");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;
console.log(clasificacionEstrato);
var oilData = {
  labels: ["1", "2", "3", "4", "otro"],
  datasets: [
    {
      data: clasificacionEstrato,
      backgroundColor: ["#FF6384", "#63FF84", "#84FF63", "#8463FF", "#6384FF"],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//Persoans a cargo_________________________________________________________
const ctx = document.getElementById("personasacargo").getContext("2d");
const myChart = new Chart(ctx, {
  type: "bar",
  data: {
    labels: ["0", "1", "2", "3", "4", "Mas de 5"],
    datasets: [
      {
        label: "# de personas a cargo",
        data: clasificacionPersonasCargo,
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(75, 192, 192, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
        borderWidth: 1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
});

//Tipo de vivienda_________________________________________________________

var oilCanvas = document.getElementById("vivienda");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var vivientaText = ["Propia", "Arrendada", "Familiar", "Otra"];

var oilData = {
  labels: vivientaText,
  datasets: [
    {
      data: clasificacionTipoVivienda,
      backgroundColor: ["#FF6384", "#63FF84", "#84FF63", "#8463FF", "#6384FF"],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//Escolaridad___________________________________________________________

var oilCanvas = document.getElementById("escolaridad");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var oilData = {
  labels: [
    "Primaria",
    "Secundaria",
    "Técnico/Tecnólogo",
    "Profesional",
    "Posgrado",
    "Otra",
  ],
  datasets: [
    {
      data: clasificacionEscolaridad,
      backgroundColor: [
        "#FF6384",
        "#63FF84",
        "#84FF63",
        "#8463FF",
        "#6384FF",
        "#783851",
      ],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//Salario___________________________________________________________

var oilCanvas = document.getElementById("salario");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var oilData = {
  labels: [
    "Menos de 1 SMLV",
    "1 SMLV",
    "2 - 3 SMLV",
    "4 - 5 SMLV",
    "6 - 7 SMLV",
    "Más de 7 SMLV",
  ],
  datasets: [
    {
      data: clasificacionSalario,
      backgroundColor: ["#FF6384", "#63FF84", "#8463FF", "#6384FF", "#84FF63"],
    },
  ],
};

var pieChart = new Chart(oilCanvas, {
  type: "pie",
  data: oilData,
});

//Tiene enfermedad_________________________________________________________
const ctxEnfermedad = document
  .getElementById("tieneEnfermedad")
  .getContext("2d");
const myChartEnfermedad = new Chart(ctxEnfermedad, {
  type: "bar",
  data: {
    labels: ["No", "Si"],
    datasets: [
      {
        label: "# de personas sin enfermedades",
        data: clasificacionTieneEnfermedad,
        backgroundColor: ["rgba(255, 99, 132, 0.2)", "rgba(54, 162, 235, 0.2)"],
        borderColor: ["rgba(255, 99, 132, 1)", "rgba(54, 162, 235, 1)"],
        borderWidth: 1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
});
