
$(function () {
})

function concatenarNombre() {
    $("#txNombreTercero").val($("#txPrimerNombreTercero").val() + " " + $("#txSegundoNombreTercero").val() + " " + $("#txPrimerApellidoTercero").val() + " " + $("#txSegundoApellidoTercero").val() + " ")
}

function concatenarDireccion() {
    $("#Direccion").val($("#Via1").val() + " " + $("#Numero1").val() + " " + $("#Nomenclatura1 option:selected").text() + " " + $("#Cardinalidad1").val() + " " + $("#Via2").val() + " " + $("#Numero2").val() + " " + $("#Nomenclatura2 option:selected").text() + " " + $("#Cardinalidad2 option:selected").text() + " " + $("#Placa").val() + " - " + $("#Complemento").val())
}

function abrirModalDireccion() {
    valores = $("#txDireccionTercero").val().split(' - '); // esto separa los datos de un campo texto por un espacio y los convierte en un array y lo coloca en datos

    datos = valores[0].split(' ')
    $('#Via1').val(datos[0]);
    $('#Numero1').val(datos[1]);
    $('#Nomenclatura1').val(datos[2]);
    $('#Cardinalidad1').val(datos[3]);
    $('#Via2').val(datos[4]);
    $('#Numero2').val(datos[5]);
    $('#Nomenclatura2').val(datos[6]);
    $('#Cardinalidad2').val(datos[7]);
    $('#Placa').val(datos[8]);

    $('#Complemento').val(valores[1]);

    $('#Direccion').val($("#txDireccionTercero").val());

    $('#ModalDireccion').modal('show');
}

function cerrarDireccion() {
    $("#txDireccionTercero").val($("#Direccion").val())
    $('.collapse').collapse('hide')
}

function modalTerceroGrid() {
    let ruta = '../asn_tercero/create';
    let iframe = $('#iframeGeneral');
    if (iframe.length) {
        iframe.attr('src', ruta);
        $('#ModalGeneral').modal('show');
    }

}

function generarDropzoneTercero() {
    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    var myDropzone = new Dropzone("div#dropzoneImageTercero", {
        headers: { 'X-CSRF-TOKEN': token },
        url: "/asociadonegocio/subirtercero",
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: "image/*",
        init: function () {
            this.on("success", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $("#imagenes").append("<input type='hidden' name='imagenTercero' value='" + pathImage.tmpPath + "'>");
            });
            this.on("removedfile", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $('input[name="imagenTercero"][value="' + pathImage.tmpPath + '"]').remove();
            });
        }
    });
}

function eliminarImagen(id) {
    $("#eliminarImagen").val(id);
    $("#imagen_" + id).remove();
}

function eliminarTercero(id) {
    modal.mostrarModal('Cuidado',
        '<div class="alert alert-danger">¿Realmente deseas eliminar este registro?</div>',
        function () {
            modal.cargando();
            let url = '/asociadonegocio/tercero/' + id;
            let token = $('meta[name="csrf-token"]').attr('content');
            let datos = { '_token': token, '_method': 'DELETE' };
            $.post(url, datos, function (resp) {
                modal.establecerAccionCerrar(function () {
                    location.href = '/asociadonegocio/tercero';
                });

                modal.mostrarModal('Eliminación exitosa', '<div class="alert alert-success">Se ha eliminado correctamente el registro</div>');
            })
        }
    );
}

let errorClass = 'is-invalid';
function mostrarErrores(errores) {
    let ul = document.createElement('ul');
    errores.forEach(function (error) {
        let li = document.createElement('li');
        li.innerHTML = error;
        ul.appendChild(li);
    });
    let div = document.createElement('div');
    div.appendChild(ul);
    div.className = 'alert alert-warning';
    document.getElementById('errores').appendChild(div);
}

function grabar() {
    $('#errores').empty();
    $('.' + errorClass).removeClass(errorClass);
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        mostrarErrores(mensajes);
    } else {
        // modal.cargando();
        let formularioId = '#form-asn_tercero';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                location.href = resp.id ? '/asociadonegocio/terceromenu/' + resp.id + '/' + resp.ct : '/asociadonegocio/tercerogrid?ct=' + resp.ct;
            });
            modal.mostrarModal('Informacion', '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        }, 'json').fail(function (resp) {
            let errors = resp.responseJSON.errors;
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $('#' + index).addClass(errorClass);
            });
            mostrarErrores(mensajes);
        });
    }


}

function validarForm() {
    let messages = [];
    if ($('#nombreVerificacion').val() == 'NOMBRE') {

        let validaciones = [
            { 'input': 'Ciudad_oidTercero', 'message': 'El campo Ciudad obligatorio.' },
            { 'input': 'TipoIdentificacion_oidTercero', 'message': 'El campo Tipo Identificación es obligatorio.' },
            { 'input': 'txDocumentoTercero', 'message': 'El campo Documento es obligatorio.' },
            { 'input': 'txCodigoTercero', 'message': 'El campo Código o contrato es obligatorio.' },
            { 'input': 'txPrimerNombreTercero', 'message': 'El Primer Nombre es obligatorio.' },
            { 'input': 'txPrimerApellidoTercero', 'message': 'El Primer Apellido es obligatorio.' },
            { 'input': 'txNombreTercero', 'message': 'La Razón Social es obligatorio.' },
            { 'input': 'txDireccionTercero', 'message': 'El campo de Dirección es obligatorio.' },
            { 'input': 'txMovilTercero', 'message': 'El campo Móvil es obligatorio.' },
            { 'input': 'txEstadoTercero', 'message': 'El Estado es obligatorio.' },
            { 'input': 'txTelefonoTercero', 'message': 'El teléfono es obligatorio.' },
            { 'input': 'txCorreoElectronicoTercero', 'message': 'El correo eléctronico es obligatorio.' },

        ];
        validaciones.forEach(validacion => {
            let input = $('#' + validacion.input);
            let value = input.val();
            if (!value) {
                input.addClass(errorClass);
                messages.push(validacion.message);
            }
        });


        // let valorCompanias = $('#Compania_oidCompania').serialize();
        // if(!valorCompanias){
        //     $('#Compania_oidCompania_chosen').addClass(errorClass)
        //     messages.push( "Debes seleccionar al menos una compañía-");
        // }

        // let tipoServicio = $('#lmTipoServicioTercero').serialize();
        // if (!tipoServicio) {
        //     $('#lmTipoServicioTercero_chosen').addClass(errorClass)
        //     messages.push("Debes seleccionar al menos un tipo de servicio");
        // }

        let clasificacion = $('#lmClasificacionTercero').serialize();
        if (!clasificacion) {
            $('#lmClasificacionTercero_chosen').addClass(errorClass)
            messages.push("Debes seleccionar al menos una clasificación");
        }

        let clasificacionTipo = $('#oidClasificacionTercero').serialize();
        if (!clasificacionTipo) {
            $('#oidClasificacionTercero').addClass(errorClass)
            messages.push("Debes seleccionar al menos una clasificación (Tipo Tercero)");
        }
        return messages;
    } else {
        let validaciones = [
            { 'input': 'Ciudad_oidTercero', 'message': 'El campo Ciudad obligatorio.' },
            { 'input': 'TipoIdentificacion_oidTercero', 'message': 'El campo Tipo Identificación es obligatorio.' },
            { 'input': 'txDocumentoTercero', 'message': 'El campo Documento es obligatorio.' },
            { 'input': 'txCodigoTercero', 'message': 'El campo Código o contrato es obligatorio.' },
            { 'input': 'txNombreTercero', 'message': 'La Razón Social es obligatorio.' },
            { 'input': 'txDireccionTercero', 'message': 'El campo de Dirección es obligatorio.' },
            { 'input': 'txMovilTercero', 'message': 'El campo Móvil es obligatorio.' },
            { 'input': 'txEstadoTercero', 'message': 'El Estado es obligatorio.' },
            { 'input': 'txTelefonoTercero', 'message': 'El teléfono es obligatorio.' },
            { 'input': 'txCorreoElectronicoTercero', 'message': 'El correo eléctronico es obligatorio' },



        ];
        validaciones.forEach(validacion => {
            let input = $('#' + validacion.input);
            let value = input.val();
            if (!value) {
                input.addClass(errorClass);
                messages.push(validacion.message);
            }
        });

        // let valorCompanias = $('#Compania_oidCompania').serialize();
        // if(!valorCompanias){
        //     $('#Compania_oidCompania_chosen').addClass(errorClass)
        //     messages.push( "Debes seleccionar al menos una compañía-");
        // }

        // let tipoServicio = $('#lmTipoServicioTercero').serialize();
        // if (!tipoServicio) {
        //     $('#lmTipoServicioTercero_chosen').addClass(errorClass)
        //     messages.push("Debes seleccionar al menos un tipo de servicio");
        // }


        let clasificacion = $('#lmClasificacionTercero').serialize();
        if (!clasificacion) {
            $('#lmClasificacionTercero_chosen').addClass(errorClass)
            messages.push("Debes seleccionar al menos una clasificación");
        }

        let clasificacionTipo = $('#oidClasificacionTercero').serialize();
        if (!clasificacionTipo) {
            $('#oidClasificacionTercero').addClass(errorClass)
            messages.push("Debes seleccionar al menos una clasificación (Tipo Tercero)");
        }
        return messages;
    }
}

function iniciarChosenSelect() {

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" },
    }
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }



}

// function llenarSelectCompania(valorCompania){
//     $("#Compania_oidCompania").val(valorCompania.split(',')).trigger("chosen:updated");
// }

function documentoTercero() {
    CalcularDv();
    verificarDocumento();
}

function CalcularDv() {
    if ($('#numeroVerificacion').val() == 1) {
        var vpri, x, y, z, i, nit1, dv1;

        nit1 = $("#txDocumentoTercero").val();
        if (isNaN(nit1)) {
            $("#txDigitoVerificacionTercero").val('');
        }
        else {
            vpri = new Array(16);

            x = 0;
            y = 0;
            z = nit1.length;

            vpri[1] = 3;
            vpri[2] = 7;
            vpri[3] = 13;
            vpri[4] = 17;
            vpri[5] = 19;
            vpri[6] = 23;
            vpri[7] = 29;
            vpri[8] = 37;
            vpri[9] = 41;
            vpri[10] = 43;
            vpri[11] = 47;
            vpri[12] = 53;
            vpri[13] = 59;
            vpri[14] = 67;
            vpri[15] = 71;

            for (i = 0; i < z; i++) {
                y = (nit1.substr(i, 1));
                x += (y * vpri[z - i]);
            }

            y = x % 11;

            if (y > 1) {
                dv1 = 11 - y;
            }
            else {
                dv1 = y;
            }
            $("#txDigitoVerificacionTercero").val(dv1);
        }

    } else {
        $("#txDigitoVerificacionTercero").val('');
    }
    let valorActual = $("#TipoIdentificacion_oidTercero").val();
    let input = document.getElementById("txDocumentoTercero");
    input.type = valorActual == 1 ? "number" : "text";

}

function verificarDocumento() {
    let Documento = $("#txDocumentoTercero").val();
    let DocumentoAnterior = $("#txDocumentoTerceroAnterior").val();
    if (Documento && !DocumentoAnterior) {
        let token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            dataType: "json",
            url: '/asociadonegocio/consultaDocumento/' + Documento,
            type: 'GET',
            success: function (resp) {
            },
            error: function (resp) {
                alert(resp.responseJSON.message);
            }
        });
    }

}
function consultarIdentificacion(id, $edit) {
    if (id) {
        document.getElementById("txDocumentoTercero").readOnly = false;
        document.getElementById("txDigitoVerificacionTercero").readOnly = true;
        let token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            dataType: "json",
            url: '/asociadonegocio/consultarIdentificacion/' + id,
            type: 'GET',
            success: function (resp) {
                if (resp['datos'][0]['lsTipoNombreTipoIdentificacion'] == 'NOMBRE') {
                    $('#divNombres').collapse("show");
                    //document.getElementById("txNombreTercero").readOnly = true;
                    $('#nombreVerificacion').val('NOMBRE')
                } else {
                    $('#divNombres').collapse("hide")
                    if ($edit != 1) {
                        //document.getElementById("txNombreTercero").readOnly = false;
                    }
                    $('#txNombreTercero').readOnly = false;
                    $('#nombreVerificacion').val('RAZON')
                }

                $('#numeroVerificacion').val(resp['datos'][0]['chDocumentoVerificacionTipoIdentificacion'])
                CalcularDv();
                if ($('#numeroVerificacion').val() == 0) {
                    document.getElementById("txDigitoVerificacionTercero").readOnly = false;
                }
                if (resp['datos'][0]['chDocumentoAutomaticoTipoIdentificacion'] == 1) {
                    document.getElementById("txDocumentoTercero").readOnly = true;
                    if (!$('#txDocumentoTercero').val()) {
                        $('#txDocumentoTercero').val(resp['numeroAutomatico'][0]['nuevo'])
                    }
                }

            },
            error: function (xhr, err) {
                modal.mostrarModal('Error', 'Ocurrio un problema');
            }
        });
    } else {
        $('#numeroVerificacion,#txDigitoVerificacionTercero').val('');
    }
}


function llenarValoresChosen(valorTipoServicio, valorClasificacion) {

    //llenamos las opciones de chosen de lmTipoServicioTercero -----------
    var selectServicio = document.getElementById('lmTipoServicioTercero');
    selectServicio.options.length = 0;
    var option = '';

    valorTipoServicio.forEach(sr => {
        option = document.createElement('option');
        option.value = sr.txNombre;
        option.text = sr.txNombre;
        // option.selected = valorTipoServicio && valorTipoServicio[0].indexOf(valorTipoServicio[0].txNombre) ? true : false;
        selectServicio.appendChild(option);
    });


    // option = document.createElement('option');
    // option.value = 'Asesoria';
    // option.text = 'Asesoría';
    // option.selected = valorTipoServicio && valorTipoServicio.indexOf("Asesoria") > -1 ? true : false;
    // selectServicio.appendChild(option);

    // option = document.createElement('option');
    // option.value = 'AdministradorSalud';
    // option.text = 'Administrador de salud';
    // option.selected = valorTipoServicio && valorTipoServicio.indexOf("AdministradorSalud") > -1 ? true : false;
    // selectServicio.appendChild(option);

    // option = document.createElement('option');
    // option.value = 'AdministradorPension';
    // option.text = 'Administrador de pensión';
    // option.selected = valorTipoServicio && valorTipoServicio.indexOf("AdministradorPension") > -1 ? true : false;
    // selectServicio.appendChild(option);

    // option = document.createElement('option');
    // option.value = 'AdministradorCesantias';
    // option.text = 'Administrador de cesantías';
    // option.selected = valorTipoServicio && valorTipoServicio.indexOf("AdministradorCesantias") > -1 ? true : false;
    // selectServicio.appendChild(option);

    // option = document.createElement('option');
    // option.value = 'AdministradorCompensacion';
    // option.text = 'Caja de compensación';
    // option.selected = valorTipoServicio && valorTipoServicio.indexOf("AdministradorCompensacion") > -1 ? true : false;
    // selectServicio.appendChild(option);

    option = document.createElement('option');
    option.value = 'Otros';
    option.text = 'Otros';
    option.selected = valorTipoServicio && valorTipoServicio.indexOf("Otros") > -1 ? true : false;
    selectServicio.appendChild(option);

    //llenamos las opciones de chosen de lmClasificacionTercero ------------
    var selectClasificacion = document.getElementById('lmClasificacionTercero');
    selectClasificacion.options.length = 0;
    var option = '';

    option = document.createElement('option');
    option.value = 'Manual';
    option.text = 'Manual';
    option.selected = valorClasificacion && valorClasificacion.indexOf("Manual") > -1 ? true : false;
    selectClasificacion.appendChild(option);

    option = document.createElement('option');
    option.value = 'Sin factura';
    option.text = 'Sin factura';
    option.selected = valorClasificacion && valorClasificacion.indexOf("Sin factura") > -1 ? true : false;
    selectClasificacion.appendChild(option);

    option = document.createElement('option');
    option.value = 'Electrónica';
    option.text = 'Electrónica';
    option.selected = valorClasificacion && valorClasificacion.indexOf("Electrónica") > -1 ? true : false;
    selectClasificacion.appendChild(option);
}

function cambiarEstadoSG(idTercero) {
    modal.cargando();
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        url: '/asociadonegocio/cambiarEstadoSG/' + idTercero,
        type: 'GET',
        success: function (resp) {
            modal.mostrarModal('Informacion', '<div class="alert alert-success">Se cambio el estado de sistema de gestión</div>', function () {
                location.reload(true);
            });
        },
        error: function (resp) {
            modal.mostrarModal('Error', 'se produjo un error al cambiar el estado', function () {
                location.reload(true);
            });
        }
    });
}

function descargarResponsabilidad(oidTercero) {
    window.open('http://' + location.host + '/asociadonegocio/generarresponsabilidad/' + oidTercero, '_blank', 'width=2500px, height=700px, scrollbars=yes');
}

function importarTareas() {
    let formularioId = "#form-importartareas";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    console.log(data);
    $.post(route, data, function (resp) {
        modal.establecerAccionCerrar(function () {
            location.href = "/asociadonegocio/tercerogrid";
        });
        modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
    }, "json").fail(function (resp) {
        modal.mostrarModal("Informacion", "<div class=\"alert alert-danger\">" + resp.responseJSON.message + "</div>");
    });
}


function cargarTareas() {
    modal.cargando()
    $.get('/asociadonegocio/importartareas', function (resp) {
        modal.mostrarModal('Importar tareas', resp, function () {
            //esta es la accion del boton aceptar
            modal.cargando();
        }).extraGrande().sinPie();
        generarDropzone();
    }).fail(function (resp) {
        modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
    });
}

function generarDropzone() {
    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    new Dropzone("div#dropzoneImage", {
        headers: { 'X-CSRF-TOKEN': token },
        url: "/asociadonegocio/cargarArchivoTareas",
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".xls, .xlsx",
        init: function () {
            this.on("success", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $("#imagenes").append("<input type='hidden' name='imagenTercero' value='" + pathImage.tmpPath + "'>");
                $("#nombreArchivo").val(file.name);
            });
            this.on("removedfile", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $('input[name="imagenTercero"][value="' + pathImage.tmpPath + '"]').remove();
            });
        }
    });
}