$(function () {
    // Copasst
    tercerocopasst = new GeneradorMultiRegistro('tercerocopasst', 'contenedorasn_copasst', 'tercerocopasst');

    tercerocopasst.altura = '35px;';
    tercerocopasst.campoid = 'oidTerceroCopasst';
    tercerocopasst.campoEliminacion = 'eliminarAsn_TerceroCopasst';
    tercerocopasst.botonEliminacion = true;
    tercerocopasst.funcionEliminacion = '';

    tercerocopasst.campos = ['oidTerceroCopasst', 'txTipoTerceroCopasst', 'txNombreTerceroCopasst', 'inDocumentoTerceroCopasst'];

    tercerocopasst.etiqueta = ['input', 'input', 'input', 'input'];
    tercerocopasst.tipo = ['hidden', 'hidden', 'text', 'number'];
    tercerocopasst.estilo = ['', '', '', ''];
    tercerocopasst.clase = ['', '', '', ''];
    tercerocopasst.sololectura = [true, true, false, false];
    tercerocopasst.opciones = ['', '', '', ''];
    tercerocopasst.funciones = ['', '', '', '', '',];
    tercerocopasst.otrosAtributos = ['', '', '', ''];
    valorAsn_TerceroCopasst.forEach(dato => {
        tercerocopasst.agregarCampos(dato, 'L');
    });

    // Cocola
    tercerococola = new GeneradorMultiRegistro('tercerococola', 'contenedorasn_cocola', 'tercerococola');

    tercerococola.altura = '35px;';
    tercerococola.campoid = 'oidTerceroCocola';
    tercerococola.campoEliminacion = 'eliminarAsn_TerceroCocola';
    tercerococola.botonEliminacion = true;
    tercerococola.funcionEliminacion = '';

    tercerococola.campos = ['oidTerceroCocola', 'txTipoTerceroCocola', 'txNombreTerceroCocola', 'inDocumentoTerceroCocola'];

    tercerococola.etiqueta = ['input', 'input', 'input', 'input', 'input'];
    tercerococola.tipo = ['hidden', 'hidden', 'text', 'number'];
    tercerococola.estilo = ['', '', '', '', '', '', '', ''];
    tercerococola.clase = ['', '', '', '', '', '', '', ''];
    tercerococola.sololectura = [true, true, false, false, false, false, false, false];
    tercerococola.opciones = ['', '', '', '', '', '', '', ''];
    tercerococola.funciones = ['', '', '', '', '', '', '', ''];
    tercerococola.otrosAtributos = ['', '', '', '', '', '', '', ''];

    valorAsn_TerceroCocola.forEach(dato => {
        tercerococola.agregarCampos(dato, 'L');
    });

    // Convivencia
    terceroconvivencia = new GeneradorMultiRegistro('terceroconvivencia', 'contenedorasn_convivencia', 'terceroconvivencia');

    terceroconvivencia.altura = '35px;';
    terceroconvivencia.campoid = 'oidTerceroConvivencia';
    terceroconvivencia.campoEliminacion = 'eliminarAsn_TerceroConvivencia';
    terceroconvivencia.botonEliminacion = true;
    terceroconvivencia.funcionEliminacion = '';

    terceroconvivencia.campos = ['oidTerceroConvivencia', 'txTipoTerceroConvivencia', 'txNombreTerceroConvivencia', 'inDocumentoTerceroConvivencia'];

    terceroconvivencia.etiqueta = ['input', 'input', 'input', 'input'];
    terceroconvivencia.tipo = ['hidden', 'hidden', 'text', 'number'];
    terceroconvivencia.estilo = ['', '', '', ''];
    terceroconvivencia.clase = ['', '', '', ''];
    terceroconvivencia.sololectura = [true, true, false, false];
    terceroconvivencia.opciones = ['', '', '', ''];
    terceroconvivencia.funciones = ['', '', '', ''];
    terceroconvivencia.otrosAtributos = ['', '', '', ''];

    valorAsn_TerceroConvivencia.forEach(dato => {
        terceroconvivencia.agregarCampos(dato, 'L');
    });

});

let errorClass = 'is-invalid';
function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = '#form-asn_integrantescomite';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion', '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        }, 'json').fail(function (resp) {
            let keyNombre = 'txNombreTerceroIntegranteComite';
            let nombres = $('input[name="' + keyNombre + '[]"]');
            $.each(resp.responseJSON.errors, function (index, value) {
                index = index.replace('.', '');
                mensajes.push(value);
                if (index.startsWith(keyNombre)) {
                    setErroresMultiregistro(index.substr(keyNombre.length, index.length), nombres);
                } else {
                    $('#' + index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function setErroresMultiregistro(index, array) {
        if (array && array.length) {
            array[index].classList.add(errorClass);
        }
    }

    function validarForm() {
        let mensajes = [];

        var nombreCopasst = $('input[name="txNombreTerceroCopasst[]"]');
        var maxLength = nombreCopasst.length >= nombreCopasst.length ? nombreCopasst.length : nombreCopasst.length;
        for (let i = 0; i < maxLength; i++) {
            let nombre = nombreCopasst[i];
            if (nombre && !nombre.value) {
                nombre.classList.add(errorClass);
                mensajes.push('El Nombre de los datos Copasst en el registro ' + (i + 1) + ' es requerido');
            }
        }

        var nombreCocola = $('input[name="txNombreTerceroCocola[]"]');
        var maxLength = nombreCocola.length >= nombreCocola.length ? nombreCocola.length : nombreCocola.length;
        for (let i = 0; i < maxLength; i++) {
            let nombre = nombreCocola[i];
            if (nombre && !nombre.value) {
                nombre.classList.add(errorClass);
                mensajes.push('El Nombre de los datos Cocola es en el registro ' + (i + 1) + ' es requerido');
            }
        }

        var nombreConvivencia = $('input[name="txNombreTerceroConvivencia[]"]');
        var maxLength = nombreConvivencia.length >= nombreConvivencia.length ? nombreConvivencia.length : nombreConvivencia.length;
        for (let i = 0; i < maxLength; i++) {
            let nombre = nombreConvivencia[i];
            if (nombre && !nombre.value) {
                nombre.classList.add(errorClass);
                mensajes.push('El Nombre de los datos de Convivencia en el registro ' + (i + 1) + ' es requerido');
            }

        }
        return mensajes;
    }
}
