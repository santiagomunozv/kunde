$(function(){
    let datePickerConfig = {
        format: 'yyyy-mm-dd',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
    };
    $("#fechaInicialCreacion").datepicker(datePickerConfig)
    $("#fechaFinalCreacion").datepicker(datePickerConfig)

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" },
    }
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

 
        $('#informeTercero').stickyTableHeaders();


})

function consultar(){
    let mensajes = validaciones();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        var formId = '#terceroParametroInforme';
        var token = document.getElementById('token').value;
        $.ajax({
            async: true,
            headers: {'X-CSRF-TOKEN': token},
            url: $(formId).attr('action'),
            type: $(formId).attr('method'),
            data: $(formId).serialize(),
            dataType: 'html',
            success: function(result){
        
            },
            error: function(){
                alert('Ha ocurrido un error en el servidor');
            }
        });
    }
}

function validaciones(){
    let messages = [];
    let errorClass = 'is-invalid';
    let validaciones = [
        {'input' :'fechaInicialCreacion' , 'message' : 'El campo fecha inicio de creación'},
        {'input' :'fechaFinalCreacion' , 'message' : 'El campo fecha fin de creación.'},
    ];
    validaciones.forEach( validacion => {
        let input = $('#'+validacion.input);         
        let value = input.val();
        if(!value){
            input.addClass(errorClass);
            messages.push( validacion.message);
        }
    });

    let tipoTercero = $('#clasificacionTerceroTipo').serialize();
    if(!tipoTercero){
        $('#clasificacionTerceroTipo_chosen').addClass(errorClass)
        messages.push( "Debes seleccionar almenos una clasificación de tercero (Tipo Tercero)");
    }

    let valorCompanias = $('#CompaniaTercero').serialize();
    if(!valorCompanias){
        $('#CompaniaTercero_chosen').addClass(errorClass)
        messages.push( "Debes seleccionar al menos una compañía");
    }

    if( messages && messages.length){
        modal.mostrarErrores(messages);
    }
}

function mostrarGrupos(id){
    $("#Clientes").addClass("d-none");
    $("#Proveedores").addClass("d-none");
    $("#Empleados").addClass("d-none");
    $("#Bancos").addClass("d-none");
    $("#Intermediariosaduaneros").addClass("d-none");
    $("#SeguridadSocial").addClass("d-none");

    option = $("#clasificacionTerceroTipo").val();    
    for (let index = 0; index < option.length; index++) {
        if(option[index] == 1){
            $("#Clientes").addClass("d-none");
            $("#Clientes").removeClass("d-none");
        }   
        if(option[index] == 2){
            $("#Proveedores").addClass("d-none");
            $("#Proveedores").removeClass("d-none");
        }
        if(option[index] == 3){
            $("#Empleados").addClass("d-none");
            $("#Empleados").removeClass("d-none");
        }
        if(option[index] == 4){
            $("#Bancos").addClass("d-none");
            $("#Bancos").removeClass("d-none");
        }
        if(option[index] == 5){
            $("#Intermediariosaduaneros").addClass("d-none");
            $("#Intermediariosaduaneros").removeClass("d-none");
        }
        if(option[index] == 6){
            $("#SeguridadSocial").addClass("d-none");
            $("#SeguridadSocial").removeClass("d-none");
        }
        
    }
}