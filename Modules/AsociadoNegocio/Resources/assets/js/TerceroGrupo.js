$(function(){         
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    } 
});

function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#terceroGrupo';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            console.log(resp);
            if(resp.responseJSON.message){
                modal.mostrarErrores([resp.responseJSON.message]);
            }else{
                modal.mostrarErrores([],route,resp);
            }
        });
    }
}

function validarFormulario(){
    modal.cargando();
    let formularioId = '#terceroGrupo';
    let route = '/asociadonegocio/validacionTerceroGrupo';
    let data = $(formularioId).serialize();
    $.post(route,data, function( resp ){
        grabar();
    },'json').fail( function(resp){
        modal.mostrarErrores([resp.responseJSON.message]);
    });
}

function validarObservacion(valor,reg){
    let token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        headers: { 'X-CSRF-TOKEN': token },
        dataType: "json",
        url: '/asociadonegocio/consultaObservacion/'+valor,
        type: 'GET',
        success: function(resp){
            modal.mostrarModal('Observación','<label>Observación:</label><input type="text" class="form-control" id="observacion"></input>',function(){
                // $('#grupo'+reg).removeClass('col-sm-6');
                // $('#grupo'+reg).addClass('col-sm-3');
                observacion = document.getElementById("observacion").value;
                $("#grupo"+reg).append("<input type='text' class='form-control col-sm-12' name='txObservacionTerceroGrupo["+reg+"]' id='txObservacionTerceroGrupo"+reg+"' value='"+observacion+"'>");
                modal.cerrarModal();
            });
        },
        error: function(resp) {
            $("#txObservacionTerceroGrupo"+reg).remove();
            $("#nombreObservacion"+reg).remove();
        }
    });
}

