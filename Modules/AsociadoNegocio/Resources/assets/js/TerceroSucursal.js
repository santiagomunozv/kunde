
function concatenarDireccion(){
    $("#Direccion").val($("#Via1").val()+" "+$("#Numero1").val()+" "+$("#Nomenclatura1 option:selected").text()+" "+$("#Cardinalidad1").val()+" "+$("#Via2").val()+" "+$("#Numero2").val()+" "+$("#Nomenclatura2 option:selected").text()+" "+$("#Cardinalidad2 option:selected").text()+" "+$("#Placa").val()+" - "+$("#Complemento").val())
}

function abrirModalDireccion(idCampo){
    idCampo = idCampo.replace('boton','txDireccionTerceroSucursal');
    $('#posicionMulti').val(idCampo.replace('txDireccionTerceroSucursal',''))
    


    valores = $("#"+idCampo).val().split(' - ');
    datos = valores[0].split(' ')
    $('#Via1').val(datos[0]);
    $('#Numero1').val(datos[1]);
    $('#Nomenclatura1').val(datos[2]);
    $('#Cardinalidad1').val(datos[3]);
    $('#Via2').val(datos[4]);
    $('#Numero2').val(datos[5]);
    $('#Nomenclatura2').val(datos[6]);
    $('#Cardinalidad2').val(datos[7]);
    $('#Placa').val(datos[8]);

    $('#Complemento').val(valores[1]);

    $('#Direccion').val($("#"+idCampo).val());

    $('#ModalDireccion').modal('show');

}

function cerrarModalDireccion(){
    reg =  $('#posicionMulti').val();
    $("#txDireccionTerceroSucursal"+reg).val($("#Direccion").val());

$('#ModalDireccion').modal('hide');
}

function mostrarErrores(errores){
    let ul = document.createElement('ul');
    errores.forEach(function( error ){
        let li = document.createElement('li');
        li.innerHTML = error;
        ul.appendChild( li );
    });
    let div = document.createElement('div');
    div.appendChild(ul);
    div.className = 'alert alert-warning';
    document.getElementById('errores').appendChild(div);
}

let errorClass = 'is-invalid';
function grabar(){
    modal.cargando();
    $('#errores').empty();
    $('.'+errorClass).removeClass( errorClass );
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-asn_tercerosucursal';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>', function(){
                location.reload();
            });
        },'json').fail( function(resp){
            let keyNombre = 'txNombreComercialTerceroSucursal';
            let keyCodigo = 'txCodigoTerceroSucursal';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let codigo = $('input[name="'+keyCodigo+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyCodigo)){
                    setErroresMultiregistro(index.substr(keyCodigo.length , index.length) , codigo);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }
}

function setErroresMultiregistro( index , array){
    if(array && array.length){
        array[index].classList.add(errorClass);
    }
}
 
function validarForm(){
    let messages = [];
    let validaciones = [
        {'input' :'txCodigoTerceroSucursal' , 'message' : 'El campo Código es obligatorio.'},
        {'input' :'txNombreComercialTerceroSucursal' , 'message' : 'El campo Nombre es obligatorio.'},
        {'input' :'Ciudad_oidTerceroSucursal' , 'message' : 'El campo de ciudad es obligatorio.'},
        {'input' :'txTelefono1TerceroSucursal' , 'message' : 'El campo de teléfono es obligatorio.'},
        {'input' :'txDireccionTerceroSucursal' , 'message' : 'El campo de dirección es obligatorio.'},
    ];
    validaciones.forEach( validacion => {
        let input = $('#'+validacion.input);
        let value = input.val();
        if(!value){
            input.addClass( errorClass );
            messages.push( validacion.message);
        }
    });
    
    return messages;
}


function modalSucursalCrear(idPadre){
    modal.cargando()
    $.get('/asociadonegocio/tercerosucursalcrear/'+idPadre, function(resp){
        modal.mostrarModal('Asociados de negocio: ', resp , function(){
            //esta es la accion del boton aceptar
            modal.cargando();

        }).extraGrande().sinPie();
        $('#Ciudad_oidTerceroSucursal').chosen();
        }).fail(function(resp){
            modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema' , 'status:'+resp.status);
        });
    
}

function modalSucursalEdit(idSucursal){
    modal.cargando()
    $.get('/asociadonegocio/tercerosucursaledit/'+idSucursal , function(resp){
        modal.mostrarModal('Asociados de negocio: ', resp , function(){
            //esta es la accion del boton aceptar
            modal.cargando();
        }).extraGrande().sinPie();
        $('#Ciudad_oidTerceroSucursal').chosen();
        }).fail(function(resp){
            modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema' , 'status:'+resp.status);
        });
}