$(document).ready( function () {
    let datePickerConfig = {
        format: 'yyyy-mm-dd',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
    };

	$("#daFechaNacimientoTerceroConvivencia").datepicker(datePickerConfig)
});


$(function(){
    let direccionPersonal = ['onclick','modalDireccion()']
    //tercero Personal
    terceropersonal = new GeneradorMultiRegistro('terceropersonal','contenedorasn_terceropersonal','terceropersonal');
            
    terceropersonal.altura = '35px;';
    terceropersonal.campoid = 'oidTerceroPersonal';
    terceropersonal.campoEliminacion = 'eliminarAsn_TerceroPersonal';
    terceropersonal.botonEliminacion = true;
    terceropersonal.funcionEliminacion = '';  

    terceropersonal.campos = ['oidTerceroPersonal','txTipoTerceroPersonal','txNombreTerceroPersonal','txOcupacionTerceroPersonal','txTelefonoTerceroPersonal','txDireccionTerceroPersonal','txObservacionTerceroPersonal'];

    terceropersonal.etiqueta = ['input','input','input','input','input','input','input'];
    terceropersonal.tipo = ['hidden','hidden','text','text','text','text','text'];
    terceropersonal.estilo = ['','','','','','',''];
    terceropersonal.clase = ['','','','','','',''];
    terceropersonal.sololectura = [true,true,false,false,false,false,false];     
    terceropersonal.opciones = ['','','','','','',''];     
    terceropersonal.funciones = ['','','','','',direccionPersonal,''];
    terceropersonal.otrosAtributos = ['','','','','','',''];
    valorAsn_TerceroPersonal.forEach( dato => {terceropersonal.agregarCampos(dato , 'L');
    });
    
    //tercero comercial
    tercerocomercial = new GeneradorMultiRegistro ('tercerocomercial','contenedorasn_tercerocomercial','tercerocomercial');
            
    tercerocomercial.altura = '35px;';
    tercerocomercial.campoid = 'oidTerceroComercial';
    tercerocomercial.campoEliminacion = 'eliminarAsn_TerceroComercial';
    tercerocomercial.botonEliminacion = true;
    tercerocomercial.funcionEliminacion = '';  

    tercerocomercial.campos = ['oidTerceroComercial','txTipoTerceroComercial','txNombreTerceroComercial','txParentescoTerceroComercial','txOcupacionTerceroComercial','txTelefonoTerceroComercial','txDireccionTerceroComercial','txObservacionTerceroComercial'];

    tercerocomercial.etiqueta = ['input','input','input','input','input','input','input','input'];
    tercerocomercial.tipo = ['hidden','hidden','text','text','text','text','text','text'];
    tercerocomercial.estilo = ['','','','','','','',''];
    tercerocomercial.clase = ['','','','','','','',''];
    tercerocomercial.sololectura = [true,true,false,false,false,false,false,false]; 
    tercerocomercial.opciones = ['','','','','','','',''];   
    tercerocomercial.funciones = ['','','','','','','',''];
    tercerocomercial.otrosAtributos = ['','','','','','','',''];

    valorAsn_TerceroComercial.forEach( dato => {tercerocomercial.agregarCampos(dato , 'L');
    });

    //tercero convivecia
    terceroconvivencia = new GeneradorMultiRegistro ('terceroconvivencia','contenedorasn_terceroconvivencia','terceroconvivencia');
                
    terceroconvivencia.altura = '35px;';
    terceroconvivencia.campoid = 'oidTerceroConvivencia';
    terceroconvivencia.campoEliminacion = 'eliminarAsn_TerceroConvivencia';
    terceroconvivencia.botonEliminacion = true;
    terceroconvivencia.funcionEliminacion = '';  

    terceroconvivencia.campos = ['oidTerceroConvivencia','txTipoTerceroConvivencia','txNombreTerceroConvivencia','txParentescoTerceroConvivencia','daFechaNacimientoTerceroConvivencia','txNivelEducativoTerceroConvivencia','txOcupacionTerceroConvivencia','txObservacionTerceroConvivencia','txTelefonoTerceroConvivencia'];

    terceroconvivencia.etiqueta = ['input','input','input','input','date','input','input','input','input'];
    terceroconvivencia.tipo = ['hidden','hidden','text','text','date','text','text','text','text'];
    terceroconvivencia.estilo = ['','','','','','','','',''];
    terceroconvivencia.clase = ['','','','','','','','',''];
    terceroconvivencia.sololectura = [true,true,false,false,false,false,false,false,false];
    terceroconvivencia.opciones = ['','','','','','','','',''];
    terceroconvivencia.funciones = ['','','','','','','','',''];
    terceroconvivencia.otrosAtributos = ['','','','','','','','',''];

    valorAsn_TerceroConvivencia.forEach( dato => {terceroconvivencia.agregarCampos(dato , 'L');
    });


    //tercero Familiar
    tercerofamiliar = new GeneradorMultiRegistro ('tercerofamiliar','contenedorasn_tercerofamiliar','tercerofamiliar');
                
    tercerofamiliar.altura = '35px;';
    tercerofamiliar.campoid = 'oidTerceroFamiliar';
    tercerofamiliar.campoEliminacion = 'eliminarAsn_TerceroFamiliar';
    tercerofamiliar.botonEliminacion = true;
    tercerofamiliar.funcionEliminacion = '';  

    tercerofamiliar.campos = ['oidTerceroFamiliar','txTipoTerceroFamiliar','txNombreTerceroFamiliar','txParentescoTerceroFamiliar','txOcupacionTerceroFamiliar','txTelefonoTerceroFamiliar','txDireccionTerceroFamiliar','txObservacionTerceroFamiliar'];

    tercerofamiliar.etiqueta = ['input','input','input','input','input','input','input','input'];
    tercerofamiliar.tipo = ['hidden','hidden','text','text','text','text','text','text'];
    tercerofamiliar.estilo = ['','','','','','','',''];
    tercerofamiliar.clase = ['','','','','','','',''];
    tercerofamiliar.sololectura = [true,true,false,false,false,false,false,false];
    tercerofamiliar.opciones = ['','','','','','','',''];
    tercerofamiliar.funciones = ['','','','','','','',''];
    tercerofamiliar.otrosAtributos = ['','','','','','','',''];

    valorAsn_TerceroFamiliar.forEach( dato => {tercerofamiliar.agregarCampos(dato , 'L');
    });

    //tercero Financiero
    tercerofinanciero = new GeneradorMultiRegistro ('tercerofinanciero','contenedorasn_tercerofinancieras','tercerofinanciero');
                
    tercerofinanciero.altura = '35px;';
    tercerofinanciero.campoid = 'oidTerceroFinancieras';
    tercerofinanciero.campoEliminacion = 'eliminarAsn_TerceroFinancieras';
    tercerofinanciero.botonEliminacion = true;
    tercerofinanciero.funcionEliminacion = '';  

    tercerofinanciero.campos = ['oidTerceroFinancieras','txTipoTerceroFinancieras','txNombreTerceroFinancieras','txTelefonoTerceroFinancieras','txDireccionTerceroFinancieras','txObservacionTerceroFinancieras'];

    tercerofinanciero.etiqueta = ['input','input','input','input','input','input'];
    tercerofinanciero.tipo = ['hidden','hidden','text','text','text','text'];
    tercerofinanciero.estilo = ['','','','','',''];
    tercerofinanciero.clase = ['','','','','',''];
    tercerofinanciero.sololectura = [true,true,false,false,false,false];
        
    tercerofinanciero.opciones = ['','','','','',''];
        
    tercerofinanciero.funciones = ['','','','','',''];
    tercerofinanciero.otrosAtributos = ['','','','','',''];

    valorAsn_TerceroFinancieras.forEach( dato => {tercerofinanciero.agregarCampos(dato , 'L');
    });

});


function modalDireccion(){

}



let errorClass = 'is-invalid';
function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = '#form-asn_terceroreferencia';
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
                location.href = '';
            });
            modal.mostrarModal('Informacion' , '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
        },'json').fail( function(resp){
            let keyNombre = 'txNombreTerceroReferencia';
            let keyTelefono = 'txTelefonoTerceroReferencia';
            let nombres = $('input[name="'+keyNombre+'[]"]');
            let telefono = $('input[name="'+keyTelefono+'[]"]')
            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace('.','');
                mensajes.push( value );
                if( index.startsWith(keyTelefono)){
                    setErroresMultiregistro(index.substr(keyTelefono.length , index.length) , telefono);
                }else if(index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function setErroresMultiregistro( index , array){
        if(array && array.length){
            array[index].classList.add(errorClass);
        }
    }

    function validarForm(){
        let mensajes = [];
        var nombres = $('input[name="txNombreTerceroPersonal[]"]');
        var telefonos = $('input[name="txTelefonoTerceroPersonal[]"]');
        var maxLength = telefonos.length >= nombres.length ? telefonos.length : nombres.length;
        for(let i = 0 ; i < maxLength; i++){
            let telefono = telefonos[i];
            let nombre = nombres[i];
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de los datos personales en el registro '+(i + 1)+' es requerido');
            }
            if(telefono && !telefono.value){
                telefono.classList.add( errorClass );
                mensajes.push('El Teléfono de los datos personales en el registro '+(i + 1)+' es requerido');
            }
        }

        var nombreComercial = $('input[name="txNombreTerceroComercial[]"]');
        var telefonosComercial = $('input[name="txTelefonoTerceroComercial[]"]');
        var maxLength = telefonosComercial.length >= nombreComercial.length ? telefonosComercial.length : nombreComercial.length;
        for(let i = 0 ; i < maxLength; i++){
            let telefono = telefonosComercial[i];
            let nombre = nombreComercial[i];
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de los datos Comerciales en el registro '+(i + 1)+' es requerido');
            }
            if(telefono && !telefono.value){
                telefono.classList.add( errorClass );
                mensajes.push('El Teléfono de los datos Comerciales en el registro '+(i + 1)+' es requerido');
            }
        }

        var nombreConvivencia = $('input[name="txNombreTerceroConvivencia[]"]');
        var maxLength = telefonosComercial.length >= nombreConvivencia.length ? telefonosComercial.length : nombreConvivencia.length;
        for(let i = 0 ; i < maxLength; i++){
            let nombre = nombreConvivencia[i];
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de los datos de Convivencia en el registro '+(i + 1)+' es requerido');
            }
            
        }

        var nombreFamiliar = $('input[name="txNombreTerceroFamiliar[]"]');
        var telefonosFamiliar = $('input[name="txTelefonoTerceroFamiliar[]"]');
        var maxLength = telefonosFamiliar.length >= nombreFamiliar.length ? telefonosFamiliar.length : nombreFamiliar.length;
        for(let i = 0 ; i < maxLength; i++){
            let telefono = telefonosFamiliar[i];
            let nombre = nombreFamiliar[i];
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de los datos Familiares en el registro '+(i + 1)+' es requerido');
            }
            if(telefono && !telefono.value){
                telefono.classList.add( errorClass );
                mensajes.push('El Teléfono de los datos Familiares en el registro '+(i + 1)+' es requerido');
            }
        }

        var nombreFinancieros = $('input[name="txNombreTerceroFinancieras[]"]');
        var telefonosFinancieros = $('input[name="txTelefonoTerceroFinancieras[]"]');
        var maxLength = telefonosFinancieros.length >= nombreFinancieros.length ? telefonosFinancieros.length : nombreFinancieros.length;
        for(let i = 0 ; i < maxLength; i++){
            let telefono = telefonosFinancieros[i];
            let nombre = nombreFinancieros[i];
            if(nombre && !nombre.value){
                nombre.classList.add( errorClass );
                mensajes.push('El Nombre de los datos Financieros en el registro '+(i + 1)+' es requerido');
            }
            if(telefono && !telefono.value){
                telefono.classList.add( errorClass );
                mensajes.push('El Teléfono de los datos Financieros en el registro '+(i + 1)+' es requerido');
            }
        }
        return mensajes;
    }
}
