let errorClass = "is-invalid";

let tipoDocumentoTercero = [
  JSON.parse(idTipoDocumento),
  JSON.parse(nombreTipoDocumento),
];

$(function () {
  terceroempleado = new GeneradorMultiRegistro(
    "terceroempleado",
    "contenedorterceroempleado",
    "terceroempleado"
  );

  terceroempleado.campoid = "oidTerceroEmpleado";
  terceroempleado.campoEliminacion = "eliminarEmpleado";
  terceroempleado.botonEliminacion = true;
  terceroempleado.funcionEliminacion = "";
  terceroempleado.campos = [
    "oidTerceroEmpleado",
    "btnFormularioEmpleado",
    "TipoIdentificacion_oidTerceroEmpleado",
    "txDocumentoEmpleado",
    "txNombreEmpleado",
    "txApellidoEmpleado",
  ];
  terceroempleado.etiqueta = [
    "input",
    "button",
    "select",
    "input",
    "input",
    "input",
    "input",
  ];
  terceroempleado.tipo = ["hidden", "button", "text", "number", "text", "text"];
  terceroempleado.estilo = ["", "", "", "", "", ""];
  terceroempleado.clase = [
    "",
    "fas fa-address-card",
    "chosen-select",
    "",
    "",
    "",
  ];
  terceroempleado.sololectura = [true, false, false, false, false, false];
  terceroempleado.opciones = ["", "", tipoDocumentoTercero, "", "", ""];
  terceroempleado.funciones = ["", "", "", "", "", ""];
  terceroempleado.otrosAtributos = ["", "", "", "", "", ""];

  let reg = 0;
  // console.log(empleados);
  empleados.forEach((dato) => {
    terceroempleado.agregarCampos(dato, "L");

    $("#btnFormularioEmpleado" + reg).attr(
      "onClick",
      "modalPerfilSociodemografico(" +
        document.getElementById("oidTerceroEmpleado" + reg).value +
        ")"
    );
    reg = reg + 1;
  });
});

// Perfil sociodemografico modal
function modalPerfilSociodemografico(id) {
  modal.cargando();
  $.get("/asociadonegocio/perfilsociodemografico/" + id, function (resp) {
    modal
      .mostrarModal("ENCUESTA PERFIL SOCIODEMOGRÁFICO", resp, function () {
        modal.cargando();
      })
      .extraGrande()
      .sinPie();
    // console.log(resp);
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

function grabar() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-terceroempleado";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();

    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          // console.log(resp);
          // window.close();
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        index = index.replace(".", "");
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }
}

function setErroresMultiregistro(index, array) {
  if (array && array.length) {
    array[index].classList.add(errorClass);
  }
}

function validarForm() {
  let mensajes = [];

  let tipoDocumentos = $(
    'select[name="TipoIdentificacion_oidTerceroEmpleado[]"]'
  );
  let documentos = $('input[name="txDocumentoEmpleado[]"]');
  let nombres = $('input[name="txNombreEmpleado[]"]');
  let apellidos = $('input[name="txApellidoEmpleado[]"]');
  let maxLength =
    documentos.length >= nombres.length ? documentos.length : nombres.length;
  for (let i = 0; i < maxLength; i++) {
    let tipoDocumento = tipoDocumentos[i];
    let documento = documentos[i];
    let nombre = nombres[i];
    let apellido = apellidos[i];

    if (tipoDocumento.value == "" || tipoDocumento.value == 0) {
      tipoDocumento.classList.add(errorClass);
      mensajes.push(
        "El tipo de documento del contacto en el registro " +
          (i + 1) +
          " es requerido"
      );
    }

    if (documento && !documento.value) {
      documento.classList.add(errorClass);
      mensajes.push(
        "El documento del contacto en el registro " + (i + 1) + " es requerido"
      );
    }

    if (nombre && !nombre.value) {
      nombre.classList.add(errorClass);
      mensajes.push(
        "El Nombre del Contacto en el registro " + (i + 1) + " es requerido"
      );
    }
    if (apellido && !apellido.value) {
      apellido.classList.add(errorClass);
      mensajes.push(
        "El apellido del Contacto en el registro " + (i + 1) + " es requerido"
      );
    }
  }
  return mensajes;
}

function cargarEmpleados(id) {
  modal.cargando();
  $.get("/asociadonegocio/importarempleados/" + id, function (resp) {
    modal
      .mostrarModal("Importar clientes", resp, function () {
        //esta es la accion del boton aceptar
        modal.cargando();
      })
      .extraGrande()
      .sinPie();

    generarDropzone();
    // console.log(resp);
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

function descargarEmpleados(id) {
  // console.log(id);
  window.open(
    "http://" + location.host + "/asociadonegocio/exportarempleados/" + id
  );
}

function generarDropzone() {
  let token = $('meta[name="csrf-token"]').attr("content");
  var myDropzone = new Dropzone("div#dropzoneImage", {
    headers: { "X-CSRF-TOKEN": token },
    url: "/asociadonegocio/cargararchivoempleados",
    addRemoveLinks: true,
    maxFiles: 1,
    acceptedFiles: ".xls, .xlsx",
    init: function () {
      this.on("success", function (file) {
        let pathImage = JSON.parse(file.xhr.response);
        $("#imagenes").append(
          "<input type='hidden' name='imagenTercero' value='" +
            pathImage.tmpPath +
            "'>"
        );
        $("#nombreArchivo").val(file.name);
      });
      this.on("removedfile", function (file) {
        let pathImage = JSON.parse(file.xhr.response);
        $(
          'input[name="imagenTercero"][value="' + pathImage.tmpPath + '"]'
        ).remove();
      });
    },
  });
}

function importarEmpleados() {
  let formularioId = "#form-importarempleados";
  let route = $(formularioId).attr("action");
  let data = $(formularioId).serialize();

  $.post(
    route,
    data,
    function (resp) {
      modal.establecerAccionCerrar(function () {
        location.href = "";
      });
      modal.mostrarModal(
        "Informacion",
        '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
      );
    },
    "json"
  ).fail(function (resp) {
    modal.mostrarModal(
      "Informacion",
      '<div class="alert alert-danger">' + resp.responseJSON.message + "</div>"
    );
  });
}

function plantillaEmpleado() {
  window.open(
    "http://" +
      location.host +
      "/asociadonegocio/terceroempleado/plantillaempleado"
  );
}

// Perfil sociodemográfico ----------------------------------------------------------
let parentesco = [
  "Abuelo/a",
  "Cuñado/a",
  "Esposo/a",
  "Familiar",
  "Hermano/a",
  "Hijo/a",
  "Padre/Madre",
  "Amigo/a",
  "Suegro/a",
  "Tio/a",
  "Vecino/a",
];

let grupoparentesco = [parentesco, parentesco];
// console.log(grupoparentesco);

function consultarFammilia(modelo) {
  // mostrar tabla
  let tabla = $("#tabla-ComFamiliarEmpleado");
  let mensaje = $("#mensajeFamilair");
  let arrayFamiliares = JSON.parse(modelo);

  tabla[0].style.visibility = "visible";
  // generar multiregistro
  generarMultiRegistroGrupoFamiliar(modelo);
  if (!arrayFamiliares.length) {
    //  mostrar mensaje de advertencia
    mensaje.html("No se encontraron familiares");
  }
  // inhabilitamos botton para no duplicar datos
  $("#btnConsultarFamilia").prop("disabled", true);
  mensaje.html("Se encontraron " + arrayFamiliares.length + " registros");
  mensaje[0].style.visibility = "visible";
}

function generarMultiRegistroGrupoFamiliar(modelo) {
  let familiares = JSON.parse(modelo);

  composicionfamiliar = new GeneradorMultiRegistro(
    "composicionfamiliar",
    "contenedorcomposicionfamiliar",
    "composicionfamiliar"
  );

  composicionfamiliar.campoid = "oidFamiliarEmpleado";
  composicionfamiliar.campoEliminacion = "eliminarComFamiliar";
  composicionfamiliar.botonEliminacion = true;
  composicionfamiliar.funcionEliminacion = "";
  composicionfamiliar.campos = [
    "oidFamiliarEmpleado",
    "lsParentescoFamiliar",
    "txCelularFamiliar",
    "txNombreFamiliar",
    "Empleado_oidFamiliarEmpleado",
  ];
  composicionfamiliar.etiqueta = ["input", "select", "input", "input", "input"];
  composicionfamiliar.tipo = ["hidden", "text", "number", "text", "hidden"];
  composicionfamiliar.estilo = ["", "", "", "", ""];
  composicionfamiliar.clase = ["", "chosen-select", "", "", ""];
  composicionfamiliar.sololectura = [true, false, false, false, true];
  composicionfamiliar.opciones = ["", grupoparentesco, "", "", ""];
  composicionfamiliar.funciones = ["", "", "", "", ""];
  composicionfamiliar.otrosAtributos = ["", "", "", "", ""];

  familiares.forEach((dato) => {
    composicionfamiliar.agregarCampos(dato, "L");
    console.log(dato);
  });
}
// departamento
function consultarCiudad(ciudadId, selectId) {
  if (ciudadId) {
    $.ajax({
      headers: { "X-CSRF-TOKEN": token },
      dataType: "json",
      url: "/asociadonegocio/consultarCiudadEmpleado/" + ciudadId,
      type: "GET",
      success: function (resp) {
        var ciudades = resp;
        addOptionsSelect(selectId, ciudades);
      },
      error: function (xhr, err) {
        modal.mostrarModal("Error", "Ocurrio un problema");
      },
    });
  }
}

function addOptionsSelect(domElement, objeto) {
  // agregar opciones a un <select>
  var $select = $("#" + domElement)[0];
  $.each(objeto, function (id, name) {
    var option = document.createElement("option");
    option.text = name;
    option.value = id;
    $select.add(option);
  });
}

function mostrarErrores(errores) {
  let ul = document.createElement("ul");
  errores.forEach(function (error) {
    let li = document.createElement("li");
    li.innerHTML = error;
    ul.appendChild(li);
  });
  let div = document.createElement("div");
  div.appendChild(ul);
  div.className = "alert alert-warning";
  document.getElementById("errores").appendChild(div);
}

function grabarPerfil() {
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarEncuestaForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    // modal.cargando();
    let formularioId = "#form-perfilsociodemografico";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          // window.close();
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      let errors = resp.responseJSON.errors;
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }
}

function validarEncuestaForm() {
  let mensajes = [];
  // validar campos
  let validaciones = [
    {
      input: "txTerceroNombreTercero",
      message: "El campo Empresa es obligatorio.",
    },
    {
      input: "Cargo_oidTerceroEmpleado",
      message: "El campo Cargo es obligatorio.",
    },
    {
      input: "txNombreEmpleado",
      message: "El Nombre empleado es obligatorio.",
    },
    {
      input: "txApellidoEmpleado",
      message: "El campo Apellido empleado es obligatorio.",
    },
    {
      input: "txDocumentoEmpleado",
      message: "El campo Documento empleado es obligatorio.",
    },
    {
      input: "txDireccionEmpleado",
      message: "El campo Dirección es obligatorio.",
    },
    { input: "txCelularEmpleado", message: "El campo Celular es obligatorio." },
    {
      input: "txCorreoEmpleado",
      message: "El campo Correo Electrónico es obligatorio.",
    },
    { input: "txEpsEmpleado", message: "El campo EPS es obligatorio." },
    {
      input: "txHorarioTrabajoEmpleado",
      message: "El Horario detrabajo es obligatorio.",
    },
    {
      input: "dtFechaNacimientoEmpleado",
      message: "La fecha de nacimiento es obbligatoria",
    },
    {
      input: "txBarrioViviendaEmpleado",
      message: "El barrio de residencia es obbligatorio",
    },
    {
      input: "txFondoPensionEmpleado",
      message: "El fondo de pensión es obbligatorio",
    },
  ];

  validaciones.forEach((validacion) => {
    let input = $("#" + validacion.input);
    let value = input.val();
    if (!value) {
      input.addClass(errorClass);
      mensajes.push(validacion.message);
    }
  });

  // validar selects
  let selects = {
    "Departamento de nacimiento": "Pais_oidTerceroEmpleadoN",
    "Ciudad de nacimiento": "Ciudad_oidTerceroEmpleadoN",
    "ARL del empleado": "Arl_oidEmpleado",
    "Tipo de contrato": "txTipoContratoEmpleado",
    "Genero del empleado": "txGeneroEmpleado",
    "Estado civil": "txEstadoCivilEmpleado",
    "Tipo de vivienda": "lsTipoViviendaEmpleado",
    "Nivel de escolaridad": "lsEscolaridadEmpleado",
    "Promedio de ingresos": "lsIngresosEmpleado",
    "Estrato empleado": "lsEstratoEmpleado",
    "Tiempo en la empresa": "txTiempoEmpresaEmpleado",
    "Edad empleado": "txEdadEmpleado",
    "Numero de personas a cargo": "intPersonasCargoEmpleado",
  };

  for (const campo in selects) {
    if (
      $("#" + selects[campo])
        .val()
        .trim() === ""
    ) {
      $("#" + selects[campo]).addClass(errorClass);
      mensajes.push("Debes seleccionar almenos un " + campo);
    } else {
      $("#" + selects[campo]).removeClass(errorClass);
    }
  }

  // validar multiregistro
  let parentescos = $('select[name="lsParentescoFamiliar[]"]');
  let documentos = $('input[name="txCelularFamiliar[]"]');
  let nombres = $('input[name="txNombreFamiliar[]"]');

  let maxLength =
    documentos.length >= nombres.length ? documentos.length : nombres.length;
  for (let i = 0; i < maxLength; i++) {
    let parentesco = parentescos[i];
    let documento = documentos[i];
    let nombre = nombres[i];

    if (parentesco.value == "" || parentesco.value == 0) {
      parentesco.classList.add(errorClass);
      mensajes.push(
        "El tipo de documento del familiar en el registro " +
          (i + 1) +
          " es requerido"
      );
    }

    if (documento && !documento.value) {
      documento.classList.add(errorClass);
      mensajes.push(
        "El documento del familiar en el registro " + (i + 1) + " es requerido"
      );
    }

    if (nombre && !nombre.value) {
      nombre.classList.add(errorClass);
      mensajes.push(
        "El Nombre del familiar en el registro " + (i + 1) + " es requerido"
      );
    }
  }

  return mensajes;
}
// validar direccion
function concatenarDireccion() {
  $("#Direccion").val(
    $("#Via1").val() +
      " " +
      $("#Numero1").val() +
      " " +
      $("#Nomenclatura1 option:selected").text() +
      " " +
      $("#Cardinalidad1").val() +
      " " +
      $("#Via2").val() +
      " " +
      $("#Numero2").val() +
      " " +
      $("#Nomenclatura2 option:selected").text() +
      " " +
      $("#Cardinalidad2 option:selected").text() +
      " " +
      $("#Placa").val() +
      " - " +
      $("#Complemento").val()
  );
}

function abrirModalDireccion() {
  valores = $("#txDireccionEmpleado").val().split(" - "); // esto separa los datos de un campo texto por un espacio y los convierte en un array y lo coloca en datos

  datos = valores[0].split(" ");
  $("#Via1").val(datos[0]);
  $("#Numero1").val(datos[1]);
  $("#Nomenclatura1").val(datos[2]);
  $("#Cardinalidad1").val(datos[3]);
  $("#Via2").val(datos[4]);
  $("#Numero2").val(datos[5]);
  $("#Nomenclatura2").val(datos[6]);
  $("#Cardinalidad2").val(datos[7]);
  $("#Placa").val(datos[8]);

  $("#Complemento").val(valores[1]);

  $("#Direccion").val($("#txDireccionEmpleado").val());

  $("#ModalDireccion").modal("show");
}

function cerrarDireccion() {
  $("#txDireccionEmpleado").val($("#Direccion").val());
  $(".collapse").collapse("hide");
}
