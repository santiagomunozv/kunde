<?php


use App\Exports\TerceroEmpleadosExport;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

Route::group(['middleware' => 'auth'], function () {

    Route::prefix('asociadonegocio')->group(function () {
        Route::get('/', 'AsociadoNegocioController@index');

        // Ruta para el controlador
        Route::resource('/tercero', 'TerceroController');
        Route::resource('/parametroInforme', 'TerceroInformeController');
        Route::post('subirtercero', 'TerceroController@subirTercero');
        Route::get('tercerogrid', 'TerceroController@index');
        Route::get('consultaimagen/{id}', 'TerceroController@consultaImagen');
        Route::get('eliminarimagen/{id}', 'TerceroController@eliminarImagen');
        Route::get('terceromenu/{id}/{ct}', 'TerceroController@terceroMenu');

        Route::get('grupotercero', 'GrupoTerceroController@index');
        Route::get('grupotercero/{id}/edit', 'GrupoTerceroController@edit');
        Route::put('grupotercero/{id}', 'GrupoTerceroController@update');

        // RUTAS PARA LAS TABLAS HIJAS DE TERCERO
        Route::get('tercerocontacto/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroContacto');
        Route::put('tercerocontacto/{id}', 'TerceroController@updateTerceroContacto');

        Route::get('terceropersonal/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroPersonal');
        Route::put('terceropersonal/{id}', 'TerceroController@updateTerceroPersonal');

        Route::get('tercerolaboral/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroLaboral');
        Route::put('tercerolaboral/{id}', 'TerceroController@updateTerceroLaboral');

        Route::get('tercerocomercial/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroComercial');
        Route::put('tercerocomercial/{id}', 'TerceroController@updateTerceroComercial');

        Route::get('terceroreferencia/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroReferencia');
        Route::put('terceroreferencia/{id}', 'TerceroController@updateTerceroReferencia');

        Route::get('tercerologistica/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroLogistica');
        Route::put('tercerologistica/{id}', 'TerceroController@updateTerceroLogistica');

        Route::get('tercerocertificado/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroCertificado');
        Route::put('tercerocertificado/{id}', 'TerceroController@updateTerceroCertificado');

        Route::get('terceroverificacion/{id}/{idClasificacion}/{tipo}/edit', 'TerceroController@editTerceroVerificacionOEA');
        Route::put('terceroverificacion/{id}', 'TerceroController@updateTerceroVerificacionOEA');

        Route::get('tercerobanco/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroBanco');
        Route::put('tercerobanco/{id}', 'TerceroController@updateTerceroBanco');

        Route::get('tercerogrupo/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroGrupo');
        Route::put('tercerogrupo/{id}', 'TerceroController@updateTerceroGrupo');

        Route::get('terceroRazonSocial/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroRazonSocial');
        Route::put('terceroRazonSocial/{id}', 'TerceroController@updateTerceroRazonSocial');

        Route::get('terceroFacturaElectronica/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroFacturaElectronica');
        Route::put('terceroFacturaElectronica/{id}', 'TerceroController@updateTerceroFacturaElectronica');

        // terceroEmpleado
        Route::get('terceroempleado/{id}/{idClasificacion}/edit', 'TerceroEmpleadoController@edit');
        Route::put('terceroempleado/{id}', 'TerceroEmpleadoController@update');

        Route::get('importarempleados/{id}', 'TerceroEmpleadoController@importarEmpleadosIndex');
        Route::post('importarempleados/{id}', 'TerceroEmpleadoController@importarEmpleados');

        Route::get('exportarempleados/{id}', 'TerceroEmpleadoController@exportarEmpleados');

        Route::get('terceroempleado/plantillaempleado/', 'TerceroEmpleadoController@descargarPlantillaEmpleado');

        Route::post('cargararchivoempleados', 'TerceroEmpleadoController@cargarArchivoEmpleado');

        Route::get('estadisticastercero/{id}', 'EstadisticaTerceroController@index');

        // Perfil sociodemográfico
        Route::get('perfilsociodemografico/{id}', 'PerfilSociodemogradicoController@index');
        Route::put('perfilsociodemografico/{id}', 'PerfilSociodemogradicoController@update');

        Route::get('consultarCiudadEmpleado/{id}', 'PerfilSociodemogradicoController@consultarCiudadEmpleado');

        Route::get('terceroimpresion/{id}', 'TerceroController@show');

        //Sucursal Grid
        Route::get('tercerosucursalgrid/{id}/{idClasificacion}/edit', 'TerceroController@terceroSucursalGrid');
        Route::get('tercerosucursalcrear/{id}', 'TerceroController@crearTerceroSucursal');
        Route::get('tercerosucursaledit/{id}', 'TerceroController@editTerceroSucursal');
        Route::put('tercerosucursalupdate', 'TerceroController@updateTerceroSucursal');


        // productos Tercero Grupo
        Route::get('grupoproducto/{id}/{idClasificacion}/edit', 'TerceroController@editGrupoProducto');
        Route::put('grupoproducto/{id}', 'TerceroController@updateGrupoProducto');
        Route::get('listargrupos', 'TerceroController@listaGrupos');
        Route::get('listarclasificacionesproducto', 'TerceroController@listaClasificacionesProducto');

        //ajax de tercerojs
        Route::get('consultarIdentificacion/{id}', 'TerceroController@consultarIdentificacion');
        Route::post('subirCertificado', 'TerceroController@subirCertificado');
        Route::get('imprimirCertificado/{nombreArchivo}', 'TerceroController@imprimirCertificado');

        //Ajax para datatables
        Route::get('/terceroDataAjax', 'TerceroController@consultaAjax');
        Route::get('consultaDocumento/{documento}', 'TerceroController@consultaDocumento');

        //Ajax para datatables
        Route::get('consultaSelectOea/{idPregunta}', 'TerceroController@consultaSelectOea');

        //Cron certificado
        Route::get('cronTercero', 'TerceroController@certificadoVencimiento');
        //validacion grupos dinamicos
        Route::put('validacionTerceroGrupo', 'TerceroController@validacionTerceroGrupo');
        //consulta si tiene observaciones los grupos dinamicos
        Route::get('consultaObservacion/{valor}', 'TerceroController@consultaObservacionTerceroGrupo');
        //informes tercero
        Route::get('generarCondicion/{parametro}', 'TerceroInformeController@generarCondicion');
        //Estado de sistemas de gestion
        Route::get('cambiarEstadoSG/{idTercero}', 'TerceroController@cambiarEstadoSG');

        Route::get('generarresponsabilidad/{id}', 'TerceroController@generarResponsabilidad');

        Route::resource('/terceroadjuntodocumento', 'TerceroAdjuntoDocumentoController');

        // Tipos de servicio
        Route::resource('/tipodeservicios', 'TerceroAdjuntoDocumentoController');

        // configuracion eventos
        Route::get('configuracioneventos/{id}/{idClasificacion}/edit', 'ConfiguracionEventoController@edit');
        // Route::post('configuracioneventos/{id}', 'ConfiguracionEventoController@update');
        Route::resource('configuracioneventos', 'ConfiguracionEventoController')->parameters(['configuracioneventos' => 'evento']);

        Route::post('cargarArchivoTareas', 'TerceroController@cargarArchivoTareas');
        Route::get('/importartareas', 'TerceroController@indexImportarTareas');
        Route::post('/importartareas', 'TerceroController@importarTareas');

        Route::get('integrantescomites/{id}/{idClasificacion}/edit', 'TerceroController@editTerceroIntegranteComite');
        Route::put('integrantescomites/{id}', 'TerceroController@updateTerceroIntegranteComite');
        Route::get('generarplantrabajotercero/{id}', 'ConfiguracionEventoController@generarPlanTrabajoTercero');
        Route::get('enviarnotificacionplantrabajotercero/{id}', 'ConfiguracionEventoController@enviarNotificacionPlanTrabajoTercero');
    });
});
