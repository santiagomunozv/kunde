@section('scripts')
    {{Html::script("modules/registroseguimiento/js/prospectoForm.js")}} 
@endsection
    {!! Form::open(['url' => "registroseguimiento/importarprospectos", 'method' => 'POST', 'id' => 'form-importarprospectos', 'files' => true])!!}

        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

        @foreach($observaciones as $observacionReg)
            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <table id="prospecto-table" class="table table-hover table-sm scalia-grid">
                        <thead class="bg-primary text-light">
                            <tr>
                                <th>Atiende</th>
                                <th>Encargado</th>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Observación</th>
                                <th>Resultado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <td>{{$observacionReg->txAtiendeProspectoSeguimiento}}</td>
                            <td>{{$observacionReg->txEncargadoProspectoSeguimiento}}</td>
                            <td>{{$observacionReg->dtFechaSeguimientoProspectoSeguimiento}}</td>
                            <td>{{$observacionReg->hrHoraSeguimientoProspectoSeguimiento}}</td>
                            <td>{{$observacionReg->txObservacionSeguimiento}}</td>
                            <td>{{$observacionReg->txNombreResultado}}</td>
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    {!! Form::close() !!}