@extends("layouts.principal")
@section("nombreModulo")
    Campañas
@endsection
@section("scripts")
    {{Html::script("modules/registroseguimiento/js/campanaForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($campana->oidCampana))
        {!!Form::model($campana,["route"=>["campana.update",$campana->oidCampana],"method"=>"PUT", "id"=>"form-campana" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($campana,["route"=>["campana.store",$campana->oidCampana],"method"=>"POST", "id"=>"form-campana", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCampana', null, array('id' => 'oidCampana')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoCampana', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoCampana',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el código'])!!}
                    </div>
                </div>
            </div>
            
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreCampana', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreCampana',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txEstadoCampana', 'Estado', array('class' => 'text-md text-primary mb-1  required ')) !!}
                        {!!Form::select('txEstadoCampana',['Activo' => 'Activo','Inactivo' => 'Inactivo'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona el estado de campaña'])!!}                    
                    </div>
                </div>
            </div>    
            @if(isset($campana->oidCampana))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        