@extends('layouts.principal')
@section('nombreModulo')
    Jornada empleado
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("jornadaEmpleado-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="jornadaEmpleado-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/registroseguimiento/jornadaempleado', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Empleado</th>
                            <th>Metas de llamada</th>
                            <th>Unidad de medida llamada</th>
                            <th>Metas de cita</th>
                            <th>Unidad de medida cita</th>
                            <th>Horas laborales</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jornadaEmpleado as $jornadaEmpleado)
                            <tr>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/registroseguimiento/jornadaempleado', [$jornadaEmpleado->oidJornadaEmpleado, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $jornadaEmpleado->oidJornadaEmpleado }}', 'rgs_jornadaempelado', 'Jornada empleado')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $jornadaEmpleado->oidJornadaEmpleado }}</td>
                                <td>{{ $jornadaEmpleado->txNombreTercero }}</td>
                                <td>{{ $jornadaEmpleado->inMetaLlamadasJornadaEmpleado }}</td>
                                <td>{{ $jornadaEmpleado->txUnidadMedidaLlamadasJornadaEmpleado }}</td>
                                <td>{{ $jornadaEmpleado->inMetaCitasJornadaEmpleado }}</td>
                                <td>{{ $jornadaEmpleado->txUnidadMedidaCitasJornadaEmpleado }}</td>
                                <td>{{ $jornadaEmpleado->inHorasLaboralesJornadaEmpleado }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Empleado</th>
                            <th>Metas de llamada</th>
                            <th>Unidad de medida llamada</th>
                            <th>Metas de cita</th>
                            <th>Unidad de medida cita</th>
                            <th>Horas laborales</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
