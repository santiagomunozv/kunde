@section('scripts')
    {{Html::script("modules/registroseguimiento/js/prospectoForm.js")}} 
@endsection
    {!! Form::open(['url' => "registroseguimiento/importarprospectos", 'method' => 'POST', 'id' => 'form-importarprospectos', 'files' => true])!!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            <input type="hidden" name="nombreArchivo" id="nombreArchivo" value="">

            <div class="col-md-12 pb-2">
                <div id="dropzoneImage" class="dropzone-div">
                    <div class="dz-message">
                        <div class="col-xs-8">
                            <div class="message">
                                <p>haz click para buscar cargar un archivo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="fallback">
                        <input type="file" name="file" multiple style="display: none;">
                    </div>
                </div>
            </div>

            {!!Form::button("Importar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"importarProspectos()"])!!}

        </div>
    </div>
    {!! Form::close() !!}