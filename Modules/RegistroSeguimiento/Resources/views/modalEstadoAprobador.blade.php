@section('scripts')
    {{ Html::script('modules/registroseguimiento/js/tareaForm.js') }}
@endsection

{!! Form::open(['url' => ['/registroseguimiento/tarea/cambiarestadoaprobador/' . $tarea->oidTarea], 'method' => 'POST', 'id' => 'form-estadoaprobador', 'onsubmit' => 'return false;']) !!}
{!! Form::hidden('oidTarea', $tarea->oidTarea, ['id' => 'oidTarea']) !!}
{!! Form::hidden('estado', null, ['id' => 'estado']) !!}

<input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
<div id="errores"></div>
<div class="row">
    <div class="col-sm-12 text-center">
        <i class="fas fa-check-circle fa-7x text-success"></i>
        <p class="lead mb-4">¿A que estado desea cambiar la tarea actual?</p>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group ">
            {!! Form::text('observacionAprobador', null, ['class' => 'form-control', 'placeholder' => 'ingrese el motivo... ', 'required']) !!}
        </div>
    </div>
</div>


<div class="trans text-center">
    {!! Form::button('Aprobar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'cambiarEstadoAprobador("Aprobado")']) !!}
    {!! Form::button('Rechazar', ['type' => 'button', 'class' => 'btn btn-danger', 'onclick' => 'cambiarEstadoAprobador("Rechazado")']) !!}
</div>




{!! Form::close() !!}
