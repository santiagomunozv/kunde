<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe empleado</title>
    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        p.normal {
          font-style: normal;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #3A64AE;
          color: white;
        }

        .textCenter{
            text-align: center !important;
        }
        </style>
        <link rel="stylesheet" href="css/fontawesome.css">
</head>
<body>
    <table id="customers">
        <tr> 
            <th colspan="8" class="textCenter"> Informe empleado registro seguimiento</th>
        </tr>
        <tr>
            <td colspan="8" class="textCenter">&nbsp;</td>
        </tr>
        @php
            $reg = 0;
        @endphp
        @while ($reg < count($consulta))
            @php
                $nombreTercero = $consulta[$reg]->nombreEmpleado; 
            @endphp
            <tr>
                <th colspan="8">{{$nombreTercero}}</th>
            </tr>
            <tr>
                <th>Fecha</th>
                <th>Horas laborales</th>
                <th>Meta de llamadas</th>
                <th>Exitoso</th>
                <th>No contestaron</th>
                <th>No aceptaron</th>
                <th>Errado</th>
                <th>Llamadas realizadas</th>
            </tr>
            @while ($reg < count($consulta) && $nombreTercero == $consulta[$reg]->nombreEmpleado)
                <tr>
                    <td>{{$consulta[$reg]->fechaSeguimiento}}</td>
                    <td>{{$consulta[$reg]->horasLaborales}}</td>
                    <td>{{$consulta[$reg]->metaLlamadas}}</td>
                    <td>{{$consulta[$reg]->exitoso}}</td>
                    <td>{{$consulta[$reg]->noContestaron}}</td>
                    <td>{{$consulta[$reg]->noAceptaron}}</td>
                    <td>{{$consulta[$reg]->errado}}</td>
                    <td>{{$consulta[$reg]->llamadasRealizadas}}</td>
                    
                </tr>
                @php
                    $reg ++;
                @endphp
            @endwhile
        @endwhile
    </table>
</body>
</html>