@extends('layouts.principal')
@section('nombreModulo')
    Tareas
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("tarea-table");
        });

        var arrayAreas = "";
        arrayAreas = arrayAreas.split(",");

        var arrayResponsables = "";
        arrayResponsables = arrayResponsables.split(",");
    </script>
    {{ Html::script('modules/registroseguimiento/js/tareaForm.js') }}
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="table-responsive">
                <a class="primary" href="#" onclick="cambiarEstado('');" title="Todos">
                    <i class="fas fa-filter fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Nuevo');" title="Nuevo">
                    <i class="far fa-file-alt fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('En Proceso');" title="En proceso">
                    <i class="fas fa-file-invoice-dollar fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Terminado');" title="Terminado">
                    <i class="far fa-check-square fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Rechazado');" title="Rechazado">
                    <i class="fas fa-times-circle fa-2x"></i>
                </a>
                <table id="tarea-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/registroseguimiento/tarea', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Fecha de asignación</th>
                            <th>Prioridad</th>
                            <th>Asunto</th>
                            <th>Área</th>
                            <th>Responsable</th>
                            <th>Fecha de vencimiento asignada</th>
                            <th>Solución</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tarea as $tarea)
                            <tr style="background-color:#{{ $tarea->colorTarea }}; color:black">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/registroseguimiento/tarea', [$tarea->oidTarea, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $tarea->oidTarea }}', 'rgs_tarea', 'Tarea')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button type="button"
                                                onclick="cambiarEstadoTarea('{{ $tarea->oidTarea }}', 'rgs_tarea', 'Tarea')"
                                                class="{{ $tarea->txEstadoTarea === 'Nuevo' ? 'btn btn-info' : 'btn btn-warning' }} btn-sm"
                                                title="Estados">
                                                <i
                                                    class="{{ $tarea->txEstadoTarea === 'Nuevo' ? 'fas fa-check' : 'fas fa-undo' }}"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button type="button"
                                                onclick="modalEstadoAprobador('{{ $tarea->oidTarea }}', 'rgs_tarea', 'Tarea')"
                                                class="{{ $tarea->estadoAprobador === 'Pendiente' ? 'btn btn-success' : 'btn btn-secondary' }} btn-sm">
                                                <i class="fas fa-tasks"></i>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $tarea->dtFechaElaboracionTarea }}</td>
                                <td>{{ $tarea->txPrioridadTarea }}</td>
                                <td>{{ $tarea->txAsuntoTarea }}</td>
                                <td>{{ $tarea->txAreaTarea }}</td>
                                <td>{{ $tarea->txResponsableTarea }}</td>
                                <td>{{ $tarea->dtFechaVencimientoTarea }}</td>
                                <td>{{ $tarea->txSolucionTarea }}</td>
                                <td>{{ $tarea->txEstadoTarea }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Fecha de asignación</th>
                            <th>Prioridad</th>
                            <th>Asunto</th>
                            <th>Área</th>
                            <th>Responsable</th>
                            <th>Fecha de vencimiento asignada</th>
                            <th>Solución</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
