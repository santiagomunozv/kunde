@extends('layouts.principal')
@section('nombreModulo')
    Servicios
@endsection
@section('scripts')
    {{ Html::script('modules/registroseguimiento/js/servicioForm.js') }}
@endsection
@section('contenido')
    @if (isset($servicio->oidServicio))
        {!! Form::model($servicio, [
            'route' => ['servicio.update', $servicio->oidServicio],
            'method' => 'PUT',
            'id' => 'form-servicio',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($servicio, [
            'route' => ['servicio.store', $servicio->oidServicio],
            'method' => 'POST',
            'id' => 'form-servicio',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidServicio', null, ['id' => 'oidServicio']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txNombreServicio', 'Nombre', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txNombreServicio', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el nombre']) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionServicio', 'Descripción ', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::text('txDescripcionServicio', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la descripción ',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txEstadoServicio', 'Estado', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select('txEstadoServicio', ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'], null, [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Selecciona el estado del servicio',
                        ]) !!}
                    </div>
                </div>
            </div>
            @if (isset($servicio->oidServicio))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
