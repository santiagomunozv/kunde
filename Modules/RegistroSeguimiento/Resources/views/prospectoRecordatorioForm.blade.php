@extends('layouts.principal')

@section('nombreModulo')
    Recordatorio
@endsection

@section('scripts')
    {{Html::script("modules/registroseguimiento/js/prospectoRecordatorioForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($prospectorecordatorio) --}}
    {!! Form::model($prospecto, ['url' => ["registroseguimiento/prospectorecordatorio/$id"], 'method' => 'PUT', 'id' => 'form-prospectorecordatorio'])
    !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreProspecto', 'Nombre', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::text('txNombreProspecto',null,[ 'class'=>'form-control', 'readonly'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inTelefonoProspecto', 'Teléfono', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::number('inTelefonoProspecto',null,['class'=>'form-control', 'readonly'])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('dtFechaSeguimientoProspectoSeguimiento', 'Fecha', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::date('dtFechaSeguimientoProspectoSeguimiento',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('hrHoraSeguimientoProspectoSeguimiento', 'Hora', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::time('hrHoraSeguimientoProspectoSeguimiento',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
            </div>

            
            {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}

        {!! Form::close() !!}
    </div>
</div>    
@endsection