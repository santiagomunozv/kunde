@extends('layouts.principal')
@section('nombreModulo')
    Prospectos
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("prospecto-table");
        });
    </script>
    {{ Html::script('modules/registroseguimiento/js/prospectoForm.js') }}
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="table-responsive">
                <a class="primary" href="#" onclick="cambiarEstado('');" title="Todos">
                    <i class="fas fa-filter fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('En_Proceso');" title="En proceso">
                    <i class="far fa-file-alt fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Propuesta');" title="Propuesta">
                    <i class="fas fa-file-invoice-dollar fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Seguimiento');" title="Seguimiento">
                    <i class="fas fa-business-time fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('No_Potencial');" title="No potencial">
                    <i class="fas fa-user-times fa-2x"></i>
                </a>
                <a class="primary" href="#" onclick="cambiarEstado('Descartado');" title="Descartado">
                    <i class="fas fa-times-circle fa-2x"></i>
                </a>
                <table id="prospecto-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/registroseguimiento/prospecto', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light"
                                    onclick="cargarProspectos()">
                                    <i class="fas fa-cloud"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($prospecto as $prospectoreg)
                            <tr style="background-color:{{ $prospectoreg->colorProspecto }}; color:black">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/registroseguimiento/prospecto', [$prospectoreg->oidProspecto, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-info btn-sm"
                                                href="prospectoasignacionempleado/{{ $prospectoreg->oidProspecto }}"
                                                target="_blank" title="Agregar Información">
                                                <i class="fas fa-users-cog"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-warning btn-sm"
                                                href="prospectoexitoso/{{ $prospectoreg->oidProspecto }}" target="_blank"
                                                title="Generar Cliente">
                                                <i class="fas fa-user-plus"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-danger"
                                                href="prospectorecordatorio/{{ $prospectoreg->oidProspecto }}"
                                                target="_blank" title="Recordatorio">
                                                <i class="fas fa-bell"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-dark btn-sm text-light"
                                                onclick="verObservacionesProspecto({{ $prospectoreg->oidProspecto }})"
                                                title="Buscar">
                                                <i class="fas fa-search"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-danger btn-sm text-light"
                                                onclick="confirmarEliminacion('{{ $prospectoreg->oidProspecto }}', 'rgs_prospecto', 'Prospecto')"
                                                title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $prospectoreg->oidProspecto }}</td>
                                <td>{{ $prospectoreg->txNombreProspecto }}</td>
                                <td>{{ $prospectoreg->inTelefonoProspecto }}</td>
                                <td>{{ $prospectoreg->tipoProspecto }}</td>
                                <td>{{ $prospectoreg->txEstadoProspecto }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            <th>Tipo</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
