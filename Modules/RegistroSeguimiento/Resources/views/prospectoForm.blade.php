@extends("layouts.principal")
@section("nombreModulo")
    Prospectos
@endsection
@section("scripts")
    {{Html::script("modules/registroseguimiento/js/prospectoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($prospecto->oidProspecto))
        {!!Form::model($prospecto,["route"=>["prospecto.update",$prospecto->oidProspecto],"method"=>"PUT", "id"=>"form-prospecto" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($prospecto,["route"=>["prospecto.store",$prospecto->oidProspecto],"method"=>"POST", "id"=>"form-prospecto", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidProspecto', null, array('id' => 'oidProspecto')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreProspecto', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreProspecto',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inTelefonoProspecto', 'Teléfono', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('inTelefonoProspecto',null,['class'=>'form-control','placeholder'=>'Ingresa Teléfono'])!!}
                    </div>
                </div>
            </div>

            @if(isset($prospecto->oidProspecto))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        