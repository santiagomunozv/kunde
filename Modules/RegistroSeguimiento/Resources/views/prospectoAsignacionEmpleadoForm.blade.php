@extends('layouts.principal')

@section('nombreModulo')
    En llamada
@endsection

@section('scripts')
    <script>
        let prospectoAsignacionEmpleado = '<?php echo json_encode($prospectoAsignacionEmpleado); ?>';
        let idResultado = '<?php echo json_encode($idResultado); ?>';
        let nombreResultado = '<?php echo json_encode($nombreResultado); ?>';

        let reunionAsistente = '<?php echo json_encode($reunionAsistente); ?>';
    </script>
    {{ Html::script('modules/registroseguimiento/js/prospectoAsignacionEmpleadoForm.js') }}
@endsection

@section('contenido')
    {{-- @dump($prospectoasignacionempleado) --}}
    {!! Form::model($prospecto, [
        'url' => ["registroseguimiento/prospectoasignacionempleado/$id"],
        'method' => 'PUT',
        'id' => 'form-prospectoasignacionempleado',
    ]) !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txNombreProspecto', 'Nombre', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::text('txNombreProspecto', null, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('inTelefonoProspecto', 'Teléfono', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::number('inTelefonoProspecto', null, ['class' => 'form-control', 'readonly']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('inIdentificacionProspecto', 'Identificación prospecto', [
                            'class' => 'text-md text-primary mb-1',
                        ]) !!}
                        {!! Form::text('inIdentificacionProspecto', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la identificación',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txNombreRepresentanteProspecto', 'Representante legal', [
                            'class' => 'text-md text-primary mb-1',
                        ]) !!}
                        {!! Form::text('txNombreRepresentanteProspecto', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa el nombre de representante legal',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('inIdentificacionRepresentanteProspecto', 'Identificación representante legal', [
                            'class' => 'text-md text-primary mb-1',
                        ]) !!}
                        {!! Form::text('inIdentificacionRepresentanteProspecto', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la identificación del representante legal',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Campana_oidCampana', 'Campaña', ['class' => 'text-md text-primary mb-1 ']) !!}
                        {!! Form::select(
                            'Campana_oidCampana',
                            $campana,
                            isset($prospecto[0]) ? $prospecto[0]['Campana_oidCampana'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona la campaña'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_oidEmpleado', 'Empleado', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select(
                            'Tercero_oidEmpleado',
                            $asn_tercerolistaEmpleado,
                            isset($prospecto[0]) ? $prospecto[0]['Tercero_oidEmpleado'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Empleado'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Departamento_oidDepartamento', 'Departamento', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::select(
                            'Departamento_oidDepartamento',
                            $departamento,
                            isset($prospecto[0]) ? $prospecto[0]['Departamento_oidDepartamento'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el departamento'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Ciudad_oidCiudad', 'Departamento', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::select('Ciudad_oidCiudad', $ciudad, isset($prospecto[0]) ? $prospecto[0]['Ciudad_oidCiudad'] : null, [
                            'class' => 'chosen-select form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Selecciona el departamento',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txNombreArlProspecto', 'ARL', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::text('txNombreArlProspecto', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el ARL']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('inCantidadEmpleadosProspecto', 'Cantidad de empleados', [
                            'class' => 'text-md text-primary mb-1 ',
                        ]) !!}
                        {!! Form::number('inCantidadEmpleadosProspecto', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la cantidad de empleados',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txAccidenteLaboralProspecto', '¿Ha tenido algún accidente laboral?', [
                            'class' => 'text-md text-primary mb-1',
                        ]) !!}
                        {!! Form::text('txAccidenteLaboralProspecto', null, ['class' => 'form-control', 'Escriba el accidente laboral']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('chSistemaProspecto', '¿Tiene sistema?', ['class' => 'label-responsive-dos']) !!}
                        {!! Form::checkbox('chSistemaProspecto', 1, null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txEstadoProspecto', 'Estado', ['class' => 'text-md text-primary mb-1 required']) !!}
                        {!! Form::select(
                            'txEstadoProspecto',
                            [
                                'En_Proceso' => 'En proceso',
                                'Propuesta' => 'Propuesta',
                                'Seguimiento' => 'Seguimiento',
                                'No_Potencial' => 'No potencial',
                                'Descartado' => 'Descartado',
                            ],
                            null,
                            ['class' => 'chosen-select form-control', 'placeholder' => 'Selecciona el estado del evento'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Reunión', 'Reunión', ['class' => 'text-md text-primary mb-1']) !!}
                        <a class="btn btn-primary btn-sm" data-toggle="collapse" href="#collapseReunion" role="button"
                            aria-expanded="false" aria-controls="collapseReunion">
                            <i class="fas fa-calendar"></i>
                        </a>
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2 collapse" id="collapseReunion">
                <div class="div card-body">
                    <div class="div card-body multi-max" style="overflow: auto">
                        {!! Form::hidden('oidReunion', isset($reunion) ? $reunion['oidReunion'] : null, ['id' => 'oidReunion']) !!}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('txAsuntoReunion', 'Asunto', ['class' => 'text-md text-primary mb-1']) !!}
                                    {!! Form::text('txAsuntoReunion', isset($reunion) ? $reunion['txAsuntoReunion'] : null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Ingresa el asunto',
                                        'readonly',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('txTipoReunion', 'Tipo de reunión', ['class' => 'text-md text-primary mb-1  required ']) !!}
                                    {!! Form::select(
                                        'txTipoReunion',
                                        ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                                        isset($reunion) ? $reunion['txTipoReunion'] : null,
                                        ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione el tipo de reunión'],
                                    ) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('dtFechaReunion', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::date('dtFechaReunion', isset($reunion) ? $reunion['dtFechaReunion'] : null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Ingresa la fecha de la reunión',
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('hrHoraReunion', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::time('hrHoraReunion', isset($reunion) ? $reunion['hrHoraReunion'] : null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Ingresa la hora de la reunión',
                                    ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('intDuracionReunion', 'Duración de la reunión', [
                                        'class' => 'text-md text-primary mb-1 required ',
                                    ]) !!}
                                    {!! Form::select(
                                        'intDuracionReunion',
                                        [
                                            '15' => '15 minutos',
                                            '30' => '30 minutos',
                                            '45' => '45 minutos',
                                            '60' => '1 hora',
                                            '75' => '1 hora y 15 minutos',
                                            '90' => '1 hora y 30 minutos',
                                            '105' => '1 hora y 45 minutos',
                                            '120' => '2 horas',
                                        ],
                                        isset($reunion) ? $reunion['intDuracionReunion'] : null,
                                        ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión'],
                                    ) !!}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('txUbicacionReunion', 'Ubicación', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::text('txUbicacionReunion', isset($reunion) ? $reunion['txUbicacionReunion'] : null, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Lugar / Link de la reunión',
                                    ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('Evento_oidEvento', 'Eventos', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::select(
                                        'Evento_oidEvento',
                                        $evento,
                                        isset($reunion[0]) ? $reunion[0]['Evento_oidEvento'] : (isset($reunion) ? $reunion['Evento_oidEvento'] : null),
                                        ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el evento'],
                                    ) !!}
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group ">
                                    {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::select(
                                        'Tercero_oidResponsable',
                                        $asn_tercerolistaEmpleado,
                                        isset($reunion[0])
                                            ? $reunion[0]['Tercero_oidTerceroEmpleado']
                                            : (isset($reunion)
                                                ? $reunion['Tercero_oidResponsable']
                                                : null),
                                        ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Responsable'],
                                    ) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    {!! Form::label('txDescripcionReunion', 'Descripción', ['class' => 'text-md text-primary mb-1']) !!}
                                    {!! Form::textArea(
                                        'txDescripcionReunion',
                                        isset($reunion[0])
                                            ? $reunion[0]['txDescripcionReunion']
                                            : (isset($reunion)
                                                ? $reunion['txDescripcionReunion']
                                                : null),
                                        ['class' => 'form-control', 'style' => 'width:100%', 'placeholder' => ''],
                                    ) !!}
                                </div>
                            </div>
                        </div>

                        <div class="div card border-left-primary shadow h-100 py-2">
                            <div class="div card-body">
                                <input type="hidden" id="eliminarreunion" name="eliminarreunion" value="">
                                Asistentes
                                <div class="div card-body multi-max">
                                    <table class="table multiregistro table-sm table-hover table-borderless">
                                        <thead class="bg-primary text-light">
                                            <th width="50px">
                                                <button type="button" class="btn btn-primary btn-sm text-light"
                                                    onclick="configuracionReunionAsistente.agregarCampos([],'L');">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                        <tbody id="contenedorReunionAsistente">

                                        </tbody>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarprospectoAsignacionEmpleado"
                        name="eliminarprospectoAsignacionEmpleado" value="">
                    <div class="div card-body multi-max" style="overflow: auto">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionprospectoAsignacionEmpleado.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Acción</th>
                                <th>Atiende</th>
                                <th>Encargado</th>
                                <th>Fecha seguimiento</th>
                                <th>Hora seguimiento</th>
                                <th>Observación</th>
                                <th>Resultado</th>
                            <tbody id="contenedorprospectoAsignacionEmpleados">
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}

            {!! Form::close() !!}
        </div>
    </div>
@endsection
