@extends('layouts.principal')
@section('nombreModulo')
    Servicios
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("servicio-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="servicio-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/registroseguimiento/servicio', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($servicio as $servicioreg)
                            <tr>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/registroseguimiento/servicio', [$servicioreg->oidServicio, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="cambiarEstado('{{ $servicioreg->oidServicio }}', 'rgs_servicio', 'Servicio')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $servicioreg->txNombreServicio }}</td>
                                <td>{{ $servicioreg->txDescripcionServicio }}</td>
                                <td>{{ $servicioreg->txEstadoServicio }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
