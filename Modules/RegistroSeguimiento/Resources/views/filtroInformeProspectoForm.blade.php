@extends('layouts.principal')

@section('nombreModulo')
    Filtro informe de prospectos
@endsection

@section('scripts')
    {{Html::script("modules/registroseguimiento/js/filtroInformeProspectoForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($filtroinformeprospecto) --}}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('prospecto', 'Prospecto', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::select('prospecto',$prospecto, null, ['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el prospecto'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('empleado', 'Empleado', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::select('empleado',$asn_tercerolistaEmpleado, null, ['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el empleado'])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('fechaInicioSeguimiento', 'Fecha inicial', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::date('fechaInicioSeguimiento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la fecha de inicio'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('fechaFinSeguimiento', 'Fecha final', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::date('fechaFinSeguimiento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la fecha final'])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('estado', 'Estado', array('class' => 'text-md text-primary mb-1 ')) !!}
                        {!!Form::select('estado',['En_Proceso' => 'En proceso', 'Propuesta' => 'Propuesta', 'Seguimiento' => 'Seguimiento', 'No_Potencial' => 'No potencial', 'Descartado' => 'Descartado'], null, ['class'=>'chosen-select form-control', 'placeholder' => 'Seleccione el estado'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('tipoInforme', 'Tipo de informe', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('tipoInforme', ['Prospecto' => 'Prospecto', 'Empleado' => 'Empleado'], null, ['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el tipo de informe'])!!}
                    </div>
                </div>
            </div>

            {!!Form::button("Generar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"generar()"])!!}
    </div>
</div>    
@endsection