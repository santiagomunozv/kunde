@extends('layouts.principal')

@section('nombreModulo')
    Registro de prospecto exitoso
@endsection

@section('scripts')

    {{Html::script("modules/registroseguimiento/js/prospectoExitosoForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($prospectoasignacionempleado) --}}
    {!! Form::model($prospectoExitoso, ['url' => ["registroseguimiento/prospectoexitoso/$id"], 'method' => 'PUT', 'id' => 'form-prospectoexitoso'])
    !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('TipoIdentificacion_oidTercero', 'Tipo de identificación', array('class' => 'text-md text-primary mb-1  required ')) !!}
                        {!!Form::select('TipoIdentificacion_oidTercero',$tipoIdentidicacion,(isset($prospectoExitoso[0]) ? $prospectoExitoso[0]["TipoIdentificacion_oidTercero"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el tipo de identificación'])!!}                   
                    </div>
                </div>
			
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txDocumentoTercero', 'Documento', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::number('txDocumentoTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingrese el número de documento'])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txMovilTercero', 'Número de celular', array('class' => 'text-md text-primary mb-1 required ')) !!}
                        {!!Form::number('txMovilTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el número de celular'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('Ciudad_oidTercero', 'Ciudad', array('class' => 'text-md text-primary mb-1  required ')) !!}
                        {!!Form::select('Ciudad_oidTercero',$ciudad,(isset($prospectoExitoso[0]) ? $prospectoExitoso[0]["Ciudad_oidTercero"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona la ciudad'])!!}                    
                    </div>
                </div>  
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txPrimerNombreTercero', 'Primer nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                        {!!Form::text('txPrimerNombreTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingrese el primer nombre'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txPrimerApellidoTercero', 'Primer apellido', array('class' => 'text-md text-primary mb-1 required ')) !!}
                        {!!Form::text('txPrimerApellidoTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingrese el primer apellido'])!!}
                    </div>
                </div>
            </div>    
              
            {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
        </div>
    </div>


@endsection
