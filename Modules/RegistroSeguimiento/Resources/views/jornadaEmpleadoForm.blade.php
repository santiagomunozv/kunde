@extends("layouts.principal")
@section("nombreModulo")
    Jornada de empleados
@endsection
@section("scripts")
    <script>
        let jornadaEmpleadoMensajes = '<?php echo json_encode($jornadaEmpleadoMensaje); ?>';
    </script>
    {{Html::script("modules/registroseguimiento/js/jornadaEmpleadoForm.js")}}

@endsection
@section("contenido")
    @if(isset($jornadaEmpleado->oidJornadaEmpleado))
        {!!Form::model($jornadaEmpleado,["route"=>["jornadaempleado.update",$jornadaEmpleado->oidJornadaEmpleado],"method"=>"PUT", "id"=>"form-jornadaEmpleado" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($jornadaEmpleado,["route"=>["jornadaempleado.store",$jornadaEmpleado->oidJornadaEmpleado],"method"=>"POST", "id"=>"form-jornadaEmpleado", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidJornadaEmpleado', null, array('id' => 'oidJornadaEmpleado')) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroEmpleado', 'Empleado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroEmpleado',$asn_tercerolistaEmpleado,(isset($jornadaEmpleado[0]) ? $jornadaEmpleado[0]["Tercero_oidTerceroEmpleado"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el Empleado'])!!}
                    </div>
                </div>
            </div>

			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inMetaLlamadasJornadaEmpleado', 'Meta de llamadas', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::number('inMetaLlamadasJornadaEmpleado',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txUnidadMedidaLlamadasJornadaEmpleado', 'Unidad de medida', array('class' => 'text-md text-primary mb-1  required ')) !!}
                        {!!Form::select('txUnidadMedidaLlamadasJornadaEmpleado',['Hora' => 'Hora','Día' => 'Día','Semana' => 'Semana','Quincena' => 'Quincena','Mes' => 'Mes'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona la unidad de medida'])!!}
                    </div>
                </div>
            </div>    
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inMetaCitasJornadaEmpleado', 'Meta de citas', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::number('inMetaCitasJornadaEmpleado',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txUnidadMedidaCitasJornadaEmpleado', 'Unidad de medida', array('class' => 'text-md text-primary mb-1 required ')) !!}
                        {!!Form::select('txUnidadMedidaCitasJornadaEmpleado',['Hora' => 'Hora','Día' => 'Día','Semana' => 'Semana','Quincena' => 'Quincena','Mes' => 'Mes'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona la unidad de medida'])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inHorasLaboralesJornadaEmpleado', 'Horas laborales', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::number('inHorasLaboralesJornadaEmpleado',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
            </div>
            

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarJornadaEmpleadoMensaje" name="eliminarJornadaEmpleadoMensaje" value="">
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionJornadaEmpleado.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Mensaje</th>
                                <th>Operador</th>
                                <th>Porcentaje</th>
                                <tbody id="contenedorJornadaEmpleado">
        
                                </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            @if(isset($jornadaEmpleado->oidJornadaEmpleado))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        