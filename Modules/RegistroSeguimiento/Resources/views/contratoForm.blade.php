@extends('layouts.principal')
@section('nombreModulo')
    Contrato
@endsection
@section('scripts')
    <script>
        var configuracionServicios = {!! $servicios !!};
    </script>
    {{ Html::script('modules/registroseguimiento/js/contratoForm.js') }}
@endsection
@section('contenido')
    {!! Form::open(['url' => ['registroseguimiento/contrato'], 'method' => 'POST', 'id' => 'form-contrato']) !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">

                <div class="col-sm-6">
                    {!! Form::label('Prospecto_oidProspecto', 'Prospecto', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('Prospecto_oidProspecto', $prospecto, null, [
                        'class' => 'chosen-select form-control',
                        'id' => 'Prospecto_oidProspecto',
                        'placeholder' => 'Selecciona un Prospecto',
                    ]) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('inNivelRiesgoContrato', 'Nivel de riesgo', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::number('inNivelRiesgoContrato', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el nivel de riesgo',
                    ]) !!}
                </div>

                <div class="col-sm-3">
                    <div class="form-group ">
                        {!! Form::label('lsModalidadContrato', 'Modalidad', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select('lsModalidadContrato', ['Mensual' => 'Mensual', 'Anual' => 'Anual'], null, [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Selecciona la modalidad del contrato',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-3">
                    {!! Form::label('inCantidadEmpleadosContrato', 'Cantidad de empleados', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::number('inCantidadEmpleadosContrato', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la cantidad de empleados',
                        'onBlur' => 'calcularValorServicio(this.value)',
                    ]) !!}
                </div>

                <div class="col-sm-3">
                    {!! Form::label('inCantidadCuotasContrato', 'Cantidad de cuotas', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::number('inCantidadCuotasContrato', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la cantidad de cuotas',
                    ]) !!}
                </div>

                <div class="col-sm-3">
                    {!! Form::label('dtFechaCuotaContrato', 'Fecha de cuota', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaCuotaContrato', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de la cuota',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                        </th>
                        <th>
                            <label class="custom-checkbox-container-title">
                                <input class="form-control" type="checkbox" id="option" onchange="checkALL(this)">
                                <span class="checkmark"></span>
                            </label>
                        </th>
                        <th>Servicio</th>
                        <th>Valor</th>

                    <tbody id="contenedorContrato"></tbody>
                    </thead>
                </table>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    {!! Form::button('Generar', [
        'type' => 'button',
        'class' => 'btn btn-primary',
        'onclick' => 'generarContrato()',
    ]) !!}
@endsection
