<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe prospecto</title>
    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        p.normal {
          font-style: normal;
        }
        
        #customers tr:nth-child(even){background-color: #f2f2f2;}
        
        #customers tr:hover {background-color: #ddd;}
        
        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #3A64AE;
          color: white;
        }

        .textCenter{
            text-align: center !important;
        }
        </style>
        <link rel="stylesheet" href="css/fontawesome.css">
</head>
<body>
    <table id="customers">
        <tr> 
            <th colspan="4" class="textCenter"> Informe prospecto registro seguimiento</th>
        </tr>
        @php
            $reg = 0;
        @endphp
        <tr>
            <th>Prospecto</th>
            <th>Llamadas realizadas</th>
            <th>Exitoso</th>
            <th>Fallido</th>
        </tr>
        @while ($reg < count($consulta))

            <tr>
                <td>{{$consulta[$reg]->txNombreProspecto}}</td>
                <td>{{$consulta[$reg]->llamadasRealizadas}}</td>
                <td>{{$consulta[$reg]->exitoso}}</td>
                <td>{{$consulta[$reg]->fallido}}</td>  
            </tr>
            @php
                $reg ++;
            @endphp
        @endwhile
    </table>
</body>
</html>