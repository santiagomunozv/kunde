@extends("layouts.principal")
@section('nombreModulo')
    Resultado de Seguimientos
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("resultados-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="div card border-left-primary shadow h-100 py-2">
        <div class="div card-body">
            <div class="div table-responsive">
                <table id="resultados-table" class="table table-hover table-sm scalia-grid">
                    
                </table>
            </div>
        </div>
    </div>
@endsection