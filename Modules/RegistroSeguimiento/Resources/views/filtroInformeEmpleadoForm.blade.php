@extends('layouts.principal')

@section('nombreModulo')
    Filtro informe de empleados
@endsection

@section('scripts')
<script>
    let filtroInformeEmpleado = '<?php echo json_encode($filtroInformeEmpleado); ?>';
</script>
    {{Html::script("modules/registroseguimiento/js/filtroInformeEmpleadoForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($filtroinformeempleado) --}}
    {!! Form::model($prospecto, ['url' => ["registroseguimiento/filtroinformeempleado/$id"], 'method' => 'PUT', 'id' => 'form-filtroinformeempleado'])
    !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidEmpleado', 'Empleado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidEmpleado',$asn_tercerolistaEmpleado,(isset($prospecto[0]) ? $prospecto[0]["Tercero_oidEmpleado"] : null),['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona el Empleado'])!!}
                    </div>
                </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('fechaInicioSeguimiento', 'Fecha', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::date('fechaInicioSeguimiento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la fecha de inicio'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('fechaFinSeguimiento', 'Fecha', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::date('fechaFinSeguimiento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la fecha final'])!!}
                    </div>
                </div>
            </div>
            {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}

        {!! Form::close() !!}
    </div>
</div>    
@endsection