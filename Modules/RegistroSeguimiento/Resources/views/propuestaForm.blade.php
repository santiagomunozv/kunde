@extends('layouts.principal')
@section('nombreModulo')
    Propuesta
@endsection
@section('scripts')
    <script>
        var configuracionServicios = {!! $servicios !!};
    </script>
    {{ Html::script('modules/registroseguimiento/js/propuestaForm.js') }}
@endsection
@section('contenido')
    {!! Form::open(['url' => ['registroseguimiento/propuesta'], 'method' => 'POST', 'id' => 'form-propuesta']) !!}

    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div class="row">

                <div class="col-sm-6">
                    {!! Form::label('Prospecto_oidProspecto', 'Prospecto', ['class' => 'text-sm text-primary mb-1 required']) !!}
                    {!! Form::select('Prospecto_oidProspecto', $prospecto, null, [
                        'class' => 'chosen-select form-control',
                        'id' => 'Prospecto_oidProspecto',
                        'placeholder' => 'Selecciona un Prospecto',
                    ]) !!}
                </div>

                <div class="col-sm-6">
                    {!! Form::label('dtFechaPropuesta', 'Fecha de propuesta', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaPropuesta', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de la propuesta',
                    ]) !!}
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('lsModalidadPropuesta', 'Modalidad', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select('lsModalidadPropuesta', ['Mensual' => 'Mensual', 'Anual' => 'Anual'], null, [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Selecciona la modalidad de la propuesta',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    {!! Form::label('inCantidadEmpleadosPropuesta', 'Cantidad de empleados', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::number('inCantidadEmpleadosPropuesta', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la cantidad de empleados',
                        'onBlur' => 'calcularValorServicio(this.value)',
                    ]) !!}
                </div>

                <div class="col-sm-12">
                    {!! Form::label('txObsequioPropuesta', 'Obsequio', [
                        'class' => 'text-md text-primary mb-1 ',
                    ]) !!}
                    {!! Form::text('txObsequioPropuesta', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el obsequio',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                        </th>
                        <th>
                            <label class="custom-checkbox-container-title">
                                <input class="form-control" type="checkbox" id="option" onchange="checkALL(this)">
                                <span class="checkmark"></span>
                            </label>
                        </th>
                        <th>Servicio</th>
                        <th>Descripción</th>
                        <th>Valor</th>

                    <tbody id="contenedorPropuesta"></tbody>
                    </thead>
                </table>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    {!! Form::button('Generar', [
        'type' => 'button',
        'class' => 'btn btn-primary',
        'onclick' => 'generarPropuesta()',
    ]) !!}
@endsection
