@extends('layouts.principal')

@section('nombreModulo')
    Resultado de Seguimientos
@endsection

@section('scripts')
<script>
    let resultados = '<?php echo json_encode($resultado); ?>';
</script>
    {{Html::script("modules/registroseguimiento/js/resultadosForm.js")}} 
@endsection

@section('contenido')
    {{-- @dump($resultado) --}}
    {!! Form::model($resultado, ['url' => ['registroseguimiento/resultados'], 'method' => 'PUT', 'id' => 'form-resultados'])
    !!}

    <div class="div card border-left-primary shadow h-100 py-2">
        <div class="div card-body">
            <input type="hidden" id="eliminarResultado" name="eliminarResultado" value="">
            <div class="div card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionResultado.agregarCampos([],'L');">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Nombre</th>
                        <th>Estado Empleado</th>
                        <th>Estado Cliente</th>
                        <tbody id="contenedorResultados">

                        </tbody>
                    </thead>
                </table>
                {!! Form::button('Modificar', ['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"]) !!}
            </div>
        </div>
    </div>

@endsection
