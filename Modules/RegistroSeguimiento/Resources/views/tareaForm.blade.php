@extends("layouts.principal")
@section("nombreModulo")
    Tareas
@endsection
@section("scripts")
    {{Html::script("modules/registroseguimiento/js/tareaForm.js")}}

    <script>
        var arrayAreas = "<?php echo (isset($tarea) ? ($tarea->txAreaTarea) : '');?>";  
        arrayAreas = arrayAreas.split(",");

        var arrayResponsables = "<?php echo (isset($tarea) ? ($tarea->txResponsableTarea) : '');?>";  
        arrayResponsables = arrayResponsables.split(",");
    </script>
    
@endsection
@section("contenido")
    @if(isset($tarea->oidTarea))
        {!!Form::model($tarea,["route"=>["tarea.update",$tarea->oidTarea],"method"=>"PUT", "id"=>"form-tarea" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($tarea,["route"=>["tarea.store",$tarea->oidTarea],"method"=>"POST", "id"=>"form-tarea", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">

            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidTarea', null, array('id' => 'oidTarea')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('dtFechaElaboracionTarea', 'Fecha de asignación', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::date('dtFechaElaboracionTarea',(isset($tarea->oidTarea) ? $tarea->dtFechaElaboracionTarea : date('Y-m-d')),[ 'class'=>'form-control','placeholder'=>'', 'readonly'])!!}
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txPrioridadTarea', 'Prioridad', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('txPrioridadTarea',['Alta' => 'Alta','Media' => 'Media', 'Baja' => 'Baja'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona la prioridad'])!!}
                    </div>
                </div>
            </div>
			
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txAsuntoTarea', 'Asunto', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txAsuntoTarea',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el asunto'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txAreaTarea', 'Área', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('idsArea', $area, null,['class'=>'chosen-select','multiple','style'=>'width:100%', 'id'=>'idsArea', 'onchange' => 'asignarIdsArea()'])!!}
                    {!!Form::hidden('txAreaTarea', (isset($tarea[0]) ? $tarea[0]['txAreaTarea'] : null), array('id' => 'txAreaTarea')) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txResponsableTarea', 'Responsable', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('idsResponsable', $asn_tercerolistaEmpleado, null,['class'=>'chosen-select','multiple','style'=>'width:100%', 'id'=>'idsResponsable', 'onchange' => 'asignarIdsResponsable()'])!!}
                    {!!Form::hidden('txResponsableTarea', (isset($tarea[0]) ? $tarea[0]['txResponsableTarea'] : null), array('id' => 'txResponsableTarea')) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('dtFechaVencimientoTarea', 'Fecha de vencimiento asignada', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::date('dtFechaVencimientoTarea',null,[ 'class'=>'form-control','placeholder'=>''])!!}
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txFechaSolucionTarea', 'Fecha de terminación real', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::date('txFechaSolucionTarea',null,[ 'class'=>'form-control','placeholder'=>'', 'readonly'])!!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoTarea', 'Estado', array('class' => 'text-md text-primary mb-1 required')) !!}
                    {!!Form::select('txEstadoTarea',['Nuevo' => 'Nuevo', 'Terminado' => 'Terminado', 'Rechazado' => 'Rechazado'],null,['class'=>'chosen-select form-control'])!!}                    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txDescripcionTarea', 'Descripción', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::textArea('txDescripcionTarea',(isset($tarea[0]) ? $tarea[0]["txDescripcionTarea"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>''])!!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txSolucionTarea', 'Solución', array('class' => 'text-md text-primary mb-1')) !!}
                    {!!Form::textArea('txSolucionTarea',(isset($tarea[0]) ? $tarea[0]["txSolucionTarea"] : null),['class'=>'form-control','style'=>'width:100%','placeholder'=>''])!!}
                    </div>
                </div>
            </div>

            <div id="imagenes">
                <input type="hidden" id="eliminarImagen" name="eliminarImagen" value="">
            </div>
            <div class="row">
                <div class="col-md-12 pb-2">
                    <div id="dropzoneImage" class="dropzone-div">
                        <div class="dz-message">
                            <div class="col-xs-8">
                                <div class="message">
                                    <p>Arrastra los elementos aqui o haz click para buscar el archivo.</p>
                                </div>
                            </div>
                        </div>
                        <div class="fallback">
                            <input type="file" name="file" multiple style="display: none;">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="max-height: 250px;display: flex;overflow: auto;">
                    @if (isset($tarea['adjunto']))
                        @foreach ($tarea['adjunto'] as $file)
                            <div class="imagenes m-1" id="imagen_{{ $file->oidAdjuntoTarea }}">
                                <span class="btn cerrar"
                                    onclick="eliminarImagen({{ $file->oidAdjuntoTarea }})">&times;</span>
                                <img src="/registroseguimiento/adjuntoadjunto/{{ $file->oidAdjuntoTarea }}"
                                    class="rounded" height="200">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            
            
            
            @if(isset($tarea->oidTarea))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        