@extends("layouts.principal")
@section('nombreModulo')
    Tareas Rapidas
@stop
@section('scripts')
    <script type="text/javascript">

    </script>
    {{-- {{ Html::script('modules/registroseguimiento/js/tareaForm.js') }} --}}
@endsection

{!! Form::open(['url' => 'registroseguimiento/tarearapida', 'method' => 'POST', 'id' => 'form-tareaRapida', 'files' => true]) !!}

@section('contenido')
<div class="card mb-4">
    <div class="card-body">
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
        <div id="errores"></div>

        <div class="custom-control custom-switch text-right">
            <input type="checkbox" class="custom-control-input" id="swhTarea"
                name="swhTarea">
            <label class="custom-control-label" for="swhTarea">Tarea avanzada</label>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txAsuntoTarea', 'Asunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txAsuntoTarea', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el asunto']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txResponsableTarea', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('txResponsableTarea', $listaEmpleados, null, ['class' => 'chosen-select form-control', 'placeholder' => 'Selecciona el estado de campaña']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('dtFechaVencimientoTarea', 'Fecha de vencimiento asignada', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaVencimientoTarea', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                </div>
            </div>

            <div class="col-sm-6" id="prioridadTarea" style="display: none;">
                <div class="form-group ">
                {!!Form::label('txPrioridadTarea', 'Prioridad', array('class' => 'text-md text-primary mb-1 required ')) !!}
                {!!Form::select('txPrioridadTarea',['Alta' => 'Alta','Media' => 'Media', 'Baja' => 'Baja'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona la prioridad', 'disabled'])!!}
                </div>
            </div>
        </div>

        <div class="row" id="descripcionTarea" style="display: none;">
            <div class="col-sm-12">
                <div class="form-group ">
                {!!Form::label('txDescripcionTarea', 'Descripción', array('class' => 'text-md text-primary mb-1')) !!}
                {!!Form::textArea('txDescripcionTarea',null,['class'=>'form-control','style'=>'width:100%','placeholder'=>'', 'readonly'])!!}
                </div>
            </div>
        </div>

    </div>
</div>

{!! Form::button('Guardar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabarModalTareaRapida()']) !!}

{!! Form::close() !!}

@endsection
