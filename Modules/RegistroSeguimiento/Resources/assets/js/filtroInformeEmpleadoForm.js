"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-filtroInformeEmpleado";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/registroseguimiento/filtroInformeEmpleado";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
		let Tercero_oidEmpleado_Input = $("#Tercero_oidEmpleado");
        let Tercero_oidEmpleado_AE = Tercero_oidEmpleado_Input.val();
        Tercero_oidEmpleado_Input.removeClass(errorClass);
        if(!Tercero_oidEmpleado_AE){
            Tercero_oidEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let fechaInicioSeguimiento_Input = $("#fechaInicioSeguimiento");
        let fechaInicioSeguimiento_AE = fechaInicioSeguimiento_Input.val();
        fechaInicioSeguimiento_Input.removeClass(errorClass);
        if(!fechaInicioSeguimiento_AE){
            fechaInicioSeguimiento_Input.addClass(errorClass)
            mensajes.push("El campo Teléfono es obligatorio");
        }
        let fechaFinSeguimiento_Input = $("#fechaFinSeguimiento");
        let fechaFinSeguimiento_AE = fechaFinSeguimiento_Input.val();
        fechaFinSeguimiento_Input.removeClass(errorClass);
        if(!fechaFinSeguimiento_AE){
            fechaFinSeguimiento_Input.addClass(errorClass)
            mensajes.push("El campo Teléfono es obligatorio");
        }
		return mensajes;
    }
}