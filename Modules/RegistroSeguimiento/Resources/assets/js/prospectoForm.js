"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-prospecto";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/registroseguimiento/prospecto";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
		let txNombreProspecto_Input = $("#txNombreProspecto");
        let txNombreProspecto_AE = txNombreProspecto_Input.val();
        txNombreProspecto_Input.removeClass(errorClass);
        if(!txNombreProspecto_AE){
            txNombreProspecto_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let inTelefonoProspecto_Input = $("#inTelefonoProspecto");
        let inTelefonoProspecto_AE = inTelefonoProspecto_Input.val();
        inTelefonoProspecto_Input.removeClass(errorClass);
        if(!inTelefonoProspecto_AE){
            inTelefonoProspecto_Input.addClass(errorClass)
            mensajes.push("El campo Teléfono es obligatorio");
        }
		return mensajes;
    }
}

function cargarProspectos(){
    modal.cargando()
        $.get('/registroseguimiento/importarprospectos', function (resp) {
            modal.mostrarModal('Importar prospectos', resp, function () {
                //esta es la accion del boton aceptar
                modal.cargando();
            }).extraGrande().sinPie();
            generarDropzone();
        }).fail(function (resp) {
            modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
        });
}

function verObservacionesProspecto(id){
    modal.cargando()
        $.get('/registroseguimiento/verObservacionesProspecto/'+id, function (resp) {
            modal.mostrarModal('Observaciones', resp, function () {
                //esta es la accion del boton aceptar
                modal.cargando();
            }).extraGrande().sinPie();
        }).fail(function (resp) {
            modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
        });
}

function importarProspectos(){
    let formularioId = "#form-importarprospectos";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    console.log(data);
    $.post(route,data, function( resp ){
        modal.establecerAccionCerrar(function(){
            location.href = "/registroseguimiento/prospecto";
        });
        modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
    },"json").fail( function(resp){
        modal.mostrarModal("Informacion" , "<div class=\"alert alert-danger\">"+resp.responseJSON.message+"</div>");
    });
}

function cambiarEstado(tipo = ''){
    location.href= 'http://'+location.host+"/registroseguimiento/prospecto?tipo="+tipo;
}

function generarDropzone(){
    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    var myDropzone = new Dropzone("div#dropzoneImage", {
        headers: { 'X-CSRF-TOKEN': token},
        url: "/registroseguimiento/cargarArchivoProspecto",
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".xls, .xlsx",
        init: function(){
            this.on("success",function(file){
                let pathImage = JSON.parse(file.xhr.response);
                $("#imagenes").append("<input type='hidden' name='imagenTercero' value='"+pathImage.tmpPath+"'>");
                $("#nombreArchivo").val(file.name);
            });
            this.on("removedfile",function(file){
                let pathImage = JSON.parse(file.xhr.response);
                $('input[name="imagenTercero"][value="'+pathImage.tmpPath+'"]').remove();
            });
        }
    });
}