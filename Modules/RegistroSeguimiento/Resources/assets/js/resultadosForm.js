"use strict";
var errorClass = "is-invalid";
var configuracionResultados = JSON.parse(resultados);
var configuracionResultado = [];

$(function(){
    //generamos la multiregistro de grupo
    configuracionResultado = new GeneradorMultiRegistro('configuracionResultado','contenedorResultados','configuracionResultado');

    let options=[['Exitoso','No aceptaron', 'Sin respuesta', 'Errado'],['Exitoso', 'No aceptaron', 'Sin respuesta', 'Errado']]

    configuracionResultado.campoid = 'oidResultado';
    configuracionResultado.campoEliminacion = 'eliminarResultado';
    configuracionResultado.botonEliminacion = true;
    configuracionResultado.funcionEliminacion = '';  
    configuracionResultado.campos = ['oidResultado','txNombreResultado','lsEstadoEmpleadoResultado','chEstadoClienteResultado'];
    configuracionResultado.etiqueta = ['input','input','select', 'checkbox'];
    configuracionResultado.tipo = ['hidden','text', '', 'checkbox'];
    configuracionResultado.estilo = ['','','',''];
    configuracionResultado.clase = ['','','', ''];
    configuracionResultado.sololectura = [true,false,false,false];
    configuracionResultado.opciones = ['','', options,''];
    configuracionResultado.funciones = ['','','', ''];
    configuracionResultado.otrosAtributos = ['','','', ''];


    configuracionResultados.forEach( dato => {configuracionResultado.agregarCampos(dato , 'L');
    });
});


function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-resultados";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
               location.href = "/registroseguimiento/resultados";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}