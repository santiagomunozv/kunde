"use strict";
var errorClass = "is-invalid";

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  if (arrayAreas.length > 0) {
    $("#idsArea").val(arrayAreas).trigger("chosen:updated");
  }

  if (arrayResponsables.length > 0) {
    $("#idsResponsable").val(arrayResponsables).trigger("chosen:updated");
  }
  generarDropzone()
});

function asignarIdsArea() {
  $("#txAreaTarea").val($("#idsArea").val());
}

function asignarIdsResponsable() {
  $("#txResponsableTarea").val($("#idsResponsable").val());
}

function grabar() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-tarea";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "/registroseguimiento/tarea";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {
    let mensajes = [];
    let txPrioridadTarea_Input = $("#txPrioridadTarea");
    let txPrioridadTarea_AE = txPrioridadTarea_Input.val();
    txPrioridadTarea_Input.removeClass(errorClass);
    if (!txPrioridadTarea_AE) {
      txPrioridadTarea_Input.addClass(errorClass);
      mensajes.push("El campo Prioridad es obligatorio");
    }
    let txAsuntoTarea_Input = $("#txAsuntoTarea");
    let txAsuntoTarea_AE = txAsuntoTarea_Input.val();
    txAsuntoTarea_Input.removeClass(errorClass);
    if (!txAsuntoTarea_AE) {
      txAsuntoTarea_Input.addClass(errorClass);
      mensajes.push("El campo Asunto es obligatorio");
    }
    let txAreaTarea_Input = $("#txAreaTarea");
    let txAreaTarea_AE = txAreaTarea_Input.val();
    txAreaTarea_Input.removeClass(errorClass);
    if (!txAreaTarea_AE) {
      txAreaTarea_Input.addClass(errorClass);
      mensajes.push("El campo Área es obligatorio");
    }
    let txResponsableTarea_Input = $("#txResponsableTarea");
    let txResponsableTarea_AE = txResponsableTarea_Input.val();
    txResponsableTarea_Input.removeClass(errorClass);
    if (!txResponsableTarea_AE) {
      txResponsableTarea_Input.addClass(errorClass);
      mensajes.push("El campo Responsable es obligatorio");
    }
    let dtFechaVencimientoTarea_Input = $("#dtFechaVencimientoTarea");
    let dtFechaVencimientoTarea_AE = dtFechaVencimientoTarea_Input.val();
    dtFechaVencimientoTarea_Input.removeClass(errorClass);
    if (!dtFechaVencimientoTarea_AE) {
      dtFechaVencimientoTarea_Input.addClass(errorClass);
      mensajes.push("El campo Fecha de vencimiento asignada es obligatorio");
    }
    let txEstadoTarea_Input = $("#txEstadoTarea");
    let txEstadoTarea_AE = txEstadoTarea_Input.val();
    txEstadoTarea_Input.removeClass(errorClass);
    if (!txEstadoTarea_AE) {
      txEstadoTarea_Input.addClass(errorClass);
      mensajes.push("El campo Estado es obligatorio");
    }

    return mensajes;
  }
}

function cambiarEstado(tipo = "") {
  location.href =
    "http://" + location.host + "/registroseguimiento/tarea?tipo=" + tipo;
}

// método encargado de cambiar el etstado de la tarea
function cambiarEstadoTarea(idTarea) {
  modal.cargando();
  let token = $('meta[name="csrf-token"]').attr("content");
  $.ajax({
    headers: { "X-CSRF-TOKEN": token },
    dataType: "json",
    url: "/registroseguimiento/tarea/cambiarestado/" + idTarea,
    type: "GET",
    success: function (resp) {
      modal.mostrarModal(
        "Informacion",
        '<div class="alert alert-success">Se cambio el estado de sistema de gestión</div>',
        function () {
          location.reload(true);
        }
      );
    },
    error: function (resp) {
      modal.mostrarModal(
        "Error",
        "se produjo un error al cambiar el estado",
        function () {
          location.reload(true);
        }
      );
    },
  });
}

// método encargado de mostrar modal del EstadoAprobador
function modalEstadoAprobador(idTarea) {
  modal.cargando();
  $.get(
    "/registroseguimiento/tarea/modalestoaprobador/" + idTarea,
    function (resp) {
      modal
        .mostrarModal("Aprobar tarea", resp, function () {
          modal.cargando();
        })
        .sinPie();
    }
  ).fail(function (resp) {
    modal.cerrarModal();
  });
}

function cambiarEstadoAprobador(estado) {
  $('#estado').val(estado) 
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarModal();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {

    let formularioId = "#form-estadoaprobador";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    console.log(data);
    $.post(
      route,
      data,
      function (resp) {
        console.log(resp);
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los cambios han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });

  }
}

function validarModal() {
  let mensajes = [];

  // let observacionAprobador_Input = $("#observacionAprobador");
  // let observacionAprobador_AE = observacionAprobador_Input.val();
  // observacionAprobador_Input.removeClass(errorClass);
  // if (!observacionAprobador_AE) {
  //   observacionAprobador_Input.addClass(errorClass);
  //   mensajes.push("El campo motivo es obligatorio");
  // }

  return mensajes;
}

//método encargado de mostrar errores en el mismo modal
function mostrarErrores(errores) {
  let ul = document.createElement("ul");
  errores.forEach(function (error) {
    let li = document.createElement("li");
    li.innerHTML = error;
    ul.appendChild(li);
  });
  let div = document.createElement("div");
  div.appendChild(ul);
  div.className = "alert alert-warning";
  document.getElementById("errores").appendChild(div);
}

function generarDropzone() {
  let token = $('meta[name="csrf-token"]').attr("content");
  var myDropzone = new Dropzone("div#dropzoneImage", {
      headers: { "X-CSRF-TOKEN": token},
      // method: 'POST',
      url: "/registroseguimiento/adjunto",
      addRemoveLinks: true,
      maxFiles: 5,
      acceptedFiles: "image/*",
      init: function () {
          this.on("success", function (file) {
              let pathImage = JSON.parse(file.xhr.response);
              console.log(pathImage.tmpPath);
              $("#imagenes").append(
                  "<input type='hidden' name='imagenAdjunto[]' value='" +
                  pathImage.tmpPath +
                  "'>"
              );
          });
          this.on("removedfile", function (file) {
              let pathImage = JSON.parse(file.xhr.response);
              $(
                  'input[name="imagenAdjunto[]"][value="' + pathImage.tmpPath + '"]'
              ).remove();
          });
      },
  });
}