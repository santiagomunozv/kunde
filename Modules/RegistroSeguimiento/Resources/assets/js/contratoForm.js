"use strict";
var errorClass = "is-invalid";
var configuracionContrato = [];


$(function () {
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

    configuracionContrato = new GeneradorMultiRegistro('configuracionContrato', 'contenedorContrato', 'configuracionContrato_');

    configuracionContrato.botonEliminacion = false;
    configuracionContrato.funcionEliminacion = '';
    configuracionContrato.campos = ['chGenerarServicio', 'txNombreServicio', 'inValorServicio'];
    configuracionContrato.etiqueta = ['checkbox', 'input', 'input'];
    configuracionContrato.tipo = ['checkbox', 'text', 'number'];
    configuracionContrato.clase = ['', '', ''];
    configuracionContrato.sololectura = [false, true, false];
    configuracionContrato.opciones = ['', '', ''];
    configuracionContrato.funciones = ['', '', ''];
    configuracionContrato.otrosAtributos = ['', '', ''];

    configuracionServicios.forEach(dato => {
        configuracionContrato.agregarCampos(dato, 'L');
    });
})

function calcularValorServicio(empleados) {
    const modalidad = document.getElementById('lsModalidadContrato').value;

    let valorMensual = 0;
    let valorAnual = 0;
    if (empleados >= 1 && empleados <= 6) {
        valorMensual = 475000;
        valorAnual = 5130000;
    } else if (empleados > 6 && empleados <= 20) {
        valorMensual = 625000;
        valorAnual = 6750000;
    } else if (empleados > 20 && empleados <= 50) {
        valorMensual = 1080000;
        valorAnual = 11644000;
    } else {
        return alert('La cantidad de empleados ingresada no se encuentra configurada');
    }

    const valor = modalidad === 'Mensual' ? valorMensual : valorAnual;

    for (let i = 0; i < configuracionContrato.contador; i++) {
        $("#inValorServicio" + i).val(valor * 0.10 + valor);
    }
}

function checkALL(mycheckbox) {
    var checkboxes = document.querySelectorAll("#contenedorContrato input[type='checkbox']");

    checkboxes.forEach(function (checkbox, index) {
        if (checkbox !== mycheckbox) {
            checkbox.checked = mycheckbox.checked;
            var idReg = "chGenerarServicio" + index;
            document.getElementById(idReg).value = mycheckbox.checked;
        }
    });
}

function generarContrato() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-contrato";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                var baseUrl = window.location.origin;
                var url = baseUrl + '/' + resp.message;
                window.open(url, '_blank');
                location.reload();
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {

        let mensajes = [];
        let Prospecto_oidProspecto_Input = $("#Prospecto_oidProspecto");
        let Prospecto_oidProspecto_AE = Prospecto_oidProspecto_Input.val();
        Prospecto_oidProspecto_Input.removeClass(errorClass);
        if (!Prospecto_oidProspecto_AE) {
            Prospecto_oidProspecto_Input.addClass(errorClass)
            mensajes.push("El campo Prospecto es obligatorio");
        }
        let inNivelRiesgoContrato_Input = $("#inNivelRiesgoContrato");
        let inNivelRiesgoContrato_AE = inNivelRiesgoContrato_Input.val();
        inNivelRiesgoContrato_Input.removeClass(errorClass);
        if (!inNivelRiesgoContrato_AE) {
            inNivelRiesgoContrato_Input.addClass(errorClass)
            mensajes.push("El campo Nivel de riesgo es obligatorio");
        }

        let lsModalidadContrato_Input = $("#lsModalidadContrato");
        let lsModalidadContrato_AE = lsModalidadContrato_Input.val();
        lsModalidadContrato_Input.removeClass(errorClass);
        if (!lsModalidadContrato_AE) {
            lsModalidadContrato_Input.addClass(errorClass)
            mensajes.push("El campo Modalidad es obligatorio");
        }

        let inCantidadEmpleadosContrato_Input = $("#inCantidadEmpleadosContrato");
        let inCantidadEmpleadosContrato_AE = inCantidadEmpleadosContrato_Input.val();
        inCantidadEmpleadosContrato_Input.removeClass(errorClass);
        if (!inCantidadEmpleadosContrato_AE) {
            inCantidadEmpleadosContrato_Input.addClass(errorClass)
            mensajes.push("El campo Cantidad de empleados es obligatorio");
        }

        let inCantidadCuotasContrato_Input = $("#inCantidadCuotasContrato");
        let inCantidadCuotasContrato_AE = inCantidadCuotasContrato_Input.val();
        inCantidadCuotasContrato_Input.removeClass(errorClass);
        if (!inCantidadCuotasContrato_AE) {
            inCantidadCuotasContrato_Input.addClass(errorClass)
            mensajes.push("El campo Cantidad de cuotas es obligatorio");
        }

        let dtFechaCuotaContrato_Input = $("#dtFechaCuotaContrato");
        let dtFechaCuotaContrato_AE = dtFechaCuotaContrato_Input.val();
        dtFechaCuotaContrato_Input.removeClass(errorClass);
        if (!dtFechaCuotaContrato_AE) {
            dtFechaCuotaContrato_Input.addClass(errorClass)
            mensajes.push("El campo Fecha cuota es obligatorio");
        }

        return mensajes;
    }
}