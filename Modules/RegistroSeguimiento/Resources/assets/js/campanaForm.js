"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-campana";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/registroseguimiento/campana";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
		let txCodigoCampana_Input = $("#txCodigoCampana");
        let txCodigoCampana_AE = txCodigoCampana_Input.val();
        txCodigoCampana_Input.removeClass(errorClass);
        if(!txCodigoCampana_AE){
            txCodigoCampana_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreCampana_Input = $("#txNombreCampana");
        let txNombreCampana_AE = txNombreCampana_Input.val();
        txNombreCampana_Input.removeClass(errorClass);
        if(!txNombreCampana_AE){
            txNombreCampana_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
        let txEstadoCampana_Input = $("#txEstadoCampana");
        let txEstadoCampana_AE = txEstadoCampana_Input.val();
        txEstadoCampana_Input.removeClass(errorClass);
        if(!txEstadoCampana_AE){
            txEstadoCampana_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
        
		return mensajes;
    }
}