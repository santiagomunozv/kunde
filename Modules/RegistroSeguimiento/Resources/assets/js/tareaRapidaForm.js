//"use strict";
//var errorClass = "is-invalid";
var check = false;

$(document).on("change", ".custom-control", function (e) {
  check = e.target.checked;

  if (check) {
    $("#prioridadTarea").css("display", "block");
    $("#descripcionTarea").css("display", "block");
  } else {
    $("#prioridadTarea").css("display", "none");
    $("#descripcionTarea").css("display", "none");
  }

  $("#txPrioridadTarea").prop("disabled", !check);
  $("#txDescripcionTarea").prop("readonly", !check);

  return check;
});

function grabarModalTareaRapida() {
  var errorClass = "is-invalid";
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-tareaRapida";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }

  function validarForm() {
    let mensajes = [];
    let txAsuntoTarea_Input = $("#txAsuntoTarea");
    let txAsuntoTarea_AE = txAsuntoTarea_Input.val();
    txAsuntoTarea_Input.removeClass(errorClass);
    if (!txAsuntoTarea_AE) {
      txAsuntoTarea_Input.addClass(errorClass);
      mensajes.push("El campo Asunto es obligatorio");
    }

    let txResponsableTarea_Input = $("#txResponsableTarea");
    let txResponsableTarea_AE = txResponsableTarea_Input.val();
    txResponsableTarea_Input.removeClass(errorClass);
    if (!txResponsableTarea_AE) {
      txResponsableTarea_Input.addClass(errorClass);
      mensajes.push("El campo Responsable es obligatorio");
    }

    let dtFechaVencimientoTarea_Input = $("#dtFechaVencimientoTarea");
    let dtFechaVencimientoTarea_AE = dtFechaVencimientoTarea_Input.val();
    dtFechaVencimientoTarea_Input.removeClass(errorClass);
    if (!dtFechaVencimientoTarea_AE) {
      dtFechaVencimientoTarea_Input.addClass(errorClass);
      mensajes.push("El campo Fecha de vencimiento asignada es obligatorio");
    }

    if (check) {
      let txPrioridadTarea_Input = $("#txPrioridadTarea");
      let txPrioridadTarea_AE = txPrioridadTarea_Input.val();
      txPrioridadTarea_Input.removeClass(errorClass);
      if (
        !txPrioridadTarea_AE &&
        txPrioridadTarea_Input.length > 0
      ) {
        txPrioridadTarea_Input.addClass(errorClass);
        mensajes.push("El campo Prioridad es obligatorio");
      }

      let txDescripcionTarea_Input = $("#txDescripcionTarea");
      let txDescripcionTarea_AE = txDescripcionTarea_Input.val();
      txDescripcionTarea_Input.removeClass(errorClass);
      if (!txDescripcionTarea_AE && txDescripcionTarea_Input.length > 0) {
        txDescripcionTarea_Input.addClass(errorClass);
        mensajes.push("El campo Descripción es obligatorio");
      }
    }
    return mensajes;
  }
}

//método encargado de mostrar errores en el mismo modal
function mostrarErrores(errores) {
  let ul = document.createElement("ul");
  errores.forEach(function (error) {
    let li = document.createElement("li");
    li.innerHTML = error;
    ul.appendChild(li);
  });
  let div = document.createElement("div");
  div.appendChild(ul);
  div.className = "alert alert-warning";
  document.getElementById("errores").appendChild(div);
}
