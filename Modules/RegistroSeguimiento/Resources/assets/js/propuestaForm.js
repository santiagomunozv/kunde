"use strict";
var errorClass = "is-invalid";
var configuracionPropuesta = [];


$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" }
  }

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  configuracionPropuesta = new GeneradorMultiRegistro('configuracionPropuesta', 'contenedorPropuesta', 'configuracionPropuesta_');

  configuracionPropuesta.botonEliminacion = false;
  configuracionPropuesta.funcionEliminacion = '';
  configuracionPropuesta.campos = ['chGenerarServicio', 'txNombreServicio', 'txDescripcionServicio', 'inValorServicio'];
  configuracionPropuesta.etiqueta = ['checkbox', 'input', 'input', 'input'];
  configuracionPropuesta.tipo = ['checkbox', 'text', 'text', 'number'];
  configuracionPropuesta.clase = ['', '', '', ''];
  configuracionPropuesta.sololectura = [false, true, true, false];
  configuracionPropuesta.opciones = ['', '', '', ''];
  configuracionPropuesta.funciones = ['', '', '', ''];
  configuracionPropuesta.otrosAtributos = ['', '', '', ''];

  configuracionServicios.forEach(dato => {
    configuracionPropuesta.agregarCampos(dato, 'L');
  });
})

function calcularValorServicio(empleados) {
  const modalidad = document.getElementById('lsModalidadPropuesta').value;

  let valorMensual = 0;
  let valorAnual = 0;
  if (empleados >= 1 && empleados <= 6) {
    valorMensual = 475000;
    valorAnual = 5130000;
  } else if (empleados > 6 && empleados <= 20) {
    valorMensual = 625000;
    valorAnual = 6750000;
  } else if (empleados > 20 && empleados <= 50) {
    valorMensual = 1080000;
    valorAnual = 11644000;
  } else {
    return alert('La cantidad de empleados ingresada no se encuentra configurada');
  }

  const valor = modalidad === 'Mensual' ? valorMensual : valorAnual;

  for (let i = 0; i < configuracionPropuesta.contador; i++) {
    $("#inValorServicio" + i).val(valor * 0.10 + valor);
  }
}

function checkALL(mycheckbox) {
  var checkboxes = document.querySelectorAll("#contenedorPropuesta input[type='checkbox']");

  checkboxes.forEach(function (checkbox, index) {
    if (checkbox !== mycheckbox) {
      checkbox.checked = mycheckbox.checked;
      var idReg = "chGenerarServicio" + index;
      document.getElementById(idReg).value = mycheckbox.checked;
    }
  });
}

function generarPropuesta() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-propuesta";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(route, data, function (resp) {
      modal.establecerAccionCerrar(function () {
        var baseUrl = window.location.origin;
        var url = baseUrl + '/' + resp.message;
        window.open(url, '_blank');
        location.reload();
      });
      modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
    }, "json").fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {

    let mensajes = [];
    let Prospecto_oidProspecto_Input = $("#Prospecto_oidProspecto");
    let Prospecto_oidProspecto_AE = Prospecto_oidProspecto_Input.val();
    Prospecto_oidProspecto_Input.removeClass(errorClass);
    if (!Prospecto_oidProspecto_AE) {
      Prospecto_oidProspecto_Input.addClass(errorClass)
      mensajes.push("El campo Prospecto es obligatorio");
    }

    let dtFechaPropuesta_Input = $("#dtFechaPropuesta");
    let dtFechaPropuesta_AE = dtFechaPropuesta_Input.val();
    dtFechaPropuesta_Input.removeClass(errorClass);
    if (!dtFechaPropuesta_AE) {
      dtFechaPropuesta_Input.addClass(errorClass)
      mensajes.push("El campo Fecha cuota es obligatorio");
    }

    let lsModalidadPropuesta_Input = $("#lsModalidadPropuesta");
    let lsModalidadPropuesta_AE = lsModalidadPropuesta_Input.val();
    lsModalidadPropuesta_Input.removeClass(errorClass);
    if (!lsModalidadPropuesta_AE) {
      lsModalidadPropuesta_Input.addClass(errorClass)
      mensajes.push("El campo Modalidad es obligatorio");
    }

    let inCantidadEmpleadosPropuesta_Input = $("#inCantidadEmpleadosPropuesta");
    let inCantidadEmpleadosPropuesta_AE = inCantidadEmpleadosPropuesta_Input.val();
    inCantidadEmpleadosPropuesta_Input.removeClass(errorClass);
    if (!inCantidadEmpleadosPropuesta_AE) {
      inCantidadEmpleadosPropuesta_Input.addClass(errorClass)
      mensajes.push("El campo Cantidad de empleados es obligatorio");
    }

    return mensajes;
  }
}