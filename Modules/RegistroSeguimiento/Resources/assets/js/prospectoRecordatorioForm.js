"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-prospectorecordatorio";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let dtFechaSeguimientoProspectoSeguimiento_Input = $("#dtFechaSeguimientoProspectoSeguimiento");
        let dtFechaSeguimientoProspectoSeguimiento_AE = dtFechaSeguimientoProspectoSeguimiento_Input.val();
        dtFechaSeguimientoProspectoSeguimiento_Input.removeClass(errorClass);
        if(!dtFechaSeguimientoProspectoSeguimiento_AE){
            dtFechaSeguimientoProspectoSeguimiento_Input.addClass(errorClass)
            mensajes.push("El campo Fecha es obligatorio");
        }
		let hrHoraSeguimientoProspectoSeguimiento_Input = $("#hrHoraSeguimientoProspectoSeguimiento");
        let hrHoraSeguimientoProspectoSeguimiento_AE = hrHoraSeguimientoProspectoSeguimiento_Input.val();
        hrHoraSeguimientoProspectoSeguimiento_Input.removeClass(errorClass);
        if(!hrHoraSeguimientoProspectoSeguimiento_AE){
            hrHoraSeguimientoProspectoSeguimiento_Input.addClass(errorClass)
            mensajes.push("El campo Hora es obligatorio");
        }
		return mensajes;
    }
}