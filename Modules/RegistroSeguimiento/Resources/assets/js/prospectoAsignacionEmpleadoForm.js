"use strict";
var errorClass = "is-invalid";
var configuracionprospectoAsignacionEmpleados = JSON.parse(prospectoAsignacionEmpleado);
var configuracionprospectoAsignacionEmpleado = [];

var configuracionReunionAsistentes = JSON.parse(reunionAsistente);
var configuracionReunionAsistente = [];

$(function(){
    //generamos la multiregistro de grupo
    configuracionprospectoAsignacionEmpleado = new GeneradorMultiRegistro('configuracionprospectoAsignacionEmpleado','contenedorprospectoAsignacionEmpleados','configuracionprospectoAsignacionEmpleado');

    let options=[JSON.parse(idResultado),JSON.parse(nombreResultado)]

    configuracionprospectoAsignacionEmpleado.campoid = 'oidProspectoSeguimiento';
    configuracionprospectoAsignacionEmpleado.campoEliminacion = 'eliminarprospectoAsignacionEmpleado';
    configuracionprospectoAsignacionEmpleado.botonEliminacion = true;
    configuracionprospectoAsignacionEmpleado.funcionEliminacion = '';  
    configuracionprospectoAsignacionEmpleado.campos = ['oidProspectoSeguimiento','txAccionProspectoSeguimiento','txAtiendeProspectoSeguimiento','txEncargadoProspectoSeguimiento', 'dtFechaSeguimientoProspectoSeguimiento','hrHoraSeguimientoProspectoSeguimiento','txObservacionSeguimiento', 'Resultado_oidResultado'];
    configuracionprospectoAsignacionEmpleado.etiqueta = ['input', 'input', 'input','input', 'input','input','input', 'select'];
    configuracionprospectoAsignacionEmpleado.tipo = ['hidden','number','text','text', 'date','time','text', ''];
    configuracionprospectoAsignacionEmpleado.estilo = ['','', '','','','','', ''];
    configuracionprospectoAsignacionEmpleado.clase = ['','', '','','','','', ''];
    configuracionprospectoAsignacionEmpleado.sololectura = [true,false,false,false,false,false,false, false];
    configuracionprospectoAsignacionEmpleado.opciones = ['','','' ,'','','','', options];
    configuracionprospectoAsignacionEmpleado.funciones = ['','','','','','','',''];
    configuracionprospectoAsignacionEmpleado.otrosAtributos = ['','','','','','','',''];


    configuracionprospectoAsignacionEmpleados.forEach( dato => {configuracionprospectoAsignacionEmpleado.agregarCampos(dato , 'L');
    });




    configuracionReunionAsistente = new GeneradorMultiRegistro('configuracionReunionAsistente','contenedorReunionAsistente','configuracionReunionAsistente');

    configuracionReunionAsistente.campoid = 'oidReunionAsistente';
    configuracionReunionAsistente.campoEliminacion = 'eliminarReunionAsistente';
    configuracionReunionAsistente.botonEliminacion = true;
    configuracionReunionAsistente.funcionEliminacion = '';  
    configuracionReunionAsistente.campos = ['oidReunionAsistente','txNombreReunionAsistente','txCorreoAsistente'];
    configuracionReunionAsistente.etiqueta = ['input','input', 'input'];
    configuracionReunionAsistente.tipo = ['hidden','text','text'];
    configuracionReunionAsistente.estilo = ['','',''];
    configuracionReunionAsistente.clase = ['','',''];
    configuracionReunionAsistente.sololectura = [true,false,false];
    configuracionReunionAsistente.opciones = ['','',''];
    configuracionReunionAsistente.funciones = ['','',''];
    configuracionReunionAsistente.otrosAtributos = ['','',''];


    configuracionReunionAsistentes.forEach( dato => {configuracionReunionAsistente.agregarCampos(dato , 'L');
    });
});


function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-prospectoasignacionempleado";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
               window.close();
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}