"use strict";
var errorClass = "is-invalid";
var configuracionJornadaEmpleados = JSON.parse(jornadaEmpleadoMensajes);
var configuracionJornadaEmpleado = [];

$(function(){
    //generamos la multiregistro de grupo
    configuracionJornadaEmpleado = new GeneradorMultiRegistro('configuracionJornadaEmpleado','contenedorJornadaEmpleado','configuracionJornadaEmpleado');

    let options=[['Mayor', 'Mayor que', 'Igual', 'Menor', 'Menor que'],['Mayor', 'Mayor que', 'Igual', 'Menor', 'Menor que']]

    configuracionJornadaEmpleado.campoid = 'oidJornadaEmpleadoMensaje';
    configuracionJornadaEmpleado.campoEliminacion = 'eliminarJornadaEmpleadoMensaje';
    configuracionJornadaEmpleado.botonEliminacion = true;
    configuracionJornadaEmpleado.funcionEliminacion = '';  
    configuracionJornadaEmpleado.campos = ['oidJornadaEmpleadoMensaje','txMensajeJornadaEmpleadoMensaje','lsOperadorJornadaEmpleadoMensaje','inPorcentajeJornadaEmpleadoMensaje'];
    configuracionJornadaEmpleado.etiqueta = ['input','input','select', 'input'];
    configuracionJornadaEmpleado.tipo = ['hidden','text', '', 'number'];
    configuracionJornadaEmpleado.estilo = ['','','',''];
    configuracionJornadaEmpleado.clase = ['','','', ''];
    configuracionJornadaEmpleado.sololectura = [true,false,false,false];
    configuracionJornadaEmpleado.opciones = ['','', options,''];
    configuracionJornadaEmpleado.funciones = ['','','', ''];
    configuracionJornadaEmpleado.otrosAtributos = ['','','', ''];


    configuracionJornadaEmpleados.forEach( dato => {configuracionJornadaEmpleado.agregarCampos(dato , 'L');
    });

    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-jornadaEmpleado";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/registroseguimiento/jornadaempleado";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
    
    function validarForm(){
        
        let mensajes =[];
        let Tercero_oidTerceroEmpleado_Input = $("#Tercero_oidTerceroEmpleado");
        let Tercero_oidTerceroEmpleado_AE = Tercero_oidTerceroEmpleado_Input.val();
        Tercero_oidTerceroEmpleado_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroEmpleado_AE){
            Tercero_oidTerceroEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Empleado es obligatorio");
        }
		let inMetaLlamadasJornadaEmpleado_Input = $("#inMetaLlamadasJornadaEmpleado");
        let inMetaLlamadasJornadaEmpleado_AE = inMetaLlamadasJornadaEmpleado_Input.val();
        inMetaLlamadasJornadaEmpleado_Input.removeClass(errorClass);
        if(!inMetaLlamadasJornadaEmpleado_AE){
            inMetaLlamadasJornadaEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Meta de llamadas es obligatorio");
        }
		let txUnidadMedidaLlamadasJornadaEmpleado_Input = $("#txUnidadMedidaLlamadasJornadaEmpleado");
        let txUnidadMedidaLlamadasJornadaEmpleado_AE = txUnidadMedidaLlamadasJornadaEmpleado_Input.val();
        txUnidadMedidaLlamadasJornadaEmpleado_Input.removeClass(errorClass);
        if(!txUnidadMedidaLlamadasJornadaEmpleado_AE){
            txUnidadMedidaLlamadasJornadaEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Unidad de medida es obligatorio");
        }
		let inMetaCitasJornadaEmpleado_Input = $("#inMetaCitasJornadaEmpleado");
        let inMetaCitasJornadaEmpleado_AE = inMetaCitasJornadaEmpleado_Input.val();
        inMetaCitasJornadaEmpleado_Input.removeClass(errorClass);
        if(!inMetaCitasJornadaEmpleado_AE){
            inMetaCitasJornadaEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Meta de citas es obligatorio");
        }
        let txUnidadMedidaCitasJornadaEmpleado_Input = $("#txUnidadMedidaCitasJornadaEmpleado");
        let txUnidadMedidaCitasJornadaEmpleado_AE = txUnidadMedidaCitasJornadaEmpleado_Input.val();
        txUnidadMedidaCitasJornadaEmpleado_Input.removeClass(errorClass);
        if(!txUnidadMedidaCitasJornadaEmpleado_AE){
            txUnidadMedidaCitasJornadaEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Unidad de medida es obligatorio");
        }
        let inHorasLaboralesJornadaEmpleado_Input = $("#inHorasLaboralesJornadaEmpleado");
        let inHorasLaboralesJornadaEmpleado_AE = inHorasLaboralesJornadaEmpleado_Input.val();
        inHorasLaboralesJornadaEmpleado_Input.removeClass(errorClass);
        if(!inHorasLaboralesJornadaEmpleado_AE){
            inHorasLaboralesJornadaEmpleado_Input.addClass(errorClass)
            mensajes.push("El campo Horas laborales es obligatorio");
        }
		return mensajes;
    }
}
