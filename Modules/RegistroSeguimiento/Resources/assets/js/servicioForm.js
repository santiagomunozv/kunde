"use strict";
var errorClass = "is-invalid";


$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" }
  }

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }


})

function grabar() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-servicio";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(route, data, function (resp) {
      modal.establecerAccionCerrar(function () {
        location.href = "/registroseguimiento/servicio";
      });
      modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
    }, "json").fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {

    let mensajes = [];

    let txNombreServicio_Input = $("#txNombreServicio");
    let txNombreServicio_AE = txNombreServicio_Input.val();
    txNombreServicio_Input.removeClass(errorClass);
    if (!txNombreServicio_AE) {
      txNombreServicio_Input.addClass(errorClass)
      mensajes.push("El campo Nombre es obligatorio");
    }

    let txDescripcionServicio_Input = $("#txDescripcionServicio");
    let txDescripcionServicio_AE = txDescripcionServicio_Input.val();
    txDescripcionServicio_Input.removeClass(errorClass);
    if (!txDescripcionServicio_AE) {
      txDescripcionServicio_Input.addClass(errorClass)
      mensajes.push("El campo Descripción es obligatorio");
    }

    let txEstadoServicio_Input = $("#txEstadoServicio");
    let txEstadoServicio_AE = txEstadoServicio_Input.val();
    txEstadoServicio_Input.removeClass(errorClass);
    if (!txEstadoServicio_AE) {
      txEstadoServicio_Input.addClass(errorClass)
      mensajes.push("El campo Estado es obligatorio");
    }

    return mensajes;
  }
}