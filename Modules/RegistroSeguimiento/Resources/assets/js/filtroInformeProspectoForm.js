$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
})

function generar(){
    prospecto = $("#prospecto").val();
    empleado = $("#empleado").val();
    fechaInicio = $("#fechaInicioSeguimiento").val();
    fechaFin = $("#fechaFinSeguimiento").val();
    estado = $("#estado").val();
    tipoInforme = $("#tipoInforme").val();

    if(tipoInforme == ''){
        alert('Debe seleccionar un tipo de informe');
        return;
    }

    if (fechaInicio != '' && fechaFin == '' || fechaInicio == '' && fechaFin != ''){
        alert('Ambas fechas deben estar llenas');
        return;
    }

    condicion = '';
    
    if (prospecto != '')
        condicion = condicion + 'oidProspecto = "'+prospecto+'"';

    if (empleado != '') 
        condicion = condicion + ((condicion !='' && empleado != '') ? ' and ' : '') + 'oidTercero = "'+empleado+'"';

    if (estado != '') 
        condicion = condicion + ((condicion !='' && estado != '') ? ' and ' : '') + 'txEstadoProspecto = "'+estado+'"';

    if (fechaInicio != '' && fechaFin != '')
        condicion = condicion + ((condicion !='' && fechaInicio !='') ? ' and ' : '') + 'dtFechaSeguimientoProspectoSeguimiento >= "'+fechaInicio+'" and dtFechaSeguimientoProspectoSeguimiento <= "'+fechaFin+'"';

    console.log(condicion);

    if(tipoInforme == 'Prospecto'){
        window.open('informeprospectoregistroseguimiento?condicion='+condicion,'_blank','width=2500px, height=700px, scrollbars=yes');
    }else if( tipoInforme == 'Empleado'){
        window.open('informeempleadoregistroseguimiento?condicion='+condicion,'_blank','width=2500px, height=700px, scrollbars=yes');
    }

}