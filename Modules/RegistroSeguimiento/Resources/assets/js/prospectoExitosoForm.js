"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-prospectoexitoso";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                window.close();
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let TipoIdentificacion_oidTercero_Input = $("#TipoIdentificacion_oidTercero");
        let TipoIdentificacion_oidTercero_AE = TipoIdentificacion_oidTercero_Input.val();
        TipoIdentificacion_oidTercero_Input.removeClass(errorClass);
        if(!TipoIdentificacion_oidTercero_AE){
            TipoIdentificacion_oidTercero_Input.addClass(errorClass)
            mensajes.push("El campo Tipo de Identificación es obligatorio");
        }
		let txDocumentoTercero_Input = $("#txDocumentoTercero");
        let txDocumentoTercero_AE = txDocumentoTercero_Input.val();
        txDocumentoTercero_Input.removeClass(errorClass);
        if(!txDocumentoTercero_AE){
            txDocumentoTercero_Input.addClass(errorClass)
            mensajes.push("El campo Documento es obligatorio");
        }
        let txMovilTercero_Input = $("#txMovilTercero");
        let txMovilTercero_AE = txMovilTercero_Input.val();
        txMovilTercero_Input.removeClass(errorClass);
        if(!txMovilTercero_AE){
            txMovilTercero_Input.addClass(errorClass)
            mensajes.push("El campo Número de celular es obligatorio");
        }
        let txPrimerNombreTercero_Input = $("#txPrimerNombreTercero");
        let txPrimerNombreTercero_AE = txPrimerNombreTercero_Input.val();
        txPrimerNombreTercero_Input.removeClass(errorClass);
        if(!txPrimerNombreTercero_AE){
            txPrimerNombreTercero_Input.addClass(errorClass)
            mensajes.push("El campo Primer Nombre es obligatorio");
        }
        let txPrimerApellidoTercero_Input = $("#txPrimerApellidoTercero");
        let txPrimerApellidoTercero_AE = txPrimerApellidoTercero_Input.val();
        txPrimerApellidoTercero_Input.removeClass(errorClass);
        if(!txPrimerApellidoTercero_AE){
            txPrimerApellidoTercero_Input.addClass(errorClass)
            mensajes.push("El campo Primer Apellido es obligatorio");
        }
        let Ciudad_oidTercero_Input = $("#Ciudad_oidTercero");
        let Ciudad_oidTercero_AE = Ciudad_oidTercero_Input.val();
        Ciudad_oidTercero_Input.removeClass(errorClass);
        if(!Ciudad_oidTercero_AE){
            Ciudad_oidTercero_Input.addClass(errorClass)
            mensajes.push("El campo Ciudad es obligatorio");
        }
		return mensajes;
    }
}