<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddTableRgstareas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_area', function (Blueprint $table) {
            $table->increments('oidArea');
            $table->string('txCodigoArea')->comment('Código');
            $table->string('txNombreArea')->comment('Nombre');
            $table->string('txEstadoArea')->comment('Nombre');
        });

        Schema::create('rgs_tarea', function (Blueprint $table) {
            $table->increments('oidTarea');
            $table->date('dtFechaElaboracionTarea')->comment('Elaboración');
            $table->string('txAsuntoTarea')->comment('Asunto');
            $table->string('txPrioridadTarea')->comment('Prioridad');
            $table->string('txAreaTarea')->comment('Area');
            $table->string('txResponsableTarea')->comment('Responsable');
            $table->string('txEstadoTarea')->comment('Estado');
            $table->date('dtFechaVencimientoTarea')->comment('Vencimiento');
            $table->date('txFechaSolucionTarea')->nullable()->comment('Solución');
            $table->text('txDescripcionTarea')->nullable()->comment('Descripción');
            $table->text('txSolucionTarea')->nullable()->comment('Solución');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>22,
            "txNombreOpcion" =>"Área",
            "txRutaOpcion" => "/gestionhumana/area"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>20,
            "txNombreOpcion" =>"Tareas",
            "txRutaOpcion" => "/registroseguimiento/tarea"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_area');
        Schema::dropIfExists('rgs_tarea');
    }
}
