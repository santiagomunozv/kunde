<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRgsAdjuntoTareaTable extends Migration
{

    public function up()
    {
        Schema::create('rgs_adjunto_tarea', function (Blueprint $table) {
            $table->increments('oidAdjuntoTarea');
            $table->string('txNombreArvchivo')->nullable();
            $table->string('txRutaArchivo')->nullable();
            $table->string('txTipoArchivo')->nullable();
            $table->string('txDescripcionArchivo')->nullable();
            $table->unsignedInteger('Tarea_oidTarea')->nullable();

            $table->foreign('Tarea_oidTarea')->references('oidTarea')->on('rgs_tarea')->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('rgs_adjunto_tarea');
    }
}
