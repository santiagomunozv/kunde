<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRgsServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_servicio', function (Blueprint $table) {
            $table->string('txDescripcionServicio', 200)->comment('Descripción servicio')->nullable(true);

            $table->dropColumn('txDescripcionServicioSST');
            $table->dropColumn('txDescripcionServicioRiesgoPsicosocial');
            $table->dropColumn('txDescripcionServicioAuditoria');
            $table->dropColumn('inValorServicio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_servicio', function (Blueprint $table) {
            $table->dropColumn('txDescripcionServicio');
            $table->string('txDescripcionServicioSST')->comment('Descripción servicio SST')->nullable(true);
            $table->string('txNombreServicio')->comment('Nombre servicio')->nullable(true);
            $table->string('txDescripcionServicioRiesgoPsicosocial')->comment('Descripción servicio riesgo psicosocial')->nullable(true);
            $table->string('txDescripcionServicioAuditoria')->comment('Descripción servicio auditoria')->nullable(true);
            $table->decimal('inValorServicio', 18, 2)->comment('Valor servicio')->nullable(true);
        });
    }
}
