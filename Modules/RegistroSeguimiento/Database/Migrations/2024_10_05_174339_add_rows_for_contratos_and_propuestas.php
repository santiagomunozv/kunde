<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddRowsForContratosAndPropuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->string('inIdentificacionProspecto')->nullable(true)->comment('Identificación del prospecto');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" => 20,
            "txNombreOpcion" => "Contratos",
            "txRutaOpcion" => "/registroseguimiento/contrato"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('inIdentificacionProspecto');
        });
    }
}
