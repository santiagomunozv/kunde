<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateOptionResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>20,
            "txNombreOpcion" =>"Resultados",
            "txRutaOpcion" => "/registroseguimiento/resultados"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Option__resultados');
    }
}
