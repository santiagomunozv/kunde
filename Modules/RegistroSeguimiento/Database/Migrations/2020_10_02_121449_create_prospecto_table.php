<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_prospecto', function (Blueprint $table) {
            $table->bigIncrements('oidProspecto');
            $table->string('txNombreProspecto', 80)->comment("Nombre del prospecto");
            $table->string('inTelefonoProspecto', 20)->comment("Teléfono del prospecto");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_prospecto');
    }
}
