<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsProspecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->integer('Tercero_oidArl')->nullable()->comment('Id Tercero ARL');;
            $table->integer('inCantidadEmpleadosProspecto')->nullable()->comment('Cantidad de empleados');
            $table->integer('chSistemaProspecto')->nullable()->comment('Tiene sistema');
            $table->string('txAccidenteLaboralProspecto')->nullable()->comment('Ha tenido accidente laboral');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('Tercero_oidArl');
            $table->dropColumn('inCantidadEmpleadosProspecto');
            $table->dropColumn('chSistemaProspecto');
            $table->dropColumn('txAccidenteLaboralProspecto');
        });
    }
}
