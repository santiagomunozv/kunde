<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRgsResultadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_resultados', function (Blueprint $table) {
            $table->increments('oidResultado');
            $table->string('txNombreResultado', 80)->comment("Nombre del resultado");
            $table->string('lsEstadoEmpleadoResultado', 20)->comment("Estado del empleado");
            $table->string('chEstadoClienteResultado', 20)->comment("Estado del cliente");

            $table->timestamps(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_resultados');
    }
}
