<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTerceroempleadoProspecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->integer('Tercero_oidEmpleado')->comment('Id tercero empleado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('Tercero_oidEmpleado');
        });
    }
}
