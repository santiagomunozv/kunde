<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionsInformeProspecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>20,
            "txNombreOpcion" =>"Informe empleado",
            "txRutaOpcion" => "/registroseguimiento/informeempleadoregistroseguimiento"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>20,
            "txNombreOpcion" =>"Informe prospecto",
            "txRutaOpcion" => "/registroseguimiento/informeprospectoregistroseguimiento"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
