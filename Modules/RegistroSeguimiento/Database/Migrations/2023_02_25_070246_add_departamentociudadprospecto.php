<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepartamentociudadprospecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->integer('Departamento_oidDepartamento')->nullable();
            $table->integer('Ciudad_oidCiudad')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('Departamento_oidDepartamento');
            $table->dropColumn('Ciudad_oidCiudad');
        });
    }
}
