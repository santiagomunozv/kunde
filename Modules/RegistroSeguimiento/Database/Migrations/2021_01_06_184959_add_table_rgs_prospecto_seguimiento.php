<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableRgsProspectoSeguimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_prospectoseguimiento', function (Blueprint $table) {
            $table->increments('oidProspectoSeguimiento');
            $table->string('txAccionProspectoSeguimiento')->nullable()->comment('Acción');
            $table->string('txAtiendeProspectoSeguimiento')->nullable()->comment('Atiende');
            $table->string('txEncargadoProspectoSeguimiento')->nullable()->comment('Encargado');
            $table->date('dtFechaPrevistaProspectoSeguimiento')->comment('Fecha prevista');
            $table->string('hrHoraPrevistaProspectoSeguimiento')->comment('Hora prevista');
            $table->date('dtFechaSeguimientoProspectoSeguimiento')->nullable()->comment('Fecha seguimiento');
            $table->string('hrHoraSeguimientoProspectoSeguimiento')->nullable()->comment('Hora seguimiento');
            $table->text('txObservacionSeguimiento')->comment('Observación');
            $table->integer('Prospecto_oidProspecto')->comment('Id prospecto');
            $table->integer('Resultado_oidResultado')->nullable()->comment('Id resultado');
            $table->integer('Tercero_oidEmpleado')->comment('Id tercero empleado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_prospectoseguimiento');
    }
}
