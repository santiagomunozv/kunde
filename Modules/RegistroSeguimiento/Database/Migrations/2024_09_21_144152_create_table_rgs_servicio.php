<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableRgsServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" => 20,
            "txNombreOpcion" => "Servicios",
            "txRutaOpcion" => "/registroseguimiento/servicio"
        ]);

        Schema::create('rgs_servicio', function (Blueprint $table) {
            $table->increments('oidServicio');
            $table->string('txNombreServicio')->comment('Nombre servicio');
            $table->string('txDescripcionServicioSST')->comment('Descripción servicio SST');
            $table->string('txDescripcionServicioRiesgoPsicosocial')->comment('Descripción servicio riesgo psicosocial');
            $table->string('txDescripcionServicioAuditoria')->comment('Descripción servicio auditoria');
            $table->decimal('inValorServicio', 18, 2)->comment('Valor servicio');
            $table->string('txEstadoServicio')->comment('Estado servicio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_servicio');
    }
}
