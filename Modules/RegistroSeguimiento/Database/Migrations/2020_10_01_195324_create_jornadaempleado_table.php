<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJornadaempleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_jornadaempleado', function (Blueprint $table) {
            $table->increments('oidJornadaEmpleado');
            $table->integer('inMetaLlamadasJornadaEmpleado')->comment('Meta de llamadas');
            $table->string('txUnidadMedidaLlamadasJornadaEmpleado')->comment('Unidad de medida llamadas');
            $table->integer('inMetaCitasJornadaEmpleado')->comment('Meta de citas');
            $table->string('txUnidadMedidaCitasJornadaEmpleado')->comment('Unidad de medida citas');
            $table->decimal('inHorasLaboralesJornadaEmpleado', 18, 2)->comment('Horas laborales');
            $table->integer('Tercero_oidTerceroEmpleado')->comment('id de tercero');
            //Hacer migracion para la foreign key
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_jornadaempleado');
    }
}
