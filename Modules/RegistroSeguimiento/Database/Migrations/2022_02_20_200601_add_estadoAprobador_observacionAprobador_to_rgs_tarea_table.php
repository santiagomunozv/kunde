<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadoAprobadorObservacionAprobadorToRgsTareaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_tarea', function (Blueprint $table) {
            $table->string('estadoAprobador')->default('Pendiente');
            $table->string('observacionAprobador');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_tarea', function (Blueprint $table) {
            $table->dropColumn('estadoAprobador');
            $table->dropColumn('observacionAprobador');
        });
    }
}
