<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProspecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->string('txNombreRepresentanteProspecto')->nullable(true)->comment('Nombre del representante del prospecto');
            $table->string('inIdentificacionRepresentanteProspecto')->nullable(true)->comment('Identificación del representante del prospecto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('txNombreRepresentanteProspecto');
            $table->dropColumn('inIdentificacionRepresentanteProspecto');
        });
    }
}
