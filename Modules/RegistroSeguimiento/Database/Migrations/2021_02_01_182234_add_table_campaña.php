<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddTableCampaña extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_campana', function (Blueprint $table) {
            $table->increments('oidCampana');
            $table->string('txCodigoCampana')->comment('Código de Campana');
            $table->string('txNombreCampana')->comment('Nombre de Campana');
            $table->string('txEstadoCampana')->comment('Estado de Campana');
            $table->timestamps();
        });

        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->integer('Campana_oidCampana')->comment('Id campana');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>20,
            "txNombreOpcion" =>"Campaña",
            "txRutaOpcion" => "/registroseguimiento/campana"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_campana');

        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('Campana_oidCampana');
        });
    }
}
