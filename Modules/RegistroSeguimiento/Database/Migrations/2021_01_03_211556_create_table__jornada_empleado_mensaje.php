<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJornadaEmpleadoMensaje extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rgs_jornadaempleadomensaje', function (Blueprint $table) {
            $table->increments('oidJornadaEmpleadoMensaje');
            $table->string('txMensajeJornadaEmpleadoMensaje')->comment('Mensaje');
            $table->string('lsOperadorJornadaEmpleadoMensaje')->comment('Operador');
            $table->integer('inPorcentajeJornadaEmpleadoMensaje')->comment('Porcentaje');
            $table->unsignedInteger('JornadaEmpleado_oidJornadaEmpleado')->comment('Id jornada empleado');
            $table->foreign('JornadaEmpleado_oidJornadaEmpleado', 'FK_JornadaEmpleado')->references('oidJornadaEmpleado')->on('rgs_jornadaempleado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rgs_jornadaempleadomensaje');
    }
}
