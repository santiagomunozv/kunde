<?php

namespace Modules\RegistroSeguimiento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CampanaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            "txCodigoCampana" => "required",
            "txNombreCampana" => "required|string",
            "txEstadoCampana" => "required",
        );


        return $validacion;
    }



    public function messages()
    {
        $mensaje = array();
        $mensaje["txNombreCampana.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txNombreCampana.required"] =  "El campo Nombre es obligatorio";
        $mensaje["txCodigoCampana.required"] =  "El campo Código es obligatorio";
        $mensaje["txEstadoCampana.unique"] =  "El campo Estado es obligatorio";


        return $mensaje;
    }
}
