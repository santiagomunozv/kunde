<?php

    namespace Modules\RegistroSeguimiento\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ProspectoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "txNombreProspecto" => "required|string",
            "inTelefonoProspecto" => "required",Rule::unique('rgs_prospecto')->ignore($this->oidProspecto),
        );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["txNombreProspecto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
                $mensaje["txNombreProspecto.required"] =  "El campo Nombre es obligatorio";
                $mensaje["inTelefonoProspecto.required"] =  "El campo Teléfono es obligatorio";
                $mensaje["inTelefonoProspecto.unique"] =  "El valor ingresado en el campo Teléfono ya existe, éste debe ser único";
        
            
            return $mensaje;
        }
    }
        