<?php

    namespace Modules\RegistroSeguimiento\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ProspectoRecordatorioRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "hrHoraSeguimientoProspectoSeguimiento" => "required",
            "dtFechaSeguimientoProspectoSeguimiento" => "required",
        );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["hrHoraSeguimientoProspectoSeguimiento.required"] =  "El campo Hora es obligatorio";
                $mensaje["dtFechaSeguimientoProspectoSeguimiento.required"] =  "El campo Fecha es obligatorio";
            
            return $mensaje;
        }
    }
        