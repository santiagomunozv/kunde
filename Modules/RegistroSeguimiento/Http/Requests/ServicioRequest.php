<?php

namespace Modules\RegistroSeguimiento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicioRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $validacion = array(
      "txNombreServicio" => "required",
      "txDescripcionServicio" => "required|string",
      "txEstadoServicio" => "required",
    );

    return $validacion;
  }



  public function messages()
  {
    $mensaje = array();
    $mensaje["txNombreServicio.required"] =  "El campo Nombre es obligatorio";
    $mensaje["txDescripcionServicio.required"] =  "El campo Descripción es obligatorio";
    $mensaje["txEstadoServicio.unique"] =  "El campo Estado es obligatorio";


    return $mensaje;
  }
}
