<?php

    namespace Modules\RegistroSeguimiento\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ProspectoExitosoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "TipoIdentificacion_oidTercero" => "required",
            "txDocumentoTercero" => "required|numeric",
            "txMovilTercero" => "required|numeric",
            "txPrimerNombreTercero" => "required",
            "txPrimerApellidoTercero" => "required",
            "Ciudad_oidTercero" => "required",
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["TipoIdentificacion_oidTercero.required"] = "El campo Tipo Identificación es obligatorio";
                $mensaje["txDocumentoTercero.required"] =  "El campo Documento es obligatorio";
                $mensaje["txDocumentoTercero.numeric"] = "El campo Documento Sólo puede contener caracteres númericos";
                $mensaje["txMovilTercero.numeric"] =  "El campo Número de Celular Sólo puede contener caracteres númericos";
                $mensaje["txPrimerNombreTercero.required"] =  "El campo Primer Nombre es obligatorio";
                $mensaje["txPrimerApellidoTercero.required"] =  "El campo Primer Apellido es obligatorio";
                $mensaje["Ciudad_oidTercero.required"] =  "El campo Ciudad es obligatorio";
        
            return $mensaje;
        }
    }
        