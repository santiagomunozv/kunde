<?php

    namespace Modules\RegistroSeguimiento\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ResultadoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
           // Personal
        $validacion = array(
            'txNombreResultado.*' => 'required',
            'lsEstadoEmpleadoResultado.*' => 'required',
          );
    
          return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
       
            $total = ($this->get("oidResultado") !== null ) ? count($this->get("oidResultado")) : 0;
            for($j=0; $j < $total; $j++)
            {
                 $mensaje['txNombreResultado.'.$j.'.required'] = "El Nombre es obligatorio en el registro ".($j+1);
                 $mensaje['lsEstadoEmpleadoResultado.'.$j.'.required'] = "El Estado Empleado es obligatorio en el registro ".($j+1);
            }
  
            return $mensaje;
        }
    }
        