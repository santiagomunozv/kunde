<?php

namespace Modules\RegistroSeguimiento\Http\Requests\Tarea;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            "txPrioridadTarea" => "required",
            "txAsuntoTarea" => "required",
            "txAreaTarea" => "required",
            "txResponsableTarea" => "required",
            "dtFechaVencimientoTarea" => "required",
            "txEstadoTarea" => "required",
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        $mensaje = array();
            $mensaje["txPrioridadTarea.required"] = "El campo Prioridad es obligatorio";
            $mensaje["txAsuntoTarea.required"] =  "El campo Asunto es obligatorio";
            $mensaje["txAreaTarea.required"] =  "El campo Área es obligatorio";
            $mensaje["txResponsableTarea.required"] =  "El campo Responsable es obligatorio";
            $mensaje["dtFechaVencimientoTarea.required"] =  "El campo Fecha de vencimiento asignada es obligatorio";
            $mensaje["txEstadoTarea.required"] =  "El campo Estado es obligatorio";

        return $mensaje;
    }
}
