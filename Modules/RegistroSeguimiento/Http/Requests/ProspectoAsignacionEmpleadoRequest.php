<?php

namespace Modules\RegistroSeguimiento\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProspectoAsignacionEmpleadoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            'txAccionProspectoSeguimiento.*' => 'required',
            "Campana_oidCampana" => "nullable",
            "Departamento_oidDepartamento" => "nullable",
            "Ciudad_oidCiudad" => "nullable",
            'Tercero_oidEmpleado' => 'required',
            'txEstadoProspecto' => 'required',
            'dtFechaSeguimientoProspectoSeguimiento.*' => 'required',
            'hrHoraSeguimientoProspectoSeguimiento.*' => 'required',
            'Resultado_oidResultado.*' => 'required',
        );

        if ($this->get('txTipoReunion') != '') {
            $validacion['txTipoReunion'] = "required";
            $validacion['dtFechaReunion'] = "required|date";
            $validacion['hrHoraReunion'] = "required";
            $validacion['intDuracionReunion'] = "required";
            $validacion['txUbicacionReunion'] = "required|string";
            $validacion['Tercero_oidResponsable'] = "required";
            $validacion['Evento_oidEvento'] = "required";
            $validacion['txNombreReunionAsistente.*'] = "required";
            $validacion['txCorreoAsistente.*'] = "required";
        }
        return $validacion;
    }


    public function messages()
    {
        $mensaje = array();
        $mensaje["Campana_oidCampana.required"] =  "El campo Campaña es obligatorio";
        $mensaje["Departamento_oidDepartamento.required"] =  "El campo Departamento es obligatorio";
        $mensaje["Ciudad_oidCiudad.required"] =  "El campo Ciudad es obligatorio";
        $mensaje["Tercero_oidEmpleado.required"] =  "El campo Empleado es obligatorio";
        $mensaje["txEstadoProspecto.required"] =  "El campo Estado es obligatorio";
        $mensaje["txTipoReunion.required"] = "El campo Tipo de reunión es obligatorio";
        $mensaje["dtFechaReunion.required"] =  "El campo Fecha es obligatorio";
        $mensaje["dtFechaReunion.date"] =  "El campo Fecha debe ser en formato fecha";
        $mensaje["hrHoraReunion.required"] = "El campo Hora es obligatorio";
        $mensaje["intDuracionReunion.required"] = "El campo Duración de la reunión es obligatorio";
        $mensaje["txUbicacionReunion.required"] =  "El campo Ubicación es obligatorio";
        $mensaje["txUbicacionReunion.string"] = "El campo Ubicación sólo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["Tercero_oidResponsable.required"] = "El campo Responsable es obligatorio";
        $mensaje["Evento_oidEvento.required"] = "El campo Eventos es obligatorio";

        $total = ($this->get("oidProspectoSeguimiento") !== null) ? count($this->get("oidProspectoSeguimiento")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['txAccionProspectoSeguimiento.' . $j . '.required'] = "El campo Acción es obligatorio en el registro " . ($j + 1);
            $mensaje['dtFechaSeguimientoProspectoSeguimiento.' . $j . '.required'] = "El campo Fecha Seguimiento es obligatorio en el registro " . ($j + 1);
            $mensaje['hrHoraSeguimientoProspectoSeguimiento.' . $j . '.required'] = "El campo Hora Seguimiento es obligatorio en el registro " . ($j + 1);
            $mensaje['Resultado_oidResultado.' . $j . '.required'] = "El campo Resultado es obligatorio en el registro " . ($j + 1);
        }

        $total = ($this->get("oidReunionAsistente") !== null) ? count($this->get("oidReunionAsistente")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['txNombreReunionAsistente.' . $j . '.required'] = "El Nombre es obligatorio en el registro " . ($j + 1);
            $mensaje['txCorreoAsistente.' . $j . '.required'] = "El Correo es obligatorio en el registro " . ($j + 1);
        }

        return $mensaje;
    }
}
