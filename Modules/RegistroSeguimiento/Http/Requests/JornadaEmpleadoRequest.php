<?php

        namespace Modules\RegistroSeguimiento\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class JornadaEmpleadoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "inMetaLlamadasJornadaEmpleado" => "required",
                "txUnidadMedidaLlamadasJornadaEmpleado" => "required",
                "inMetaCitasJornadaEmpleado" => "required",
                "txUnidadMedidaCitasJornadaEmpleado" => "required",
                "inHorasLaboralesJornadaEmpleado" => "required|numeric|min:1",
                "Tercero_oidTerceroEmpleado" => "required",
                'txMensajeJornadaEmpleadoMensaje.*' => 'required',
                'lsOperadorJornadaEmpleadoMensaje.*' => 'required',
                'inPorcentajeJornadaEmpleadoMensaje.*' => 'required',

                );

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                $mensaje["Tercero_oidTerceroEmpleado.required"] =  "El campo Empleado es obligatorio";
                $mensaje["inMetaLlamadasJornadaEmpleado.required"] = "El campo Meta de llamadas es obligatorio";
                $mensaje["txUnidadMedidaLlamadasJornadaEmpleado.required"] =  "El campo Unidad de medida es obligatorio";
                $mensaje["inMetaCitasJornadaEmpleado.required"] = "El campo Meta de citas es obligatorio";
                $mensaje["txUnidadMedidaCitasJornadaEmpleado.required"] =  "El campo Unidad de medida es obligatorio";
                $mensaje["inHorasLaboralesJornadaEmpleado.required"] =  "El campo Horas laborales es obligatorio";
                $mensaje["inHorasLaboralesJornadaEmpleado.min"] =  "El campo Horas laborales debe ser mayor o igual a 1";
       
                $total = ($this->get("oidJornadaEmpleadoMensaje") !== null ) ? count($this->get("oidJornadaEmpleadoMensaje")) : 0;
                for($j=0; $j < $total; $j++)
                {
                    $mensaje['txMensajeJornadaEmpleadoMensaje.'.$j.'.required'] = "El Mensaje es obligatorio en el registro ".($j+1);
                    $mensaje['lsOperadorJornadaEmpleadoMensaje.'.$j.'.required'] = "El Operador es obligatorio en el registro ".($j+1);
                    $mensaje['inPorcentajeJornadaEmpleadoMensaje.'.$j.'.required'] = "El Porcentaje es obligatorio en el registro ".($j+1);
                }
                    return $mensaje;
            }
        }