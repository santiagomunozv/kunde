<?php

    namespace Modules\RegistroSeguimiento\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class TareaRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "txPrioridadTarea" => "required",
            "txAsuntoTarea" => "required",
            "txAreaTarea" => "required",
            "txResponsableTarea" => "required",
            "dtFechaVencimientoTarea" => "required",
            "txEstadoTarea" => "required",
        );


            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["txPrioridadTarea.required"] = "El campo Prioridad es obligatorio";
                $mensaje["txAsuntoTarea.required"] =  "El campo Asunto es obligatorio";
                $mensaje["txAreaTarea.required"] =  "El campo Área es obligatorio";
                $mensaje["txResponsableTarea.required"] =  "El campo Responsable es obligatorio";
                $mensaje["dtFechaVencimientoTarea.required"] =  "El campo Fecha de vencimiento asignada es obligatorio";
                $mensaje["txEstadoTarea.required"] =  "El campo Estado es obligatorio";

            
            return $mensaje;
        }
    }
        