<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use Modules\RegistroSeguimiento\Entities\ServicioModel;
use Modules\RegistroSeguimiento\Http\Requests\ServicioRequest;

class ServicioController extends Controller
{
  /**
   * Display a listing of the resource.
   * @return Response
   */
  public function index()
  {
    $permisos = $this->consultarPermisos();
    if (!$permisos) {
      abort(401);
    }
    $servicio = ServicioModel::orderBy('oidServicio', 'DESC')->get();
    return view('registroseguimiento::servicioGrid', compact('permisos', 'servicio'));
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
    $servicio = new ServicioModel();
    $servicio->txEstadoServicio = 'Activo';

    return view('registroseguimiento::servicioForm', ['servicio' => $servicio]);
  }

  /**
   * Store a newly created resource in storage.
   * @param Request $request
   * @return Response
   */
  public function store(ServicioRequest $request)
  {
    DB::beginTransaction();
    try {
      $servicio = ServicioModel::create($request->all());
      DB::commit();
      return response(['oidServicio' => $servicio->oidServicio], 201);
    } catch (\Exception $e) {
      DB::rollback();
      return abort(500, $e->getMessage());
    }
  }

  /**
   * Show the specified resource.
   * @param int $id
   * @return Response
   */
  public function show($id)
  {
    return view('registroseguimiento::show');
  }

  /**
   * Show the form for editing the specified resource.
   * @param int $id
   * @return Response
   */
  public function edit($id)
  {
    $servicio = ServicioModel::find($id);

    return view('registroseguimiento::servicioForm', ['servicio' => $servicio]);
  }

  /**
   * Update the specified resource in storage.
   * @param Request $request
   * @param int $id
   * @return Response
   */
  public function update(ServicioRequest $request, $id)
  {
    DB::beginTransaction();
    try {
      $servicio = ServicioModel::find($id);
      $servicio->fill($request->all());
      $servicio->save();
      DB::commit();
      return response(['oidServicio' => $servicio->oidServicio], 200);
    } catch (\Exception $e) {
      DB::rollback();
      return response(['oidServicio' => $servicio->oidServicio], 500);
    }
  }

  /**
   * Remove the specified resource from storage.
   * @param int $id8
   * @return Response
   */
  public function destroy($id)
  {
    //
  }
}
