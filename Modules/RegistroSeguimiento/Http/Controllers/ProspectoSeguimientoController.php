<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\RegistroSeguimiento\Entities\ProspectoSeguimientoModel;
use DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\CiudadModel;
use Modules\General\Entities\DepartamentoModel;
use Modules\RegistroSeguimiento\Entities\CampanaModel;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use Modules\RegistroSeguimiento\Entities\ResultadosModel;
use Modules\RegistroSeguimiento\Http\Requests\ProspectoAsignacionEmpleadoRequest;
use Modules\RegistroSeguimiento\Http\Requests\ProspectoRecordatorioRequest;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionAsistenteModel;
use Modules\Reunion\Entities\ReunionModel;

class ProspectoSeguimientoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $prospecto = ProspectoModel::find($id);
        $campana = CampanaModel::where("txEstadoCampana", '=', "Activo")->pluck("txNombreCampana", "oidCampana");
        $prospectoAsignacionEmpleado = ProspectoSeguimientoModel::select("oidProspectoSeguimiento", 'txAccionProspectoSeguimiento', 'txAtiendeProspectoSeguimiento', 'txEncargadoProspectoSeguimiento', 'dtFechaPrevistaProspectoSeguimiento', 'hrHoraPrevistaProspectoSeguimiento', 'dtFechaSeguimientoProspectoSeguimiento', 'hrHoraSeguimientoProspectoSeguimiento', 'txObservacionSeguimiento', 'Prospecto_oidProspecto', 'Resultado_oidResultado', 'Tercero_oidEmpleado')->where('Prospecto_oidProspecto', $id)->get();

        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        $departamento = DepartamentoModel::where('Pais_oidDepartamento', '=', 11)->pluck('txNombreDepartamento', 'oidDepartamento');
        $ciudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");

        $idResultado = ResultadosModel::pluck('oidResultado');

        $nombreResultado = ResultadosModel::pluck('txNombreResultado');

        $dataReunion = ReunionModel::where('Prospecto_oidProspecto', $prospecto->oidProspecto)->first();
        if ($dataReunion) {
            $reunion = ReunionModel::find($dataReunion->oidReunion);
            $reunionAsistente = ReunionAsistenteModel::where('Reunion_oidReunion', '=', $dataReunion->oidReunion)->get();
        } else {
            $reunion = new ReunionModel();
            $reunionAsistente = new ReunionAsistenteModel();
        }

        $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');


        return view('registroseguimiento::prospectoAsignacionEmpleadoForm', compact("prospectoAsignacionEmpleado", 'prospecto', "id", 'asn_tercerolistaEmpleado', 'campana', 'idResultado', 'nombreResultado', "asn_tercerolistaEmpleado", "reunionAsistente", "reunion", "evento", "ciudad", "departamento"));
    }

    public function indexRecordatorio($id)
    {
        $prospecto = ProspectoModel::find($id);
        return view('registroseguimiento::prospectoRecordatorioForm', compact('prospecto', "id"));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id, ProspectoAsignacionEmpleadoRequest $request)
    {
        DB::beginTransaction();
        try {
            $prospecto = ProspectoModel::find($id);
            $prospecto->Campana_oidCampana = $request['Campana_oidCampana'];
            $prospecto->Departamento_oidDepartamento = $request['Departamento_oidDepartamento'];
            $prospecto->Ciudad_oidCiudad = $request['Ciudad_oidCiudad'];
            $prospecto->Tercero_oidEmpleado = $request['Tercero_oidEmpleado'];
            $prospecto->txNombreArlProspecto = $request['txNombreArlProspecto'];
            $prospecto->inCantidadEmpleadosProspecto = $request['inCantidadEmpleadosProspecto'];
            $prospecto->chSistemaProspecto = $request['chSistemaProspecto'];
            $prospecto->txAccidenteLaboralProspecto = $request['txAccidenteLaboralProspecto'];
            $prospecto->txEstadoProspecto = $request['txEstadoProspecto'];
            $prospecto->inIdentificacionProspecto = $request['inIdentificacionProspecto'];
            $prospecto->txNombreRepresentanteProspecto = $request['txNombreRepresentanteProspecto'];
            $prospecto->inIdentificacionRepresentanteProspecto = $request['inIdentificacionRepresentanteProspecto'];
            $prospecto->save();

            $modelo = new ProspectoSeguimientoModel;
            $idsEliminar = explode(',', $request['eliminarprospectoAsignacionEmpleado']);
            $modelo::whereIn('oidProspectoSeguimiento', $idsEliminar)->delete();
            for ($i = 0; $i <  count($request['oidProspectoSeguimiento']); $i++) {
                $indice = array('oidProspectoSeguimiento' => $request['oidProspectoSeguimiento'][$i]);
                $datos = [
                    'txAccionProspectoSeguimiento' => $request["txAccionProspectoSeguimiento"][$i],
                    'dtFechaPrevistaProspectoSeguimiento' => $request["dtFechaSeguimientoProspectoSeguimiento"][$i],
                    'hrHoraPrevistaProspectoSeguimiento' => $request["hrHoraSeguimientoProspectoSeguimiento"][$i],
                    'Prospecto_oidProspecto' => $id,
                    'txAtiendeProspectoSeguimiento' => $request["txAtiendeProspectoSeguimiento"][$i],
                    'txEncargadoProspectoSeguimiento' => $request["txEncargadoProspectoSeguimiento"][$i],
                    'dtFechaSeguimientoProspectoSeguimiento' => $request["dtFechaSeguimientoProspectoSeguimiento"][$i],
                    'hrHoraSeguimientoProspectoSeguimiento' => $request["hrHoraSeguimientoProspectoSeguimiento"][$i],
                    'txObservacionSeguimiento' => $request["txObservacionSeguimiento"][$i],
                    'Resultado_oidResultado' => $request["Resultado_oidResultado"][$i],
                ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            if ($request->get('txTipoReunion') != '') {

                $tercero = TerceroModel::where('oidTercero', $request->get('Tercero_oidResponsable'))->first();
                $asunto = $prospecto->txNombreProspecto . ' - ' . $tercero->txNombreTercero . ' - ' . $prospecto->inTelefonoProspecto;

                $fecha = $request->get("dtFechaReunion") . " " . $request->get("hrHoraReunion");
                $fechaJuliana = strtotime($fecha) * 1000;

                $fechaFinal = strtotime("+" . $request->get("intDuracionReunion") . " minute", strtotime($fecha));
                $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
                $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

                $dataReunion = ReunionModel::where('oidReunion', $request->get('oidReunion'))->first();
                if ($dataReunion) {
                    $reunion = ReunionModel::find($dataReunion->oidReunion);
                } else {
                    $reunion = new ReunionModel;
                }

                $reunion->txAsuntoReunion = $asunto;
                $reunion->txTipoReunion = $request->get('txTipoReunion');
                $reunion->dtFechaReunion = $request->get('dtFechaReunion');
                $reunion->hrHoraReunion = $request->get('hrHoraReunion');
                $reunion->intDuracionReunion = $request->get('intDuracionReunion');
                $reunion->txFechaJulianoReunion = $fechaJuliana;
                $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
                $reunion->txUbicacionReunion = $request->get('txUbicacionReunion');
                $reunion->Tercero_oidResponsable = $request->get('Tercero_oidResponsable');
                $reunion->txDescripcionReunion = $request->get('txDescripcionReunion');
                $reunion->Evento_oidEvento = $request->get('Evento_oidEvento');
                $reunion->Prospecto_oidProspecto = $id;

                $reunion->save();

                $modelo = new ReunionAsistenteModel;
                $total = ($request['oidReunionAsistente'] !== null) ? count($request['oidReunionAsistente']) : 0;
                for ($i = 0; $i <  $total; $i++) {
                    $indice = array('oidReunionAsistente' => $request['oidReunionAsistente'][$i]);
                    $datos = [
                        'txNombreReunionAsistente' => $request["txNombreReunionAsistente"][$i],
                        'txCorreoAsistente' => $request["txCorreoAsistente"][$i],
                        'Reunion_oidReunion' => $reunion->oidReunion
                    ];
                    $guardar = $modelo::updateOrCreate($indice, $datos);
                }
            }
            DB::commit();
            return response(['oidProspecto' => $prospecto->oidProspecto], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function updateRecordatorio($id, ProspectoRecordatorioRequest $request)
    {
        try {
            $modelo = new ProspectoSeguimientoModel();
            $modelo->dtFechaPrevistaProspectoSeguimiento = $request["dtFechaSeguimientoProspectoSeguimiento"];
            $modelo->hrHoraPrevistaProspectoSeguimiento = $request["hrHoraSeguimientoProspectoSeguimiento"];
            $modelo->dtFechaSeguimientoProspectoSeguimiento = $request["dtFechaSeguimientoProspectoSeguimiento"];
            $modelo->hrHoraSeguimientoProspectoSeguimiento = $request["hrHoraSeguimientoProspectoSeguimiento"];
            $modelo->Prospecto_oidProspecto = $id;
            $modelo->save();
            return response(['oidProspecto' => $id], 200);
        } catch (\Exception $e) {
            return abort(500, $e->getMessage());
        }
    }
}
