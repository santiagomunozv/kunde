<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\RegistroSeguimiento\Entities\JornadaEmpleadoModel;
use Modules\RegistroSeguimiento\Http\Requests\JornadaEmpleadoRequest;
use DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\JornadaEmpleadoMensajeModel;

class JornadaEmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $jornadaEmpleado = JornadaEmpleadoModel::join("asn_tercero", "rgs_jornadaempleado.Tercero_oidTerceroEmpleado", "asn_tercero.oidTercero") -> orderBy('oidJornadaEmpleado' , 'DESC')->get();
        return view('registroseguimiento::jornadaEmpleadoGrid', compact('permisos' , 'jornadaEmpleado'));

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $jornadaEmpleado = new JornadaEmpleadoModel();
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
        ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
        ->where('asn_tercero.txEstadoTercero','=', 'Activo')
        ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Emp')
        ->pluck('txNombreTercero','oidTercero');

        $jornadaEmpleadoMensaje = new JornadaEmpleadoMensajeModel();

        return view('registroseguimiento::jornadaEmpleadoForm', compact("asn_tercerolistaEmpleado", 'jornadaEmpleadoMensaje'), ['jornadaEmpleado' => $jornadaEmpleado]);

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(JornadaEmpleadoRequest $request)
    {
        DB::beginTransaction();
        try{
            $jornadaEmpleado = JornadaEmpleadoModel::create($request->all());
             // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
             
            $modelo = new JornadaEmpleadoMensajeModel;
            $total = ($request['oidJornadaEmpleadoMensaje'] !== null ) ? count($request['oidJornadaEmpleadoMensaje']) : 0;
            for($i = 0; $i <  $total; $i++)
            {
                $indice = array('oidJornadaEmpleadoMensaje' => $request['oidJornadaEmpleadoMensaje'][$i]);
                $datos = [
                        'txMensajeJornadaEmpleadoMensaje'=>$request["txMensajeJornadaEmpleadoMensaje"][$i],
                        'lsOperadorJornadaEmpleadoMensaje'=>$request["lsOperadorJornadaEmpleadoMensaje"][$i],
                        'inPorcentajeJornadaEmpleadoMensaje'=>$request["inPorcentajeJornadaEmpleadoMensaje"][$i],
                        'JornadaEmpleado_oidJornadaEmpleado'=>$jornadaEmpleado->oidJornadaEmpleado
                    ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            DB::commit();
            return response(['oidJornadaEmpleado' => $jornadaEmpleado->oidJornadaEmpleado] , 201);
        }
        catch(\Exception $e){
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('registroseguimiento::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $jornadaEmpleado = JornadaEmpleadoModel::find($id);
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
        ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
        ->where('asn_tercero.txEstadoTercero','=', 'Activo')
        ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Emp')
        ->pluck('txNombreTercero','oidTercero');

        $jornadaEmpleadoMensaje = JornadaEmpleadoMensajeModel::where('JornadaEmpleado_oidJornadaEmpleado', '=', $id)->get();

        return view('registroseguimiento::jornadaEmpleadoForm' , compact("asn_tercerolistaEmpleado", "jornadaEmpleadoMensaje"), ['jornadaEmpleado' => $jornadaEmpleado] );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(JornadaEmpleadoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $jornadaEmpleado = JornadaEmpleadoModel::find($id);
            $jornadaEmpleado->fill($request->all());
            $jornadaEmpleado->save();

            $modelo = new JornadaEmpleadoMensajeModel();
            $idsEliminar = explode(',', $request['eliminarJornadaEmpleadoMensaje']);
            $modelo::whereIn('oidJornadaEmpleadoMensaje',$idsEliminar)->delete();
            $total = ($request['oidJornadaEmpleadoMensaje'] !== null ) ? count($request['oidJornadaEmpleadoMensaje']) : 0;
            for($i = 0; $i <  $total; $i++)
            {
                $indice = array('oidJornadaEmpleadoMensaje' => $request['oidJornadaEmpleadoMensaje'][$i]);
                $datos = [
                        'txMensajeJornadaEmpleadoMensaje'=>$request["txMensajeJornadaEmpleadoMensaje"][$i],
                        'lsOperadorJornadaEmpleadoMensaje'=>$request["lsOperadorJornadaEmpleadoMensaje"][$i],
                        'inPorcentajeJornadaEmpleadoMensaje'=>$request["inPorcentajeJornadaEmpleadoMensaje"][$i],
                        'JornadaEmpleado_oidJornadaEmpleado'=>$jornadaEmpleado->oidJornadaEmpleado
                    ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidJornadaEmpleado' => $jornadaEmpleado->oidJornadaEmpleado] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
    
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
