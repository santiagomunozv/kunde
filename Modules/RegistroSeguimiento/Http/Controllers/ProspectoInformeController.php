<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\CampanaModel;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;

class ProspectoInformeController extends Controller
{
    public function filtrarInformeEmpleado(){
        return view ('registroseguimiento::filtroInformeEmpleadoRegistroSeguimiento', compact("consulta"));
    }

    public function indexEmpleado(){
        $condicion = (isset($_GET['condicion']) && $_GET['condicion'] != '' ? 'WHERE '.$_GET['condicion'] : '');
        $consulta = DB::select(
        "SELECT
            t.txNombreTercero as nombreEmpleado, 
            ps.dtFechaSeguimientoProspectoSeguimiento as fechaSeguimiento, 
            je.inHorasLaboralesJornadaEmpleado as horasLaborales,
            CASE
                WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
                WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
                WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
                WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado /11
                WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
            END as  metaLlamadas,
            SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
            SUM(IF(rs.lsEstadoEmpleadoResultado = 'Sin respuesta', 1 , 0)) as noContestaron,
            SUM(IF(rs.lsEstadoEmpleadoResultado = 'No aceptaron', 1 , 0)) as noAceptaron,
            SUM(IF(rs.lsEstadoEmpleadoResultado = 'Errado', 1 , 0)) as errado,
            count(ps.oidProspectoSeguimiento) as llamadasRealizadas
        FROM
            rgs_prospecto p
            JOIN rgs_prospectoseguimiento ps ON p.oidProspecto = ps.Prospecto_oidProspecto
            JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
            JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
            JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
        $condicion
        GROUP BY t.oidTercero, ps.dtFechaSeguimientoProspectoSeguimiento");

        if(count($consulta) == 0){
            echo 'No se encontraron registros para el filtro indicado';
            return;
        }
        return view('registroseguimiento::informeEmpleadoRegistroSeguimiento', compact("consulta"));

    }

    // public function indexCitasConcretadas(){
    //     $consulta = DB::select(
    //     "SELECT
    //         t.txNombreTercero as nombreEmpleado, 
    //         ps.dtFechaSeguimientoProspectoSeguimiento as fechaSeguimiento, 
    //         je.inHorasLaboralesJornadaEmpleado as horasLaborales,
    //         CASE
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado /11
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
    //         END as  metaLlamadas,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Sin respuesta', 1 , 0)) as noContestaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'No aceptaron', 1 , 0)) as noAceptaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Errado', 1 , 0)) as errado,
    //         count(ps.oidProspectoSeguimiento) as llamadasRealizadas
    //     FROM
    //         rgs_prospecto p
    //         JOIN rgs_prospectoseguimiento ps ON p.oidProspecto = ps.Prospecto_oidProspecto
    //         JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
    //         JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
    //         JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
    //     GROUP BY t.oidTercero, ps.dtFechaSeguimientoProspectoSeguimiento");
    //     return view('registroseguimiento::informeCitasConcretadasRegistroSeguimiento', compact("consulta"));

    // }

    // public function indexCitasSacadas(){
    //     $consulta = DB::select(
    //     "SELECT
    //         t.txNombreTercero as nombreEmpleado, 
    //         ps.dtFechaSeguimientoProspectoSeguimiento as fechaSeguimiento, 
    //         je.inHorasLaboralesJornadaEmpleado as horasLaborales,
    //         CASE
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado /11
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
    //         END as  metaLlamadas,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Sin respuesta', 1 , 0)) as noContestaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'No aceptaron', 1 , 0)) as noAceptaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Errado', 1 , 0)) as errado,
    //         count(ps.oidProspectoSeguimiento) as llamadasRealizadas
    //     FROM
    //         rgs_prospecto p
    //         JOIN rgs_prospectoseguimiento ps ON p.oidProspecto = ps.Prospecto_oidProspecto
    //         JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
    //         JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
    //         JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
    //     GROUP BY t.oidTercero, ps.dtFechaSeguimientoProspectoSeguimiento");
    //     return view('registroseguimiento::informeCitasSacadasRegistroSeguimiento', compact("consulta"));

    // }

    //  public function indexCierre(){
    //     $consulta = DB::select(
    //     "SELECT
    //         t.txNombreTercero as nombreEmpleado, 
    //         ps.dtFechaSeguimientoProspectoSeguimiento as fechaSeguimiento, 
    //         je.inHorasLaboralesJornadaEmpleado as horasLaborales,
    //         CASE
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado /11
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
    //         END as  metaLlamadas,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Sin respuesta', 1 , 0)) as noContestaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'No aceptaron', 1 , 0)) as noAceptaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Errado', 1 , 0)) as errado,
    //         count(ps.oidProspectoSeguimiento) as llamadasRealizadas
    //     FROM
    //         rgs_prospecto p
    //         JOIN rgs_prospectoseguimiento ps ON p.oidProspecto = ps.Prospecto_oidProspecto
    //         JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
    //         JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
    //         JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
    //     GROUP BY t.oidTercero, ps.dtFechaSeguimientoProspectoSeguimiento");
    //     return view('registroseguimiento::informeCierreRegistroSeguimiento', compact("consulta"));

    // }

    // public function indexTiempoCierre(){
    //     $consulta = DB::select(
    //     "SELECT
    //         t.txNombreTercero as nombreEmpleado, 
    //         ps.dtFechaSeguimientoProspectoSeguimiento as fechaSeguimiento, 
    //         je.inHorasLaboralesJornadaEmpleado as horasLaborales,
    //         CASE
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado /11
    //             WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
    //         END as  metaLlamadas,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Sin respuesta', 1 , 0)) as noContestaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'No aceptaron', 1 , 0)) as noAceptaron,
    //         SUM(IF(rs.lsEstadoEmpleadoResultado = 'Errado', 1 , 0)) as errado,
    //         count(ps.oidProspectoSeguimiento) as llamadasRealizadas
    //     FROM
    //         rgs_prospecto p
    //         JOIN rgs_prospectoseguimiento ps ON p.oidProspecto = ps.Prospecto_oidProspecto
    //         JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
    //         JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
    //         JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
    //     GROUP BY t.oidTercero, ps.dtFechaSeguimientoProspectoSeguimiento");
    //     return view('registroseguimiento::informeTiempoCierreRegistroSeguimiento', compact("consulta"));

    // }

    public function indexProspecto(){
        $condicion = (isset($_GET['condicion']) && $_GET['condicion'] != '' ? 'WHERE '.$_GET['condicion'] : '');

        $consulta = DB::select(
            "SELECT
                p.txNombreProspecto,
                count(ps.oidProspectoSeguimiento) as llamadasRealizadas,
                SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1 , 0)) as exitoso,
                SUM(IF(rs.lsEstadoEmpleadoResultado != 'Exitoso', 1 , 0)) as fallido
            FROM
                rgs_prospecto p
                join rgs_prospectoseguimiento ps on p.oidProspecto = ps.Prospecto_oidProspecto
                JOIN rgs_resultados rs ON ps.Resultado_oidResultado = rs.oidResultado
                JOIN rgs_jornadaempleado je ON p.Tercero_oidEmpleado = je.Tercero_oidTerceroEmpleado
                JOIN asn_tercero t ON p.Tercero_oidEmpleado = t.oidTercero
            $condicion
            GROUP BY p.txNombreProspecto");

        if(count($consulta) == 0){
            echo 'No se encontraron registros para el filtro indicado';
            return;
        }

        return view('registroseguimiento::informeProspectoRegistroSeguimiento', compact("consulta"));

    }

    public function indexFiltroInforme(){
            $prospecto =ProspectoModel::pluck('txNombreProspecto', 'oidProspecto');
            $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero','=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero','=', 'Emp')
            ->pluck('txNombreTercero','oidTercero');
            return view('registroseguimiento::filtroInformeProspectoForm', compact("prospecto","asn_tercerolistaEmpleado"));

    }
}