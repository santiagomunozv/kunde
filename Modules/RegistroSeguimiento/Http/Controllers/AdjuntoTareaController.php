<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use App\Traits\FilesTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\RegistroSeguimiento\Entities\AdjuntoTareaModel;

class AdjuntoTareaController extends Controller
{
    use FilesTrait;

    public function store(Request $request)
    {
        return $this->response($request, 'tareas');
    }

    public function show($id)
    {
        $file = AdjuntoTareaModel::find($id);
        $path = Storage::path($file->txRutaArchivo);

        return $this->get($path);
    }

}
