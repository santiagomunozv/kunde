<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Traits\FilesTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\TareaModel;
use Modules\RegistroSeguimiento\Http\Requests\TareaRequest;
use Modules\GestionHumana\Entities\AreaModel;
use Modules\RegistroSeguimiento\Entities\AdjuntoTareaModel;
use Modules\RegistroSeguimiento\Http\Requests\Tarea\UpdateRequest;

class TareaController extends Controller
{
    use FilesTrait;

    public function index()
    {
        $area = new AreaModel();
        $permisos = $this->consultarPermisos();

        if (!$permisos) {
            abort(401);
        }

        if (isset($_GET['tipo'])) {
            if ($_GET['tipo'] == '') {
                Cache::put('tipo', '', 60);
                $tipo = Cache::get('tipo');
            } else {
                Cache::put('tipo', $_GET['tipo'], 60);
                $tipo = Cache::get('tipo');
                redirect('registroseguimiento/tarea?tipo=' . $tipo);
            }
        } else {
            $tipo = Cache::get('tipo');
        }

        $tarea = TareaModel::from('rgs_tarea as t')
            ->join('gen_area as a', function ($join) {
                $join->where(DB::raw("FIND_IN_SET(a.oidArea, t.txAreaTarea)"), "<>", "0");
            })
            ->join('asn_tercero as ter', function ($join) {
                $join->where(DB::raw("FIND_IN_SET(ter.oidTercero, t.txResponsableTarea)"), "<>", "0");
            });

        if (Session::get('rolUsuario') != 1) {
            $tarea->whereRaw("txResponsableTarea REGEXP '" . Session::get('oidTercero') . "'");
        }

        if ($tipo != '') {
            $tarea->whereRaw("txEstadoTarea = '$tipo'");
        }

        $tarea = $tarea->groupBy('t.oidTarea')
            ->select(
                "oidTarea",
                "dtFechaElaboracionTarea",
                "txAsuntoTarea",
                "txPrioridadTarea",
                DB::raw("GROUP_CONCAT(DISTINCT txNombreArea) AS txAreaTarea"),
                DB::raw("GROUP_CONCAT(DISTINCT txNombreTercero) AS txResponsableTarea"),
                "txEstadoTarea",
                "dtFechaVencimientoTarea",
                "txFechaSolucionTarea",
                "txDescripcionTarea",
                "txSolucionTarea",
                "estadoAprobador",
                "observacionAprobador",
                DB::raw("IF(txEstadoTarea = 'Terminado', '81DAF5',(CASE
                WHEN txPrioridadTarea = 'Alta' THEN 'F5B7B1'
                WHEN txPrioridadTarea = 'Media' THEN 'F9E79F'
                WHEN txPrioridadTarea = 'Baja' THEN 'ABEBC6'
            END))
            AS colorTarea")
            )
            ->get();

        $estado = false;

        return view('registroseguimiento::tareaGrid', compact('permisos', 'tarea', 'estado'));
    }

    public function create()
    {
        $tarea = new TareaModel();
        $area = AreaModel::where("txEstadoArea", '=', "Activo")->pluck("txNombreArea", "oidArea");
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        return view('registroseguimiento::tareaForm', compact('asn_tercerolistaEmpleado', 'area'), ['tarea' => $tarea]);
    }

    public function store(TareaRequest $request)
    {
        DB::beginTransaction();
        try {
            $tarea = TareaModel::create($request->all());
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTarea' => $tarea->oidTarea], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        return view('tarea::show');
    }

    public function edit($id)
    {
        $tarea = TareaModel::with('adjunto')->find($id);
        $area = AreaModel::where("txEstadoArea", '=', "Activo")->pluck("txNombreArea", "oidArea");
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        return view('registroseguimiento::tareaForm', compact('asn_tercerolistaEmpleado', 'area'), ['tarea' => $tarea]);
    }

    public function update(UpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $tarea = TareaModel::find($id);
            $adjunto =  new AdjuntoTareaModel();

            $tarea->fill($request->all());
            if ($request->get('txEstadoTarea') == 'Terminado' && !$tarea->txFechaSolucionTarea) {
                $tarea->txFechaSolucionTarea = date('Y-m-d');
            }

            $this->saveAdjunto($request, $adjunto, $id);
            $tarea->save();


            DB::commit();
            return response(['oidTarea' => $tarea->oidTarea], 200);
        } catch (\Exception $e) {

            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function destroy($id)
    {
        TareaModel::destroy($id);
        return redirect('/registroseguimiento/tarea');
    }

    // método encargado de cambiar el estado de la tarea
    public function cambiarEstadoTarea($idTarea)
    {
        $tarea = TareaModel::select('oidTarea', 'txEstadoTarea')->where('oidTarea', $idTarea)->first();
        if ($tarea->txEstadoTarea === 'Nuevo') {
            $tarea->txEstadoTarea = "Terminado";
            $tarea->txFechaSolucionTarea = date('Y-m-d');
        } else {
            $tarea->txEstadoTarea = "Nuevo";
            $tarea->txFechaSolucionTarea = NULL;
        }
        $tarea->save();

        return response(['oidTarea' => $tarea->oidTarea], 200);
    }

    //método encargado de modificar el estado aprobador
    public function modalCambiarEstadoAprobador($idTarea)
    {
        $tarea = TareaModel::find($idTarea);

        return view('registroseguimiento::modalEstadoAprobador', compact('tarea'));
    }

    public function cambiarEstadoAprobador(Request $request)
    {
        // dd($request->all());
        // dd($request['estado'] === "Rechazado");

        $tarea = TareaModel::select('oidTarea', 'txEstadoTarea', 'estadoAprobador', 'observacionAprobador')->where('oidTarea', $request['oidTarea'])->first();

        if ($request['estado'] === "Aprobado") {

            $tarea->estadoAprobador = "Aprobado";
        } elseif ($request['estado'] === "Rechazado") {

            $tarea->estadoAprobador = "Rechazado";
            $tarea->txEstadoTarea = "Nuevo";
            $tarea->observacionAprobador = $request['observacionAprobador'];
        }
        $tarea->save();

        return response(['oidTarea' => $tarea->oidTarea], 200);
    }

    public function saveAdjunto($data, $adjunto, $idTarea)
    {
        if (isset($data['imagenAdjunto'])) {
            $path = 'tarea/' . $data['oidTarea'] . '_' . random_int(100, 999);
            $this->save($data, $adjunto, 'tareas_save', $path, $idTarea);
        }
        if ($data['eliminarImagen'] !== null) {
            $this->delete($data['eliminarImagen'], $adjunto);
        }
    }
}
