<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use PhpOffice\PhpWord\TemplateProcessor;
use Modules\RegistroSeguimiento\Entities\ServicioModel;

class PropuestaController extends Controller
{
  public function index()
  {
    $prospecto = ProspectoModel::pluck('txNombreProspecto', 'oidProspecto');
    $servicios = ServicioModel::where('txEstadoServicio', '=', 'Activo')->get();

    return view('registroseguimiento::propuestaForm', compact('prospecto', 'servicios'));
  }

  public function store(Request $request)
  {
    $prospectoId = $request->get('Prospecto_oidProspecto');
    $prospecto = ProspectoModel::find($prospectoId);

    $path = public_path('docs/propuesta/' . $prospecto->txNombreProspecto);
    if (!file_exists(public_path('docs/propuesta/' . $prospecto->txNombreProspecto))) {
      mkdir($path, 0777, true);
    }

    $this->guardarDocumento("Propuesta Alianza Seguridad y Salud en el Trabajo", $prospecto, $path, $request);
    return response(['message' => 'docs/propuesta/' . $prospecto->txNombreProspecto . "/Propuesta Alianza Seguridad y Salud en el Trabajo.docx"], 200);
  }

  public function guardarDocumento($documentoNombre, $prospecto, $ruta, $request)
  {
    $rutaPlantilla = public_path("docs/propuesta/$documentoNombre.docx");

    $templateProcessor = new TemplateProcessor($rutaPlantilla);
    $fechaCarbon = Carbon::parse($request->get('dtFechaPropuesta'));
    $dia = $fechaCarbon->day;
    $mes = $fechaCarbon->formatLocalized('%B');
    $anio = $fechaCarbon->year;

    $colores = [
      '00B0F0',
      'FF0000',
      '00FF00',
      'FFD700'
    ];

    $valor = 0;
    $descripciones = [];
    for ($i = 0; $i < count($request->get('txNombreServicio')); $i++) {
      if ($request['chGenerarServicio'][$i]) {
        $valor += $request->get('inValorServicio')[$i];

        $descripciones[] = [
          'color' => $colores[rand(0, count($colores) - 1)],
          'texto' => $request->get('txDescripcionServicio')[$i]
        ];
      }
    }

    $templateProcessor->setValues(array(
      'anio' => $anio,
      'mes' => $mes,
      'dia' => $dia,
      'txNombreProspecto' => $prospecto->txNombreProspecto,
      'txObsequioPropuesta' => $request->get('txObsequioPropuesta'),
      'inValorPropuesta' => number_format($valor * 12, 2, ',', '.'),
      'inValorCuotaPropuesta' => number_format($valor, 2, ',', '.'),
    ));

    $descripcionesTexto = '';
    foreach ($descripciones as $descripcion) {
      $descripcionesTexto .= "<w:p><w:r><w:rPr><w:color w:val='{$descripcion['color']}'/></w:rPr><w:t>● {$descripcion['texto']}</w:t></w:r></w:p>";
    }

    $templateProcessor->setValue('descripciones', $descripcionesTexto);

    $templateProcessor->saveAs($ruta . '/' . $documentoNombre . '.docx');
  }
}
