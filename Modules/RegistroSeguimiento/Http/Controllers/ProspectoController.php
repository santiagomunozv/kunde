<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Imports\ProspectoImport;
use App\Imports\ProspectoImport1;
use DB;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Modules\RegistroSeguimiento\Entities\CampanaModel;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use Modules\RegistroSeguimiento\Entities\ProspectoSeguimientoModel;
use Modules\RegistroSeguimiento\Http\Requests\ProspectoRequest;
use RuntimeException;

class ProspectoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $tipo = (isset($_GET['tipo']) ? $_GET['tipo'] : '');
        $prospecto = ProspectoModel::leftJoin('reu_reunion', 'rgs_prospecto.oidProspecto', '=', 'reu_reunion.Prospecto_oidProspecto')
            ->selectRaw("oidProspecto, txNombreProspecto, inTelefonoProspecto, txEstadoProspecto, IF(reu_reunion.oidReunion IS NULL, 'Creado', 'Asignado') as tipoProspecto, IF(reu_reunion.oidReunion IS NULL, '#FDEBD0', '#D1F2EB') as colorProspecto")
            ->orderBy('oidProspecto', 'DESC')
            ->where(function ($query) {
                $query->where('Usuario_oidUsuario', '=', Auth::id())
                    ->orWhere('Tercero_oidResponsable', '=', Session::get('oidTercero'));
            });

        if ($tipo != '') {
            $prospecto->whereRaw("txEstadoProspecto = '$tipo'");
        }

        $prospecto = $prospecto->groupBy('oidProspecto')->get();

        return view('registroseguimiento::prospectoGrid', compact('permisos', 'prospecto'), ['prospecto' => $prospecto]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $prospecto = new ProspectoModel();
        $campana = CampanaModel::where("txEstadoCampana", '=', "Activo")->pluck("txNombreCampana", "oidCampana");
        return view('registroseguimiento::prospectoForm', compact("campana"), ['prospecto' => $prospecto]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProspectoRequest $request)
    {
        DB::beginTransaction();
        try {
            $prospecto = new ProspectoModel();
            $prospecto->fill($request->all());
            $prospecto->Usuario_oidUsuario = Auth::id();
            $prospecto->save();
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProspecto' => $prospecto->oidProspecto], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('registroseguimiento::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $prospecto = ProspectoModel::find($id);
        $campana = CampanaModel::where("txEstadoCampana", '=', "Activo")->pluck("txNombreCampana", "oidCampana");

        return view('registroseguimiento::prospectoForm', compact("prospecto", "campana"), ['prospecto' => $prospecto]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProspectoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $prospecto = ProspectoModel::find($id);
            $prospecto->fill($request->all());
            $prospecto->save();
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProspecto' => $prospecto->oidProspecto], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id8
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function indexImportarProspecto()
    {
        return view('registroseguimiento::prospectoImportForm');
    }

    public function cargarArchivoProspecto()
    {
        $destinationPath = public_path() . '/images';
        $fileName = Input::file('file')->getClientOriginalName();
        $upload_success = Input::file('file')->move($destinationPath, $fileName);

        if ($upload_success) {
            return response(["nombreArchivo" => $fileName], 200);
        } else {
            return abort(500, 'error');
        }
    }

    public function importarProspectos(Request $request)
    {
        DB::beginTransaction();
        try {
            $file = public_path() . '/images/' . $request['nombreArchivo'];
            $datos = Excel::toArray(new ProspectoImport1, $file);
            if (!$datos) {
                throw new Exception('No se encontraron datos en la plantilla importada');
            }

            foreach ($datos[0] as $pos => $dato) {
                if ($pos !== 0) {
                    $nombreProspecto = $dato[0];
                    $telefonoProspecto = $dato[1];

                    $this->buscarTelefonoProspecto($telefonoProspecto);

                    $prospecto = new ProspectoModel();
                    $prospecto->txNombreProspecto = $nombreProspecto;
                    $prospecto->inTelefonoProspecto = $telefonoProspecto;
                    $prospecto->Usuario_oidUsuario = Auth::id();
                    $prospecto->save();
                }
            }
            DB::commit();
            return response(["message" => 'Prospectos cargados'], Response::HTTP_CREATED);
        } catch (Exception $e) {
            DB::rollBack();
            return response(["message" => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function buscarTelefonoProspecto($telefonoProspecto)
    {
        $prospecto = ProspectoModel::where('inTelefonoProspecto', $telefonoProspecto)->first();

        if ($prospecto) {
            throw new Exception("El prospecto con número telefónico $telefonoProspecto ya se encuentra registrado");
        }
    }

    public function indexObservacionesProspecto($id)
    {
        $observaciones = ProspectoSeguimientoModel::leftJoin('rgs_resultados', 'rgs_prospectoseguimiento.Resultado_oidResultado', 'rgs_resultados.oidResultado')
            ->where("Prospecto_oidProspecto", $id)
            ->get();

        return view('registroseguimiento::prospectoObservacionesForm', compact('observaciones'));
    }
}
