<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use PhpOffice\PhpWord\TemplateProcessor;
use Modules\RegistroSeguimiento\Entities\ServicioModel;
use NumberFormatter;

class ContratoController extends Controller
{
    public function index()
    {
        $prospecto = ProspectoModel::pluck('txNombreProspecto', 'oidProspecto');
        $servicios = ServicioModel::where('txEstadoServicio', '=', 'Activo')->get();

        return view('registroseguimiento::contratoForm', compact('prospecto', 'servicios'));
    }

    public function store(Request $request)
    {
        $prospectoId = $request->get('Prospecto_oidProspecto');
        $prospecto = ProspectoModel::find($prospectoId);

        $path = public_path('docs/contrato/' . $prospecto->txNombreProspecto);
        if (!file_exists(public_path('docs/contrato/' . $prospecto->txNombreProspecto))) {
            mkdir($path, 0777, true);
        }

        $this->guardarDocumento("Contrato Kunde SG-SST", $prospecto, $path, $request);
        return response(['message' => 'docs/contrato/' . $prospecto->txNombreProspecto . "/Contrato Kunde SG-SST.docx"], 200);
    }

    public function guardarDocumento($documentoNombre, $prospecto, $ruta, $request)
    {
        $rutaPlantilla = public_path("docs/contrato/$documentoNombre.docx");

        $templateProcessor = new TemplateProcessor($rutaPlantilla);
        $fechaCarbon = Carbon::parse($request->get('dtFechaCuotaContrato'));
        $dia = $fechaCarbon->day;
        $mes = $fechaCarbon->formatLocalized('%B');
        $anio = $fechaCarbon->year;

        $valor = 0;
        $etapas = 0;
        $tabla = [];
        for ($i = 0; $i < count($request->get('txNombreServicio')); $i++) {
            if ($request['chGenerarServicio'][$i]) {
                $valor += $request->get('inValorServicio')[$i];
                $etapas += 1;

                $tabla[] = [
                    'servicio' => $request->get('txNombreServicio')[$i],
                    'valor' => number_format($request->get('inValorServicio')[$i], 2, ',', '.')
                ];
            }
        }

        $valorLetras = new NumberFormatter("es", NumberFormatter::SPELLOUT);

        $templateProcessor->setValues(array(
            'txNombreCliente' => $prospecto->txNombreProspecto,
            'inDocumentoCliente' => $this->quitarEspacios($prospecto->inIdentificacionProspecto),
            'txRepresentanteLegalCliente' => $prospecto->txNombreRepresentanteProspecto,
            'inIdentificacionRepresentanteLegalCliente' => $this->quitarEspacios($prospecto->inIdentificacionRepresentanteProspecto),
            'inNivelRiesgo' => $request->get('inNivelRiesgoContrato'),
            'inNumeroEmpleados' => $request->get('inCantidadEmpleadosContrato'),
            'inCantidadCuotas' => $request->get('inCantidadCuotasContrato'),
            'inDiaCuota' => $dia,
            'txMesCuota' => $mes,
            'txAnioCuota' => $anio,
            'inEtapasContrato' => $etapas,
            'inValorContrato' => number_format($valor  * 12, 2, ',', '.'),
            'inValorCuota' => number_format($valor, 2, ',', '.'),
            'txValorCuota' => $valorLetras->format($valor),
        ));

        // $templateProcessor->cloneRowAndSetValues('servicio', $tabla);
        $templateProcessor->saveAs($ruta . '/' . $documentoNombre . '.docx');
    }

    function quitarEspacios($cadena)
    {
        return preg_replace(['/\s+/', '/^\s|\s$/'], [' ', ''], $cadena);
    }

    function base64($archivo)
    {

        $logo = '&nbsp;';
        $fp = fopen($archivo, "r", 0);
        if ($archivo != '' and $fp) {
            $imagen = fread($fp, filesize($archivo));
            fclose($fp);

            $base64 = chunk_split(base64_encode($imagen));
            $logo =  '<img src="data:image/png;base64,' . $base64 . '" alt="Texto alternativo" width="130px"/>';
        }
        return $logo;
    }

    public function limpiarArchivos($carpeta)
    {
        function rmDir_rf($carpeta)
        {
            foreach (glob($carpeta . "/*") as $archivos_carpeta) {
                if (is_dir($archivos_carpeta)) {
                    rmDir_rf($archivos_carpeta);
                } else {
                    unlink($archivos_carpeta);
                }
            }
            rmdir($carpeta);
        }

        rmDir_rf($carpeta);
    }
}
