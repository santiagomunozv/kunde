<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\TareaModel;

class TareaRapidaController extends Controller
{

    public function index()
    {
        $tercero = new TerceroModel();
        $listaEmpleados = $tercero->getTerceroAcoordingTypeAndState('Emp');

        return view('registroseguimiento::tareaRapidaForm', compact('listaEmpleados'));
    }

    public function create()
    {
        return view('registroseguimiento::create');
    }

    public function store(Request $request)
    {
        // dd($request['txAsuntoTarea']);
        DB::beginTransaction();
        try {
            $tarea = new TareaModel;

            $tarea->txAsuntoTarea = $request['txAsuntoTarea'];
            $tarea->txResponsableTarea = $request['txResponsableTarea'];
            $tarea->dtFechaVencimientoTarea = $request['dtFechaVencimientoTarea'];
            $tarea->dtFechaElaboracionTarea = Carbon::now();
            $tarea->txPrioridadTarea = $request['txPrioridadTarea'] ? $request['txPrioridadTarea'] : "Media";
            $tarea->txAreaTarea = 10;
            $tarea->txEstadoTarea = "Nuevo";
            $tarea->txDescripcionTarea = $request['txDescripcionTarea'] ? $request['txDescripcionTarea'] : "";

            $tarea->save();

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTarea' => $tarea->oidTarea], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }
}
