<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use DB;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroClasificacionModel;
use Modules\AsociadoNegocio\Entities\TerceroCompaniaModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\CiudadModel;
use Modules\General\Entities\TipoIdentificacionModel;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use Modules\RegistroSeguimiento\Http\Requests\ProspectoExitosoRequest;

class ProspectoExitosoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($id)
    {
        $prospectoExitoso = ProspectoModel::select("oidProspecto", "txNombreProspecto", "inTelefonoProspecto")->where ('oidProspecto', $id)->get();

        $ciudad = CiudadModel::where('Departamento_oidDepartamento_1aM', '=', 10)->pluck("txNombreCiudad", "oidCiudad");
    
        $tipoIdentidicacion = TipoIdentificacionModel::pluck("txNombreTipoIdentificacion", "oidTipoIdentificacion");

        return view('registroseguimiento::prospectoExitosoForm', compact("ciudad", "tipoIdentidicacion", "id"), ['prospectoExitoso' => $prospectoExitoso]);
    }


    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update($id, ProspectoExitosoRequest $request)
    {
        DB::beginTransaction();
        try{
            $modelo = new TerceroModel();
            $modelo->TipoIdentificacion_oidTercero = $request->get("TipoIdentificacion_oidTercero");
            $modelo->txDocumentoTercero = $request->get("txDocumentoTercero");
            $modelo->txCodigoTercero = $request->get("txDocumentoTercero");
            $modelo->txMovilTercero = $request->get("txMovilTercero");
            $modelo->txPrimerNombreTercero = $request->get("txPrimerNombreTercero");
            $modelo->txPrimerApellidoTercero = $request->get("txPrimerApellidoTercero");
            $modelo->Ciudad_oidTercero = $request->get("Ciudad_oidTercero");
            $modelo->txNombreComercialTercero = $request->get("txPrimerNombreTercero")." ".$request->get("txPrimerApellidoTercero");

            $modelo->txEstadoTercero = "Activo";
            $modelo->daFechaCreacionTercero = date("Y-m-d");
            $modelo->save();

            $terceroClasificacion = new TerceroClasificacionModel();
            $terceroClasificacion->Tercero_oidTercero_1aM = $modelo->oidTercero;
            $terceroClasificacion->ClasificacionTercero_oidTerceroClasificacion_1aM = 1;
            $terceroClasificacion->save();

            $terceroCompania = new TerceroCompaniaModel();
            $terceroCompania->Tercero_oidTercero_1aM = $modelo->oidTercero;
            $terceroCompania->Compania_oidCompania = Session::get('oidCompania');
            $terceroCompania->save();
            DB::commit();
            return response(['oidProspectoSeguimiento' => $modelo->oidProspectoSeguimiento] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

   
}
