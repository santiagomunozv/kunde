<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\RegistroSeguimiento\Entities\ResultadosModel;
use DB;
use Modules\RegistroSeguimiento\Http\Requests\ResultadoRequest;

class ResultadosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $resultado = ResultadosModel::select("oidResultado", "txNombreResultado", "lsEstadoEmpleadoResultado", "chEstadoClienteResultado")->get();
        return view('registroseguimiento::resultadosForm', ['resultado' => $resultado]);
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ResultadoRequest $request)
    {
        DB::beginTransaction();
        try{
            $modelo = new ResultadosModel;
            $idsEliminar = explode(',', $request['eliminarResultado']);
            $modelo::whereIn('oidResultado',$idsEliminar)->delete();
            for($i = 0; $i <  count($request['oidResultado']); $i++)
            {
                $indice = array('oidResultado' => $request['oidResultado'][$i]);
                $datos = [
                        'txNombreResultado'=>$request["txNombreResultado"][$i],
                        'lsEstadoEmpleadoResultado'=>$request["lsEstadoEmpleadoResultado"][$i],
                        'chEstadoClienteResultado'=>$request["chEstadoClienteResultado"][$i],
                    ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }
            DB::commit();
            return response(['oidResultado' => $guardar->oidResultado] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response($e->getMessage());
        }
    }
   
}
