<?php

namespace Modules\RegistroSeguimiento\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\RegistroSeguimiento\Entities\CampanaModel;

use Modules\RegistroSeguimiento\Http\Requests\CampanaRequest;

class CampanaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $campana = CampanaModel::orderBy('oidCampana' , 'DESC')->get();
        return view('registroseguimiento::campanaGrid', compact('permisos' , 'campana'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $campana = new CampanaModel();
        return view('registroseguimiento::campanaForm', ['campana' => $campana] );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CampanaRequest $request)
    {
        DB::beginTransaction();
        try{
            $campana = CampanaModel::create($request->all());
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCampana' => $campana->oidCampana] , 201);
        }
        catch(\Exception $e){
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('registroseguimiento::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $campana = CampanaModel::find($id);
        
        return view('registroseguimiento::campanaForm' , ['campana' => $campana] );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CampanaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $campana = CampanaModel::find($id);
            $campana->fill($request->all());
            $campana->save();
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCampana' => $campana->oidCampana] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidCampana' => $campana->oidCampana] , 500);
    
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id8
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
