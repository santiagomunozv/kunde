<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class JornadaEmpleadoMensajeModel extends Model
{
    protected $table = 'rgs_jornadaempleadomensaje';
    protected $primaryKey = 'oidJornadaEmpleadoMensaje';
    protected $fillable = ['txMensajeJornadaEmpleadoMensaje', 'lsOperadorJornadaEmpleadoMensaje','inPorcentajeJornadaEmpleadoMensaje', 'JornadaEmpleado_oidJornadaEmpleado'];
    public $timestamps = false;
}
