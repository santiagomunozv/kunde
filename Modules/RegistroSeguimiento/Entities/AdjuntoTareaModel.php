<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class AdjuntoTareaModel extends Model
{
    protected $table = 'rgs_adjunto_tarea';
    protected $primaryKey = 'oidAdjuntoTarea';
    protected $fillable = ['txNombreArvchivo', 'txRutaArchivo', 'txTipoArchivo', 'txDescripcionArchivo', 'Tarea_oidTarea'];
    public $timestamps = false;
}
