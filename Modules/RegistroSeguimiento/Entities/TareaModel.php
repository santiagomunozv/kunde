<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TareaModel extends Model
{
    protected $table = 'rgs_tarea';
    protected $primaryKey = 'oidTarea';
    protected $fillable = ['dtFechaElaboracionTarea', 'txAsuntoTarea', 'txPrioridadTarea', 'txAreaTarea', 'txResponsableTarea', 'txEstadoTarea', 'dtFechaVencimientoTarea', 'txFechaSolucionTarea', 'txDescripcionTarea', 'txSolucionTarea', 'ReunionBitacora_oidBitacora', 'estadoAprobador', 'observacionAprobador'];
    public $timestamps = false;

    public static function getTareaByIdBitacora($id)
    {
        $tarea = DB::table('rgs_tarea as ta')
            ->join('reu_reunionbitacora as reb', 'ta.ReunionBitacora_oidBitacora', 'reb.oidReunionBitacora')
            ->join('reu_reunion as re', 'reb.Reunion_idReunion','re.oidReunion')
            ->select('oidTarea', 're.Tercero_oidResponsable','txAsuntoTarea','dtFechaVencimientoTarea')
            ->where('reb.oidReunionBitacora', '=', $id)
            ->get();

        return $tarea;
    }

    public function adjunto()
    {
        return $this->hasMany(AdjuntoTareaModel::class, 'Tarea_oidTarea', 'oidTarea');
    }
}
