<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class ServicioModel extends Model
{
  protected $table = 'rgs_servicio';
  protected $primaryKey = 'oidServicio';
  protected $fillable = ['txNombreServicio', 'txDescripcionServicio'];
  public $timestamps = false;
}
