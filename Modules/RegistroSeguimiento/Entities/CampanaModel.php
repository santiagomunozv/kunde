<?php
namespace Modules\RegistroSeguimiento\Entities;
use Illuminate\Database\Eloquent\Model;

class CampanaModel extends Model
{
    protected $table = 'rgs_campana';
    protected $primaryKey = 'oidCampana';
    protected $fillable = ['txCodigoCampana','txNombreCampana', 'txEstadoCampana'];
    public $timestamps = false;

}