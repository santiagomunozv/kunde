<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class ProspectoSeguimientoModel extends Model
{
    protected $table = 'rgs_prospectoseguimiento';
    protected $primaryKey = 'oidProspectoSeguimiento';
    protected $fillable = ['txAccionProspectoSeguimiento', 'txAtiendeProspectoSeguimiento','txEncargadoProspectoSeguimiento', 'dtFechaPrevistaProspectoSeguimiento', 'hrHoraPrevistaProspectoSeguimiento', 'dtFechaSeguimientoProspectoSeguimiento', 'hrHoraSeguimientoProspectoSeguimiento', 'txObservacionSeguimiento', 'Prospecto_oidProspecto', 'Resultado_oidResultado', 'Tercero_oidEmpleado'];
    public $timestamps = false;
}
