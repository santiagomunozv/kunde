<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class ProspectoModel extends Model
{
    protected $table = 'rgs_prospecto';
    protected $primaryKey = 'oidProspecto';
    protected $fillable = [
        'txNombreProspecto',
        'inTelefonoProspecto',
        'Campana_oidCampana',
        'Tercero_oidEmpleado',
        'txNombreArlProspecto',
        'inCantidadEmpleadosProspecto',
        'chSistemaProspecto',
        'txAccidenteLaboralProspecto',
        'txEstadoProspecto',
        'Usuario_oidUsuario',
        'Departamento_oidDepartamento',
        'Ciudad_oidCiudad',
        'inIdentificacionProspecto',
        'txNombreRepresentanteProspecto',
        'inIdentificacionRepresentanteProspecto'
    ];
    public $timestamps = false;
}
