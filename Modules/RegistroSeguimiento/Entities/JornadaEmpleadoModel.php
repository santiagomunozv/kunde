<?php
namespace Modules\RegistroSeguimiento\Entities;
use Illuminate\Database\Eloquent\Model;

class JornadaEmpleadoModel extends Model
{
    protected $table = 'rgs_jornadaempleado';
    protected $primaryKey = 'oidJornadaEmpleado';
    protected $fillable = ['inMetaLlamadasJornadaEmpleado', 'txUnidadMedidaLlamadasJornadaEmpleado', 'inMetaCitasJornadaEmpleado', 'txUnidadMedidaCitasJornadaEmpleado', 'Tercero_oidTerceroEmpleado', 'inHorasLaboralesJornadaEmpleado'];
    public $timestamps = false;

}