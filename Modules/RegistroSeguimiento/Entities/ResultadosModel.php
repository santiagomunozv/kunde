<?php

namespace Modules\RegistroSeguimiento\Entities;

use Illuminate\Database\Eloquent\Model;

class ResultadosModel extends Model
{
    protected $table = 'rgs_resultados';
    protected $primaryKey = 'oidResultado';
    protected $fillable = ['txNombreResultado', 'lsEstadoEmpleadoResultado','chEstadoClienteResultado'];
    public $timestamps = false;
}
