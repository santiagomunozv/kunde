<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {

    Route::prefix('registroseguimiento')->group(function () {
        Route::get('/', 'RegistroSeguimientoController@index');
        Route::resource('/jornadaempleado', 'JornadaEmpleadoController');
        Route::resource('/prospecto', 'ProspectoController');
        Route::get('/resultados', 'ResultadosController@index');
        Route::put('/resultados', 'ResultadosController@update');
        Route::get('/prospectoasignacionempleado/{id}', 'ProspectoSeguimientoController@index');
        Route::put('/prospectoasignacionempleado/{id}', 'ProspectoSeguimientoController@update');
        Route::get('/prospectoseguimiento/{id}', 'ProspectoSeguimientoController@indexSeguimiento');
        Route::put('/prospectoseguimiento/{id}', 'ProspectoSeguimientoController@updateSeguimiento');
        Route::get('/prospectoexitoso/{id}', 'ProspectoExitosoController@index');
        Route::put('/prospectoexitoso/{id}', 'ProspectoExitosoController@update');
        Route::get('/informeprospectoregistroseguimiento', 'ProspectoInformeController@indexProspecto');
        Route::get('/informeempleadoregistroseguimiento', 'ProspectoInformeController@indexEmpleado');
        Route::get('/informecierreregistroseguimiento', 'ProspectoInformeController@indexCierre');
        Route::get('/informecitasconcretadasregistroseguimiento', 'ProspectoInformeController@indexCitasConcretadas');
        Route::get('/informecitassacadasregistroseguimiento', 'ProspectoInformeController@indexCitasSacadas');
        Route::get('/informetiempocierreregistroseguimiento', 'ProspectoInformeController@indexTiempoCierre');
        Route::get('/filtroinformeprospecto', 'ProspectoInformeController@indexFiltroInforme');
        Route::resource('/campana', 'CampanaController');
        Route::get('/filtroinformeprospectoregistroseguimiento', 'ProspectoInformeController@filtrarInformeEmpleado');
        Route::get('/importarprospectos', 'ProspectoController@indexImportarProspecto');
        Route::post('/importarprospectos', 'ProspectoController@importarProspectos');
        Route::post('cargarArchivoProspecto', 'ProspectoController@cargarArchivoProspecto');
        Route::get('/prospectorecordatorio/{id}', 'ProspectoSeguimientoController@indexRecordatorio');
        Route::put('/prospectorecordatorio/{id}', 'ProspectoSeguimientoController@updateRecordatorio');
        Route::get('/verObservacionesProspecto/{id}', 'ProspectoController@indexObservacionesProspecto');
        Route::resource('/tarea', 'TareaController');
        Route::get('/tarea/cambiarestado/{id}', 'TareaController@cambiarEstadoTarea');
        Route::get('/tarea/modalestoaprobador/{id}', 'TareaController@modalCambiarEstadoAprobador');
        Route::post('/tarea/cambiarestadoaprobador/{id}', 'TareaController@cambiarEstadoAprobador');
        Route::resource('adjunto', 'AdjuntoTareaController')->only(['store', 'show']);
        Route::resource('/tarearapida', 'TareaRapidaController');
        Route::resource('/servicio', 'ServicioController');
        Route::resource('/contrato', 'ContratoController');
        Route::resource('/propuesta', 'PropuestaController');
    });
});
