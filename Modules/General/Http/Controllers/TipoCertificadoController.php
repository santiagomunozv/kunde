<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\TipoCertificadoModel;


use Modules\General\Http\Requests\TipoCertificadoRequest;
use Modules\Seguridad\Entities\CompaniaModel;
use Session;

class TipoCertificadoController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipocertificado = TipoCertificadoModel::orderBy('oidTipoCertificado' , 'DESC')->get();
        return view('general::tipoCertificadoGrid', compact('permisos' , 'tipocertificado'));
    }

            
    public function create()
    {
        $tipocertificado = new TipoCertificadoModel();
        $tipocertificado->txEstadoTipoCertificado = 'Activo';
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

        
        
        return view('general::tipoCertificadoForm' , ['tipocertificado' => $tipocertificado,'compania'=>$compania] );
    }
            
    public function store(TipoCertificadoRequest $request)
    {
        DB::beginTransaction();
        try{
            $tipocertificado = TipoCertificadoModel::create([
                'txCodigoTipoCertificado'=>$request['txCodigoTipoCertificado'], 
                'txNombreTipoCertificado'=> $request['txNombreTipoCertificado'],
                'lsTipoTipoCertificado'=>$request['lsTipoTipoCertificado'],
                'txVigenciaTipoCertificado'=>$request['txVigenciaTipoCertificado'], 
                'Compania_oidCompania'=>Session::get('oidCompania'),
                'lmImpactoTipoCertificado'=>implode(',',$request['lmImpactoTipoCertificado']),
                'txEstadoTipoCertificado'=>$request['txEstadoTipoCertificado'],
            ]);
        	// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoCertificado' => $tipocertificado->oidTipoCertificado] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $tipocertificado = TipoCertificadoModel::find($id);
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');
        
        return view('general::tipoCertificadoForm' , ['tipocertificado' => $tipocertificado,'compania'=>$compania] );
    }
            

    public function update(TipoCertificadoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $indice = (['oidTipoCertificado'=>$request['oidTipoCertificado']]);
            $datos = ([
                'txCodigoTipoCertificado'=>$request['txCodigoTipoCertificado'], 
                'txNombreTipoCertificado'=> $request['txNombreTipoCertificado'],
                'lsTipoTipoCertificado'=>$request['lsTipoTipoCertificado'],
                'txVigenciaTipoCertificado'=>$request['txVigenciaTipoCertificado'], 
                'Compania_oidCompania'=>Session::get('oidCompania'),
                'lmImpactoTipoCertificado'=>implode(',',$request['lmImpactoTipoCertificado']),
                'txEstadoTipoCertificado'=>$request['txEstadoTipoCertificado'],
            ]);
            $tipocertificado = TipoCertificadoModel::updateOrCreate($indice,$datos);
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoCertificado' => $tipocertificado->oidTipoCertificado] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidTipoCertificado' => $tipocertificado->oidTipoCertificado] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
