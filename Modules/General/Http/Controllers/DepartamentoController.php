<?php 
namespace Modules\General\Http\Controllers; 

use App\Http\Controllers\Controller;
use Modules\General\Http\Requests\DepartamentoRequest;
use Modules\General\Entities\DepartamentoModel;
use Modules\General\Entities\PaisModel;
use Modules\General\Entities\CiudadModel;
use Modules\Saya\Http\Controllers\SayaTerceroController;

use DB;

class DepartamentoController extends SayaTerceroController{
    public function index(){
        $permisos = $this->consultarPermisos();
        $departamentos = DepartamentoModel::orderBy('oidDepartamento' , 'DESC')->get();
        if($permisos){
            return view('general::departamentoGrid', compact('permisos' , 'departamentos'));
        }else{
            return view('Seg_AccesoDenegadoForm');
        }
    }

    public function create(){
        $paises = PaisModel::where('gen_pais.txEstadoPais','=', 'Activo')->pluck('txNombrePais','oidPais');
        $departamento = new DepartamentoModel();
        $departamento->txEstadoDepartamento = 'Activo';
        $ciudades = $departamento->ciudades;
        return view('general::departamentoForm' , compact('paises' , 'departamento' , 'ciudades'));
    }

    public function store(DepartamentoRequest $request){
        DB::beginTransaction();
        try{
            $departamento = DepartamentoModel::create($request->all());
            $this->actualizarDepartamento($departamento->oidDepartamento);
            $this->grabarDetalle($request, $departamento->oidDepartamento);
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
        }
        return response(['oidDepartamento' => $departamento->oidDepartamento] , 200);
    }

    public function edit($id){
        $departamento = DepartamentoModel::find($id);
        $paises = PaisModel::where('gen_pais.txEstadoPais','=', 'Activo')->pluck('txNombrePais','oidPais');
        $ciudades = $departamento->ciudades;
        return view('general::departamentoForm', compact('paises' , 'departamento' , 'ciudades'));
    }
    /**
     * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(DepartamentoRequest $request, $id){
        DB::beginTransaction();
        $departamento = DepartamentoModel::find($id);
        $departamento->fill($request->all());
        try{
            $departamento->save();
            $this->actualizarDepartamento($departamento->oidDepartamento);
            $this->grabarDetalle($request, $departamento->oidDepartamento);
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
        return response(['oidDepartamento' => $departamento->oidDepartamento] , 200);
    }

    /**
     * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id){
        DepartamentoModel::destroy($id);
        return redirect('/gen_departamento');
    }

    protected function grabarDetalle($request, $id){
        $idsEliminar = explode(',', $request['eliminarCiudad']);
        CiudadModel::whereIn('oidCiudad', $idsEliminar)->delete();
        $idCiudades = $request['oidCiudad'];
        if($idCiudades){
            for($i = 0; $i < count($idCiudades); $i++){
                $indice = array('oidCiudad' => $request['oidCiudad'][$i]);
                $datos= [
                    'Departamento_oidDepartamento_1aM' => $id,
                    'txCodigoCiudad' => $request['txCodigoCiudad'][$i],
                    'txNombreCiudad' => $request['txNombreCiudad'][$i],
                    'txCodigoPostalCiudad' => $request['txCodigoPostalCiudad'][$i],
                ];
                $guardar = CiudadModel::updateOrCreate($indice, $datos);
                $this->actualizarCiudad($guardar->oidCiudad);  
            }
        }
    }
}