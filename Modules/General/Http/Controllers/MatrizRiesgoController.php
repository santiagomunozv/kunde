<?php 
namespace Modules\General\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\General\Entities\CargoModel;
use Modules\General\Entities\MatrizRiesgoModel;
use Modules\General\Http\Requests\MatrizRiesgoRequest;

class MatrizRiesgoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $matrizRiesgo = MatrizRiesgoModel::get();
        $nombreCargo = CargoModel::pluck('txNombreCargo');
        $idCargo = CargoModel::pluck('oidCargo');
        return view('general::matrizRiesgoForm', compact('matrizRiesgo', 'nombreCargo', 'idCargo'), ['matrizRiesgo' => $matrizRiesgo]);
    }
    /**
     * Update the specified resource in storage.
     * @param MatrizRiesgoRequest $request
     * @param int $id
     * @return Response
     */
    public function update(MatrizRiesgoRequest $request)
    {
        DB::beginTransaction();
        try{
            $modelo = new MatrizRiesgoModel();
            $idsEliminar = explode(',', $request['eliminarMatriz']);
            $modelo::whereIn('oidMatrizRiesgo',$idsEliminar)->delete();
            $total = ($request['oidMatrizRiesgo'] !== null ) ? count($request['oidMatrizRiesgo']) : 0;
            for($i = 0; $i <  $total; $i++)
            {
                $indice = array('oidMatrizRiesgo' => $request['oidMatrizRiesgo'][$i]);
                $datos = [
                        'Cargo_oidCargo'=>$request["Cargo_oidCargo"][$i],
                        'txActividadCargoMatrizRiesgo'=>$request["txActividadCargoMatrizRiesgo"][$i],
                        'lsRutinariaMatrizRiesgo'=>$request["lsRutinariaMatrizRiesgo"][$i],
                        'lsClasificacionMatrizRiesgo'=>$request["lsClasificacionMatrizRiesgo"][$i],
                        'txDescripcionClasificacionMatrizRiesgo'=>$request["txDescripcionClasificacionMatrizRiesgo"][$i],
                        'txEfectosPosiblesMatrizRiesgo'=>$request["txEfectosPosiblesMatrizRiesgo"][$i],
                        'txFuenteMatrizRiesgo'=>$request["txFuenteMatrizRiesgo"][$i],
                        'txMedioMatrizRiesgo'=>$request["txMedioMatrizRiesgo"][$i],
                        'txIndividuoMatrizRiesgo'=>$request["txIndividuoMatrizRiesgo"][$i],
                        'lsNivelDeficienciaMatrizRiesgo'=>$request["lsNivelDeficienciaMatrizRiesgo"][$i],
                        'lsNivelExposicionMatrizRiesgo'=>$request["lsNivelExposicionMatrizRiesgo"][$i],
                        'txNivelProbabilidadMatrzRiesgo'=>$request["txNivelProbabilidadMatrzRiesgo"][$i],
                        'txInterpretacionProbabilidadMatrizRiesgo'=>$request["txInterpretacionProbabilidadMatrizRiesgo"][$i],
                        'lsNivelConsecuenciaMatrizRiesgo'=>$request["lsNivelConsecuenciaMatrizRiesgo"][$i],
                        'txNivelRiesgoMatrizRiesgo'=>$request["txNivelRiesgoMatrizRiesgo"][$i],
                        'txInterpretacionRiesgoMatrizRiesgo'=>$request["txInterpretacionRiesgoMatrizRiesgo"][$i],
                        'txAceptabilidadRiesgoMatrizRiesgo'=>$request["txAceptabilidadRiesgoMatrizRiesgo"][$i],
                        'inExpuestosMatrizRiesgo'=>$request["inExpuestosMatrizRiesgo"][$i],
                        'txPeorConsecuenciaMatrizRiesgo'=>$request["txPeorConsecuenciaMatrizRiesgo"][$i],
                        'txEliminacionMatrizRiesgo'=>$request["txEliminacionMatrizRiesgo"][$i],
                        'txSustitucionMatrizRiesgo'=>$request["txSustitucionMatrizRiesgo"][$i],
                        'txControlIngenieria'=>$request["txControlIngenieria"][$i],
                        'txControlAdministrativoMatrizRiesgo'=>$request["txControlAdministrativoMatrizRiesgo"][$i],
                        'txElementosProteccionPersonalMatrizRiesgo'=>$request["txElementosProteccionPersonalMatrizRiesgo"][$i],
                    ];
                $modelo::updateOrCreate($indice, $datos);
            }
            DB::commit();
            return response([] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return response($e->getMessage());
        }
    }
}