<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Session;
use Modules\General\Entities\ProgramacionPlanTrabajoModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\General\Entities\ActividadPlanTrabajoDetalleModel;
use Modules\General\Entities\ActividadPlanTrabajoModel;
use Modules\General\Entities\ConfiguracionPlanTrabajoModel;
use Modules\General\Http\Requests\ActividadPlanTrabajoRequest;
use Modules\General\Http\Requests\ProgramacionPlanTrabajoRequest;

class ProgramacionPlanTrabajoController extends Controller
{

    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $programacionplantrabajo = ProgramacionPlanTrabajoModel::orderBy('oidProgramacionPlanTrabajo', 'DESC')
            ->selectRaw('TE.txNombreTercero as Encargado, TC.txNombreTercero as Compania, oidProgramacionPlanTrabajo')
            ->leftjoin('asn_tercero as TE', 'Tercero_oidTerceroEncargado', '=', 'TE.oidTercero')
            ->leftjoin('asn_tercero as TC', 'Tercero_oidTerceroCompania', '=', 'TC.oidTercero');

        if (Session::get('rolUsuario') != 1) {
            $programacionplantrabajo->where("Tercero_oidTerceroEncargado", Session::get('oidTercero'));
        }

        $programacionplantrabajo = $programacionplantrabajo->get();
        return view('general::programacionPlanTrabajoGrid', compact('permisos', 'programacionplantrabajo'));
    }


    public function create()
    {
        $configuracion = DB::select("SELECT
            null as oidActividadPlanTrabajo,
            oidConfiguracionPlanTrabajo as ConfiguracionPlanTrabajo_oidConfiguracionPlan,
            txActividadProgramadaConfiguracionPlanTrabajo,
            txMesConfiguracionPlanTrabajo,
            null as daFechaProgramadaActividadPlanTrabajo,
            null as txHorasProgramadasActividadPlanTrabajo,
            null as txCantidadHorasActividadPlanTrabajo
        FROM
            kunde.gen_configuracionplantrabajo
        WHERE chSugerirConfiguracionPlanTrabajo = 1");
        $programacionplantrabajo = new ProgramacionPlanTrabajoModel();
        $programacionplantrabajo->txEstadoProgramacionPlanTrabajo = 'Activo';
        // Consultamos los maestros necesarios para las listas de seleccion
        $asn_tercerolistaCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        return view('general::programacionPlanTrabajoForm', ['programacionplantrabajo' => $programacionplantrabajo], compact('asn_tercerolistaCliente', 'asn_tercerolistaEmpleado', 'configuracion'));
    }

    public function store(ProgramacionPlanTrabajoRequest $request)
    {
        DB::beginTransaction();
        try {
            $programacionplantrabajo = ProgramacionPlanTrabajoModel::create($request->all());
            $this->grabarDetalle($request, $programacionplantrabajo->oidProgramacionPlanTrabajo);
            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProgramacionPlanTrabajo' => $programacionplantrabajo->oidProgramacionPlanTrabajo], 201);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $configuracion = DB::select("SELECT
            oidActividadPlanTrabajo,
            oidConfiguracionPlanTrabajo as ConfiguracionPlanTrabajo_oidConfiguracionPlan,
            txActividadProgramadaConfiguracionPlanTrabajo,
            txMesConfiguracionPlanTrabajo,
            daFechaProgramadaActividadPlanTrabajo,
            txHorasProgramadasActividadPlanTrabajo,
            txCantidadHorasActividadPlanTrabajo
        FROM
            kunde.gen_actividadplantrabajo
                LEFT JOIN
            gen_configuracionplantrabajo ON ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo
            where ProgramacionPlanTrabajo_oidPlanTrabajo_1aM = $id");
        // $configuracion = ConfiguracionPlanTrabajoModel::select('oidConfiguracionPlanTrabajo','txActividadProgramadaConfiguracionPlanTrabajo')->get();
        $programacionplantrabajo = ProgramacionPlanTrabajoModel::find($id);
        // Consultamos los maestros necesarios para las listas de seleccion
        $asn_tercerolistaCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');

        return view('general::programacionPlanTrabajoForm', ['programacionplantrabajo' => $programacionplantrabajo], compact('asn_tercerolistaEmpleado', 'asn_tercerolistaCliente', 'configuracion'));
    }

    public function getActividadPlantrabajoDetalle($id)
    {
        $actividadPlanTrabajoDetalle = ActividadPlanTrabajoDetalleModel::where('ActividadPlanTrabajo_oidActividadPlanTrabajo', '=', $id)->get();

        $view = view('general::actividadPlanTrabajoForm', compact('id', 'actividadPlanTrabajoDetalle'))->render();
        return response(['view' => $view, 'data' => $actividadPlanTrabajoDetalle], 200);
    }

    public function updateActividad(ActividadPlanTrabajoRequest $request, $id)
    {
        $idsEliminarA = explode(',', $request['eliminarActividad']);
        ActividadPlanTrabajoDetalleModel::whereIn('oidActividadPlanTrabajoDetalle', $idsEliminarA)->delete();
        $total = ($request['oidActividadPlanTrabajoDetalle'] !== null) ? count($request['oidActividadPlanTrabajoDetalle']) : 0;
        for ($i = 0; $i < $total; $i++) {
            $indice = array('oidActividadPlanTrabajoDetalle' => $request['oidActividadPlanTrabajoDetalle'][$i]);
            $datos = array(
                'ActividadPlanTrabajo_oidActividadPlanTrabajo' => $id,
                'txNombreActividadPlanTrabajoDetalle' => $request['txNombreActividadPlanTrabajoDetalle'][$i],
            );
            ActividadPlanTrabajoDetalleModel::updateOrCreate($indice, $datos);
        }

        return response(200);
    }


    public function update(ProgramacionPlanTrabajoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $programacionplantrabajo = ProgramacionPlanTrabajoModel::find($id);
            $programacionplantrabajo->fill($request->all());
            $programacionplantrabajo->save();
            $this->grabarDetalle($request, $programacionplantrabajo->oidProgramacionPlanTrabajo);

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProgramacionPlanTrabajo' => $programacionplantrabajo->oidProgramacionPlanTrabajo], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }
    // En este método guardamos todas las tablas relacionadas
    protected function grabarDetalle($request, $id)
    {
        $modelo = new ActividadPlanTrabajoModel();
        $idsEliminar = explode(',', $request['eliminarProgramacion']);
        $modelo::whereIn('oidActividadPlanTrabajo', $idsEliminar)->delete();
        $datos = ($request['oidActividadPlanTrabajo'] !== null) ? count($request['oidActividadPlanTrabajo']) : 0;
        for ($i = 0; $i < $datos; $i++) {
            $oidConfiguracion = null;
            if (!$request['ConfiguracionPlanTrabajo_oidConfiguracionPlan'][$i]) {
                $oidConfiguracion = $this->adicionarActividadConfiguracion($request['txActividadProgramadaConfiguracionPlanTrabajo'][$i], $request['txMesConfiguracionPlanTrabajo'][$i]);
            }
            $indice = ['oidActividadPlanTrabajo' => $request['oidActividadPlanTrabajo'][$i]];
            $datosReg = [
                'ProgramacionPlanTrabajo_oidPlanTrabajo_1aM' => $id,
                'ConfiguracionPlanTrabajo_oidConfiguracionPlan' => $request["ConfiguracionPlanTrabajo_oidConfiguracionPlan"][$i] ? $request["ConfiguracionPlanTrabajo_oidConfiguracionPlan"][$i] : $oidConfiguracion,
                'daFechaProgramadaActividadPlanTrabajo' => $request["daFechaProgramadaActividadPlanTrabajo"][$i],
                'txHorasProgramadasActividadPlanTrabajo' => $request["txHorasProgramadasActividadPlanTrabajo"][$i],
                'txCantidadHorasActividadPlanTrabajo' => $request["txCantidadHorasActividadPlanTrabajo"][$i],
            ];
            ActividadPlanTrabajoModel::updateOrCreate($indice, $datosReg);
        }
    }

    private function adicionarActividadConfiguracion($actividad, $mes)
    {
        $configuracion = ConfiguracionPlanTrabajoModel::create([
            'txActividadProgramadaConfiguracionPlanTrabajo' => $actividad,
            'txMesConfiguracionPlanTrabajo' => $mes,
            'chSugerirConfiguracionPlanTrabajo' => 0
        ]);
        return $configuracion->oidConfiguracionPlanTrabajo;
    }
}
