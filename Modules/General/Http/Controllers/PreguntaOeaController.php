<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\PreguntaOeaModel;
use Modules\General\Entities\PreguntaOeaDetalleModel;


use Modules\General\Http\Requests\PreguntaOeaRequest;
use Modules\Seguridad\Entities\CompaniaModel;

class PreguntaOeaController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $preguntaoea = PreguntaOeaModel::orderBy('oidPreguntaOea' , 'DESC')->get();
        return view('general::preguntaOeaGrid', compact('permisos' , 'preguntaoea'));
    }

            
    public function create()
    {
        $preguntaoea = new PreguntaOeaModel();
        $preguntaoea->txEstadoPreguntaOea = 'Activo';
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');        
        
        return view('general::preguntaOeaForm' , ['preguntaoea' => $preguntaoea,'compania'=>$compania] );
    }
            
    public function store(PreguntaOeaRequest $request)
    {
        DB::beginTransaction();
        try{
            $preguntaoea = PreguntaOeaModel::create($request->all());
        	$this->grabarDetalle($request, $preguntaoea->oidPreguntaOea);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPreguntaOea' => $preguntaoea->oidPreguntaOea] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $preguntaoea = PreguntaOeaModel::find($id);
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');        

        
        return view('general::preguntaOeaForm' , ['preguntaoea' => $preguntaoea, 'compania'=>$compania] );
    }
            

    public function update(PreguntaOeaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $preguntaoea = PreguntaOeaModel::find($id);
            $preguntaoea->fill($request->all());
            $preguntaoea->save();
        	$this->grabarDetalle($request, $preguntaoea->oidPreguntaOea);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPreguntaOea' => $preguntaoea->oidPreguntaOea] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidPreguntaOea' => $preguntaoea->oidPreguntaOea] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarPreguntaOeaDetalle']);
        PreguntaOeaDetalleModel::whereIn('oidPreguntaOeaDetalle',$idsEliminar)->delete();
        $valores = ($request['oidPreguntaOeaDetalle'] ? count($request['oidPreguntaOeaDetalle']) : 0);
        // recorremos cada registro del detalle para guardarlos en la tabla
        for($i = 0; $i < $valores; $i++)
        {    
            // creamos un array con el campo oid autonumerico
            $indice = array(
                'oidPreguntaOeaDetalle' => $request['oidPreguntaOeaDetalle'][$i]);
            // En otro array guardamos los demas campos fillables
            $datos= array(
                'PreguntaOea_oidPreguntaOea_1aM' => $id,
				'inCalificacionPreguntaOeaDetalle' => $request['inCalificacionPreguntaOeaDetalle'][$i],
				'txTextoPreguntaOeaDetalle' => $request['txTextoPreguntaOeaDetalle'][$i],
				'txObservacionPreguntaOeaDetalle' => $request['txObservacionPreguntaOeaDetalle'][$i],
				'daVigenciaPreguntaOeaDetalle' => $request['daVigenciaPreguntaOeaDetalle'][$i],
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            $guardar = PreguntaOeaDetalleModel::updateOrCreate($indice, $datos);
        }


    }}
