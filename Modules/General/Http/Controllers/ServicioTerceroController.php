<?php

namespace Modules\General\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Modules\General\Entities\Servicio;

class ServicioTerceroController extends Controller
{

    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $servicios = Servicio::orderBy('created_at', 'DESC')->get();
        return view('general::tipoServiciosGrid', compact('permisos', 'servicios'));
    }

    public function create()
    {
        $servicios = new Servicio();
        return view('general::tipoServiciosForm', compact('servicios'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'txNombre' => 'required',
            'chEstado' => 'required'
        ]);

        Servicio::create($data);
        return response(['data' => $data], 201);
    }

    public function edit(Servicio $tipodeservicio)
    {
        return view('general::tipoServiciosForm', ['servicios' => $tipodeservicio]);
    }

    public function update(Request $request, Servicio $tipodeservicio)
    {
        $data = $request->validate([
            'txNombre' => 'required',
            'chEstado' => 'required'
        ]);

        if ($tipodeservicio->fill($data)->isDirty()) {
            $tipodeservicio->save();
            return response()->json(200);
        } else {
            throw new Exception('No hay campos que modificar');
        }
    }

    public function destroy($id)
    {
        Servicio::whereId($id)->delete();
        return response()->json(200);
    }
}
