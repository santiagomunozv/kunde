<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\CentroTrabajoModel;
use Modules\Seguridad\Entities\CompaniaModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;


use Modules\General\Http\Requests\CentroTrabajoRequest;

class CentroTrabajoController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $centrotrabajo = CentroTrabajoModel::orderBy('oidCentroTrabajo' , 'DESC')->get();
        return view('general::centroTrabajoGrid', compact('permisos' , 'centrotrabajo'));
    }

            
    public function create()
    {
        $centrotrabajo = new CentroTrabajoModel();
        $centrotrabajo->txEstadoCentroTrabajo = 'Activo';
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');
        // Consultamos los maestros necesarios para las listas de seleccion
        $asn_tercerolista = TerceroModel::findByTipoCliente(['Proveedor','Cliente'],['oidTercero','txNombreTercero'])->pluck('txNombreTercero','oidTercero');

			
        
        return view('general::centroTrabajoForm' , ['centrotrabajo' => $centrotrabajo,'compania'=>$compania] , compact('asn_tercerolista'));
    }
            
    public function store(CentroTrabajoRequest $request)
    {
        DB::beginTransaction();
        try{
            $centrotrabajo = CentroTrabajoModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCentroTrabajo' => $centrotrabajo->oidCentroTrabajo] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $centrotrabajo = CentroTrabajoModel::find($id);
        // Consultamos los maestros necesarios para las listas de seleccion
        $asn_tercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('txNombreTercero','oidTercero');
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

			
        return view('general::centroTrabajoForm' , ['centrotrabajo' => $centrotrabajo,'compania'=>$compania] , compact('asn_tercerolista'));
    }
            

    public function update(CentroTrabajoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $centrotrabajo = CentroTrabajoModel::find($id);
            $centrotrabajo->fill($request->all());
            $centrotrabajo->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCentroTrabajo' => $centrotrabajo->oidCentroTrabajo] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidCentroTrabajo' => $centrotrabajo->oidCentroTrabajo] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
