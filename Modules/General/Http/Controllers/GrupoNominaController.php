<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\GrupoNominaModel;


use Modules\General\Http\Requests\GrupoNominaRequest;

class GrupoNominaController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $gruponomina = GrupoNominaModel::orderBy('oidGrupoNomina' , 'DESC')->get();
        return view('general::grupoNominaGrid', compact('permisos' , 'gruponomina'));
    }

            
    public function create()
    {
        $gruponomina = new GrupoNominaModel();
        $gruponomina->txEstadoGrupoNomina = 'Activo';
        
        
        return view('general::grupoNominaForm' , ['gruponomina' => $gruponomina] );
    }
            
    public function store(GrupoNominaRequest $request)
    {
        DB::beginTransaction();
        try{
            $gruponomina = GrupoNominaModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidGrupoNomina' => $gruponomina->oidGrupoNomina] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidGrupoNomina' => $gruponomina->oidGrupoNomina] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $gruponomina = GrupoNominaModel::find($id);
        
        return view('general::grupoNominaForm' , ['gruponomina' => $gruponomina] );
    }
            

    public function update(GrupoNominaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $gruponomina = GrupoNominaModel::find($id);
            $gruponomina->fill($request->all());
            $gruponomina->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidGrupoNomina' => $gruponomina->oidGrupoNomina] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidGrupoNomina' => $gruponomina->oidGrupoNomina] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
