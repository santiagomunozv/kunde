<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\NaturalezaJuridicaModel;
use Modules\Saya\Http\Controllers\SayaTerceroController;



use Modules\General\Http\Requests\NaturalezaJuridicaRequest;

class NaturalezaJuridicaController extends SayaTerceroController
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $naturalezajuridica = NaturalezaJuridicaModel::orderBy('oidNaturalezaJuridica' , 'DESC')->get();
        return view('general::naturalezaJuridicaGrid', compact('permisos' , 'naturalezajuridica'));
    }

            
    public function create()
    {
        $naturalezajuridica = new NaturalezaJuridicaModel();
        $naturalezajuridica->txEstadoNaturalezaJuridica = 'Activo';
        
        
        return view('general::naturalezaJuridicaForm' , ['naturalezajuridica' => $naturalezajuridica] );
    }
            
    public function store(NaturalezaJuridicaRequest $request)
    {
        DB::beginTransaction();
        try{
            $naturalezajuridica = NaturalezaJuridicaModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            $this->actualizarNaturalezaJuridica($naturalezajuridica->oidNaturalezaJuridica);
            return response(['id'=>$naturalezajuridica->oidNaturalezaJuridica], 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $naturalezajuridica = NaturalezaJuridicaModel::find($id);
        
        return view('general::naturalezaJuridicaForm' , ['naturalezajuridica' => $naturalezajuridica] );
    }
            

    public function update(NaturalezaJuridicaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $naturalezajuridica = NaturalezaJuridicaModel::find($id);
            $naturalezajuridica->fill($request->all());
            $naturalezajuridica->save();
            $this->actualizarNaturalezaJuridica($naturalezajuridica->oidNaturalezaJuridica);
        	// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidNaturalezaJuridica' => $naturalezajuridica->oidNaturalezaJuridica] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidNaturalezaJuridica' => $naturalezajuridica->oidNaturalezaJuridica] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
