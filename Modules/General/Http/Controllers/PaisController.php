<?php 
namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request; 
use App\Http\Requests; 
use App\Http\Controllers\Controller;
use Modules\Saya\Http\Controllers\SayaTerceroController;


use Modules\General\Entities\PaisModel;
use Modules\General\Http\Requests\PaisRequest;

class PaisController extends SayaTerceroController
{

    public function index(){
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $paises = PaisModel::orderBy('oidPais' , 'DESC')->get();
        return view('general::paisGrid', compact('permisos' , 'paises'));
    }

    public function create(){
        $pais = new PaisModel();
        $pais->txEstadoPais = 'Activo';
        return view('general::paisForm' , compact('pais'));
    }

    public function store(PaisRequest $request){
        $pais = PaisModel::create($request->all());
        $this->actualizarPais($pais->oidPais);
        return response(['mensaje' => 'Datos guardados correctamente.'] , 201);
    }

    public function edit($id){
        $pais = PaisModel::find($id);
        return view('general::paisForm', compact('pais'));
    }

    public function update(PaisRequest $request, $id){
        $pais = PaisModel::find($id);
        $pais->fill($request->all());
        $pais->save(); 
        $this->actualizarPais($pais->oidPais);
        return response(200);
    }

    public function destroy($id){
        PaisModel::destroy($id);
        return redirect('/pais');
    }
}