<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\FormaPagoModel;
use Modules\Saya\Http\Controllers\SayaTerceroController;



use Modules\General\Http\Requests\FormaPagoRequest;

class FormaPagoController extends SayaTerceroController
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $formapago = FormaPagoModel::orderBy('oidFormaPago' , 'DESC')->get();
        return view('general::formaPagoGrid', compact('permisos' , 'formapago'));
    }

            
    public function create()
    {
        $formapago = new FormaPagoModel();
        $formapago->txEstadoFormaPago = 'Activo';
        
        
        return view('general::formaPagoForm' , ['formapago' => $formapago] );
    }
            
    public function store(FormaPagoRequest $request)
    {
        DB::beginTransaction();
        try{
            $formapago = FormaPagoModel::create($request->all());
            $this->actualizarFormaPago($formapago->oidFormaPago);
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidFormaPago' => $formapago->oidFormaPago] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $formapago = FormaPagoModel::find($id);
        
        return view('general::formaPagoForm' , ['formapago' => $formapago] );
    }
            

    public function update(FormaPagoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $formapago = FormaPagoModel::find($id);
            $formapago->fill($request->all());
            $formapago->save();
            $this->actualizarFormaPago($formapago->oidFormaPago);
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidFormaPago' => $formapago->oidFormaPago] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidFormaPago' => $formapago->oidFormaPago] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
