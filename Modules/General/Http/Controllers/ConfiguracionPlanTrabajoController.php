<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Modules\General\Entities\ConfiguracionPlanTrabajoModel;

class ConfiguracionPlanTrabajoController extends Controller
{
            
    public function index()
    {
        $plan = ConfiguracionPlanTrabajoModel::select("oidConfiguracionPlanTrabajo","txActividadProgramadaConfiguracionPlanTrabajo","txMesConfiguracionPlanTrabajo", "chSugerirConfiguracionPlanTrabajo")
        ->orderBy("txMesConfiguracionPlanTrabajo", "asc")
        ->get();
        
        return view('general::configuracionPlanTrabajoForm' , ['plan' => $plan] );
    }
            
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $modelo = new ConfiguracionPlanTrabajoModel();
            $idsEliminar = explode(',', $request['eliminarConfiguracion']);
            $modelo::whereIn('oidConfiguracionPlanTrabajo',$idsEliminar)->delete();
            for($i = 0; $i <  count($request['oidConfiguracionPlanTrabajo']); $i++)
            {
                $indice = array('oidConfiguracionPlanTrabajo' => $request['oidConfiguracionPlanTrabajo'][$i]);
                $datos = [
                        'txActividadProgramadaConfiguracionPlanTrabajo'=>$request["txActividadProgramadaConfiguracionPlanTrabajo"][$i],
                        'txMesConfiguracionPlanTrabajo'=>$request["txMesConfiguracionPlanTrabajo"][$i],
                        'chSugerirConfiguracionPlanTrabajo'=>$request["chSugerirConfiguracionPlanTrabajo"][$i],
                    ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }
            DB::commit();
            return response(['oidConfiguracionPlanTrabajo' => $guardar->oidConfiguracionPlanTrabajo] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

}
