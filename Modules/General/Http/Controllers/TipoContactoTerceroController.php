<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Modules\General\Entities\ClasificacionTerceroModel;
use Modules\General\Entities\TipoContactoTerceroModel;



class TipoContactoTerceroController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipoContacto = TipoContactoTerceroModel::orderBy('oidTipoContactoTercero' , 'DESC')->get();
        return view('general::TipoContactoTerceroGrid', compact('permisos' , 'tipoContacto'));
    }

            
    public function create()
    {
        $tipoContacto = new TipoContactoTerceroModel();
        $tipoContacto->txEstadoTipoContactoTercero = 'Activo';
        $clasificacionTercero = ClasificacionTerceroModel::pluck('txNombreClasificacionTercero','oidClasificacionTercero');
        
        
        return view('general::TipoContactoTerceroForm' , ['tipoContacto' => $tipoContacto,'clasificacionTercero'=>$clasificacionTercero] );
    }
            
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $tipoContacto = TipoContactoTerceroModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoContactoTercero' => $tipoContacto->oidTipoContactoTercero] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['error' =>$e] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $tipoContacto = TipoContactoTerceroModel::find($id);
        $clasificacionTercero = ClasificacionTerceroModel::pluck('txNombreClasificacionTercero','oidClasificacionTercero');

        
        return view('general::TipoContactoTerceroForm' , ['tipoContacto' => $tipoContacto,'clasificacionTercero'=>$clasificacionTercero] );
    }
            

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $tipoContacto = TipoContactoTerceroModel::find($id);
            $tipoContacto->fill($request->all());
            $tipoContacto->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoContactoTercero' => $tipoContacto->oidTipoContactoTercero] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidTipoContactoTercero' => $tipoContacto->oidTipoContactoTercero] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
