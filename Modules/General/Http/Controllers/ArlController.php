<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\ArlModel;


use Modules\General\Http\Requests\ArlRequest;

class ArlController extends Controller
{      
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $arl = ArlModel::orderBy('oidArl' , 'DESC')->get();
        return view('general::arlGrid', compact('permisos' , 'arl'));
    }
   
    public function create()
    {
        $arl = new ArlModel();
        $arl->txEstadoArl = 'Activo';
        
        return view('general::arlForm' , ['arl' => $arl] );
    }
            
    public function store(ArlRequest $request)
    {
        DB::beginTransaction();
        try{
            $arl = ArlModel::create($request->all());
            DB::commit();
            return response(['oidArl' => $arl->oidArl] , 201);
        }catch(\Exception $e){
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }
            
    public function edit($id)
    {
        return view('general::arlForm' , ['arl' => ArlModel::find($id)] );
    }
            
    public function update(ArlRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $arl = ArlModel::find($id);
            $arl->fill($request->all());
            $arl->save();
            DB::commit();
            return response(['oidArl' => $arl->oidArl] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return response(['oidArl' => $arl->oidArl] , 500);
        }
    }
}
