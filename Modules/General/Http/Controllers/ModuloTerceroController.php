<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\ModuloTerceroModel;
use Modules\General\Entities\ModuloTerceroCorreoModel;


use Modules\General\Http\Requests\moduloTerceroRequest;

class ModuloTerceroController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $modulotercero = ModuloTerceroModel::orderBy('oidModuloTercero' , 'DESC')->get();
        return view('general::moduloTerceroGrid', compact('permisos' , 'modulotercero'));
    }

            
    public function create()
    {
        $modulotercero = new ModuloTerceroModel();
        $modulotercero->txEstadoModuloTercero = 'Activo';
        
        
        return view('general::moduloTerceroForm' , ['modulotercero' => $modulotercero] );
    }
            
    public function store(moduloTerceroRequest $request)
    {
        DB::beginTransaction();
        try{
            $modulotercero = ModuloTerceroModel::create($request->all());
        

				ModuloTerceroCorreoModel::create([$request->all()]);

			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidModuloTercero' => $modulotercero->oidModuloTercero] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidModuloTercero' => $modulotercero->oidModuloTercero] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $modulotercero = ModuloTerceroModel::find($id);
        
        return view('general::moduloTerceroForm' , ['modulotercero' => $modulotercero] );
    }
            

    public function update(moduloTerceroRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $modulotercero = ModuloTerceroModel::find($id);
            $modulotercero->fill($request->all());
            $modulotercero->save();
        

			$modulotercerocorreo = ModuloTerceroCorreoModel::find($id);
            $modulotercerocorreo->fill($request->all());
            $modulotercerocorreo->save();
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidModuloTercero' => $modulotercero->oidModuloTercero] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidModuloTercero' => $modulotercero->oidModuloTercero] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
