<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Saya\Http\Controllers\SayaTerceroController;
use Modules\General\Entities\TipoIdentificacionModel;


use Modules\General\Http\Requests\TipoIdentificacionRequest;

class TipoIdentificacionController extends SayaTerceroController
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipoidentificacion = TipoIdentificacionModel::orderBy('oidTipoIdentificacion' , 'DESC')->get();
        return view('general::tipoIdentificacionGrid', compact('permisos' , 'tipoidentificacion'));
    }

            
    public function create()
    {
        $tipoidentificacion = new TipoIdentificacionModel();
        $tipoidentificacion->txEstadoTipoIdentificacion = 'Activo';
        
        
        return view('general::tipoIdentificacionForm' , ['tipoidentificacion' => $tipoidentificacion] );
    }
            
    public function store(TipoIdentificacionRequest $request)
    {
        DB::beginTransaction();
        try{
            $tipoidentificacion = TipoIdentificacionModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            $this->actualizarTipoIdentificacion($tipoidentificacion->oidTipoIdentificacion);
            return response(['id'=>$tipoidentificacion->oidTipoIdentificacion], 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $tipoidentificacion = TipoIdentificacionModel::find($id);
        
        return view('general::tipoIdentificacionForm' , ['tipoidentificacion' => $tipoidentificacion] );
    }
            

    public function update(TipoIdentificacionRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $tipoidentificacion = TipoIdentificacionModel::find($id);
            $tipoidentificacion->fill($request->all());
            $tipoidentificacion->save();
            $this->actualizarTipoIdentificacion($tipoidentificacion->oidTipoIdentificacion);

        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoIdentificacion' => $tipoidentificacion->oidTipoIdentificacion] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidTipoIdentificacion' => $tipoidentificacion->oidTipoIdentificacion] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
