<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\CentroCostoModel;


use Modules\General\Http\Requests\CentroCostoRequest;
use Modules\Seguridad\Entities\CompaniaModel;

class CentroCostoController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $centrocosto = CentroCostoModel::orderBy('oidCentroCosto' , 'DESC')->get();
        return view('general::centroCostoGrid', compact('permisos' , 'centrocosto'));
    }

            
    public function create()
    {
        $centrocosto = new CentroCostoModel();
        $centrocosto->txEstadoCentroCosto = 'Activo';

        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');
        
        
        return view('general::centroCostoForm' , ['centrocosto' => $centrocosto,'compania'=>$compania] );
    }
            
    public function store(CentroCostoRequest $request)
    {
        DB::beginTransaction();
        try{
            $centrocosto = CentroCostoModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCentroCosto' => $centrocosto->oidCentroCosto] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $centrocosto = CentroCostoModel::find($id);
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');
        
        return view('general::centroCostoForm' , ['centrocosto' => $centrocosto,'compania'=>$compania] );
    }
            

    public function update(CentroCostoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $centrocosto = CentroCostoModel::find($id);
            $centrocosto->fill($request->all());
            $centrocosto->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCentroCosto' => $centrocosto->oidCentroCosto] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidCentroCosto' => $centrocosto->oidCentroCosto] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
