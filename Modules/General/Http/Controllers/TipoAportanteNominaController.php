<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\TipoAportanteNominaModel;
use Modules\Saya\Http\Controllers\SayaTerceroController;



use Modules\General\Http\Requests\TipoAportanteNominaRequest;

class TipoAportanteNominaController extends SayaTerceroController
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipoaportantenomina = TipoAportanteNominaModel::orderBy('oidTipoAportanteNomina' , 'DESC')->get();
        return view('general::tipoAportanteNominaGrid', compact('permisos' , 'tipoaportantenomina'));
    }

            
    public function create()
    {
        $tipoaportantenomina = new TipoAportanteNominaModel();
        $tipoaportantenomina->txEstadoTipoAportanteNomina = 'Activo';
        
        
        return view('general::tipoAportanteNominaForm' , ['tipoaportantenomina' => $tipoaportantenomina] );
    }
            
    public function store(TipoAportanteNominaRequest $request)
    {
        DB::beginTransaction();
        try{
            $tipoaportantenomina = TipoAportanteNominaModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            $this->actualizarTipoAportanteNomina($tipoaportantenomina->oidTipoAportanteNomina);       

            return response(['id'=>$tipoaportantenomina->oidTipoAportanteNomina], 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $tipoaportantenomina = TipoAportanteNominaModel::find($id);
        
        return view('general::tipoAportanteNominaForm' , ['tipoaportantenomina' => $tipoaportantenomina] );
    }
            

    public function update(TipoAportanteNominaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $tipoaportantenomina = TipoAportanteNominaModel::find($id);
            $tipoaportantenomina->fill($request->all());
            $tipoaportantenomina->save();
            $this->actualizarTipoAportanteNomina($tipoaportantenomina->oidTipoAportanteNomina);       
        	// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoAportanteNomina' => $tipoaportantenomina->oidTipoAportanteNomina] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidTipoAportanteNomina' => $tipoaportantenomina->oidTipoAportanteNomina] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
