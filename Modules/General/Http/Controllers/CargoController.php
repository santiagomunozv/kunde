<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\CargoModel;


use Modules\General\Http\Requests\CargoRequest;

class CargoController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $cargo = CargoModel::orderBy('oidCargo' , 'DESC')->get();
        return view('general::cargoGrid', compact('permisos' , 'cargo'));
    }
   
    public function create()
    {
        $cargo = new CargoModel();
        $cargo->txEstadoCargo = 'Activo';
        
        return view('general::cargoForm' , ['cargo' => $cargo] );
    }
            
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $cargo = CargoModel::create($request->all());
            DB::commit();
            return response(['oidCargo' => $cargo->oidCargo] , 201);
        }catch(\Exception $e){
            DB::rollback();
            return response(['oidCargo' => $cargo->oidCargo] , 500);
        }   
    }
            
       
    public function edit($id)
    {
        $cargo = CargoModel::find($id);
        return view('general::cargoForm' , ['cargo' => $cargo] );
    }
            

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $cargo = CargoModel::find($id);
            $cargo->fill($request->all());
            $cargo->save();
            DB::commit();
            return response(['oidCargo' => $cargo->oidCargo] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return response(['oidCargo' => $cargo->oidCargo] , 500);
        }
    }
}
