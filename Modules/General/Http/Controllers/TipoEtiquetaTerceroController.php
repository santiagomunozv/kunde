<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\TipoEtiquetaTerceroModel;


use Modules\General\Http\Requests\TipoEtiquetaTerceroRequest;
use Modules\Seguridad\Entities\CompaniaModel;

class TipoEtiquetaTerceroController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipoetiquetatercero = TipoEtiquetaTerceroModel::orderBy('oidTipoEtiquetaTercero' , 'DESC')->get();
        return view('general::tipoEtiquetaTerceroGrid', compact('permisos' , 'tipoetiquetatercero'));
    }

            
    public function create()
    {
        $tipoetiquetatercero = new TipoEtiquetaTerceroModel();
        $tipoetiquetatercero->txEstadoTipoEtiquetaTercero = 'Activo';
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

        
        
        return view('general::tipoEtiquetaTerceroForm' , ['tipoetiquetatercero' => $tipoetiquetatercero,'compania'=>$compania] );
    }
            
    public function store(TipoEtiquetaTerceroRequest $request)
    {
        DB::beginTransaction();
        try{
            $tipoetiquetatercero = TipoEtiquetaTerceroModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoEtiquetaTercero' => $tipoetiquetatercero->oidTipoEtiquetaTercero] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $tipoetiquetatercero = TipoEtiquetaTerceroModel::find($id);
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

        
        return view('general::tipoEtiquetaTerceroForm' , ['tipoetiquetatercero' => $tipoetiquetatercero,'compania'=>$compania] );
    }
            

    public function update(TipoEtiquetaTerceroRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $tipoetiquetatercero = TipoEtiquetaTerceroModel::find($id);
            $tipoetiquetatercero->fill($request->all());
            $tipoetiquetatercero->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoEtiquetaTercero' => $tipoetiquetatercero->oidTipoEtiquetaTercero] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidTipoEtiquetaTercero' => $tipoetiquetatercero->oidTipoEtiquetaTercero] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
