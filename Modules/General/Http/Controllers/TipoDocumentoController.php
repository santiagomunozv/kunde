<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Modules\General\Entities\TipoDocumentoModel;
use Modules\General\Http\Requests\TipoDocumentoRequest;

class TipoDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $tipoDocumento = TipoDocumentoModel::select("oidTipoDocumento", "txCodigoTipoDocumento", "txNombreTipoDocumento")->orderBy('oidTipoDocumento' , 'DESC')->get();
        return view('general::tipoDocumentoGrid', compact('permisos' , 'tipoDocumento'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $tipoDocumento = new TipoDocumentoModel();
        return view('general::tipoDocumentoForm', ['tipoDocumento' => $tipoDocumento] );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(TipoDocumentoRequest $request)
    {
        DB::beginTransaction();
        try{
            $tipoDocumento = TipoDocumentoModel::create($request->all());
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidTipoDocumento' => $tipoDocumento->oidTipoDocumento] , 201);
        }
        catch(\Exception $e){
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('tipodocumento::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $tipoDocumento = TipoDocumentoModel::find($id);
        
        return view('general::tipoDocumentoForm' , ['tipoDocumento' => $tipoDocumento] );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(TipoDocumentoRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $tipoDocumento = TipoDocumentoModel::find($id);
            $tipoDocumento->fill($request->all());
            $tipoDocumento->save();
			
            DB::commit();
            return response(['oidTipoDocumento' => $tipoDocumento->oidTipoDocumento] , 200);
        }catch(\Exception $e){
           
            DB::rollback();
            return abort(500, $e->getMessage());
    
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        TipoDocumentoModel::destroy($id); 
        return redirect('/general/tipodocumento');
    }
}
