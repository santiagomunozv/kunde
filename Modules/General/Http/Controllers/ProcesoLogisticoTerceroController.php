<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\ProcesoLogisticoTerceroModel;


use Modules\General\Http\Requests\ProcesoLogisticoTerceroRequest;
use Modules\Seguridad\Entities\CompaniaModel;

class ProcesoLogisticoTerceroController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $procesologisticotercero = ProcesoLogisticoTerceroModel::orderBy('oidProcesoLogisticoTercero' , 'DESC')->get();
        return view('general::procesoLogisticoTerceroGrid', compact('permisos' , 'procesologisticotercero'));
    }

            
    public function create()
    {
        $procesologisticotercero = new ProcesoLogisticoTerceroModel();
        $procesologisticotercero->txEstadoProcesoLogisticoTercero = 'Activo';
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

        
        
        return view('general::procesoLogisticoTerceroForm' , ['procesologisticotercero' => $procesologisticotercero,'compania'=>$compania] );
    }
            
    public function store(ProcesoLogisticoTerceroRequest $request)
    {
        DB::beginTransaction();
        try{
            $procesologisticotercero = ProcesoLogisticoTerceroModel::create($request->all());
        			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProcesoLogisticoTercero' => $procesologisticotercero->oidProcesoLogisticoTercero] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $procesologisticotercero = ProcesoLogisticoTerceroModel::find($id);
        $compania = CompaniaModel::pluck('txNombreCompania','oidCompania');

        
        return view('general::procesoLogisticoTerceroForm' , ['procesologisticotercero' => $procesologisticotercero,'compania'=>$compania] );
    }
            

    public function update(ProcesoLogisticoTerceroRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $procesologisticotercero = ProcesoLogisticoTerceroModel::find($id);
            $procesologisticotercero->fill($request->all());
            $procesologisticotercero->save();
        			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidProcesoLogisticoTercero' => $procesologisticotercero->oidProcesoLogisticoTercero] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidProcesoLogisticoTercero' => $procesologisticotercero->oidProcesoLogisticoTercero] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        
    }}
