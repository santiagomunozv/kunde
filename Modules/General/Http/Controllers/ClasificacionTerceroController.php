<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\seg_RolModel;
use Modules\General\Entities\ClasificacionTerceroModel;
use Modules\General\Entities\ModuloTerceroModel;
use Modules\General\Entities\ClasificacionTerceroRolModel;
use Modules\General\Entities\ClasificacionTerceroGrupoModel;
use Modules\General\Entities\ClasificacionTerceroConfigModel;
use Modules\General\Entities\ModuloTerceroCorreoModel;
use DB;

class ClasificacionTerceroController extends Controller
{
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $clasificaciones = ClasificacionTerceroModel::orderBy('oidClasificacionTercero','desc')->get();
        return view('general::ClasificacionTerceroGrid', compact('permisos','clasificaciones'));
    }

    public function create()
    {   
        $config = [];
        $grupos = [];
        $idRol = seg_RolModel::All()->pluck('oidRol');
        $nombreRol = seg_RolModel::All()->pluck('txNombreRol');
        return view('general::ClasificacionTerceroForm', compact('grupos','idRol','nombreRol','config'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $clasificacionTercero = new ClasificacionTerceroModel();
            $clasificacionTercero->fill($request->all());
            $clasificacionTercero->lmTipoClasificacionTercero = implode(',',$request['lmTipoClasificacionTercero']);
            $clasificacionTercero->save();
            $idClasificacion = $clasificacionTercero->oidClasificacionTercero;
            $this->grabarGrupo($request,$idClasificacion);
            $this->grabarConfiguracion($request,$idClasificacion); 
            DB::commit();
            return response(['oidClasificacionTercero' => $idClasificacion] , 200);
        }catch(\Exception $exception){
            DB::rollback();
            return response(['oidClasificacionTercero' => $idClasificacion] , 500);
        }
    }

    public function edit($id)
    {
        $idRol = seg_RolModel::All()->pluck('oidRol');
        $nombreRol = seg_RolModel::All()->pluck('txNombreRol');

        $grupos = ClasificacionTerceroGrupoModel::where('ClasificacionTercero_oidClasificacionTercero_1aM','=',$id)
        ->select('oidClasificacionTerceroGrupo','txNombreClasificacionTerceroGrupo','chMultipleClasificacionTerceroGrupo','chObligatorioClasificacionTerceroGrupo','chCorreoClasificacionTerceroGrupo')->get();

        $clasificacion = ClasificacionTerceroModel::find($id);

        $config = ClasificacionTerceroConfigModel::select('oidClasificacionTerceroConfig','ModuloTercero_ClasificacionTerceroConfig','txNombreModuloTercero',
        'chCorreoClasificacionTerceroConfig','oidModuloTerceroCorreo','txParaModuloTerceroCorreo','txCopiaModuloTerceroCorreo','txAsuntoModuloTerceroCorreo',
        'txMensajeModuloTerceroCorreo')
        ->leftJoin('gen_modulotercerocorreo',function($join){
            $join->on('ModuloTercero_ClasificacionTerceroConfig','=','ModuloTercero_oidModuloTercero_1a1')
            ->on('gen_clasificacionterceroconfig.ClasificacionTercero_oidClasificacionTercero_1aM','=','gen_modulotercerocorreo.ClasificacionTercero_oidClasificacionTercero_1aM');
        })->leftJoin('gen_modulotercero','ModuloTercero_ClasificacionTerceroConfig','=','oidModuloTercero')
        ->where('gen_clasificacionterceroconfig.ClasificacionTercero_oidClasificacionTercero_1aM','=',$id)->get();

        return view('general::ClasificacionTerceroForm', compact('clasificacion','grupos','idRol','nombreRol','config'));
    }
    

    public function update(Request $request, $id)
    {  
        DB::beginTransaction();
        try{
            $clasificacionTercero = ClasificacionTerceroModel::find($id);
            $clasificacionTercero->fill($request->all());
            $clasificacionTercero->lmTipoClasificacionTercero = implode(',',$request["lmTipoClasificacionTercero"]);
            $clasificacionTercero->save();
            $this->grabarGrupo($request,$id);
            $this->grabarConfiguracion($request,$id);
            DB::commit();
            return response(['oidClasificacionTercero' => $id] , 200);
        }catch(\Exception $e){
            DB::rollback();
            return response(['oidClasificacionTercero' => $id] , 500);
        }
    }

    public function listarModuloTercero(){
        $modulos = ModuloTerceroModel::select('oidModuloTercero','txNombreModuloTercero')->get();

        return view('general::listaModuloTercero',compact('modulos'));
    }

    public function listaRol(){

        return view('general::clasificacionTerceroRol');
    }

    public function listaCorreo(){

        return view('general::clasificacionTerceroCorreo');
    }

    public function grabarGrupo($request,$id){
        
        $grupo = new ClasificacionTerceroGrupoModel();

        $grupo::whereIn($grupo->getKeyName(),explode(',', $request["eliminarGrupo"]))->delete();

        if(isset($request[$grupo->getKeyName()])){
            for($i = 0; $i < count($request[$grupo->getKeyName()]); $i++)
            {
                $indice = array($grupo->getKeyName() => $request[$grupo->getKeyName()][$i]);
                $datos = ['ClasificacionTercero_oidClasificacionTercero_1aM'=> $id,
                    'txNombreClasificacionTerceroGrupo'=>$request["txNombreClasificacionTerceroGrupo"][$i],
                    'chMultipleClasificacionTerceroGrupo'=>$request["chMultipleClasificacionTerceroGrupo"][$i],
                    'chObligatorioClasificacionTerceroGrupo'=>$request["chObligatorioClasificacionTerceroGrupo"][$i],
                    'chCorreoClasificacionTerceroGrupo'=>$request["chCorreoClasificacionTerceroGrupo"][$i]
                    ];
                $guardar = $grupo::updateOrCreate($indice, $datos);
            }
        }
    }

    public function grabarConfiguracion($request,$id){

        $terceroConfig = new ClasificacionTerceroConfigModel();
        $terceroConfig::where('ClasificacionTercero_oidClasificacionTercero_1aM','=',$id)->delete();

        if(isset($request["ModuloTercero_ClasificacionTerceroConfig"])){
            for($i = 0; $i < count($request["ModuloTercero_ClasificacionTerceroConfig"]); $i++){

                $indice = array($terceroConfig->getKeyName() => $request[$terceroConfig->getKeyName()][$i]);
                $datos = ['ClasificacionTercero_oidClasificacionTercero_1aM'=> $id,
                    'ModuloTercero_ClasificacionTerceroConfig'=>$request["ModuloTercero_ClasificacionTerceroConfig"][$i],
                    'chCorreoClasificacionTerceroConfig'=>$request["chCorreoClasificacionTerceroConfig"][$i]
                    ];
                $config = $terceroConfig::updateOrCreate($indice, $datos);
                $this->grabarCorreo($request,$id,$config->ModuloTercero_ClasificacionTerceroConfig,$i);
                $this->grabarRol($request,$config->oidClasificacionTerceroConfig,$i);
            }
        }
    }

    public function grabarCorreo($request,$idClasificacion,$idModulo,$i){

        $correo = new ModuloTerceroCorreoModel();

        if(isset($request["oidModuloTerceroCorreo"][$i])){
            if(isset($request["txParaModuloTerceroCorreo"][$i])){
                $indice = array($correo->getKeyName() => $request[$correo->getKeyName()][$i]);
                $datos = ['ClasificacionTercero_oidClasificacionTercero_1aM'=> $idClasificacion,
                    'ModuloTercero_oidModuloTercero_1a1'=> $idModulo,
                    'txParaModuloTerceroCorreo'=>$request["txParaModuloTerceroCorreo"][$i],
                    'txCopiaModuloTerceroCorreo'=>$request["txCopiaModuloTerceroCorreo"][$i],
                    'txAsuntoModuloTerceroCorreo'=>$request["txAsuntoModuloTerceroCorreo"][$i],
                    'txMensajeModuloTerceroCorreo'=>$request["txMensajeModuloTerceroCorreo"][$i]
                    ];
                $correos = $correo::updateOrCreate($indice, $datos);
            }
        }

    }

    public function grabarRol($request,$idConfig,$i){

        $rolModel = new ClasificacionTerceroRolModel();

        $composicionesString = $request['clasificacionTerceroRol'][$i];
        if($composicionesString){
            $composicionesString = substr($composicionesString,0,-1);
            $composiciones = explode("|",$composicionesString);

            if($composiciones){
                foreach($composiciones as $composicion){
                    $datosComposicion = explode("/",$composicion);

                    $indice = array($rolModel->getKeyName() => $datosComposicion[0]);
                    $datos = ['ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM' => $idConfig,
                        'Rol_oidClasificacionTerceroRol'=>$datosComposicion[1],
                        'chModificarClasificacionTerceroRol'=>$datosComposicion[2]
                        ];
                    $rolModel::updateOrCreate($indice, $datos);
                }
            }
        }
        
    }

    public function consultaRol(Request $request){
        $idConfig = $request->get("id");

        $roles = ClasificacionTerceroRolModel::select('oidClasificacionTerceroRol','Rol_oidClasificacionTerceroRol','chModificarClasificacionTerceroRol')
        ->where('ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM','=',$idConfig)->get();

        return json_encode($roles);

    }

}
