<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\General\Entities\ActividadEconomicaModel;
use Modules\General\Entities\TerceroActividadEconomicaModel;


use Modules\General\Http\Requests\ActividadEconomicaRequest;

class ActividadEconomicaController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $actividadeconomica = ActividadEconomicaModel::orderBy('oidActividadEconomica' , 'DESC')->get();
        return view('general::actividadEconomicaGrid', compact('permisos' , 'actividadeconomica'));
    }

            
    public function create()
    {
        $actividadeconomica = new ActividadEconomicaModel();
        $actividadeconomica->txEstadoActividadEconomica = 'Activo';
        
        
        return view('general::actividadEconomicaForm' , ['actividadeconomica' => $actividadeconomica] );
    }
            
    public function store(ActividadEconomicaRequest $request)
    {
        DB::beginTransaction();
        try{
            $actividadeconomica = ActividadEconomicaModel::create($request->all());
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidActividadEconomica' => $actividadeconomica->oidActividadEconomica] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $actividadeconomica = ActividadEconomicaModel::find($id);
        
        return view('general::actividadEconomicaForm' , ['actividadeconomica' => $actividadeconomica] );
    }
            

    public function update(ActividadEconomicaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $actividadeconomica = ActividadEconomicaModel::find($id);
            $actividadeconomica->fill($request->all());
            $actividadeconomica->save();
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidActividadEconomica' => $actividadeconomica->oidActividadEconomica] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidActividadEconomica' => $actividadeconomica->oidActividadEconomica] , 500);
        }
    }

}
