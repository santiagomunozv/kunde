<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CanalDistribucionTerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCanalDistribucionTercero" => "required|string|unique:gen_canaldistribuciontercero,txCodigoCanalDistribucionTercero,".$this->get('oidCanalDistribucionTercero') .",oidCanalDistribucionTercero,Compania_oidCompania, ". (\Session::get('oidCompania')),
						"txNombreCanalDistribucionTercero" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCanalDistribucionTercero.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCanalDistribucionTercero.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCanalDistribucionTercero.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCanalDistribucionTercero.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCanalDistribucionTercero.required"] =  "El campo Nombre es obligatorio";


                

                return $mensaje;
            }
        }
        