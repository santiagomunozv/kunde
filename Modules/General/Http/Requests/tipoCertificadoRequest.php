<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoCertificadoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoCertificado" => "required|string|unique:gen_tipocertificado,txCodigoTipoCertificado,".$this->get('oidTipoCertificado') .",oidTipoCertificado",
						"txNombreTipoCertificado" => "required|string",
						"txEstadoTipoCertificado" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoCertificado.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoCertificado.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoCertificado.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoCertificado.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoCertificado.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoTipoCertificado.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoTipoCertificado.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        