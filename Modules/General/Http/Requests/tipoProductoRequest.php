<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoProductoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoProducto" => "required|string|unique:gen_tipoproducto,txCodigoTipoProducto,".$this->get('oidTipoProducto') .",oidTipoProducto",
						"txNombreTipoProducto" => "required|string",
						"lsMetodoInventarioTipoProducto" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoProducto.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoProducto.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoProducto.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoProducto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoProducto.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsMetodoInventarioTipoProducto.string"] = "El campo Método de Inventario Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsMetodoInventarioTipoProducto.required"] =  "El campo Método de Inventario es obligatorio";


                

                return $mensaje;
            }
        }
        