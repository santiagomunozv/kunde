<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class AcoteRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoAcote" => "required|string|unique:gen_acote,txCodigoAcote,".$this->get('oidAcote') .",oidAcote",
						"txNombreAcote" => "required|string",
						"txEstadoAcote" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoAcote.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoAcote.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoAcote.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreAcote.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreAcote.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoAcote.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoAcote.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        