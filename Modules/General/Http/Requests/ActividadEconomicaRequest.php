<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ActividadEconomicaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoActividadEconomica" => "required|string|unique:gen_actividadeconomica,txCodigoActividadEconomica,".$this->get('oidActividadEconomica') .",oidActividadEconomica",
						"txNombreActividadEconomica" => "required|string",
						"txEstadoActividadEconomica" => "required|string",

                );

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoActividadEconomica.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoActividadEconomica.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoActividadEconomica.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreActividadEconomica.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreActividadEconomica.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoActividadEconomica.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoActividadEconomica.required"] =  "El campo Estado es obligatorio";

                return $mensaje;
            }
        }
        