<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class RecursoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoRecurso" => "required|string|unique:gen_recurso,txCodigoRecurso,".$this->get('oidRecurso') .",oidRecurso",
						"txNombreRecurso" => "required|string",
						"txEstadoRecurso" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoRecurso.string"] = "El campo Codigo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoRecurso.required"] =  "El campo Codigo es obligatorio";
				$mensaje["txCodigoRecurso.unique"] =  "El valor ingresado en el campo Codigo ya existe, éste debe ser único";
				$mensaje["txNombreRecurso.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreRecurso.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoRecurso.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoRecurso.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        