<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CostoIndirectoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCostoIndirecto" => "required|string|unique:gen_costoindirecto,txCodigoCostoIndirecto,".$this->get('oidCostoIndirecto') .",oidCostoIndirecto",
						"txNombreCostoIndirecto" => "required|string",
						"lsTipoCostoIndirecto" => "required|string",
						"lsBaseCostoIndirecto" => "required|string",
						"deValorCostoIndirecto" => "required|numeric",
						"txEstadoCostoIndirecto" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCostoIndirecto.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCostoIndirecto.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCostoIndirecto.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCostoIndirecto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCostoIndirecto.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsTipoCostoIndirecto.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoCostoIndirecto.required"] =  "El campo Tipo es obligatorio";
				$mensaje["lsBaseCostoIndirecto.string"] = "El campo Base de Cálculo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsBaseCostoIndirecto.required"] =  "El campo Base de Cálculo es obligatorio";
				$mensaje["deValorCostoIndirecto.numeric"] =  "El campo Valor Solo puede contener un valor numérico con decimales";
				$mensaje["deValorCostoIndirecto.required"] =  "El campo Valor es obligatorio";
				$mensaje["txEstadoCostoIndirecto.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCostoIndirecto.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        