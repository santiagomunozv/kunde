<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CentroCostoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCentroCosto" => "required|string|unique:gen_centrocosto,txCodigoCentroCosto,".$this->get('oidCentroCosto') .",oidCentroCosto,Compania_oidCompania, ". (\Session::get('oidCompania')),
                "txNombreCentroCosto" => "required|string",
                "txResponsableCentroCosto" => "required|string",
                "txEstadoCentroCosto" => "required|string",
                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCentroCosto.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCentroCosto.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCentroCosto.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCentroCosto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCentroCosto.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txResponsableCentroCosto.string"] = "El campo Responsable Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txResponsableCentroCosto.required"] =  "El campo Responsable es obligatorio";
				$mensaje["txEstadoCentroCosto.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCentroCosto.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        