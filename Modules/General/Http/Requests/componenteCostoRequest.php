<?php

namespace Modules\General\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ComponenteCostoRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(){
        return array(
            "txNombreComponenteCosto" => "required|string",
            "Tercero_oidComponenteCosto" => "required|integer",
            "txEstadoComponenteCosto" => "required|string",
            "txFormulaPrecio" => "required",
            "txValorComponenteCostoConcepto.*" => "required",
            'lsTipoComponenteCostoConcepto.*' => "required",
            "txNombreComponenteCostoConcepto.*" => "required",
            "txCodigoComponenteCostoConcepto.*" => "required"
        );
    }

    public function messages(){
        $mensaje = array();
        $mensaje["txNombreComponenteCosto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txNombreComponenteCosto.required"] =  "El campo Nombre es obligatorio";
        $mensaje["Tercero_oidComponenteCosto.integer"] = "El campo Tercero Solo puede contener un valor entero";
        $mensaje["Tercero_oidComponenteCosto.required"] =  "El campo Tercero es obligatorio";
        $mensaje["txEstadoComponenteCosto.string"] = "El campo Estado Solo puede contener caracteres alfabéticos, númericos, guión y guión bajo";
        $mensaje["txEstadoComponenteCosto.required"] =  "El campo Estado es obligatorio";
        $mensaje["txFormulaPrecio.required"] = "No se ha definido una fórmula para este componente costo";
        $componentes = $this->get('oidComponenteCostoConcepto');
        if($componentes){
            $reg = count($componentes);
            for($i = 0; $i < $reg; $i++){
                $mensaje["txCodigoComponenteCostoConcepto.".$i.".required"] = "El campo Código en la línea ".($i + 1)." es obligatorio";
                $mensaje["txNombreComponenteCostoConcepto.".$i.".required"] = "El campo Nombre en la línea ".($i + 1)." es obligatorio";
                $mensaje["lsTipoComponenteCostoConcepto.".$i.".required"] = "El campo Tipo en la línea ".($i + 1)." es obligatorio";
                $mensaje["txValorComponenteCostoConcepto.".$i.".required"] = "El campo Valor en la línea ".($i + 1)." es obligatorio";
            }
        }
        return $mensaje;
    }
}