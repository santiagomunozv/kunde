<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class PrecioEstimadoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoPrecioEstimado" => "required|string|unique:gen_precioestimado,txCodigoPrecioEstimado,".$this->get('oidPrecioEstimado') .",oidPrecioEstimado",
						"txNombrePrecioEstimado" => "required|string",
						"lsTipoPrecioEstimado" => "required|string",
						"lsBasePrecioEstimado" => "required|string",
						"deValorPrecioEstimado" => "required|numeric",
						"txEstadoPrecioEstimado" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoPrecioEstimado.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoPrecioEstimado.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoPrecioEstimado.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombrePrecioEstimado.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombrePrecioEstimado.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsTipoPrecioEstimado.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoPrecioEstimado.required"] =  "El campo Tipo es obligatorio";
				$mensaje["lsBasePrecioEstimado.string"] = "El campo Base de Cálculo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsBasePrecioEstimado.required"] =  "El campo Base de Cálculo es obligatorio";
				$mensaje["deValorPrecioEstimado.numeric"] =  "El campo Valor Solo puede contener un valor numérico con decimales";
				$mensaje["deValorPrecioEstimado.required"] =  "El campo Valor es obligatorio";
				$mensaje["txEstadoPrecioEstimado.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoPrecioEstimado.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        