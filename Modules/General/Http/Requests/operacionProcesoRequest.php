<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class OperacionProcesoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"CentroProduccion_oidOperacionProceso" => "required|integer",
						"ClasificacionProducto_oidOperacionProceso" => "required|integer",
						"LineaProduccion_oidOperacionProceso" => "required|integer",
						"txEstadoOperacionProceso" => "required|string",

                );

                $reg = count($this->get('oidOperacionProcesoDetalle'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('txSecuenciaOperacionProcesoDetalle')[$i]) == '' )
                                $validacion['txSecuenciaOperacionProcesoDetalle'.$i] =  'required';

					if(trim($this->get('txNombreOperacionProcesoDetalle')[$i]) == '' )
                                $validacion['txNombreOperacionProcesoDetalle'.$i] =  'required';

					if(trim($this->get('deSamOperacionProcesoDetalle')[$i]) == '' )
                                $validacion['deSamOperacionProcesoDetalle'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["CentroProduccion_oidOperacionProceso.integer"] = "El campo Centro de Producción Solo puede contener un valor entero";
				$mensaje["CentroProduccion_oidOperacionProceso.required"] =  "El campo Centro de Producción es obligatorio";
				$mensaje["ClasificacionProducto_oidOperacionProceso.integer"] = "El campo Clasificación de Producto Solo puede contener un valor entero";
				$mensaje["ClasificacionProducto_oidOperacionProceso.required"] =  "El campo Clasificación de Producto es obligatorio";
				$mensaje["LineaProduccion_oidOperacionProceso.integer"] = "El campo Línea de Producción Solo puede contener un valor entero";
				$mensaje["LineaProduccion_oidOperacionProceso.required"] =  "El campo Línea de Producción es obligatorio";
				$mensaje["txEstadoOperacionProceso.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoOperacionProceso.required"] =  "El campo Estado es obligatorio";


                $reg = count($this->get('oidOperacionProcesoDetalle'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["txSecuenciaOperacionProcesoDetalle".$i.".required"] = "El campo Secuencia en la línea ".($i + 1)." es obligatorio";
				$mensaje["txNombreOperacionProcesoDetalle".$i.".required"] = "El campo Operación en la línea ".($i + 1)." es obligatorio";
				$mensaje["deSamOperacionProcesoDetalle".$i.".required"] = "El campo SAM en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        