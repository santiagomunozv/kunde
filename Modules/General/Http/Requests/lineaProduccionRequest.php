<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class LineaProduccionRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoLineaProduccion" => "required|string|unique:gen_lineaproduccion,txCodigoLineaProduccion,".$this->get('oidLineaProduccion') .",oidLineaProduccion",
						"txNombreLineaProduccion" => "required|string",
						"txEstadoLineaProduccion" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoLineaProduccion.string"] = "El campo Codigo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoLineaProduccion.required"] =  "El campo Codigo es obligatorio";
				$mensaje["txCodigoLineaProduccion.unique"] =  "El valor ingresado en el campo Codigo ya existe, éste debe ser único";
				$mensaje["txNombreLineaProduccion.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreLineaProduccion.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoLineaProduccion.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoLineaProduccion.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        