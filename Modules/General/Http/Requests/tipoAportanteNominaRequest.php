<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoAportanteNominaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoAportanteNomina" => "required|string|unique:gen_tipoaportantenomina,txCodigoTipoAportanteNomina,".$this->get('oidTipoAportanteNomina') .",oidTipoAportanteNomina",
						"txNombreTipoAportanteNomina" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoAportanteNomina.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoAportanteNomina.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoAportanteNomina.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoAportanteNomina.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoAportanteNomina.required"] =  "El campo Nombre es obligatorio";


                

                return $mensaje;
            }
        }
        