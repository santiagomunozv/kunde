<?php
namespace Modules\General\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class DepartamentoRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }
    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(){
        $validacion = array(
            "Pais_oidDepartamento" => "required|integer",
            "txCodigoDepartamento" => "required|string|unique:gen_departamento,txCodigoDepartamento,".$this->get('oidDepartamento').",oidDepartamento",
            "txNombreDepartamento" => "required|string",
            "txEstadoDepartamento" => "required|string",
            "txCodigoCiudad.*" => "required|distinct|string|unique:gen_ciudad,txCodigoCiudad,".$this->get('oidDepartamento').",Departamento_oidDepartamento_1aM",
            "txNombreCiudad.*" => "required|string",
        );

        return $validacion;
    }

    public function messages(){
        $mensaje = array();
        $mensaje["Pais_oidDepartamento.integer"] = "El campo Pais Solo puede contener un valor entero";
        $mensaje["Pais_oidDepartamento.required"] =  "El campo Pais es obligatorio";
        $mensaje["txCodigoDepartamento.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txCodigoDepartamento.required"] =  "El campo Código es obligatorio";
        $mensaje["txCodigoDepartamento.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
        $mensaje["txNombreDepartamento.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txNombreDepartamento.required"] =  "El campo Nombre es obligatorio";
        $mensaje["txEstadoDepartamento.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txEstadoDepartamento.required"] =  "El campo Estado es obligatorio";
        $ids = $this->get('oidCiudad');
        if($ids){
            for($i=0;$i < count($ids);$i++){
                $mensaje["txCodigoCiudad.".$i.".required"] =  "El campo Codigo ciudad en la fila ".($i + 1)." es obligatorio";
                $mensaje["txCodigoCiudad.".$i.".string"] =  "El campo Codigo ciudad en la fila ".($i + 1)." solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
                $mensaje["txCodigoCiudad.".$i.".distinct"] =  "El campo Codigo ciudad en la fila ".($i + 1)." ya esta en uso, por favor use un codigo diferente";
                $mensaje["txCodigoCiudad.".$i.".unique"] =  "El campo Codigo ciudad en la fila ".($i + 1)." ya existe en la base de datos, por favor use un codigo diferente";
                $mensaje["txNombreCiudad.".$i.".required"] =  "El campo Nombre ciudad en la fila ".($i + 1)." es obligatorio";
                $mensaje["txNombreCiudad.".$i.".string"] =  "El campo Nombre ciudad en la fila ".($i + 1)." solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            }
        }
        return $mensaje;
    }
}