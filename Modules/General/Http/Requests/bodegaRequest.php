<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class BodegaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoBodega" => "required|string|unique:gen_bodega,txCodigoBodega,".$this->get('oidBodega') .",oidBodega",
						"txNombreBodega" => "required|string",
						"txEstadoBodega" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoBodega.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoBodega.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoBodega.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreBodega.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreBodega.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoBodega.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoBodega.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        