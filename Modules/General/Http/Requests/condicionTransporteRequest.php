<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CondicionTransporteRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCondicionTransporte" => "required|string|unique:gen_condiciontransporte,txCodigoCondicionTransporte,".$this->get('oidCondicionTransporte') .",oidCondicionTransporte",
						"txNombreCondicionTransporte" => "required|string",
						"lsTipoCondicionTransporte" => "required|string",
						"txEstadoCondicionTransporte" => "required|string",

                );

                $reg = count($this->get('oidCondicionTransporteDetalle'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('txObservacionCondicionTransporteDetalle')[$i]) == '' )
                                $validacion['txObservacionCondicionTransporteDetalle'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCondicionTransporte.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCondicionTransporte.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCondicionTransporte.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCondicionTransporte.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCondicionTransporte.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsTipoCondicionTransporte.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoCondicionTransporte.required"] =  "El campo Tipo es obligatorio";
				$mensaje["txEstadoCondicionTransporte.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCondicionTransporte.required"] =  "El campo Estado es obligatorio";


                $reg = count($this->get('oidCondicionTransporteDetalle'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["txObservacionCondicionTransporteDetalle".$i.".required"] = "El campo Observación en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        