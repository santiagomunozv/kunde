<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CentroProduccionRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCentroProduccion" => "required|string|unique:gen_centroproduccion,txCodigoCentroProduccion,".$this->get('oidCentroProduccion') .",oidCentroProduccion",
						"txNombreCentroProduccion" => "required|string",
						"deValorMinutoCentroProduccion" => "required|numeric",
						"lsClasificacionCentroProduccion" => "required|string",
						"txEstadoCentroProduccion" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCentroProduccion.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCentroProduccion.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCentroProduccion.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCentroProduccion.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCentroProduccion.required"] =  "El campo Nombre es obligatorio";
				$mensaje["deValorMinutoCentroProduccion.numeric"] =  "El campo Valor Minuto Solo puede contener un valor numérico con decimales";
				$mensaje["deValorMinutoCentroProduccion.required"] =  "El campo Valor Minuto es obligatorio";
				$mensaje["lsClasificacionCentroProduccion.string"] = "El campo Clasificación Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsClasificacionCentroProduccion.required"] =  "El campo Clasificación es obligatorio";
				$mensaje["txEstadoCentroProduccion.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCentroProduccion.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        