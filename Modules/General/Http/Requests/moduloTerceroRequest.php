<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class moduloTerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"txNombreModuloTercero" => "required|string",
						"txRutaImagenModuloTercero" => "required|string",
						"txRutaAccionModuloTercero" => "required|string",
						"txParaModuloTerceroCorreo" => "required|string",
						"txAsuntoModuloTerceroCorreo" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txNombreModuloTercero.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreModuloTercero.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txRutaImagenModuloTercero.string"] = "El campo Imagen Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txRutaImagenModuloTercero.required"] =  "El campo Imagen es obligatorio";
				$mensaje["txRutaAccionModuloTercero.string"] = "El campo Ruta Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txRutaAccionModuloTercero.required"] =  "El campo Ruta es obligatorio";
				$mensaje["ClasificacionTercero_oidClasificacionTercero_1aM.integer"] = "El campo  Solo puede contener un valor entero";
				$mensaje["ModuloTercero_oidModuloTercero_1a1.integer"] = "El campo  Solo puede contener un valor entero";
				$mensaje["txParaModuloTerceroCorreo.string"] = "El campo  Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txParaModuloTerceroCorreo.required"] =  "El campo  es obligatorio";
				$mensaje["txAsuntoModuloTerceroCorreo.string"] = "El campo  Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txAsuntoModuloTerceroCorreo.required"] =  "El campo  es obligatorio";


                

                return $mensaje;
            }
        }
        