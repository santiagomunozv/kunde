<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class GrupoTransaccionComercialRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoGrupoTransaccionComercial" => "required|string|unique:gen_grupotransaccioncomercial,txCodigoGrupoTransaccionComercial,".$this->get('oidGrupoTransaccionComercial') .",oidGrupoTransaccionComercial",
						"txNombreGrupoTransaccionComercial" => "required|string",
						"txEstadoGrupoTransaccionComercial" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoGrupoTransaccionComercial.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoGrupoTransaccionComercial.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoGrupoTransaccionComercial.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreGrupoTransaccionComercial.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreGrupoTransaccionComercial.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoGrupoTransaccionComercial.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoGrupoTransaccionComercial.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        