<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ArlRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoArl" => "required|string|unique:gen_arl,txCodigoArl,".$this->get('oidArl') .",oidArl",
						"txNombreArl" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoArl.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoArl.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoArl.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreArl.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreArl.required"] =  "El campo Nombre es obligatorio";


                

                return $mensaje;
            }
        }
        