<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoProductoTerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoProductoTercero" => "required|string|unique:gen_tipoproductotercero,txCodigoTipoProductoTercero,".$this->get('oidTipoProductoTercero') .",oidTipoProductoTercero",
						"txNombreTipoProductoTercero" => "required|string"
                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoProductoTercero.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoProductoTercero.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoProductoTercero.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoProductoTercero.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoProductoTercero.required"] =  "El campo Nombre es obligatorio";

                return $mensaje;
            }
        }
        