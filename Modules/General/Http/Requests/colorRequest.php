<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ColorRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                    "txCodigoColor" => "required|string|unique:gen_color,txCodigoColor,".$this->get('oidColor') .",oidColor",
                    "txNombreColor" => "required|string",
                    "clTonoColor" => "required|string",
                    "txEstadoColor" => "required|string",
                    "ClasificacionProducto_oidColor" => "required"

                );

            
                return $validacion;
            }

            public function messages()
            {
                $mensaje = array();
                $mensaje["txCodigoColor.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoColor.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoColor.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreColor.string"] = "El campo Nombre 1 Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreColor.required"] =  "El campo Nombre 1 es obligatorio";
				$mensaje["clTonoColor.string"] = "El campo Tono Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["clTonoColor.required"] =  "El campo Tono es obligatorio";
				$mensaje["txEstadoColor.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoColor.required"] =  "El campo Estado es obligatorio";
				$mensaje["ClasificacionProducto_oidColor.required"] =  "El campo Clasificacion es obligatorio";

                return $mensaje;
            }
        }
        