<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CostoAdicionalRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCostoAdicional" => "required|string|unique:gen_costoadicional,txCodigoCostoAdicional,".$this->get('oidCostoAdicional') .",oidCostoAdicional",
						"txNombreCostoAdicional" => "required|string",
						"lsTipoCostoAdicional" => "required|string",
						"lsBaseCostoAdicional" => "required|string",
						"deValorCostoAdicional" => "required|numeric",
						"txEstadoCostoAdicional" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCostoAdicional.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCostoAdicional.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCostoAdicional.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCostoAdicional.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCostoAdicional.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsTipoCostoAdicional.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoCostoAdicional.required"] =  "El campo Tipo es obligatorio";
				$mensaje["lsBaseCostoAdicional.string"] = "El campo Base de Cálculo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsBaseCostoAdicional.required"] =  "El campo Base de Cálculo es obligatorio";
				$mensaje["deValorCostoAdicional.numeric"] =  "El campo Valor Solo puede contener un valor numérico con decimales";
				$mensaje["deValorCostoAdicional.required"] =  "El campo Valor es obligatorio";
				$mensaje["txEstadoCostoAdicional.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCostoAdicional.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        