<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ClasificacionProductoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoClasificacionProducto" => "required|string|unique:gen_clasificacionproducto,txCodigoClasificacionProducto,".$this->get('oidClasificacionProducto') .",oidClasificacionProducto",
                "txNombreClasificacionProducto" => "required|string",
                "txNombreClasificacionProductoGrupo.*" => "required|string",
                );

                return $validacion;
            }        

            public function messages()
            {
                $mensaje = array();
                $mensaje["txCodigoClasificacionProducto.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoClasificacionProducto.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoClasificacionProducto.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreClasificacionProducto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreClasificacionProducto.required"] =  "El campo Nombre es obligatorio";


                if(null != $this->get('txNombreClasificacionProductoGrupo')){
                    for($i=0;$i < count($this->get('txNombreClasificacionProductoGrupo'));$i++){
                        $mensaje["txNombreClasificacionProductoGrupo.".$i.".required"] =  "El campo nombre en la fila ".($i + 1)." es obligatorio";
                    }
                }

                return $mensaje;
            }
        }
        