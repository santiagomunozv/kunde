<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoEtiquetaTerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoEtiquetaTercero" => "required|string|unique:gen_tipoetiquetatercero,txCodigoTipoEtiquetaTercero,".$this->get('oidTipoEtiquetaTercero') .",oidTipoEtiquetaTercero,Compania_oidCompania, ". (\Session::get('oidCompania')),
						"txNombreTipoEtiquetaTercero" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoEtiquetaTercero.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoEtiquetaTercero.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoEtiquetaTercero.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoEtiquetaTercero.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoEtiquetaTercero.required"] =  "El campo Nombre es obligatorio";


                

                return $mensaje;
            }
        }
        