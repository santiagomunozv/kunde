<?php

namespace Modules\General\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartidaArancelariaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "txNivelPartidaArancelaria" => "required",
            "txCodigoBasePartidaArancelaria" => "required",
            "txCodigoPartidaArancelaria" => "required|unique:gen_partidaarancelaria,txCodigoPartidaArancelaria,".$this->get('oidPartidaArancelaria').",oidPartidaArancelaria",
            "txNombrePartidaArancelaria" => "required",
        ];
    }

    public function messages(){
        return [
            "txNivelPartidaArancelaria.required" => 'El campo código es requerido',
            "txCodigoBasePartidaArancelaria.required" => "El campo código es requerido",
            "txCodigoBasePartidaArancelaria.unique" => "El código indicado ya existe en nuestros registros.",
            "txNombrePartidaArancelaria.required" => "El campo nombre es requerido",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
