<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TemporadaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                    "txCodigoTemporada" => "required|string|unique:gen_temporada,txCodigoTemporada,".$this->get('oidTemporada') .",oidTemporada",
                    "txNombreTemporada" => "required|string",
                    "txNombreCompraTemporada" => "required|string",
                    "daFechaInicialTemporada" => "required|date",
                    "daFechaFinalTemporada" => "required|date",
                    "txEstadoTemporada" => "required|string",
                );

                return $validacion;
            }

            public function messages()
            {
                $mensaje = array();
                $mensaje["txCodigoTemporada.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTemporada.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTemporada.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTemporada.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTemporada.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txNombreCompraTemporada.string"] = "El campo Nombre de la Compra Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCompraTemporada.required"] =  "El campo Nombre de la Compra es obligatorio";
				$mensaje["daFechaInicialTemporada.date"] = "El campo Fecha Inicial Solo puede contener una fecha en el formato AAAA-MM-DD";
				$mensaje["daFechaInicialTemporada.required"] =  "El campo Fecha Inicial es obligatorio";
				$mensaje["daFechaFinalTemporada.date"] = "El campo Fecha Final Solo puede contener una fecha en el formato AAAA-MM-DD";
				$mensaje["daFechaFinalTemporada.required"] =  "El campo Fecha Final es obligatorio";
				$mensaje["txEstadoTemporada.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoTemporada.required"] =  "El campo Estado es obligatorio";

                return $mensaje;
            }
        }
        