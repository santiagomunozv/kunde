<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CentroTrabajoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCentroTrabajo" => "required|string",
                "txCodigoNominaCentroTrabajo" => "required|string",
                "Tercero_oidSucursal" => "required|integer",
                "txNombreCentroTrabajo" => "required|string",
                "dePorcentajeARPCentroTrabajo" => "required|numeric",
                "txEstadoCentroTrabajo" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                $mensaje["txCodigoCentroTrabajo.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCentroTrabajo.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoNominaCentroTrabajo.string"] = "El campo Código Nomina Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoNominaCentroTrabajo.required"] =  "El campo Código Nomina es obligatorio";
				$mensaje["Tercero_oidSucursal.integer"] = "El campo Sucursal Solo puede contener un valor entero";
				$mensaje["Tercero_oidSucursal.required"] =  "El campo Sucursal es obligatorio";
				$mensaje["txNombreCentroTrabajo.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCentroTrabajo.required"] =  "El campo Nombre es obligatorio";
				$mensaje["dePorcentajeARPCentroTrabajo.numeric"] =  "El campo % ARP Solo puede contener un valor numérico con decimales";
				$mensaje["dePorcentajeARPCentroTrabajo.required"] =  "El campo % ARP es obligatorio";
				$mensaje["txEstadoCentroTrabajo.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCentroTrabajo.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        