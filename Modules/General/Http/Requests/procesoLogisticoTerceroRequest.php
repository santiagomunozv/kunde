<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ProcesoLogisticoTerceroRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoProcesoLogisticoTercero" => "required|string|unique:gen_procesologisticotercero,txCodigoProcesoLogisticoTercero,".$this->get('oidProcesoLogisticoTercero') .",oidProcesoLogisticoTercero,Compania_oidCompania, ". (\Session::get('oidCompania')),
						"txNombreProcesoLogisticoTercero" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoProcesoLogisticoTercero.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoProcesoLogisticoTercero.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoProcesoLogisticoTercero.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreProcesoLogisticoTercero.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreProcesoLogisticoTercero.required"] =  "El campo Nombre es obligatorio";


                

                return $mensaje;
            }
        }
        