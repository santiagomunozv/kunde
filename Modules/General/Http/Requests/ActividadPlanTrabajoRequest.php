<?php

    namespace Modules\General\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

    class ActividadPlanTrabajoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
                'txNombreActividadPlanTrabajoDetalle.*' => 'required',
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();

            $total = ($this->get("oidActividadPlanTrabajoDetalle") !== null ) ? count($this->get("oidActividadPlanTrabajoDetalle")) : 0;
            for($j=0; $j < $total; $j++)
            {
                $mensaje['txNombreActividadPlanTrabajoDetalle.'.$j.'.required'] = "El campo Actividad es obligatorio en el registro ".($j+1);
            }


            

            return $mensaje;
        }
    }