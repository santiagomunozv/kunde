<?php

    namespace Modules\General\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class TipoDocumentoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
            "txCodigoTipoDocumento" => "required",
            "txNombreTipoDocumento" => "required|string|",
            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
                $mensaje["txCodigoTipoDocumento.required"] =  "El campo Código es obligatorio";
                $mensaje["txNombreTipoDocumento.string"] = "El campo Nombre sólo puede contener caracteres afabéticos";
                $mensaje["txNombreTipoDocumento.required"] =  "El campo Nombre es obligatorio";

            return $mensaje;
        }
    }
        