<?php

namespace Modules\General\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartidaArancelariaAcuerdoRequest extends FormRequest{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'PartidaArancelaria_oidPartidaArancelaria_1aM' => 'required',
            'txNombrePartidaArancelariaAcuerdo.*' => 'required',
            'Pais_oidPartidaArancelariaAcuerdo.*' => 'required|integer',
            'deGravamenPartidaArancelariaAcuerdo.*' =>'required|numeric',
            'deIndicePartidaArancelariaAcuerdo.*' =>'nullable|numeric',
            'deTasaPartidaArancelariaAcuerdo.*' =>'nullable|numeric',
            'daDesdePartidaArancelariaAcuerdo.*' =>'required|date',
        ];
    }

    public function messages(){
        $mensajes =  [
            'PartidaArancelaria_oidPartidaArancelaria_1aM.required' => 'No se ha especificado a cual partida pertenecen estos acuerdos.',
        ];
        $ids = $this->get('oidPartidaArancelariaAcuerdo');
        if( $ids ){
            for($i = 0; $i <count($ids); $i++){
                $mensajes ['txNombrePartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo nombre en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['Pais_oidPartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo pais en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['Pais_oidPartidaArancelariaAcuerdo.'.$i.'.integer'] = 'El campo pais en la fila '.($i + 1).' debe ser de tipo entero.';
                $mensajes ['deGravamenPartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo gravamen en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['deGravamenPartidaArancelariaAcuerdo.'.$i.'.numeric'] = 'El campo gravamen en la fila '.($i + 1).' debe ser numérico.';
                $mensajes ['deIndicePartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo índice en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['deIndicePartidaArancelariaAcuerdo.'.$i.'.numeric'] = 'El campo índice en la fila '.($i + 1).' debe ser numérico.';
                $mensajes ['deTasaPartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo tasa en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['deTasaPartidaArancelariaAcuerdo.'.$i.'.numeric'] = 'El campo tasa en la fila '.($i + 1).' debe ser numérico.';
                $mensajes ['daDesdePartidaArancelariaAcuerdo.'.$i.'.required'] = 'El campo desde en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['daDesdePartidaArancelariaAcuerdo.'.$i.'.date'] = 'El campo desde en la fila '.($i + 1).' debe ser una fecha.';
            }
        }
        return $mensajes;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }
}
