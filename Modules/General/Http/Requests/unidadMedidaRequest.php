<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class UnidadMedidaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoUnidadMedida" => "required|string|unique:gen_unidadmedida,txCodigoUnidadMedida,".$this->get('oidUnidadMedida') .",oidUnidadMedida",
				"txNombreUnidadMedida" => "required|string",
				"txEstadoUnidadMedida" => "required|string",
                );

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                $mensaje["txCodigoUnidadMedida.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoUnidadMedida.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoUnidadMedida.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreUnidadMedida.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreUnidadMedida.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoUnidadMedida.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoUnidadMedida.required"] =  "El campo Estado es obligatorio";

                return $mensaje;
            }
        }
        