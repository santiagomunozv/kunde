<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class FormaPagoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoFormaPago" => "required|string|unique:gen_formapago,txCodigoFormaPago,".$this->get('oidFormaPago') .",oidFormaPago",
						"txNombreFormaPago" => "required|string",
						"txVencimientoFormaPago" => "required|string",
						"lsTipoFormaPago" => "required|string",
						"txEstadoFormaPago" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoFormaPago.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoFormaPago.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoFormaPago.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreFormaPago.string"] = "El campo Forma de Pago Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreFormaPago.required"] =  "El campo Forma de Pago es obligatorio";
				$mensaje["txVencimientoFormaPago.string"] = "El campo Vencimiento Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txVencimientoFormaPago.required"] =  "El campo Vencimiento es obligatorio";
				$mensaje["lsTipoFormaPago.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoFormaPago.required"] =  "El campo Tipo es obligatorio";
				$mensaje["txEstadoFormaPago.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoFormaPago.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        