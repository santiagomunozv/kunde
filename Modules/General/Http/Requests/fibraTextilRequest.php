<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class FibraTextilRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoFibraTextil" => "required|string|unique:gen_fibratextil,txCodigoFibraTextil,".$this->get('oidFibraTextil') .",oidFibraTextil",
						"txNombreFibraTextil" => "required|string",
						"txEstadoFibraTextil" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoFibraTextil.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoFibraTextil.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoFibraTextil.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreFibraTextil.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreFibraTextil.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoFibraTextil.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoFibraTextil.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        