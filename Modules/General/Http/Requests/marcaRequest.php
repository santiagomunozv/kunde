<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class MarcaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoMarca" => "required|string|unique:gen_marca,txCodigoMarca,".$this->get('oidMarca') .",oidMarca",
						"txNombreMarca" => "required|string",
						"txEstadoMarca" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoMarca.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoMarca.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoMarca.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreMarca.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreMarca.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoMarca.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoMarca.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        