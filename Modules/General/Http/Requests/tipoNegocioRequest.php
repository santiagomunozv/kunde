<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoNegocioRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoNegocio" => "required|string|unique:gen_tiponegocio,txCodigoTipoNegocio,".$this->get('oidTipoNegocio') .",oidTipoNegocio",
						"txNombreTipoNegocio" => "required|string",
						"txEstadoTipoNegocio" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoNegocio.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoNegocio.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoNegocio.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoNegocio.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoNegocio.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoTipoNegocio.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoTipoNegocio.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        