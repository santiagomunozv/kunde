<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class EventoSayaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                    "Tercero_oidTercero_1aM" => "required",
                    "txCodigoEvento" => "required|alpha", 
                    "txNombreEvento" => "required|string",
                    "txEstadoEvento" => "required"
                );

                return $validacion;
            }

            public function messages()
            {
                $mensaje = array();
                $mensaje["Tercero_oidTercero_1aM.required"] = "El campo Tercero es obligatorio";
				$mensaje["txCodigoEvento.string"] = "El campo Codigo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoEvento.required"] =  "El campo Codigo es obligatorio";
				$mensaje["txCodigoEvento.unique"] =  "El valor ingresado en el campo Codigo ya existe, éste debe ser único";
				$mensaje["txNombreEvento.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreEvento.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoEvento.required"] =  "El campo Estado es obligatorio";

                return $mensaje;
            }
        }
        