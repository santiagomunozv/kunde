<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ClasificacionRentaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoClasificacionRenta" => "required|string|unique:gen_clasificacionrenta,txCodigoClasificacionRenta,".$this->get('oidClasificacionRenta') .",oidClasificacionRenta",
						"txNombreClasificacionRenta" => "required|string",
						"txEstadoClasificacionRenta" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoClasificacionRenta.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoClasificacionRenta.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoClasificacionRenta.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreClasificacionRenta.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreClasificacionRenta.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoClasificacionRenta.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoClasificacionRenta.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        