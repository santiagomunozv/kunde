<?php

namespace Modules\General\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartidaArancelariaImpuestoRequest extends FormRequest{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'PartidaArancelaria_oidPartidaArancelaria_1aM' => 'required',
            'txTasaPartidaArancelariaImpuesto.*' =>'required',
            'daDesdePartidaArancelariaImpuesto.*' =>'required|date',
        ];
    }

    public function messages(){
        $mensajes =  [
            'PartidaArancelaria_oidPartidaArancelaria_1aM.required' => 'No se ha especificado a cual partida pertenecen estos impuestos.',
        ];
        $ids = $this->get('oidPartidaArancelariaImpuesto');
        if( $ids ){
            for($i = 0; $i <count($ids); $i++){
                $mensajes ['txTasaPartidaArancelariaImpuesto.'.$i.'.required'] = 'El campo tasa en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['daDesdePartidaArancelariaImpuesto.'.$i.'.required'] = 'El campo desde en la fila '.($i + 1).' es obligatorio.';
                $mensajes ['daDesdePartidaArancelariaImpuesto.'.$i.'.date'] = 'El campo desde en la fila '.($i + 1).' debe ser una fecha.';
            }
        }
        return $mensajes;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }
}
