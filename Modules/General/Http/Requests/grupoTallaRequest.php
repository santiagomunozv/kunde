<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class GrupoTallaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoGrupoTalla" => "required|string|unique:gen_grupotalla,txCodigoGrupoTalla,".$this->get('oidGrupoTalla') .",oidGrupoTalla",
						"txNombreGrupoTalla" => "required|string",
						"ClasificacionProducto_oidGrupoTalla" => "required|integer",
						"txEstadoGrupoTalla" => "required|string",

                );

                $reg = count($this->get('oidTalla'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('inOrdenTalla')[$i]) == '' )
                                $validacion['inOrdenTalla'.$i] =  'required';

					if(trim($this->get('txCodigoTalla')[$i]) == '' )
                                $validacion['txCodigoTalla'.$i] =  'required';

					if(trim($this->get('txNombreTalla')[$i]) == '' )
                                $validacion['txNombreTalla'.$i] =  'required';

					if(trim($this->get('chBaseTalla')[$i]) == '' )
                                $validacion['chBaseTalla'.$i] =  'required';

					if(trim($this->get('inCurvaTalla')[$i]) == '' )
                                $validacion['inCurvaTalla'.$i] =  'required';

					if(trim($this->get('txEstadoTalla')[$i]) == '' )
                                $validacion['txEstadoTalla'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoGrupoTalla.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoGrupoTalla.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoGrupoTalla.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreGrupoTalla.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreGrupoTalla.required"] =  "El campo Nombre es obligatorio";
				$mensaje["ClasificacionProducto_oidGrupoTalla.integer"] = "El campo Clasificacion de Producto Solo puede contener un valor entero";
				$mensaje["ClasificacionProducto_oidGrupoTalla.required"] =  "El campo Clasificacion de Producto es obligatorio";
				$mensaje["txEstadoGrupoTalla.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoGrupoTalla.required"] =  "El campo Estado es obligatorio";


                $reg = count($this->get('oidTalla'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["inOrdenTalla".$i.".required"] = "El campo Orden en la línea ".($i + 1)." es obligatorio";
				$mensaje["txCodigoTalla".$i.".required"] = "El campo Código en la línea ".($i + 1)." es obligatorio";
				$mensaje["txNombreTalla".$i.".required"] = "El campo Nombre 1 en la línea ".($i + 1)." es obligatorio";
				$mensaje["chBaseTalla".$i.".required"] = "El campo Medida Base en la línea ".($i + 1)." es obligatorio";
				$mensaje["inCurvaTalla".$i.".required"] = "El campo Curva en la línea ".($i + 1)." es obligatorio";
				$mensaje["txEstadoTalla".$i.".required"] = "El campo Estado en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        