<?php

namespace Modules\General\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class MetadatoRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules(){
        $validacion = array(
            "lsModuloMetadato" => "required|string",
            "txCodigoMetadato" => "nullable|string|unique:gen_metadato,txCodigoMetadato,".$this->get('oidMetadato') .",oidMetadato",
            "txNombreMetadato" => "required|string",
            "lsTipoMetadato" => "required|string",
            "inLongitudMetadato" => "required|integer",
            "inDecimalMetadato" => "required|integer",
            "txEstadoMetadato" => "required|string",
        );
        $reg = $this->get('oidMetadatoDetalle');
        if($reg){
            $reg = count($reg);
            for($i = 0; $i < $reg; $i++){
                if(trim($this->get('txNombreMetadatoDetalle')[$i]) == '' ){
                    $validacion['txNombreMetadatoDetalle'.$i] =  'required';
                }
                if(trim($this->get('daDesdeMetadatoDetalle')[$i]) == '' ){
                    $validacion['daDesdeMetadatoDetalle'.$i] =  'required';
                }
            }
        }
        
        return $validacion;
    }

    public function messages(){
        $mensaje = array();
        $mensaje["lsModuloMetadato.string"] = "El campo Módulo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["lsModuloMetadato.required"] =  "El campo Módulo es obligatorio";
        $mensaje["txCodigoMetadato.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txCodigoMetadato.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
        $mensaje["txNombreMetadato.string"] = "El campo Nombre Campo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txNombreMetadato.required"] =  "El campo Nombre Campo es obligatorio";
        $mensaje["lsTipoMetadato.string"] = "El campo Tipo de dato Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["lsTipoMetadato.required"] =  "El campo Tipo de dato es obligatorio";
        $mensaje["inLongitudMetadato.integer"] = "El campo Longitud Solo puede contener un valor entero";
        $mensaje["inLongitudMetadato.required"] =  "El campo Longitud es obligatorio";
        $mensaje["inDecimalMetadato.integer"] = "El campo Decimales Solo puede contener un valor entero";
        $mensaje["inDecimalMetadato.required"] =  "El campo Decimales es obligatorio";
        $mensaje["txEstadoMetadato.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
        $mensaje["txEstadoMetadato.required"] =  "El campo Estado es obligatorio";
        $reg = $this->get('oidMetadatoDetalle');
        if($reg){
            $reg = count($reg);
            for($i = 0; $i < $reg; $i++){
                $mensaje["txCodigoMetadatoDetalle".$i.".required"] = "El campo Código en la línea ".($i + 1)." es obligatorio";
                $mensaje["txNombreMetadatoDetalle".$i.".required"] = "El campo Nombre en la línea ".($i + 1)." es obligatorio";
                $mensaje["daDesdeMetadatoDetalle".$i.".required"] = "El campo Desde en la línea ".($i + 1)." es obligatorio";
            }
        }
        return $mensaje;
    }
}