<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class GrupoNominaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoGrupoNomina" => "required|string|unique:gen_gruponomina,txCodigoGrupoNomina,".$this->get('oidGrupoNomina') .",oidGrupoNomina",
						"txNombreGrupoNomina" => "required|string",
						"lsBasePagoGrupoNomina" => "required|string",
						"txEstadoGrupoNomina" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoGrupoNomina.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoGrupoNomina.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoGrupoNomina.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreGrupoNomina.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreGrupoNomina.required"] =  "El campo Nombre es obligatorio";
				$mensaje["lsBasePagoGrupoNomina.string"] = "El campo Base de Pago Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsBasePagoGrupoNomina.required"] =  "El campo Base de Pago es obligatorio";
				$mensaje["txEstadoGrupoNomina.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoGrupoNomina.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        