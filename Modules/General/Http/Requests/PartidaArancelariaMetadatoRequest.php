<?php

namespace Modules\General\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartidaArancelariaMetadatoRequest extends FormRequest{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'PartidaArancelaria_oidPartidaArancelaria_1aM' => 'required',
            'txCodigoPartidaArancelariaMetadato.*' => 'required',
            'Metadato_oidPartidaArancelariaMetadato.*' => 'required|integer',
            'chObligatorioPartidaArancelariaMetadato.*'=> 'required',
            'chRegistroPartidaArancelariaMetadato.*'=> 'required',
            'chValorPartidaArancelariaMetadato.*'=> 'required',
            'chImportacionPartidaArancelariaMetadato.*'=> 'required',
            'daDesdePartidaArancelariaMetadato.*'=> 'required|date',
        ];
    }

    public function messages(){
        $mensajes =  [
            'PartidaArancelaria_oidPartidaArancelaria_1aM.required' => 'No se ha especificado a cual partida pertenecen estos impuestos.',
        ];
        $ids = $this->get('oidPartidaArancelariaMetadato');
        if( $ids ){
            for($i = 0; $i <count($ids); $i++){
                $mensajes['txCodigoPartidaArancelariaMetadato.'.$i.'.required'] = 'El código en la fila '.($i + 1).' es obligatorio.'; 
                $mensajes['txCodigoPartidaArancelariaMetadato.'.$i.'.distinct'] = 'El código en la fila '.($i + 1).' tiene valores duplicados para esta partida arancelaria.';
                $mensajes['txCodigoPartidaArancelariaMetadato.'.$i.'.required'] = 'El código en la fila '.($i + 1).' debe ser único para esta partida arancelaria.';
                $mensajes['Metadato_oidPartidaArancelariaMetadato.'.$i.'.required'] = 'No se ha definido una descripcion para la fila '.($i + 1);
                $mensajes['Metadato_oidPartidaArancelariaMetadato.'.$i.'.integer'] = 'La descripción para la fila '.($i + 1).' no es valida.';
                $mensajes['chObligatorioPartidaArancelariaMetadato.'.$i. '.required'] = 'El campo req para la fila '.($i + 1).' no está presente.';
                $mensajes['chRegistroPartidaArancelariaMetadato.'.$i. '.required']='El campo req para la fila '.($i + 1).' no está presente.';
                $mensajes['chValorPartidaArancelariaMetadato.'.$i.'.required'] = 'El campo req para la fila '.($i + 1).' no está presente.';
                $mensajes['chImportacionPartidaArancelariaMetadato.'.$i. '.required'] = 'El campo req para la fila '.($i + 1).' no está presente.';
                $mensajes['daDesdePartidaArancelariaMetadato.'.$i.'.required'] = 'El campo desde para la fila '.($i + 1).' es requerido';
                $mensajes['daDesdePartidaArancelariaMetadato.'.$i.'.date'] = 'El campo desde para la fila '.($i + 1).' debe ser de tipo fecha';
            }
        }
        return $mensajes;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }
}
