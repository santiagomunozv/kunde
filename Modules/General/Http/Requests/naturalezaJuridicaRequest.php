<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class NaturalezaJuridicaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoNaturalezaJuridica" => "required|string|unique:gen_naturalezajuridica,txCodigoNaturalezaJuridica,".$this->get('oidNaturalezaJuridica') .",oidNaturalezaJuridica",
						"txNombreNaturalezaJuridica" => "required|string",
						"txEstadoNaturalezaJuridica" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoNaturalezaJuridica.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoNaturalezaJuridica.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoNaturalezaJuridica.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreNaturalezaJuridica.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreNaturalezaJuridica.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoNaturalezaJuridica.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoNaturalezaJuridica.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        