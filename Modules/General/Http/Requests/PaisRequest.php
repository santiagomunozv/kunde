<?php
namespace Modules\General\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;
        
class PaisRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        $validacion = [
            "txNombrePais" => "required",
            "txCodigoDIANPais" => "required",
            "txEstadoPais" => "required",
        ];
        return $validacion;
    }

    public function messages(){
        $mensaje = array();
        $mensaje["txNombrePais.required"] =  "El campo Nombre es obligatorio";
        $mensaje["txEstadoPais.required"] =  "El campo Estado es obligatorio";
        $mensaje["txCodigoDIANPais.required"] =  "El campo código DIAN es obligatorio";
        return $mensaje;
    }
}