<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CategoriaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCategoria" => "required|string|unique:gen_categoria,txCodigoCategoria,".$this->get('oidCategoria') .",oidCategoria",
						"txNombreCategoria" => "required|string",
						"txEstadoCategoria" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["Tercero_oidTercero_1aM.integer"] = "El campo Tercero Solo puede contener un valor entero";
				$mensaje["txCodigoCategoria.string"] = "El campo Codigo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCategoria.required"] =  "El campo Codigo es obligatorio";
				$mensaje["txCodigoCategoria.unique"] =  "El valor ingresado en el campo Codigo ya existe, éste debe ser único";
				$mensaje["txNombreCategoria.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCategoria.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoCategoria.string"] = "El campo  Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCategoria.required"] =  "El campo  es obligatorio";


                

                return $mensaje;
            }
        }
        