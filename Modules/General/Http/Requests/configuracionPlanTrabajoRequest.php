<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ConfiguracionPlanTrabajoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"txActividadProgramadaConfiguracionPlanTrabajo" => "required|string",
						"txMesConfiguracionPlanTrabajo" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txActividadProgramadaConfiguracionPlanTrabajo.string"] = "El campo Actividad Programada Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txActividadProgramadaConfiguracionPlanTrabajo.required"] =  "El campo Actividad Programada es obligatorio";
				$mensaje["txMesConfiguracionPlanTrabajo.string"] = "El campo Mes Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txMesConfiguracionPlanTrabajo.required"] =  "El campo Mes es obligatorio";


                

                return $mensaje;
            }
        }
        