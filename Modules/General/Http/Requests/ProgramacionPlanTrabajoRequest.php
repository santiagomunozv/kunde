<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ProgramacionPlanTrabajoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                    'Tercero_oidTerceroEncargado.*' => 'required',
                    "Tercero_oidTerceroCompania" => "required",
                    'txActividadProgramadaConfiguracionPlanTrabajo.*' => 'required',
                    'txMesConfiguracionPlanTrabajo.*' => 'required',
                    'daFechaProgramadaActividadPlanTrabajo.*' => 'required',
                    'txHorasProgramadasActividadPlanTrabajo.*' => 'required',
                    'txCantidadHorasActividadPlanTrabajo.*' => 'required',
                );

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                $mensaje["Tercero_oidTerceroEncargado.required"] = "El campo Encargado es obligatorio";
                $mensaje["Tercero_oidTerceroCompania.required"] =  "El campo Compañía es obligatorio";

                $total = ($this->get("oidActividadPlanTrabajo") !== null ) ? count($this->get("oidActividadPlanTrabajo")) : 0;
                for($j=0; $j < $total; $j++)
                {
                    $mensaje['txActividadProgramadaConfiguracionPlanTrabajo.'.$j.'.required'] = "El campo Actividad es obligatorio en el registro ".($j+1);
                    $mensaje['txMesConfiguracionPlanTrabajo.'.$j.'.required'] = "El campo Mes es obligatorio en el registro ".($j+1);
                    $mensaje['daFechaProgramadaActividadPlanTrabajo.'.$j.'.required'] = "El campo Fecha es obligatorio en el registro ".($j+1);
                    $mensaje['txHorasProgramadasActividadPlanTrabajo.'.$j.'.required'] = "El campo Horas programadas es obligatorio en el registro ".($j+1);
                    $mensaje['txCantidadHorasActividadPlanTrabajo.'.$j.'.required'] = "El campo Cantidad horas es obligatorio en el registro ".($j+1);
                }

                return $mensaje;
            }
        }
        