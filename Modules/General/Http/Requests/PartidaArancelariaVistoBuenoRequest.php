<?php

namespace Modules\General\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartidaArancelariaVistoBuenoRequest extends FormRequest{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'PartidaArancelaria_oidVistoBueno_1aM' => 'required',
            'DocumentoRequerido_oidVistoBueno_1aM.*' => 'required|distinct',
        ];
    }

    public function messages(){
        $mensajes =  [
            'PartidaArancelaria_oidVistoBueno_1aM.required' => 'No se ha especificado a cual partida pertenecen estos impuestos.',
        ];
        $ids = $this->get('oidPartidaArancelariaVistoBueno');
        if( $ids ){
            for($i = 0; $i < count($ids); $i++){
                $mensajes['DocumentoRequerido_oidVistoBueno_1aM.'.$i.'.required'] = 'El documento en la fila '.($i + 1).' es obligatorio.'; 
                $mensajes['DocumentoRequerido_oidVistoBueno_1aM.'.$i.'.distinct'] = 'El documento en la fila '.($i + 1).' tiene valores duplicados para esta partida arancelaria.';
            }
        }
        return $mensajes;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }
}
