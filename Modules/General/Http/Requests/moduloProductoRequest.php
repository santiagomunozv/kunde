<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ModuloProductoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"txNombreModuloProducto" => "required|string",
						"txRutaImagenModuloProducto" => "required|string",
						"txRutaAccionModuloProducto" => "required|string",
						"txParaModuloProductoCorreo" => "required|string",
						"txAsuntoModuloProductoCorreo" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txNombreModuloProducto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreModuloProducto.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txRutaImagenModuloProducto.string"] = "El campo Imagen Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txRutaImagenModuloProducto.required"] =  "El campo Imagen es obligatorio";
				$mensaje["txRutaAccionModuloProducto.string"] = "El campo Accion Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txRutaAccionModuloProducto.required"] =  "El campo Accion es obligatorio";
				$mensaje["ClasificacionProducto_oidClasificacionProducto_1aM.integer"] = "El campo  Solo puede contener un valor entero";
				$mensaje["ModuloProducto_oidModuloProducto_1a1.integer"] = "El campo Módulo Solo puede contener un valor entero";
				$mensaje["txParaModuloProductoCorreo.string"] = "El campo Para Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txParaModuloProductoCorreo.required"] =  "El campo Para es obligatorio";
				$mensaje["txAsuntoModuloProductoCorreo.string"] = "El campo Asunto Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txAsuntoModuloProductoCorreo.required"] =  "El campo Asunto es obligatorio";


                

                return $mensaje;
            }
        }
        