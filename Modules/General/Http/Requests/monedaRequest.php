<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class MonedaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoMoneda" => "required|string|unique:gen_moneda,txCodigoMoneda,".$this->get('oidMoneda') .",oidMoneda",
						"txNombreMoneda" => "required|string",
						"txSimboloMoneda" => "required|string",
						"lsTipoMoneda" => "required|string",
						"txEstadoMoneda" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoMoneda.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoMoneda.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoMoneda.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreMoneda.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreMoneda.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txSimboloMoneda.string"] = "El campo Símbolo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txSimboloMoneda.required"] =  "El campo Símbolo es obligatorio";
				$mensaje["lsTipoMoneda.string"] = "El campo Tipo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoMoneda.required"] =  "El campo Tipo es obligatorio";
				$mensaje["txEstadoMoneda.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoMoneda.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        