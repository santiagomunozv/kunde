<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class DependenciaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoDependencia" => "required|string|unique:gen_dependencia,txCodigoDependencia,".$this->get('oidDependencia') .",oidDependencia",
						"txNombreDependencia" => "required|string",
						"txAbreviaturaDependencia" => "required|string",
						"txDirectorioDependencia" => "required|string",
						"txEstadoDependencia" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoDependencia.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoDependencia.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoDependencia.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreDependencia.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreDependencia.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txAbreviaturaDependencia.string"] = "El campo Abreviatura Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txAbreviaturaDependencia.required"] =  "El campo Abreviatura es obligatorio";
				$mensaje["txDirectorioDependencia.string"] = "El campo Directorio Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txDirectorioDependencia.required"] =  "El campo Directorio es obligatorio";
				$mensaje["txEstadoDependencia.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoDependencia.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        