<?php

    namespace Modules\General\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;

    class ProductoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }
    
        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            $validacion = array(
                "txCodigoProducto" => "required|string",
                "Tercero_oidProducto" => "required|integer",
                "txReferenciaProducto" => "required|string",
                "txNombreProducto" => "required|string",
                "dePrecioProducto" => "required|numeric",
                "txEstadoProducto" => "required|string",
            );

            return $validacion;
        }

        public function messages()
        {
            $mensaje = array();
            $mensaje["Tercero_oidProducto.integer"] = "El campo Tercero Solo puede contener un valor entero";
            $mensaje["Tercero_oidProducto.required"] = "El campo Tercero es obligatorio";
            $mensaje["txCodigoProducto.string"] = "El campo Codigo Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["txCodigoProducto.required"] = "El campo Codigo es obligatorio";
            $mensaje["txReferenciaProducto.string"] = "El campo Referencia Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["txReferenciaProducto.required"] = "El campo Referencia es obligatorio";
            $mensaje["txNombreProducto.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["txNombreProducto.required"] = "El campo Nombre es obligatorio";
            $mensaje["dePrecioProducto.numeric"] = "El campo Precio Solo puede contener un valor numérico con decimales";
            $mensaje["dePrecioProducto.required"] = "El campo Precio es obligatorio";
            $mensaje["txEstadoProducto.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["txEstadoProducto.required"] = "El campo Estado es obligatorio";

            return $mensaje;
        }
    }
    