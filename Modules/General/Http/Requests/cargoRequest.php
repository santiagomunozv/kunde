<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CargoRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCargo" => "required|string|unique:gen_cargo,txCodigoCargo,".$this->get('oidCargo') .",oidCargo",
						"txNombreCargo" => "required|string",
						"txEstadoCargo" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCargo.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCargo.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCargo.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCargo.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCargo.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoCargo.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCargo.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        