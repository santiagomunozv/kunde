<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class TipoIdentificacionRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoTipoIdentificacion" => "required|string|unique:gen_tipoidentificacion,txCodigoTipoIdentificacion,".$this->get('oidTipoIdentificacion') .",oidTipoIdentificacion",
						"txNombreTipoIdentificacion" => "required|string",
						"txEstadoTipoIdentificacion" => "required|string",

                );

                

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoTipoIdentificacion.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoTipoIdentificacion.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoTipoIdentificacion.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreTipoIdentificacion.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreTipoIdentificacion.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoTipoIdentificacion.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoTipoIdentificacion.required"] =  "El campo Estado es obligatorio";


                

                return $mensaje;
            }
        }
        