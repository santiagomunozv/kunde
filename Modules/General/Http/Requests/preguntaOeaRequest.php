<?php

        namespace Modules\General\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class PreguntaOeaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoPreguntaOea" => "required|string",
						"txNombrePreguntaOea" => "required|string",
						"lsTipoPreguntaOea" => "required|string",
						"inPorcentajePreguntaOea" => "required|integer",
						"txEstadoPreguntaOea" => "required|string",

                );
                $reg = ($this->get('oidPreguntaOeaDetalle') ? count($this->get('oidPreguntaOeaDetalle')) : 0);
                // $reg = count($this->get('oidPreguntaOeaDetalle'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('inCalificacionPreguntaOeaDetalle')[$i]) == '' )
                                $validacion['inCalificacionPreguntaOeaDetalle'.$i] =  'required';

					if(trim($this->get('txTextoPreguntaOeaDetalle')[$i]) == '' )
                                $validacion['txTextoPreguntaOeaDetalle'.$i] =  'required';

					if(trim($this->get('txObservacionPreguntaOeaDetalle')[$i]) == '' )
                                $validacion['txObservacionPreguntaOeaDetalle'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoPreguntaOea.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoPreguntaOea.required"] =  "El campo Código es obligatorio";
				$mensaje["txNombrePreguntaOea.string"] = "El campo Pregunta Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombrePreguntaOea.required"] =  "El campo Pregunta es obligatorio";
				$mensaje["lsTipoPreguntaOea.string"] = "El campo Tipo de Pregunta Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["lsTipoPreguntaOea.required"] =  "El campo Tipo de Pregunta es obligatorio";
				$mensaje["inPorcentajePreguntaOea.integer"] = "El campo Porcentaje Solo puede contener un valor entero";
				$mensaje["inPorcentajePreguntaOea.required"] =  "El campo Porcentaje es obligatorio";
				$mensaje["txEstadoPreguntaOea.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoPreguntaOea.required"] =  "El campo Estado es obligatorio";

                $reg = ($this->get('oidPreguntaOeaDetalle') ? count($this->get('oidPreguntaOeaDetalle')) : 0);
                // $reg = count($this->get('oidPreguntaOeaDetalle'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["inCalificacionPreguntaOeaDetalle".$i.".required"] = "El campo Calificación en la línea ".($i + 1)." es obligatorio";
				$mensaje["txTextoPreguntaOeaDetalle".$i.".required"] = "El campo Texto Calificación en la línea ".($i + 1)." es obligatorio";
				$mensaje["txObservacionPreguntaOeaDetalle".$i.".required"] = "El campo Observación en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        