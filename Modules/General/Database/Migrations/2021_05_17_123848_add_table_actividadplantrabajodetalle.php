<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableActividadplantrabajodetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_actividadplantrabajodetalle', function (Blueprint $table) {
            $table->increments('oidActividadPlanTrabajoDetalle');
            $table->integer('ActividadPlanTrabajo_oidActividadPlanTrabajo')->comment('Id Actividad');;
            $table->string('txNombreActividadPlanTrabajoDetalle')->comment('Cantidad de empleados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_actividadplantrabajodetalle');
    }
}
