<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSugerirInGenConfiguracionplantrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gen_configuracionplantrabajo', function (Blueprint $table) {
            $table->boolean('chSugerirConfiguracionPlanTrabajo')->comment('Sugerir');
        });

        DB::update('UPDATE gen_configuracionplantrabajo set chSugerirConfiguracionPlanTrabajo = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gen_configuracionplantrabajo', function (Blueprint $table) {
            $table->dropColumn('chSugerirConfiguracionPlanTrabajo');
        });
    }
}
