<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddTableDocumentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gen_tipodocumento', function (Blueprint $table) {
            $table->increments('oidTipoDocumento');
            $table->string('txCodigoTipoDocumento')->comment('Código de tipo de documento');
            $table->string('txNombreTipoDocumento')->comment('Nombre de tipo de documento');
        });

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>1,
            "txNombreOpcion" =>"Tipos de documento",
            "txRutaOpcion" => "/general/tipodocumento"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gen_tipodocumento');
    }
}
