<?php 
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class PaisModel extends Model{
    protected $table = 'gen_pais';
    protected $primaryKey = 'oidPais';
    protected $fillable = ['txCodigoISOPais','txCodigoEANPais','txCodigoDIANPais','txNombrePais','txEstadoPais'];
    public $timestamps = false;

    public static function listSelect($orId = 0){
        return self::where('txEstadoPais','Activo')
            ->orWhere('oidPais',$orId)
            ->orderBy('txNombrePais','Asc')
            ->select('txNombrePais','oidPais')
            ->pluck('txNombrePais','oidPais');
    }
}