<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TipoCertificadoModel extends Model
{
    protected $table = 'gen_tipocertificado';
    protected $primaryKey = 'oidTipoCertificado';
    protected $fillable = ['txCodigoTipoCertificado','txNombreTipoCertificado','lsTipoTipoCertificado','txVigenciaTipoCertificado','Compania_oidCompania','lmImpactoTipoCertificado','txEstadoTipoCertificado',];
    public $timestamps = false;

}