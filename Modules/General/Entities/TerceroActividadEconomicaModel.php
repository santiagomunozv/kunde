<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TerceroActividadEconomicaModel extends Model
{
    protected $table = 'asn_terceroactividadeconomica';
    protected $primaryKey = 'oidTerceroActividadEconomica';
    protected $fillable = ['Tercero_oidTercero_1aM','ActividadEconomica_oidActividadEconomica_1aM',];
    public $timestamps = false;

}