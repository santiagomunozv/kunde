<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class MatrizRiesgoModel extends Model
{
    protected $table = 'gen_matrizriesgo';
    protected $primaryKey = 'oidMatrizRiesgo';
    protected $fillable = ['Cargo_oidCargo', 'txActividadCargoMatrizRiesgo','lsRutinariaMatrizRiesgo', 'lsClasificacionMatrizRiesgo', 'txDescripcionClasificacionMatrizRiesgo', 'txEfectosPosiblesMatrizRiesgo', 'txFuenteMatrizRiesgo', 'txMedioMatrizRiesgo', 'txIndividuoMatrizRiesgo', 'lsNivelDeficienciaMatrizRiesgo', 'lsNivelExposicionMatrizRiesgo', 'txNivelProbabilidadMatrzRiesgo', 'txInterpretacionProbabilidadMatrizRiesgo', 'lsNivelConsecuenciaMatrizRiesgo', 'txNivelRiesgoMatrizRiesgo', 'txInterpretacionRiesgoMatrizRiesgo', 'txAceptabilidadRiesgoMatrizRiesgo', 'inExpuestosMatrizRiesgo', 'txPeorConsecuenciaMatrizRiesgo', 'txEliminacionMatrizRiesgo', 'txSustitucionMatrizRiesgo', 'txControlIngenieria', 'txControlAdministrativoMatrizRiesgo', 'txElementosProteccionPersonalMatrizRiesgo'];
    public $timestamps = false;

}