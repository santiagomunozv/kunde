<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

class ResponsabilidadFiscalModel extends Model
{
    protected $table = 'gen_responsabilidadfiscal';
    protected $primaryKey = 'oidResponsabilidadFiscal';
    protected $fillable = ['txCodigoResponsabilidadFiscal','txNombreResponsabilidadFiscal'];
    public $timestamps = false;
}
