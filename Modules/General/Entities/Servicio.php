<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';
    protected $fillable = ['txNombre', 'chEstado'];
}
