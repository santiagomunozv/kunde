<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class CentroTrabajoModel extends Model
{
    protected $table = 'gen_centrotrabajo';
    protected $primaryKey = 'oidCentroTrabajo';
    protected $fillable = ['txCodigoCentroTrabajo','txCodigoNominaCentroTrabajo','Tercero_oidSucursal','txNombreCentroTrabajo','dePorcentajeARPCentroTrabajo','Compania_oidCompania','txEstadoCentroTrabajo',];
    public $timestamps = false;

	public function tercero()
    {
        return $this->hasOne('\Modules\AsociadoNegocio\Entities\TerceroModel','oidTercero','Tercero_oidSucursal');
    }

}