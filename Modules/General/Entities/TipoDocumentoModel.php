<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TipoDocumentoModel extends Model
{
    protected $table = 'gen_tipodocumento';
    protected $primaryKey = 'oidTipoDocumento';
    protected $fillable = ['txCodigoTipoDocumento','txNombreTipoDocumento'];
    public $timestamps = false;

}