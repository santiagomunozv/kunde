<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class FormaPagoModel extends Model
{
    protected $table = 'gen_formapago';
    protected $primaryKey = 'oidFormaPago';
    protected $fillable = ['txCodigoFormaPago','txNombreFormaPago','txVencimientoFormaPago','chAfectaCFormaPago','chAfectaCCFormaPago','lsTipoFormaPago','txEstadoFormaPago',];
    public $timestamps = false;

}