<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class CargoModel extends Model
{
    protected $table = 'gen_cargo';
    protected $primaryKey = 'oidCargo';
    protected $fillable = ['Compania_oidCompania','txCodigoCargo','txNombreCargo','txEstadoCargo'];
    public $timestamps = false;

}