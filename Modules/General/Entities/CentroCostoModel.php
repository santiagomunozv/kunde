<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class CentroCostoModel extends Model
{
    protected $table = 'gen_centrocosto';
    protected $primaryKey = 'oidCentroCosto';
    protected $fillable = ['txCodigoCentroCosto','txNombreCentroCosto','txResponsableCentroCosto','Compania_oidCompania','txEstadoCentroCosto',];
    public $timestamps = false;

}