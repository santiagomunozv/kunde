<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ProductoTerceroModel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'gen_productotercero';
    protected $primaryKey = 'oidProductoTercero';
    protected $fillable = ['Producto_oidProducto_1a1','Categoria_oidCategoria','TipoProducto_oidTipoProducto','Evento_oidEvento','txCodigoProductoTercero',
    'txReferenciaProductoTercero','txNombreProductoTercero','txPLUProductoTercero','txCodigoBarrasProductoTercero','dePrecioVentaProductoTercero'];

    public $timestamps = false;

    public static function findProductoTerceroByIdProducto($idProducto){
        return self::select('oidProductoTercero')->where('Producto_oidProducto_1a1',$idProducto)->first();
    }

    public function setExists($valor){
        return $this->exists = $valor;
    }

}