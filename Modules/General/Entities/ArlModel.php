<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ArlModel extends Model
{
    protected $table = 'gen_arl';
    protected $primaryKey = 'oidArl';
    protected $fillable = ['txCodigoArl','txNombreArl',];
    public $timestamps = false;

}