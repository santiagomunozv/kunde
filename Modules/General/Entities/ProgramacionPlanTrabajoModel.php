<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ProgramacionPlanTrabajoModel extends Model
{
    protected $table = 'gen_programacionplantrabajo';
    protected $primaryKey = 'oidProgramacionPlanTrabajo';
    protected $fillable = ['Tercero_oidTerceroEncargado','Tercero_oidTerceroCompania',];
    public $timestamps = false;

	public function tercero($id)
    {
        return $this->hasOne('\Modules\AsociadoNegocio\Entities\TerceroModel','oidTercero','Tercero_oidTerceroCompania');
        
        // return $this->hasMany('Modules\AsociadoNegocio\Entities\GrupoTerceroModel','ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo')->where('ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo','=',$id);

    }

}