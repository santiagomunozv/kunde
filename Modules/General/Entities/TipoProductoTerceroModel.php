<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TipoProductoTerceroModel extends Model
{
    protected $table = 'gen_tipoproductotercero';
    protected $primaryKey = 'oidTipoProductoTercero';
    protected $fillable = ['txCodigoTipoProductoTercero','txNombreTipoProductoTercero','Compania_oidCompania',];
    public $timestamps = false;

}