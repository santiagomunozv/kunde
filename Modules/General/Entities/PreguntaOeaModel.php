<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class PreguntaOeaModel extends Model
{
    protected $table = 'gen_preguntaoea';
    protected $primaryKey = 'oidPreguntaOea';
    protected $fillable = ['txCodigoPreguntaOea','txNombrePreguntaOea','lsTipoPreguntaOea','inPorcentajePreguntaOea','txEstadoPreguntaOea','Compania_oidCompania'];
    public $timestamps = false;

	public function PreguntaOeaDetalle()
    {
        return $this->hasMany('\Modules\General\Entities\PreguntaOeaDetalleModel','PreguntaOea_oidPreguntaOea_1aM','oidPreguntaOea');
    }

}