<?php 
        namespace Modules\General\Entities;
        use Illuminate\Database\Eloquent\Model;

        class RegimenContableModel extends Model
        {
            protected $table = 'gen_regimencontable';
            protected $primaryKey = 'oidRegimenContable';
            protected $fillable = ['txCodigoRegimenContable','txNombreRegimenContable'];
            public $timestamps = false;
        
        }