<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class OperacionProcesoDetalleModel extends Model
{
    protected $table = 'gen_operacionprocesodetalle';
    protected $primaryKey = 'oidOperacionProcesoDetalle';
    protected $fillable = ['OperacionProceso_oidOperacionProceso_1aM','txSecuenciaOperacionProcesoDetalle','txNombreOperacionProcesoDetalle','deSamOperacionProcesoDetalle','txObservacionOperacionProcesoDetalle',];
    public $timestamps = false;

}