<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ClasificacionTerceroRolModel extends Model
{
    protected $table = 'gen_clasificaciontercerorol';
    protected $primaryKey = 'oidClasificacionTerceroRol';
    protected $fillable = ['ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM','Rol_oidClasificacionTerceroRol','chModificarClasificacionTerceroRol'];
    public $timestamps = false;
}