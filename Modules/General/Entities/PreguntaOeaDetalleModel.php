<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class PreguntaOeaDetalleModel extends Model
{
    protected $table = 'gen_preguntaoeadetalle';
    protected $primaryKey = 'oidPreguntaOeaDetalle';
    protected $fillable = ['PreguntaOea_oidPreguntaOea_1aM','inCalificacionPreguntaOeaDetalle','txTextoPreguntaOeaDetalle','txObservacionPreguntaOeaDetalle','daVigenciaPreguntaOeaDetalle'];
    public $timestamps = false;

}