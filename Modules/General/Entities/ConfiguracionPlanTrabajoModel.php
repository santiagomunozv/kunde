<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ConfiguracionPlanTrabajoModel extends Model
{
    protected $table = 'gen_configuracionplantrabajo';
    protected $primaryKey = 'oidConfiguracionPlanTrabajo';
    protected $fillable = ['txActividadProgramadaConfiguracionPlanTrabajo','txMesConfiguracionPlanTrabajo', 'chSugerirConfiguracionPlanTrabajo'];
    public $timestamps = false;

}