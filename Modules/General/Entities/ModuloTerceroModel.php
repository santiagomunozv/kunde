<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ModuloTerceroModel extends Model
{
    protected $table = 'gen_modulotercero';
    protected $primaryKey = 'oidModuloTercero';
    protected $fillable = ['txNombreModuloTercero','txRutaImagenModuloTercero','txRutaAccionModuloTercero',];
    public $timestamps = false;

	public function ModuloTerceroCorreo()
    {
        return $this->hasOne('\Modules\General\Entities\ModuloTerceroCorreoModel','ModuloTercero_oidModuloTercero_1a1','oidModuloTercero');
    }

}