<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TipoContactoTerceroModel extends Model
{
    protected $table = 'gen_tipocontactotercero';
    protected $primaryKey = 'oidTipoContactoTercero';
    protected $fillable = ['ClasificacionTercero_oidTipoContactoTercero','txCodigoTipoContactoTercero','txNombreTipoContactoTercero','txEstadoTipoContactoTercero'];
    public $timestamps = false;

}