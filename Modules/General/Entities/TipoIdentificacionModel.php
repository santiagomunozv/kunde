<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class TipoIdentificacionModel extends Model
{
    protected $table = 'gen_tipoidentificacion';
    protected $primaryKey = 'oidTipoIdentificacion';
    protected $fillable = ['txCodigoTipoIdentificacion','txCodigoDIANTipoIdentificacion','txCodigoNominaTipoIdentificacion','txNombreTipoIdentificacion','txEstadoTipoIdentificacion','lsTipoNombreTipoIdentificacion','chDocumentoAutomaticoTipoIdentificacion','txDocumentoPrefijoTipoIdentificacion','inDocumentoLongitudTipoIdentificacion','chDocumentoVerificacionTipoIdentificacion','chReportarUbicacionMediosTipoIdentificacion',];
    public $timestamps = false;

}