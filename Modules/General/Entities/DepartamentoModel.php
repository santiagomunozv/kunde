<?php 
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class DepartamentoModel extends Model{
    protected $table = 'gen_departamento';
    protected $primaryKey = 'oidDepartamento';
    protected $fillable = ['Pais_oidDepartamento','txCodigoDepartamento','txNombreDepartamento','txEstadoDepartamento',];
    public $timestamps = false;

    public function ciudades(){
        return $this->hasMany('Modules\General\Entities\CiudadModel','Departamento_oidDepartamento_1aM');
    }

    public function pais(){
        return $this->belongsTo('Modules\General\Entities\PaisModel' , 'Pais_oidDepartamento');
    }
}