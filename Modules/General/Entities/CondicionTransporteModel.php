<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class CondicionTransporteModel extends Model
{
    protected $table = 'gen_condiciontransporte';
    protected $primaryKey = 'oidCondicionTransporte';
    protected $fillable = ['txCodigoCondicionTransporte','txNombreCondicionTransporte','lsTipoCondicionTransporte','txEstadoCondicionTransporte',];
    public $timestamps = false;

	public function CondicionTransporteDetalle()
    {
        return $this->hasMany('\Modules\General\Entities\CondicionTransporteDetalleModel','CondicionTransporte_oidCondicionTransporte_1aM','oidCondicionTransporte');
    }

}