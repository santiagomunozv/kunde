<?php 
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

        class ClasificacionTerceroConfigModel extends Model
        {
            protected $table = 'gen_clasificacionterceroconfig';
            protected $primaryKey = 'oidClasificacionTerceroConfig';
            protected $fillable = ['ClasificacionTercero_oidClasificacionTercero_1aM','ModuloTercero_ClasificacionTerceroConfig','chCorreoClasificacionTerceroConfig'];
            public $timestamps = false;

            public function modulo(){
                return $this->belongsTo('Modules\AsociadoNegocio\Entities\ModuloTerceroModel','ModuloTercero_ClasificacionTerceroConfig');
            }

            public function rol()
            {
                return $this->hasMany('Modules\General\Entities\ClasificacionTerceroRolModel','ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM');
            }
        
        }