<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ActividadEconomicaModel extends Model
{
    protected $table = 'gen_actividadeconomica';
    protected $primaryKey = 'oidActividadEconomica';
    protected $fillable = ['txCodigoActividadEconomica','txNombreActividadEconomica','lsClasificacionActividadEconomica','lsVersionActividadEconomica','txEstadoActividadEconomica',];
    public $timestamps = false;

	public function TerceroActividadEconomica()
    {
        return $this->hasMany('\Modules\AsociadoNegocio\Entities\TerceroActividadEconomicaModel','ActividadEconomica_oidActividadEconomica_1aM','oidActividadEconomica');
    }

}