<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

class CiudadModel extends Model
{
    protected $table = 'gen_ciudad';
    protected $primaryKey = 'oidCiudad';
    protected $fillable = ['Departamento_oidDepartamento_1aM', 'txCodigoCiudad', 'txNombreCiudad', 'txCodigoPostalCiudad'];
    public $timestamps = false;



    public function departamento()
    {
        return $this->belongsTo('Modules\General\Entities\DepartamentoModel', 'Departamento_oidDepartamento_1aM');
    }
}
