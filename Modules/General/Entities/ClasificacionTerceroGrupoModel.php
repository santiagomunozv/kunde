<?php 
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Session;

class ClasificacionTerceroGrupoModel extends Model
        {
            protected $table = 'gen_clasificaciontercerogrupo';
            protected $primaryKey = 'oidClasificacionTerceroGrupo';
            protected $fillable = ['ClasificacionTercero_oidClasificacionTercero_1aM','txNombreClasificacionTerceroGrupo','chMultipleClasificacionTerceroGrupo','chObligatorioClasificacionTerceroGrupo','chCorreoClasificacionTerceroGrupo'];
            public $timestamps = false;


            public function grupoTercero($id)
            {
                return $this->hasMany('Modules\AsociadoNegocio\Entities\GrupoTerceroModel','ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo')->where('ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo','=',$id);
            }

            public function valoresGrupo($idTercero)
            {
                $idCompania = Session::get('oidCompania');
                $valor = DB::select("SELECT GrupoTercero_oidTerceroGrupo FROM asn_tercerogrupo
                LEFT JOIN asn_grupotercero 
                on GrupoTercero_oidTerceroGrupo = oidGrupoTercero 
                where ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = {$this->oidClasificacionTerceroGrupo} 
                AND Tercero_oidTercero_1aM = $idTercero AND Compania_oidCompania = $idCompania");
                
                $assocKeys = array();
                foreach($valor as $key) {
                  array_push($assocKeys,$key->GrupoTercero_oidTerceroGrupo);
                }
                return $assocKeys;
                
            }

            // public function observacionGrupo($idTercero,$idGrupo)
            // {
            //   $observacion = DB::select("SELECT 
            //   txObservacionTerceroGrupo
            //   FROM
            //   kunde.asn_tercerogrupo
            //   where Tercero_oidTercero_1aM = $idTercero
            //   and GrupoTercero_oidTerceroGrupo = $idGrupo");

            //   return $observacion[0]->txObservacionTerceroGrupo;
              
            // }
        
        }