<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ActividadPlanTrabajoModel extends Model
{
    protected $table = 'gen_actividadplantrabajo';
    protected $primaryKey = 'oidActividadPlanTrabajo';
    protected $fillable = ['ProgramacionPlanTrabajo_oidPlanTrabajo_1aM','ConfiguracionPlanTrabajo_oidConfiguracionPlan','daFechaProgramadaActividadPlanTrabajo','txHorasProgramadasActividadPlanTrabajo','txCantidadHorasActividadPlanTrabajo'];
    public $timestamps = false;

}