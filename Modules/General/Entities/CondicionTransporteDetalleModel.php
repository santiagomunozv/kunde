<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class CondicionTransporteDetalleModel extends Model
{
    protected $table = 'gen_condiciontransportedetalle';
    protected $primaryKey = 'oidCondicionTransporteDetalle';
    protected $fillable = ['CondicionTransporte_oidCondicionTransporte_1aM','txObservacionCondicionTransporteDetalle',];
    public $timestamps = false;

}