<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ProcesoLogisticoTerceroModel extends Model
{
    protected $table = 'gen_procesologisticotercero';
    protected $primaryKey = 'oidProcesoLogisticoTercero';
    protected $fillable = ['txCodigoProcesoLogisticoTercero','txNombreProcesoLogisticoTercero','Compania_oidCompania',];
    public $timestamps = false;

}