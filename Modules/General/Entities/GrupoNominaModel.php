<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class GrupoNominaModel extends Model
{
    protected $table = 'gen_gruponomina';
    protected $primaryKey = 'oidGrupoNomina';
    protected $fillable = ['txCodigoGrupoNomina','txNombreGrupoNomina','lsBasePagoGrupoNomina','txEstadoGrupoNomina',];
    public $timestamps = false;

}