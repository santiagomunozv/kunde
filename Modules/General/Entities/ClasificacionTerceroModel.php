<?php 
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;
use DB;

class ClasificacionTerceroModel extends Model
        {
            protected $table = 'gen_clasificaciontercero';
            protected $primaryKey = 'oidClasificacionTercero';
            protected $fillable = ['txCodigoClasificacionTercero','txNombreClasificacionTercero','lmTipoClasificacionTercero','txEstadoClasificacionTercero'];
            public $timestamps = false;

            public function config(){
                return $this->hasMany('Modules\General\Entities\ClasificacionTerceroConfigModel','ClasificacionTercero_oidClasificacionTercero_1aM');
            }

            public function rol(){
                return $this->hasMany('Modules\General\Entities\ClasificacionTerceroRolModel','ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM');
            }
            
            public function grupos(){
                return $this->hasMany('Modules\General\Entities\ClasificacionTerceroGrupoModel','ClasificacionTercero_oidClasificacionTercero_1aM');
            }

            public static function getOpcionMenu($id){
                return DB::select("SELECT oidClasificacionTercero,txNombreClasificacionTercero FROM gen_clasificaciontercero
                inner join gen_clasificacionterceroconfig on ClasificacionTercero_oidClasificacionTercero_1aM = oidClasificacionTercero
                inner join gen_clasificaciontercerorol on ClasificacionTerceroConfig_oidClasificacionTerceroConfig_1aM = oidClasificacionTerceroConfig
                inner join seg_usuariocompaniarol on Rol_oidUsuarioCompaniaRol = Rol_oidClasificacionTerceroRol
                where Usuario_oidUsuario_1aM = $id group by oidClasificacionTercero,txNombreClasificacionTercero");
            }
        }