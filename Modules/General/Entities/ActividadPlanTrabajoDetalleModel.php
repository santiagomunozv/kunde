<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class ActividadPlanTrabajoDetalleModel extends Model
{
    protected $table = 'gen_actividadplantrabajodetalle';
    protected $primaryKey = 'oidActividadPlanTrabajoDetalle';
    protected $fillable = ['ActividadPlanTrabajo_oidActividadPlanTrabajo','txNombreActividadPlanTrabajoDetalle'];
    public $timestamps = false;

}