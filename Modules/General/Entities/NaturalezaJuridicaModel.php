<?php
namespace Modules\General\Entities;
use Illuminate\Database\Eloquent\Model;

class NaturalezaJuridicaModel extends Model
{
    protected $table = 'gen_naturalezajuridica';
    protected $primaryKey = 'oidNaturalezaJuridica';
    protected $fillable = ['txCodigoNaturalezaJuridica','txNombreNaturalezaJuridica','txEstadoNaturalezaJuridica',];
    public $timestamps = false;

}