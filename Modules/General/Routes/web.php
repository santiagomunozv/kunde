<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function(){
    Route::prefix('general')->group(function() {

        Route::resource('/arl', 'ArlController')->except(['show']);

        Route::resource('/configuracionplantrabajo', 'ConfiguracionPlanTrabajoController')
        ->only(['index', 'store']);

        Route::resource('/programacionplantrabajo', 'ProgramacionPlanTrabajoController');

        Route::get('/actividadplantrabajodetalle/{idActividadPlanTrabajo}/modal', 'ProgramacionPlanTrabajoController@getActividadPlantrabajoDetalle');
        Route::put('actividadupdate/{id}', 'ProgramacionPlanTrabajoController@updateActividad');

        Route::resource('/actividadeconomica' , 'ActividadEconomicaController');

        Route::resource('/cargo', 'CargoController');

        Route::resource('/centrocosto', 'CentroCostoController');

        Route::resource('/centrotrabajo', 'CentroTrabajoController');

        Route::resource('/modulotercero', 'ModuloTerceroController');

        Route::resource('/naturalezajuridica', 'NaturalezaJuridicaController');

        Route::resource('/tipoidentificacion', 'TipoIdentificacionController');

        Route::resource('/pais', 'PaisController');

        Route::resource('/departamento' , 'DepartamentoController');

        Route::resource('/clasificaciontercero', 'ClasificacionTerceroController');

        Route::resource('/preguntaoea', 'PreguntaOeaController');

        Route::resource('/tipoContacto', 'TipoContactoTerceroController');

        Route::resource('/tipodocumento', 'TipoDocumentoController');

        Route::get('/clasificaciontercerocorreo','ClasificacionTerceroController@listaCorreo');
        Route::get('/listarmodulotercero','ClasificacionTerceroController@listarModuloTercero');
        Route::get('/listarclasificacionrol','ClasificacionTerceroController@listaRol');

        Route::get('/clasificaciontercerorol','ClasificacionTerceroController@consultaRol');

        Route::get('/matrizriesgo', 'MatrizRiesgoController@index');

        Route::put('/matrizriesgo', 'MatrizRiesgoController@update');

        Route::resource('/tipodeservicios', 'ServicioTerceroController');


    });
});
