@extends('layouts.principal')
@section('nombreModulo')
    Tipo de Identificación
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("tipoidentificacion-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="tipoidentificacion-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/tipoidentificacion', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Código DIAN</th>
                            <th>Código Nómina</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Tipo de Nombre</th>
                            <th>Documento Automático</th>
                            <th>Prefijo</th>
                            <th>Longitud</th>
                            <th>Calcular Dígito Verificación</th>
                            <th>Reportar Ubicación Medios Magnéticos</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tipoidentificacion as $tipoidentificacionreg)
                            <tr
                                class="{{ $tipoidentificacionreg->txEstadoTipoIdentificacion == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/tipoidentificacion', [$tipoidentificacionreg->oidTipoIdentificacion, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $tipoidentificacionreg->oidTipoIdentificacion }}', 'gen_tipoidentificacion', 'TipoIdentificacion')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $tipoidentificacionreg->oidTipoIdentificacion }}', 'gen_tipoidentificacion', 'TipoIdentificacion','{{ $tipoidentificacionreg->txEstadoTipoIdentificacion }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/tipoidentificacion', [$tipoidentificacionreg->oidTipoIdentificacion]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $tipoidentificacionreg->oidTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txCodigoTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txCodigoDIANTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txCodigoNominaTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txNombreTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txEstadoTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->lsTipoNombreTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->chDocumentoAutomaticoTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->txDocumentoPrefijoTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->inDocumentoLongitudTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->chDocumentoVerificacionTipoIdentificacion }}</td>
                                <td>{{ $tipoidentificacionreg->chReportarUbicacionMediosTipoIdentificacion }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Código DIAN</th>
                            <th>Código Nómina</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Tipo de Nombre</th>
                            <th>Documento Automático</th>
                            <th>Prefijo</th>
                            <th>Longitud</th>
                            <th>Calcular Dígito Verificación</th>
                            <th>Reportar Ubicación Medios Magnéticos</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
