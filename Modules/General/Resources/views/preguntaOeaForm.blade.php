@extends("layouts.principal")
@section("nombreModulo")
    Preguntas
@endsection
@section("scripts")
    {{Html::script("modules/general/js/preguntaOeaForm.js")}}
     
    <script>
        var PreguntaOeaDetalle = {!! $preguntaoea->PreguntaOeaDetalle !!};
    
        
    </script>
@endsection
@section("contenido")
    @if(isset($preguntaoea->oidPreguntaOea))
        {!!Form::model($preguntaoea,["route"=>["preguntaoea.update",$preguntaoea->oidPreguntaOea],"method"=>"PUT", "id"=>"form-preguntaoea" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($preguntaoea,["route"=>["preguntaoea.store",$preguntaoea->oidPreguntaOea],"method"=>"POST", "id"=>"form-preguntaoea", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidPreguntaOea', null, array('id' => 'oidPreguntaOea')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoPreguntaOea', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoPreguntaOea',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombrePreguntaOea', 'Pregunta', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombrePreguntaOea',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Pregunta'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsTipoPreguntaOea', 'Tipo de Pregunta', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('lsTipoPreguntaOea',['Confeccion' => 'Confeccion','Comercializacion' => 'Comercializacion',],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Tipo de Pregunta'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inPorcentajePreguntaOea', 'Porcentaje', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('inPorcentajePreguntaOea',null,['class'=>'text-right form-control','placeholder'=>'Ingresa Porcentaje'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('Compania_oidCompania', 'Compañia', array('class' => 'text-md text-primary mb-1  ')) !!}
                    {!!Form::select('Compania_oidCompania',$compania,null,['class'=>'form-control', 'id' =>'Compania_oidCompania'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoPreguntaOea', 'Estado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txEstadoPreguntaOea',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Clasificaciòn</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <input type="hidden" name="eliminarPreguntaOeaDetalle" id="eliminarPreguntaOeaDetalle"/>
                            <table class="table table-hover table-borderless table-sm">
                                <thead class="bg-primary text-light">
                                    <tr>
                                        <th>
                                            <button class="btn btn-primary btn-sm" onclick="gen_preguntaoeadetalle.agregarCampos(valorPreguntaOeaDetalle, 'A');">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </th>
                                        <th class="required " style="width: 9%;" >Calificación</th>
                                        <th class="required " style="width: 27%;" >Texto Calificación</th>
                                        <th class="required " style="width: 40%;" >Observación</th>
                                        <th class="required " style="width: 30%;" >Vigencia</th>
                                    </tr>
                                </thead>
                                <tbody id="contenedor_gen_preguntaoeadetalle"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($preguntaoea->oidPreguntaOea))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        