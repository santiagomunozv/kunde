@extends("layouts.principal")
@section("nombreModulo")
 Tipo Contacto tercero
@endsection
@section("scripts")
    {{Html::script("modules/general/js/TipoContactoTercero.js")}}
    
@endsection
@section("contenido")
    @if(isset($tipoContacto->oidTipoContactoTercero))
        {!!Form::model($tipoContacto,["route"=>["tipoContacto.update",$tipoContacto->oidTipoContactoTercero],"method"=>"PUT", "id"=>"form-tipoContacto" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($tipoContacto,["route"=>["tipoContacto.store",$tipoContacto->oidTipoContactoTercero],"method"=>"POST", "id"=>"form-tipoContacto", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCargo', null, array('id' => 'oidCargo')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txCodigoTipoContactoTercero', 'Código tipo contacto', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoTipoContactoTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreTipoContactoTercero', 'Nombre tipo contacto', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreTipoContactoTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('ClasificacionTercero_oidTipoContactoTercero', 'Clasificación tercero', array('class' => 'control-label required ')) !!}
                    {!!Form::select('ClasificacionTercero_oidTipoContactoTercero',$clasificacionTercero,null,[ 'class'=>'chosen-select','id'=>'ClasificacionTercero_oidTipoContactoTercero','multiple','autocomplete'=>'off'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txEstadoTipoContactoTercero', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoTipoContactoTercero',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($tipoContacto->oidTipoContactoTercero))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        