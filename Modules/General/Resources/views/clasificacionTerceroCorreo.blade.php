<div style="height: 400px;">
    <div class="row">
        {!!Form::hidden('oidModuloTerceroCorreo',null, array('id' => 'oidModuloTerceroCorreo')) !!}
        <div class="col-sm-6">
            <div class="form-group ">
            {!!Form::label('txParaModuloTerceroCorreo', 'Para', array('class' => 'control-label required')) !!}
            {!!Form::text('txParaModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa un correo'])!!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group ">
            {!!Form::label('txCopiaModuloTerceroCorreo', 'Copia', array('class' => 'control-label')) !!}
            {!!Form::text('txCopiaModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa un correo'])!!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group ">
            {!!Form::label('txAsuntoModuloTerceroCorreo', 'Asunto', array('class' => 'control-label required')) !!}
            {!!Form::text('txAsuntoModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa un asunto'])!!}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group ">
            {!!Form::label('txMensajeModuloTerceroCorreo', 'Mensaje', array('class' => 'control-label')) !!}
            {!!Form::textarea('txMensajeModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa un mensaje'])!!}
            </div>
        </div>
    </div>
</div>
<hr>
<div id="alertaCampos" class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
    <strong>Error</strong> Por favor verifica los campos indicados
</div>
    
    