<div class="table-responsive">
    <table id="modulos-table" class="table table-hover table-sm scalia-grid">
        <thead class="bg-primary text-light">
            <tr>
                <th>Nombre</th>
            </tr>
        </thead>
        <tbody>
            @foreach($modulos as $modulo)
                <tr data-id-modulo="{{$modulo->oidModuloTercero}}" data-nombre-modulo="{{$modulo->txNombreModuloTercero}}">
                    <td>{{$modulo->txNombreModuloTercero}}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>