<div id="sm-messages" class="sm-messages"></div>
<div class="table-responsive" id="multi-container" style="height: 400px;overflow:auto;">
    @yield('multiregistro')
</div>
@yield("calculator")
<div class="d-none" id="lg-messages">
    <div class="col text-right">
        <button type="button" id="btnToggleToSmContainer" class="btn btn-sm btn-primary my-2" onclick="switchToSmContainer( this )">
            <i class="fas fa-hand-point-left"></i>
            <span class="d-none d-lg-inline">Volver</span>
        </button>
    </div>
    <div id="messages-container" style="height: 400px;overflow:auto;"></div>
</div>