<div class="table-responsive" style="height: 400px;">
        <table class="table multiregistro table-sm table-hover table-borderless">
            <thead>
                <tr>
                    <th class="bg-primary text-light">
                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="rol.agregarCampos([0,null,0] , 'A');">
                            <i class="fa fa-plus"></i>
                        </button>
                    </th>
                    <th class="required bg-primary text-light">Rol</th>
                    <th class="required bg-primary text-light">Modificar</th>
                </tr>            
            </thead>
            <tbody id="tRol"></tbody>
        </table>
    </div>
    <hr>
    <div id="alertaCampos" class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
        <strong>Error</strong> Por favor verifica los campos indicados
    </div>
    <div id="alertaRepetido" class="alert alert-warning alert-dismissible fade show" role="alert" style="display: none;">
        <strong>Error</strong> No se permite seleccionar dos veces el mismo rol
    </div>
    
    