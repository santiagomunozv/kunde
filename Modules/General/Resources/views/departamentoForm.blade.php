@extends('layouts.principal')
@section('nombreModulo')
    Departamento
@stop
@section('scripts')
    <script>
        var ciudades = {!! $ciudades !!};
    </script>
    {!!Html::script('modules/general/js/departamentoForm.js')!!}
@endsection

@section('contenido')
    @if(isset($departamento->oidDepartamento))
        {!!Form::model($departamento,['route'=>['departamento.update', $departamento->oidDepartamento],'method'=>'PUT', 'id'=>'form-departamento' , 'onsubmit' => 'return false;'])!!}
	@else
        {!!Form::model($departamento,['route'=>['departamento.store',$departamento->oidDepartamento],'method'=>'POST', 'id'=>'form-departamento', 'onsubmit' => 'return false;'])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {!!Form::hidden('oidDepartamento', null, ['id' => 'oidDepartamento']) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-grop">
                        {!!Form::label('Pais_oidDepartamento', 'Pais', array('class' => 'text-md text-primary mb-1 required')) !!}
                        {!!Form::select('Pais_oidDepartamento',$paises,null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Pais'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txCodigoDepartamento', 'Código', array('class' => 'text-md text-primary mb-1 required')) !!}
                        {!!Form::text('txCodigoDepartamento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código' , 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txNombreDepartamento', 'Nombre', array('class' => 'text-md text-primary mb-1 required')) !!}
                        {!!Form::text('txNombreDepartamento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre' , 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txEstadoDepartamento', 'Estado', array('class' => 'text-md text-primary mb-1 required')) !!}
                        {!!Form::text('txEstadoDepartamento',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado' , 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6 pt-3">
                    <div class="table-responsive">
                        <input type="hidden" name="eliminarCiudad" id="eliminarCiudad"/>
                        <table class="table table-hover table-borderless table-sm">
                            <thead class="bg-primary text-light">
                                <tr>
                                    <th>
                                        <button class="btn btn-primary btn-sm" onclick="gen_ciudad.agregarCampos(['0','','',], 'A')">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </th>
                                    <th class="required">Código</th>
                                    <th style="width:65%" class="required">Nombre</th>
                                    <th>Código Postal</th>
                                </tr>
                            </thead>
                            <tbody id="contenedor_gen_ciudad"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if(isset($departamento->oidDepartamento))
                {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button('Adicionar',['type' => 'button' , "class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
        {!! Form::close() !!}
        </div>
    </div>
@stop