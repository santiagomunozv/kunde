@extends('layouts.principal')
@section('nombreModulo')
    Formas de Pago
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("formapago-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="formapago-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/formapago', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Forma de Pago</th>
                            <th>Vencimiento</th>
                            <th>Afecta Cartera</th>
                            <th>Afecta Cartera Crédito</th>
                            <th>Tipo</th>
                            <th>Estado</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($formapago as $formapagoreg)
                            <tr class="{{ $formapagoreg->txEstadoFormaPago == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/formapago', [$formapagoreg->oidFormaPago, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $formapagoreg->oidFormaPago }}', 'gen_formapago', 'FormaPago')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $formapagoreg->oidFormaPago }}', 'gen_formapago', 'FormaPago','{{ $formapagoreg->txEstadoFormaPago }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/formapago', [$formapagoreg->oidFormaPago]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $formapagoreg->oidFormaPago }}</td>
                                <td>{{ $formapagoreg->txCodigoFormaPago }}</td>
                                <td>{{ $formapagoreg->txNombreFormaPago }}</td>
                                <td>{{ $formapagoreg->txVencimientoFormaPago }}</td>
                                <td class="text-center"><?php echo $formapagoreg->chAfectaCFormaPago ? '<i class="fas fa-check text-center" style="color:green"></i>' : '<i class="fas fa-times text-center" style="color:red"></i>'; ?></td>
                                <td class="text-center"><?php echo $formapagoreg->chAfectaCCFormaPago ? '<i class="fas fa-check text-center" style="color:green"></i>' : '<i class="fas fa-times text-center" style="color:red"></i>'; ?></td>
                                <td>{{ $formapagoreg->lsTipoFormaPago }}</td>
                                <td>{{ $formapagoreg->txEstadoFormaPago }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Forma de Pago</th>
                            <th>Vencimiento</th>
                            <th>Afecta Cartera</th>
                            <th>Afecta Cartera Crédito</th>
                            <th>Tipo</th>
                            <th>Estado</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
