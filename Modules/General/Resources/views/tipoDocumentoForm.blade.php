@extends("layouts.principal")
@section("nombreModulo")
    Tipo de documento
@endsection
@section("scripts")
    {{Html::script("modules/general/js/tipoDocumentoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($tipoDocumento->oidTipoDocumento))
        {!!Form::model($tipoDocumento,["route"=>["tipodocumento.update",$tipoDocumento->oidTipoDocumento],"method"=>"PUT", "id"=>"form-tipodocumento" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($tipoDocumento,["route"=>["tipodocumento.store",$tipoDocumento->oidTipoDocumento],"method"=>"POST", "id"=>"form-tipodocumento", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidTipoDocumento', null, array('id' => 'oidTipoDocumento')) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoTipoDocumento', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoTipoDocumento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el código'])!!}
                    </div>
                </div>
		
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreTipoDocumento', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreTipoDocumento',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
                    </div>
                </div>
			</div>

            @if(isset($tipoDocumento->oidTipoDocumento))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        