@extends("layouts.principal")
@section("nombreModulo")
    Arl
@endsection
@section("scripts")
    {{Html::script("modules/general/js/arlForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($arl->oidArl))
        {!!Form::model($arl,["route"=>["arl.update",$arl->oidArl],"method"=>"PUT", "id"=>"form-arl" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($arl,["route"=>["arl.store",$arl->oidArl],"method"=>"POST", "id"=>"form-arl", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidArl', null, array('id' => 'oidArl')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txCodigoArl', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoArl',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreArl', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreArl',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($arl->oidArl))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        