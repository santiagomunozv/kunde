@extends("layouts.principal")
@section("nombreModulo")
    Formas de Pago
@endsection
@section("scripts")
    {{Html::script("modules/general/js/formaPagoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($formapago->oidFormaPago))
        {!!Form::model($formapago,["route"=>["formapago.update",$formapago->oidFormaPago],"method"=>"PUT", "id"=>"form-formapago" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($formapago,["route"=>["formapago.store",$formapago->oidFormaPago],"method"=>"POST", "id"=>"form-formapago", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidFormaPago', null, array('id' => 'oidFormaPago')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoFormaPago', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoFormaPago',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreFormaPago', 'Forma de Pago', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreFormaPago',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Forma de Pago'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txVencimientoFormaPago', 'Vencimiento', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txVencimientoFormaPago',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Vencimiento'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    &nbsp;
                    <div class="my-1 mr-sm-2">
                        <div class="divCheckHeader">
                            <label class="header-checkbox-container">
                                {!!Form::hidden("chAfectaCFormaPago", null,["id"=>"chAfectaCFormaPago"])!!}
                                             {!!Form::checkbox("chAfectaCFormaPagoC",null, $formapago->chAfectaCFormaPago ? true : false,["class"=>"form-control","onclick"=>"cambioCheckbox('chAfectaCFormaPago')","id"=>"chAfectaCFormaPagoC"])!!}
                                <span class="checkmark" ></span>
                                <label class="etiquetaCheck">{!!Form::label('chAfectaCFormaPagoC', 'Afecta Cartera', array('class' => 'control-label  ')) !!}</label>
                            </label>
                            
                        </div>
                        
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    &nbsp;
                    <div class="my-1 mr-sm-2">
                        <div class="divCheckHeader">
                            <label class="header-checkbox-container">
                                {!!Form::hidden("chAfectaCCFormaPago", null,["id"=>"chAfectaCCFormaPago"])!!}
                                             {!!Form::checkbox("chAfectaCCFormaPagoC",null, $formapago->chAfectaCCFormaPago ? true : false,["class"=>"form-control","onclick"=>"cambioCheckbox('chAfectaCCFormaPago')","id"=>"chAfectaCCFormaPagoC"])!!}
                                <span class="checkmark" ></span>
                                <label class="etiquetaCheck">{!!Form::label('chAfectaCCFormaPagoC', 'Afecta Cartera Crédito', array('class' => 'control-label  ')) !!}</label>
                            </label>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsTipoFormaPago', 'Tipo', array('class' => 'control-label required ')) !!}
                    {!!Form::select('lsTipoFormaPago',['Contado' => 'Contado','Financiacion' => 'Financiacion','Pago en cuotas' => 'Pago en cuotas','Convenio' => 'Convenio','Cheque Postfechado' => 'Cheque Postfechado','Debito Automatico' => 'Debito Automatico','Descuento por Banco' => 'Descuento por Banco',],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Tipo'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoFormaPago', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoFormaPago',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($formapago->oidFormaPago))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        