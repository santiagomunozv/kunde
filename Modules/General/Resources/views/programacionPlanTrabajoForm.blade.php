@extends("layouts.principal")
@section("nombreModulo")
    Programacion plan de trabajo
@endsection
@section("scripts")
<script type="text/javascript">   
    let valores = '<?php echo (isset($configuracion) ? json_encode($configuracion) : "");?>';
    // let valoresDetalle = '<?php echo (isset($preguntaDetalle) ? json_encode($preguntaDetalle) : "");?>';
    
    </script>
    {{Html::script("modules/general/js/programacionPlanTrabajoForm.js")}}
    {{Html::script("modules/general/js/actividadPlanTrabajoForm.js")}}
@endsection
@section("contenido")
    @if(isset($programacionplantrabajo->oidProgramacionPlanTrabajo))
        {!!Form::model($programacionplantrabajo,["route"=>["programacionplantrabajo.update",$programacionplantrabajo->oidProgramacionPlanTrabajo],"method"=>"PUT", "id"=>"form-programacionplantrabajo" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($programacionplantrabajo,["route"=>["programacionplantrabajo.store",$programacionplantrabajo->oidProgramacionPlanTrabajo],"method"=>"POST", "id"=>"form-programacionplantrabajo", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidProgramacionPlanTrabajo', null, array('id' => 'oidProgramacionPlanTrabajo')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroEncargado', 'Encargado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroEncargado',$asn_tercerolistaEmpleado,null,['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Encargado'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidTerceroCompania', 'Compañia', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidTerceroCompania',$asn_tercerolistaCliente,null,['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Compañia'])!!}
                    </div>
                </div>
            </div>
            
            <div class="card-body multi-max">
                <table id="tabla-verificacion" class="table multiregistro table-sm table-hover table-borderless">
                    <input type="hidden" id="eliminarProgramacion" name="eliminarProgramacion" value="">
                    <thead>
                        <tr>
                            <th>
                                <button type="button" class="btn btn-primary btn-sm text-light" onclick="actividad.agregarCampos([],'L');">
                                    <i  class="fa fa-plus"></i>
                                </button>
                            </th>
                            <th class="bg-primary text-light">Sub tarea</th>
                            <th class="bg-primary text-light">Actividad Programada</th>
                            <th class="bg-primary text-light">Mes</th>
                            <th class="bg-primary text-light">Fecha Programada</th>
                            <th class="bg-primary text-light">Hora Programada</th>
                            <th class="bg-primary text-light">Cantidad de horas</th>
                        </tr>
                    </thead>
                    <tbody id="contenedorActividad"></tbody>
                </table>
            </div>
            

            @if(isset($programacionplantrabajo->oidProgramacionPlanTrabajo))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        