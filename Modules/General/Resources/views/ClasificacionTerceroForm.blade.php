@extends('layouts.principal')
@section('nombreModulo')
    Clasificación de Tercero    
@endsection

@section("scripts")
    <script>
        let grupos = '<?php echo json_encode($grupos); ?>';
        let idRol = "<?php echo json_encode($idRol); ?>";
        let nombreRol = '<?php echo json_encode($nombreRol); ?>';
        let tipoClasificacionTercero = '<?php echo isset($clasificacion->lmTipoClasificacionTercero) ? $clasificacion->lmTipoClasificacionTercero : ""; ?>';
        let config = {!! json_encode($config) !!};
    </script>
        {{Html::script("modules/general/js/ClasificacionTercero.js")}}
@endsection
@section('contenido')
    @if(isset($clasificacion->oidClasificacionTercero))
        {!!Form::model($clasificacion,['route'=>['clasificaciontercero.update',$clasificacion->oidClasificacionTercero],'method'=>'PUT', 'id'=>'form-clasificaciontercero', 'onsubmit' => 'return false;'])!!}
    @else
        {!!Form::open(['route'=>['clasificaciontercero.store'],'method'=>'POST', 'id'=>'form-clasificaciontercero', 'onsubmit' => 'return false;'])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" id="eliminarRol" name="eliminarRol" value="">
            {!!Form::hidden('oidClasificacionTercero',null, array('id' => 'oidClasificacionTercero')) !!}
            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('txCodigoClasificacionTercero', 'Codigo', array('class' => 'text-sm text-primary mb-1'))!!}
                    {!!Form::text('txCodigoClasificacionTercero',null,['class'=>'form-control','placeholder'=>'Ingrese el Codigo.'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txNombreClasificacionTercero', 'Nombre', array('class' => 'text-sm text-primary mb-1'))!!}
                    {!!Form::text('txNombreClasificacionTercero',null,['class'=>'form-control','placeholder'=>'Ingrese el Nombre'])!!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    {!!Form::label('lmTipoClasificacionTercero[]', 'Tipo Tercero', array('class' => 'text-sm text-primary mb-1'))!!}
                    {!!Form::select('lmTipoClasificacionTercero[]',['Cliente'=>'Cliente','Proveedor'=>'Proveedor','Empleado'=>'Empleado','SIA'=>'SIA','EntidadSS'=>'Entidad SS','Bancos'=>'Bancos'],null,[ 'class'=>'chosen-select','multiple','id'=>'lmTipoClasificacionTercero', 'style'=>'width:450px;'])!!}
                </div>
                <div class="col-sm-6">
                    {!!Form::label('txEstadoClasificacionTercero', 'Estado', array('class' => 'text-sm text-primary mb-1'))!!}
                    {!!Form::text('txEstadoClasificacionTercero','Activo',['class'=>'form-control','readonly'=>'readonly'])!!}
                </div>
            </div>
        </div> 
    </div>   

    <h3>Configuración</h3>
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="card-header">
                <button type="button" class="btn btn-primary btn-sm text-light" onclick="modalModulo();">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="card-body multi-max">
                <ul style="list-style: none;"  class="js-sortable h-100" id="modulosTercero"></ul>
            </div>
        </div>
    </div>

        <h3>Grupo</h3>
        <!-- Miltiregistro Grupo -->
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <input type="hidden" id="eliminarGrupo" name="eliminarGrupo" value="">                                       
                <div class="card-body multi-max">
                    <table class="table multiregistro table-sm table-hover table-borderless">
                        <thead class="bg-primary text-light">
                            <th width="50px">
                                <button type="button" class="btn btn-primary btn-sm text-light" onclick="clasificaciontercerogrupo.agregarCampos([],'L');">
                                    <i  class="fa fa-plus"></i>
                                </button>
                            </th>
                            <th>Grupo</th>
                            <th>Multiple</th>
                            <th>Obligatorio</th>
                            <th>Correo</th>
                            <tbody id="contenedorclasificaciontercerogrupo"></tbody>
                        </thead>
                    </table>
                </div>
            </div>
        </div><br>
        
    
    @if(isset($clasificacion->oidClasificacionTercero))
        {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
    @else
        {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
    @endif
    {!! Form::close() !!}
@endsection