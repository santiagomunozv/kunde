@extends('layouts.principal')
@section('nombreModulo')
    Actividades Económicas
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("actividadeconomica-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="actividadeconomica-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/actividadeconomica', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Clasificación</th>
                            <th>Versión</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($actividadeconomica as $actividadeconomicareg)
                            <tr
                                class="{{ $actividadeconomicareg->txEstadoActividadEconomica == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/actividadeconomica', [$actividadeconomicareg->oidActividadEconomica, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $actividadeconomicareg->oidActividadEconomica }}', 'gen_actividadeconomica', 'ActividadEconomica')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $actividadeconomicareg->oidActividadEconomica }}', 'gen_actividadeconomica', 'ActividadEconomica','{{ $actividadeconomicareg->txEstadoActividadEconomica }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        {{--
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/actividadeconomica', [$actividadeconomicareg->oidActividadEconomica]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                        --}}
                                    </div>
                                </td>
                                <td>{{ $actividadeconomicareg->oidActividadEconomica }}</td>
                                <td>{{ $actividadeconomicareg->txCodigoActividadEconomica }}</td>
                                <td>{{ $actividadeconomicareg->txNombreActividadEconomica }}</td>
                                <td>{{ $actividadeconomicareg->lsClasificacionActividadEconomica }}</td>
                                <td>{{ $actividadeconomicareg->lsVersionActividadEconomica }}</td>
                                <td>{{ $actividadeconomicareg->txEstadoActividadEconomica }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Clasificación</th>
                            <th>Versión</th>
                            <th>Estado</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
