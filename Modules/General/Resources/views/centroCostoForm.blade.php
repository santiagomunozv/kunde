@extends("layouts.principal")
@section("nombreModulo")
    Centros de costos
@endsection
@section("scripts")
    {{Html::script("modules/general/js/centroCostoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($centrocosto->oidCentroCosto))
        {!!Form::model($centrocosto,["route"=>["centrocosto.update",$centrocosto->oidCentroCosto],"method"=>"PUT", "id"=>"form-centrocosto" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($centrocosto,["route"=>["centrocosto.store",$centrocosto->oidCentroCosto],"method"=>"POST", "id"=>"form-centrocosto", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCentroCosto', null, array('id' => 'oidCentroCosto')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoCentroCosto', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoCentroCosto',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreCentroCosto', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreCentroCosto',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txResponsableCentroCosto', 'Responsable', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txResponsableCentroCosto',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Responsable'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('Compania_oidCompania', 'Compañia', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Compania_oidCompania',$compania,null,['class'=>'form-control','placeholder'=>'Seleccione una Compañia'])!!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoCentroCosto', 'Estado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txEstadoCentroCosto',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($centrocosto->oidCentroCosto))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        