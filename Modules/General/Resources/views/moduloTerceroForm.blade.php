@extends("layouts.principal")
@section("nombreModulo")
    Módulos de Terceros
@endsection
@section("scripts")
    {{Html::script("modules/general/js/moduloTerceroForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($modulotercero->oidModuloTercero))
        {!!Form::model($modulotercero,["route"=>["modulotercero.update",$modulotercero->oidModuloTercero],"method"=>"PUT", "id"=>"form-modulotercero" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($modulotercero,["route"=>["modulotercero.store",$modulotercero->oidModuloTercero],"method"=>"POST", "id"=>"form-modulotercero", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidModuloTercero', null, array('id' => 'oidModuloTercero')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreModuloTercero', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreModuloTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txRutaImagenModuloTercero', 'Imagen', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txRutaImagenModuloTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Imagen'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txRutaAccionModuloTercero', 'Ruta', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txRutaAccionModuloTercero',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Ruta'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">ModuloTerceroCorreo</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    {!!Form::hidden('oidModuloTerceroCorreo', null, array('id' => 'oidModuloTerceroCorreo')) !!}
			<div class="row">
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txParaModuloTerceroCorreo', '', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txParaModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa '])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCopiaModuloTerceroCorreo', '', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txCopiaModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa '])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txAsuntoModuloTerceroCorreo', '', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txAsuntoModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa '])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txMensajeModuloTerceroCorreo', '', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txMensajeModuloTerceroCorreo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa '])!!}
                    </div>
                </div>
			</div>
                </div>
            </div>

            @if(isset($modulotercero->oidModuloTercero))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        