@extends("layouts.principal")
@section("nombreModulo")
    Centros de Trabajo
@endsection
@section("scripts")
    {{Html::script("modules/general/js/centroTrabajoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($centrotrabajo->oidCentroTrabajo))
        {!!Form::model($centrotrabajo,["route"=>["centrotrabajo.update",$centrotrabajo->oidCentroTrabajo],"method"=>"PUT", "id"=>"form-centrotrabajo" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($centrotrabajo,["route"=>["centrotrabajo.store",$centrotrabajo->oidCentroTrabajo],"method"=>"POST", "id"=>"form-centrotrabajo", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCentroTrabajo', null, array('id' => 'oidCentroTrabajo')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoCentroTrabajo', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoCentroTrabajo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoNominaCentroTrabajo', 'Código Nomina', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoNominaCentroTrabajo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código Nomina'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('Tercero_oidSucursal', 'Sucursal', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Tercero_oidSucursal',$asn_tercerolista,null,['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona Sucursal'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreCentroTrabajo', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreCentroTrabajo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('dePorcentajeARPCentroTrabajo', '% ARP', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('dePorcentajeARPCentroTrabajo',null,['class'=>'text-right form-control','placeholder'=>'Ingresa % ARP'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('Compania_oidCompania', 'Compañia', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::select('Compania_oidCompania',$compania,null,['class'=>'form-control','placeholder'=>'Seleccione una Compañia'])!!}
                    </div>
                </div>
			</div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoCentroTrabajo', 'Estado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txEstadoCentroTrabajo',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
            </div>

            @if(isset($centrotrabajo->oidCentroTrabajo))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        