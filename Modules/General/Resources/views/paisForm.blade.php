@extends('layouts.principal')
@section('nombreModulo')
    País
@endsection
@section('scripts')
    {{Html::script('modules/general/js/paisForm.js')}}
@endsection
@section('contenido')
    @if(isset($pais->oidPais))
        {!!Form::model($pais,['route'=>['pais.update',$pais->oidPais],'method'=>'PUT', 'id'=>'form-pais' , 'onsubmit' => 'return false;'])!!}
    @else
        {!!Form::model($pais,['route'=>['pais.store',$pais->oidPais],'method'=>'POST', 'id'=>'form-pais', 'onsubmit' => 'return false;'])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            {!!Form::hidden('oidPais', null, array('id' => 'oidPais')) !!}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-grop">
                        {!!Form::label('txNombrePais', 'Nombre', array('class' => 'text-md text-primary mb-1 required')) !!}
                        {!!Form::text('txNombrePais',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre', 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txCodigoISOPais', 'Código ISO' , ['class' => 'text-md text-primary mb-1 required']) !!}                    
                        {!!Form::text('txCodigoISOPais',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el código ISO', 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txCodigoEANPais', 'Código EAN', ['class' => 'text-md text-primary mb-1']) !!}
                        {!!Form::text('txCodigoEANPais',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el código EAN' , 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txCodigoDIANPais', 'Código DIAN', ['class' => 'text-md text-primary mb-1']) !!}
                        {!!Form::text('txCodigoDIANPais',null, [ 'class'=>'form-control','placeholder'=>'Ingresa Código DIAN' , 'autocomplete' => 'off'])!!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!!Form::label('txEstadoPais', 'Estado', ['class' => 'text-md text-primary mb-1']) !!}
                        {!!Form::text('txEstadoPais', null ,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado', 'autocomplete' => 'off'])!!}
                    </div>
                </div>
            </div>
                @if(isset($pais->oidPais))
                    {!!Form::button('Modificar',['type' => 'button' ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
                @else
                    {!!Form::button('Adicionar',['type' => 'button' ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
                @endif
            {!! Form::close() !!}
        </div>
@endsection