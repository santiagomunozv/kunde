@extends("layouts.principal")
@section("nombreModulo")
    Cargos
@endsection
@section("scripts")
    {{Html::script("modules/general/js/cargoForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($cargo->oidCargo))
        {!!Form::model($cargo,["route"=>["cargo.update",$cargo->oidCargo],"method"=>"PUT", "id"=>"form-cargo" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($cargo,["route"=>["cargo.store",$cargo->oidCargo],"method"=>"POST", "id"=>"form-cargo", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCargo', null, array('id' => 'oidCargo')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txCodigoCargo', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoCargo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreCargo', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreCargo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txEstadoCargo', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoCargo',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($cargo->oidCargo))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        