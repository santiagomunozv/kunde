@extends('layouts.principal')
@section('nombreModulo')
    Naturalezas jurídicas
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("naturalezajuridica-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="naturalezajuridica-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/naturalezajuridica', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Estado</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($naturalezajuridica as $naturalezajuridicareg)
                            <tr
                                class="{{ $naturalezajuridicareg->txEstadoNaturalezaJuridica == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/naturalezajuridica', [$naturalezajuridicareg->oidNaturalezaJuridica, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $naturalezajuridicareg->oidNaturalezaJuridica }}', 'gen_naturalezajuridica', 'NaturalezaJuridica')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $naturalezajuridicareg->oidNaturalezaJuridica }}', 'gen_naturalezajuridica', 'NaturalezaJuridica','{{ $naturalezajuridicareg->txEstadoNaturalezaJuridica }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/naturalezajuridica', [$naturalezajuridicareg->oidNaturalezaJuridica]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $naturalezajuridicareg->oidNaturalezaJuridica }}</td>
                                <td>{{ $naturalezajuridicareg->txCodigoNaturalezaJuridica }}</td>
                                <td>{{ $naturalezajuridicareg->txNombreNaturalezaJuridica }}</td>
                                <td>{{ $naturalezajuridicareg->txEstadoNaturalezaJuridica }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Estado</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
