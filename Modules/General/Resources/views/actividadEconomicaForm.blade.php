@extends("layouts.principal")
@section("nombreModulo")
    Actividades Económicas
@endsection
@section("scripts")
    {{Html::script("modules/general/js/actividadEconomicaForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($actividadeconomica->oidActividadEconomica))
        {!!Form::model($actividadeconomica,["route"=>["actividadeconomica.update",$actividadeconomica->oidActividadEconomica],"method"=>"PUT", "id"=>"form-actividadeconomica" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($actividadeconomica,["route"=>["actividadeconomica.store",$actividadeconomica->oidActividadEconomica],"method"=>"POST", "id"=>"form-actividadeconomica", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidActividadEconomica', null, array('id' => 'oidActividadEconomica')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoActividadEconomica', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoActividadEconomica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreActividadEconomica', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreActividadEconomica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsClasificacionActividadEconomica', 'Clasificación', array('class' => 'text-md text-primary mb-1  ')) !!}
                    {!!Form::select('lsClasificacionActividadEconomica',['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Clasificación'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsVersionActividadEconomica', 'Versión', array('class' => 'text-md text-primary mb-1  ')) !!}
                    {!!Form::select('lsVersionActividadEconomica',['1' => '1','2' => '2'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Versión'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoActividadEconomica', 'Estado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txEstadoActividadEconomica',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            @if(isset($actividadeconomica->oidActividadEconomica))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        