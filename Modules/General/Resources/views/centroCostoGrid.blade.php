@extends('layouts.principal')
@section('nombreModulo')
    Centros de costos
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("centrocosto-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="centrocosto-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/centrocosto', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Responsable</th>
                            <th>Compañia</th>
                            <th>Estado</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($centrocosto as $centrocostoreg)
                            <tr class="{{ $centrocostoreg->txEstadoCentroCosto == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/centrocosto', [$centrocostoreg->oidCentroCosto, 'edit']) !!}">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $centrocostoreg->oidCentroCosto }}', 'gen_centrocosto', 'CentroCosto')"
                                                class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $centrocostoreg->oidCentroCosto }}', 'gen_centrocosto', 'CentroCosto','{{ $centrocostoreg->txEstadoCentroCosto }}')">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        {{--
                                    @if ($permisos['chConsultarRolOpcion'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to('/general/centrocosto' , [$centrocostoreg->oidCentroCosto])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                    --}}
                                    </div>
                                </td>
                                <td>{{ $centrocostoreg->oidCentroCosto }}</td>
                                <td>{{ $centrocostoreg->txCodigoCentroCosto }}</td>
                                <td>{{ $centrocostoreg->txNombreCentroCosto }}</td>
                                <td>{{ $centrocostoreg->txResponsableCentroCosto }}</td>
                                <td>{{ $centrocostoreg->Compania_oidCompania }}</td>
                                <td>{{ $centrocostoreg->txEstadoCentroCosto }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Responsable</th>
                            <th>Compañia</th>
                            <th>Estado</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
