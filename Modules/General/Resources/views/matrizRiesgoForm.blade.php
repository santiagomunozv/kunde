@extends('layouts.principal')

@section('nombreModulo')
    Matriz de riesgo
@endsection

@section('scripts')
<script>
    let matrizRiesgos = '<?php echo json_encode($matrizRiesgo); ?>';
    let idCargo = '<?php echo json_encode($idCargo); ?>';
    let nombreCargo = '<?php echo json_encode($nombreCargo); ?>';
</script>
    {{Html::script("modules/general/js/matrizRiesgoForm.js")}} 
@endsection

@section('contenido')
    {!! Form::model($matrizRiesgo, ['url' => ['general/matrizriesgo'], 'method' => 'PUT', 'id' => 'form-matrizriesgo'])
    !!}

    <div class="div card border-left-primary shadow h-100 py-2">
        <div class="div card-body">
            <input type="hidden" id="eliminarMatriz" name="eliminarMatriz" value="">
            <div class="div card-body multi-max" style="overflow: auto">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <tr text-align="center">
                            <th colspan="4">&nbsp;</th>
                            <th text-align="center" colspan="2">PELIGROS</th>
                            <th>&nbsp;</th>
                            <th text-align="center" colspan="3">CONTROLES EXISTENTES</th>
                            <th text-align="center" colspan="7">EVALUACIÓN DEL RIESGO</th>
                            <th text-align="center" colspan="1">VALORACIÓN DEL RIESGO</th>
                            <th text-align="center" colspan="2">CRITERIOS PARA CONTROLES</th>
                            <th text-align="center" colspan="5">MEDIDAS DE INTERVENCIÓN</th>
                        </tr>
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionMatriz.agregarCampos([],'L');">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Proceso</th>
                        <th>Actividad y/o tarea</th>
                        <th>Rutinaria</th>
                        <th>Clasificación</th>
                        <th>Descripción</th>
                        <th>Efectos posibles</th>
                        <th>Fuente</th>
                        <th>Medio</th>
                        <th>Individuo</th>
                        <th>Nivel de deficiencia</th>
                        <th>Nivel de exposición</th>
                        <th>Nivel de probabilidad</th>
                        <th>Interpretación nivel de probabilidad</th>
                        <th>Nivel de consecuencia</th>
                        <th>Nivel de riesgo e invervención</th>
                        <th>Interpretación nivel de riesgo</th>
                        <th>Aceptabilidad de riesgo</th>
                        <th>N de expuestos</th>
                        <th>Peor consecuencia</th>
                        <th>Eliminación</th>
                        <th>Sustitución</th>
                        <th>Controles de ingeniería</th>
                        <th>Controles administrativos</th>
                        <th>Elementos de protección personal</th>
                        <tbody id="contenedorMatriz">

                        </tbody>
                    </thead>
                </table>
                {!! Form::button('Modificar', ['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"]) !!}
            </div>
        </div>
    </div>

@endsection
