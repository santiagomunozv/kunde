@extends("layouts.principal")
@section("nombreModulo")
    Grupos de Nómina
@endsection
@section("scripts")
    {{Html::script("modules/general/js/grupoNominaForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($gruponomina->oidGrupoNomina))
        {!!Form::model($gruponomina,["route"=>["gruponomina.update",$gruponomina->oidGrupoNomina],"method"=>"PUT", "id"=>"form-gruponomina" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($gruponomina,["route"=>["gruponomina.store",$gruponomina->oidGrupoNomina],"method"=>"POST", "id"=>"form-gruponomina", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidGrupoNomina', null, array('id' => 'oidGrupoNomina')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoGrupoNomina', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoGrupoNomina',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreGrupoNomina', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreGrupoNomina',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsBasePagoGrupoNomina', 'Base de Pago', array('class' => 'control-label required ')) !!}
                    {!!Form::select('lsBasePagoGrupoNomina',['360' => '360','365' => '365',],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Base de Pago'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoGrupoNomina', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoGrupoNomina',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($gruponomina->oidGrupoNomina))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        