@extends('layouts.principal')
@section('nombreModulo')
    Configuracion Plan de Trabajp
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("configuracionplantrabajo-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="configuracionplantrabajo-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/configuracionplantrabajo', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Actividad Programada</th>
                            <th>Mes</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($configuracionplantrabajo as $configuracionplantrabajoreg)
                            <tr
                                class="{{ $configuracionplantrabajoreg->txEstadoConfiguracionPlanTrabajo == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        {{--
                                    @if ($permisos['chModificarRolOpcion'])
                                        <a class="btn btn-success btn-sm" href="{!!URL::to('/general/configuracionplantrabajo' , [$configuracionplantrabajoreg->oidConfiguracionPlanTrabajo , 'edit'])!!}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @endif
                                    --}}
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $configuracionplantrabajoreg->oidConfiguracionPlanTrabajo }}', 'gen_configuracionplantrabajo', 'ConfiguracionPlanTrabajo')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $configuracionplantrabajoreg->oidConfiguracionPlanTrabajo }}', 'gen_configuracionplantrabajo', 'ConfiguracionPlanTrabajo','{{ $configuracionplantrabajoreg->txEstadoConfiguracionPlanTrabajo }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/configuracionplantrabajo', [$configuracionplantrabajoreg->oidConfiguracionPlanTrabajo]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $configuracionplantrabajoreg->oidConfiguracionPlanTrabajo }}</td>
                                <td>{{ $configuracionplantrabajoreg->txActividadProgramadaConfiguracionPlanTrabajo }}</td>
                                <td>{{ $configuracionplantrabajoreg->txMesConfiguracionPlanTrabajo }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Actividad Programada</th>
                            <th>Mes</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
