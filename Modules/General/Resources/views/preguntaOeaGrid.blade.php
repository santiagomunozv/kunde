@extends('layouts.principal')
@section('nombreModulo')
    Preguntas
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid("preguntaoea-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="preguntaoea-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/preguntaoea', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Pregunta</th>
                            <th>Tipo de Pregunta</th>
                            <th>Porcentaje</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($preguntaoea as $preguntaoeareg)
                            <tr class="{{ $preguntaoeareg->txEstadoPreguntaOea == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/preguntaoea', [$preguntaoeareg->oidPreguntaOea, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $preguntaoeareg->oidPreguntaOea }}', 'gen_preguntaoea', 'PreguntaOea')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $preguntaoeareg->oidPreguntaOea }}', 'gen_preguntaoea', 'PreguntaOea','{{ $preguntaoeareg->txEstadoPreguntaOea }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chConsultarRolOpcion'])
                                            <a class="btn btn-info btn-sm" href="{!! URL::to('/general/preguntaoea', [$preguntaoeareg->oidPreguntaOea]) !!}">
                                                <i class="fas fa-print"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $preguntaoeareg->oidPreguntaOea }}</td>
                                <td>{{ $preguntaoeareg->txCodigoPreguntaOea }}</td>
                                <td>{{ $preguntaoeareg->txNombrePreguntaOea }}</td>
                                <td>{{ $preguntaoeareg->lsTipoPreguntaOea }}</td>
                                <td>{{ $preguntaoeareg->inPorcentajePreguntaOea }}</td>
                                <td>{{ $preguntaoeareg->txEstadoPreguntaOea }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Código</th>
                            <th>Pregunta</th>
                            <th>Tipo de Pregunta</th>
                            <th>Porcentaje</th>
                            <th>Estado</th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
