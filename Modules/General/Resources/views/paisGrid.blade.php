@extends('layouts.principal')
@section('nombreModulo')
    Paises
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarGrid('paises-table');
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="paises-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/general/pais', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>ID</th>
                            <th>Código ISO</th>
                            <th>Código EAN</th>
                            <th>Código DIAN</th>
                            <th>Nombre</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paises as $pais)
                            <tr class="{{ $pais->txEstadoPais == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/general/pais', [$pais->oidPais, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $pais->oidPais }}', 'gen_pais', 'Pais')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $pais->oidPais }}', 'gen_pais', 'Pais','{{ $pais->txEstadoPais }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        {{--
                                    @if ($permisos['chConsultarRolOpcion'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to('/pais' , [$pais->oidPais])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                    --}}
                                    </div>
                                </td>
                                <td>{{ $pais->oidPais }}</td>
                                <td>{{ $pais->txCodigoISOPais }}</td>
                                <td>{{ $pais->txCodigoEANPais }}</td>
                                <td>{{ $pais->txCodigoDIANPais }}</td>
                                <td>{{ $pais->txNombrePais }}</td>
                                <td>{{ $pais->txEstadoPais }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
