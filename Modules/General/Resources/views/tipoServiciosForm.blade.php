@extends("layouts.principal")
@section("nombreModulo")
    Tipo de servicios
@endsection
@section("scripts")
    {{Html::script("modules/general/js/serviciosForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($servicios->id))
        {!!Form::model($servicios,["route"=>["tipodeservicios.update",$servicios->id],"method"=>"PUT", "id"=>"form-servicios" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($servicios,["route"=>["tipodeservicios.store",$servicios->id],"method"=>"POST", "id"=>"form-servicios", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('id', null, array('id' => 'id')) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('txNombre', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                        {!!Form::text('txNombre',null,[ 'class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('chEstado', 'Estado', array('class' => 'control-label  ')) !!}
                    {!!Form::select('chEstado',[1 => 'Activo', 0 => 'Inactivo'],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Tun estado'])!!}
                    </div>
                </div>
			</div>

            @if(isset($servicios->id))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        