@extends("layouts.principal")
@section("nombreModulo")
    Tipo de Identificación
@endsection
@section("scripts")
    {{Html::script("modules/general/js/tipoIdentificacionForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($tipoidentificacion->oidTipoIdentificacion))
        {!!Form::model($tipoidentificacion,["route"=>["tipoidentificacion.update",$tipoidentificacion->oidTipoIdentificacion],"method"=>"PUT", "id"=>"form-tipoidentificacion" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($tipoidentificacion,["route"=>["tipoidentificacion.store",$tipoidentificacion->oidTipoIdentificacion],"method"=>"POST", "id"=>"form-tipoidentificacion", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidTipoIdentificacion', null, array('id' => 'oidTipoIdentificacion')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoTipoIdentificacion', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoTipoIdentificacion',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoDIANTipoIdentificacion', 'Código DIAN', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txCodigoDIANTipoIdentificacion',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código DIAN'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoNominaTipoIdentificacion', 'Código Nómina', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txCodigoNominaTipoIdentificacion',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código Nómina'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreTipoIdentificacion', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreTipoIdentificacion',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoTipoIdentificacion', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoTipoIdentificacion',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('lsTipoNombreTipoIdentificacion', 'Tipo de Nombre', array('class' => 'control-label  ')) !!}
                    {!!Form::select('lsTipoNombreTipoIdentificacion',['RAZON' => 'Razón Social','NOMBRE' => 'Nombres y Apellidos',],null,['class'=>'chosen-select form-control','placeholder'=>'Selecciona Tipo de Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('chDocumentoAutomaticoTipoIdentificacion', 'Documento Automático', array('class' => 'control-label  ')) !!}
                    {!!Form::checkbox('chDocumentoAutomaticoTipoIdentificacion',$tipoidentificacion->chDocumentoAutomaticoTipoIdentificacion, null,['class'=>'form-control'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txDocumentoPrefijoTipoIdentificacion', 'Prefijo', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txDocumentoPrefijoTipoIdentificacion',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Prefijo'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('inDocumentoLongitudTipoIdentificacion', 'Longitud', array('class' => 'control-label  ')) !!}
                    {!!Form::text('inDocumentoLongitudTipoIdentificacion',null,['class'=>'text-right form-control','placeholder'=>'Ingresa Longitud'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('chDocumentoVerificacionTipoIdentificacion', 'Calcular Dígito Verificación', array('class' => 'control-label  ')) !!}
                    {!!Form::checkbox('chDocumentoVerificacionTipoIdentificacion',$tipoidentificacion->chDocumentoVerificacionTipoIdentificacion, null,['class'=>'form-control'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('chReportarUbicacionMediosTipoIdentificacion', 'Reportar Ubicación Medios Magnéticos', array('class' => 'control-label  ')) !!}
                    {!!Form::checkbox('chReportarUbicacionMediosTipoIdentificacion',$tipoidentificacion->chReportarUbicacionMediosTipoIdentificacion, null,['class'=>'form-control'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($tipoidentificacion->oidTipoIdentificacion))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        