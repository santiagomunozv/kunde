@extends("layouts.principal")
@section("nombreModulo")
    Naturalezas jurídicas
@endsection
@section("scripts")
    {{Html::script("modules/general/js/naturalezaJuridicaForm.js")}}
    
@endsection
@section("contenido")
    @if(isset($naturalezajuridica->oidNaturalezaJuridica))
        {!!Form::model($naturalezajuridica,["route"=>["naturalezajuridica.update",$naturalezajuridica->oidNaturalezaJuridica],"method"=>"PUT", "id"=>"form-naturalezajuridica" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($naturalezajuridica,["route"=>["naturalezajuridica.store",$naturalezajuridica->oidNaturalezaJuridica],"method"=>"POST", "id"=>"form-naturalezajuridica", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidNaturalezaJuridica', null, array('id' => 'oidNaturalezaJuridica')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txCodigoNaturalezaJuridica', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoNaturalezaJuridica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreNaturalezaJuridica', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreNaturalezaJuridica',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txEstadoNaturalezaJuridica', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoNaturalezaJuridica',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            

            @if(isset($naturalezajuridica->oidNaturalezaJuridica))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        