@extends("layouts.principal")
@section("nombreModulo")
    Configuración Plan de Trabajo
@endsection
@section("scripts")
<script>
    let planes = '<?php echo json_encode($plan); ?>';
    </script>
{{Html::script("modules/general/js/configuracionPlanTrabajoForm.js")}} 
@endsection
@section("contenido")
    {!!Form::model($plan,['url'=>['general/configuracionplantrabajo'],'method'=>'POST', 'id'=>'form-configuracionplantrabajo'])!!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" id="eliminarConfiguracion" name="eliminarConfiguracion" value="">
            <div class="card-body multi-max">
                <table class="table multiregistro table-sm table-hover table-borderless">
                    <thead class="bg-primary text-light">
                        <th width="50px">
                            <button type="button" class="btn btn-primary btn-sm text-light" onclick="configuracionPlan.agregarCampos([],'L');">
                                <i  class="fa fa-plus"></i>
                            </button>
                        </th>
                        <th>Configuración plan de trabajo</th>
                        <th>Mes</th>
                        <th>Sugerencia</th>
                        <tbody id="contenedorConfiguracionPlan"></tbody>
                    </thead>
                </table>
            </div><br>
        
        {!!Form::button('Modificar',['type'=>'button' , "class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}

        </div>
    </div>
@endsection
        