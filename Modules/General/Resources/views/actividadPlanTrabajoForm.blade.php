
    {!!Form::model($actividadPlanTrabajoDetalle,['url'=>['general/actividadupdate',$id],'method'=>'PUT', 'id'=>'form-actividad'])!!}
    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
    {!!Form::hidden('ActividadPlanTrabajo_oidActividadPlanTrabajo',$id, array('id' => 'ActividadPlanTrabajo_oidActividadPlanTrabajo')) !!}

    <div class="card-body multi-max">
        <input type="hidden" id="eliminarActividad" name="eliminarActividad" value="">
        <table id="tabla-asistente" class="table multiregistro table-sm table-hover table-borderless">
            <thead>
                <tr>
                    <th class="bg-primary text-light">
                        <button type="button" class="btn btn-primary btn-sm text-light" onclick="actividad.agregarCampos([],'M');">
                            <i  class="fa fa-plus"></i>
                        </button>
                    </th>
                    <th class="bg-primary text-light">Actividad</th>
                </tr>
            </thead>
            <tbody id="contenedorActividadDetalle"></tbody>
        </table>
    </div>
    
    {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabarActividad()"])!!}
