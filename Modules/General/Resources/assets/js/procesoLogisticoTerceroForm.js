"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-procesologisticotercero";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/procesologisticotercero";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoProcesoLogisticoTercero_Input = $("#txCodigoProcesoLogisticoTercero");
        let txCodigoProcesoLogisticoTercero_AE = txCodigoProcesoLogisticoTercero_Input.val();
        txCodigoProcesoLogisticoTercero_Input.removeClass(errorClass);
        if(!txCodigoProcesoLogisticoTercero_AE){
            txCodigoProcesoLogisticoTercero_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreProcesoLogisticoTercero_Input = $("#txNombreProcesoLogisticoTercero");
        let txNombreProcesoLogisticoTercero_AE = txNombreProcesoLogisticoTercero_Input.val();
        txNombreProcesoLogisticoTercero_Input.removeClass(errorClass);
        if(!txNombreProcesoLogisticoTercero_AE){
            txNombreProcesoLogisticoTercero_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}