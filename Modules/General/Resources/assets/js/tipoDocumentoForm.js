"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-tipodocumento";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipodocumento";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoTipoDocumento_Input = $("#txCodigoTipoDocumento");
        let txCodigoTipoDocumento_AE = txCodigoTipoDocumento_Input.val();
        txCodigoTipoDocumento_Input.removeClass(errorClass);
        if(!txCodigoTipoDocumento_AE){
            txCodigoTipoDocumento_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreTipoDocumento_Input = $("#txNombreTipoDocumento");
        let txNombreTipoDocumento_AE = txNombreTipoDocumento_Input.val();
        txNombreTipoDocumento_Input.removeClass(errorClass);
        if(!txNombreTipoDocumento_AE){
            txNombreTipoDocumento_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }

		return mensajes;
    }
}