"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-modulotercero";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/modulotercero";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txNombreModuloTercero_Input = $("#txNombreModuloTercero");
        let txNombreModuloTercero_AE = txNombreModuloTercero_Input.val();
        txNombreModuloTercero_Input.removeClass(errorClass);
        if(!txNombreModuloTercero_AE){
            txNombreModuloTercero_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txRutaImagenModuloTercero_Input = $("#txRutaImagenModuloTercero");
        let txRutaImagenModuloTercero_AE = txRutaImagenModuloTercero_Input.val();
        txRutaImagenModuloTercero_Input.removeClass(errorClass);
        if(!txRutaImagenModuloTercero_AE){
            txRutaImagenModuloTercero_Input.addClass(errorClass)
            mensajes.push("El campo Imagen es obligatorio");
        }
		let txRutaAccionModuloTercero_Input = $("#txRutaAccionModuloTercero");
        let txRutaAccionModuloTercero_AE = txRutaAccionModuloTercero_Input.val();
        txRutaAccionModuloTercero_Input.removeClass(errorClass);
        if(!txRutaAccionModuloTercero_AE){
            txRutaAccionModuloTercero_Input.addClass(errorClass)
            mensajes.push("El campo Ruta es obligatorio");
        }
		return mensajes;
    }
}