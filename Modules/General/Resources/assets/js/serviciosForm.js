"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-servicios";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipodeservicios";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txNombre_Input = $("#txNombre");
        let txNombre_AE = txNombre_Input.val();
        txNombre_Input.removeClass(errorClass);
        if(!txNombre_AE){
            txNombre_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let chEstado_Input = $("#chEstado");
        let chEstado_AE = chEstado_Input.val();
        chEstado_Input.removeClass(errorClass);
        if(!chEstado_AE){
            chEstado_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }

		return mensajes;
    }
}

function eliminar(id) {
    $.ajax({
        url: '/general/tipodeservicios/' + id,
        data: {
            "_token": $("meta[name='csrf-token']").attr("content"),
            "_method": "DELETE"
        },
        type: 'POST',
        dataType: "json",
        success: function(result) {
            modal.establecerAccionCerrar(function(){
                location.href = "/general/tipodeservicios";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }
    });
}