"use strict";
let errorClass = "is-invalid";


                $(function(){
                    $('.editorTexto').summernote({ height: 150 });
                });


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-moduloproducto";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/moduloproducto";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txNombreModuloProducto_Input = $("#txNombreModuloProducto");
        let txNombreModuloProducto_AE = txNombreModuloProducto_Input.val();
        txNombreModuloProducto_Input.removeClass(errorClass);
        if(!txNombreModuloProducto_AE){
            txNombreModuloProducto_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txRutaImagenModuloProducto_Input = $("#txRutaImagenModuloProducto");
        let txRutaImagenModuloProducto_AE = txRutaImagenModuloProducto_Input.val();
        txRutaImagenModuloProducto_Input.removeClass(errorClass);
        if(!txRutaImagenModuloProducto_AE){
            txRutaImagenModuloProducto_Input.addClass(errorClass)
            mensajes.push("El campo Imagen es obligatorio");
        }
		let txRutaAccionModuloProducto_Input = $("#txRutaAccionModuloProducto");
        let txRutaAccionModuloProducto_AE = txRutaAccionModuloProducto_Input.val();
        txRutaAccionModuloProducto_Input.removeClass(errorClass);
        if(!txRutaAccionModuloProducto_AE){
            txRutaAccionModuloProducto_Input.addClass(errorClass)
            mensajes.push("El campo Accion es obligatorio");
        }
		return mensajes;
    }
}