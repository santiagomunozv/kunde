"use strict";
let errorClass = "is-invalid";


function cambioCheckbox(campo){
    if($("#"+campo+"C").prop("checked")){
        $("#"+campo).val("1");
    }else{
        $("#"+campo).val("0");
    }
}


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-formapago";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/formapago";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoFormaPago_Input = $("#txCodigoFormaPago");
        let txCodigoFormaPago_AE = txCodigoFormaPago_Input.val();
        txCodigoFormaPago_Input.removeClass(errorClass);
        if(!txCodigoFormaPago_AE){
            txCodigoFormaPago_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreFormaPago_Input = $("#txNombreFormaPago");
        let txNombreFormaPago_AE = txNombreFormaPago_Input.val();
        txNombreFormaPago_Input.removeClass(errorClass);
        if(!txNombreFormaPago_AE){
            txNombreFormaPago_Input.addClass(errorClass)
            mensajes.push("El campo Forma de Pago es obligatorio");
        }
		let txVencimientoFormaPago_Input = $("#txVencimientoFormaPago");
        let txVencimientoFormaPago_AE = txVencimientoFormaPago_Input.val();
        txVencimientoFormaPago_Input.removeClass(errorClass);
        if(!txVencimientoFormaPago_AE){
            txVencimientoFormaPago_Input.addClass(errorClass)
            mensajes.push("El campo Vencimiento es obligatorio");
        }
		let lsTipoFormaPago_Input = $("#lsTipoFormaPago");
        let lsTipoFormaPago_AE = lsTipoFormaPago_Input.val();
        lsTipoFormaPago_Input.removeClass(errorClass);
        if(!lsTipoFormaPago_AE){
            lsTipoFormaPago_Input.addClass(errorClass)
            mensajes.push("El campo Tipo es obligatorio");
        }
		let txEstadoFormaPago_Input = $("#txEstadoFormaPago");
        let txEstadoFormaPago_AE = txEstadoFormaPago_Input.val();
        txEstadoFormaPago_Input.removeClass(errorClass);
        if(!txEstadoFormaPago_AE){
            txEstadoFormaPago_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}