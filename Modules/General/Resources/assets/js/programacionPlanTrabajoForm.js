"use strict";
var errorClass = "is-invalid";
var registros = JSON.parse(valores);
var actividad = [];


$(function(){    
    actividad = new GeneradorMultiRegistro('actividad','contenedorActividad','actividad');
    actividad.campoid = 'oidActividadPlanTrabajo';
    actividad.campoEliminacion = 'eliminarProgramacion';
    actividad.botonEliminacion = true;
    actividad.etiqueta = ['input','input','button','input','input','input','input','input'];
    actividad.tipo = ['hidden','hidden','button','text','text','date','time','number'];
    actividad.campos = ['oidActividadPlanTrabajo','ConfiguracionPlanTrabajo_oidConfiguracionPlan','btnAdicionarSubTarea','txActividadProgramadaConfiguracionPlanTrabajo', 'txMesConfiguracionPlanTrabajo', 'daFechaProgramadaActividadPlanTrabajo','txHorasProgramadasActividadPlanTrabajo','txCantidadHorasActividadPlanTrabajo']
    actividad.opciones = ['','','','','','','', ''];
    actividad.funciones = ['','','','','','','',''];
    actividad.clase = ['','','fa fa-folder-open','','','','',''];
    actividad.sololectura = [true,true,true,false,false,false,false,false];

    let reg = 0;
    while (reg < registros.length) {
        actividad.agregarCampos(registros[reg] , 'L')
        if(document.getElementById("oidActividadPlanTrabajo"+reg).value){
            document.getElementById("btnAdicionarSubTarea"+reg).disabled = false;
            $("#btnAdicionarSubTarea"+reg).attr('onClick', 'modalSubTarea('+document.getElementById("oidActividadPlanTrabajo"+reg).value+')');
        }
        reg = reg + 1;

    }
});

function modalSubTarea(idActividad) {

    modal.cargando()
    $.get('/general/actividadplantrabajodetalle/'+idActividad+'/modal', function (resp) {

        modal.mostrarModal('Programación de Subtareas ', resp.view, function () {
            modal.cargando();
        }).extraGrande().sinPie();

            var registroActividades = resp.data;
            actividad = new GeneradorMultiRegistro('actividad','contenedorActividadDetalle','actividad');
            actividad.campoid = 'oidActividadPlanTrabajoDetalle';
            actividad.campoEliminacion = 'eliminarActividad';
            actividad.etiqueta = ['input','input','input'];
            actividad.tipo = ['hidden','hidden','text'];
            actividad.campos = ['oidActividadPlanTrabajoDetalle','ActividadPlanTrabajo_oidActividadPlanTrabajo','txNombreActividadPlanTrabajoDetalle'];
            actividad.opciones = ['','',''];
            actividad.funciones = ['','',''];
            actividad.clase = ['','',''];
            actividad.sololectura = [true,true,false];
            let regs = 0;
            while (regs < registroActividades.length) {
                actividad.agregarCampos(registroActividades[regs] , 'L')
                regs = regs + 1;
            }
    },'json').fail(function (resp) {
        modal.mostrarModal('OOOOOPPPSSSS ocurrió un problema', 'status:' + resp.status);
    });
}


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-programacionplantrabajo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/programacionplantrabajo/"+resp.oidProgramacionPlanTrabajo+'/edit';
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let Tercero_oidTerceroEncargado_Input = $("#Tercero_oidTerceroEncargado");
        let Tercero_oidTerceroEncargado_AE = Tercero_oidTerceroEncargado_Input.val();
        Tercero_oidTerceroEncargado_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroEncargado_AE){
            Tercero_oidTerceroEncargado_Input.addClass(errorClass)
            mensajes.push("El campo Encargado es obligatorio");
        }
		let Tercero_oidTerceroCompania_Input = $("#Tercero_oidTerceroCompania");
        let Tercero_oidTerceroCompania_AE = Tercero_oidTerceroCompania_Input.val();
        Tercero_oidTerceroCompania_Input.removeClass(errorClass);
        if(!Tercero_oidTerceroCompania_AE){
            Tercero_oidTerceroCompania_Input.addClass(errorClass)
            mensajes.push("El campo Compañia es obligatorio");
        }
		return mensajes;
    }
}