"use strict";
var errorClass = "is-invalid";
var configuracionPlanes = JSON.parse(planes);
var configuracionPlan = [];

$(function(){
    //generamos la multiregistro de grupo
    configuracionPlan = new GeneradorMultiRegistro('configuracionPlan','contenedorConfiguracionPlan','configuracionPlan');

    configuracionPlan.campoid = 'oidConfiguracionPlanTrabajo';
    configuracionPlan.campoEliminacion = 'eliminarConfiguracion';
    configuracionPlan.botonEliminacion = true;
    configuracionPlan.funcionEliminacion = '';  
    configuracionPlan.campos = ['oidConfiguracionPlanTrabajo','txActividadProgramadaConfiguracionPlanTrabajo','txMesConfiguracionPlanTrabajo', 'chSugerirConfiguracionPlanTrabajo'];
    configuracionPlan.etiqueta = ['input','input','input', 'checkbox'];
    configuracionPlan.tipo = ['hidden','text','number','checkbox'];
    configuracionPlan.estilo = ['','','',''];
    configuracionPlan.clase = ['','','',''];
    configuracionPlan.sololectura = [true,false,false,false];
    configuracionPlan.opciones = ['','','',''];
    configuracionPlan.funciones = ['','','',''];
    configuracionPlan.otrosAtributos = ['','','',''];


    configuracionPlanes.forEach( dato => {configuracionPlan.agregarCampos(dato , 'L');
    });
});


function grabar(){
    modal.cargando();
    let mensajes = [];
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-configuracionplantrabajo";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/configuracionplantrabajo";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }
}