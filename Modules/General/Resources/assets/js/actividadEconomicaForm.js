"use strict";
var errorClass = "is-invalid";


$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-actividadeconomica";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/actividadeconomica";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoActividadEconomica_Input = $("#txCodigoActividadEconomica");
        let txCodigoActividadEconomica_AE = txCodigoActividadEconomica_Input.val();
        txCodigoActividadEconomica_Input.removeClass(errorClass);
        if(!txCodigoActividadEconomica_AE){
            txCodigoActividadEconomica_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreActividadEconomica_Input = $("#txNombreActividadEconomica");
        let txNombreActividadEconomica_AE = txNombreActividadEconomica_Input.val();
        txNombreActividadEconomica_Input.removeClass(errorClass);
        if(!txNombreActividadEconomica_AE){
            txNombreActividadEconomica_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txEstadoActividadEconomica_Input = $("#txEstadoActividadEconomica");
        let txEstadoActividadEconomica_AE = txEstadoActividadEconomica_Input.val();
        txEstadoActividadEconomica_Input.removeClass(errorClass);
        if(!txEstadoActividadEconomica_AE){
            txEstadoActividadEconomica_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}