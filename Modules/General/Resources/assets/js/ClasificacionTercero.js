'use strict';
$(function(){
    iniciarChosenSelect();
    llenarSelectClasificacion(tipoClasificacionTercero);
})
let rol = [];
let errorClass = "is-invalid";
let clasificaciontercerogrupo = [];
let claseSeleccion = 'table-info';
let arrayModulos =[];

let validarRoles = ['onchange','validarRol();'];
let roles = [JSON.parse(idRol), JSON.parse(nombreRol)];
$(function(){
    //generamos la multiregistro de grupo
    clasificaciontercerogrupo = new GeneradorMultiRegistro('clasificaciontercerogrupo','contenedorclasificaciontercerogrupo','clasificaciontercerogrupo');

    clasificaciontercerogrupo.campoid = 'oidClasificacionTerceroGrupo';
    clasificaciontercerogrupo.campoEliminacion = 'eliminarGrupo';
    clasificaciontercerogrupo.botonEliminacion = true;
    clasificaciontercerogrupo.funcionEliminacion = '';  
    clasificaciontercerogrupo.campos = ['oidClasificacionTerceroGrupo','txNombreClasificacionTerceroGrupo','chMultipleClasificacionTerceroGrupo','chObligatorioClasificacionTerceroGrupo','chCorreoClasificacionTerceroGrupo'];
    clasificaciontercerogrupo.etiqueta = ['input','input','checkbox','checkbox','checkbox'];
    clasificaciontercerogrupo.tipo = ['hidden','text','','',''];
    clasificaciontercerogrupo.estilo = ['','','','',''];
    clasificaciontercerogrupo.clase = ['','','','',''];
    clasificaciontercerogrupo.sololectura = [true,false,false,false,false];
    clasificaciontercerogrupo.opciones = ['','','','',''];
    clasificaciontercerogrupo.funciones = ['','','','',''];
    clasificaciontercerogrupo.otrosAtributos = ['','','','',''];

    let datos = JSON.parse(grupos);
    let columna = new Array();
    let valorclasificacionterceroGrupo = new Array();

    for(let j in datos){
        for (var i in datos[j]){
            columna.push(datos[j][i]);
        }
        valorclasificacionterceroGrupo.push(columna);
        columna = [];
    }
    if(valorclasificacionterceroGrupo == ''){
        valorclasificacionterceroGrupo = [['','Gestión'],['','Lider'],['','Sublider'],['','Criticidad']];
    }
    valorclasificacionterceroGrupo.forEach( dato => {clasificaciontercerogrupo.agregarCampos(dato , 'A');
    });

    cargarModulos(config);
    sortable('.js-sortable', { items: 'li' ,forcePlaceholderSize: true, placeholderClass: 'mb1 border'});
});

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }


// ----------- FUNCION PARA AGREGAR MODULOS A MULTI----------------
function cargarModulos(configuracion){

  if(configuracion && configuracion.length){
    configuracion.forEach(dato =>{
            let id = dato.oidClasificacionTerceroConfig;
            let idModulo = dato.ModuloTercero_ClasificacionTerceroConfig;
            let nombreModulo = dato.txNombreModuloTercero;
            let chCorreo = dato.chCorreoClasificacionTerceroConfig;
            let idModuloCorreo = dato.oidModuloTerceroCorreo ? dato.oidModuloTerceroCorreo : 0;
            let txPara = dato.txParaModuloTerceroCorreo ? dato.txParaModuloTerceroCorreo : "";
            let txCopia = dato.txCopiaModuloTerceroCorreo ? dato.txCopiaModuloTerceroCorreo : "";
            let txAsunto = dato.txAsuntoModuloTerceroCorreo ? dato.txAsuntoModuloTerceroCorreo : "";
            let txMensaje = dato.txMensajeModuloTerceroCorreo ? escapeHtml(dato.txMensajeModuloTerceroCorreo) : "";

            let checked = chCorreo == "1" ? "checked" : "";
            $.get('/general/clasificaciontercerorol',{'id': id},function(data){
                let datosparse = JSON.parse(data);
                let registro = new Array();
                let roles = new Array();
                let concatenado = "";
    
                for(let j in datosparse){
                    for (var i in datosparse[j]){
                        registro.push(datosparse[j][i]);
                    }
                    roles.push(registro);
                    registro = [];
                }
    
                roles.forEach(dataUni => {
                    concatenado += dataUni[0] + "/" + dataUni[1] + "/" + dataUni[2] + "|";
                });

                if (!arrayModulos.includes(idModulo)){
                    let liModulo = '<li id="moduloTercero'+idModulo+'" class="d-flex flex-row mb-1 border rounded" draggable="true" role="option" aria-grabbed="false">'+
                        '<input id="oidModuloTerceroCorreo'+idModulo+'" name="oidModuloTerceroCorreo[]" type="hidden" value="'+idModuloCorreo+'">'+
                        '<input id="txParaModuloTerceroCorreo'+idModulo+'" name="txParaModuloTerceroCorreo[]" type="hidden" value="'+txPara+'">'+
                        '<input id="txCopiaModuloTerceroCorreo'+idModulo+'" name="txCopiaModuloTerceroCorreo[]" type="hidden" value="'+txCopia+'">'+
                        '<input id="txAsuntoModuloTerceroCorreo'+idModulo+'" name="txAsuntoModuloTerceroCorreo[]" type="hidden" value="'+txAsunto+'">'+
                        '<input id="txMensajeModuloTerceroCorreo'+idModulo+'" name="txMensajeModuloTerceroCorreo[]" type="hidden" value="'+txMensaje+'">'+
                        '<input id="clasificacionTerceroRol'+idModulo+'" name="clasificacionTerceroRol[]" type="hidden" value="'+concatenado+'">'+
                        '<input id="oidClasificacionTerceroConfig'+idModulo+'" name="oidClasificacionTerceroConfig[]" type="hidden" value="'+id+'">'+
                        '<input id="ModuloTercero" name="ModuloTercero_ClasificacionTerceroConfig[]" type="hidden" value="'+idModulo+'">'+
                        '<span class="p-1"><button type="button" class="btn btn-sm" onclick=""><i class="fas fa-sort-amount-up"></i></button></span>'+
                        '<label class="custom-checkbox-container" >'+
                        '<input id="chCorreoClasificacionTerceroConfig'+idModulo+'" name="chCorreoClasificacionTerceroConfig[]" type="hidden" value="'+chCorreo+'">'+
                        '<input id="checkCorreo'+idModulo+'" name="checkCorreo[]" type="checkbox" onclick="checkboxCorreo('+idModulo+');" '+checked+'>'+
                        '<span class="checkmark"></span>'+
                        '</label>'+
                        '<span class="p-1"><button type="button" class="btn btn-success btn-sm" onclick="modalCorreo('+idModulo+');"><i class="far fa-envelope"></i></button></span>'+
                        '<span class="p-1"><button type="button" class="btn btn-success btn-sm" onclick="modalRol('+idModulo+');"><i class="fas fa-lock"></i></button></span>'+
                        '<span class="p-1 flex-grow-1 align-middle m-auto font-weight-bold">'+nombreModulo+'</span>'+
                        '<span class="p-1"><button type="button" class="btn btn-danger btn-sm" onclick="eliminarModulo('+idModulo+');"><i class="fas fa-trash"></i></button></span>'+
                    '</li>';
    
                    $("#modulosTercero").append(liModulo);
                    arrayModulos.push(String(idModulo));
                }
            });
        });
    }
}


// ------------ FUNCION PARA MOSTRAR Y SELECCIONAR MODULOS-------------
function modalModulo(){
    modal.cargando();
    $.get('/general/listarmodulotercero',function( data ) {
        modal.mostrarModal('Lista de Modulos tercero',data,function(){
            let trs = document.querySelectorAll('.'+claseSeleccion);
            adicionarModulos(trs);
            sortable('.js-sortable', { items: 'li' ,forcePlaceholderSize: true, placeholderClass: 'mb1 border'});
            modal.cerrarModal();
        }).extraGrande();
        configurarGridSelect('modulos-table');
    }).fail(function(resp){
        modal.cerrarModal();
    });
}

// ----------- FUNCION PARA AGREGAR MODULOS A MULTI----------------
function adicionarModulos(trModulos){
    if(trModulos && trModulos.length){
        trModulos.forEach(tr => {
            let id = tr.dataset.idModulo;
            let nombre = tr.dataset.nombreModulo;

            if (!arrayModulos.includes(id)){
                let liModulo = '<li id="moduloTercero'+id+'" class="d-flex flex-row mb-1 border rounded">'+
                    '<input id="oidModuloTerceroCorreo'+id+'" name="oidModuloTerceroCorreo[]" type="hidden" value="0">'+
                    '<input id="txParaModuloTerceroCorreo'+id+'" name="txParaModuloTerceroCorreo[]" type="hidden" value="">'+
                    '<input id="txCopiaModuloTerceroCorreo'+id+'" name="txCopiaModuloTerceroCorreo[]" type="hidden" value="">'+
                    '<input id="txAsuntoModuloTerceroCorreo'+id+'" name="txAsuntoModuloTerceroCorreo[]" type="hidden" value="">'+
                    '<input id="txMensajeModuloTerceroCorreo'+id+'" name="txMensajeModuloTerceroCorreo[]" type="hidden" value="">'+
                    '<input id="clasificacionTerceroRol'+id+'" name="clasificacionTerceroRol[]" type="hidden" value="">'+
                    '<input id="oidClasificacionTerceroConfig'+id+'" name="oidClasificacionTerceroConfig[]" type="hidden" value="0">'+
                    '<input id="ModuloTercero" name="ModuloTercero_ClasificacionTerceroConfig[]" type="hidden" value="'+id+'">'+
                    '<span class="p-1"><button type="button" class="btn btn-sm" onclick=""><i class="fas fa-sort-amount-up"></i></button></span>'+
                    '<label class="custom-checkbox-container" >'+
                    '<input id="chCorreoClasificacionTerceroConfig'+id+'" name="chCorreoClasificacionTerceroConfig[]" type="hidden" value="0">'+
                    '<input id="checkCorreo'+id+'" name="checkCorreo[]" type="checkbox" onclick="checkboxCorreo('+id+');">'+
                    '<span class="checkmark"></span>'+
                    '</label>'+
                    '<span class="p-1"><button type="button" class="btn btn-success btn-sm" onclick="modalCorreo('+id+');"><i class="far fa-envelope"></i></button></span>'+
                    '<span class="p-1"><button type="button" class="btn btn-success btn-sm" onclick="modalRol('+id+');"><i class="fas fa-lock"></i></button></span>'+
                    '<span class="p-1 flex-grow-1 align-middle m-auto font-weight-bold">'+nombre+'</span>'+
                    '<span class="p-1"><button type="button" class="btn btn-danger btn-sm" onclick="eliminarModulo('+id+');"><i class="fas fa-trash"></i></button></span>'+
                '</li>';

                $("#modulosTercero").append(liModulo);
                arrayModulos.push(id);
            }

        });
    }
}

//Funcion para cambiar el valor de checkbox
function checkboxCorreo(id){
    if($("#checkCorreo"+id).prop('checked')){
        $("#chCorreoClasificacionTerceroConfig"+id).val("1");
    }else{
        $("#chCorreoClasificacionTerceroConfig"+id).val("0");
    }
}

//Funcion para abrir modal de rol
function modalRol(id){
    modal.cargando();
    $.get('/general/listarclasificacionrol',function( data ) {
        modal.mostrarModal('Lista de Rol',data,function(){
            if(validarRol() == true){
                obtenerRol(id);
                modal.cerrarModal();
            }
        }).extraGrande();

        // -------------GENERAMOS MULTI DE COMPOSICION-----------------
        rol = new GeneradorMultiRegistro('rol','tRol','rol');
        rol.campoid = 'oidClasificacionTerceroRol';
        rol.campoEliminacion = 'eliminarRol';
        rol.etiqueta = ['input','select','checkbox'];
        rol.tipo = ['hidden','',''];
        rol.campos = ['oidClasificacionTerceroRol','Rol_oidClasificacionTerceroRol','chModificarClasificacionTerceroRol']
        rol.opciones = ['',roles,''];
        rol.clase = ['','',''];
        rol.sololectura = [true,false,false];
        rol.funciones = ['',validarRoles,'']

        let valoresRol = $("#clasificacionTerceroRol"+id).val();
        if(valoresRol){
            let valorRol = valoresRol.slice(0,-1);
            valorRol = valorRol.split('|');
            valorRol.forEach(valor => {
                let dato = valor.split('/');
                rol.agregarCampos(dato,'A');
            });
        }

    }).fail(function(resp){
        modal.cerrarModal();
    });
}

//Funcion para abrir modal de correo
function modalCorreo(id){
    modal.cargando();
    $.get('/general/clasificaciontercerocorreo',function( data ) {
        modal.mostrarModal('Correo',data,function(){
            if(validarCorreo() == true){
                obtenerCorreo(id);
                modal.cerrarModal();
            }
        }).extraGrande();

        $("#oidModuloTerceroCorreo").val($("#oidModuloTerceroCorreo"+id).val());
        $("#txParaModuloTerceroCorreo").val($("#txParaModuloTerceroCorreo"+id).val());
        $("#txCopiaModuloTerceroCorreo").val($("#txCopiaModuloTerceroCorreo"+id).val());
        $("#txAsuntoModuloTerceroCorreo").val($("#txAsuntoModuloTerceroCorreo"+id).val());
        $("#txMensajeModuloTerceroCorreo").val($("#txMensajeModuloTerceroCorreo"+id).val());
        $("#txMensajeModuloTerceroCorreo").summernote({ height: 150 });

    }).fail(function(resp){
        modal.cerrarModal();
    });

}

function obtenerCorreo(id){
    $("#oidModuloTerceroCorreo"+id).val($("#oidModuloTerceroCorreo").val());
    $("#txParaModuloTerceroCorreo"+id).val($("#txParaModuloTerceroCorreo").val());
    $("#txCopiaModuloTerceroCorreo"+id).val($("#txCopiaModuloTerceroCorreo").val());
    $("#txAsuntoModuloTerceroCorreo"+id).val($("#txAsuntoModuloTerceroCorreo").val());
    $("#txMensajeModuloTerceroCorreo"+id).val($("#txMensajeModuloTerceroCorreo").val());
}

//Funcion para validar correo
function validarCorreo(){
    $("#alertaCampos").css('display','none');
    let valor = true;
    let para = $("#txParaModuloTerceroCorreo").val();
    let asunto = $("#txAsuntoModuloTerceroCorreo").val();

    if(!para){
        $("#txParaModuloTerceroCorreo").addClass(errorClass);
        valor = false;
        $("#alertaCampos").css('display','block');
    }
    if(!asunto){
        $("#txAsuntoModuloTerceroCorreo").addClass(errorClass);
        valor = false;
        $("#alertaCampos").css('display','block');
    }

    return valor;
}

//Funcion para validar rol
function validarRol(){
    $("#alertaCampos").css('display','none');
    $("#alertaRepetido").css('display','none');

    let valor = true;
    let arrayRol = [];
    let roles = $('select[name="Rol_oidClasificacionTerceroRol[]"]');

    $.each(roles,function(i,rol){
        let codigo = $("#"+rol.id+" option:selected").text();
        if(codigo == "Seleccione..."){
            $("#"+rol.id).addClass(errorClass);
            $("#alertaCampos").css('display','block');
            valor = false;
            return false;
        }

        if(arrayRol.indexOf(codigo) > -1){
            $("#"+rol.id).addClass(errorClass);
            $("#alertaRepetido").css('display','block');
            valor = false;
            return false;
        }else{
            arrayRol.push(codigo);
        }
    });

    return valor;

}

//Funcion para obtener roles
function obtenerRol(idModulo){

    let idClasificacionRoles = $('input[name="oidClasificacionTerceroRol[]"]');
    let selectRol = $('select[name="Rol_oidClasificacionTerceroRol[]"]');
    let chModificarClasificacionTerceroRol = $('input[name="chModificarClasificacionTerceroRol[]"]');
    let rol = "";

    $.each(idClasificacionRoles,function(i,idClasificacionRol){
        let idRol = $("#"+selectRol[i].id+" option:selected").val()
        rol += idClasificacionRol.value + "/" + idRol + "/" + chModificarClasificacionTerceroRol[i].value + "|";
    });

    $("#clasificacionTerceroRol"+idModulo).val(rol);

}

//Funcion para eliminar elemento de html y id de array
function eliminarModulo(id){
    $("#moduloTercero"+id).remove();
    arrayModulos = [];
    let modulos = document.querySelectorAll('input[name="ModuloTercero_ClasificacionTerceroConfig[]"]');
    if(modulos){
        modulos.forEach(modulo => {
            if (modulo && modulo.value){
                arrayModulos.push(String(modulo.value));
            }
        })
    }
}

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-clasificaciontercero";
        let route = $(formularioId).attr('action');
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/clasificaciontercero";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            let keyNombre = 'txNombreClasificacionTerceroGrupo';
            let nombres = $('input[name="'+keyNombre+'[]"]')

            $.each(resp.responseJSON.errors, function(index, value) {
                index = index.replace(".","");
                mensajes.push(value);

                if( index.startsWith(keyNombre)){
                    setErroresMultiregistro(index.substr(keyNombre.length , index.length) , nombres);
                }else{
                    $('#'+index).addClass(errorClass);
                }
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoClasificacionTercero_Input = $("#txCodigoClasificacionTercero");
        let txCodigoClasificacionTercero_AE = txCodigoClasificacionTercero_Input.val();
        txCodigoClasificacionTercero_Input.removeClass(errorClass);
        if(!txCodigoClasificacionTercero_AE){
            txCodigoClasificacionTercero_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreClasificacionTercero_Input = $("#txNombreClasificacionTercero");
        let txNombreClasificacionTercero_AE = txNombreClasificacionTercero_Input.val();
        txNombreClasificacionTercero_Input.removeClass(errorClass);
        if(!txNombreClasificacionTercero_AE){
            txNombreClasificacionTercero_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }

        let tipoClasificacionTercero = $('#lmTipoClasificacionTercero').serialize();
            if(!tipoClasificacionTercero){
                $('#lmTipoClasificacionTercero_chosen').addClass(errorClass)
                mensajes.push( "Debes seleccionar al menos un tipo de tercero");
            }


        let nombreGrupo = document.querySelectorAll('input[name="txNombreClasificacionTerceroGrupo[]"]');
    
        let validar = [nombreGrupo];
        let mensajesArray = ["el nombre del grupo "]
    
        validar.forEach((valida,j) => {
    
            if(valida){
                valida.forEach((valor , i) => {
                    if (valor && !valor.value){
                        valor.classList.add( errorClass );
                        mensajes.push(mensajesArray[j]+' en la linea '+(i + 1)+' es requerida');
                    }
                })
            }
        });
    

		return mensajes;
    }
}


function iniciarChosenSelect(){
            
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
}

function llenarSelectClasificacion(tipoClasificacionTercero){
    $("#lmTipoClasificacionTercero").val(tipoClasificacionTercero.split(',')).trigger("chosen:updated");
}