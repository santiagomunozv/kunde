"use strict";
let errorClass = "is-invalid";




function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-dependencia";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/general/dependencia";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txCodigoDependencia_Input = $("#txCodigoDependencia");
        let txCodigoDependencia_AE = txCodigoDependencia_Input.val();
        txCodigoDependencia_Input.removeClass(errorClass);
        if(!txCodigoDependencia_AE){
            txCodigoDependencia_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
		let txNombreDependencia_Input = $("#txNombreDependencia");
        let txNombreDependencia_AE = txNombreDependencia_Input.val();
        txNombreDependencia_Input.removeClass(errorClass);
        if(!txNombreDependencia_AE){
            txNombreDependencia_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		let txAbreviaturaDependencia_Input = $("#txAbreviaturaDependencia");
        let txAbreviaturaDependencia_AE = txAbreviaturaDependencia_Input.val();
        txAbreviaturaDependencia_Input.removeClass(errorClass);
        if(!txAbreviaturaDependencia_AE){
            txAbreviaturaDependencia_Input.addClass(errorClass)
            mensajes.push("El campo Abreviatura es obligatorio");
        }
		let txDirectorioDependencia_Input = $("#txDirectorioDependencia");
        let txDirectorioDependencia_AE = txDirectorioDependencia_Input.val();
        txDirectorioDependencia_Input.removeClass(errorClass);
        if(!txDirectorioDependencia_AE){
            txDirectorioDependencia_Input.addClass(errorClass)
            mensajes.push("El campo Directorio es obligatorio");
        }
		let txEstadoDependencia_Input = $("#txEstadoDependencia");
        let txEstadoDependencia_AE = txEstadoDependencia_Input.val();
        txEstadoDependencia_Input.removeClass(errorClass);
        if(!txEstadoDependencia_AE){
            txEstadoDependencia_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
		return mensajes;
    }
}