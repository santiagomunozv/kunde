<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeColumnsReunionAndProspecto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->string('txNombreArlProspecto')->nullable()->comment('Arl del prospecto');
        });

        DB::update('UPDATE 
                rgs_prospecto rp
            JOIN asn_tercero at2 ON
                rp.Tercero_oidArl = at2.oidTercero
            SET txNombreArlProspecto = txNombreTercero');

        Schema::table('rgs_prospecto', function (Blueprint $table) {
            $table->dropColumn('Tercero_oidArl');
        });

        DB::select("ALTER TABLE reu_reunion MODIFY txDescripcionReunion text null;");

        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->string('intDuracionReunion')->nullable()->comment('Duración de la reunión en minutos');
            $table->string('txFechaFinalJulianoReunion')->nullable()->comment('Fecha en juliano de la reunión');
        });

        DB::update("UPDATE reu_reunion SET txFechaFinalJulianoReunion = txFechaJulianoReunion");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
