<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuReunionbitacoraasistenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reunionbitacoraasistente', function (Blueprint $table) {
            $table->increments('oidBitacorAsistente');
            $table->string('txNombreBitacoraAsistente')->nullable()->comment('nombre del Asistente');
            $table->string('txCorreoBitacoraAsistente')->nullable()->comment('correo del Asistente');
            $table->boolean('shTalleristaAsistente')->default(0)->comment('tallerista');
            $table->unsignedInteger('ReunionBitacora_oidBitacora')->nullable()->comment('bitacora');

            $table->foreign('ReunionBitacora_oidBitacora')->references('oidReunionBitacora')->on('reu_reunionbitacora')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reunionbitacoraasistente');
    }
}
