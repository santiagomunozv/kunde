<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHrHoraRinalReunionToReuReunionTable extends Migration
{
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->time('hrHoraFinalReunion');
        });
    }

    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->time('hrHoraFinalReunion');
        });
    }
}
