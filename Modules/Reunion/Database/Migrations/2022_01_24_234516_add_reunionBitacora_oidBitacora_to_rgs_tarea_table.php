<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReunionBitacoraOidBitacoraToRgsTareaTable extends Migration
{
    public function up()
    {
        Schema::table('rgs_tarea', function (Blueprint $table) {
            $table->unsignedInteger('ReunionBitacora_oidBitacora')->nullable()->comment('Id Bitacora');

            $table->foreign('ReunionBitacora_oidBitacora')->references('oidReunionBitacora')->on('reu_reunionbitacora')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('rgs_tarea', function (Blueprint $table) {
            $table->dropForeign(['ReunionBitacora_oidBitacora']);
        });
    }
}
