<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuAdjuntosTable extends Migration
{
    public function up()
    {
        Schema::create('reu_adjuntos', function (Blueprint $table) {
            $table->increments('oidAdjunto');
            $table->string('txNombreArvchivo')->nullable()->comment('nombre del archivo');
            $table->string('txRutaArchivo')->nullable()->comment('ruta dle archivo');
            $table->string('txTipoArchivo')->nullable()->comment('tipo de archivo');
            $table->string('txDescripcionArchivo')->nullable()->comment('descripcion del archivo');
            $table->date('dtFechaSubidaArchivo')->nullable()->comment('fecha de subida del archivo');
            $table->unsignedInteger('Reunion_idReunion')->comment('id reunion')->nullable();

            $table->foreign('Reunion_idReunion')->references('oidReunion')->on('reu_reunion')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('reu_adjuntos');
    }
}
