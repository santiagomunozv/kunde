<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateJulianoReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->string('txFechaJulianoReunion')->nullable()->comment('Fecha en formato juliano');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->dropColumn('txFechaJulianoReunion');
        });
    }
}
