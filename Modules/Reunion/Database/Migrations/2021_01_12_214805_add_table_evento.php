<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddTableEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_evento', function (Blueprint $table) {
            $table->increments('oidEvento');
            $table->string('txCodigoEvento')->comment('Código de Evento');
            $table->string('txNombreEvento')->comment('Nombre de Evento');
            $table->string('txEstadoEvento')->comment('Estado de Evento');
            $table->timestamps();
        });

        DB::table('seg_modulo')->insert([
            "oidModulo" => 21,
            "Paquete_oidPaquete_1aM" => 5,
            "txNombreModulo" => "Reuniones",
            "txIconoModulo" => "fas fa-user-clock"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>21,
            "txNombreOpcion" =>"Evento",
            "txRutaOpcion" => "/reuniones/evento"
        ]);

        DB::table("seg_opcion")->insert([
            "Modulo_oidModulo_1aM" =>21,
            "txNombreOpcion" =>"Reunión",
            "txRutaOpcion" => "/reuniones/reunion"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_evento');
    }
}
