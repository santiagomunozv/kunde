<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReunioneventoactividad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reunioneventoactividad', function (Blueprint $table) {
            $table->increments('oidReunionEventoActividad');
            $table->boolean('chCumplidaEventoActividad')->comment('Cumplida');
            $table->integer('EventoActividad_oidEventoActividad')->comment('Id Evento actividad');
            $table->integer('Reunion_oidReunion')->comment('Id Reunion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reunioneventoactividad');
    }
}
