<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTxEstadoReunionToReuReunionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('Tercero_idCliente')->comment('id cliente')->nullable();
            $table->string('txEstadoReunion')->nullable()->comment('etapas de la reunion');

            $table->foreign('Tercero_idCliente')->references('oidTercero')->on('asn_tercero');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->dropForeign(['Tercero_idCliente']);
            $table->dropForeign(['Reunion_idReunion']);
            $table->dropColumn('txEstadoReunion');
        });
    }
}
