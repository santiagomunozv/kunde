<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTerceroReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('Tercero_oidResponsable')->nullable()->comment('Responsable de la reunión');
            $table->dropColumn('Tercero_oidOrganizador');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('Tercero_oidOrganizador')->nullable()->comment('Organizador de la reunión');
            $table->dropColumn('Tercero_oidResponsable');
        });
    }
}
