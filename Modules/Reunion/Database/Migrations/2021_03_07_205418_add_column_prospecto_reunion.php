<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnProspectoReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('Prospecto_oidProspecto')->nullable()->comment('Prospecto asociado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->dropColumn('Prospecto_oidProspecto');
        });
    }
}
