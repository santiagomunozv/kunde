<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuReunionbitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reunionbitacora', function (Blueprint $table) {
            $table->increments('oidReunionBitacora');
            $table->string('txActividadReunionBitacora')->comment('Nombre de la actividad');
            $table->date('dtFechaReunionBitacora')->comment('Fecha de reunión');
            $table->time('hrHoraInicioReunionBitacora')->comment('Hora inicio de reunión');
            $table->time('hrHoraFinalReunionBitacora')->comment('Hora  final de reunión');
            $table->string('txObservacionReunionBitacora')->comment('Obsercaciones');
            $table->unsignedInteger('Reunion_idReunion')->comment('id reunion')->nullable();

            $table->foreign('Reunion_idReunion')->references('oidReunion')->on('reu_reunion')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reunionbitacora');
    }
}
