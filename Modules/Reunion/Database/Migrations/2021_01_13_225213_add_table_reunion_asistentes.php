<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableReunionAsistentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reunionasistentes', function (Blueprint $table) {
            $table->increments('oidReunionAsistente');
            $table->string('txNombreReunionAsistente')->comment('Nombre asistente');
            $table->string('txCorreoAsistente')->comment('Correo asistente');
            $table->integer('Reunion_oidReunion')->comment('Id Reunión');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reunionasistentes');
    }
}
