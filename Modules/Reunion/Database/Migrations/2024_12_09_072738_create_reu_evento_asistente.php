<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuEventoAsistente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_evento_asistente', function (Blueprint $table) {
            $table->increments('oidEventoAsistente');
            $table->string('txAsistenteEventoAsistente');
            $table->unsignedInteger('Evento_oidEvento');
            $table->foreign('Evento_oidEvento')->references('oidEvento')->on('reu_evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_evento_asistente');
    }
}
