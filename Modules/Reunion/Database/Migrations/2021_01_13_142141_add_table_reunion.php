<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reunion', function (Blueprint $table) {
            $table->increments('oidReunion');
            $table->string('txAsuntoReunion')->comment('Asunto de reunión');
            $table->string('txTipoReunion')->comment('Tipo de reunión');
            $table->date('dtFechaReunion')->comment('Fecha de reunión');
            $table->time('hrHoraReunion')->comment('Hora de reunión');
            $table->string('txUbicacionReunion')->comment('Ubicación de reunión');
            $table->integer('Tercero_oidOrganizador')->comment('Id jornada empleado');
            $table->text('txDescripcionReunion')->comment('Descripción de reunión');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reunion');
    }
}
