<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEstadoReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('chEstadoReunion')->nullable()->comment('Estado de la reunión');
            $table->string('txDescripcionEstadoReunion')->nullable()->comment('Descripción estado reunion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->dropColumn('chEstadoReunion');
            $table->dropColumn('txDescripcionEstadoReunion');
        });
    }
}
