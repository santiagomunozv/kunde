<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowCronogramacumplido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_cronogramaespacios', function (Blueprint $table) {
            $table->integer('chProgramadoCronogramaEspacios')->nullable()->comment('Validador si el cronograma se programó')->default(0);
        });

        Schema::table('asn_terceroperiodicidad', function (Blueprint $table) {
            $table->decimal('inHorasConsumidasTerceroPeriodicidad', 15, 2)->nullable()->comment('Horas consumidas')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_cronogramaespacios', function (Blueprint $table) {
            $table->dropColumn('chProgramadoCronogramaEspacios');
        });

        Schema::table('asn_terceroperiodicidad', function (Blueprint $table) {
            $table->dropColumn('inHorasConsumidasTerceroPeriodicidad');
        });
    }
}
