<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLsModalidadTerceroPeriodicidadToReuCronogramaespaciosTable extends Migration
{
    public function up()
    {
        Schema::table('reu_cronogramaespacios', function (Blueprint $table) {
            $table->time('dtHoraEstimadaTerceroEventoPeriodicidad')->nullable();
            $table->text('lsModalidadTerceroPeriodicidad')->nullable();
        });
    }

    public function down()
    {
        Schema::table('reu_cronogramaespacios', function (Blueprint $table) {
            $table->dropColumn('dtHoraEstimadaTerceroEventoPeriodicidad');
            $table->dropColumn('lsModalidadTerceroPeriodicidad');
        });
    }
}
