<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnColorEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_evento', function (Blueprint $table) {
            $table->string('txColorEvento')->nullable()->comment('Color evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_evento', function (Blueprint $table) {
            $table->dropColumn('txColorEvento');
        });
    }
}
