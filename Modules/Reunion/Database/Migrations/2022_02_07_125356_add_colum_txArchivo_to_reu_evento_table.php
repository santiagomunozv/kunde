<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumTxArchivoToReuEventoTable extends Migration
{
    public function up()
    {
        Schema::table('reu_evento', function (Blueprint $table) {
            $table->string('txArchivoEvento')->nullable()->comment('archivo de la reunion');
            $table->string('txMensajeEvento')->nullable()->comment('archivo de la reunion');
        });
    }

    public function down()
    {
        Schema::table('reu_evento', function (Blueprint $table) {
            $table->dropColumn('txArchivoEvento');
            $table->dropColumn('txMensajeEvento');
        });
    }
}
