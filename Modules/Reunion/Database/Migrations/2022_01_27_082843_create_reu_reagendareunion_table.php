<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuReagendareunionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_reagendareunion', function (Blueprint $table) {
            $table->increments('oidReagendaReunion');
            $table->string('txMotivoReagenda')->comment('descripción del motivo de re agenda de la reunion');
            $table->date('dtFechaAnteriorReagenda')->comment('fecha anterior de reunion');
            $table->time('hrHoraAnteriorReagenda')->comment('hora anterior de reunion');
            $table->string('txRespAnteriorReagenda')->comment('responsable anterior de la reunion');
            $table->string('txObservacionReagenda')->comment('observacion de la reagenda');
            $table->unsignedInteger('Reunion_idReunion')->comment('id reunion');

            $table->foreign('Reunion_idReunion')->references('oidReunion')->on('reu_reunion')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_reagendareunion');
    }
}
