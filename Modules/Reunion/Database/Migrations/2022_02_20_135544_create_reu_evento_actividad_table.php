<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReuEventoActividadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_evento_actividad', function (Blueprint $table) {
            $table->increments('oidEventoActividad');
            $table->string('txDescripcionEventoActividad');
            $table->unsignedInteger('evento_oidEvento');

            $table->foreign('evento_oidEvento')->references('oidEvento')->on('reu_evento')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_evento_actividad');
    }
}
