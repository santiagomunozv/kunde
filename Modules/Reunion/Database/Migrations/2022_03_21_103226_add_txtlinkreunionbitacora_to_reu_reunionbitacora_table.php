<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTxtlinkreunionbitacoraToReuReunionbitacoraTable extends Migration
{
    public function up()
    {
        Schema::table('reu_reunionbitacora', function (Blueprint $table) {
            $table->string('txLinkReunionBitacora')->nullable();
        });
    }

    public function down()
    {
        Schema::table('reu_reunionbitacora', function (Blueprint $table) {
            $table->dropColumn('txLinkReunionBitacora');
        });
    }
}
