<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEventoReunion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->integer('Evento_oidEvento')->comment('Id Evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reu_reunion', function (Blueprint $table) {
            $table->dropColumn('Evento_oidEvento');
        });
    }
}
