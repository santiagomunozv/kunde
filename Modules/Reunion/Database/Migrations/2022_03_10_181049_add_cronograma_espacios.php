<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCronogramaEspacios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reu_cronogramaespacios', function (Blueprint $table) {
            $table->increments('oidCronogramaEspacios');
            $table->string('txAsuntoCronogramaEspacios')->comment('Asunto de reunión');
            $table->date('dtFechaCronogramaEspacios')->comment('Fecha de reunión');
            $table->time('hrHoraCronogramaEspacios')->comment('Hora de reunión');
            $table->string('txFechaJulianoCronogramaEspacios')->comment('Fecha en formato juliano');
            $table->string('txFechaFinalJulianoCronogramaEspacios')->comment('Fecha final en formato juliano');
            $table->integer('Evento_oidEvento')->comment('Id Evento');
            $table->integer('Tercero_idCliente')->comment('id cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reu_cronogramaespacios');
    }
}
