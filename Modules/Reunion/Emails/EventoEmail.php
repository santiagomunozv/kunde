<?php

namespace Modules\Reunion\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventoEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $evento;

    public function __construct($evento)
    {
        $this->evento = $evento;
    }

    public function build()
    {
        $address = 'Kunde@example.com';
        $subject = 'Confirmación de reunión Kunde';
        $name = 'Consultora Kunde';

        return $this->view('reunion::emails.recordatorioEvento')
            ->from($address, $name)
            ->subject($subject);
    }
}
