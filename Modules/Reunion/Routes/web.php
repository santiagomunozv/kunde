<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {

    Route::prefix('reuniones')->group(function () {
        // Route::get('/calendario', 'ReunionController@showCalendar');
        Route::resource('calendario', 'CalendarController');
        Route::resource('/evento', 'EventosController');
        Route::resource('/reunion', 'ReunionController');
        Route::resource('/tareasporestado', 'TareasEstadoController');
        Route::resource('informe', 'InformeController')->except(['edit', 'update', 'destroy']);
        Route::post('/subirevento', 'EventosController@subirEvento');
        Route::post('/tareasporestado/adjuntos', 'TareasEstadoController@adjuntos');
        Route::get('obtenerreuniones', 'ReunionController@obtenerReuniones');
        Route::get('/tareasporestado/modal/{id}', 'TareasEstadoController@edit');
        Route::get('/tareasporestado/filtrostareas', 'TareasEstadoController@show');
        Route::get('/tareasporestado/modal/obtenerjson/{id}', 'TareasEstadoController@cargarJson');
        Route::get('/tareasporestado/modalreagendar/{id}', 'TareasEstadoController@reagendar');
        Route::post('/tareasporestado/reagendartarea/{id}', 'TareasEstadoController@reagendarReunion');
        Route::get('/tareasporestado/modalreagendar/{id}', 'TareasEstadoController@reagendar');
        Route::get('/tareasporestado/email/{id}', 'EmailEventoController@modalEmail');
        Route::post('/tareasporestado/email/enviar/{id}', 'EmailEventoController@enviarEmail');
        Route::get('/tareasporestado/consultaradjuntos/{id}', 'TareasEstadoController@consultarAdjuntos')->name('bitacoraImg');
        Route::post('/tareasporestado/eliminartarea/{id}', 'TareasEstadoController@destroy');
        Route::get('/tareasporestado/getevent/{id}', 'TareasEstadoController@getEvent');
        Route::get('/tareasporestado/getclient/{id}', 'TareasEstadoController@getClient');
        Route::post('/tareasporestado/descargar', 'TareasEstadoController@descargar');
        Route::resource('cronogramaespacios', 'CronogramaEspacioController');
        Route::get('obtenercronogramaespacios', 'CronogramaEspacioController@obtenerCronogramaEspacios');
    });
});
