<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReagendaReunionModel extends Model
{
    protected $table = 'reu_reagendareunion';
    protected $primaryKey = 'oidReagendaReunion';
    protected $fillable = ['txMotivoReagenda', 'dtFechaAnteriorReagenda', 'hrHoraAnteriorReagenda', 'txRespAnteriorReagenda', 'txObservacionReagenda', 'Reunion_idReunion'];
    public $timestamps = false;

    public function getReagendaById($id)
    {
        $reagendas = DB::table('reu_reagendareunion as re')
            ->join('asn_tercero as te', 're.txRespAnteriorReagenda', 'te.oidTercero')
            ->where('Reunion_idReunion', $id)
            ->select('oidReagendaReunion', 'txMotivoReagenda', 'dtFechaAnteriorReagenda', 'hrHoraAnteriorReagenda', 'txRespAnteriorReagenda', 'txObservacionReagenda', 'te.txNombreTercero')
            ->get();

        return $reagendas;
    }

    public function getReagendaByTerceroId($terceroId)
    {
        $reagendas = DB::table('reu_reagendareunion as re')
            ->join('reu_reunion as reu', 're.Reunion_idReunion', 'reu.oidReunion')
            ->join('asn_tercero as te', 're.txRespAnteriorReagenda', 'te.oidTercero')
            ->where('reu.Tercero_idCliente', $terceroId)
            ->select('oidReagendaReunion', 'txMotivoReagenda', 'dtFechaAnteriorReagenda', 'hrHoraAnteriorReagenda', 'txRespAnteriorReagenda', 'txObservacionReagenda', 'te.txNombreTercero', 'txAsuntoReunion')
            ->get();

        return $reagendas;
    }
}
