<?php
namespace Modules\Reunion\Entities;
use Illuminate\Database\Eloquent\Model;

class ReunionAsistenteModel extends Model
{
    protected $table = 'reu_reunionasistentes';
    protected $primaryKey = 'oidReunionAsistente';
    protected $fillable = ['txNombreReunionAsistente','txCorreoAsistente', 'Reunion_oidReunion'];
    public $timestamps = false;

}