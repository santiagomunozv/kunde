<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;

class AdjuntosModel extends Model
{
    protected $table = 'reu_adjuntos';
    protected $primaryKey = 'oidAdjunto';
    protected $fillable = ['txNombreArvchivo', 'txRutaArchivo', 'txTipoArchivo', 'txDescripcionArchivo', 'dtFechaSubidaArchivo', 'Reunion_idReunion'];
    public $timestamps = false;
}
