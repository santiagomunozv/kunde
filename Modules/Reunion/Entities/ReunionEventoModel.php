<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;

class ReunionEventoModel extends Model
{
    protected $table = 'reu_evento_actividad';
    protected $primaryKey = 'oidEventoActividad';
    protected $fillable = ['txDescripcionEventoActividad', 'evento_oidEvento'];
    public $timestamps = false;
}
