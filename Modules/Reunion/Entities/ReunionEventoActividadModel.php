<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReunionEventoActividadModel extends Model
{
  protected $table = 'reu_reunioneventoactividad';
  protected $primaryKey = 'oidReunionEventoActividad';
  protected $fillable = ['chCumplidaEventoActividad', 'EventoActividad_oidEventoActividad', 'Reunion_oidReunion'];
  public $timestamps = false;

  public static function getEventoActividadByReunion($idReunion)
  {
    $eventoActividad = DB::table('reu_reunion as reu')
      ->join('reu_evento as eve', 'reu.Evento_oidEvento', 'eve.oidEvento')
      ->join('reu_evento_actividad as rea', 'eve.oidEvento', 'rea.evento_oidEvento')
      ->leftJoin('reu_reunioneventoactividad as reuea', 'reuea.EventoActividad_oidEventoActividad', 'rea.oidEventoActividad')
      ->select(
        'oidReunionEventoActividad',
        'chCumplidaEventoActividad',
        'oidEventoActividad as EventoActividad_oidEventoActividad',
        'txDescripcionEventoActividad',
      )
      ->where('reu.oidReunion', '=', $idReunion)
      ->get();

    return $eventoActividad;
  }
}
