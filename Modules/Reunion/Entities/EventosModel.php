<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;

class EventosModel extends Model
{
    protected $table = 'reu_evento';
    protected $primaryKey = 'oidEvento';
    protected $fillable = ['txAnioEvento', 'txCodigoEvento', 'txNombreEvento', 'txEstadoEvento', 'txColorEvento', 'txImagenEvento', 'txArchivoEvento', 'txMensajeEvento'];
    public $timestamps = false;

    public static function getById($id)
    {
        return self::where('oidEvento', $id)->firstOrFail();
    }
}
