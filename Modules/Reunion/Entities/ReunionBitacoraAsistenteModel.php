<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;

class ReunionBitacoraAsistenteModel extends Model
{
    protected $table = 'reu_reunionbitacoraasistente';
    protected $primaryKey = 'oidBitacorAsistente';
    protected $fillable = ['txNombreBitacoraAsistente', 'txCorreoBitacoraAsistente', 'ReunionBitacora_oidBitacora', 'shTalleristaAsistente'];
    public $timestamps = false;

    
    public function bitacora()
    {
        return $this->belongsTo('Modules\Reunion\Entities\ReunionBitacoraModel', 'oidReunionBitacora', 'ReunionBitacora_oidBitacora');
    }
}
