<?php

namespace Modules\Reunion\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;

class ReunionModel extends Model
{
    protected $table = 'reu_reunion';
    protected $primaryKey = 'oidReunion';
    protected $fillable = ['txAsuntoReunion', 'txTipoReunion', 'dtFechaReunion', 'hrHoraReunion', 'txFechaJulianoReunion', 'txUbicacionReunion', 'Tercero_oidResponsable', 'txDescripcionReunion', 'Evento_oidEvento', 'chEstadoReunion', 'txDescripcionEstadoReunion', 'Prospecto_oidProspecto', 'intDuracionReunion', 'txFechaFinalJulianoReunion', 'Tercero_idCliente', 'txEstadoReunion'];
    public $timestamps = false;

    public static function getById($id)
    {
        return self::where('oidReunion', $id)->first();
    }

    public function bitacora()
    {
        return $this->hasOne(ReunionBitacoraModel::class, 'Reunion_idReunion', 'oidReunion');
    }

    public function getClienteById($id)
    {
        $cliente = DB::table('reu_reunion as re')
            ->leftJoin('asn_tercero as tc', 're.Tercero_idCliente', 'tc.oidTercero')
            ->select('oidReunion', 're.Tercero_idCliente as idCliente', 'tc.txNombreTercero as nombreCliente', 'tc.txCorreoElectronicoTercero as emailTercero')
            ->where('re.oidReunion', '=', $id)
            ->first();

        return $cliente;
    }

    public static function valitdateTimeTask($idEmpleado, $horainicio, $horaFinal, $fecha)
    {
        $already_exist = DB::table('reu_reunion as re')
            ->join('asn_tercero as tc', 're.Tercero_oidResponsable', 'tc.oidTercero')
            ->where('tc.oidTercero', $idEmpleado)
            ->where('re.txEstadoReunion', '!=', 'Terminado')
            ->whereDate('dtFechaReunion', $fecha)
            ->where(function ($query) use ($horainicio, $horaFinal) {
                $query->whereBetween('re.hrHoraReunion', [$horainicio, $horaFinal]);
                $query->orwhereBetween('re.hrHoraFinalReunion', [$horainicio, $horaFinal]);
            })
            ->get();

        return $already_exist;
    }

    public function scopeStatus($query, $status = 'Pendiente')
    {
        return $query->where('txEstadoReunion', $status);
    }

    public function scopeClient($query, $id  = null)
    {
        return $query->when($id, function ($query) use ($id) {
            $query->where("Tercero_idCliente", $id);
        });
    }

    public function scopeEmployee($query, $id = null)
    {
        return $query->join('asn_tercero as te', 'Tercero_oidResponsable', 'te.oidTercero')
            ->when($id, function ($query) use ($id) {
                $query->where("Tercero_oidResponsable", $id);
            });
    }

    public function scopeDateRange($query, $start = null, $end = null)
    {
        $end ? $end : $end = Carbon::now();
        if ($start) {
            return $query->whereBetween("dtFechaReunion", [$start, $end])->orderByDesc('dtFechaReunion');
        } else {
            return $query->whereRaw("dtFechaReunion >= CURDATE()")->orderByDesc('dtFechaReunion');
        }
    }
}
