<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReunionEventoAsistenteModel extends Model
{
  protected $table = 'reu_evento_asistente';
  protected $primaryKey = 'oidEventoAsistente';
  protected $fillable = ['txAsistenteEventoAsistente', 'Evento_oidEvento'];
  public $timestamps = false;
}
