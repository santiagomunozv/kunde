<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;

class ReunionBitacoraPendienteModel extends Model
{
    protected $table = 'reu_reunionbitacorapendiente';
    protected $primaryKey = 'oidReunionBitacoraPendiente';
    protected $fillable = ['Responsable_oidResponsable', 'ReunionBitacora_oidBitacora', 'txNombreTareaBitacoraPendiente', 'dtFechaVenBitacoraTareaPendiente'];
    public $timestamps = false;


}
