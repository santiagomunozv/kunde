<?php

namespace Modules\Reunion\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\AsociadoNegocio\Entities\TerceroPeriodicidadModel;

class CronogramaEspacioModel extends Model
{
	protected $table = 'reu_cronogramaespacios';
	protected $primaryKey = 'oidCronogramaEspacios';
	protected $fillable = ['txAsuntoCronogramaEspacios', 'dtFechaCronogramaEspacios', 'hrHoraCronogramaEspacios', 'txFechaJulianoCronogramaEspacios', 'txFechaFinalJulianoCronogramaEspacios', 'Evento_oidEvento', 'Tercero_idCliente', 'chProgramadoCronogramaEspacios', 'dtHoraEstimadaTerceroEventoPeriodicidad', 'lsModalidadTerceroPeriodicidad'];

	public $timestamps = false;

	public static function validateAvailabilityCronograma($cronogramaEspacios, $duracion)
	{
		$terceroPeriodicidad = TerceroPeriodicidadModel::where('Tercero_oidTercero', $cronogramaEspacios->Tercero_idCliente)->first();
		$duracionHora = self::defineDurationHour($duracion);

		if (($duracionHora + $terceroPeriodicidad->inHorasConsumidasTerceroPeriodicidad) > $terceroPeriodicidad->inHorasTerceroPeriodicidad) {
			return false;
		}
		return true;
	}

	public function cliente()
	{
		return $this->hasOne('\Modules\AsociadoNegocio\Entities\TerceroModel', 'oidTercero', 'Tercero_idCliente');
	}

	public static function defineDurationHour($duracion)
	{
		return $duracion / 60;
	}

	public function scopeClient($query, $client)
	{
		return $query->where('Tercero_idCliente', $client);
	}

	public function scopeStartDate($query, $date)
	{
		$date ? $date : $date = Carbon::now();
		return $query->whereDate('dtFechaCronogramaEspacios', '>=', $date);
	}

	public function scopeTime($query, $time)
	{
		$time ? $time : $time = Carbon::now()->format('H:i');
		return $query->whereTime('hrHoraCronogramaEspacios', '>=', $time);
	}
}
