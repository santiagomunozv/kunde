<?php

namespace Modules\Reunion\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ReunionBitacoraModel extends Model
{
    protected $table = 'reu_reunionbitacora';
    protected $primaryKey = 'oidReunionBitacora';
    protected $fillable = ['txActividadReunionBitacora', 'dtFechaReunionBitacora', 'hrHoraInicioReunionBitacora', 'hrHoraFinalReunionBitacora', 'txObservacionReunionBitacora', 'txLinkReunionBitacora', 'Reunion_idReunion'];
    public $timestamps = false;

    public function asistentes()
    {
        return $this->hasMany('Modules\Reunion\Entities\ReunionBitacoraAsistenteModel', 'ReunionBitacora_oidBitacora', 'oidReunionBitacora');
    }

    public static function getBitacoraByIdReunion($idReunion)
    {
        $bitacora = DB::table('reu_reunionbitacora as bi')
            ->leftJoin('reu_reunion as re', 'bi.Reunion_idReunion', 're.oidReunion')
            ->select(
                'oidReunionBitacora',
                'dtFechaReunionBitacora',
                'hrHoraInicioReunionBitacora',
                'hrHoraFinalReunionBitacora',
                'txObservacionReunionBitacora',
                'Reunion_idReunion',
                'txActividadReunionBitacora',
                'txLinkReunionBitacora'
            )
            ->where('bi.Reunion_idReunion', '=', $idReunion)
            ->first();

        return $bitacora;
    }

    // public function getIdReunionBitacoraByIdAsistente($oidAsistente)
    // {
    //     $idBitacora = DB::table('reu_reunionbitacora as rb')
    //         ->join('reu_reunionbitacoraasistente as ra', 'rb.oidReunionBitacora', 'ra.ReunionBitacora_oidBitacora')
    //         ->where('rb.oidReunionBitacora', '=', $oidAsistente)
    //         ->select('oidReunionBitacora')
    //         ->get();
    //     return $idBitacora;
    // }
}
