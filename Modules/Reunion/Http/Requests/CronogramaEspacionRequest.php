<?php

namespace Modules\Reunion\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CronogramaEspacionRequest extends FormRequest
{

  public function authorize()
  {
    return true;
  }

  public function rules()
  {
    $validacion = array(
      "txAsuntoCronogramaEspacios" => "nullable|string",
      "lsModalidadTerceroPeriodicidad" => "required",
      "dtFechaCronogramaEspacios" => "required|date",
      "hrHoraCronogramaEspacios" => "required",
      "txUbicacionCronogramaEspacios" => "required|string",
      "Tercero_oidResponsable" => "required",
      'intDuracionCronogramaEspacios' => 'required',
      'Tercero_idCliente' => 'nullable',
    );

    return $validacion;
  }

  public function messages()
  {
    $mensaje = array();
    $mensaje["txAsuntoCronogramaEspacios.required"] =  "El campo Asunto es obligatorio";
    $mensaje["txAsuntoCronogramaEspacios.string"] = "El campo Asunto sólo puede contener caracteres afabéticos, númericos, guión y guión bajo";
    $mensaje["lsModalidadTerceroPeriodicidad.required"] = "El campo Tipo de reunión es obligatorio";
    $mensaje["dtFechaCronogramaEspacios.required"] =  "El campo Fecha es obligatorio";
    $mensaje["dtFechaCronogramaEspacios.date"] =  "El campo Fecha debe ser en formato fecha";
    $mensaje["hrHoraCronogramaEspacios.required"] = "El campo Hora es obligatorio";
    $mensaje["txUbicacionCronogramaEspacios.required"] =  "El campo Ubicación es obligatorio";
    $mensaje["txUbicacionCronogramaEspacios.string"] = "El campo Ubicación sólo puede contener caracteres afabéticos, númericos, guión y guión bajo";
    $mensaje["Tercero_oidResponsable.required"] = "El campo Responsable es obligatorio";
    $mensaje["intDuracionCronogramaEspacios.required"] = "El campo Duración de la reunión es obligatorio";

    return $mensaje;
  }
}
