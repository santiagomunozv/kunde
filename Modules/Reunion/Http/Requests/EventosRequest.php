<?php

namespace Modules\Reunion\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EventosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            "txAnioEvento" => "required|string",
            "txCodigoEvento" => "required|alpha",
            "txNombreEvento" => "required|string|",
            "txEstadoEvento" => "required",
        );

        return $validacion;
    }



    public function messages()
    {
        $mensaje = array();
        $mensaje["txAnioEvento.required"] =  "El campo Año es obligatorio";
        $mensaje["txCodigoEvento.required"] =  "El campo Código es obligatorio";
        $mensaje["txCodigoEvento.alpha"] = "El campo Código sólo puede contener caracteres afabéticos";
        $mensaje["txNombreEvento.string"] = "El campo Nombre sólo puede contener caracteres afabéticos";
        $mensaje["txNombreEvento.required"] =  "El campo Nombre es obligatorio";
        $mensaje["txEstadoEvento.required"] =  "El campo Estado es obligatorio";

        return $mensaje;
    }
}
