<?php

    namespace Modules\Reunion\Http\Requests;
    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Validation\Rule;

    class ReunionRequest extends FormRequest
    {

        public function authorize()
        {
            return true;
        }
    
        public function rules()
        {
            $validacion = array(
            "txAsuntoReunion" => "nullable|string",
            "txTipoReunion" => "required",
            "dtFechaReunion" => "required|date",
            "hrHoraReunion" => "required",
            "txUbicacionReunion" => "required|string",
            "Tercero_oidResponsable" => "required",
            "Evento_oidEvento" => "required",
            'txNombreReunionAsistente.*' => 'required',
            'txCorreoAsistente.*' => 'required',
            'intDuracionReunion'=> 'required',
            'Tercero_idCliente'=> 'nullable',
            'hrHoraFinalReunion' => 'nullable'

            );

            return $validacion;
        }

    

        public function messages()
        {
            $mensaje = array();
            $mensaje["txAsuntoReunion.required"] =  "El campo Asunto es obligatorio";
            $mensaje["txAsuntoReunion.string"] = "El campo Asunto sólo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["txTipoReunion.required"] = "El campo Tipo de reunión es obligatorio";
            $mensaje["dtFechaReunion.required"] =  "El campo Fecha es obligatorio";
            $mensaje["dtFechaReunion.date"] =  "El campo Fecha debe ser en formato fecha";
            $mensaje["hrHoraReunion.required"] = "El campo Hora es obligatorio";
            $mensaje["txUbicacionReunion.required"] =  "El campo Ubicación es obligatorio";
            $mensaje["txUbicacionReunion.string"] = "El campo Ubicación sólo puede contener caracteres afabéticos, númericos, guión y guión bajo";
            $mensaje["Tercero_oidResponsable.required"] = "El campo Responsable es obligatorio";
            $mensaje["Evento_oidEvento.required"] = "El campo Eventos es obligatorio";
            $mensaje["intDuracionReunion.required"] = "El campo Duración de la reunión es obligatorio";

            $mensaje = array();
            $total = ($this->get("oidReunionAsistente") !== null ) ? count($this->get("oidReunionAsistente")) : 0;
            for($j=0; $j < $total; $j++)
            {
                    $mensaje['txNombreReunionAsistente.'.$j.'.required'] = "El Nombre es obligatorio en el registro ".($j+1);
                    $mensaje['txCorreoAsistente.'.$j.'.required'] = "El Correo es obligatorio en el registro ".($j+1);
            }
    

            return $mensaje;
        }
    }
        