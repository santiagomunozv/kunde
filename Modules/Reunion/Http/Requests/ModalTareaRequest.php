<?php

namespace Modules\Reunion\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModalTareaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validacion = array(
            'txEstadoReunion' => 'nullable',
            'Tercero_oidResponsable.*' => 'required',
            'txAsuntoTarea.*' => 'required',
            'dtFechaVencimientoTarea.*' => 'required',
            'txNombreAsistenteBitacoraTarea.*' => 'required',
            'txCorreoAsistenteBitacoraTarea.*' => 'required',
            'txLinkReunionBitacora' => 'nullable'
        );

        return $validacion;
    }

    public function messages()
    {
        $mensaje = array();

        $mensaje["txEstadoReunion.required"] = "El campo Estado es obligatorio";


        $mensaje = array();
        $total = ($this->get("oidTarea") !== null) ? count($this->get("oidTarea")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['Tercero_oidResponsable.' . $j . '.required'] = "El Responsable es obligatorio en el registro " . ($j + 1);
            $mensaje['txAsuntoTarea.' . $j . '.required'] = "La Tarea es obligatoria en el registro " . ($j + 1);
            $mensaje['dtFechaVencimientoTarea.' . $j . '.required'] = "La Fecha es obligatoria en el registro " . ($j + 1);
        }

        $mensaje = array();
        $total = ($this->get("oidBitacoraTareaAsistente") !== null) ? count($this->get("oidBitacoraTareaAsistente")) : 0;
        for ($j = 0; $j < $total; $j++) {
            $mensaje['txNombreAsistenteBitacoraTarea.' . $j . '.required'] = "El Nombre es obligatorio en el registro " . ($j + 1);
            $mensaje['txCorreoAsistenteBitacoraTarea.' . $j . '.required'] = "El correo es obligatoria en el registro " . ($j + 1);
        }

        return $mensaje;
    }
}
