<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\AsociadoNegocio\Entities\TerceroPeriodicidadModel;
use Modules\Reunion\Entities\CronogramaEspacioModel;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionModel;
use Modules\Reunion\Http\Requests\CronogramaEspacionRequest;

class CronogramaEspacioController extends Controller
{

  public function index(Request $request)
  {
    if ($request->ajax()) {
      $events = $this->obtenerDatosCronograma();
      return response()->json($events);
    }
    $evento = EventosModel::get();
    return view("reunion::cronogramaEspaciosCalendarForm", compact($evento), ["evento" => $evento]);
  }

  public function edit($id)
  {
    $cronogramaEspacios = CronogramaEspacioModel::find($id);
    $tercero = TerceroModel::find($cronogramaEspacios->Tercero_idCliente);
    $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
    $asn_tercerolistaCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
      ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
      ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
      ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
      ->pluck('txNombreTercero', 'oidTercero');

    $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
      ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
      ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
      ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
      ->pluck('txNombreTercero', 'oidTercero');
    return view("reunion::cronogramaEspaciosForm", compact("asn_tercerolistaEmpleado", "asn_tercerolistaCliente", "cronogramaEspacios", "evento", "tercero"), ['cronogramaEspacios' => $cronogramaEspacios]);
  }

  public function update($id, CronogramaEspacionRequest $request)
  {
    $cronogramaEspacios = CronogramaEspacioModel::find($id);

    $f = $request->dtFechaCronogramaEspacios;
    $h = $request->hrHoraCronogramaEspacios;
    $d = $request->intDuracionCronogramaEspacios;
    $idResponsable = $request->Tercero_oidResponsable;

    $intInitial = strtotime($f . " " . $h);
    $horai = date('H:i:s', $intInitial);

    $intFinal = strtotime("+" . $d . " minute", strtotime($horai));
    $hFinal = date('H:i:s', $intFinal);

    $task_exists = ReunionModel::valitdateTimeTask($idResponsable, $horai, $hFinal, $f);
    $availability = CronogramaEspacioModel::validateAvailabilityCronograma($cronogramaEspacios, $d);
    if (!$availability) {
      return abort(409, "Está excediendo la cantidad de horas contradadas por el cliente");
    }

    DB::beginTransaction();
    try {
      if (count($task_exists) > 0) {
        return abort(409, 'Ya tienes la tarea ' .  $task_exists[0]->txAsuntoReunion . ' a las ' . $task_exists[0]->hrHoraReunion . ' para esta fecha.');
      }

      $fecha = $request->get("dtFechaCronogramaEspacios") . " " . $request->get("hrHoraCronogramaEspacios");
      $fechaJuliana = strtotime($fecha) * 1000;
      $fechaFinal = strtotime("+" . $request->get("intDuracionCronogramaEspacios") . " minute", strtotime($fecha));
      $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
      $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

      $reunion = new ReunionModel;
      $reunion->txAsuntoReunion = $request->get('txAsuntoCronogramaEspacios');
      $reunion->txTipoReunion = $request->get('lsModalidadTerceroPeriodicidad');
      $reunion->dtFechaReunion = $request->get('dtFechaCronogramaEspacios');
      $reunion->hrHoraReunion = $request->get('hrHoraCronogramaEspacios');
      $reunion->txFechaJulianoReunion = $fechaJuliana;
      $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
      $reunion->txUbicacionReunion = $request->get('txUbicacionCronogramaEspacios');
      $reunion->Tercero_oidResponsable = $request->get('Tercero_oidResponsable');
      $reunion->txDescripcionReunion = $request->get('txDescripcionCronogramaEspacios');
      $reunion->Evento_oidEvento = $cronogramaEspacios->Evento_oidEvento;
      $reunion->intDuracionReunion = $request->get('intDuracionCronogramaEspacios');
      $reunion->Tercero_idCliente = $cronogramaEspacios->Tercero_idCliente;
      $reunion->txEstadoReunion = "Pendiente";
      $reunion->hrHoraFinalReunion = $hFinal;

      $cronogramaEspacios->chProgramadoCronogramaEspacios = 1;
      $cronogramaEspacios->save();

      $terceroPeriodicidad = TerceroPeriodicidadModel::where('Tercero_oidTercero', $cronogramaEspacios->Tercero_idCliente)->first();
      $duracionHora = CronogramaEspacioModel::defineDurationHour($d);
      $terceroPeriodicidad->inHorasConsumidasTerceroPeriodicidad = $terceroPeriodicidad->inHorasConsumidasTerceroPeriodicidad + $duracionHora;
      $terceroPeriodicidad->save();

      $reunion->save();
      DB::commit();
      return response(['oidReunion' => $reunion->oidReunion], 201);
    } catch (\Exception $e) {
      DB::rollback();
      return abort(500, $e->getMessage());
    }
  }

  public function obtenerCronogramaEspacios()
  {
    $events = $this->obtenerDatosCronograma();
    echo json_encode(
      array(
        "success" => 1,
        "result" => $events
      )
    );
  }

  public function obtenerDatosCronograma()
  {
    $query = CronogramaEspacioModel::selectRaw(
      "oidCronogramaEspacios AS id,
      CONCAT(txAsuntoCronogramaEspacios, ' ', hrHoraCronogramaEspacios) AS title,
      dtFechaCronogramaEspacios AS start,
      hrHoraCronogramaEspacios as time,
      CONCAT('cronogramaespacios/',oidCronogramaEspacios,'/edit') AS url,
      txCodigoEvento AS class,
      '' AS body,
      'si' AS event"
    )
      ->join('reu_evento', 'reu_cronogramaespacios.Evento_oidEvento', 'reu_evento.oidEvento')
      ->where('chProgramadoCronogramaEspacios', 0)
      ->get();

    if (count($query) > 0) {
      return $query;
    }
  }
}
