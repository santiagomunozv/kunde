<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionEventoAsistenteModel;
use Modules\Reunion\Entities\ReunionEventoModel;
use Modules\Reunion\Http\Requests\EventosRequest;

class EventosController extends Controller
{
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }

        $eventos = EventosModel::select("oidEvento", "txAnioEvento", "txCodigoEvento", "txNombreEvento", "txEstadoEvento")->orderBy('oidEvento', 'DESC')->get();
        return view('reunion::eventosGrid', compact('permisos', 'eventos'));
    }

    public function create()
    {
        $eventos = new EventosModel();
        $reunionEventos = new ReunionEventoModel();
        $reunionEventosAsistentes = new ReunionEventoAsistenteModel();
        $anio = [
            "2024" => "2024",
            "2025" => "2025"
        ];

        $asistentes = [
            "TODO EL PERSONAL",
            "TODOS LOS COMITÉS",
            "JEFES, DIRECTIVOS O REPRESENTANTE LEGAL",
            "BRIGADA",
            "SECRETARIOS DE LOS COMITÉS",
            "RESPONSABLE SST",
            "REPRESENTANTE LEGAL",
            "COPASST",
            "COCOLA",
            "ADMINISTRATIVO",
            "MIEMBROS DE LOS COMITÉS"
        ];

        return view('reunion::eventosForm', ['eventos' => $eventos, 'anio' => $anio, 'reunionEventos' => $reunionEventos, 'reunionEventosAsistentes' => $reunionEventosAsistentes, 'asistentes' => $asistentes]);
    }

    public function store(EventosRequest $request)
    {
        DB::beginTransaction();
        try {
            $eventos = EventosModel::create($request->all());

            if ($request['imagenEvento']) {
                $origen = Storage::url($request['imagenEvento']);
                $nombre = pathinfo($origen, PATHINFO_FILENAME);
                $extension = pathinfo($origen, PATHINFO_EXTENSION);
                $eventos->txArchivoEvento = $nombre . '.' . $extension;
            }
            $eventos->save();

            if ($eventos->oidEvento) {
                $modelo = new ReunionEventoModel();
                $total = ($request['oidEventoActividad'] !== null) ? count($request['oidEventoActividad']) : 0;
                for ($i = 0; $i <  $total; $i++) {
                    $indice = array('oidEventoActividad' => $request['oidEventoActividad'][$i]);
                    $datos = [
                        'txDescripcionEventoActividad' => $request["txDescripcionEventoActividad"][$i],
                        'evento_oidEvento' => $eventos->oidEvento
                    ];
                    $guardar = $modelo::updateOrCreate($indice, $datos);
                }
            }

            if ($eventos->oidEvento) {
                $modelo = new ReunionEventoAsistenteModel();
                $total = ($request['oidEventoAsistente'] !== null) ? count($request['oidEventoAsistente']) : 0;
                for ($i = 0; $i <  $total; $i++) {
                    $indice = array('oidEventoAsistente' => $request['oidEventoAsistente'][$i]);
                    $datos = [
                        'txAsistenteEventoAsistente' => $request["txAsistenteEventoAsistente"][$i],
                        'Evento_oidEvento' => $eventos->oidEvento
                    ];
                    $guardar = $modelo::updateOrCreate($indice, $datos);
                }
            }

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidEvento' => $eventos->oidEvento], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function show($id)
    {
        return view('eventos::show');
    }

    public function edit($id)
    {
        $eventos = EventosModel::find($id);
        $reunionEventos = ReunionEventoModel::where('evento_oidEvento', $id)->get();
        $reunionEventosAsistentes = ReunionEventoAsistenteModel::where('Evento_oidEvento', $id)->get();

        $anio = [
            "2024" => "2024",
            "2025" => "2025"
        ];

        $asistentes = [
            "TODO EL PERSONAL",
            "TODOS LOS COMITÉS",
            "JEFES, DIRECTIVOS O REPRESENTANTE LEGAL",
            "BRIGADA",
            "SECRETARIOS DE LOS COMITÉS",
            "RESPONSABLE SST",
            "REPRESENTANTE LEGAL",
            "COPASST",
            "COCOLA",
            "ADMINISTRATIVO",
            "MIEMBROS DE LOS COMITÉS"
        ];

        return view('reunion::eventosForm', ['eventos' => $eventos, 'anio' => $anio, 'reunionEventos' => $reunionEventos, 'reunionEventosAsistentes' => $reunionEventosAsistentes, 'asistentes' => $asistentes]);
    }

    public function update(EventosRequest $request, $id)
    {
        // dd($request->all());
        $eliminarImagenes = $request['eliminarImagen'];
        DB::beginTransaction();
        try {
            $eventos = EventosModel::find($id);
            $eventos->fill($request->all());

            if ($eliminarImagenes) {
                $eventos->txArchivoEvento = null;
                $rutaImagenEliminar = EventosModel::find($eliminarImagenes)->txArchivoEvento;

                $this->eliminarImagen($rutaImagenEliminar);
            }

            if ($request['imagenEvento']) {
                $origen = Storage::url($request['imagenEvento']);
                $nombre = pathinfo($origen, PATHINFO_FILENAME);
                $extension = pathinfo($origen, PATHINFO_EXTENSION);
                $eventos->txArchivoEvento = $nombre . '.' . $extension;
            }

            $eventos->save();

            $modelo = new ReunionEventoModel();
            $idsEliminar = explode(',', $request['eliminarEventoActividad']);
            $modelo::whereIn('oidEventoActividad', $idsEliminar)->delete();
            $total = ($request['txDescripcionEventoActividad'] !== null) ? count($request['txDescripcionEventoActividad']) : 0;
            for ($i = 0; $i <  $total; $i++) {
                $indice = array('oidEventoActividad' => $request['oidEventoActividad'][$i]);
                $datos = [
                    'txDescripcionEventoActividad' => $request["txDescripcionEventoActividad"][$i],
                    'evento_oidEvento' => $eventos->oidEvento
                ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            $modelo = new ReunionEventoAsistenteModel();
            $idsEliminar = explode(',', $request['eliminarEventoAsistente']);
            $modelo::whereIn('oidEventoAsistente', $idsEliminar)->delete();
            $total = ($request['txAsistenteEventoAsistente'] !== null) ? count($request['txAsistenteEventoAsistente']) : 0;
            for ($i = 0; $i <  $total; $i++) {
                $indice = array('oidEventoAsistente' => $request['oidEventoAsistente'][$i]);
                $datos = [
                    'txAsistenteEventoAsistente' => $request["txAsistenteEventoAsistente"][$i],
                    'Evento_oidEvento' => $eventos->oidEvento
                ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidEvento' => $eventos->oidEvento], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function destroy($id)
    {
        EventosModel::destroy($id);
        return redirect('/reuniones/evento');
    }

    public function subirEvento(Request $request)
    {
        $imagen = $request->file('file');
        $path = Storage::disk('tmp')->put('reunion/evento', $imagen);
        return response(['tmpPath' => $path], 200);
    }

    //Funcion para eliminar imagenes mediante id
    public function eliminarImagen($imagen)
    {
        $path = Storage::disk('discoc')->put("asociadonegocio/tercero/", $imagen);

        if (file_exists($path)) {
            Storage::disk('discoc')->delete("asociadonegocio/tercero/" . $imagen);
        }
    }
}
