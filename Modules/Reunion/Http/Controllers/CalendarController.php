<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionModel;

class CalendarController extends Controller
{

  public function index(Request $request)
  {
    $rolUsuario = Session::get('rolUsuario');

    if ($request->ajax()) {
      $empleadoId = $request->empleado_id ?? null;
      $events = $this->obtenerDatosReunion($empleadoId);
      return response()->json($events);
    }

    // Si el rol es 1, carga todos los empleados
    if ($rolUsuario == 1) {
      $empleados = TerceroModel::from('asn_tercero as t')
        ->select(['t.txNombreTercero', 't.oidTercero'])
        ->join('reu_reunion as r', 't.oidTercero', 'r.Tercero_oidResponsable')
        ->groupBy('t.oidTercero')
        ->get();
    } else {
      // Caso contrario, solo el usuario actual
      $empleados = TerceroModel::where('oidTercero', Session::get('oidTercero'))->get();
    }

    $evento = EventosModel::get();
    return view("reunion::calendarioForm", compact('evento', 'empleados'));
  }

  public function show($id)
  {
    $events = $this->formatData();
    return response()->json($events);
  }

  public function obtenerDatosReunion($empleadoId = null)
  {
    $rolUsuario = Session::get('rolUsuario');
    $oidTercero = Session::get('oidTercero');
    $query = ReunionModel::selectRaw(
      "oidReunion AS id,
      CONCAT(txAsuntoReunion, ' ', hrHoraReunion) AS title,
      dtFechaReunion AS start,
      hrHoraReunion as time,
      CONCAT('reunion/',oidReunion,'/edit') AS url,
      txCodigoEvento AS class,
      '' AS body,
      'si' AS event"
    )
      ->join('reu_evento', 'reu_reunion.Evento_oidEvento', 'reu_evento.oidEvento');

    // Si no es rol 1, solo ve las reuniones del usuario logueado
    if ($rolUsuario != 1) {
      $query->where('Tercero_oidResponsable', $oidTercero);
    } else if ($empleadoId) {
      // Si selecciona un empleado, muestra solo sus reuniones
      $query->where('Tercero_oidResponsable', $empleadoId);
    }

    $query = $query->get();

    if (count($query) > 0) {
      return $query;
    }
  }
}
