<?php

namespace Modules\Reunion\Http\Controllers;

use App\Traits\WebResponseTrait;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\TareaModel;
use Modules\Reunion\Entities\AdjuntosModel;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReagendaReunionModel;
use Modules\Reunion\Entities\ReunionAsistenteModel;
use Modules\Reunion\Entities\ReunionBitacoraAsistenteModel;
use Modules\Reunion\Entities\ReunionBitacoraModel;
use Modules\Reunion\Entities\ReunionEventoActividadModel;
use Modules\Reunion\Entities\ReunionEventoModel;
use Modules\Reunion\Entities\ReunionModel;
use Modules\Reunion\Http\Requests\ModalTareaRequest;
use Modules\Reunion\Http\Requests\ReunionRequest;


class TareasEstadoController extends Controller
{
    use WebResponseTrait;

    private static $FINAL_PATH = "plantrabajo/";

    public function index()
    {
        $tareas = new ReunionModel();
        $tercero = new TerceroModel();

        $client     = isset($_GET['cli']) ? $_GET['cli'] : null;
        $employee   = isset($_GET['emp']) ? $_GET['emp'] : (Session::get('rolUsuario') == 1 ? null : Session::get('oidTercero'));
        $start      = isset($_GET['fei']) ? $_GET['fei'] : null;
        $end        = isset($_GET['fef']) ? $_GET['fef'] : null;

        $startToday = isset($_GET['fei']) ? $_GET['fei'] : date('Y-m-d');
        $endToday = isset($_GET['fef']) ? $_GET['fef'] : date('Y-m-d');

        $pendientes = $tareas::with('bitacora')->status('Pendiente')->client($client)->Employee($employee)->get();
        $enProceso = $tareas::with('bitacora')->status('En proceso')->client($client)->Employee($employee)->get();
        $completos = $tareas::with('bitacora')->status('Terminado')->client($client)->Employee($employee)->get();

        $idResponsable = $tercero->getAllPropertysTerceroByName('oidTercero');
        $nombreResponsable = $tercero->getAllPropertysTerceroByName('txNombreTercero');

        return view('reunion::tareasEstadoForm', compact('pendientes', 'enProceso', 'completos',  'idResponsable', 'nombreResponsable'));
    }

    public function create()
    {
        $tercero = new TerceroModel();

        $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
        $asn_tercerolistaCliente = $tercero->getTerceroAcoordingTypeAndState('Cli');
        $asn_tercerolistaEmpleado = $tercero->getTerceroAcoordingTypeAndState('Emp');

        return view('reunion::modalTareaCreateForm', compact('evento', 'asn_tercerolistaCliente', 'asn_tercerolistaEmpleado'));
    }

    public function store(ReunionRequest $request)
    {
        $f = $request->dtFechaReunion;
        $h = $request->hrHoraReunion;
        $d = $request->intDuracionReunion;
        $id = $request->Tercero_oidResponsable;

        $intInitial = strtotime($f . " " . $h);
        $horai = date('H:i:s', $intInitial);

        $intFinal = strtotime("+" . $d . " minute", strtotime($horai));
        $hFinal = date('H:i:s', $intFinal);

        $task_exists = ReunionModel::valitdateTimeTask($id, $horai, $hFinal, $f);

        $asunto = "";
        DB::beginTransaction();
        try {
            if (count($task_exists) > 0) {
                return abort(500, 'Ya tienes la tarea ' .  $task_exists[0]->txAsuntoReunion . ' a las ' . $task_exists[0]->hrHoraReunion . ' para esta fecha.');
            }

            if (isset($request['Evento_oidEvento'])  && isset($request['Tercero_idCliente'])) {
                $eventInput = $this->getEvent($request->get('Evento_oidEvento'));
                $clientInput = $this->getClient($request->get('Tercero_idCliente'));
                $asunto = $eventInput->txNombreEvento . ' - ' . $clientInput->txNombreTercero;
            } else {
                return abort(404);
            }
            $fecha = $request->get("dtFechaReunion") . " " . $request->get("hrHoraReunion");
            $fechaJuliana = strtotime($fecha) * 1000;
            $fechaFinal = strtotime("+" . $request->get("intDuracionReunion") . " minute", strtotime($fecha));
            $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
            $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

            $reunion = new ReunionModel;
            $reunion->txAsuntoReunion = $asunto;
            $reunion->txTipoReunion = $request->get('txTipoReunion');
            $reunion->dtFechaReunion = $request->get('dtFechaReunion');
            $reunion->hrHoraReunion = $request->get('hrHoraReunion');
            $reunion->txFechaJulianoReunion = $fechaJuliana;
            $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
            $reunion->txUbicacionReunion = $request->get('txUbicacionReunion');
            $reunion->Tercero_oidResponsable = $request->get('Tercero_oidResponsable');
            $reunion->txDescripcionReunion = $request->get('txDescripcionReunion');
            $reunion->Evento_oidEvento = $request->get('Evento_oidEvento');
            $reunion->chEstadoReunion = $request->get('chEstadoReunion');
            $reunion->txDescripcionEstadoReunion = $request->get('txDescripcionEstadoReunion');
            $reunion->intDuracionReunion = $request->get('intDuracionReunion');
            $reunion->Tercero_idCliente = $request->get('Tercero_idCliente');
            $reunion->txEstadoReunion = "Pendiente";
            $reunion->hrHoraFinalReunion = $hFinal;

            $reunion->save();
            DB::commit();
            return response(['oidReunion' => $reunion->oidReunion], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }
    //modal filtros
    public function show($id)
    {
        $tercero = new TerceroModel();

        $asn_tercerolistaCliente = $tercero->getTerceroAcoordingTypeAndState('Cli');
        $asn_tercerolistaEmpleado = $tercero->getTerceroAcoordingTypeAndState('Emp');

        return view('reunion::modalFiltroForm', compact('asn_tercerolistaCliente', 'asn_tercerolistaEmpleado'));
    }

    public function edit($id)
    {
        $tercero = new TerceroModel();
        $reunion = ReunionModel::leftJoin('rgs_prospecto', 'reu_reunion.Prospecto_oidProspecto', '=', 'rgs_prospecto.oidProspecto')->find($id);
        $bitacoras = new ReunionBitacoraModel();
        $bitacora = $bitacoras->getBitacoraByIdReunion($id);
        $reagendas = new ReagendaReunionModel();
        $reagenda = $reagendas->getReagendaByTerceroId($reunion->Tercero_idCliente);

        $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
        $asn_tercerolistaEmpleado = $tercero->getTerceroAcoordingTypeAndState('Emp', 'Activo');
        $asn_tercerolistaCliente = $tercero->getTerceroAcoordingTypeAndState('Cli', 'Activo');
        $reunionAsistente = ReunionAsistenteModel::where('Reunion_oidReunion', '=', $id)->get();

        #Adjuntos
        $adjuntos = AdjuntosModel::where('Reunion_idReunion', '=', $id)->get();

        return view('reunion::modalTareaForm', compact("bitacora", "asn_tercerolistaEmpleado", "asn_tercerolistaCliente", "reunionAsistente", "reunion", "evento", "reagenda", "adjuntos"), ['reunion' => $reunion]);
    }

    public function update(ModalTareaRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            //actualizar estado de Reunion
            $modeloReunion = new ReunionModel();
            $indice = ['oidReunion' => $id];
            $datos = [
                'txEstadoReunion' => $request->get('txEstadoReunion'),
                'txDescripcionReunion' => $request->get('txDescripcionReunion'),
            ];
            $guardarReunion = $modeloReunion::updateOrCreate($indice, $datos);

            #Crear o actualizar Bitácora
            if (isset($guardarReunion->oidReunion) && $request->get('swhBitacora') === 'on') {
                $modeloBitacora = new ReunionBitacoraModel();
                $indiceBitacora = ['oidReunionBitacora' => $request->get('oidReunionBitacora')];
                $datosBitacora = [
                    'txActividadReunionBitacora' => $request->get('txActividadReunionBitacora'),
                    'dtFechaReunionBitacora' => $request->get('dtFechaReunionBitacora'),
                    'hrHoraInicioReunionBitacora' => $request->get('hrHoraInicioReunionBitacora'),
                    'hrHoraFinalReunionBitacora' => $request->get('hrHoraFinalReunionBitacora'),
                    'txObservacionReunionBitacora' => $request->get('txObservacionReunionBitacora'),
                    'txLinkReunionBitacora' => $request->get('txLinkReunionBitacora'),
                    'Reunion_idReunion' => $id
                ];
                $grabarBitacora = $modeloBitacora::updateOrCreate($indiceBitacora, $datosBitacora);
            };

            if (isset($grabarBitacora->Reunion_idReunion)) {
                //guardar Asistentes
                $modeloAsistente = new ReunionBitacoraAsistenteModel();
                $idsEliminar = explode(',', $request['eliminarResponsable']);
                $modeloAsistente::whereIn('oidBitacorAsistente', $idsEliminar)->delete();

                $totalAsistentes = ($request->get('oidBitacorAsistente') !== null ? count($request->get('oidBitacorAsistente')) : 0);
                for ($i = 0; $i < $totalAsistentes; $i++) {
                    $indiceAsistente = ['oidBitacorAsistente' => $request->get('oidBitacorAsistente')[$i]];
                    $datosAsistente = [
                        'txNombreBitacoraAsistente' => $request->get('txNombreBitacoraAsistente')[$i],
                        'txCorreoBitacoraAsistente' => $request->get('txCorreoBitacoraAsistente')[$i],
                        'shTalleristaAsistente' => $request->get('shTalleristaAsistente')[$i],
                        'ReunionBitacora_oidBitacora' => $grabarBitacora->oidReunionBitacora
                    ];
                    $grabarAsistente = $modeloAsistente::updateOrCreate($indiceAsistente, $datosAsistente);
                }

                //guardar Pendientes
                $cliente = $modeloReunion->getClienteById($id);
                $tarea = new TareaModel();
                $idsEliminarTarea = explode(',', $request['eliminarPendiente']);
                $tarea::whereIn('oidTarea', $idsEliminarTarea)->delete();
                $totalPendientes = ($request['oidTarea'] !== null) ? count($request['oidTarea']) : 0;
                for ($i = 0; $i < $totalPendientes; $i++) {
                    $indicePendiente = ['oidTarea' => $request['oidTarea'][$i]];
                    $datosPendiente = [
                        'dtFechaElaboracionTarea' => date('Y-m-d'),
                        'txAsuntoTarea' => $request['txAsuntoTarea'][$i] . ' - ' . $cliente->nombreCliente,
                        'txPrioridadTarea' => 'Alta',
                        'txAreaTarea' => 8,
                        'txResponsableTarea' => $request['Tercero_oidResponsable'][$i],
                        'txEstadoTarea' => 'Nuevo',
                        'dtFechaVencimientoTarea' => $request['dtFechaVencimientoTarea'][$i],
                        'ReunionBitacora_oidBitacora' => $grabarBitacora->oidReunionBitacora,
                    ];

                    $grabarAsistente = $tarea::updateOrCreate($indicePendiente, $datosPendiente);
                }
            }

            //Guardar actividad pos encuentro
            $eventoActividad = new ReunionEventoActividadModel();
            $totalEventoActividad = ($request->get('txDescripcionEventoActividad') !== null ? count($request->get('txDescripcionEventoActividad')) : 0);
            for ($i = 0; $i < $totalEventoActividad; $i++) {
                $indiceAsistente = ['oidReunionEventoActividad' => $request->get('oidReunionEventoActividad')[$i]];
                $datosAsistente = [
                    'chCumplidaEventoActividad' => $request->get('chCumplidaEventoActividad')[$i],
                    'EventoActividad_oidEventoActividad' => $request->get('EventoActividad_oidEventoActividad')[$i],
                    'Reunion_idReunion' => $id
                ];

                if ($request['Tercero_oidResponsableActividad'][$i]) {
                    $cliente = $modeloReunion->getClienteById($id);
                    TareaModel::create([
                        'dtFechaElaboracionTarea' => date('Y-m-d'),
                        'txAsuntoTarea' => $request['txAsuntoActividad'][$i] . ' - ' . $cliente->nombreCliente,
                        'txPrioridadTarea' => 'Alta',
                        'txAreaTarea' => 8,
                        'txResponsableTarea' => $request['Tercero_oidResponsableActividad'][$i],
                        'txEstadoTarea' => 'Nuevo',
                        'dtFechaVencimientoTarea' => $request['dtFechaVencimientoActividad'][$i],
                    ]);
                }

                $eventoActividad::updateOrCreate($indiceAsistente, $datosAsistente);
            }

            //Guardar Adjuntos
            $datosEliminar = substr($request['eliminarImagen'], 0, strlen($request['eliminarImagen']) - 1);
            if (strlen($datosEliminar) > 0) {
                foreach (explode(',', $datosEliminar) as $idEliminar) {
                    $this->eliminarFile($idEliminar);
                    AdjuntosModel::where('oidAdjunto', $idEliminar)->delete();
                }
            }

            if (isset($request['imagenAdjunto'])) {
                $total = count($request['imagenAdjunto']);
                for ($i = 0; $i < $total; $i++) {
                    $origin = $request['imagenAdjunto'][$i];
                    $extension = File::extension($origin);
                    $name = basename($origin);
                    $destination = 'public/images/tareasPorEstado/bitacora/' . $guardarReunion->oidReunion . "/" . $name;
                    $path = Storage::disk('local')->move($origin, $destination);

                    if ($path) {
                        AdjuntosModel::create([
                            'txNombreArvchivo' => $name,
                            'txRutaArchivo' => $destination,
                            'txTipoArchivo' => $extension,
                            'txDescripcionArchivo' => null,
                            'dtFechaSubidaArchivo' => date('Y-m-d'),
                            'Reunion_idReunion' => $id
                        ]);
                    } else {
                        return abort(200, 'no se pudo guardar el archivo');
                        $this->limpiarAdjuntosFolder();
                    }
                }
            }
            DB::commit();
            return response(['oidReunion' => $modeloReunion->oidReunion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            ReunionModel::destroy($id);
            return response(200);
        } catch (\Exception $e) {
            // devolvemos error 423 -> Locked: este recurso está bloqueado.
            return response(['tarea' => ''], 423);
        }
    }

    //método encargardo de obtener Json de asistentes y pendientes para el modal
    public function cargarJson($id)
    {
        $reuniones = new ReunionBitacoraModel();
        $tareas = new TareaModel();
        $reunion = ReunionModel::find($id);

        $reunionBitacora = $reuniones->getBitacoraByIdReunion($id);
        $asistentes = [];
        $pendientes = [];
        $actividadPosEncuentro = [];
        if (isset($reunionBitacora->oidReunionBitacora)) {
            $asistentes = ReunionBitacoraAsistenteModel::where('ReunionBitacora_oidBitacora', $reunionBitacora->oidReunionBitacora)->get();
            $pendientes = $tareas->getTareaByIdBitacora($reunionBitacora->oidReunionBitacora);
        }
        $actividadPosEncuentro = ReunionEventoActividadModel::getEventoActividadByReunion($id);
        return array($asistentes, $pendientes, $actividadPosEncuentro);
    }

    //método encargado de mostrar modal de reagendamiento
    public function reagendar($id)
    {
        $tercero = new TerceroModel();

        $reunion = ReunionModel::where('oidReunion', $id)->first();
        $asn_tercerolistaEmpleado = $tercero->getTerceroAcoordingTypeAndState('Emp');

        return view('reunion::modalReagendarForm', compact('reunion', 'asn_tercerolistaEmpleado'));
    }

    //método encargado de guardar el reagendamiento
    public function reagendarReunion(Request $request, $id)
    {
        DB::beginTransaction();
        $modeloReunion = new ReunionModel();
        $modeloReagenda = new ReagendaReunionModel();

        $oldReunion = $modeloReunion::where('oidReunion', $id)->first();
        $reagenda = $modeloReagenda::where('Reunion_idReunion', $id)->first();

        try {
            //guardar reagenda
            $indiceReagenda = ['oidReagendaReunion' => $reagenda ? $reagenda->oidReagendaReunion : null];
            $datosReagenda = [
                'dtFechaAnteriorReagenda' => $oldReunion->dtFechaReunion,
                'hrHoraAnteriorReagenda' => $oldReunion->hrHoraReunion,
                'txRespAnteriorReagenda' => $oldReunion->Tercero_oidResponsable,
                'txMotivoReagenda' => $request->get('txMotivoReagenda'),
                'txObservacionReagenda' => $request->get('txObservacionReagenda'),
                'Reunion_idReunion' => $id
            ];
            $guardarReagenda = $modeloReagenda::updateOrCreate($indiceReagenda, $datosReagenda);
            //guardar reunion
            if ($guardarReagenda) {
                $indice = ['oidReunion' => $id];
                $datos = [
                    'dtFechaReunion' => $request->get('dtFechaReunion'),
                    'hrHoraReunion' => $request->get('hrHoraReunion'),
                    'intDuracionReunion' => $request->get('intDuracionReunion'),
                    'txUbicacionReunion' => $request->get('txUbicacionReunion'),
                    'Tercero_oidResponsable' => $request->get('Tercero_oidResponsable'),
                    'txDescripcionReunion' => $request->get('txDescripcionReunion'),
                ];
                $guardar = $modeloReunion::updateOrCreate($indice, $datos);
            }
            DB::commit();
            return response(['oidReunion' => $indice], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    //método encargado de retornar la ruta temporal de los asjuntos
    public function adjuntos(Request $request)
    {
        $file = $request->file('file');
        if (!$file->isValid()) return abort(200, 'No se cargo el archivo correctamente.');
        $path = Storage::disk('local')->put('public/images/tareasPorEstado', $file);
        return response()->json(['tmpPath' => $path]);
    }

    //método encargado de consultar archivos de adjuntos
    public function consultarAdjuntos($id)
    {
        $file = AdjuntosModel::find($id);
        $path = Storage::path($file->txRutaArchivo);
        if (file_exists($path)) {
            return response()->file($path);
        } else {
            return response()->file(public_path('images/FichaImagePlaceHolder.png'));
        }
    }

    //método encargado de eleiminar archivos de adjuntos
    public function eliminarFile($id)
    {
        $file = AdjuntosModel::find($id);
        $path = Storage::path($file->txRutaArchivo);
        if (file_exists($path)) {
            unlink($path);
        }
    }

    //metodo encargado de limpiar la carpeta adjuntos
    public function limpiarAdjuntosFolder()
    {
        $files = glob(storage_path('public/images/tareasPorEstado/*'));
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }

    public function getEvent($id)
    {
        return EventosModel::getById($id);
        // return response(['data' => EventosModel::getById($id)], 200);
    }

    public function getClient($id)
    {
        return TerceroModel::getById($id);
        // return response(['data' => TerceroModel::getById($id)], 200);
    }


    public function descargar(Request $request)
    {
        $reunion = ReunionModel::where('oidReunion', $request->id)->first();

        $bitacora = ReunionBitacoraModel::with('asistentes')
            ->where('Reunion_idReunion', $reunion->oidReunion)
            ->first();
        if ($bitacora == null)  return abort(500, "no se encontró bitacora para esta tarea");
        // dd($bitacora);
        // ->toArray();
        $tarea = TareaModel::getTareaByIdBitacora($bitacora['oidReunionBitacora']);

        if (!isset($bitacora)) return abort(500, "no se encontró bitacora para esta tarea");

        $asistentes = $bitacora['asistentes'];

        $participantes = [];
        $total = (count($asistentes) !== null) ? count($asistentes) : 0;
        for ($i = 0; $i < $total; $i++) {
            $participantes[$i]['txNombreAsistente'] = $asistentes[$i]['txNombreBitacoraAsistente'];
        }

        $tareas = [];
        $totalT = (count($tarea) !== null) ? count($tarea) : 0;
        for ($n = 0; $n < $totalT; $n++) {
            $responsable = TerceroModel::find($tarea[0]->Tercero_oidResponsable)->first();
            $tareas[$n]['txNombreTercero'] = $responsable->txNombreTercero;
            $tareas[$n]['txBitacoraActividad'] = $tarea[$n]->txAsuntoTarea;
            $tareas[$n]['daBitacoraFehcaEntrega'] = $tarea[$n]->dtFechaVencimientoTarea;
        }

        $cliente = TerceroModel::find($reunion->Tercero_idCliente)->first();
        $actividad = [
            "nombreActividad" => $bitacora['txActividadReunionBitacora'],
            "fechaPlanTrabajoBitacora" => $bitacora['dtFechaReunionBitacora'],
            "horaInicioPlanTrabajoBitacora" => $bitacora['hrHoraInicioReunionBitacora'],
            "horaSalidaPlanTrabajoBitacora" => $bitacora['hrHoraFinalReunionBitacora'],
            "observacionPlanTrabajoBitacora" => $bitacora['txObservacionReunionBitacora'],
            "txNombreTercero" => $cliente->txNombreTercero,
        ];

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('plantrabajo::impresionBitacora', ['actividad' => (object)$actividad, "participantes" => json_decode(json_encode($participantes), FALSE), "tareas" => json_decode(json_encode($tareas), FALSE)]);

        $fileName = time() . '.' . 'pdf';
        $pdf->save(public_path() . '/' . $fileName);
        $pdf = public_path($fileName);

        $nombre = self::generateFileName($fileName);
        $destino = Storage::disk('discoc')->path($nombre);
        File::move($pdf, $destino);

        return response(['archivo' => $fileName], 201);
    }

    public static function generateFileName($fileName)
    {
        return self::$FINAL_PATH . "{$fileName}";
    }
}
