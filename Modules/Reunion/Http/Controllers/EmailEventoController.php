<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Reunion\Emails\EventoEmail;
use Modules\Reunion\Entities\ReunionModel;

class EmailEventoController extends Controller
{
    public function modalEmail($id)
    {
        // $reunion = ReunionModel::find($id);
        $reuniones = new ReunionModel();
        $reunion = $reuniones->getClienteById($id);
        // dd($reunion);
        return view('reunion::modalEmail', compact('reunion'));
    }

    public function enviarEmail(Request $request)
    {
        // $reunion = ReunionModel::find($request['oidReunion']);
        $reuniones = new ReunionModel();
        $reunion = $reuniones->getClienteById($request['oidReunion']);
        $email = $request['txEmailCliente'];
        // dd($reunion->all());
        // dd($reunion);
        Mail::to($email)->queue(new EventoEmail($reunion));
        return response(['oidReunion' => $reunion->oidReunion], 200);
        // return new EventoEmail($reunion);
    }
}
