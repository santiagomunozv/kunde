<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Session;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use Modules\Reunion\Entities\EventosModel;
use Modules\Reunion\Entities\ReunionAsistenteModel;
use Modules\Reunion\Entities\ReunionModel;

use Modules\Reunion\Http\Requests\ReunionRequest;


class ReunionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $reunion = new ReunionModel();
        $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
        $reunionAsistente = new ReunionAsistenteModel();
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');
        $asn_tercerolistaCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');
        return view('reunion::reunionForm', compact("asn_tercerolistaEmpleado", "asn_tercerolistaCliente", "reunionAsistente", "reunion", "evento"), ['reunion' => $reunion]);
    }

    public function showCalendar()
    {
        // $evento = EventosModel::get();
        $evento = $this->obtenerDatosReunion();
        return view("reunion::calendarioForm", compact($evento), ["evento" => $evento]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('reunion::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ReunionRequest $request)
    {
        DB::beginTransaction();
        try {

            $fecha = $request->get("dtFechaReunion") . " " . $request->get("hrHoraReunion");
            $fechaJuliana = strtotime($fecha) * 1000;
            $fechaFinal = strtotime("+" . $request->get("intDuracionReunion") . " minute", strtotime($fecha));
            $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
            $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

            $reunion = new ReunionModel;
            $reunion->txAsuntoReunion = $request->get('txAsuntoReunion');
            $reunion->txTipoReunion = $request->get('txTipoReunion');
            $reunion->dtFechaReunion = $request->get('dtFechaReunion');
            $reunion->hrHoraReunion = $request->get('hrHoraReunion');
            $reunion->txFechaJulianoReunion = $fechaJuliana;
            $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
            $reunion->txUbicacionReunion = $request->get('txUbicacionReunion');
            $reunion->Tercero_oidResponsable = $request->get('Tercero_oidResponsable');
            $reunion->txDescripcionReunion = $request->get('txDescripcionReunion');
            $reunion->Evento_oidEvento = $request->get('Evento_oidEvento');
            $reunion->chEstadoReunion = $request->get('chEstadoReunion');
            $reunion->txDescripcionEstadoReunion = $request->get('txDescripcionEstadoReunion');
            $reunion->intDuracionReunion = $request->get('intDuracionReunion');
            $reunion->Tercero_idCliente = $request->get('Tercero_idCliente');
            $reunion->txEstadoReunion = "Pendiente";

            $reunion->save();

            $modelo = new ReunionAsistenteModel;
            $total = ($request['oidReunionAsistente'] !== null) ? count($request['oidReunionAsistente']) : 0;
            for ($i = 0; $i <  $total; $i++) {
                $indice = array('oidReunionAsistente' => $request['oidReunionAsistente'][$i]);
                $datos = [
                    'txNombreReunionAsistente' => $request["txNombreReunionAsistente"][$i],
                    'txCorreoAsistente' => $request["txCorreoAsistente"][$i],
                    'Reunion_oidReunion' => $reunion->oidReunion
                ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidReunion' => $reunion->oidReunion], 201);
        } catch (\Exception $e) {
            //si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('reunion::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $reunion = ReunionModel::leftJoin('rgs_prospecto', 'reu_reunion.Prospecto_oidProspecto', '=', 'rgs_prospecto.oidProspecto')->find($id);
        $evento = EventosModel::where("txEstadoEvento", '=', "Activo")->pluck("txNombreEvento", "oidEvento");
        $asn_tercerolistaEmpleado = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Emp')
            ->pluck('txNombreTercero', 'oidTercero');
        $asn_tercerolistaCliente = TerceroModel::join('asn_terceroclasificacion', 'asn_tercero.oidTercero', 'asn_terceroclasificacion.Tercero_oidTercero_1aM')
            ->join('gen_clasificaciontercero', 'asn_terceroclasificacion.ClasificacionTercero_oidTerceroClasificacion_1aM', 'gen_clasificaciontercero.oidClasificacionTercero')
            ->where('asn_tercero.txEstadoTercero', '=', 'Activo')
            ->where('gen_clasificaciontercero.txCodigoClasificacionTercero', '=', 'Cli')
            ->pluck('txNombreTercero', 'oidTercero');

        $reunionAsistente = ReunionAsistenteModel::where('Reunion_oidReunion', '=', $id)->get();
        return view('reunion::reunionForm', compact("asn_tercerolistaEmpleado", "asn_tercerolistaCliente", "reunionAsistente", "reunion", "evento"), ['reunion' => $reunion]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ReunionRequest $request, $id)
    {
        DB::beginTransaction();
        try {

            $fecha = $request->get("dtFechaReunion") . " " . $request->get("hrHoraReunion");
            $fechaJuliana = strtotime($fecha) * 1000;
            $fechaFinal = strtotime("+" . $request->get("intDuracionReunion") . " minute", strtotime($fecha));
            $fechaFinal = date('Y-m-d H:i:s', $fechaFinal);
            $fechaFinalJuliana = strtotime($fechaFinal) * 1000;

            $reunion = ReunionModel::find($id);
            $reunion->txAsuntoReunion = $request->get('txAsuntoReunion');
            $reunion->txTipoReunion = $request->get('txTipoReunion');
            $reunion->dtFechaReunion = $request->get('dtFechaReunion');
            $reunion->hrHoraReunion = $request->get('hrHoraReunion');
            $reunion->txFechaJulianoReunion = $fechaJuliana;
            $reunion->txFechaFinalJulianoReunion = $fechaFinalJuliana;
            $reunion->txUbicacionReunion = $request->get('txUbicacionReunion');
            $reunion->Tercero_oidResponsable = $request->get('Tercero_oidResponsable');
            $reunion->txDescripcionReunion = $request->get('txDescripcionReunion');
            $reunion->Evento_oidEvento = $request->get('Evento_oidEvento');
            $reunion->chEstadoReunion = $request->get('chEstadoReunion');
            $reunion->txDescripcionEstadoReunion = $request->get('txDescripcionEstadoReunion');
            $reunion->intDuracionReunion = $request->get('intDuracionReunion');
            $reunion->save();

            if ($reunion->Prospecto_oidProspecto) {
                $prospecto = ProspectoModel::find($reunion->Prospecto_oidProspecto);
                $prospecto->txEstadoProspecto = $request->get('txEstadoProspecto');
                $prospecto->save();
            }

            $modelo = new ReunionAsistenteModel();
            $idsEliminar = explode(',', $request['eliminarReunionAsistente']);
            $modelo::whereIn('oidReunionAsistente', $idsEliminar)->delete();
            $total = ($request['oidReunionAsistente'] !== null) ? count($request['oidReunionAsistente']) : 0;
            for ($i = 0; $i <  $total; $i++) {
                $indice = array('oidReunionAsistente' => $request['oidReunionAsistente'][$i]);
                $datos = [
                    'txNombreReunionAsistente' => $request["txNombreReunionAsistente"][$i],
                    'txCorreoAsistente' => $request["txCorreoAsistente"][$i],
                    'Reunion_oidReunion' => $id
                ];
                $guardar = $modelo::updateOrCreate($indice, $datos);
            }

            // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidReunion' => $reunion->oidReunion], 200);
        } catch (\Exception $e) {
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        ReunionModel::destroy($id);
        return redirect('/reuniones/reunion');
    }

    /**
     * Get all events from users.
     * @return Response
     */
    public function obtenerReuniones()
    {
        $events = $this->obtenerDatosReunion();
        echo json_encode(
            array(
                "success" => 1,
                "result" => $events
            )
        );
    }

    public function obtenerDatosReunion()
    {
        $query = ReunionModel::selectRaw(
            "oidReunion AS id,
            txAsuntoReunion AS title,
            CONCAT('reunion/',oidReunion,'/edit') AS url,
            txCodigoEvento AS class,
            txFechaJulianoReunion AS start,
            txFechaFinalJulianoReunion AS end,
            txDescripcionReunion AS body,
            'si' AS event"
        )
            ->join('reu_evento', 'reu_reunion.Evento_oidEvento', 'reu_evento.oidEvento')
            ->where('Tercero_oidResponsable', '=', Session::get('oidTercero'))
            ->get();

        if (count($query) > 0) {
            return $query;
        }
    }
}
