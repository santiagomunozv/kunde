<?php

namespace Modules\Reunion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\Reunion\Entities\CronogramaEspacioModel;
use Modules\Reunion\Entities\EventosModel;

class InformeController extends Controller
{
    protected $cronograma;

    public function __construct(CronogramaEspacioModel $cronograma)
    {
        $this->cronograma = $cronograma;
    }

    public function index(TerceroModel $tercero)
    {
        $cliente = $tercero->getTerceroAcoordingTypeAndState('Cli', 'Activo');
        return view('reunion::informeCronogramaForm', compact('cliente'));
    }

    public function create()
    {
        return view('reunion::create');
    }

    public function store(Request $request, TerceroModel $tercero)
    {

        $request->validate([
            'cliente' => 'required',
        ]);

        $data = $this->cronograma
            ->client($request->get('cliente'))
            ->startDate($request->get('fechaInicio'))
            ->with('cliente')
            ->orderBy('dtFechaCronogramaEspacios', 'ASC')
            ->get();

        if (!count($data) > 0) {
            return response(['No hay datos para el usuario seleccionado'], 404);
        } else {
            return response()->json($data);
        }
    }

    public function show(Request $request)
    {
        $data = json_decode($request->get('list'));
        $tipo = $request->get('tipo');
        $client = $data[0]->cliente;
        foreach ($data as $key => $val) {
            unset($val->cliente);
        }

        if ($tipo == 'html') {
            return view('reunion::informeCronograma', compact('data', 'client'));
        } else {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView('reunion::informeCronograma', ['data' => $data, 'client' => $client]);
            return $pdf->stream();
        }
    }
}
