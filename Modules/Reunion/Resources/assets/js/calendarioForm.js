document.addEventListener('DOMContentLoaded', function () {
    console.log('1')
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'es',
        height: 700,
        contentHeight: 700,
        headerToolbar: {
            left: 'dayGridMonth,timeGridWeek,timeGridDay',
            center: 'title',
            right: 'today,prevYear,prev,next,nextYear'
        },
        navLinks: true,
        editable: true,
        selectable: true,
        nowIndicator: true,
        eventSources: [
            {
                events: function (info, successCallback, failureCallback) {
                    $.ajax({
                        url: '/reuniones/calendario',
                        dataType: 'json',
                        data: {
                            empleado_id: $('#empleado').val()
                        },
                        success: function (doc) {
                            var events = [];
                            if (!!doc) {
                                $.map(doc, function (r) {
                                    let dateStart = moment(r.start + " " + r.time);
                                    events.push({
                                        id: r.id,
                                        title: r.title,
                                        start: dateStart.format(),
                                        end: r.start,
                                        url: r.url
                                    });
                                });
                            }
                            successCallback(events);
                        }
                    });
                }
            }
        ],
    });
    calendar.render();
    console.log('2')
    $('#empleado').on('change', function () {
        calendar.refetchEvents();
    });
});
