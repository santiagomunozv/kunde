"use strict";

var errorClass = "is-invalid";

var configuracionBitacoraTareaPendiente = [];
var configuracionBitacoraTareaAsistente = [];
var configuracionActividadPosEncuentro = [];
var check = false;

//método encargado de validar o no la bítacora
$(document).on("change", ".custom-control", function (e) {
  check = e.target.checked;

  check
    ? $("#shAlert").css("display", "block")
    : $("#shAlert").css("display", "none");
  check
    ? $("#secctionMulti").css("display", "block")
    : $("#secctionMulti").css("display", "none");

  $("#txActividadReunionBitacora").prop("readonly", !check);
  $("#dtFechaReunionBitacora").prop("readonly", !check);
  $("#hrHoraInicioReunionBitacora").prop("readonly", !check);
  $("#hrHoraFinalReunionBitacora").prop("readonly", !check);
  $("#txLinkReunionBitacora").prop("readonly", !check);

  !check ? $("#divTextarea").hide() : $("#divTextarea").show()

  return check;
});

function cargarEditor() {
  var editor = CKEDITOR.replace('txObservacionReunionBitacora');

  editor.on('change', function (evt) {
    document.getElementById('txObservacionReunionBitacora').value = evt.editor.getData()
  })
}

function generarDropzone() {
  // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
  let token = $('meta[name="csrf-token"]').attr("content");
  var myDropzone = new Dropzone("div#dropzoneImage", {
    headers: { "X-CSRF-TOKEN": token },
    url: "/reuniones/tareasporestado/adjuntos/",
    addRemoveLinks: true,
    maxFiles: 5,
    acceptedFiles: "image/*",
    init: function () {
      this.on("success", function (file) {
        let pathImage = JSON.parse(file.xhr.response);
        console.log(pathImage.tmpPath);
        $("#imagenes").append(
          "<input type='hidden' name='imagenAdjunto[]' value='" +
          pathImage.tmpPath +
          "'>"
        );
      });
      this.on("removedfile", function (file) {
        let pathImage = JSON.parse(file.xhr.response);
        $(
          'input[name="imagenAdjunto[]"][value="' + pathImage.tmpPath + '"]'
        ).remove();
      });
    },
  });
}

function generarMultiregistroAsistente(objeto) {
  var asistentes = objeto;

  configuracionBitacoraTareaAsistente = new GeneradorMultiRegistro(
    "configuracionBitacoraTareaAsistente",
    "contenedorBitacoraTareaAsistente",
    "configuracionBitacoraTareaAsistente"
  );

  configuracionBitacoraTareaAsistente.campoid = "oidBitacorAsistente";
  configuracionBitacoraTareaAsistente.campoEliminacion = "eliminarResponsable";
  configuracionBitacoraTareaAsistente.botonEliminacion = true;
  configuracionBitacoraTareaAsistente.funcionEliminacion = "";
  configuracionBitacoraTareaAsistente.campos = [
    "oidBitacorAsistente",
    "txNombreBitacoraAsistente",
    "txCorreoBitacoraAsistente",
    "shTalleristaAsistente",
  ];
  configuracionBitacoraTareaAsistente.etiqueta = [
    "input",
    "input",
    "input",
    "checkbox",
  ];
  configuracionBitacoraTareaAsistente.tipo = ["hidden", "text", "email", ""];
  configuracionBitacoraTareaAsistente.estilo = ["", "", "", ""];
  configuracionBitacoraTareaAsistente.clase = ["", "", "", ""];
  configuracionBitacoraTareaAsistente.sololectura = [true, false, false, false];
  configuracionBitacoraTareaAsistente.opciones = ["", "", "", ""];
  configuracionBitacoraTareaAsistente.funciones = ["", "", "", ""];
  configuracionBitacoraTareaAsistente.otrosAtributos = ["", "", "", ""];

  asistentes.forEach((dato) => {
    configuracionBitacoraTareaAsistente.agregarCampos(dato, "L");
  });
}

function generarMultiregistroPendiente(modelo) {
  var pendientes = modelo;

  configuracionBitacoraTareaPendiente = new GeneradorMultiRegistro(
    "configuracionBitacoraTareaPendiente",
    "contenedorBitacoraTareaPendiente",
    "configuracionBitacoraTareaPendiente"
  );

  let options = [JSON.parse(idResponsable), JSON.parse(nombreResponsable)];

  configuracionBitacoraTareaPendiente.campoid = "oidTarea";
  configuracionBitacoraTareaPendiente.campoEliminacion = "eliminarPendiente";
  configuracionBitacoraTareaPendiente.botonEliminacion = true;
  configuracionBitacoraTareaPendiente.funcionEliminacion = "";
  configuracionBitacoraTareaPendiente.campos = [
    "oidTarea",
    "Tercero_oidResponsable",
    "txAsuntoTarea",
    "dtFechaVencimientoTarea",
  ];
  configuracionBitacoraTareaPendiente.etiqueta = [
    "input",
    "select",
    "input",
    "input",
  ];
  configuracionBitacoraTareaPendiente.tipo = ["hidden", "", "text", "date"];
  configuracionBitacoraTareaPendiente.estilo = ["", "", "", ""];
  configuracionBitacoraTareaPendiente.clase = ["", "chosen-select", "", ""];
  configuracionBitacoraTareaPendiente.sololectura = [true, false, false, false];
  configuracionBitacoraTareaPendiente.opciones = ["", options, "", ""];
  configuracionBitacoraTareaPendiente.funciones = ["", "", "", ""];
  configuracionBitacoraTareaPendiente.otrosAtributos = ["", "", "", ""];

  pendientes.forEach((dato) => {
    configuracionBitacoraTareaPendiente.agregarCampos(dato, "L");
  });

  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }
}

function generarMultiregistroPosEncuentro(modelo) {
  var pendientes = modelo;
  let options = [JSON.parse(idResponsable), JSON.parse(nombreResponsable)];

  configuracionActividadPosEncuentro = new GeneradorMultiRegistro(
    "configuracionActividadPosEncuentro",
    "contenedorActividadPosEncuentro",
    "configuracionActividadPosEncuentro"
  );


  configuracionActividadPosEncuentro.campoid = "oidReunionEventoActividad";
  configuracionActividadPosEncuentro.botonEliminacion = false;
  configuracionActividadPosEncuentro.campos = [
    "oidReunionEventoActividad",
    "txDescripcionEventoActividad",
    "chCumplidaEventoActividad",
    "EventoActividad_oidEventoActividad",
    "Tercero_oidResponsableActividad",
    "txAsuntoActividad",
    "dtFechaVencimientoActividad",
  ];
  configuracionActividadPosEncuentro.etiqueta = [
    "input",
    "input",
    "checkbox",
    "input",
    "select",
    "input",
    "input",
  ];
  configuracionActividadPosEncuentro.tipo = ["hidden", "text", "checkbox", "hidden", "", "text", "date"];
  configuracionActividadPosEncuentro.estilo = ["", "", "", "", "", "", ""];
  configuracionActividadPosEncuentro.clase = ["", "", "", "", "", "", ""];
  configuracionActividadPosEncuentro.sololectura = [true, true, false, false, false, false, false];
  configuracionActividadPosEncuentro.opciones = ["", "", "", "", options, "", ""];
  configuracionActividadPosEncuentro.funciones = ["", "", "", "", "", "", ""];
  configuracionActividadPosEncuentro.otrosAtributos = ["", "", "", "", "", "", ""];

  pendientes.forEach((dato) => {
    configuracionActividadPosEncuentro.agregarCampos(dato, "L");
  });
}

//método encargado de mostrar errores en el mismo modal
function mostrarErrores(errores) {
  let ul = document.createElement("ul");
  errores.forEach(function (error) {
    let li = document.createElement("li");
    li.innerHTML = error;
    ul.appendChild(li);
  });
  let div = document.createElement("div");
  div.appendChild(ul);
  div.className = "alert alert-warning";
  document.getElementById("errores").appendChild(div);
}

//método encargado de guardar el modal de tareas
function grabarModalTarea() {
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-modalTarea";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    // console.log(route);
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });

        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }
}

// método encargado de mostar el modal de tareas
function ModalTarea(id) {
  modal.cargando();
  $.get("/reuniones/tareasporestado/modal/" + id, function (resp) {
    modal
      .mostrarModal("Editar tarea", resp, function () {
        modal.cargando();
      })
      .extraGrande()
      .sinPie();
    generarDropzone();
    cargarEditor();
    configurarGrid("trazabilidad-table");
    cargarMultiregistros(id);
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

//método encargado de validar el formulario
function validarForm() {
  let mensajes = [];

  // console.log(check);
  if (check) {
    let txActividadReunionBitacora_Input = $("#txActividadReunionBitacora");
    let txActividadReunionBitacora_AE = txActividadReunionBitacora_Input.val();
    txActividadReunionBitacora_Input.removeClass(errorClass);
    if (
      !txActividadReunionBitacora_AE &&
      txActividadReunionBitacora_Input.length > 0
    ) {
      txActividadReunionBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Actividad es obligatorio");
    }

    let dtFechaReunionBitacora_Input = $("#dtFechaReunionBitacora");
    let dtFechaReunionBitacora_AE = dtFechaReunionBitacora_Input.val();
    dtFechaReunionBitacora_Input.removeClass(errorClass);
    if (!dtFechaReunionBitacora_AE && dtFechaReunionBitacora_Input.length > 0) {
      dtFechaReunionBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Fecha es obligatorio");
    }

    let hrHoraInicioReunionBitacora_Input = $("#hrHoraInicioReunionBitacora");
    let hrHoraInicioReunionBitacora_AE =
      hrHoraInicioReunionBitacora_Input.val();
    hrHoraInicioReunionBitacora_Input.removeClass(errorClass);
    if (
      !hrHoraInicioReunionBitacora_AE &&
      hrHoraInicioReunionBitacora_Input.length > 0
    ) {
      hrHoraInicioReunionBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Hora inicio es obligatorio");
    }

    let hrHoraFinalReunionBitacora_Input = $("#hrHoraFinalReunionBitacora");
    let hrHoraFinalReunionBitacora_AE = hrHoraFinalReunionBitacora_Input.val();
    hrHoraFinalReunionBitacora_Input.removeClass(errorClass);
    if (
      !hrHoraFinalReunionBitacora_AE &&
      hrHoraInicioReunionBitacora_Input.length > 0
    ) {
      hrHoraFinalReunionBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Hora final es obligatorio");
    }

    let txObservacionReunionBitacora_Input = $("#txObservacionReunionBitacora");
    let txObservacionReunionBitacora_AE =
      txObservacionReunionBitacora_Input.val();
    txObservacionReunionBitacora_Input.removeClass(errorClass);
    if (
      !txObservacionReunionBitacora_AE &&
      txObservacionReunionBitacora_Input.length > 0
    ) {
      txObservacionReunionBitacora_Input.addClass(errorClass);
      mensajes.push("El campo Observaciones es obligatorio");
    }
  }

  //validar multiregistro Asistente
  let nombresAsistentes = $('input[name="txNombreBitacoraAsistente[]"]');

  for (let i = 0; i < nombresAsistentes.length; i++) {
    let nombre = nombresAsistentes[i];
    //name
    if (nombre && !nombre.value) {
      nombre.classList.add(errorClass);
      mensajes.push(
        "El Nombre del Asistente en el registro:" + (i + 1) + " es requerido"
      );
    }
  }

  //validar multiregistro Pendiente
  let responsablesPendiente = $('select[name="Tercero_oidResponsable[]"]');
  let tareasPendiente = $('input[name="txAsuntoTarea[]"]');
  let fechasVencimientos = $('input[name="dtFechaVencimientoTarea[]"]');

  for (let j = 0; j < responsablesPendiente.length; j++) {
    let responsable = responsablesPendiente[j];
    let tarea = tareasPendiente[j];
    let fechasBitacora = fechasVencimientos[j];
    //responsable
    if (responsable.value == "" || responsable.value == 0) {
      responsable.classList.add(errorClass);
      mensajes.push(
        "Seleccionar un responsable en el registro: " +
        (j + 1) +
        " es requerido"
      );
    }
    //tarea
    if (tarea && !tarea.value) {
      tarea.classList.add(errorClass);
      mensajes.push(
        "El Nombre de la Tarea Pendiente en el registro:" +
        (j + 1) +
        " es requerido"
      );
    }
    //fecha
    if (fechasBitacora && !fechasBitacora.value) {
      fechasBitacora.classList.add(errorClass);
      mensajes.push(
        "La fecha de vencimiento en Pendientes en el registro: " +
        (j + 1) +
        " es requerido"
      );
    }
  }
  return mensajes;
}

//método encargado de inicializar los multiregistros
function cargarMultiregistros(id) {
  var token = document.getElementById("token").value;
  $.ajax({
    headers: { "X-CSRF-TOKEN": token },
    type: "GET",
    url: "/reuniones/tareasporestado/modal/obtenerjson/" + id,
    success: function (data) {
      var objeto = data;
      generarMultiregistroAsistente(objeto[0]);
      generarMultiregistroPendiente(objeto[1]);
      generarMultiregistroPosEncuentro(objeto[2]);
      // return data;
    },
  });
}

//método encargado de mostrar modal de crear tarea
function abrirModalCrearTarea() {
  modal.cargando();
  $.get("/reuniones/tareasporestado/create", function (resp) {
    modal
      .mostrarModal("Espacio Kunde", resp, function () {
        modal.cargando();
      })
      .extraGrande()
      .sinPie();
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

//método encargado de crear la tarea
function crearTarea() {
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarCrearTarea();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-crearTarea";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        console.log(resp);
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {

      if (resp.responseJSON.message !== undefined) {
        mensajes.push(resp.responseJSON.message);
      }
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }
}

//método encargado de guardar modal de crear tarea
function validarCrearTarea() {
  let mensajes = [];
  let txAsuntoReunion_Input = $("#txAsuntoReunion");
  let txAsuntoReunion_AE = txAsuntoReunion_Input.val();
  txAsuntoReunion_Input.removeClass(errorClass);
  if (!txAsuntoReunion_AE) {
    txAsuntoReunion_Input.addClass(errorClass);
    mensajes.push("El campo Asunto es obligatorio");
  }
  let txTipoReunion_Input = $("#txTipoReunion");
  let txTipoReunion_AE = txTipoReunion_Input.val();
  txTipoReunion_Input.removeClass(errorClass);
  if (!txTipoReunion_AE) {
    txTipoReunion_Input.addClass(errorClass);
    mensajes.push("El campo Tipo de reunión es obligatorio");
  }
  let dtFechaReunion_Input = $("#dtFechaReunion");
  let dtFechaReunion_AE = dtFechaReunion_Input.val();
  dtFechaReunion_Input.removeClass(errorClass);
  if (!dtFechaReunion_AE) {
    dtFechaReunion_Input.addClass(errorClass);
    mensajes.push("El campo Fecha es obligatorio");
  }
  let hrHoraReunion_Input = $("#hrHoraReunion");
  let hrHoraReunion_AE = hrHoraReunion_Input.val();
  hrHoraReunion_Input.removeClass(errorClass);
  if (!hrHoraReunion_AE) {
    hrHoraReunion_Input.addClass(errorClass);
    mensajes.push("El campo Hora es obligatorio");
  }
  let txUbicacionReunion_Input = $("#txUbicacionReunion");
  let txUbicacionReunion_AE = txUbicacionReunion_Input.val();
  txUbicacionReunion_Input.removeClass(errorClass);
  if (!txUbicacionReunion_AE) {
    txUbicacionReunion_Input.addClass(errorClass);
    mensajes.push("El campo Ubicación es obligatorio");
  }
  let Tercero_oidResponsable_Input = $("#Tercero_oidResponsable");
  let Tercero_oidResponsable_AE = Tercero_oidResponsable_Input.val();
  Tercero_oidResponsable_Input.removeClass(errorClass);
  if (!Tercero_oidResponsable_AE) {
    Tercero_oidResponsable_Input.addClass(errorClass);
    mensajes.push("El campo Responsable es obligatorio");
  }
  let Evento_oidEvento_Input = $("#Evento_oidEvento");
  let Evento_oidEvento_AE = Evento_oidEvento_Input.val();
  Evento_oidEvento_Input.removeClass(errorClass);
  if (!Evento_oidEvento_AE) {
    Evento_oidEvento_Input.addClass(errorClass);
    mensajes.push("El campo Eventos es obligatorio");
  }
  let intDuracionReunion_Input = $("#intDuracionReunion");
  let intDuracionReunion_AE = intDuracionReunion_Input.val();
  intDuracionReunion_Input.removeClass(errorClass);
  if (!intDuracionReunion_AE) {
    intDuracionReunion_Input.addClass(errorClass);
    mensajes.push("El campo Duración de la reunión es obligatorio");
  }
  return mensajes;
}

//método encargado de mostrar modal de reagendar
function modalRegendar(id) {
  modal.cargando();
  $.get("/reuniones/tareasporestado/modalreagendar/" + id, function (resp) {
    modal
      .mostrarModal("Re agendar tarea", resp, function () {
        modal.cargando();
      })
      .extraGrande()
      .sinPie();
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

//método encargado de grabar el reagendamiento
function grabarModalReagendar() {
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-modalReagendar";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    console.log(route);
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }
}

//método encargado de mostrar modal de filtración
function modalFiltrosTareas() {
  modal.cargando();
  $.get("/reuniones/tareasporestado/filtrostareas", function (resp) {
    modal
      .mostrarModal("Filtrar tareas", resp, function () {
        modal.cargando();
      })
      .grande()
      .sinPie();
  }).fail(function (resp) {
    modal.cerrarModal();
  });
}

//método encargado de enviar valroes a filtrar
function filtrar() {
  let cliente = $("#FilterTerceroCli").val();
  let empleado = $("#FilterTerceroEmp").val();
  let fechaInico = $("#FilterDtFechaInicio").val();
  let fechaFinal = $("#FilterDtFechaFinal").val();

  location.href =
    "http://" +
    location.host +
    "/reuniones/tareasporestado?cli=" +
    cliente +
    "&emp=" +
    empleado +
    "&fei=" +
    fechaInico +
    "&fef=" +
    fechaFinal;
}

//método encargado de limpiar filtros
function limpiarFiltros() {
  location.href = "http://" + location.host + "/reuniones/tareasporestado";
}

//metodo encargado de liminar tareas
function eliminarTarea(id) {
  let token = $('meta[name="csrf-token"]').attr("content");
  modal.cargando();
  modal.mostrarModal(
    "Eliminar",
    "Esta Seguro de eliminar esta tarea?",
    function () {
      $.ajax({
        headers: { "X-CSRF-TOKEN": token },
        dataType: "json",
        url: "/reuniones/tareasporestado/eliminartarea/" + id,
        data: { id: id },
        type: "POST",
        success: function (resp) {
          modal.mostrarModal(
            "Eliminación exitosa",
            "Se ha eliminado correctamente la tareas ",
            function () {
              location.reload(true);
            }
          );
        },
        error: function (err) {
          modal.mostrarModal("Error al eliminar", err.responseJSON.tabla);
        },
      });

      modal.cerrarModal();
    }
  );
}

//método encargado de mostrar modal de Email
function modalEmail(id) {
  modal.cargando();
  $.get("/reuniones/tareasporestado/email/" + id, function (resp) {
    modal
      .mostrarModal("Enviar Email", resp, function () {
        modal.cargando();
      })
      .sinPie();
  }).fail(function (resp) {
    modal.mostrarModal(
      "OOOOOPPPSSSS ocurrió un problema",
      "status:" + resp.status
    );
  });
}

function enviarEmail() {
  $("#errores").empty();
  $("." + errorClass).removeClass(errorClass);
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-email";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">El correo sido enviado correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      mostrarErrores(mensajes);
    });
  }
}

//método encargado de eliminar imagenes
function eliminarImagen(id) {
  $("#eliminarImagen").val($("#eliminarImagen").val() + id + ",");
  $("#imagen_" + id).remove();
}

// método encargado de auto llenar el campo de asunto
function getEvento(id) {
  $.get("/reuniones/tareasporestado/getevent/" + id, function (data) {
    $('#txAsuntoReunion').val(data.txNombreEvento)
  }).fail(function (resp) {
    return 'erroe'
  });
}

function descargarBitacora(id) {
  modal.cargando();
  $.ajax({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
    type: 'POST',
    url: "/reuniones/tareasporestado/descargar",
    data: { id: id },
    success: function (resp) {
      modal.establecerAccionCerrar(function () {
        downloadFile(resp);
      });
      modal.mostrarModal(
        "Informacion",
        '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
      );
    }
  }).fail(function (resp) {
    modal.mostrarErrores([resp.responseJSON.message]);
  });
}

function downloadFile(response) {
  window.open(
    "http://" + location.host + "/modules/plantrabajo/" + response.archivo,
    "_blank",
    "width=2500px, height=700px, scrollbars=yes"
  );
}


