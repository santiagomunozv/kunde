$(window).on("load", function () {
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }
});

$('#informerForm').on('submit', function (e) {
    e.preventDefault();
    modal.cargando();
    let mensajes = [];
    var data = $(this).serialize()

    $.ajax({
        type: 'post',
        url: '/reuniones/informe',
        data: data,
        dataType: 'json',
        success: function (res) {
            modal.establecerAccionCerrar(function () {
                var data = JSON.stringify(res)
                console.log(data);
                window.open("/reuniones/informe/filtro?tipo=html&list=" + data);
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido cargados correctamente</div>");
        },
        error: function (res) {
            var m = res.responseJSON
            $.each(res.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass("is-invalid");
            });
            console.log(m);
            m.length > 0 ? modal.mostrarErrores([m]) :
                modal.mostrarErrores(mensajes)
        }
    })
});

function generarPDF() {
    modal.cargando();
    let mensajes = [];
    let formularioId = "#informerForm";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(route, data, function (res) {
        modal.establecerAccionCerrar(function () {
            var data = JSON.stringify(res)
            console.log(data);
            window.open("/reuniones/informe/filtro?tipo=pdf&list=" + data);
        });
        modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido cargados correctamente</div>");
    }, "json").fail(function (res) {
        var m = res.responseJSON
        $.each(res.responseJSON.errors, function (index, value) {
            mensajes.push(value);
            $("#" + index).addClass("is-invalid");
        });
        console.log(m);
        m.length > 0 ? modal.mostrarErrores([m]) :
            modal.mostrarErrores(mensajes)
    });
}
