"use strict";
var errorClass = "is-invalid";
var configuracionReunionAsistentes = JSON.parse(reunionAsistente);
var configuracionReunionAsistente = [];


$(function(){
    //generamos la multiregistro de grupo
    configuracionReunionAsistente = new GeneradorMultiRegistro('configuracionReunionAsistente','contenedorReunionAsistente','configuracionReunionAsistente');

    configuracionReunionAsistente.campoid = 'oidReunionAsistente';
    configuracionReunionAsistente.campoEliminacion = 'eliminarReunionAsistente';
    configuracionReunionAsistente.botonEliminacion = true;
    configuracionReunionAsistente.funcionEliminacion = '';  
    configuracionReunionAsistente.campos = ['oidReunionAsistente','txNombreReunionAsistente','txCorreoAsistente'];
    configuracionReunionAsistente.etiqueta = ['input','input', 'input'];
    configuracionReunionAsistente.tipo = ['hidden','text','text'];
    configuracionReunionAsistente.estilo = ['','',''];
    configuracionReunionAsistente.clase = ['','',''];
    configuracionReunionAsistente.sololectura = [true,false,false];
    configuracionReunionAsistente.opciones = ['','',''];
    configuracionReunionAsistente.funciones = ['','',''];
    configuracionReunionAsistente.otrosAtributos = ['','',''];


    configuracionReunionAsistentes.forEach( dato => {configuracionReunionAsistente.agregarCampos(dato , 'L');
    });

    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})

function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-reunion";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            console.log(resp)
            modal.establecerAccionCerrar(function(){
                location.href = "/reuniones/reunion/"+resp.oidReunion+"/edit";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let txAsuntoReunion_Input = $("#txAsuntoReunion");
        let txAsuntoReunion_AE = txAsuntoReunion_Input.val();
        txAsuntoReunion_Input.removeClass(errorClass);
        if(!txAsuntoReunion_AE){
            txAsuntoReunion_Input.addClass(errorClass)
            mensajes.push("El campo Asunto es obligatorio");
        }
		let txTipoReunion_Input = $("#txTipoReunion");
        let txTipoReunion_AE = txTipoReunion_Input.val();
        txTipoReunion_Input.removeClass(errorClass);
        if(!txTipoReunion_AE){
            txTipoReunion_Input.addClass(errorClass)
            mensajes.push("El campo Tipo de reunión es obligatorio");
        }
		let dtFechaReunion_Input = $("#dtFechaReunion");
        let dtFechaReunion_AE = dtFechaReunion_Input.val();
        dtFechaReunion_Input.removeClass(errorClass);
        if(!dtFechaReunion_AE){
            dtFechaReunion_Input.addClass(errorClass)
            mensajes.push("El campo Fecha es obligatorio");
        }
        let hrHoraReunion_Input = $("#hrHoraReunion");
        let hrHoraReunion_AE = hrHoraReunion_Input.val();
        hrHoraReunion_Input.removeClass(errorClass);
        if(!hrHoraReunion_AE){
            hrHoraReunion_Input.addClass(errorClass)
            mensajes.push("El campo Hora es obligatorio");
        }
		let txUbicacionReunion_Input = $("#txUbicacionReunion");
        let txUbicacionReunion_AE = txUbicacionReunion_Input.val();
        txUbicacionReunion_Input.removeClass(errorClass);
        if(!txUbicacionReunion_AE){
            txUbicacionReunion_Input.addClass(errorClass)
            mensajes.push("El campo Ubicación es obligatorio");
        }
		let Tercero_oidResponsable_Input = $("#Tercero_oidResponsable");
        let Tercero_oidResponsable_AE = Tercero_oidResponsable_Input.val();
        Tercero_oidResponsable_Input.removeClass(errorClass);
        if(!Tercero_oidResponsable_AE){
            Tercero_oidResponsable_Input.addClass(errorClass)
            mensajes.push("El campo Responsable es obligatorio");
        }
        let Evento_oidEvento_Input = $("#Evento_oidEvento");
        let Evento_oidEvento_AE = Evento_oidEvento_Input.val();
        Evento_oidEvento_Input.removeClass(errorClass);
        if(!Evento_oidEvento_AE){
            Evento_oidEvento_Input.addClass(errorClass)
            mensajes.push("El campo Eventos es obligatorio");
        }
        let intDuracionReunion_Input = $("#intDuracionReunion");
        let intDuracionReunion_AE = intDuracionReunion_Input.val();
        intDuracionReunion_Input.removeClass(errorClass);
        if(!intDuracionReunion_AE){
            intDuracionReunion_Input.addClass(errorClass)
            mensajes.push("El campo Duración de la reunión es obligatorio");
        }
        return mensajes;
        
    }
}