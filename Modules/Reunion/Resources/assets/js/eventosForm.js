"use strict";

var errorClass = "is-invalid"
let eventoActividad = []
let eventoAsistente = []
let eventoActividades = JSON.parse(reunionEventos);
let eventoAsistentes = JSON.parse(reunionEventosAsistentes);

$(function () {

    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    var myDropzone = new Dropzone("div#dropzoneImage", {
        headers: { 'X-CSRF-TOKEN': token },
        url: "/reuniones/subirEvento",
        addRemoveLinks: true,
        acceptedFiles: "image/*, video/*",
        maxFilesize: 500,
        init: function () {
            this.on("success", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $("#imagenes").append("<input type='hidden' name='publicacionImagen[]' value='" + pathImage.tmpPath + "'>");
            });
            this.on("removedfile", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $('input[name="publicacionImagen[]"][value="' + pathImage.tmpPath + '"]').remove();
            });
        }
    });

    function eliminarImagen(id) {
        $("#eliminarImagen").val($("#eliminarImagen").val() + id + ",");
        $("#imagen_" + id).remove();
    }

    eventoActividad = new GeneradorMultiRegistro('eventoActividad', 'eventoActividadContent', 'eventoActividad_');

    eventoActividad.campoid = 'oidEventoActividad';
    eventoActividad.campoEliminacion = 'eliminarEventoActividad';
    eventoActividad.botonEliminacion = true;
    eventoActividad.funcionEliminacion = '';
    eventoActividad.campos = ['oidEventoActividad', 'txDescripcionEventoActividad'];
    eventoActividad.etiqueta = ['input', 'input'];
    eventoActividad.tipo = ['hidden', 'text'];
    eventoActividad.estilo = ['', ''];
    eventoActividad.clase = ['', ''];
    eventoActividad.sololectura = [true, false];
    eventoActividad.opciones = ['', ''];
    eventoActividad.funciones = ['', ''];
    eventoActividad.otrosAtributos = ['', ''];


    eventoActividades.forEach(dato => {
        eventoActividad.agregarCampos(dato, 'L');
    });

    eventoAsistente = new GeneradorMultiRegistro('eventoAsistente', 'eventoAsistenteContent', 'eventoAsistente_');

    const asistentesOpcion = [JSON.parse(asistentes), JSON.parse(asistentes)];

    eventoAsistente.campoid = 'oidEventoAsistente';
    eventoAsistente.campoEliminacion = 'eliminarEventoAsistente';
    eventoAsistente.botonEliminacion = true;
    eventoAsistente.funcionEliminacion = '';
    eventoAsistente.campos = ['oidEventoAsistente', 'txAsistenteEventoAsistente'];
    eventoAsistente.etiqueta = ['input', 'select'];
    eventoAsistente.tipo = ['hidden', ''];
    eventoAsistente.estilo = ['', ''];
    eventoAsistente.clase = ['', 'chosen-select'];
    eventoAsistente.sololectura = [true, false];
    eventoAsistente.opciones = ['', asistentesOpcion];
    eventoAsistente.funciones = ['', ''];
    eventoAsistente.otrosAtributos = ['', ''];

    eventoAsistentes.forEach(dato => {
        eventoAsistente.agregarCampos(dato, 'L');
    });


    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }

    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }

    createMultiRecord()

})

function grabar() {
    modal.cargando();
    let mensajes = validarForm();
    if (mensajes && mensajes.length) {
        modal.mostrarErrores(mensajes);
    } else {
        let formularioId = "#form-eventos";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route, data, function (resp) {
            modal.establecerAccionCerrar(function () {
                location.href = "/reuniones/evento";
            });
            modal.mostrarModal("Informacion", "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        }, "json").fail(function (resp) {
            $.each(resp.responseJSON.errors, function (index, value) {
                mensajes.push(value);
                $("#" + index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm() {

        let mensajes = [];
        let txAnioEvento_Input = $("#txAnioEvento");
        let txAnioEvento_AE = txAnioEvento_Input.val();
        txAnioEvento_Input.removeClass(errorClass);
        if (!txAnioEvento_AE) {
            txAnioEvento_Input.addClass(errorClass)
            mensajes.push("El campo Año es obligatorio");
        }

        let txCodigoEvento_Input = $("#txCodigoEvento");
        let txCodigoEvento_AE = txCodigoEvento_Input.val();
        txCodigoEvento_Input.removeClass(errorClass);
        if (!txCodigoEvento_AE) {
            txCodigoEvento_Input.addClass(errorClass)
            mensajes.push("El campo Código es obligatorio");
        }
        let txNombreEvento_Input = $("#txNombreEvento");
        let txNombreEvento_AE = txNombreEvento_Input.val();
        txNombreEvento_Input.removeClass(errorClass);
        if (!txNombreEvento_AE) {
            txNombreEvento_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
        let txEstadoEvento_Input = $("#txEstadoEvento");
        let txEstadoEvento_AE = txEstadoEvento_Input.val();
        txEstadoEvento_Input.removeClass(errorClass);
        if (!txEstadoEvento_AE) {
            txEstadoEvento_Input.addClass(errorClass)
            mensajes.push("El campo Estado es obligatorio");
        }
        return mensajes;
    }
}


function generarDropzone() {
    // ----------GENERAMOS DROPZONE Y ENVIAMOS IMAGENES------------------
    let token = $('meta[name="csrf-token"]').attr('content');
    var myDropzone = new Dropzone("div#dropzoneImage", {
        headers: { 'X-CSRF-TOKEN': token },
        url: "/reuniones/subirevento",
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: "image/*",
        init: function () {
            this.on("success", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $("#imagenes").append("<input type='hidden' name='imagenEvento' value='" + pathImage.tmpPath + "'>");
            });
            this.on("removedfile", function (file) {
                let pathImage = JSON.parse(file.xhr.response);
                $('input[name="imagenEvento"][value="' + pathImage.tmpPath + '"]').remove();
            });
        }
    });
}

function createMultiRecord() {
    // //generamos la multiregistro de grupo
    // configuracionReunionAsistente = new GeneradorMultiRegistro('configuracionReunionAsistente','contenedorReunionAsistente','configuracionReunionAsistente');

    // configuracionReunionAsistente.campoid = 'oidReunionAsistente';
    // configuracionReunionAsistente.campoEliminacion = 'eliminarReunionAsistente';
    // configuracionReunionAsistente.botonEliminacion = true;
    // configuracionReunionAsistente.funcionEliminacion = '';
    // configuracionReunionAsistente.campos = ['oidReunionAsistente','txNombreReunionAsistente','txCorreoAsistente'];
    // configuracionReunionAsistente.etiqueta = ['input','input', 'input'];
    // configuracionReunionAsistente.tipo = ['hidden','text','text'];
    // configuracionReunionAsistente.estilo = ['','',''];
    // configuracionReunionAsistente.clase = ['','',''];
    // configuracionReunionAsistente.sololectura = [true,false,false];
    // configuracionReunionAsistente.opciones = ['','',''];
    // configuracionReunionAsistente.funciones = ['','',''];
    // configuracionReunionAsistente.otrosAtributos = ['','',''];


    // configuracionReunionAsistentes.forEach( dato => {configuracionReunionAsistente.agregarCampos(dato , 'L');
    // });

    // var config = {
    //     ".chosen-select": {},
    //     ".chosen-select-deselect": { allow_single_deselect: true },
    //     ".chosen-select-no-single": { disable_search_threshold: 10 },
    //     ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    //     ".chosen-select-width": { width: "100%" }
    // }

    // for (let selector in config) {
    //     $(selector).chosen(config[selector]);
    // }


}