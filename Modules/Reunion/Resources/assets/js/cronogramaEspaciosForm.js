"use strict";
var errorClass = "is-invalid";
$(function () {

  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" }
  }

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }

  var date = new Date();
  var yyyy = date.getFullYear().toString();
  var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
  var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate()).toString() : (date.getDate()).toString();

  //establecemos los valores del calendario
  var options = {
    events_source: 'http://' + location.host + '/reuniones/obtenercronogramaespacios',
    view: 'week',
    language: 'es-ES',
    tmpl_path: 'http://' + location.host + '/bootstrap-calendar/tmpls/',
    tmpl_cache: false,
    day: yyyy + "-" + mm + "-" + dd,
    time_start: '6:00',
    time_end: '18:00',
    time_split: '30',
    width: '100%',
    onAfterEventsLoad: function (events) {
      if (!events) {
        return;
      }
      var list = $('#eventlist');
      list.html('');

      $.each(events, function (key, val) {
        $(document.createElement('li'))
          .html('<a target="_blank" href="' + val.url + '">' + val.title + '</a>')
          .appendTo(list);
      });
    },
    onAfterViewLoad: function (view) {
      $('.page-header h3').text(this.getTitle());
      $('.btn-group button').removeClass('active');
      $('button[data-calendar-view="' + view + '"]').addClass('active');
    },
    classes: {
      months: {
        general: 'label'
      }
    }
  };

  var calendar = $('#calendar').calendar(options);

  $('.btn-group button[data-calendar-nav]').each(function () {
    var $this = $(this);
    $this.click(function () {
      calendar.navigate($this.data('calendar-nav'));
    });
  });

  $('.btn-group button[data-calendar-view]').each(function () {
    var $this = $(this);
    $this.click(function () {
      calendar.view($this.data('calendar-view'));
    });
  });

  $('#first_day').change(function () {
    var value = $(this).val();
    value = value.length ? parseInt(value) : null;
    calendar.setOptions({ first_day: value });
    calendar.view();
  });

  $('#events-in-modal').change(function () {
    var val = $(this).is(':checked') ? $(this).val() : null;
    calendar.setOptions(
      {
        modal: val,
        modal_type: 'iframe'
      }
    );
  });
});

function mostrarErrores(errores) {
  let ul = document.createElement('ul');
  errores.forEach(function (error) {
    let li = document.createElement('li');
    li.innerHTML = error;
    ul.appendChild(li);
  });
  let div = document.createElement('div');
  div.appendChild(ul);
  div.className = 'alert alert-warning';
  document.getElementById('errores').appendChild(div);
}

function grabar() {
  $('#errores').empty();
  $('.' + errorClass).removeClass(errorClass);
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    mostrarErrores(mensajes);
  } else {
    // modal.cargando();
    let formularioId = '#form-cronogramaespacios';
    let route = $(formularioId).attr('action');
    let data = $(formularioId).serialize();
    $.post(route, data, function (resp) {
      modal.establecerAccionCerrar(function () {
        modal.cerrarModal();
        location.href = "/reuniones/cronogramaespacios";
      });
      modal.mostrarModal('Informacion', '<div class="alert alert-success">Los datos han sido guardados correctamente</div>');
    }, 'json').fail(function (resp) {
      mensajes.push(resp.responseJSON.message);
      mostrarErrores(mensajes);
    });
  }
}



function validarForm() {

  let mensajes = [];

  let lsModalidadTerceroPeriodicidad_Input = $("#lsModalidadTerceroPeriodicidad");
  let lsModalidadTerceroPeriodicidad_AE = lsModalidadTerceroPeriodicidad_Input.val();
  lsModalidadTerceroPeriodicidad_Input.removeClass(errorClass);
  if (!lsModalidadTerceroPeriodicidad_AE) {
    lsModalidadTerceroPeriodicidad_Input.addClass(errorClass)
    mensajes.push("El campo Tipo de reunión es obligatorio");
  }

  let hrHoraCronogramaEspacios_Input = $("#hrHoraCronogramaEspacios");
  let hrHoraCronogramaEspacios_AE = hrHoraCronogramaEspacios_Input.val();
  hrHoraCronogramaEspacios_Input.removeClass(errorClass);
  if (!hrHoraCronogramaEspacios_AE) {
    hrHoraCronogramaEspacios_Input.addClass(errorClass)
    mensajes.push("El campo Hora es obligatorio");
  }

  let intDuracionCronogramaEspacios_Input = $("#intDuracionCronogramaEspacios");
  let intDuracionCronogramaEspacios_AE = intDuracionCronogramaEspacios_Input.val();
  intDuracionCronogramaEspacios_Input.removeClass(errorClass);
  if (!intDuracionCronogramaEspacios_AE) {
    intDuracionCronogramaEspacios_Input.addClass(errorClass)
    mensajes.push("El campo Duración es obligatorio");
  }

  let txUbicacionCronogramaEspacios_Input = $("#txUbicacionCronogramaEspacios");
  let txUbicacionCronogramaEspacios_AE = txUbicacionCronogramaEspacios_Input.val();
  txUbicacionCronogramaEspacios_Input.removeClass(errorClass);
  if (!txUbicacionCronogramaEspacios_AE) {
    txUbicacionCronogramaEspacios_Input.addClass(errorClass)
    mensajes.push("El campo Ubicación es obligatorio");
  }

  let Tercero_oidResponsable_Input = $("#Tercero_oidResponsable");
  let Tercero_oidResponsable_AE = Tercero_oidResponsable_Input.val();
  Tercero_oidResponsable_Input.removeClass(errorClass);
  if (!Tercero_oidResponsable_AE) {
    Tercero_oidResponsable_Input.addClass(errorClass)
    mensajes.push("El campo Responsable es obligatorio");
  }

  return mensajes;

}