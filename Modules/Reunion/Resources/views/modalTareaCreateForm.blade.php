@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
@endsection

{{-- {!! Form::model($reunion, ['route' => ['reunion.update', $reunion->oidReunion], 'method' => 'PUT', 'id' => 'form-reunion', 'onsubmit' => 'return false;']) !!} --}}
{!! Form::open([
    'url' => ['/reuniones/tareasporestado/'],
    'method' => 'POST',
    'id' => 'form-crearTarea',
    'onsubmit' => 'return false;',
]) !!}

<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

        {!! Form::hidden('oidReunion', null, ['id' => 'oidReunion']) !!}
        <!-- mostramos los errores de insercion -->
        <div id="errores"></div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txAsuntoReunion', 'Asunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txAsuntoReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el asunto',
                        'disabled',
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txTipoReunion', 'Tipo de reunión', ['class' => 'text-md text-primary mb-1  required ']) !!}
                    {!! Form::select(
                        'txTipoReunion',
                        ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                        null,
                        ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione el tipo de reunión'],
                    ) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('dtFechaReunion', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de la reunión',
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('hrHoraReunion', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::time('hrHoraReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la hora de la reunión',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('intDuracionReunion', 'Duración de la reunión', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::select(
                        'intDuracionReunion',
                        [
                            '15' => '15 minutos',
                            '30' => '30 minutos',
                            '45' => '45 minutos',
                            '60' => '1 hora',
                            '75' => '1 hora y 15 minutos',
                            '90' => '1 hora y 30 minutos',
                            '105' => '1 hora y 45 minutos',
                            '120' => '2 horas',
                        ],
                        null,
                        ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión'],
                    ) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txUbicacionReunion', 'Ubicación', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txUbicacionReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Lugar / Link de la reunión',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Evento_oidEvento', 'Eventos', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('Evento_oidEvento', $evento, null, [
                        'class' => 'chosen-select form-control',
                        'style' => 'width:100%',
                        'placeholder' => 'Selecciona el evento',
                        'onchange' => 'getEvento(this.value)',
                    ]) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('Tercero_oidResponsable', $asn_tercerolistaEmpleado, null, [
                        'class' => 'chosen-select form-control',
                        'style' => 'width:100%',
                        'placeholder' => 'Selecciona el Responsable',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Tercero_idCliente', 'Cliente', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::select('Tercero_idCliente', $asn_tercerolistaCliente, null, [
                        'class' => 'chosen-select form-control',
                        'style' => 'width:100%',
                        'placeholder' => 'Selecciona el Cliente',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group ">
                    {!! Form::label('txDescripcionReunion', 'Descripción', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::textArea('txDescripcionReunion', isset($reunion[0]) ? $reunion[0]['txDescripcionReunion'] : null, [
                        'class' => 'form-control',
                        'rows' => 4,
                    ]) !!}
                </div>
            </div>
        </div>
        {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'crearTarea()']) !!}

        {!! Form::close() !!}
    </div>
</div>
