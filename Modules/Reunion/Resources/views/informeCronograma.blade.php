<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }

        #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        p.normal {
          font-style: normal;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #3A64AE;
          color: white;
        }

        .textCenter{
            text-align: center !important;
        }
        </style>
        <link rel="stylesheet" href="css/fontawesome.css">
</head>

<body>
    <table id="customers">
        <tr>
            <th colspan="4" class="textCenter"> Informe conograma de: {{$client->txNombreTercero}}</th>
        </tr>
        @php
            $reg = 0;
        @endphp
        <tr>
            <th>Asunto</th>
            <th>Modalidad</th>
            <th>Fecha</th>
            <th>Hora</th>
        </tr>
        @while ($reg < count($data))
            <tr>
                <td>{{ $data[$reg]->txAsuntoCronogramaEspacios }}</td>
                <td>{{ $data[$reg]->lsModalidadTerceroPeriodicidad }}</td>
                <td>{{ $data[$reg]->dtFechaCronogramaEspacios }}</td>
                <td>{{ $data[$reg]->hrHoraCronogramaEspacios }}</td>
            </tr>
            @php
                $reg++;
            @endphp
        @endwhile
    </table>
</body>

</html>
