@extends("layouts.principal")
@section("nombreModulo")
    Cronograma de espacios
@endsection

@section("contenido")
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="container-xl px-4 mt-n10 w-100">
	<div class="row">
		<div class="col-lg-12">
			<div class="card mt-n8">
				<div class="card-body calendar-container">
					<div id='calendar'></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section("scripts")
	{{Html::script("modules/reunion/js/calendar.js")}}
@endsection
