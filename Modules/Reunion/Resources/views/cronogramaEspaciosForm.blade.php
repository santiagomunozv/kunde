@extends('layouts.principal')
@section('nombreModulo')
    Cronograma de espacios
@endsection
@section('scripts')
    {{ Html::script('modules/reunion/js/cronogramaEspaciosForm.js') }}
@endsection
@section('contenido')
    {!! Form::model($cronogramaEspacios, [
        'route' => ['cronogramaespacios.update', $cronogramaEspacios->oidCronogramaEspacios],
        'method' => 'PUT',
        'id' => 'form-cronogramaespacios',
        'onsubmit' => 'return false;',
    ]) !!}
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
            <div id="errores"></div>

            {!! Form::hidden('oidCronogramaEspacios', null, ['id' => 'oidCronogramaEspacios']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txAsuntoCronogramaEspacios', 'Asunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txAsuntoCronogramaEspacios', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa el asunto',
                            'readonly',
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('lsModalidadTerceroPeriodicidad', 'Tipo de reunión', [
                            'class' => 'text-md text-primary mb-1  required ',
                        ]) !!}
                        {!! Form::select(
                            'lsModalidadTerceroPeriodicidad',
                            ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                            null,
                            ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione el tipo de reunión'],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtFechaCronogramaEspacios', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::date('dtFechaCronogramaEspacios', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la fecha de la reunión',
                            'readonly',
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('hrHoraCronogramaEspacios', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::time('hrHoraCronogramaEspacios', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la hora de la reunión',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('intDuracionCronogramaEspacios', 'Duración de la reunión', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::select(
                            'intDuracionCronogramaEspacios',
                            [
                                '15' => '15 minutos',
                                '30' => '30 minutos',
                                '45' => '45 minutos',
                                '60' => '1 hora',
                                '75' => '1 hora y 15 minutos',
                                '90' => '1 hora y 30 minutos',
                                '105' => '1 hora y 45 minutos',
                                '120' => '2 horas',
                            ],
                            null,
                            ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txUbicacionCronogramaEspacios', 'Ubicación', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::text('txUbicacionCronogramaEspacios', $tercero->txUrl, [
                            'class' => 'form-control',
                            'placeholder' => 'Lugar / Link de la reunión',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Evento_oidEvento', 'Eventos', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select(
                            'Evento_oidEvento',
                            $evento,
                            isset($cronogramaEspacios[0]) ? $cronogramaEspacios[0]['Evento_oidEvento'] : null,
                            [
                                'class' => 'chosen-select form-control',
                                'style' => 'width:100%',
                                'placeholder' => 'Selecciona el evento',
                                'disabled',
                            ],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select(
                            'Tercero_oidResponsable',
                            $asn_tercerolistaEmpleado,
                            isset($cronogramaEspacios[0]) ? $cronogramaEspacios[0]['Tercero_oidTerceroEmpleado'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Responsable'],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_idCliente', 'Cliente', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::select(
                            'Tercero_idCliente',
                            $asn_tercerolistaCliente,
                            isset($cronogramaEspacios[0]) ? $cronogramaEspacios[0]['Tercero_idCliente'] : null,
                            [
                                'class' => 'chosen-select form-control',
                                'style' => 'width:100%',
                                'placeholder' => 'Selecciona el Cliente',
                                'disabled',
                            ],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionCronogramaEspacios', 'Descripción', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::textArea(
                            'txDescripcionCronogramaEspacios',
                            isset($cronogramaEspacios[0]) ? $cronogramaEspacios[0]['txDescripcionCronogramaEspacios'] : null,
                            ['class' => 'form-control', 'style' => 'width:100%', 'placeholder' => ''],
                        ) !!}
                    </div>
                </div>
            </div>

            {!! Form::button('Programar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
