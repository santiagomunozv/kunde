@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
@endsection

{!! Form::model($reunion, ['url' => ['reuniones/tareasporestado/reagendartarea', $reunion->oidReunion], 'method' => 'POST', 'id' => 'form-modalReagendar', 'onsubmit' => 'return false;']) !!}
<div class="card mb-4">
    <div class="card-body">
        {!! Form::hidden('oidReunion', null, ['id' => 'oidReunion']) !!}
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
        <div id="errores"></div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('dtFechaReunion', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaReunion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la fecha de la reunión']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('hrHoraReunion', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::time('hrHoraReunion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la hora de la reunión']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('intDuracionReunion', 'Duración de la reunión', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('intDuracionReunion', ['15' => '15 minutos', '30' => '30 minutos', '45' => '45 minutos', '60' => '1 hora', '75' => '1 hora y 15 minutos', '90' => '1 hora y 30 minutos', '105' => '1 hora y 45 minutos', '120' => '2 horas'], null, ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txUbicacionReunion', 'Ubicación', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txUbicacionReunion', null, ['class' => 'form-control', 'placeholder' => 'Lugar / Link de la reunión']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('Tercero_oidResponsable', $asn_tercerolistaEmpleado, isset($reunion) ? $reunion->Tercero_oidTerceroEmpleado : null, ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Responsable']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txMotivoReagenda', 'Motivo', ['class' => 'text-md text-primary mb-1  required ']) !!}
                    {!! Form::text('txMotivoReagenda', null, ['class' => 'form-control', 'placeholder' => 'ingrese motivo de reagenda']) !!}
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-12">
                {!! Form::label('txObservacionReagenda', 'Observación de reagenda', ['class' => 'text-md text-primary mb-1 ']) !!}
                {!! Form::textArea('txObservacionReagenda', null, ['class' => 'form-control', 'rows' => 4]) !!}
            </div>
        </div>
    </div>
</div>

{!! Form::button('Guardar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabarModalReagendar()']) !!}

{!! Form::close() !!}
