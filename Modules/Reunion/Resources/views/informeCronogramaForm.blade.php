@extends('layouts.principal')

@section('nombreModulo')
    Informe de Cronogramas
@endsection

@section('contenido')
{!! Form::open(['id' => 'informerForm']) !!}
    <div class="card border-left-primary border-primary mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('cliente', 'Cliente', array('class' => 'text-md text-primary mb-1 required')) !!}
                    {!!Form::select('cliente',$cliente, null, ['class'=>'chosen-select form-control','style'=>'width:100%','placeholder'=>'Selecciona un cliente'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!!Form::label('fechaInicio', 'Fecha inicial', array('class' => 'text-md text-primary mb-1')) !!}
                        {!!Form::date('fechaInicio',null,[ 'class'=>'form-control','placeholder'=>'Ingresa la fecha de inicio'])!!}
                    </div>
                </div>
            </div>
            {!!Form::button("Generar",["type" => "submit" ,"class"=>"btn btn-primary"])!!}
            {!!Form::button("Generar PDF",["type" => "button", "onclick" => "generarPDF()", "class"=>"btn btn-primary"])!!}
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('scripts')
    {{Html::script("modules/reunion/js/informeCronogramaForm.js")}}
@endsection
