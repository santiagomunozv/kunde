@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
    {{ Html::script('/vendor/ckeditor/ckeditor.js') }}
@endsection

{!! Form::model($reunion, [
    'url' => ['reuniones/tareasporestado', $reunion->oidReunion],
    'method' => 'PUT',
    'id' => 'form-modalTarea',
    'onsubmit' => 'return false;',
]) !!}
<div class="card border-left-primary shadow h-100">

    <div class="card-body">
        {!! Form::hidden('oidReunion', null, ['id' => 'oidReunion']) !!}
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
        <!-- mostramos los errores de insercion -->
        <div id="errores"></div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txAsuntoReunion', 'Asunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txAsuntoReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa el asunto',
                        'disabled',
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txTipoReunion', 'Tipo de reunión', ['class' => 'text-md text-primary mb-1  required ']) !!}
                    {!! Form::select(
                        'txTipoReunion',
                        ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                        null,
                        [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Seleccione el tipo de reunión',
                            'disabled',
                        ],
                    ) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('dtFechaReunion', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::date('dtFechaReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la fecha de la reunión',
                        'disabled',
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('hrHoraReunion', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::time('hrHoraReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Ingresa la hora de la reunión',
                        'disabled',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('intDuracionReunion', 'Duración de la reunión', [
                        'class' => 'text-md text-primary mb-1 required ',
                    ]) !!}
                    {!! Form::select(
                        'intDuracionReunion',
                        [
                            '15' => '15 minutos',
                            '30' => '30 minutos',
                            '45' => '45 minutos',
                            '60' => '1 hora',
                            '75' => '1 hora y 15 minutos',
                            '90' => '1 hora y 30 minutos',
                            '105' => '1 hora y 45 minutos',
                            '120' => '2 horas',
                        ],
                        null,
                        ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión', 'disabled'],
                    ) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txUbicacionReunion', 'Ubicación', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::text('txUbicacionReunion', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Lugar / Link de la reunión',
                        'readonly',
                    ]) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Evento_oidEvento', 'Eventos', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select('Evento_oidEvento', $evento, isset($reunion[0]) ? $reunion[0]['Evento_oidEvento'] : null, [
                        'class' => 'chosen-select form-control',
                        'style' => 'width:100%',
                        'placeholder' => 'Selecciona el evento',
                        'disabled',
                    ]) !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    {!! Form::select(
                        'Tercero_oidResponsable',
                        $asn_tercerolistaEmpleado,
                        isset($reunion[0]) ? $reunion[0]['Tercero_oidTerceroEmpleado'] : null,
                        [
                            'class' => 'chosen-select form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Selecciona el Responsable',
                            'disabled',
                        ],
                    ) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('Tercero_idCliente', 'Cliente', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::select(
                        'Tercero_idCliente',
                        $asn_tercerolistaCliente,
                        isset($reunion[0]) ? $reunion[0]['Tercero_idCliente'] : null,
                        [
                            'class' => 'chosen-select form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Selecciona el Cliente',
                            'disabled',
                        ],
                    ) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('txEstadoReunion', 'Estado', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::select(
                        'txEstadoReunion',
                        ['Pendiente' => 'Pendiente', 'En proceso' => 'En proceso', 'Terminado' => 'Terminado'],
                        null,
                        ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Estado'],
                    ) !!}
                </div>
            </div>
        </div>

        <div class="card  ">
            <div class="card-header">
                <!-- NavBar-->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-observaciones-tab" data-toggle="tab"
                            href="#nav-observaciones" role="tab" aria-controls="nav-observaciones"
                            aria-selected="true">
                            Observaciones
                        </a>
                        <a class="nav-item nav-link" id="nav-bitacora-tab" data-toggle="tab" href="#nav-bitacora"
                            role="tab" aria-controls="nav-bitacora" aria-selected="false">
                            Bitácora
                        </a>
                        <a class="nav-item nav-link" id="nav-trazabilidad-tab" data-toggle="tab"
                            href="#nav-trazabilidad" role="tab" aria-controls="nav-trazabilidad"
                            aria-selected="false">
                            Trazabilidad
                        </a>
                        <a class="nav-item nav-link" id="nav-adjunto-tab" data-toggle="tab" href="#nav-adjunto"
                            role="tab" aria-controls="nav-adjunto" aria-selected="false">
                            Adjuntos
                        </a>
                        <a class="nav-item nav-link" id="nav-actividadesposencuentro-tab" data-toggle="tab"
                            href="#nav-actividadesposencuentro" role="tab"
                            aria-controls="nav-actividadesposencuentro" aria-selected="false">
                            Actividades pos encuentro
                        </a>
                    </div>
                </nav>
                <!-- End NavBar-->

            </div>
            <div class="card-body">
                <div class="tab-content" id="nav-tabContent">
                    <!-- Observaciones -->
                    <div class="tab-pane fade show active" id="nav-observaciones" role="tabpanel"
                        aria-labelledby="nav-observaciones-tab">
                        <!-- content -->
                        <div class="form-outline">
                            {!! Form::textArea('txDescripcionReunion', isset($reunion[0]) ? $reunion[0]['txDescripcionReunion'] : null, [
                                'class' => 'form-control',
                                'rows' => 4,
                            ]) !!}
                        </div>
                        <!--End content -->
                    </div>
                    <!-- End Observaciones -->

                    <!-- Bitácora -->

                    <div class="tab-pane fade show" id="nav-bitacora" role="tabpanel"
                        aria-labelledby="nav-bitacora-tab">
                        {!! Form::hidden(
                            'oidReunionBitacora',
                            isset($bitacora->oidReunionBitacora) ? $bitacora->oidReunionBitacora : null,
                            ['id' => 'oidReunionBitacora'],
                        ) !!}
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="shAlert" name="shAlert" class="alert alert-warning" role="alert"
                                    style="display: none;">
                                    Debe completar los campos de la bitacora
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <!--Button Switch -->
                                <div class="custom-control custom-switch text-right">
                                    <input type="checkbox" class="custom-control-input" id="swhBitacora"
                                        name="swhBitacora">
                                    <label class="custom-control-label" for="swhBitacora">Generar bitacora</label>
                                </div>
                                <!--End Button Switch -->
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    {!! Form::label('txActividadReunionBitacora', 'Actividad', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::text(
                                        'txActividadReunionBitacora',
                                        isset($bitacora->txActividadReunionBitacora) ? $bitacora->txActividadReunionBitacora : $reunion->txAsuntoReunion,
                                        [
                                            'class' => 'form-control',
                                            'style' => 'width:100%',
                                            'placeholder' => 'Digita el nombre de la actividad',
                                            'readonly' => 'true',
                                        ],
                                    ) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group ">
                                    {!! Form::label('dtFechaReunionBitacora', 'Fecha', ['class' => 'text-md text-primary mb-1']) !!}
                                    {!! Form::date(
                                        'dtFechaReunionBitacora',
                                        isset($bitacora->dtFechaReunionBitacora) ? $bitacora->dtFechaReunionBitacora : Carbon\Carbon::now(),
                                        ['class' => 'form-control', 'readonly' => 'true'],
                                    ) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group ">
                                    {!! Form::label('hrHoraInicioReunionBitacora', 'Hora inicial', [
                                        'class' => 'text-md text-primary mb-1 required ',
                                    ]) !!}
                                    {!! Form::time(
                                        'hrHoraInicioReunionBitacora',
                                        isset($bitacora->hrHoraInicioReunionBitacora)
                                            ? $bitacora->hrHoraInicioReunionBitacora
                                            : Carbon\Carbon::now()->format('H:i'),
                                        ['class' => 'form-control', 'style' => 'width:100%', 'readonly' => 'true'],
                                    ) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group ">
                                    {!! Form::label('hrHoraFinalReunionBitacora', 'Hora final', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    {!! Form::time(
                                        'hrHoraFinalReunionBitacora',
                                        isset($bitacora->hrHoraFinalReunionBitacora) ? $bitacora->hrHoraFinalReunionBitacora : null,
                                        ['class' => 'form-control', 'style' => 'width:100%', 'readonly' => 'true'],
                                    ) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group" id="divTextarea" style="display: none">
                                    {!! Form::label('txObservacionReunionBitacora', 'Observaciones', [
                                        'class' => 'text-md text-primary mb-1 required ',
                                    ]) !!}
                                    {!! Form::textArea(
                                        'txObservacionReunionBitacora',
                                        isset($bitacora->txObservacionReunionBitacora) ? $bitacora->txObservacionReunionBitacora : null,
                                        ['class' => 'form-control'],
                                    ) !!}
                                </div>
                            </div>
                        </div>

                        <section id="secctionMulti" name="secctionMulti" style="display: none;">
                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::label('txResponReunionBitacora', 'Pendientes', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    <input type="hidden" id="eliminarResponsable" name="eliminarResponsable"
                                        value="">
                                    <table class="table multiregistro table-sm table-hover table-borderless">
                                        <thead class="bg-primary text-light">
                                            <th width="50px">
                                                <button type="button" class="btn btn-primary btn-sm text-light"
                                                    onclick="configuracionBitacoraTareaPendiente.agregarCampos([],'L');">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </th>
                                            <th>Responsable</th>
                                            <th>Tarea</th>
                                            <th>Fecha de vencimiento</th>
                                        <tbody id="contenedorBitacoraTareaPendiente">
                                        </tbody>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::label('txOPendReunionBitacora', 'Asistentes', ['class' => 'text-md text-primary mb-1 required ']) !!}
                                    <input type="hidden" id="eliminarPendiente" name="eliminarPendiente"
                                        value="">
                                    <table class="table multiregistro table-sm table-hover table-borderless">
                                        <thead class="bg-primary text-light">
                                            <th width="50px">
                                                <button type="button" class="btn btn-primary btn-sm text-light"
                                                    onclick="configuracionBitacoraTareaAsistente.agregarCampos([],'L');">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Tallerista</th>
                                        <tbody id="contenedorBitacoraTareaAsistente">
                                        </tbody>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                    </div>
                    </section>
                    <!-- End Bitácora -->

                    <!-- Trazabilidad -->
                    <div class="tab-pane fade show" id="nav-trazabilidad" role="tabpanel"
                        aria-labelledby="nav-trazabilidad-tab">
                        <table id="trazabilidad-table" class="table table-hover table-sm scalia-grid">
                            <thead class="bg-primary text-light">
                                <tr>
                                    <th>Asunto</th>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Responsable</th>
                                    <th>Motivo</th>
                                    <th>Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($reagenda as $tarea)
                                    <tr>
                                        <td>{{ $tarea->txAsuntoReunion }}</td>
                                        <td>{{ $tarea->dtFechaAnteriorReagenda }}</td>
                                        <td>{{ $tarea->hrHoraAnteriorReagenda }}</td>
                                        <td>{{ $tarea->txNombreTercero }}</td>
                                        <td>{{ $tarea->txMotivoReagenda }}</td>
                                        <td>{{ $tarea->txObservacionReagenda }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- End Trazabilidad -->

                    <!-- Adjuntos -->
                    <div class="tab-pane fade show" id="nav-adjunto" role="tabpanel"
                        aria-labelledby="nav-adjunto-tab">
                        <!-- content -->


                        <div id="imagenes"> {{-- -----DIV DONDE SE AGREGAN LOS INPUT DE LAS IMAGENES------------- --}}
                            <input type="hidden" id="eliminarImagen" name="eliminarImagen" value="">
                        </div>
                        <div class="row">
                            <div class="col-md-12 pb-2">
                                <div id="dropzoneImage" class="dropzone-div">
                                    <div class="dz-message">
                                        <div class="col-xs-8">
                                            <div class="message">
                                                <p>Arrastra los elementos aqui o haz click para buscar el archivo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fallback">
                                        <input type="file" name="file" multiple style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            {{-- {{ Form::hidden('imagenCatalogo', null, ['id' => 'imagenCatalogo']) }} --}}
                            <div class="col-md-12" style="max-height: 250px;display: flex;overflow: auto;">
                                @if (isset($adjuntos))
                                    @foreach ($adjuntos as $file)
                                        <div class="imagenes m-1" id="imagen_{{ $file->oidAdjunto }}">
                                            <span class="btn cerrar"
                                                onclick="eliminarImagen({{ $file->oidAdjunto }})">&times;</span>
                                            <img src="/reuniones/tareasporestado/consultaradjuntos/{{ $file->oidAdjunto }}"
                                                class="rounded" height="200">
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <!--End content -->
                    </div>
                    <!-- End Adjuntos -->

                    <!-- Actividades pos encuentro -->
                    <div class="tab-pane fade show" id="nav-actividadesposencuentro" role="tabpanel"
                        aria-labelledby="nav-actividadesposencuentro-tab">
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table multiregistro table-sm table-hover table-borderless">
                                    <thead class="bg-primary text-light">
                                        <th width="50px">
                                        </th>
                                        <th>Actividad</th>
                                        <th>Cumplida</th>
                                        <th>Responsable</th>
                                        <th>Tarea</th>
                                        <th>Fecha de vencimiento</th>
                                    <tbody id="contenedorActividadPosEncuentro">
                                    </tbody>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End Actividades pos encuentro -->
                </div>
            </div>
        </div>

        {!! Form::button('Guardar', [
            'type' => 'button',
            'class' => 'btn btn-primary',
            'onclick' => 'grabarModalTarea()',
        ]) !!}

    </div>
</div>
{!! Form::close() !!}
