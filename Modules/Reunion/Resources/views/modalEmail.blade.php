@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
@endsection

<div class="alert alert-info" role="alert">
    ¿Está seguro de enviar el correo a <b>{{ $reunion->nombreCliente }}</b>?
</div>

{!! Form::open(['url' => ['/reuniones/tareasporestado/email/enviar/' . $reunion->oidReunion], 'method' => 'POST', 'id' => 'form-email', 'onsubmit' => 'return false;']) !!}
{!! Form::hidden('oidReunion', $reunion->oidReunion, ['id' => 'oidReunion']) !!}
<!-- mostramos los errores de insercion -->
<div id="errores"></div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-check">
            <input class="form-check-input" id="chEnviarEmail" type="checkbox" value="" checked>
            {!! Form::label('chEnviarEmail', 'Enviar Correo', ['class' => 'text-md text-primary mb-1 required ']) !!}
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group ">
            {!! Form::text('txEmailCliente', isset($reunion->emailTercero) ? $reunion->emailTercero : null, ['class' => 'form-control', 'placeholder' => 'Ingese el email del cliente ']) !!}
        </div>
    </div>
    {!! Form::button('Enviar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'enviarEmail()']) !!}
</div>

</form>
