@extends('layouts.principal')
@section('nombreModulo')
    Reuniones
@endsection
@section('scripts')
    <script>
        let reunionAsistente = '<?php echo json_encode($reunionAsistente); ?>';
    </script>
    {{ Html::script('modules/reunion/js/reunionForm.js') }}
@endsection
@section('contenido')
    @if (isset($reunion->oidReunion))
        {!! Form::model($reunion, [
            'route' => ['reunion.update', $reunion->oidReunion],
            'method' => 'PUT',
            'id' => 'form-reunion',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($reunion, [
            'route' => ['reunion.store', $reunion->oidReunion],
            'method' => 'POST',
            'id' => 'form-reunion',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidReunion', null, ['id' => 'oidReunion']) !!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txAsuntoReunion', 'Asunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txAsuntoReunion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el asunto']) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txTipoReunion', 'Tipo de reunión', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select(
                            'txTipoReunion',
                            ['Virtual' => 'Virtual', 'Presencial' => 'Presencial', 'Virtual/Grupal' => 'Virtual/Grupal'],
                            null,
                            ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione el tipo de reunión'],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('dtFechaReunion', 'Fecha', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::date('dtFechaReunion', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la fecha de la reunión',
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('hrHoraReunion', 'Hora', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::time('hrHoraReunion', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa la hora de la reunión',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('intDuracionReunion', 'Duración de la reunión', [
                            'class' => 'text-md text-primary mb-1 required ',
                        ]) !!}
                        {!! Form::select(
                            'intDuracionReunion',
                            [
                                '15' => '15 minutos',
                                '30' => '30 minutos',
                                '45' => '45 minutos',
                                '60' => '1 hora',
                                '75' => '1 hora y 15 minutos',
                                '90' => '1 hora y 30 minutos',
                                '105' => '1 hora y 45 minutos',
                                '120' => '2 horas',
                            ],
                            null,
                            ['class' => 'chosen-select form-control', 'placeholder' => 'Seleccione la duración de la reunión'],
                        ) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txUbicacionReunion', 'Ubicación', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txUbicacionReunion', null, [
                            'class' => 'form-control',
                            'placeholder' => 'Lugar / Link de la reunión',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Evento_oidEvento', 'Eventos', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select('Evento_oidEvento', $evento, isset($reunion[0]) ? $reunion[0]['Evento_oidEvento'] : null, [
                            'class' => 'chosen-select form-control',
                            'style' => 'width:100%',
                            'placeholder' => 'Selecciona el evento',
                        ]) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_oidResponsable', 'Responsable', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select(
                            'Tercero_oidResponsable',
                            $asn_tercerolistaEmpleado,
                            isset($reunion[0]) ? $reunion[0]['Tercero_oidTerceroEmpleado'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Responsable'],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('Tercero_idCliente', 'Cliente', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::select(
                            'Tercero_idCliente',
                            $asn_tercerolistaCliente,
                            isset($reunion[0]) ? $reunion[0]['Tercero_idCliente'] : null,
                            ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Cliente'],
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionReunion', 'Descripción', ['class' => 'text-md text-primary mb-1']) !!}
                        {!! Form::textArea('txDescripcionReunion', isset($reunion[0]) ? $reunion[0]['txDescripcionReunion'] : null, [
                            'class' => 'form-control',
                            'style' => 'width:100%',
                            'placeholder' => '',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarreunion" name="eliminarreunion" value="">
                    Asistentes
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="configuracionReunionAsistente.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Nombre</th>
                                <th>Correo</th>
                            <tbody id="contenedorReunionAsistente">

                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('chEstadoReunion', '¿Reunión exitosa?', ['class' => 'label-responsive-dos']) !!}
                        {!! Form::checkbox('chEstadoReunion', 1, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txDescripcionEstadoReunion', 'Descripción del estado de la reunión', [
                            'class' => 'text-md text-primary mb-1',
                        ]) !!}
                        {!! Form::textArea(
                            'txDescripcionEstadoReunion',
                            isset($reunion[0]) ? $reunion[0]['txDescripcionEstadoReunion'] : null,
                            ['class' => 'form-control', 'style' => 'width:100%', 'placeholder' => ''],
                        ) !!}
                    </div>
                </div>
            </div>

            @if (isset($reunion->txNombreProspecto))
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group ">
                            {!! Form::label('txNombreProspecto', 'Prospecto', ['class' => 'text-md text-primary mb-1 ']) !!}
                            {!! Form::text('txNombreProspecto', null, [
                                'class' => 'form-control',
                                'placeholder' => 'Ingresa Nombre',
                                'readonly',
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group ">
                            {!! Form::label('txEstadoProspecto', 'Estado', ['class' => 'text-md text-primary mb-1 ']) !!}
                            {!! Form::select(
                                'txEstadoProspecto',
                                [
                                    'En_Proceso' => 'En proceso',
                                    'Propuesta' => 'Propuesta',
                                    'Seguimiento' => 'Seguimiento',
                                    'No_Potencial' => 'No potencial',
                                    'Descartado' => 'Descartado',
                                ],
                                null,
                                ['class' => 'chosen-select form-control'],
                            ) !!}
                        </div>
                    </div>
                </div>
            @endif

            @if (isset($reunion->oidReunion) && Session::get('rolUsuario') == 1)
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
                {!! Form::button('Cancelar', [
                    'type' => 'button',
                    'class' => 'btn btn-info',
                    'onclick' => 'location.href="/reuniones/calendario"',
                ]) !!}
            @elseif(isset($reunion->oidReunion) && Session::get('rolUsuario') != 1)
                {!! Form::button('Cancelar', [
                    'type' => 'button',
                    'class' => 'btn btn-info',
                    'onclick' => 'location.href="/reuniones/calendario"',
                ]) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
