@extends('layouts.principal')
@section('nombreModulo')
    Eventos
@endsection
@section('scripts')
    <script>
        let reunionEventos = '<?php echo json_encode($reunionEventos); ?>';
        let reunionEventosAsistentes = '<?php echo json_encode($reunionEventosAsistentes); ?>';
        let asistentes = '<?php echo json_encode($asistentes); ?>';
    </script>
    {{ Html::script('modules/reunion/js/eventosForm.js') }}
@endsection
@section('contenido')
    @if (isset($eventos->oidEvento))
        {!! Form::model($eventos, [
            'route' => ['evento.update', $eventos->oidEvento],
            'method' => 'PUT',
            'id' => 'form-eventos',
            'onsubmit' => 'return false;',
        ]) !!}
    @else
        {!! Form::model($eventos, [
            'route' => ['evento.store', $eventos->oidEvento],
            'method' => 'POST',
            'id' => 'form-eventos',
            'onsubmit' => 'return false;',
        ]) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidEvento', null, ['id' => 'oidEvento']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txAnioEvento', 'Año', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::select('txAnioEvento', $anio, null, [
                            'class' => 'form-control chosen-select',
                            'placeholder' => 'Ingresa el año',
                        ]) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txCodigoEvento', 'Código', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txCodigoEvento', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el código']) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txNombreEvento', 'Nombre', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txNombreEvento', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el nombre']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txEstadoEvento', 'Estado', ['class' => 'text-md text-primary mb-1  required ']) !!}
                        {!! Form::select('txEstadoEvento', ['Activo' => 'Activo', 'Inactivo' => 'Inactivo'], null, [
                            'class' => 'chosen-select form-control',
                            'placeholder' => 'Selecciona el estado del evento',
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txColorEvento', 'Color de evento', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::color('txColorEvento', null, ['class' => 'form-control', 'placeholder' => 'Seleccione el color']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="imagenes"> {{-- -----DIV DONDE SE AGREGAN LOS INPUT DE LAS IMAGENES------------- --}}
                    <input type="hidden" id="eliminarImagen" name="eliminarImagen" value="">
                </div>
                <div class="col-sm-6 pb-2">
                    <div class="form-group ">
                        {!! Form::label('txArchivoEvento', 'Adjunto', ['class' => 'text-md text-primary mb-1 required ']) !!}
                    </div>
                    <div id="dropzoneImage" class="dropzone-div">
                        <div class="dz-message">
                            <div class="col-xs-8">
                                <div class="message">
                                    <p>haz click para buscar una imagen.</p>
                                </div>
                            </div>
                        </div>
                        <div class="fallback">
                            <input type="file" name="file" multiple style="display: none;">
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group ">
                        {!! Form::label('txMensajeEvento', 'Cuerpo de mensaje', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::textArea('txMensajeEvento', null, ['class' => 'form-control mt-4', 'placeholder' => '', 'rows' => 5]) !!}
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarreunion" name="eliminarreunion" value="">
                    Actividad
                    <input type="hidden" id="eliminarEventoActividad" name="eliminarEventoActividad" value="">
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="eventoActividad.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Descripción</th>
                            <tbody id="eventoActividadContent">
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

            <div class="div card border-left-primary shadow h-100 py-2">
                <div class="div card-body">
                    <input type="hidden" id="eliminarreunion" name="eliminarreunion" value="">
                    Asistentes
                    <input type="hidden" id="eliminarEventoAsistente" name="eliminarEventoAsistente" value="">
                    <div class="div card-body multi-max">
                        <table class="table multiregistro table-sm table-hover table-borderless">
                            <thead class="bg-primary text-light">
                                <th width="50px">
                                    <button type="button" class="btn btn-primary btn-sm text-light"
                                        onclick="eventoAsistente.agregarCampos([],'L');">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </th>
                                <th>Asistente</th>
                            <tbody id="eventoAsistenteContent">
                            </tbody>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>



            @if (isset($eventos->oidEvento))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
