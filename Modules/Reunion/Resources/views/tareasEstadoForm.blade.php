@extends("layouts.principal")
@section('nombreModulo')
    Espacios Kunde
@endsection
@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
    {{Html::script("/vendor/ckeditor/ckeditor.js")}}

    <script>
        let idResponsable = '<?php echo json_encode($idResponsable); ?>';
        let nombreResponsable = '<?php echo json_encode($nombreResponsable); ?>';
    </script>

    <style>
        .card-box {
            padding: 20px;
            border-radius: 3px;
            margin-bottom: 30px;
            background-color: #fff;
            overflow: auto;
            max-height: 100vh
        }

        .taskList li {
            background-color: #fff;
            border: 1px solid rgba(165, 13, 13, 0.2);
            padding: 10px;
            margin-bottom: 20px;
            border-radius: 3px
        }
        .card_task {
            transition: all 0.2s ease;
            cursor: pointer
        }
        .card_task:hover {
            box-shadow: 5px 6px 6px 2px #e9ecef;
            transform: scale(1.1)
        }
        .area-top{
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 1020;
            background-color: #fff;
         }
    </style>

@endsection
@section('contenido')

    <div class="form-group">
        <a href="#" class="btn btn-secondary btn-icon-split" onclick="modalFiltrosTareas()">
            <span class="icon text-white-50 float-right">
                <i class="fas fa-filter"></i>
            </span>
            <span class="text">Filtrar Tareas</span>
        </a>
    </div>

    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card-box">
                    <div class="area-top">

                        <h4 class="text-dark header-title">Pendientes</h4>
                        <hr class="mt-0 mb-4">
                    </div>
                    @if (count($pendientes) > 0)
                        @foreach ($pendientes as $pendiente)
                            <ul class="sortable-list taskList list-unstyled ui-sortable" id="upcoming">

                                <div class="card card_task border-info text-center" >
                                    <div class="card-header">
                                        <div class="d-flex justify-content-between">
                                            <a class="font-weight-bold mb-0" >
                                                {{$pendiente->txAsuntoReunion}}
                                            </a>
                                            <div class="dropdown">
                                                <!--Trigger-->
                                                <a type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="fas fa-cog fa-lg"></i>
                                                </a>
                                                <!--Menu Tringger-->
                                                <div class="dropdown-menu dropdown-primary">
                                                    @if (Session::get('rolUsuario') == 1)
                                                        <a class="dropdown-item" style="color: navy"
                                                            onclick="modalRegendar({{$pendiente->oidReunion}})">
                                                            <i class="far fa-clock fa-lg"></i>&nbsp;&nbsp;Re agendar
                                                        </a>
                                                    @endif
                                                    @if (Session::get('rolUsuario') == 1)
                                                        <a class="dropdown-item" href="#" style="color: crimson"
                                                            onclick="eliminarTarea({{$pendiente->oidReunion}})">
                                                            <i class="fas fa-ban fa-lg"></i>&nbsp;&nbsp;Eliminar
                                                        </a>
                                                    @endif
                                                    @if (isset($pendiente->bitacora->oidReunionBitacora))
                                                    <a class="dropdown-item" href="#" style="color: navy"
                                                        onclick="descargarBitacora({{$pendiente->oidReunion}})">
                                                        <i class="fas fa-download"></i>&nbsp;&nbsp;Decagar Bitácora
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="modalTarea" class="card-body" onclick="ModalTarea({{ $pendiente->oidReunion }})">
                                        {{$pendiente->txNombreTercero}}
                                        <div class="d-flex justify-content-between">
                                            <small class="font-weight-bold">
                                                {{ $pendiente->dtFechaReunion }}
                                            </small>
                                            <small class="font-weight-bold">
                                                {{ $pendiente->hrHoraReunion }}
                                            </small>
                                        </div>
                                    </div>
                                </div>

                            </ul>
                        @endforeach
                    @endif
                    @if (Session::get('rolUsuario') == 1)
                        <a href="#" onclick="abrirModalCrearTarea()"
                            class="btn btn-custom btn-block mt-3 waves-effect waves-light">
                            <i class="fa fa-plus-circle"></i> Agregar Nueva Tarea
                        </a>
                    @endif
                </div>
            </div>
            <!-- End Pednientes-->

            <!-- En proceso -->
            <div class="col-lg-4">
                <div class="card-box">
                    <h4 class="text-dark header-title">En proceso</h4>
                    <hr class="mt-0 mb-4">
                    @if (count($enProceso) > 0)
                        @foreach ($enProceso as $proceso)
                            <ul class="sortable-list taskList list-unstyled ui-sortable" id="upcoming">

                                <div class="card card_task border-warning text-center">
                                    <div class="card-header">
                                        <div class="d-flex justify-content-between">
                                            <a class="font-weight-bold mb-0" >
                                                {{$proceso->txAsuntoReunion}}
                                            </a>
                                            <div class="dropdown">
                                                <!--Trigger-->
                                                <a type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="fas fa-cog fa-lg"></i>
                                                </a>
                                                <!--Menu Tringger-->
                                                <div class="dropdown-menu dropdown-primary">
                                                    {{-- <a class="dropdown-item" style="color: navy"
                                                        onclick="modalRegendar({{$proceso->oidReunion}})">
                                                        <i class="far fa-clock fa-lg"></i>&nbsp;&nbsp;Re agendar
                                                    </a> --}}
                                                    {{-- <a class="dropdown-item" href="#" style="color: crimson"
                                                        onclick="eliminarTarea({{$proceso->oidReunion}})">
                                                        <i class="fas fa-ban fa-lg"></i>&nbsp;&nbsp;Eliminar
                                                    </a> --}}
                                                    @if (isset($proceso->bitacora->oidReunionBitacora))
                                                    <a class="dropdown-item" href="#" style="color: navy"
                                                        onclick="descargarBitacora({{$proceso->oidReunion}})">
                                                        <i class="fas fa-download"></i>&nbsp;&nbsp;Decagar Bitácora
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body" onclick="ModalTarea({{ $proceso->oidReunion }})">
                                        {{$proceso->txNombreTercero}}
                                        <div class="d-flex justify-content-between">
                                            <small class="font-weight-bold">
                                                {{ $proceso->dtFechaReunion }}
                                            </small>
                                            <small class="font-weight-bold">
                                                {{ $proceso->hrHoraReunion }}
                                            </small>
                                        </div>
                                    </div>
                                </div>

                            </ul>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- End En proceso -->

            <!-- Completos -->
            <div class="col-lg-4">
                <div class="card-box">
                    <h4 class="text-dark header-title">Completos</h4>
                    <hr class="mt-0 mb-4">
                    @if (count($completos) > 0)

                        @foreach ($completos as $completo)
                            <ul class="sortable-list taskList list-unstyled ui-sortable" id="upcoming">

                                <div class="card card_task border-success nfo text-center">
                                    <div class="card-header">
                                        <div class="d-flex justify-content-between">
                                            <a class="font-weight-bold mb-0" >
                                                {{$completo->txAsuntoReunion}}
                                            </a>
                                            <div class="dropdown">
                                                <!--Trigger-->
                                                <a type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="fas fa-cog fa-lg"></i>
                                                </a>
                                                <!--Menu Tringger-->
                                                <div class="dropdown-menu dropdown-primary">
                                                    {{-- <a class="dropdown-item" style="color: navy"
                                                        onclick="modalRegendar({{$completo->oidReunion}})">
                                                        <i class="far fa-clock fa-lg"></i>&nbsp;&nbsp;Re agendar
                                                    </a>
                                                    <a class="dropdown-item" href="#" style="color: crimson"
                                                        onclick="eliminarTarea({{$completo->oidReunion}})">
                                                        <i class="fas fa-ban fa-lg"></i>&nbsp;&nbsp;Eliminar
                                                    </a> --}}
                                                    @if (isset($completo->bitacora->oidReunionBitacora))
                                                    <a class="dropdown-item" href="#" style="color: navy"
                                                        onclick="descargarBitacora({{$completo->oidReunion}})">
                                                        <i class="fas fa-download"></i>&nbsp;&nbsp;Decagar Bitácora
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body" onclick="ModalTarea({{ $completo->oidReunion }})">
                                        {{$completo->txNombreTercero}}
                                        <div class="d-flex justify-content-between">
                                            <small class="font-weight-bold">
                                                {{ $completo->dtFechaReunion }}
                                            </small>
                                            <small class="font-weight-bold">
                                                {{ $completo->hrHoraReunion }}
                                            </small>
                                        </div>
                                    </div>
                                </div>

                            </ul>
                        @endforeach
                    @endif
                </div>
            </div>
            <!--End Completos -->

        </div>
        <!-- end row -->
    </div>







@endsection
