@section('scripts')
    {{ Html::script('modules/reunion/js/tareasPorEstadoForm.js') }}
@endsection

<div class="card mb-4">
    <div class="card-body">
        <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('FilterTerceroCli', 'Filtrar por cliente', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::select('FilterTerceroCli', $asn_tercerolistaCliente, null, ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Cliente']) !!}

                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('FilterTerceroEmp', 'Filtrar por Responsable', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::select('FilterTerceroEmp', $asn_tercerolistaEmpleado, null, ['class' => 'chosen-select form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona el Responsable']) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('FilterDtFechaInicio', 'Filtrar por fecha de inicio', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::date('FilterDtFechaInicio', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la fecha de la reunión']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group ">
                    {!! Form::label('FilterDtFechaFinal', 'Filtrar por fecha final', ['class' => 'text-md text-primary mb-1']) !!}
                    {!! Form::date('FilterDtFechaFinal', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la fecha de la reunión']) !!}
                </div>
            </div>
        </div>

        <div class="modal-footer">
            {!! Form::button('Filtrar', ['type' => 'button', 'class' => 'btn btn-primary mr-auto', 'onclick' => 'filtrar()']) !!}
            {!! Form::button('Limpiar Filtros', ['type' => 'button', 'class' => 'btn btn-secondary', 'onclick' => 'limpiarFiltros()']) !!}
        </div>

    </div>
</div>


{!! Form::close() !!}
