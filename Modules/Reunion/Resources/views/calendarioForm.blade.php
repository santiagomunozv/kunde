@extends('layouts.principal')
@section('nombreModulo')
    Calendario
@endsection

@section('scripts')
    {{-- {{ Html::script('modules/reunion/js/calendarioForm.js') }} --}}
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'es',
                height: 700,
                contentHeight: 700,
                headerToolbar: {
                    left: 'dayGridMonth,timeGridWeek,timeGridDay',
                    center: 'title',
                    right: 'today,prevYear,prev,next,nextYear'
                },
                navLinks: true,
                editable: true,
                selectable: true,
                nowIndicator: true,
                eventSources: [{
                    events: function(info, successCallback, failureCallback) {
                        $.ajax({
                            url: '/reuniones/calendario',
                            dataType: 'json',
                            data: {
                                empleado_id: $('#empleado').val()
                            },
                            success: function(doc) {
                                var events = [];
                                if (!!doc) {
                                    $.map(doc, function(r) {
                                        let dateStart = moment(r.start +
                                            " " + r.time);
                                        events.push({
                                            id: r.id,
                                            title: r.title,
                                            start: dateStart
                                                .format(),
                                            end: r.start,
                                            url: r.url
                                        });
                                    });
                                }
                                successCallback(events);
                            }
                        });
                    }
                }],
            });
            calendar.render();
            $('#empleado').on('change', function() {
                calendar.refetchEvents();
            });
        });
    </script>
@endsection

@section('estilos')
    <style>

    </style>
@endsection

@section('contenido')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container-xl px-4 mt-n10 w-100">
        <div class="row">
            <div class="col-lg-12">
                @if (Session::get('rolUsuario') == 1)
                    <div class="card mt-n8">
                        <div class="card-body calendar-container">
                            <div class="form-group">
                                <label for="empleado">Seleccionar Empleado:</label>
                                <select id="empleado" class="form-control">
                                    <option value="">Todos los empleados</option>
                                    @foreach ($empleados as $empleado)
                                        <option value="{{ $empleado->oidTercero }}">{{ $empleado->txNombreTercero }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card mt-n8">
                    <div class="card-body calendar-container">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
