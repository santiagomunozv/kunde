<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Seguridad\Entities\UsuarioModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\Seguridad\Entities\UsuarioCompaniaRolModel;
use Modules\Seguridad\Entities\CompaniaModel;
use Modules\Seguridad\Entities\RolModel;


use Modules\Seguridad\Http\Requests\UsuarioRequest;
use Carbon\Carbon;

class UsuarioController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $usuario = UsuarioModel::orderBy('oidUsuario' , 'DESC')->get();
        return view('seguridad::usuarioGrid', compact('permisos' , 'usuario'));
    }

            
    public function create()
    {
        $usuario = new UsuarioModel();
        $usuario->txEstadoUsuario = 'Activo';
        // Consultamos los maestros necesarios para las listas de seleccion
		$asn_tercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('txNombreTercero','oidTercero');
        $oidCompanialista = CompaniaModel::where('seg_compania.txEstadoCompania','=', 'Activo')->pluck('oidCompania');
        $txNombreCompanialista = CompaniaModel::where('seg_compania.txEstadoCompania','=', 'Activo')->pluck('txNombreCompania');
        $oidRollista = RolModel::where('seg_rol.txEstadoRol','=', 'Activo')->pluck('oidRol');
        $txNombreRollista = RolModel::where('seg_rol.txEstadoRol','=', 'Activo')->pluck('txNombreRol');
			
        return view('seguridad::usuarioForm' , ['usuario' => $usuario] , compact('asn_tercerolista','oidCompanialista','txNombreCompanialista','oidRollista','txNombreRollista'));
    }
            
    public function store(UsuarioRequest $request)
    {
        $usuario = new UsuarioModel();
        $usuario->fill($request->all());

        DB::beginTransaction();
        try{
            $usuario->save();
        	$this->grabarDetalle($request, $usuario->oidUsuario);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidUsuario' => $usuario->oidUsuario] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500,$e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $usuario = UsuarioModel::find($id);
        // Consultamos los maestros necesarios para las listas de seleccion
		$asn_tercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('txNombreTercero','oidTercero');
				$oidCompanialista = CompaniaModel::where('seg_compania.txEstadoCompania','=', 'Activo')->pluck('oidCompania');
				$txNombreCompanialista = CompaniaModel::where('seg_compania.txEstadoCompania','=', 'Activo')->pluck('txNombreCompania');
				$oidRollista = RolModel::where('seg_rol.txEstadoRol','=', 'Activo')->pluck('oidRol');
				$txNombreRollista = RolModel::where('seg_rol.txEstadoRol','=', 'Activo')->pluck('txNombreRol');
			
        return view('seguridad::usuarioForm' , ['usuario' => $usuario] , compact('asn_tercerolista','oidCompanialista','txNombreCompanialista','oidRollista','txNombreRollista'));
    }
            

    public function update(UsuarioRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $usuario = UsuarioModel::find($id);
            $usuario->fill($request->all());
            $usuario->save();
        	$this->grabarDetalle($request, $usuario->oidUsuario);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidUsuario' => $usuario->oidUsuario] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidUsuario' => $usuario->oidUsuario] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarUsuarioCompaniaRol']);
		UsuarioCompaniaRolModel::whereIn('oidUsuarioCompaniaRol',$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        if($request['oidUsuarioCompaniaRol']){
            for($i = 0; $i < count($request['oidUsuarioCompaniaRol']); $i++)
            {
                
                // creamos un array con el campo oid autonumerico
                $indice = array(
                    'oidUsuarioCompaniaRol' => $request['oidUsuarioCompaniaRol'][$i]);
                // En otro array guardamos los demas campos fillables
                $datos= array(
                    'Usuario_oidUsuario_1aM' => $id,
                                    'Compania_oidUsuarioCompaniaRol' => ($request['Compania_oidUsuarioCompaniaRol'][$i] == '' ? null : $request['Compania_oidUsuarioCompaniaRol'][$i]),
                            'Rol_oidUsuarioCompaniaRol' => ($request['Rol_oidUsuarioCompaniaRol'][$i] == '' ? null : $request['Rol_oidUsuarioCompaniaRol'][$i]),
                            
                    );
                // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
                $guardar = UsuarioCompaniaRolModel::updateOrCreate($indice, $datos);
            }
        }

    }}
