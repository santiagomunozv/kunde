<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Seguridad\Entities\ModuloModel;
use Modules\Seguridad\Entities\OpcionModel;
use Modules\Seguridad\Http\Requests\OpcionRequest;

class OpcionController extends Controller
{
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if (!$permisos) {
            abort(401);
        }
        $option = OpcionModel::join("seg_modulo", 'seg_opcion.Modulo_oidModulo_1aM', 'seg_modulo.oidModulo')->orderBy('oidOpcion', 'DESC')->get();

        return view('seguridad::opcionGrid', compact('permisos', 'option'));
    }

    public function create()
    {
        $option = new OpcionModel();
        $txNombreModulo = ModuloModel::all()->pluck('txNombreModulo', 'oidModulo');
        return view('seguridad::opcionForm', ['option' => $option], compact('txNombreModulo'));
    }


    public function store(OpcionRequest $request)
    {
        DB::beginTransaction();
        try {
            $option = OpcionModel::create($request->all());
            DB::commit();
            return response(['oidOpcion' => $option->oidOpcion], 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return abort(500, $e->getMessage());
        }
    }


    public function show($id)
    {
        // return view('seguridad::show');
    }

    public function edit($id)
    {
        $option = OpcionModel::find($id);
        $txNombreModulo = ModuloModel::all()->pluck('txNombreModulo', 'oidModulo');
        return view('seguridad::opcionForm', ['option' => $option], compact('txNombreModulo'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $option = OpcionModel::find($id);
            $option->fill($request->all());
            $option->save();

            DB::commit();
            return response(['oidOpcion' => $option->oidOpcion], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response(['oidOpcion' => $option->oidRol], 500);
        }
    }

    public function destroy($id)
    {
        OpcionModel::destroy($id);
        return redirect('/registroseguimiento/opcion');
    }
}
