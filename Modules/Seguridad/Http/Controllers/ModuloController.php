<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Seguridad\Entities\ModuloModel;
use Modules\Seguridad\Entities\OpcionModel;


use Modules\Seguridad\Http\Requests\ModuloRequest;

class ModuloController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $modulo = ModuloModel::orderBy('oidModulo' , 'DESC')->get();
        return view('seguridad::moduloGrid', compact('permisos' , 'modulo'));
    }

            
    public function create()
    {
        $modulo = new ModuloModel();
        $modulo->txEstadoModulo = 'Activo';
        
        
        return view('seguridad::moduloForm' , ['modulo' => $modulo] );
    }
            
    public function store(ModuloRequest $request)
    {
        DB::beginTransaction();
        try{
            $modulo = ModuloModel::create($request->all());
        	$this->grabarDetalle($request, $modulo->oidModulo);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidModulo' => $modulo->oidModulo] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidModulo' => $modulo->oidModulo] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $modulo = ModuloModel::find($id);
        
        return view('seguridad::moduloForm' , ['modulo' => $modulo] );
    }
            

    public function update(ModuloRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $modulo = ModuloModel::find($id);
            $modulo->fill($request->all());
            $modulo->save();
        	$this->grabarDetalle($request, $modulo->oidModulo);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidModulo' => $modulo->oidModulo] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidModulo' => $modulo->oidModulo] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarOpcion']);
		OpcionModel::whereIn('oidOpcion',$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        for($i = 0; $i < count($request['oidOpcion']); $i++)
        {
            
            // creamos un array con el campo oid autonumerico
            $indice = array(
                'oidOpcion' => $request['oidOpcion'][$i]);
            // En otro array guardamos los demas campos fillables
            $datos= array(
                'Modulo_oidModulo_1aM' => $id,
								'txNombreOpcion' => $request['txNombreOpcion'][$i],
						'txRutaOpcion' => $request['txRutaOpcion'][$i],
						'txIconoOpcion' => $request['txIconoOpcion'][$i],
						
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            $guardar = OpcionModel::updateOrCreate($indice, $datos);
        }


    }}
