<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Seguridad\Entities\PaqueteModel;
use Modules\Seguridad\Entities\ModuloModel;


use Modules\Seguridad\Http\Requests\PaqueteRequest;

class PaqueteController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $paquete = PaqueteModel::orderBy('oidPaquete' , 'DESC')->get();
        return view('seguridad::paqueteGrid', compact('permisos' , 'paquete'));
    }

            
    public function create()
    {
        $paquete = new PaqueteModel();
        $paquete->txEstadoPaquete = 'Activo';
        
        
        return view('seguridad::paqueteForm' , ['paquete' => $paquete] );
    }
            
    public function store(PaqueteRequest $request)
    {
        DB::beginTransaction();
        try{
            $paquete = PaqueteModel::create($request->all());
        	$this->grabarDetalle($request, $paquete->oidPaquete);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPaquete' => $paquete->oidPaquete] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidPaquete' => $paquete->oidPaquete] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $paquete = PaqueteModel::find($id);
        
        return view('seguridad::paqueteForm' , ['paquete' => $paquete] );
    }
            

    public function update(PaqueteRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $paquete = PaqueteModel::find($id);
            $paquete->fill($request->all());
            $paquete->save();
        	$this->grabarDetalle($request, $paquete->oidPaquete);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidPaquete' => $paquete->oidPaquete] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidPaquete' => $paquete->oidPaquete] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarModulo']);
		ModuloModel::whereIn('oidModulo',$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        for($i = 0; $i < count($request['oidModulo']); $i++)
        {
            
            // creamos un array con el campo oid autonumerico
            $indice = array(
                'oidModulo' => $request['oidModulo'][$i]);
            // En otro array guardamos los demas campos fillables
            $datos= array(
                'Paquete_oidPaquete_1aM' => $id,
								'txNombreModulo' => $request['txNombreModulo'][$i],
						'txIconoModulo' => $request['txIconoModulo'][$i],
						
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            $guardar = ModuloModel::updateOrCreate($indice, $datos);
        }


    }}
