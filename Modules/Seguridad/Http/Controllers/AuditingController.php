<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class AuditingController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }

        $auditing = DB::table('audits as ad')
        ->leftJoin('seg_usuario as u', 'ad.user_id', '=', 'u.oidUsuario')
        ->leftJoin('asn_tercero as t', 'u.Tercero_oidUsuario', '=', 't.oidTercero')
        ->select(DB::raw("REPLACE (auditable_type, 'App', '') AS Modulo,
                        txNombreTercero AS Usuario,
                        CASE 
                            WHEN EVENT = 'updated' THEN 'Actualizar' 
                            WHEN EVENT = 'created' THEN 'Crear' 
                            WHEN EVENT = 'deleted' THEN 'Eliminar'
                        END AS 'Evento',
                        auditable_id AS IdModulo,
                        event AS Evento,
                        old_values AS ValorAnterior,
                        new_values AS ValorNuevo,
                        ad.created_at as FechaTransaccion"))
        // ->where('Compania_idCompania', '=', \Session::get('idCompania'))
        ->get();
        return view('seguridad::AuditingGrid', compact('auditing'));
    }

            
    public function create()
    {

    }
            
    public function store(CompaniaRequest $request)
    {


        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {

    }
            

    public function update(CompaniaRequest $request, $id)
    {

    }
}