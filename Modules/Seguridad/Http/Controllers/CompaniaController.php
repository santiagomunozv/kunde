<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Seguridad\Entities\CompaniaModel;
use Modules\Seguridad\Entities\CompaniaTerceroModel;
use Modules\AsociadoNegocio\Entities\TerceroModel;


use Modules\Seguridad\Http\Requests\CompaniaRequest;

class CompaniaController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $compania = CompaniaModel::orderBy('oidCompania' , 'DESC')->get();
        return view('seguridad::companiaGrid', compact('permisos' , 'compania'));
    }

            
    public function create()
    {
        $compania = new CompaniaModel();
        $compania->txEstadoCompania = 'Activo';
        // Consultamos los maestros necesarios para las listas de seleccion
		$oidTercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('oidTercero');
				$txNombreTercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('txNombreTercero');
			
        
        return view('seguridad::companiaForm' , ['compania' => $compania] , compact('oidTercerolista','txNombreTercerolista'));
    }
            
    public function store(CompaniaRequest $request)
    {
        DB::beginTransaction();
        try{
            $compania = CompaniaModel::create($request->all());
        	$this->grabarDetalle($request, $compania->oidCompania);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCompania' => $compania->oidCompania] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidCompania' => $compania->oidCompania] , 500);
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $compania = CompaniaModel::find($id);
        // Consultamos los maestros necesarios para las listas de seleccion
		$oidTercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('oidTercero');
				$txNombreTercerolista = TerceroModel::where('asn_tercero.txEstadoTercero','=', 'Activo')->pluck('txNombreTercero');
			
        return view('seguridad::companiaForm' , ['compania' => $compania] , compact('oidTercerolista','txNombreTercerolista'));
    }
            

    public function update(CompaniaRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $compania = CompaniaModel::find($id);
            $compania->fill($request->all());
            $compania->save();
        	$this->grabarDetalle($request, $compania->oidCompania);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidCompania' => $compania->oidCompania] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidCompania' => $compania->oidCompania] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarCompaniaTercero']);
		CompaniaTerceroModel::whereIn('oidCompaniaTercero',$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        for($i = 0; $i < count($request['oidCompaniaTercero']); $i++)
        {
            
            // creamos un array con el campo oid autonumerico
            $indice = array(
                'oidCompaniaTercero' => $request['oidCompaniaTercero'][$i]);
            // En otro array guardamos los demas campos fillables
            $datos= array(
                'Compania_oidCompania_1aM' => $id,
								'Tercero_oidCompaniaTercero' => ($request['Tercero_oidCompaniaTercero'][$i] == '' ? null : $request['Tercero_oidCompaniaTercero'][$i]),
						'lsCodificacionCompaniaTercero' => $request['lsCodificacionCompaniaTercero'][$i],
						
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            $guardar = CompaniaTerceroModel::updateOrCreate($indice, $datos);
        }


    }}
