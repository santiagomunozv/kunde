<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

use Modules\Seguridad\Entities\RolModel;
use Modules\Seguridad\Entities\RolOpcionModel;
use Modules\Seguridad\Entities\OpcionModel;


use Modules\Seguridad\Http\Requests\RolRequest;

class RolController extends Controller
{
            
    public function index()
    {
        $permisos = $this->consultarPermisos();
        if(!$permisos){
            abort(401);
        }
        $rol = RolModel::orderBy('oidRol' , 'DESC')->get();
        return view('seguridad::rolGrid', compact('permisos' , 'rol'));
    }

            
    public function create()
    {
        $rol = new RolModel();
        $rol->txEstadoRol = 'Activo';
        // Consultamos los maestros necesarios para las listas de seleccion
		$oidOpcionlista = OpcionModel::All()->pluck('oidOpcion');
				$txNombreOpcionlista = OpcionModel::All()->pluck('txNombreOpcion');
			
        
        return view('seguridad::rolForm' , ['rol' => $rol] , compact('oidOpcionlista','txNombreOpcionlista'));
    }
            
    public function store(RolRequest $request)
    {
        DB::beginTransaction();
        try{
            $rol = RolModel::create($request->all());
        	$this->grabarDetalle($request, $rol->oidRol);
			 // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidRol' => $rol->oidRol] , 201);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, $e->getMessage());
        }

        
    }
            
    public function show($id)
    {
        //
    }
            
    public function edit($id)
    {
        $rol = RolModel::find($id);
        // Consultamos los maestros necesarios para las listas de seleccion
		$oidOpcionlista = OpcionModel::All()->pluck('oidOpcion');
				$txNombreOpcionlista = OpcionModel::All()->pluck('txNombreOpcion');
			
        return view('seguridad::rolForm' , ['rol' => $rol] , compact('oidOpcionlista','txNombreOpcionlista'));
    }
            

    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $rol = RolModel::find($id);
            $rol->fill($request->all());
            $rol->save();
        	$this->grabarDetalle($request, $rol->oidRol);
			// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oidRol' => $rol->oidRol] , 200);
        }catch(\Exception $e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oidRol' => $rol->oidRol] , 500);
        }
    }
    // En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle($request, $id)
    {
        $idsEliminar = explode(',', $request['eliminarRolOpcion']);
		RolOpcionModel::whereIn('oidRolOpcion',$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        for($i = 0; $i < count($request['oidRolOpcion']); $i++)
        {
            
            // creamos un array con el campo oid autonumerico
            $indice = array(
                'oidRolOpcion' => $request['oidRolOpcion'][$i]);
            // En otro array guardamos los demas campos fillables
            $datos= array(
                'Rol_oidRol_1aM' => $id,
								'Opcion_oidRolOpcion' => ($request['Opcion_oidRolOpcion'][$i] == '' ? null : $request['Opcion_oidRolOpcion'][$i]),
						'chAdicionarRolOpcion' => $request['chAdicionarRolOpcion'][$i],
						'chModificarRolOpcion' => $request['chModificarRolOpcion'][$i],
						'chEliminarRolOpcion' => $request['chEliminarRolOpcion'][$i],
						'chConsultarRolOpcion' => $request['chConsultarRolOpcion'][$i],
						'chInactivarRolOpcion' => $request['chInactivarRolOpcion'][$i],
						
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            $guardar = RolOpcionModel::updateOrCreate($indice, $datos);
        }


    }}
