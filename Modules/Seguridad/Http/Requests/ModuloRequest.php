<?php

        namespace Modules\Seguridad\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class ModuloRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"txNombreModulo" => "required|string",

                );

                $reg = count($this->get('oidOpcion'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('txNombreOpcion')[$i]) == '' )
                                $validacion['txNombreOpcion'.$i] =  'required';

					if(trim($this->get('txRutaOpcion')[$i]) == '' )
                                $validacion['txRutaOpcion'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["Paquete_oidPaquete_1aM.integer"] = "El campo Paquete Solo puede contener un valor entero";
				$mensaje["txNombreModulo.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreModulo.required"] =  "El campo Nombre es obligatorio";


                $reg = count($this->get('oidOpcion'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["txNombreOpcion".$i.".required"] = "El campo Nombre en la línea ".($i + 1)." es obligatorio";
				$mensaje["txRutaOpcion".$i.".required"] = "El campo Ruta en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        