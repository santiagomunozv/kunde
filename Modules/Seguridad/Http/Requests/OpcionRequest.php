<?php

namespace Modules\Seguridad\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OpcionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validacion = array(
            "txNombreOpcion" => "required|string",
            "txRutaOpcion" => "required|string",
            "Modulo_oidModulo_1aM" => "required|int",
        );

        return $validacion;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
