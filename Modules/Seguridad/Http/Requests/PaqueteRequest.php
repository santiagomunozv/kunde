<?php

        namespace Modules\Seguridad\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class PaqueteRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                						"inOrdenPaquete" => "required|integer",
						"txNombrePaquete" => "required|string",

                );

                $reg = count($this->get('oidModulo'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('txNombreModulo')[$i]) == '' )
                                $validacion['txNombreModulo'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["inOrdenPaquete.integer"] = "El campo Orden Solo puede contener un valor entero";
				$mensaje["inOrdenPaquete.required"] =  "El campo Orden es obligatorio";
				$mensaje["txNombrePaquete.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombrePaquete.required"] =  "El campo Nombre es obligatorio";


                $reg = count($this->get('oidModulo'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["txNombreModulo".$i.".required"] = "El campo Nombre en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        