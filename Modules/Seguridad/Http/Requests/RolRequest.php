<?php

        namespace Modules\Seguridad\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class RolRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoRol" => "required|string|unique:seg_rol,txCodigoRol,".$this->get('oidRol') .",oidRol",
						"txNombreRol" => "required|string",
						"txEstadoRol" => "required|string",

                );

                $reg = count($this->get('oidRolOpcion'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('Opcion_oidRolOpcion')[$i]) == '' )
                                $validacion['Opcion_oidRolOpcion'.$i] =  'required';

					if(trim($this->get('chAdicionarRolOpcion')[$i]) == '' )
                                $validacion['chAdicionarRolOpcion'.$i] =  'required';

					if(trim($this->get('chModificarRolOpcion')[$i]) == '' )
                                $validacion['chModificarRolOpcion'.$i] =  'required';

					if(trim($this->get('chEliminarRolOpcion')[$i]) == '' )
                                $validacion['chEliminarRolOpcion'.$i] =  'required';

					if(trim($this->get('chConsultarRolOpcion')[$i]) == '' )
                                $validacion['chConsultarRolOpcion'.$i] =  'required';

					if(trim($this->get('chInactivarRolOpcion')[$i]) == '' )
                                $validacion['chInactivarRolOpcion'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoRol.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoRol.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoRol.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreRol.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreRol.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoRol.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoRol.required"] =  "El campo Estado es obligatorio";


                $reg = count($this->get('oidRolOpcion'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["Opcion_oidRolOpcion".$i.".required"] = "El campo Opción en la línea ".($i + 1)." es obligatorio";
				$mensaje["chAdicionarRolOpcion".$i.".required"] = "El campo Adicionar en la línea ".($i + 1)." es obligatorio";
				$mensaje["chModificarRolOpcion".$i.".required"] = "El campo Modificar en la línea ".($i + 1)." es obligatorio";
				$mensaje["chEliminarRolOpcion".$i.".required"] = "El campo Eliminar en la línea ".($i + 1)." es obligatorio";
				$mensaje["chConsultarRolOpcion".$i.".required"] = "El campo Consultar en la línea ".($i + 1)." es obligatorio";
				$mensaje["chInactivarRolOpcion".$i.".required"] = "El campo Inactivar en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        