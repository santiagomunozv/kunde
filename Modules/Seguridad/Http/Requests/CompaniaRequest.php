<?php

        namespace Modules\Seguridad\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class CompaniaRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                "txCodigoCompania" => "required|string|unique:seg_compania,txCodigoCompania,".$this->get('oidCompania') .",oidCompania",
						"txNombreCompania" => "required|string",
						"txEstadoCompania" => "required|string",

                );

                $reg = count($this->get('oidCompaniaTercero'));
                for($i = 0; $i < $reg; $i++)
                {

					if(trim($this->get('Tercero_oidCompaniaTercero')[$i]) == '' )
                                $validacion['Tercero_oidCompaniaTercero'.$i] =  'required';

					if(trim($this->get('lsCodificacionCompaniaTercero')[$i]) == '' )
                                $validacion['lsCodificacionCompaniaTercero'.$i] =  'required';
				}

                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                				$mensaje["txCodigoCompania.string"] = "El campo Código Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txCodigoCompania.required"] =  "El campo Código es obligatorio";
				$mensaje["txCodigoCompania.unique"] =  "El valor ingresado en el campo Código ya existe, éste debe ser único";
				$mensaje["txNombreCompania.string"] = "El campo Nombre Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txNombreCompania.required"] =  "El campo Nombre es obligatorio";
				$mensaje["txEstadoCompania.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoCompania.required"] =  "El campo Estado es obligatorio";


                $reg = count($this->get('oidCompaniaTercero'));
                    for($i = 0; $i < $reg; $i++)
                    {
				$mensaje["Tercero_oidCompaniaTercero".$i.".required"] = "El campo Asociado en la línea ".($i + 1)." es obligatorio";
				$mensaje["lsCodificacionCompaniaTercero".$i.".required"] = "El campo Codificación en la línea ".($i + 1)." es obligatorio";
				}

                return $mensaje;
            }
        }
        