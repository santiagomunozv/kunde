<?php

        namespace Modules\Seguridad\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class UsuarioRequest extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                $validacion = array(
                		"txLoginUsuario" => "required|string",
						"password" => "required|string",
						"txEstadoUsuario" => "required|string",

                );
                if($this->get('oidUsuarioCompaniaRol')){
                    $reg = count($this->get('oidUsuarioCompaniaRol'));
                    for($i = 0; $i < $reg; $i++)
                    {
                        if(trim($this->get('Compania_oidUsuarioCompaniaRol')[$i]) == '' )
                            $validacion['Compania_oidUsuarioCompaniaRol'.$i] =  'required';

                        if(trim($this->get('Rol_oidUsuarioCompaniaRol')[$i]) == '' )
                            $validacion['Rol_oidUsuarioCompaniaRol'.$i] =  'required';
                    }
                }
                
                return $validacion;
            }

        

            public function messages()
            {
                $mensaje = array();
                $mensaje["txLoginUsuario.string"] = "El campo Login Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txLoginUsuario.required"] =  "El campo Login es obligatorio";
				$mensaje["password.string"] = "El campo Clave Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["password.required"] =  "El campo Clave es obligatorio";
				$mensaje["dtCambioClaveUsuario.date"] = "El campo Cambio Clave Solo puede contener una fecha en el formato AAAA-MM-DD";
				$mensaje["dtCambioClaveUsuario.required"] =  "El campo Cambio Clave es obligatorio";
				$mensaje["txEstadoUsuario.string"] = "El campo Estado Solo puede contener caracteres afabéticos, númericos, guión y guión bajo";
				$mensaje["txEstadoUsuario.required"] =  "El campo Estado es obligatorio";


                if($this->get('oidUsuarioCompaniaRol')){
                    $reg = count($this->get('oidUsuarioCompaniaRol'));
                    for($i = 0; $i < $reg; $i++)
                    {
                        $mensaje["Compania_oidUsuarioCompaniaRol".$i.".required"] = "El campo Compañía en la línea ".($i + 1)." es obligatorio";
                        $mensaje["Rol_oidUsuarioCompaniaRol".$i.".required"] = "El campo Rol en la línea ".($i + 1)." es obligatorio";
                    }
                }

                return $mensaje;
            }
        }
        