@extends("layouts.principal")
@section("nombreModulo")
    Paquetes del menú
@endsection
@section("scripts")
    {{Html::script("modules/seguridad/js/paqueteForm.js")}}
     
    <script>
        var Modulo = {!! $paquete->Modulo !!};
    </script>
    
@endsection
@section("contenido")
    @if(isset($paquete->oidPaquete))
        {!!Form::model($paquete,["route"=>["paquete.update",$paquete->oidPaquete],"method"=>"PUT", "id"=>"form-paquete" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($paquete,["route"=>["paquete.store",$paquete->oidPaquete],"method"=>"POST", "id"=>"form-paquete", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidPaquete', null, array('id' => 'oidPaquete')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('inOrdenPaquete', 'Orden', array('class' => 'control-label required ')) !!}
                    {!!Form::text('inOrdenPaquete',null,['class'=>'text-right form-control','placeholder'=>'Ingresa Orden'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombrePaquete', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombrePaquete',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txIconoPaquete', 'Icono', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txIconoPaquete',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Icono'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Módulos</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <div class="table-responsive">
                                <input type="hidden" name="eliminarModulo" id="eliminarModulo"/>
                                <table class="table table-hover table-borderless table-sm">
                                    <thead class="bg-primary text-light">
                                        <tr>
                                            <th>
                                                <button class="btn btn-primary btn-sm" onclick="seg_modulo.agregarCampos(valorModulo, 'A');">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </th>
                                            <th class="requiredMulti" style="width: 25%;" >Nombre</th><th class="" style="width: 62%;" >Icono</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenedor_seg_modulo"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($paquete->oidPaquete))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        