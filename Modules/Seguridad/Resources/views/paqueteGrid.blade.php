@extends('layouts.principal')
@section('nombreModulo')
    Paquetes del menú
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("paquete-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="paquete-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/seguridad/paquete', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Orden</th>
                            <th>Nombre</th>
                            <th>Icono</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($paquete as $paquetereg)
                            <tr class="{{ $paquetereg->txEstadoPaquete == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/seguridad/paquete', [$paquetereg->oidPaquete, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $paquetereg->oidPaquete }}', 'seg_paquete', 'Paquete')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $paquetereg->oidPaquete }}', 'seg_paquete', 'Paquete','{{ $paquetereg->txEstadoPaquete }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        {{--
                                    @if ($permisos['chConsultarRolOpcion'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to('/seguridad/paquete' , [$paquetereg->oidPaquete])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                    --}}
                                    </div>
                                </td>
                                <td>{{ $paquetereg->oidPaquete }}</td>
                                <td>{{ $paquetereg->inOrdenPaquete }}</td>
                                <td>{{ $paquetereg->txNombrePaquete }}</td>
                                <td>{{ $paquetereg->txIconoPaquete }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Orden</th>
                            <th>Nombre</th>
                            <th>Icono</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
