@extends("layouts.principal")
@section("nombreModulo")
    Auditing
@stop
@section("scripts")
    <script type="text/javascript">
        $(function(){
            configurarGrid("auditing-table");
        });
    </script>
@endsection
@section('estilos')
 <style>
     #auditing-table tbody td{
        white-space: nowrap;
     }
 </style>   
@endsection
@section("contenido")
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div class="table-responsive">
            <table id="auditing-table" class="table table-hover table-sm scalia-grid">
                <thead class="bg-primary text-light">
                    <tr>
                        <th style="width: 150px;" data-orderable="false">
                            <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light" >
                                <i class="fas fa-broom"></i>
                            </button>
                            <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                        </th>
                        <th>Modulo</th>
                        <th>Evento</th>
						<th>Usuario</th>
						<th>Id Modulo</th>
						<th>Valor Anterior</th>
						<th>Valor Nuevo</th>
						<th>Fecha Transacción</th>
		
                    </tr>
                </thead>
                <tbody>
                    @foreach($auditing as $valor)
                        <tr>
                            <td></td>
                            <td>{{$valor->Modulo}}</td>
                            <td>{{$valor->Evento}}</td>
							<td>{{$valor->Usuario}}</td>
							<td>{{$valor->IdModulo}}</td>
							<td>{{$valor->ValorAnterior}}</td>
							<td>{{$valor->ValorNuevo}}</td>
							<td>{{$valor->FechaTransaccion}}</td>
							
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Modulo</th>
                        <th>Evento</th>
						<th>Usuario</th>
						<th>Id Modulo</th>
						<th>Valor Anterior</th>
						<th>Valor Nuevo</th>
						<th>Fecha Transacción</th>
						
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection