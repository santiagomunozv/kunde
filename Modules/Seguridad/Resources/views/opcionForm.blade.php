@extends("layouts.principal")
@section('nombreModulo')
    Crear Opciones
@endsection
@section('scripts')
    {{ Html::script('modules/seguridad/js/opcionForm.js') }}


@endsection
@section('contenido')
    @if (isset($option->oidOpcion))
        {!! Form::model($option, ['route' => ['opcion.update', $option->oidOpcion], 'method' => 'PUT', 'id' => 'form-opcion', 'onsubmit' => 'return false;']) !!}
    @else
        {!! Form::model($option, ['route' => ['opcion.store', $option->oidOpcion], 'method' => 'POST', 'id' => 'form-opcion', 'onsubmit' => 'return false;']) !!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!! Form::hidden('oidOpcion', null, ['id' => 'oidOpcion']) !!}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txNombreOpcion', 'Nombre', ['class' => 'text-md text-primary mb-1 required ']) !!} 
                        {!! Form::text('txNombreOpcion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa Nombre']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('txRutaOpcion', 'Ruta', ['class' => 'text-md text-primary mb-1 required ']) !!}
                        {!! Form::text('txRutaOpcion', null, ['class' => 'form-control', 'placeholder' => 'Ingresa la Ruta']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                        {!! Form::label('Modulo_oidModulo_1aM', 'Modulo', ['class' => 'control-label  ']) !!}
                        {!! Form::select('Modulo_oidModulo_1aM', $txNombreModulo, null, ['class' => 'form-control', 'style' => 'width:100%', 'placeholder' => 'Selecciona Modulo']) !!}
                    </div>
                </div>
            </div>


            @if (isset($option->oidoption))
                {!! Form::button('Modificar', ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'grabar()']) !!}
            @else
                {!! Form::button('Adicionar', ['type' => 'button', 'class' => 'btn btn-success', 'onclick' => 'grabar()']) !!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
