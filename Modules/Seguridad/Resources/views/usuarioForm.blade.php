@extends("layouts.principal")
@section("nombreModulo")
    Usuarios
@endsection
@section("scripts")
    {{Html::script("modules/seguridad/js/usuarioForm.js")}}
     
    <script>
        var UsuarioCompaniaRol = {!! $usuario->UsuarioCompaniaRol !!}
        var oidCompanialista = '<?php echo isset($oidCompanialista) ? $oidCompanialista : "";?>';
        var txNombreCompanialista = '<?php echo isset($txNombreCompanialista) ? $txNombreCompanialista : "";?>';
        var Compania_oidUsuarioCompaniaRollista = [JSON.parse(oidCompanialista), JSON.parse(txNombreCompanialista)];var oidRollista = '<?php echo isset($oidRollista) ? $oidRollista : "";?>';
        var txNombreRollista = '<?php echo isset($txNombreRollista) ? $txNombreRollista : "";?>';
        var Rol_oidUsuarioCompaniaRollista = [JSON.parse(oidRollista), JSON.parse(txNombreRollista)];
    </script>
@endsection
@section("contenido")
    @if(isset($usuario->oidUsuario))
        {!!Form::model($usuario,["route"=>["usuario.update",$usuario->oidUsuario],"method"=>"PUT", "id"=>"form-usuario" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($usuario,["route"=>["usuario.store",$usuario->oidUsuario],"method"=>"POST", "id"=>"form-usuario", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidUsuario', null, array('id' => 'oidUsuario')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    {!!Form::label('txLoginUsuario', 'Login', array('class' => 'control-label required')) !!}
                    {!!Form::text('txLoginUsuario',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Login','autocomplete'=>'off'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                    {!!Form::label('password', 'Clave', array('class' => 'control-label')) !!}
                    {!!Form::password('password',['class'=>'form-control','placeholder'=>'Ingresa Clave','autocomplete'=>'off'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    {!!Form::label('dtCambioClaveUsuario', 'Cambio Clave', array('class' => 'control-label')) !!}
                    {!!Form::text('dtCambioClaveUsuario',null,["readonly" => "readonly",'class'=>'form-control','autocomplete'=>'off'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                    {!!Form::label('Tercero_oidUsuario', 'Tercero', array('class' => 'control-label  ')) !!}
                    {!!Form::select('Tercero_oidUsuario',$asn_tercerolista,null,['class'=>'form-control','style'=>'width:100%','placeholder'=>'Selecciona Tercero'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    {!!Form::label('txEstadoUsuario', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoUsuario',null,["readonly" => "readonly", 'class'=>'form-control'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Permisos por compañía</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <div class="table-responsive" style="min-height: 400px;">
                                <input type="hidden" name="eliminarUsuarioCompaniaRol" id="eliminarUsuarioCompaniaRol"/>
                                <table class="table table-hover table-borderless table-sm">
                                    <thead class="bg-primary text-light">
                                        <tr>
                                            <th>
                                                <button class="btn btn-primary btn-sm" onclick="seg_usuariocompaniarol.agregarCampos(valorUsuarioCompaniaRol, 'A');">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </th>
                                            <th class="requiredMulti" style="width: 33%;" >Compañía</th><th class="requiredMulti" style="width: 33%;" >Rol</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenedor_seg_usuariocompaniarol"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($usuario->oidUsuario))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        