@extends("layouts.principal")
@section("nombreModulo")
    Módulos
@endsection
@section("scripts")
    {{Html::script("modules/seguridad/js/moduloForm.js")}}
     
    <script>
        var Opcion = {!! $modulo->Opcion !!};
    </script>
    
@endsection
@section("contenido")
    @if(isset($modulo->oidModulo))
        {!!Form::model($modulo,["route"=>["modulo.update",$modulo->oidModulo],"method"=>"PUT", "id"=>"form-modulo" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($modulo,["route"=>["modulo.store",$modulo->oidModulo],"method"=>"POST", "id"=>"form-modulo", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidModulo', null, array('id' => 'oidModulo')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreModulo', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreModulo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txIconoModulo', 'Icono', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txIconoModulo',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Icono'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Opciones</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <div class="table-responsive">
                                <input type="hidden" name="eliminarOpcion" id="eliminarOpcion"/>
                                <table class="table table-hover table-borderless table-sm">
                                    <thead class="bg-primary text-light">
                                        <tr>
                                            <th>
                                                <button class="btn btn-primary btn-sm" onclick="seg_opcion.agregarCampos(valorOpcion, 'A');">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </th>
                                            <th class="requiredMulti" style="width: 21%;" >Nombre</th><th class="requiredMulti" style="width: 35%;" >Ruta</th><th class="" style="width: 35%;" >Icono</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenedor_seg_opcion"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($modulo->oidModulo))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        