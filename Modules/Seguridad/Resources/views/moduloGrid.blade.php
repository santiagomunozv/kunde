@extends("layouts.principal")
@section("nombreModulo")
    Módulos
@stop
@section("scripts")
    <script type="text/javascript">
        $(function(){
            configurarDataTable("modulo-table");
        });
    </script>
@endsection
@section("contenido")
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div class="table-responsive">
            <table id="modulo-table" class="table table-hover table-sm scalia-grid">
                <thead class="bg-primary text-light">
                    <tr>
                        <th style="width: 150px;" data-orderable="false">
                            @if($permisos["chAdicionarRolOpcion"])
                            <a class="btn btn-primary btn-sm text-light" href="{!!URL::to('/seguridad/modulo' , ['create'])!!}">
                                <i class="fa fa-plus"></i>
                            </a>
                            @endif
                            <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light" >
                                <i class="fas fa-broom"></i>
                            </button>
                            <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                        </th>
                        <th>Id</th>
						<th>Nombre</th>
						<th>Icono</th>
						
                    </tr>
                </thead>
                <tbody>
                    @foreach($modulo as $moduloreg)
                        <tr class="{{$moduloreg->txEstadoModulo == 'Anulado' ? 'text-danger': '' }}">
                            <td>
                                <div class="btn-group" role="group" aria-label="Acciones">
                                    @if($permisos['chModificarRolOpcion'])
                                        <a class="btn btn-success btn-sm" href="{!!URL::to('/seguridad/modulo' , [$moduloreg->oidModulo , 'edit'])!!}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @endif
                                    @if($permisos['chEliminarRolOpcion'])
                                        <button type="button" onclick="confirmarEliminacion('{{$moduloreg->oidModulo}}', 'seg_modulo', 'Modulo')" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    @endif
                                    @if($permisos['chModificarRolOpcion'])
                                        <button class="btn btn-warning btn-sm" onclick="cambiarEstado('{{$moduloreg->oidModulo}}', 'seg_modulo', 'Modulo','{{$moduloreg->txEstadoModulo}}')">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    @endif
                                    @if($permisos['chConsultarRolOpcion'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to('/seguridad/modulo' , [$moduloreg->oidModulo])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                            <td>{{$moduloreg->oidModulo}}</td>
							<td>{{$moduloreg->txNombreModulo}}</td>
							<td>{{$moduloreg->txIconoModulo}}</td>
							
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Id</th>
						<th>Nombre</th>
						<th>Icono</th>
						
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection