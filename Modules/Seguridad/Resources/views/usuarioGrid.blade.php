@extends('layouts.principal')
@section('nombreModulo')
    Usuarios
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("usuario-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="usuario-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/seguridad/usuario', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Id</th>
                            <th>Login</th>
                            <th>Cambio Clave</th>
                            <th>Asociados de Negocio</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($usuario as $usuarioreg)
                            <tr class="{{ $usuarioreg->txEstadoUsuario == 'Anulado' ? 'text-danger' : '' }}">
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/seguridad/usuario', [$usuarioreg->oidUsuario, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $usuarioreg->oidUsuario }}', 'seg_usuario', 'Usuario')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                        @if ($permisos['chModificarRolOpcion'])
                                            <button class="btn btn-warning btn-sm"
                                                onclick="cambiarEstado('{{ $usuarioreg->oidUsuario }}', 'seg_usuario', 'Usuario','{{ $usuarioreg->txEstadoUsuario }}')"
                                                title="Estados">
                                                <i class="fas fa-power-off"></i>
                                            </button>
                                        @endif
                                        {{--
                                    @if ($permisos['chConsultarRolOpcion'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to('/seguridad/usuario' , [$usuarioreg->oidUsuario])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                    --}}
                                    </div>
                                </td>
                                <td>{{ $usuarioreg->oidUsuario }}</td>
                                <td>{{ $usuarioreg->txLoginUsuario }}</td>
                                <td>{{ $usuarioreg->dtCambioClaveUsuario }}</td>
                                <td>{{ $usuarioreg->tercero->txNombreTercero ?? '' }}</td>
                                <td>{{ $usuarioreg->txEstadoUsuario }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Id</th>
                            <th>Login</th>
                            <th>Cambio Clave</th>
                            <th>Asociados de Negocio</th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
