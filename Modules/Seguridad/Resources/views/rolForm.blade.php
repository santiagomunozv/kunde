@extends("layouts.principal")
@section("nombreModulo")
    Roles
@endsection
@section("scripts")
    {{Html::script("modules/seguridad/js/rolForm.js")}}
     
    <script>
        var RolOpcion = {!! $rol->RolOpcion !!};
    
        var oidOpcionlista = '<?php echo isset($oidOpcionlista) ? $oidOpcionlista : "";?>';
                            var txNombreOpcionlista = '<?php echo isset($txNombreOpcionlista) ? $txNombreOpcionlista : "";?>';
                            var Opcion_oidRolOpcionlista = [JSON.parse(oidOpcionlista), JSON.parse(txNombreOpcionlista)];
    </script>
@endsection
@section("contenido")
    @if(isset($rol->oidRol))
        {!!Form::model($rol,["route"=>["rol.update",$rol->oidRol],"method"=>"PUT", "id"=>"form-rol" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($rol,["route"=>["rol.store",$rol->oidRol],"method"=>"POST", "id"=>"form-rol", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidRol', null, array('id' => 'oidRol')) !!}
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txCodigoRol', 'Código', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txCodigoRol',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txNombreRol', 'Nombre', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txNombreRol',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-12">
                    <div class="form-group ">
                    {!!Form::label('txEstadoRol', 'Estado', array('class' => 'text-md text-primary mb-1 required ')) !!}
                    {!!Form::text('txEstadoRol',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Asignación de permisos</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <input type="hidden" name="eliminarRolOpcion" id="eliminarRolOpcion"/>
                            <table class="table table-hover table-borderless table-sm">
                                <thead class="bg-primary text-light">
                                    <tr>
                                        <th>
                                            <button class="btn btn-primary btn-sm" onclick="seg_rolopcion.agregarCampos(valorRolOpcion, 'A');">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </th>
                                        <th class="required " style="width: 15%;" >Opción</th><th class="required text-center" style="width: 14%;" >Adicionar</th><th class="required text-center" style="width: 14%;" >Modificar</th><th class="required text-center" style="width: 14%;" >Eliminar</th><th class="required text-center" style="width: 14%;" >Consultar</th><th class="required text-center" style="width: 14%;" >Inactivar</th>
                                    </tr>
                                </thead>
                                <tbody id="contenedor_seg_rolopcion"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($rol->oidRol))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        