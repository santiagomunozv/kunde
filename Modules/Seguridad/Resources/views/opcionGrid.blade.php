@extends('layouts.principal')
@section('nombreModulo')
    Opciones
@stop
@section('scripts')
    <script type="text/javascript">
        $(function() {
            configurarDataTable("opcion-table");
        });
    </script>
@endsection
@section('contenido')
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="table-responsive">
                <table id="opcion-table" class="table table-hover table-sm scalia-grid">
                    <thead class="bg-primary text-light">
                        <tr>
                            <th style="width: 150px;" data-orderable="false">
                                @if ($permisos['chAdicionarRolOpcion'])
                                    <a class="btn btn-primary btn-sm text-light" href="{!! URL::to('/seguridad/opcion', ['create']) !!}">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-broom"></i>
                                </button>
                                <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                    <i class="fas fa-redo-alt"></i>
                                </button>
                            </th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Ruta</th>
                            <th>Modulo</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($option as $optionreg)
                            <tr>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Acciones">
                                        @if ($permisos['chModificarRolOpcion'])
                                            <a class="btn btn-success btn-sm" href="{!! URL::to('/seguridad/opcion', [$optionreg->oidOpcion, 'edit']) !!}"
                                                title="Modificar">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                        @endif
                                        @if ($permisos['chEliminarRolOpcion'])
                                            <button type="button"
                                                onclick="confirmarEliminacion('{{ $optionreg->oidOpcion }}', 'seg_opcion', 'Option')"
                                                class="btn btn-danger btn-sm" title="Eliminar">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $optionreg->oidOpcion }}</td>
                                <td>{{ $optionreg->txNombreOpcion }}</td>
                                <td>{{ $optionreg->txRutaOpcion }}</td>
                                <td>{{ $optionreg->txNombreModulo }}</td>

                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>Ruta</th>
                            <th>Modulo</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
