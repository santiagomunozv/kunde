@extends("layouts.principal")
@section("nombreModulo")
    Compañías
@endsection
@section("scripts")
    {{Html::script("modules/seguridad/js/companiaForm.js")}}
     
    <script>
        var CompaniaTercero = {!! $compania->CompaniaTercero !!};
    </script>
    var oidTercerolista = '<?php echo isset($oidTercerolista) ? $oidTercerolista : "";?>';
                            var txNombreTercerolista = '<?php echo isset($txNombreTercerolista) ? $txNombreTercerolista : "";?>';
                            var Tercero_oidCompaniaTercerolista = [JSON.parse(oidTercerolista), JSON.parse(txNombreTercerolista)];
@endsection
@section("contenido")
    @if(isset($compania->oidCompania))
        {!!Form::model($compania,["route"=>["compania.update",$compania->oidCompania],"method"=>"PUT", "id"=>"form-compania" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($compania,["route"=>["compania.store",$compania->oidCompania],"method"=>"POST", "id"=>"form-compania", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            {!!Form::hidden('oidCompania', null, array('id' => 'oidCompania')) !!}
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txCodigoCompania', 'Código', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txCodigoCompania',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Código'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txNombreCompania', 'Nombre', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txNombreCompania',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Nombre'])!!}
                    </div>
                </div>
			</div>
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txBaseDatosCompania', 'Base de datos', array('class' => 'control-label  ')) !!}
                    {!!Form::text('txBaseDatosCompania',null,[ 'class'=>'form-control','placeholder'=>'Ingresa Base de datos'])!!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group ">
                    {!!Form::label('txEstadoCompania', 'Estado', array('class' => 'control-label required ')) !!}
                    {!!Form::text('txEstadoCompania',null,["readonly" => "readonly", 'class'=>'form-control','placeholder'=>'Ingresa Estado'])!!}
                    </div>
                </div>
			</div>
            <h1 class="h3 mb-2 text-gray-800">Configuración de Asociados de Negocio</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <div class="table-responsive">
                                <input type="hidden" name="eliminarCompaniaTercero" id="eliminarCompaniaTercero"/>
                                <table class="table table-hover table-borderless table-sm">
                                    <thead class="bg-primary text-light">
                                        <tr>
                                            <th>
                                                <button class="btn btn-primary btn-sm" onclick="seg_companiatercero.agregarCampos(valorCompaniaTercero, 'A');">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </th>
                                            <th class="requiredMulti" style="width: 34%;" >Asociado</th><th class="requiredMulti" style="width: 31%;" >Codificación</th>
                                        </tr>
                                    </thead>
                                    <tbody id="contenedor_seg_companiatercero"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if(isset($compania->oidCompania))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        