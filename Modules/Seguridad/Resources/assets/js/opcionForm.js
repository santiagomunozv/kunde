"use strict";
var errorClass = "is-invalid";

$(function () {
  var config = {
    ".chosen-select": {},
    ".chosen-select-deselect": { allow_single_deselect: true },
    ".chosen-select-no-single": { disable_search_threshold: 10 },
    ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
    ".chosen-select-width": { width: "100%" },
  };

  for (let selector in config) {
    $(selector).chosen(config[selector]);
  }
});

function grabar() {
  modal.cargando();
  let mensajes = validarForm();
  if (mensajes && mensajes.length) {
    modal.mostrarErrores(mensajes);
  } else {
    let formularioId = "#form-opcion";
    let route = $(formularioId).attr("action");
    let data = $(formularioId).serialize();
    $.post(
      route,
      data,
      function (resp) {
        modal.establecerAccionCerrar(function () {
          location.href = "/seguridad/opcion";
        });
        modal.mostrarModal(
          "Informacion",
          '<div class="alert alert-success">Los datos han sido guardados correctamente</div>'
        );
      },
      "json"
    ).fail(function (resp) {
      $.each(resp.responseJSON.errors, function (index, value) {
        mensajes.push(value);
        $("#" + index).addClass(errorClass);
      });
      modal.mostrarErrores(mensajes);
    });
  }

  function validarForm() {
    let mensajes = [];
    let txNombreOpcion_Input = $("#txNombreOpcion");
    let txNombreOpcion_AE = txNombreOpcion_Input.val();
    txNombreOpcion_Input.removeClass(errorClass);
    if (!txNombreOpcion_AE) {
      txNombreOpcion_Input.addClass(errorClass);
      mensajes.push("El campo Nombre es obligatorio");
    }
    let txRutaOpcion_Input = $("#txRutaOpcion");
    let txRutaOpcion_AE = txRutaOpcion_Input.val();
    txRutaOpcion_Input.removeClass(errorClass);
    if (!txRutaOpcion_AE) {
      txRutaOpcion_Input.addClass(errorClass);
      mensajes.push("El campo Ruta es obligatorio");
    }
    let Modulo_oidModulo_1aM_Input = $("#Modulo_oidModulo_1aM");
    let Modulo_oidModulo_1aM_AE = Modulo_oidModulo_1aM_Input.val();
    Modulo_oidModulo_1aM_Input.removeClass(errorClass);
    if (!Modulo_oidModulo_1aM_AE) {
      Modulo_oidModulo_1aM_Input.addClass(errorClass);
      mensajes.push("El campo Modulo es obligatorio");
    }

    return mensajes;
  }
}
