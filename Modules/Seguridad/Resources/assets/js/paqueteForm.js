"use strict";
let errorClass = "is-invalid";

var valorModulo = ['0','','',];
var seg_modulo;
$(function(){

    seg_modulo = new GeneradorMultiRegistro('seg_modulo','contenedor_seg_modulo','seg_modulo_');

    seg_modulo.altura = '35px;';
    seg_modulo.campoid = 'oidModulo';
    seg_modulo.campoEliminacion = 'eliminarModulo';
    seg_modulo.botonEliminacion = true;
    seg_modulo.funcionEliminacion = '';

    seg_modulo.campos = ['oidModulo','txNombreModulo','txIconoModulo',];

    seg_modulo.etiqueta = ['input','input','input',];
    seg_modulo.tipo = ['hidden','text','text',];
    seg_modulo.estilo = ['','','',];
    seg_modulo.clase = ['text-right ','','',];
    seg_modulo.sololectura = [true,false,false,];

    seg_modulo.opciones = ['','','',];

    seg_modulo.funciones = ['','','',];
    seg_modulo.otrosAtributos = ['','','',];

    Modulo.forEach(Moduloreg => {
        seg_modulo.agregarCampos(Moduloreg,'L');
    });

});


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-paquete";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/seguridad/paquete";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        let inOrdenPaquete_Input = $("#inOrdenPaquete");
        let inOrdenPaquete_AE = inOrdenPaquete_Input.val();
        inOrdenPaquete_Input.removeClass(errorClass);
        if(!inOrdenPaquete_AE){
            inOrdenPaquete_Input.addClass(errorClass)
            mensajes.push("El campo Orden es obligatorio");
        }
		let txNombrePaquete_Input = $("#txNombrePaquete");
        let txNombrePaquete_AE = txNombrePaquete_Input.val();
        txNombrePaquete_Input.removeClass(errorClass);
        if(!txNombrePaquete_AE){
            txNombrePaquete_Input.addClass(errorClass)
            mensajes.push("El campo Nombre es obligatorio");
        }
		return mensajes;
    }
}