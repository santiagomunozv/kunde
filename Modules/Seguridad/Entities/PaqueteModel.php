<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class PaqueteModel extends Model
{
    protected $table = 'seg_paquete';
    protected $primaryKey = 'oidPaquete';
    protected $fillable = ['inOrdenPaquete','txNombrePaquete','txIconoPaquete',];
    public $timestamps = false;

	public function Modulo()
    {
        return $this->hasMany('\Modules\Seguridad\Entities\ModuloModel','Paquete_oidPaquete_1aM','oidPaquete');
    }

}