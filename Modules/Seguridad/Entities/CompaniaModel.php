<?php
namespace Modules\Seguridad\Entities;

use App\AuthService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Modules\FichaTecnica\Exceptions\FichaException;

class CompaniaModel extends Model
{
    protected $table = 'seg_compania';
    protected $primaryKey = 'oidCompania';
    protected $fillable = ['txCodigoCompania','txNombreCompania','txBaseDatosCompania','txEstadoCompania'];
    public $timestamps = false;

	public function CompaniaTercero(){
        return $this->hasMany('\Modules\Seguridad\Entities\CompaniaTerceroModel','Compania_oidCompania_1aM','oidCompania');
    }

    public static function listSelect($id = 0){
        return self::select('oidCompania','txNombreCompania')
        ->where('txEstadoCompania','Activo')
        ->orWhere('oidCompania',$id)
        ->pluck('txNombreCompania','oidCompania');
    }

    public static function getConnectionById($id){
        $compania = self::find($id)->txBaseDatosCompania ?? null;

        if(!$compania){
            throw new FichaException('No se encontro la base de datos solicitada'); 
        }

        return $compania;
    }

    /**
     * Metodo que retorna la compania en sesión, seleccionada por el usuario
     * actualmente logueado.
     * 
     * @return CompaniaModel la compania en la que el usuario actualmente está logueado
     */
    public static function getCurrentCompania(){
        return self::find(Session::get(AuthService::$COMPANIA_ID));
    }

}