<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class RolModel extends Model
{
    protected $table = 'seg_rol';
    protected $primaryKey = 'oidRol';
    protected $fillable = ['txCodigoRol','txNombreRol','txEstadoRol',];
    public $timestamps = false;

	public function RolOpcion()
    {
        return $this->hasMany('\Modules\Seguridad\Entities\RolOpcionModel','Rol_oidRol_1aM','oidRol');
    }

}