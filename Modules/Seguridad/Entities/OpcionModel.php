<?php

namespace Modules\Seguridad\Entities;

use Illuminate\Database\Eloquent\Model;

class OpcionModel extends Model
{
    protected $table = 'seg_opcion';
    protected $primaryKey = 'oidOpcion';
    protected $fillable = ['Modulo_oidModulo_1aM', 'txNombreOpcion', 'txRutaOpcion', 'txIconoOpcion',];
    public $timestamps = false;

    // public function modulo()
    // {
    //     return $this->hasOne('\Modules\Seguridad\Entities\ModuloModel', 'oidModulo', 'Modulo_oidOpcion');
    // }

    public function modulo(){
        return $this->belongsTo('\Modules\Seguridad\Entities\ModuloModel' , 'Modulo_oidModulo_1aM');
    }
}
