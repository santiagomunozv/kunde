<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class CompaniaTerceroModel extends Model
{
    protected $table = 'seg_companiatercero';
    protected $primaryKey = 'oidCompaniaTercero';
    protected $fillable = ['Compania_oidCompania_1aM','Tercero_oidCompaniaTercero','lsCodificacionCompaniaTercero',];
    public $timestamps = false;

	public function tercero()
    {
        return $this->hasOne('\Modules\AsociadoNegocio\Entities\TerceroModel','oidTercero','Tercero_oidCompaniaTercero');
    }

    public static function getCodificacionCompania($idCompania,$idTercero,$codificacion){
        $consulta = self::select('lsCodificacionCompaniaTercero')
        ->where('Compania_oidCompania_1aM','=',$idCompania)->where('Tercero_oidCompaniaTercero','=',$idTercero)->first();

        if(isset($consulta->lsCodificacionCompaniaTercero) && $consulta->lsCodificacionCompaniaTercero){
            return $consulta->lsCodificacionCompaniaTercero;
        }else{
            return $codificacion;
        }
    }
}