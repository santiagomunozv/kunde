<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class RolOpcionModel extends Model
{
    protected $table = 'seg_rolopcion';
    protected $primaryKey = 'oidRolOpcion';
    protected $fillable = ['Rol_oidRol_1aM','Opcion_oidRolOpcion','chAdicionarRolOpcion','chModificarRolOpcion','chEliminarRolOpcion','chConsultarRolOpcion','chInactivarRolOpcion',];
    public $timestamps = false;

	public function opcion()
    {
        return $this->hasOne('\Modules\Seguridad\Entities\OpcionModel','oidOpcion','Opcion_oidRolOpcion');
    }

}