<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class ModuloModel extends Model
{
    protected $table = 'seg_modulo';
    protected $primaryKey = 'oidModulo';
    protected $fillable = ['Paquete_oidPaquete_1aM','txNombreModulo','txIconoModulo',];
    public $timestamps = false;

	public function Opcion()
    {
        return $this->hasMany('\Modules\Seguridad\Entities\OpcionModel','Modulo_oidModulo_1aM','oidModulo');
    }

}