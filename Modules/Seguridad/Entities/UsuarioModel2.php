<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class UsuarioModel extends Model  implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'seg_usuario';
    protected $primaryKey = 'oidUsuario';
    protected $fillable = ['txLoginUsuario','password','dtCambioClaveUsuario','Tercero_oidUsuario','txEstadoUsuario','remember_token',];
    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    
	public function tercero()
    {
        return $this->hasOne('\Modules\AsociadoNegocio\Entities\TerceroModel','oidTercero','Tercero_oidUsuario');
    }

	public function UsuarioCompaniaRol()
    {
        return $this->hasMany('\Modules\Seguridad\Entities\UsuarioCompaniaRolModel','Usuario_oidUsuario_1aM','oidUsuario');
    }

    // $usuario = UsuarioModel::select('oidUsuario','txLoginUsuario','dtCambioClaveUsuario','txNombreTercero','txNombreComercialTercero','txEstadoUsuario')
    //         ->leftjoin('asn_tercero','Tercero_oidUsuario','=','oidTercero')
    //         ->orderBy('oidUsuario' , 'DESC')
    //         ->get();

}