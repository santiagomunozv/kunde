<?php
namespace Modules\Seguridad\Entities;
use Illuminate\Database\Eloquent\Model;

class UsuarioCompaniaRolModel extends Model
{
    protected $table = 'seg_usuariocompaniarol';
    protected $primaryKey = 'oidUsuarioCompaniaRol';
    protected $fillable = ['Usuario_oidUsuario_1aM','Compania_oidUsuarioCompaniaRol','Rol_oidUsuarioCompaniaRol',];
    public $timestamps = false;

	public function compania()
    {
        return $this->hasOne('\Modules\Seguridad\Entities\CompaniaModel','oidCompania','Compania_oidUsuarioCompaniaRol');
    }

	public function rol()
    {
        return $this->hasOne('\Modules\Seguridad\Entities\RolModel','oidRol','Rol_oidUsuarioCompaniaRol');
    }

}