<?php

namespace Modules\MasterDev\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;

class MasterDevController extends Controller
{

    private function consultarModulo($tabla) 
    {
        // primero extraemos el prefijo de la tabla, que es el c´dogio que vamos a buscar en nuestra tabla de módulos
        $codigo = explode('_',$tabla)[0];

        // if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {

        //     $consulta = DB::table($basedatos . '.sys.tables AS tabla')
        //         ->select(DB::raw("'$basedatos' AS TABLE_SCHEMA,
        //                     tabla.name AS TABLE_NAME ,
        //                     columna.name AS COLUMN_NAME "))
        //         ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
        //         ->where('columna.name', "like", $terminacionCampo . '_oid' . $terminacionCampo . '_%')
        //         ->get();
        // } else 
        {
            $consulta = DB::table('mdv_modulo')
                ->select('txCodigoModulo', 'txNombreModulo', 'txFolderModulo')
                ->where('txCodigoModulo', "=", $codigo)
                ->get();
        }

        // si no encontramos el módulo, detenemos el proceso y mostramos el motivo
        if(count($consulta) == 0)
        {    
            echo 'MasterDev tiene una tabla llamada mdv_modulo, en esta se deben registrar los módulo con su prefijo de tabla y folder, por favor adicione el módulo correspondiente para la tabla '.$tabla;
            return '';
        }
        // obtenemos el nombre de la carpeta del módulo
        $modulo = get_object_vars($consulta[0])["txFolderModulo"];
        

        return $modulo;
    }


    private function consultarTablas($basedatos, $terminacionCampo) 
    {
        if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {

            $consulta = DB::table($basedatos . '.sys.tables AS tabla')
                ->select(DB::raw("'$basedatos' AS TABLE_SCHEMA,
                            tabla.name AS TABLE_NAME ,
                            columna.name AS COLUMN_NAME "))
                ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
                ->where('columna.name', "like", $terminacionCampo . '_oid' . $terminacionCampo . '_%')
                ->get();
        } else {
            $consulta = DB::table('information_schema.COLUMNS')
                ->select(DB::raw('TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME'))
                ->where('COLUMN_NAME', "like", $terminacionCampo . '\_oid' . $terminacionCampo . '\_%')
                ->where('TABLE_SCHEMA', "=", $basedatos)
                ->orderby('TABLE_NAME')
                ->get();
echo  $terminacionCampo . '\_oid' . $terminacionCampo . '\_%';
                print_r($consulta);
        }

        return $consulta;
    }

    private function consultarTablaRelacional($bd, $campo)
    {
        // tomamos el nombre de la tabla a consultar (la palabra que esta antes del underline)
        $tablaRelacionada = strtolower(explode('_', $campo)[0]);

        if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {
            // consultamos en la base de datos el nombre de una tabla que contenga el nombre de $tablaRelacionada con un underline Previo
            $datoABuscar = '___[_]' . $tablaRelacionada;

            $consulta = DB::table($bd . '.sys.tables AS tabla')
                ->select(DB::raw("tabla.name AS TABLE_NAME ,
                            columna.name AS COLUMN_NAME "))
                ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
                ->where('tabla.name', "like", $datoABuscar)
                ->get();
        } else {
            // consultamos en la base de datos el nombre de una tabla que contenga el nombre de $tablaRelacionada con un underline Previo
            $datoABuscar = '___\_' . $tablaRelacionada;
            $consulta = DB::table('information_schema.COLUMNS')
                ->select(DB::raw('TABLE_NAME, COLUMN_NAME'))
                ->where('TABLE_NAME', "like", $datoABuscar)
                ->where('TABLE_SCHEMA', "=", $bd)
                ->get();
        }

        if (count($consulta) > 0) {

            $prefijo = ucfirst(explode('_', get_object_vars($consulta[0])["TABLE_NAME"])[0]);

            $tabla = substr(get_object_vars($consulta[0])["COLUMN_NAME"], 3);
            $tablaRelacionada = $prefijo . '_' . $tabla;

        }

        return $tablaRelacionada;
    }

    private function consultarCampos($tabla, $basedatos)
    {
        if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {
            $valorComment = 'MS_Description';
            $consulta = DB::table($basedatos . '.sys.tables AS tabla')
                ->select(DB::raw("'$basedatos' AS TABLE_SCHEMA,
                            tabla.name AS TABLE_NAME,
                            columna.name AS COLUMN_NAME,
                            columna.column_id AS ORDINAL_POSITION,
                            columna.name AS COLUMN_COMMENT,
                            object_definition(default_object_id) AS COLUMN_DEFAULT,
                            CONCAT(tipo.name,'(',IIF(columna.precision = 0, columna.max_length, columna.precision),')') as COLUMN_TYPE,
                            tipo.name AS DATA_TYPE,
                            IIF(columna.is_nullable = 0, 'NO','YES') AS IS_NULLABLE,
                            tabla.name AS TABLE_COMMENT "))
                ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
                ->join('sys.systypes AS tipo', 'columna.system_type_id', '=', 'tipo.xtype')
            //->leftjoin('sys.syscolumns AS syscolumna', 'columna.object_id', '=', 'syscolumna.id')
                ->leftjoin('sys.syscolumns AS syscolumna', function ($join) {
                    $join->on('columna.object_id', '=', 'syscolumna.id')
                        ->on('columna.column_id', '=', 'syscolumna.colid');
                })
                ->leftjoin('sys.syscomments AS comentario', 'comentario.id', '=', 'syscolumna.cdefault')
                ->leftjoin('sys.extended_properties AS datocampo', function ($join) use ($valorComment) {

                    $join->on('tabla.object_id', '=', 'datocampo.major_id')
                        ->on('columna.column_id', '=', 'datocampo.minor_id')
                        ->where('datocampo.name', '=', $valorComment);
                })
                ->leftjoin('sys.extended_properties AS desctabla', function ($join) use ($valorComment) {
                    $join->on('desctabla.major_id', '=', 'tabla.object_id')
                        ->where('desctabla.minor_id', '=', 0)
                        ->where('desctabla.name', '=', $valorComment);
                })
                ->where('tabla.name', "=", $tabla)
                ->orderby('columna.column_id')
                ->get();
        } else {
            $consulta = DB::table('information_schema.COLUMNS')
                ->select(DB::raw('COLUMNS.TABLE_SCHEMA, COLUMNS.TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, COLUMN_TYPE, COLUMN_COMMENT, COLUMN_DEFAULT, TABLE_COMMENT, IS_NULLABLE'))
                ->leftjoin('information_schema.TABLES', function ($join) {
                    $join->on('COLUMNS.TABLE_NAME', '=', 'TABLES.TABLE_NAME')
                        ->on('COLUMNS.TABLE_SCHEMA', '=', 'TABLES.TABLE_SCHEMA');
                })
                ->where('COLUMNS.TABLE_NAME', "=", $tabla)
                ->where('COLUMNS.TABLE_SCHEMA', "=", $basedatos)
                ->orderby('COLUMNS.TABLE_NAME')
                ->get();

        }

        return $consulta;
    }

    private function consultarNombreCampo($tablaRelacionada, $basedatos)
    {
        if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {
            $consulta = DB::table($basedatos . '.sys.tables AS tabla')
                ->select(DB::raw("columna.name AS COLUMN_NAME "))
                ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
                ->where('tabla.name', "=", $tablaRelacionada)
                ->get();
        } else {
            $consulta = DB::table('information_schema.COLUMNS')
                ->select(DB::raw('COLUMN_NAME'))
                ->where('TABLE_NAME', "=", $tablaRelacionada)
                ->where('TABLE_SCHEMA', "=", $basedatos)
                ->get();

        }
        return $consulta;
    }

    private function consultarCampoRelacionado($tablaRelacionada, $basedatos, $nombreMaestro)
    {
        
        if (ENV('DB_MOTOR', 'mysql') == 'sqlserver') {
            $valorComment = 'MS_Description';
            $consulta = DB::table($basedatos . '.sys.tables AS tabla')
                ->select(DB::raw("'$basedatos' AS TABLE_SCHEMA,
                        tabla.name AS TABLE_NAME ,
                        columna.name AS COLUMN_NAME ,
                        columna.name AS COLUMN_COMMENT,
                        tabla.name AS  TABLE_COMMENT"))
                ->join('sys.columns AS columna', 'tabla.object_id', '=', 'columna.object_id')
                ->join('sys.systypes AS tipo', 'columna.system_type_id', '=', 'tipo.xtype')
                ->leftjoin('sys.extended_properties AS metadato', function ($join) use ($valorComment) {
                    $join->on('tabla.object_id', '=', 'metadato.major_id')
                        ->on('columna.column_id', '=', 'metadato.minor_id')
                        ->where('metadato.name', '=', $valorComment);
                })
                ->leftjoin('sys.extended_properties AS desctabla', function ($join) use ($valorComment) {
                    $join->on('desctabla.major_id', '=', 'tabla.object_id')
                        ->where('desctabla.minor_id', '=', 0)
                        ->where('desctabla.name', '=', $valorComment);
                })
                ->where('tabla.name', "=", $tablaRelacionada)
                ->where(function ($query) use($nombreMaestro) {
                    $query->where('columna.name', 'like', 'oid%')
                        ->orWhere('columna.name', 'like', 'txNombre'.$nombreMaestro);
                })
                ->orderby('columna.name', 'desc')
                ->get();
        } else {
            $consulta = DB::table('information_schema.COLUMNS')
                ->leftjoin('information_schema.TABLES', function ($join) {
                    $join->on('COLUMNS.TABLE_NAME', '=', 'TABLES.TABLE_NAME')
                        ->on('COLUMNS.TABLE_SCHEMA', '=', 'TABLES.TABLE_SCHEMA');
                })
                ->select(DB::raw('COLUMNS.TABLE_NAME, COLUMN_NAME, COLUMN_COMMENT, TABLE_COMMENT'))
                ->where('COLUMNS.TABLE_NAME', "=", $tablaRelacionada)
                ->where('COLUMNS.TABLE_SCHEMA', "=", $basedatos)
                ->where(function ($query) use($nombreMaestro) {
                    $query->where('COLUMN_NAME', 'like', 'oid%')
                        ->orWhere('COLUMN_NAME', 'like', 'txNombre'.$nombreMaestro);
                })
                ->orderby('COLUMN_NAME', 'desc')
                ->get();
        }
        return $consulta;
    }

    public function generarFormulario($basedatos, $tabla, $estilo)
    {
        // dd($basedatos);
        // consultamos en que modulo debe ser ubicado el formulario a generar
        // MasterDev tiene una tabla llamada mdv_modulo, en esta se deben registrar los módulo con su prefijo de tabla y folder
        $modulo = $this->consultarModulo($tabla) ;
        if($modulo == '')
            return;
        

        $consulta = $this->consultarCampos($tabla, $basedatos);
        $camposBD = array();
        for ($i = 0; $i < count($consulta); $i++) {
            $camposBD[] = get_object_vars($consulta[$i]);
        }
        $this->generarModelo($tabla, $camposBD, $modulo);
        $this->generarControlador($tabla, $camposBD, $modulo);
        $this->generarGrid($tabla, $camposBD, $modulo);
        $this->generarVista($tabla, $camposBD, $estilo, $modulo);
        $this->generarRequest($tabla, $camposBD, $modulo);

    }

    private function generarRelacionesHijasModelo($tabla, $campo, $terminacionCampo, $primaria)
    {
        //*******************************
        // TABLAS HIJAS
        //*******************************
        $relaciones = '';

        // con la variable TerminacionCampo, Verificamos si el nombre de la tabla, lo encontramos en otras tablas de la misma base de datos com ouna clave foránea
        // lo que indicaria que esa tabla es parte de este formulario y la debemos anexar, ya sea como una multiregistro o como
        // campos simples, esto depende del nombre de la clave foránea que encontremos
        $consulta = $this->consultarTablas($campo[0]["TABLE_SCHEMA"], $terminacionCampo);

        // recorremos cada una de las tablas hijas
        for ($i = 0; $i < count($consulta); $i++) {
            $tablaHija = get_object_vars($consulta[$i])["TABLE_NAME"];
            $basedatosHija = get_object_vars($consulta[$i])["TABLE_SCHEMA"];
            $campoHija = get_object_vars($consulta[$i])["COLUMN_NAME"];
            $tipoRelacion = explode('_', $campoHija)[2];
            $tipoRelacion = ($tipoRelacion == '1a1' ? 'hasOne' : 'hasMany');
            $moduloHija = $this->consultarModulo($tablaHija);

            //*******************************
            // ESTRUCTURA DE LA TABLA HIJA
            //*******************************
            // consultamos la estructura de la tabla y con esta creamos los campos la relacion en el modelo
            $consultaCampos = $this->consultarCampos($tablaHija, $basedatosHija);

            // $consultaCampos = DB::table('information_schema.COLUMNS')
            //     ->select(DB::raw('COLUMNS.TABLE_SCHEMA, COLUMNS.TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, COLUMN_TYPE, COLUMN_COMMENT, COLUMN_DEFAULT, TABLE_COMMENT, IS_NULLABLE'))
            //     ->leftjoin('information_schema.TABLES','COLUMNS.TABLE_NAME','=','TABLES.TABLE_NAME')
            //     ->where('COLUMNS.TABLE_NAME', "=", $tablaHija)
            //     ->where('COLUMNS.TABLE_SCHEMA', "=", $basedatosHija)
            //     ->orderby('COLUMNS.TABLE_NAME')
            //     ->get();

            $campo = array();
            for ($j = 0; $j < count($consultaCampos); $j++) {
                $campo[] = get_object_vars($consultaCampos[$j]);

                // si el campo empieza por oid, es el campo de ID, en este caso guardamos el nombre para la tabla
                if (substr($campo[$j]["COLUMN_NAME"], 0, 3) == 'oid') {
                    $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$j]["COLUMN_NAME"], 3);
                    $terminacionCampo = substr($campo[$j]["COLUMN_NAME"], 3);
                }
            }

            $relaciones .=
    "\n\tpublic function $terminacionCampo()
    {
        return \$this->$tipoRelacion('\Modules\\".$moduloHija."\Entities\\" . $terminacionCampo . "Model','$campoHija','$primaria');
    }\n";
        }

        return $relaciones;
    }

    private function generarRelacionesMaestrosModelo($campo)
    {
        //*******************************
        // TABLAS MAESTRAS RELACIONADAS 
        //*******************************
        $relaciones = '';
        // si es un campo de una tabla maestra relacionada y no es el campo COmpania
        if (strpos($campo["COLUMN_NAME"], '_') !== false AND $campo["COLUMN_NAME"] != 'Compania_oidCompania') {

            // tomamos el nombre de la tabla a consultar (la palabra que esta antes del underline)
            $tablaRelacionada = explode('_', $campo["COLUMN_NAME"])[0];
            // tomamos el nombre del campo (despues del underline)
            $nombreCampo = substr(explode('_', $campo["COLUMN_NAME"])[1], 3);

            // si la tabla relacionada y el campo son iguales, es porque este Foreign key esta apuntando a una tabla Padre (relacion uno a muchos)
            // si son diferentes es porque esta apuntando a una relacion con una tabla maestra (que son las que nos interesa consultar para hacer listas)
            if ($tablaRelacionada != $nombreCampo AND $tablaRelacionada != 'remember') {
                // consultamos la tabla que esta relacionada para una lista de selección
                $tablaRelacionadaMayuscula = $this->consultarTablaRelacional($campo["TABLE_SCHEMA"], $campo["COLUMN_NAME"]);
                $tablaRelacionada = strtolower($tablaRelacionadaMayuscula);
                
                // para armar una funcion de relacion hasOne en el controller, necesitamos el nombre de la tabla, su campos ID y el campo de la tabla actual que lo relaciona
                $moduloRelacionada = $this->consultarModulo($tablaRelacionada);
                $nombreModelo = explode('_',$tablaRelacionadaMayuscula)[1];
                $campoOID = 'oid'.$nombreModelo;
                $campoFK = $campo["COLUMN_NAME"];
                $nombreFuncion = strtolower($nombreModelo);

                $relaciones =
                "\n\tpublic function $nombreFuncion()
    {
        return \$this->hasOne('\Modules\\".$moduloRelacionada."\Entities\\" . $nombreModelo . "Model','$campoOID','$campoFK');
    }\n";
                
            }

        }
        return $relaciones;
    }

    private function generarModelo($tabla, $campo, $modulo)
    {

        $relaciones = '';
        $fillables = '';
        //recorremos los campos de la tabla, el que empieza por oid lo guardamos como campo primarykey, los demas como campos fillable
        for ($i = 0; $i < count($campo); $i++) {
            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $primaria = $campo[$i]["COLUMN_NAME"] ;
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[0]["COLUMN_NAME"], 3);
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);
                $tablamin = strtolower($tabla);
            } 
            else
            // Recorremos los campos que van a ser FILLABLE, entre ellos pueden haber campos de relaciones con tablas maestras 
            {
                // adicionamos funciones de relaciones con tablas maestras, si el campo es una FK
                $relaciones .= $this->generarRelacionesMaestrosModelo($campo[$i]);

                // Adicionamos el campo al array de campos fillables
                $fillables .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
            }
        }

        $slash = DIRECTORY_SEPARATOR;
        $raiz = (str_replace('MasterDev\Http\Controllers\\', '', dirname(realpath(__FILE__)) . $slash));
        $arch = fopen($raiz . $slash . $modulo . $slash . 'Entities' . $slash . $terminacionCampo . 'Model.php', "w");

        fwrite($arch,
"<?php
namespace Modules\\" . $modulo . "\Entities;
use Illuminate\Database\Eloquent\Model;

class " . $terminacionCampo . "Model extends Model
{
    protected \$table = '$tablamin';
    protected \$primaryKey = '$primaria';
    protected \$fillable = [$fillables];
    public \$timestamps = false;
");

       

        $relaciones .= $this->generarRelacionesHijasModelo($tabla, $campo, $terminacionCampo, $primaria);
        


        fwrite($arch,
            $relaciones. "
}"
        );
        fclose($arch);

    }

    private function crearCamposControlador($tabla, $campo, $tipoRelacion, $modulo)
    {
        // si vamos a crear campos para controlador de una tabla hija de detalle (relacion 1aM)
        // sus campos deben llevar al final un indice de array (en la variable $camposStore)
        $indiceArray = ($tipoRelacion == '1a1' ? '' : '[$i]');
        $camposStore = '';
        $consultasMaestros = '';
        $compactMaestros = '';
        $camposImagen = '';
        $useModelos = '';

        // recorremos la lista de campos de la tabla
        for ($i = 0; $i < count($campo); $i++) {
            // si el campo empieza por oid, es el campo de ID, en este caso lo guardamos como campo primario y tomamos de ahi el nombre para la tabla y los archivos a crear
            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $primaria = "'" . $campo[$i]["COLUMN_NAME"] . "'";
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$i]["COLUMN_NAME"], 3);
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);
                $useModelos .= 'use Modules\\' . $modulo . '\Entities\\' . $terminacionCampo . "Model;\n";
            } else {
                // sin importar que campo es, verificamos si tiene un underline (_) en el nombre del campo, esto significa que es un campo relacionado con otra tabla
                // a estos es vamos a hacer una consulta adicional a esa tabla relacionada para enviarlos como datos consultados a la vista y mostrarlos como una lista de seleccion
                // solo vamos a ignorar el que sea campo de compañía que tiene un tratamiento especial
                if (strpos($campo[$i]["COLUMN_NAME"], '_') !== false) {

                    // el campo de compania se guarda diferente en el store ya que se asigna QUEMADO el id de la compania de la variable de session
                    //\Session::get('oidCompania')
                    if ($campo[$i]["COLUMN_NAME"] == 'Compania_oidCompania') {
                        $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => \Session::get('oidCompania'),\n\t\t\t\t";
                    } else {

                        // tomamos el nombre de la tabla a consultar (la palabra que esta antes del underline)
                        $tablaRelacionada = explode('_', $campo[$i]["COLUMN_NAME"])[0];
                        // tomamos el nombre del campos (despues del underline sin las 3 primeras letras "oid")
                        $nombreCampo = substr(explode('_', $campo[$i]["COLUMN_NAME"])[1], 3);

                        // si la tabla relacionada y el campo son iguales, es porque este Foreign key esta apuntando a una tabla Padre (relacion uno a muchos)
                        // si son diferentes es porque esta apuntando a una relacion con una tabla maestra (que son las que nos interesa consultar para hacer listas)
                        if ($tablaRelacionada != $nombreCampo AND $tablaRelacionada != 'remember') {
                            // Si no es compañía, lo guardamos en camposStore y adicionalmente le hacemos una consulta
                            $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => (\$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray == '' ? null : \$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray),\n\t\t\t\t\t\t";
                            $finCampo = $tablaRelacionada;
                            $tablaRelacionadaMayuscula = $this->consultarTablaRelacional($campo[$i]["TABLE_SCHEMA"], $campo[$i]["COLUMN_NAME"]);
                            echo $tablaRelacionadaMayuscula.' = ';
                            // con el nombre de la tabla devuelta por la funcion (contiene el prefijo de la tabla), sacamos la terminacionde los campos y el nombre de tabla en minusculas
                            $terminacionCampoRelacionada = explode('_',$tablaRelacionadaMayuscula)[1];
                            $tablaRelacionada = strtolower($tablaRelacionadaMayuscula);

                            $moduloRelacionada = $this->consultarModulo($tablaRelacionada);

                            $consulta = $this->consultarNombreCampo($tablaRelacionada, $campo[$i]["TABLE_SCHEMA"]);
                            
                            $camposMaestros = '';
                            $condicionLista = '';
                            $swNombre = false;
                            $campoNOMBRE = '';
                            for ($j = 0; $j < count($consulta); $j++) {
                                $registro = get_object_vars($consulta[$j]);
                                // si el campo es OID, o Nombre lo pasamos  a la lista de campos maestros para la consulta de la lista de seleccion
                                if (substr($registro["COLUMN_NAME"], 0, 3) == 'oid') {
                                    $campoOID = "'" . $registro["COLUMN_NAME"] . "'";
                                }
                                if ((substr($registro["COLUMN_NAME"], 0, 8) == 'txNombre' or substr($registro["COLUMN_NAME"], 0, 13) == 'txDescripcion' or substr($registro["COLUMN_NAME"], 0, 7) == 'txLogin') and $swNombre == false) {
                                    $campoNOMBRE = "'" . $registro["COLUMN_NAME"] . "',";
                                    $swNombre = true;
                                }
                                // si el campo es Compania_oidCompania, creamos una condicion para la consulta de lista de seleccion
                                if ($registro["COLUMN_NAME"] == 'Compania_oidCompania') {
                                    $condicionLista .= "->where('$tablaRelacionada.Compania_oidCompania','=', \Session::get('oidCompania'))";
                                }
                                // si el campo es txEstado..., , creamos una condicion para la consulta de lista de seleccion
                                if ($registro["COLUMN_NAME"] == 'txEstado' . $finCampo) {
                                    $condicionLista .= "->where('$tablaRelacionada.txEstado$finCampo','=', 'Activo')";
                                }

                            }
                            $condicionLista = ($condicionLista == '' ? 'All()' : substr($condicionLista, 2));
                            $camposMaestros = $campoNOMBRE . $campoOID;
                            $useModelos .= 'use Modules\\' . $moduloRelacionada . '\Entities\\' .$terminacionCampoRelacionada . "Model;\n";
                            // $camposMaestros = substr($camposMaestros,0, strlen($camposMaestros)-1);
                            echo $tablaRelacionadaMayuscula.' ==> ';
                            // la consulta de tabla relacionada se hace diferente dependiendo del tipo de relacion
                            if ($tipoRelacion == '1a1') {
                                // adiciona una línea que consulta el maestro para enviarlo como lista en el compatc a la vista
                                $consultasMaestros .= "\t\$" . $tablaRelacionada . 'lista = ' . $terminacionCampoRelacionada . 'Model::' . $condicionLista . '->pluck(' . $camposMaestros . ');' . "\n\t\t\t";
                                $compactMaestros .= "'" . $tablaRelacionada . "lista',";
                            } else {
                                // adiciona una línea que consulta el maestro para enviarlo como lista en el compatc a la vista
                                $consultasMaestros .= "\t\$oid" . $terminacionCampoRelacionada. 'lista = ' . $terminacionCampoRelacionada . 'Model::' . $condicionLista . '->pluck(\'oid' . $terminacionCampoRelacionada . '\');' . "\n\t\t\t";
                                $consultasMaestros .= "\t\$txNombre" . $terminacionCampoRelacionada . 'lista = ' . $terminacionCampoRelacionada . 'Model::' . $condicionLista . '->pluck(\'txNombre' . $terminacionCampoRelacionada . '\');' . "\n\t\t\t";
                                $compactMaestros .= "'oid" . $terminacionCampoRelacionada . "lista',";
                                $compactMaestros .= "'txNombre" . $terminacionCampoRelacionada . "lista',";
                            }

                        } else {
                            // Si no es compañía, lo guardamos en camposStore y adicionalmente le hacemos una consulta
                            $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => \$id,\n\t\t\t\t\t\t\t\t";
                        }
                    }
                }
                // los campos que NO tienen underline se tratan todos de la misma forma asignando a cada cmapo su valor
                // a excepción del cmapo de tipo checkbox (ch), el de Imagen (im) y el de lista de selección multiple (lm)
                // que los tratamos de forma especial
                elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch') {
                    $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => \$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray,\n\t\t\t\t\t\t";
                } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'im') {
                    // guardamos el codigo para almacenar la imagen
                    $camposImagen = "
                        \$archivo = Input::file('" . $campo[$i]["COLUMN_NAME"] . "')$indiceArray;

                        if(\$request->hasFile('" . $campo[$i]["COLUMN_NAME"] . "')$indiceArray)
                        {
                            \$image = \$archivo;
                            \$nombre" . $campo[$i]["COLUMN_NAME"] . " = \$archivo->getClientOriginalName();

                            \$manager = new ImageManager();
                            \$manager->make(\$image->getRealPath())->heighten(300)->save('imagenes/$terminacionCampo/'. \$nombre" . $campo[$i]["COLUMN_NAME"] . ");
                        }
                        else
                        {
                            \$nombre" . $campo[$i]["COLUMN_NAME"] . " = '';
                        }";

                    $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => \$nombre" . $campo[$i]["COLUMN_NAME"] . ",\n\t\t\t\t\t\t";
                } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'lm') {
                    $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => (\$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray != '' ? implode(',',\$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray) : ''),\n\t\t\t\t\t\t";
                } else {
                    // si no tiene underline, lo metemos a camposStore
                    $camposStore .= "'" . $campo[$i]["COLUMN_NAME"] . "' => \$request['" . $campo[$i]["COLUMN_NAME"] . "']$indiceArray,\n\t\t\t\t\t\t";
                }
            }

        }

        return array($primaria, $tabla, $terminacionCampo, $camposStore, $consultasMaestros, $compactMaestros, $camposImagen, $useModelos);
    }

    private function generarControlador($tabla, $campo, $modulo)
    {

        //*******************************
        // TABLA PRINCIPAL
        //*******************************

        // creamos los campos para la tabla principal
        $datos = $this->crearCamposControlador($tabla, $campo, '1a1', $modulo);

        // guardamos los datos recibidos de la funcion en variables independientes
        $primaria = $datos[0];
        $tabla = $datos[1];
        $tablamin = strtolower($tabla);
        $terminacionCampo = $datos[2];
        $terminacionCampoMin = strtolower($terminacionCampo);
        $useModelos = $datos[7];


        // a los campos para guardar en el store, de una vez les adicionamos el modelo de la tabla principal
        $camposStore = 
"DB::beginTransaction();
        try{
            \$" . strtolower($terminacionCampo) . " = " . $terminacionCampo . "Model::create(\$request->all());
        ";

       
        $camposUpdate = 
"DB::beginTransaction();
        try{
            \$" . strtolower($terminacionCampo) . " = " . $terminacionCampo . "Model::find(\$id);
            \$" . strtolower($terminacionCampo) . "->fill(\$request->all());
            \$" . strtolower($terminacionCampo) . "->save();
        ";

        // consultas de tablas maestras relacionadas como listas desplegables
        $consultasMaestros = $datos[4];
        $compactMaestros = $datos[5];
        $camposDetalle = '';
        $ejecutarDetalle = '';

        //*******************************
        // TABLAS HIJAS
        //*******************************

        // con la variable TerminacionCampo, Verificamos si el nombre de la tabla, lo encontramos en otras tablas de la misma base de datos com ouna clave foránea
        // lo que indicaria que esa tabla es parte de este formulario y la debemos anexar, ya sea como una multiregistro o como
        // campos simples, esto depende del nombre de la clave foránea que encontremos
        $consulta = $this->consultarTablas($campo[0]["TABLE_SCHEMA"], $terminacionCampo);

        // $consulta = DB::table('information_schema.COLUMNS')
        //     ->select(DB::raw('TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME'))
        //     ->where('COLUMN_NAME', "like", $terminacionCampo.'\_oid'.$terminacionCampo.'\_%')
        //     ->where('TABLE_SCHEMA', "=", $campo[0]["TABLE_SCHEMA"])
        //     ->orderby('TABLE_NAME')
        //     ->get();

        // recorremos cada una de las tablas hijas
        for ($i = 0; $i < count($consulta); $i++) {
            $tablaHija = get_object_vars($consulta[$i])["TABLE_NAME"];
            $basedatosHija = get_object_vars($consulta[$i])["TABLE_SCHEMA"];
            $campoHija = get_object_vars($consulta[$i])["COLUMN_NAME"];
            $tipoRelacion = explode('_', $campoHija)[2];

            //*******************************
            // ESTRUCTURA DE LA TABLA HIJA
            //*******************************
            // consultamos la estructura de la tabla y con esta creamos los campos para el controller
            $consultaCampos = $this->consultarCampos($tablaHija, $basedatosHija);

            // $consultaCampos = DB::table('information_schema.COLUMNS')
            //     ->select(DB::raw('COLUMNS.TABLE_SCHEMA, COLUMNS.TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, COLUMN_TYPE, COLUMN_COMMENT, COLUMN_DEFAULT, TABLE_COMMENT, IS_NULLABLE'))
            //     ->leftjoin('information_schema.TABLES','COLUMNS.TABLE_NAME','=','TABLES.TABLE_NAME')
            //     ->where('COLUMNS.TABLE_NAME', "=", $tablaHija)
            //     ->where('COLUMNS.TABLE_SCHEMA', "=", $basedatosHija)
            //     ->orderby('COLUMNS.TABLE_NAME')
            //     ->get();

            $camposBD = array();
            for ($j = 0; $j < count($consultaCampos); $j++) {
                $camposBD[] = get_object_vars($consultaCampos[$j]);
            }

            // generamos el modelo para la tabla hija
            $datos = $this->generarModelo($tablaHija, $camposBD,$modulo);

            //generamos los campos para el crear Controlador de la tabla hija
            $datos = $this->crearCamposControlador($tablaHija, $camposBD, $tipoRelacion, $modulo);

            // guardamos datos devueltos por la funcion
            $tablaRelacionadaMayuscula = $datos[1];
            $consultasMaestros .= $datos[4];
            $compactMaestros .= $datos[5];
            $useModelos .= $datos[7];

            // segun el tipo de relacion entre las tablas, creamos de forma diferente los campos
            if ($tipoRelacion == '1a1') {
                // si es relacion 1 a 1, creamos campos Simples
                // $camposStore .= $datos[6]."\n\n".
                //                 "\t\t\t\t".''.$datos[1].'Model::create(['.
                //                     $datos[3].
                //                 ']);'."\n\n";
                $camposStore .= $datos[6] . "\n\n" .
                    "\t\t\t\t" . $datos[2] . 'Model::create([$request->all()]);' . "\n\n";

                // $camposUpdate .=
                //                 "\$indice = array('oid".$datos[2]."' => \$request['oid".$datos[2]."']);

                // \$datos= array(
                //         ".$datos[3]."
                //         );

                // \$guardar = ".$datos[1]."Model::updateOrCreate(\$indice, \$datos);";

                $camposUpdate .= $datos[6] . "\n\n" .
                "\t\t\t" . '$' . strtolower($datos[2]) . ' = ' . $datos[2] . 'Model::find($id);
            $' . strtolower($datos[2]) . '->fill($request->all());
            $' . strtolower($datos[2]) . '->save();'."\n";

                            
            } else {
                // si es una relacion de 1 a Muchos, creamos un grabado de multiregistro
                $ejecutarDetalle = "\t\$this->grabarDetalle(\$request, $" . strtolower($terminacionCampo) . "->oid" . $terminacionCampo . ");\n";

                $camposStore .= $ejecutarDetalle;
                $camposUpdate .= $ejecutarDetalle;

                $camposDetalle .=
                    "\$idsEliminar = explode(',', \$request['eliminar" . $datos[2] . "']);\n\t\t" .
        $datos[2] . "Model::whereIn('oid" . $datos[2] . "',\$idsEliminar)->delete();
        // recorremos cada registro del detalle para guardarlos en la tabla
        for(\$i = 0; \$i < count(\$request['oid" . $datos[2] . "']); \$i++)
        {
            " . $datos[6] . "
            // creamos un array con el campo oid autonumerico
            \$indice = array(
                'oid" . $datos[2] . "' => \$request['oid" . $datos[2] . "'][\$i]);
            // En otro array guardamos los demas campos fillables
            \$datos= array(
                " . $datos[3] . "
                );
            // ejecutamos la funcion UpdateOrCreate de laravel que adiciona los registros nuevos y actualiza los existentes
            \$guardar = " . $datos[2] . "Model::updateOrCreate(\$indice, \$datos);
        }\n\n";

            }

        }


        $camposStore .= 
        "\t\t\t // si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oid" . $terminacionCampo . "' => \$" . strtolower($terminacionCampo) . "->oid" . $terminacionCampo . "] , 201);
        }catch(\Exception \$e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return abort(500, \$e->getMessage());
        }";


    $camposUpdate .= 
        "\t\t\t// si no se generaron errores al guardar ninguna de las tablas, hacemos commit
            DB::commit();
            return response(['oid" . $terminacionCampo . "' => \$" . strtolower($terminacionCampo) . "->oid" . $terminacionCampo . "] , 200);
        }catch(\Exception \$e){
            // si se generó algún error al guardar en las tablas, devolvemos toda la transacción
            DB::rollback();
            return response(['oid" . $terminacionCampo . "' => \$" . strtolower($terminacionCampo) . "->oid" . $terminacionCampo . "] , 500);
        }";

        // a las consultas de los maestros relacionados les agregamos comentario
        $consultasMaestros = ($consultasMaestros != '' ? "// Consultamos los maestros necesarios para las listas de seleccion\n\t" . $consultasMaestros : '');
        // alos nombres de variables de los maestros relacionados, les adicionamos la funcion compact
        $compactMaestros = ($compactMaestros != '' ? ', compact(' . substr($compactMaestros, 0, strlen($compactMaestros) - 1) . ')' : '');

        // creamos la funcion para guardar las tablas hijas que sean de 1 a Muchos
        $funcionDetalle =
    "// En este método guardamos todas las tablas relacionadas 
    protected function grabarDetalle(\$request, \$id)
    {
        " . $camposDetalle . "
    }";

        {

            $slash = DIRECTORY_SEPARATOR;
            $raiz = (str_replace('app' . $slash . 'Http' . $slash . 'Controllers' . $slash . '', '', dirname(realpath(__FILE__)) . $slash));
            $raiz = (str_replace('MasterDev\Http\Controllers\\', '', dirname(realpath(__FILE__)) . $slash));
            // creamos el archivo de controlador
            $archCont = fopen($raiz . $slash . $modulo . $slash . 'Http' . $slash . 'Controllers' . $slash . $terminacionCampo . 'Controller.php', "w");

            // escribimos el codigo de la clase
            fwrite($archCont, 
"<?php

namespace Modules\\" . $modulo . "\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

$useModelos

use Modules\\" . $modulo . "\Http\Requests\\" . $terminacionCampo . "Request;

class " . $terminacionCampo . "Controller extends Controller
{
            ");

            // Escribimos el codigo del INDEX
            fwrite($archCont, "
    public function index()
    {
        \$permisos = \$this->consultarPermisos();
        if(!\$permisos){
            abort(401);
        }
        $" . strtolower($terminacionCampo) . " = " . $terminacionCampo . "Model::orderBy('oid" . $terminacionCampo . "' , 'DESC')->get();
        return view('" . strtolower($modulo) . "::" . lcfirst($terminacionCampo) . "Grid', compact('permisos' , '" . strtolower($terminacionCampo) . "'));
    }

            ");

            // Escribimos el codigo del CREATE
            //
            fwrite($archCont, "
    public function create()
    {
        $" . strtolower($terminacionCampo) . " = new " . $terminacionCampo . "Model();
        $" . strtolower($terminacionCampo) . "->txEstado$terminacionCampo = 'Activo';
        $consultasMaestros
        
        return view('" . strtolower($modulo) . "::" . lcfirst($terminacionCampo) . "Form' , ['" . strtolower($terminacionCampo) . "' => $" . strtolower($terminacionCampo) . "] $compactMaestros);
    }
            ");

            // Escribimos el codigo del STORE
            fwrite($archCont, "
    public function store(" . $terminacionCampo . "Request \$request)
    {
        $camposStore

        
    }
            ");

            // Escribimos el codigo del SHOW
            fwrite($archCont, "
    public function show(\$id)
    {
        //
    }
            ");

            // Escribimos el codigo del EDIT
            fwrite($archCont, "
    public function edit(\$id)
    {
        $" . strtolower($terminacionCampo) . " = " . $terminacionCampo . "Model::find(\$id);
        $consultasMaestros
        return view('" . strtolower($modulo) . "::" . lcfirst($terminacionCampo) . "Form' , ['" . strtolower($terminacionCampo) . "' => $" . strtolower($terminacionCampo) . "] $compactMaestros);
    }
            ");

            $camposImagen = $datos[6] . "\n\n";
            // Escribimos el codigo del UPDATE
            fwrite($archCont, "

    public function update(" . $terminacionCampo . "Request \$request, \$id)
    {
        $camposUpdate
    }
    ");

            // Escribimos el codigo del grabarDetalle
            fwrite($archCont,
    $funcionDetalle .
    "}\n"
            );

            // cerramos el archivo
            fclose($archCont);

            // Imprimimos en pantalla las rutas para el formulario creado
            echo "  // Ruta para el controlador (dentro del prefijo del módulo)<br>
                    Route::resource('/" . strtolower($terminacionCampo) . "', '" . $terminacionCampo . "Controller');<br>
                    ";
        }

    }

    private function generarGrid($tabla, $campo, $modulo)
    {

        $camposConsulta = '';
        $camposTitulos = '';
        $camposGrid = '';
        $condicion = '';
        $relaciones = '';

        for ($i = 0; $i < count($campo); $i++) {

            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $primaria = $campo[$i]["COLUMN_NAME"];
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$i]["COLUMN_NAME"], 3);
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);
                $tablamin = strtolower($tabla);
                $titulo = ($campo[$i]["TABLE_COMMENT"] == '' ? $terminacionCampo : $campo[$i]["TABLE_COMMENT"]);
            }

            if ($campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {
                // si es un campo de uan tabla maestra relacionada
                if (strpos($campo[$i]["COLUMN_NAME"], '_') !== false) {

                    // tomamos el nombre de la tabla a consultar (la palabra que esta antes del underline)
                    $tablaRelacionada = explode('_', $campo[$i]["COLUMN_NAME"])[0];
                    // tomamos el nombre del campo (despues del underline)
                    $nombreCampo = substr(explode('_', $campo[$i]["COLUMN_NAME"])[1], 3);

                    // si la tabla relacionada y el campo son iguales, es porque este Foreign key esta apuntando a una tabla Padre (relacion uno a muchos)
                    // si son diferentes es porque esta apuntando a una relacion con una tabla maestra (que son las que nos interesa consultar para hacer listas)
                    if ($tablaRelacionada != $nombreCampo AND $tablaRelacionada != 'remember') {
                        $tablaRelacionadaMayuscula = $this->consultarTablaRelacional($campo[$i]["TABLE_SCHEMA"], $campo[$i]["COLUMN_NAME"]);
                        $tablaRelacionada = strtolower($tablaRelacionadaMayuscula);
                        $nombreMaestro = explode('_',$tablaRelacionadaMayuscula)[1];

                        // $consulta = $this->consultarTablas(  $tablaRelacionada, $campo[$i]["TABLE_SCHEMA"]);
                        // $consulta = $this->consultarTablas(  $tablaRelacionada, $campo[$i]["TABLE_SCHEMA"]);
                        $consulta = $this->consultarCampoRelacionado($tablaRelacionada, $campo[$i]["TABLE_SCHEMA"], $nombreMaestro);

                       

                        for ($c = 0; $c < count($consulta); $c++) {
                            $aConsulta = get_object_vars($consulta[$c]);

                            $relTabla = $aConsulta["TABLE_NAME"];
                            $relAlias = $aConsulta["TABLE_NAME"] . $nombreCampo;

                            // Si es el campo de ID, creamos una relacion, si es el de nombre lo adicionamos a los campos de la cosulta
                            if (substr($aConsulta["COLUMN_NAME"], 0, 3) == 'oid') {

                                $relaciones .= "->leftjoin('" . $relTabla . " as " . $relAlias . "','" . $campo[$i]["COLUMN_NAME"] . "','=','" . $relAlias . "." . $aConsulta["COLUMN_NAME"] . "')\n";
                            } else {
                                $camposConsulta .= $relAlias . "." . $aConsulta["COLUMN_NAME"] . " as " . $aConsulta["COLUMN_NAME"] . $nombreCampo . ",";
                                $camposTitulos .= "<th>" . $aConsulta["TABLE_COMMENT"] . "</th>\n\t\t\t\t\t\t";
                                $camposGrid .= "<td>{{\$" . strtolower($terminacionCampo) . 'reg->'. strtolower($nombreMaestro) . "->" . $aConsulta["COLUMN_NAME"] . "}}</td>\n\t\t\t\t\t\t\t";
                                //$camposGrid .= "\$row[\$key][] = '<span $estilo>'.\$value[\"" . $aConsulta["COLUMN_NAME"] . $nombreCampo . "\"].'</span>';\n\t\t\t";
                            }

                        }
                    }
                } else {
                    $camposConsulta .= $campo[$i]["COLUMN_NAME"] . ",";
                    $camposTitulos .= "<th>" . explode('_',explode('_',$campo[$i]["COLUMN_COMMENT"])[0])[0] . "</th>\n\t\t\t\t\t\t";


                    if ($campo[$i]["COLUMN_NAME"] == 'txEstado' . $terminacionCampo) {
                        $estilo = "style=\"color: '.(\$value['" . $campo[$i]["COLUMN_NAME"] . "'] == 'Anulado' ? 'red': 'black').'\"";
                        $camposGrid .= "<td>{{\$" . strtolower($terminacionCampo) . "reg->" . $campo[$i]["COLUMN_NAME"] . "}}</td>\n\t\t\t\t\t\t\t";

                        //$camposGrid .= "\$row[\$key][] = '<span $estilo>'.\$value[\"" . $campo[$i]["COLUMN_NAME"] . "\"].'</span>';\n\t\t\t";
                    } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'cl') {
                        $estilo = "style=\"width:100%; background-color: {{ \$". strtolower($terminacionCampo) ."reg->". $campo[$i]["COLUMN_NAME"] . " }} \"";
                        $camposGrid .= "<td><div $estilo>&nbsp;</div></td>\n\t\t\t\t\t\t\t";

                        //$camposGrid .= "\$row[\$key][] = '<div $estilo>&nbsp;</div>';\n\t\t\t";
                    } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'im') {
                        $estilo = "style=\"width:100px;\"";
                        //$camposGrid .= "\$row[\$key][] = '<div><img $estilo src=\"http://'.\$_SERVER['HTTP_HOST'].'/imagenes/tercero/'.(\$value['" . $campo[$i]["COLUMN_NAME"] . "'] == '' ? 'sinfoto.png' : \$value['" . $campo[$i]["COLUMN_NAME"] . "']).'\"></div>';";
                    } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch') {
                        $estilo = "";
                        $camposGrid .= "<td class=\"text-center\"><?php echo (\$".strtolower($terminacionCampo)."reg->".$campo[$i]["COLUMN_NAME"].") ? '<i class=\"fas fa-check text-center\" style=\"color:green\"></i>' : '<i class=\"fas fa-times text-center\" style=\"color:red\"></i>'?></td>\n\t\t\t\t\t\t\t";
                    } else {
                        $estilo = '';
                        $camposGrid .= "<td>{{\$" . strtolower($terminacionCampo) . "reg->" . $campo[$i]["COLUMN_NAME"] . "}}</td>\n\t\t\t\t\t\t\t";
                        //$camposGrid .= "\$row[\$key][] = '<span $estilo>'.\$value[\"" . $campo[$i]["COLUMN_NAME"] . "\"].'</span>';\n\t\t\t";
                    }

                }
                //$row[$key][] = '<span $estilo>'.$value['txEstadoBodega'].'</span>';
            } else {
                $condicion = "->where('$tablamin.Compania_oidCompania','=', \Session::get('oidCompania'))";
            }

        }
        $camposConsulta = substr($camposConsulta, 0, strlen($camposConsulta) - 1);

        $slash = DIRECTORY_SEPARATOR;
        $raiz = (str_replace('MasterDev' . $slash . 'Http' . $slash . 'Controllers' . $slash . '', '', dirname(realpath(__FILE__)) . $slash));

        // creamos el archivo de controlador
        $archGrid = fopen($raiz . $modulo . $slash . 'Resources' . $slash . 'views' . $slash . lcfirst($terminacionCampo) . 'Grid.blade.php', "w");

        fwrite($archGrid, 
'@extends("layouts.principal")
@section("nombreModulo")
    ' . $titulo . '
@stop
@section("scripts")
    <script type="text/javascript">
        $(function(){
            configurarGrid("' . strtolower($terminacionCampo) . '-table");
        });
    </script>
@endsection
@section("contenido")
<div class="card border-left-primary shadow h-100 py-2">
    <div class="card-body">
        <div class="table-responsive">
            <table id="' . strtolower($terminacionCampo) . '-table" class="table table-hover table-sm scalia-grid">
                <thead class="bg-primary text-light">
                    <tr>
                        <th style="width: 150px;" data-orderable="false">
                            @if($permisos["chAdicionarRolOpcion"])
                            <a class="btn btn-primary btn-sm text-light" href="{!!URL::to(\'/' . strtolower($modulo) . '/' . strtolower($terminacionCampo) . '\' , [\'create\'])!!}">
                                <i class="fa fa-plus"></i>
                            </a>
                            @endif
                            <button id="btnLimpiarFiltros" class="btn btn-primary btn-sm text-light" >
                                <i class="fas fa-broom"></i>
                            </button>
                            <button id="btnRecargar" class="btn btn-primary btn-sm text-light">
                                <i class="fas fa-redo-alt"></i>
                            </button>
                        </th>
                        '.$camposTitulos.'
                    </tr>
                </thead>
                <tbody>
                    @foreach($' . strtolower($terminacionCampo) . ' as $' . strtolower($terminacionCampo) . 'reg)
                        <tr class="{{$' . strtolower($terminacionCampo) . 'reg->txEstado' . $terminacionCampo . ' == \'Anulado\' ? \'text-danger\': \'\' }}">
                            <td>
                                <div class="btn-group" role="group" aria-label="Acciones">
                                    @if($permisos[\'chModificarRolOpcion\'])
                                        <a class="btn btn-success btn-sm" href="{!!URL::to(\'/' . strtolower($modulo) . '/' . strtolower($terminacionCampo) . '\' , [$' . strtolower($terminacionCampo) . 'reg->oid' . $terminacionCampo . ' , \'edit\'])!!}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    @endif
                                    @if($permisos[\'chEliminarRolOpcion\'])
                                        <button type="button" onclick="confirmarEliminacion(\'{{$' . strtolower($terminacionCampo) . 'reg->oid' . $terminacionCampo . '}}\', \'' . $tablamin . '\', \'' . $terminacionCampo . '\')" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    @endif
                                    @if($permisos[\'chModificarRolOpcion\'])
                                        <button class="btn btn-warning btn-sm" onclick="cambiarEstado(\'{{$' . strtolower($terminacionCampo) . 'reg->oid' . $terminacionCampo . '}}\', \'' . $tablamin . '\', \'' . $terminacionCampo . '\',\'{{$' . strtolower($terminacionCampo) . 'reg->txEstado' . $terminacionCampo . '}}\')">
                                            <i class="fas fa-power-off"></i>
                                        </button>
                                    @endif
                                    @if($permisos[\'chConsultarRolOpcion\'])
                                        <a class="btn btn-info btn-sm" href="{!!URL::to(\'/' . strtolower($modulo) . '/' . strtolower($terminacionCampo) . '\' , [$' . strtolower($terminacionCampo) . 'reg->oid' . $terminacionCampo . '])!!}">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    @endif
                                </div>
                            </td>
                            '.$camposGrid.'
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        '.$camposTitulos.'
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection');

        fclose($archGrid);
    }

    private function consultarOpcionesListaEncabezado($datosCampo)
    {
        // el valor por defecto del campo puede venir en 2 formatos diferentes
        // Formato 1: Lista de valores separados por coma (,)
        //      Ejemplo Comercial, Administrativo, Cartera, Contable
        // Formato 2: Lista de Pares separados pro coma (,), donde cada par contiene el valor y el titulo y su separador es el pipe (|)
        //      Ejemplo:  *COM*|Comercial,*ADM*|Administrativo,*CAR*|Cartera,*CON*|Contabilidad
        $comentario = explode('_',$datosCampo["COLUMN_COMMENT"])[1];
        $opciones = explode(",", str_replace("'", '', str_replace(')', '', str_replace('(', '', $comentario))));
        $strOpciones = '';

        // recorremos cada una de las opciones, armando un array de parejas de valor y opcion
        for ($i = 0; $i < count($opciones); $i++) {
            // *COM*|Comercial
            $valores = explode("|", $opciones[$i]);
            $strOpciones .= "'" . $valores[0] . "' => '" . (isset($valores[1]) ? $valores[1] : $valores[0]) . "',";
        }
        $strOpciones = '[' . $strOpciones . ']';
        return $strOpciones;
    }

    private function consultarOpcionesListaDetalle($datosCampo)
    {
        // el valor por defecto del campo puede venir en 2 formatos diferentes
        // Formato 1: Lista de valores separados por coma (,)
        //      Ejemplo Comercial, Administrativo, Cartera, Contable
        // Formato 2: Lista de Pares separados pro coma (,), donde cada par contiene el valor y el titulo y su separador es el pipe (|)
        //      Ejemplo:  *COM*|Comercial,*ADM*|Administrativo,*CAR*|Cartera,*CON*|Contabilidad
        $comentario = explode('_',$datosCampo["COLUMN_COMMENT"])[1];
        $opciones = explode(",", str_replace("'", '', str_replace(')', '', str_replace('(', '', $comentario))));
        $strValores = '';
        $strTitulos = '';

        // recorremos cada una de las opciones, armando un array de parejas de valor y opcion
        for ($i = 0; $i < count($opciones); $i++) {
            // *COM*|Comercial
            $valores = explode("|", $opciones[$i]);
            $strValores .= "'" . $valores[0] . "',";
            $strTitulos .= "'" . (isset($valores[1]) ? $valores[1] : $valores[0]) . "',";
        }
        $strOpciones = '[[' . $strValores . '], [' . $strTitulos . ']]';
        return $strOpciones;
    }

    private function longitudCampo($registro)
    {
        // El tipo de Dato viene en el formato:  tipo(long), del cual vamos a extraer la longitud para devolverla
        // los tipo DATE y TEXT no tienen longitud, en ese caso devolvemos 100
        // esta longitud no va a limitar la cantidad de campos a digitar, solo es para saber el ancho de la columna en una multiregistro

        $tipo = $registro["COLUMN_TYPE"];
        // si encuentra un parentesis inicial, es porque tiene longitud
        if (strpos($tipo, '(') !== false) {
            // devolvemos desde la posicion encontrada hasta la longitud total del campo -1 caracter (parentesis de cierre)
            $longitud = str_replace(',', '.', substr($tipo, strpos($tipo, '(') + 1, strpos($tipo, ')') - 1  -  strpos($tipo, '(') ));
        } else {
            $longitud = 100;
        }
               
        // si al final de todo la longitud es menor a 10, devolvemos un 10, ya que
        // los campos mas pequeños quedan demasiado estrechos en la interface grafica
        return ($longitud < 10 ? 10 : $longitud);
    }

    private function crearCamposVista($tabla, $campo, $tablaPadre)
    {

        $script = '';
        $librerias = '';
        $pares = 1;
        $totCampos = count($campo);
        $swTexto = false;
        $swdate = false;
        $swtime = false;
        $swdatetime = false;
        $swcheck = false;
        $swchosen = false;
        

        if ($totCampos >= 5) {
            $claseAncho = 'col-sm-6';
        } else {
            $claseAncho = 'col-sm-12';
        }

        $campos = '';
        for ($i = 0; $i < $totCampos; $i++) {

            // los campos que se van a validar como obligatorios, van a tener un * en el LABEL
            $Validacion = ($campo[$i]["IS_NULLABLE"] == 'NO' ? 'required' : '');

            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);

                if ($tabla != $tablaPadre) {
                    $relacion = '->' . $terminacionCampo;
                } else {
                    $relacion = '';
                }

                $primaria = $campo[$i]["COLUMN_NAME"];
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$i]["COLUMN_NAME"], 3);
                $tablamin = strtolower($tabla);

                $valorCampo = 'null';

                $titulo = ($campo[$i]["TABLE_COMMENT"] == '' ? $terminacionCampo : $campo[$i]["TABLE_COMMENT"]);
                $campos .= "{!!Form::hidden('$primaria', $valorCampo, array('id' => '$primaria')) !!}";
            } else {
                if ($campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {

                    // si existen mas de 6 campos, insertamos un div con clase ROW cada par de campos (lo abrimos en el campo PAR, multiplo de 2 y lo cerramos en el campo impar)
                    if ($totCampos >= 1 and $pares == 1) {
                        $campos .= "\n\t\t\t".'<div class="row">';
                    }

                    $valorCampo = 'null';

                    switch ($campo[$i]["DATA_TYPE"]) {
                        case 'int':
                            // Si el campo tiene un Underline, es porque es una clave foránea, en ese caso  verificamos si es relacion con tabla maestra o es campo
                            // de unión entre una tabla padre y una tabla hija, si es de maestro hacemos lista, sino lo ignoramos
                            if (strpos($campo[$i]["COLUMN_NAME"], '_') !== false) {

                                $tablaRelacionada = strtolower($this->consultarTablaRelacional($campo[$i]["TABLE_SCHEMA"], $campo[$i]["COLUMN_NAME"]));

                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $campoForm = '{!!Form::select(\'' . $campo[$i]["COLUMN_NAME"] . '\',$' . $tablaRelacionada . 'lista,' . $valorCampo . ',[\'class\'=>\'chosen-select form-control\',\'style\'=>\'width:100%\',\'placeholder\'=>\'Selecciona ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';

                            }
                            // si es un campo de checkbox
                            elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch') {

                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . 'C\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $parametro = "'".$campo[$i]["COLUMN_NAME"]."'";
                                $campoForm = "{!!Form::hidden(\"" . $campo[$i]["COLUMN_NAME"] . "\", null,[\"id\"=>\"" . $campo[$i]["COLUMN_NAME"] . "\"])!!}
                                             {!!Form::checkbox(\"" . $campo[$i]["COLUMN_NAME"] . "C\",null, ".('$'.strtolower($terminacionCampo).'->'.$campo[$i]["COLUMN_NAME"]. ' ? true : false').",[\"class\"=>\"form-control\",\"onclick\"=>\"cambioCheckbox(" . $parametro . ")\",\"id\"=>\"" . $campo[$i]["COLUMN_NAME"] . "C\"])!!}";
                                $swcheck = true;
                                
                            }
                            // si es un campo numérico normal
                            else {
                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'text-right form-control\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            }
                            break;

                        case 'varchar':
                            // si es un campo de Lista de opciones fijas SELECCION UNICA
                            if (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ls') {
                                $opcionesLista = $this->consultarOpcionesListaEncabezado($campo[$i]);
                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $campoForm = '{!!Form::select(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $opcionesLista . ',' . $valorCampo . ',[\'class\'=>\'chosen-select form-control\',\'placeholder\'=>\'Selecciona ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                                $swchosen = true;
                            }
                            // si es un campo de Lista de opciones fijas SELECCION MULTIPLE
                            elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'lm') {
                                $opcionesLista = $this->consultarOpcionesListaEncabezado($campo[$i]);
                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $campoForm = '{!!Form::select(\'' . $campo[$i]["COLUMN_NAME"] . '[]\',' . $opcionesLista . ',' . $valorCampo . ',[\'class\'=>\'chosen-select form-control\', \'multiple\', \'id\' =>\'' . $campo[$i]["COLUMN_NAME"] . '\'])!!}';
                                $script .= $campo[$i]["COLUMN_NAME"] . ' = "<?php echo (isset($' . $tablaPadre . $relacion . '->' . $campo[$i]["COLUMN_NAME"] . ') ? $' . $tablaPadre . $relacion . '->' . $campo[$i]["COLUMN_NAME"] . ' : \'\')?>";' . "\n\t\t\t\t";
                                $script .= '$("#' . $campo[$i]["COLUMN_NAME"] . '").val(' . $campo[$i]["COLUMN_NAME"] . '.split(",")).trigger("chosen:updated");' . "\n";
                                $swchosen = true;
                            }
                            // para los campos de Color
                            elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'cl') {
                                // si es cl (adicionamos un campo de selector de colores)
                                $librerias .= "{!! Html::style('assets/colorpicker/css/bootstrap-colorpicker.min.css'); !!}
                                                {!! Html::script('assets/colorpicker/js/bootstrap-colorpicker.js'); !!}";

                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                // $campoForm = '<div id="' . $campo[$i]["COLUMN_NAME"] . '" class="input-group colorpicker-component" style="width: 100%;">
                                //             {!!Form::hidden(\'' . $campo[$i]["COLUMN_NAME"] . '\',(isset($' . $tablamin . ') ? $' . $tablamin . '->' . $campo[$i]["COLUMN_NAME"] . ' : \'#255986\'), null,[\'id\'=>\'' . $campo[$i]["COLUMN_NAME"] . '\'])!!}
                                //             <span class="input-group-addon"><i style="width: 100%;"></i></span>';

                                $campoForm = '<input type="color" name="' . $campo[$i]["COLUMN_NAME"] . '" id="' . $campo[$i]["COLUMN_NAME"] . '" value="{{ (isset($' . strtolower($terminacionCampo) . ') ? $' . strtolower($terminacionCampo) . '->' . $campo[$i]["COLUMN_NAME"] . ' : \'#255986\') }}" class="form-control">';

                                // $script .= "$(function () {
                                //                     $('#" . $campo[$i]["COLUMN_NAME"] . "').colorpicker();
                                //             });";
                            }
                            // Para los campos de imagen
                            elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'im') {
                                // si es im (adicionamos un campo de cargar Imagen)
                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';

                                $campoForm = '<div class="panel panel-default" >
                                                <input id="' . $campo[$i]["COLUMN_NAME"] . '" name="' . $campo[$i]["COLUMN_NAME"] . '"
                                                        type="file"
                                                        value="<?php echo (isset($' . $tablamin . ') ? \'imagenes/tercero/\'. $' . $tablamin . '->' . $campo[$i]["COLUMN_NAME"] . ' : \'\'); ?>" >
                                              </div>';

                                $script .= " $('#" . $campo[$i]["COLUMN_NAME"] . "').fileinput({
                                                language: 'es',
                                                uploadUrl: '#',
                                                allowedFileExtensions : ['jpg', 'png','gif'],
                                                initialPreview: [
                                                '<?php if(isset($" . $tablamin . "))
                                                        echo Html::image(\"imagenes/tercero/\". $" . $tablamin . "->" . $campo[$i]["COLUMN_NAME"] . ",\"Imagen no encontrada\",array(\"style\"=>\"width:148px;height:158px;\"));
                                                                    ;?>'
                                                        ],
                                                dropZoneTitle: 'Seleccione la Imagen',
                                                removeLabel: '',
                                                uploadLabel: '',
                                                browseLabel: '',
                                                uploadClass: '',
                                                uploadLabel: '',
                                                uploadIcon: '',
                                                });";

                            } else {
                                // en los de tipo varchar, puede estar el campo de Estado, el cual haremos con un campos input readonly y con valor por defecto Activo
                                if ($campo[$i]["COLUMN_NAME"] == 'txEstado' . $terminacionCampo) {
                                    $valorCampo = 'null';
                                    //$valor = '(isset($'.$tablamin.') ? $'.$tablamin.'->'.$campo[$i]["COLUMN_NAME"].'   : \'Activo\')';
                                    $sololectura = '"readonly" => "readonly",';
                                } else {
                                    $valorCampo = 'null';
                                    $sololectura = '';
                                }
                                $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                                $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[' . $sololectura . ' \'class\'=>\'form-control\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            }

                            break;

                        case 'date':
                            $swdate = true;

                            $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                            $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'date-picker\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            break;

                        case 'time':
                            $swtime = true;

                            $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                            $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'time-picker\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            break;

                        case 'datetime':
                            $swdatetime = true;

                            $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                            $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'datetime-picker\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            break;

                        case 'text':
                            $swTexto = true;
                            $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                            $campoForm = '{!!Form::textarea(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'editorTexto form-control\',\'style\'=>\'width:100%\', \'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            break;

                        case 'decimal':
                            $etiquetaForm = '{!!Form::label(\'' . $campo[$i]["COLUMN_NAME"] . '\', \'' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\', array(\'class\' => \'text-md text-primary mb-1 ' . $Validacion . ' \')) !!}';
                            $campoForm = '{!!Form::text(\'' . $campo[$i]["COLUMN_NAME"] . '\',' . $valorCampo . ',[\'class\'=>\'text-right form-control\',\'placeholder\'=>\'Ingresa ' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '\'])!!}';
                            break;
                    }

                    if (!isset(explode('_', $campo[$i]["COLUMN_NAME"])[2])) {

                        
                        if (($campo[$i]["DATA_TYPE"]) == 'text') {
                            $campos .=
                                '
                <div class="col-md-12">
                    <div class="form-group ">
                    ' . $etiquetaForm . '
                    ' . $campoForm . '
                    </div>
                </div>';
                        } else if (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch') {
                            
                            $campos .=
                                '
                <div class="' . $claseAncho . '">
                    &nbsp;
                    <div class="my-1 mr-sm-2">
                        <div class="divCheckHeader">
                            <label class="header-checkbox-container">
                                ' . $campoForm . '
                                <span class="checkmark" ></span>
                                <label class="etiquetaCheck">' . $etiquetaForm . '</label>
                            </label>
                            
                        </div>
                        
                    </div>
                </div>';
                        
                        }
                        else{
                            $campos .=
                                '
                <div class="' . $claseAncho . '">
                    <div class="form-group ">
                    ' . $etiquetaForm . '
                    ' . $campoForm . '
                    </div>
                </div>';
                        }
                    }

                    if ($totCampos >= 1 and ($pares == 2 or $i == ($totCampos - 1))) {
                        $pares = 0;
                        $campos .= "\n\t\t\t".'</div>';
                    }
                    $pares++;

                }
            }

        }

        if($swchosen)
        {
            $script .= '
$(function(){
    var config = {
        ".chosen-select": {},
        ".chosen-select-deselect": { allow_single_deselect: true },
        ".chosen-select-no-single": { disable_search_threshold: 10 },
        ".chosen-select-no-results": { no_results_text: "Oops, nothing found!" },
        ".chosen-select-width": { width: "100%" }
    }
    
    for (let selector in config) {
        $(selector).chosen(config[selector]);
    }


})';
        }

        if($swcheck)
        {
            $script .= '
function cambioCheckbox(campo){
    if($("#"+campo+"C").prop("checked")){
        $("#"+campo).val("1");
    }else{
        $("#"+campo).val("0");
    }
}';

        }

        if($swTexto)
        {
            $script .=
                "
                $(function(){
                    $('.editorTexto').summernote({ height: 150 });
                });";
        }

        if($swdate)                               
        {
            $script .=
        "
$(function(){
    let datePickerConfig = {
        format: 'yyyy-mm-dd',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
        showRightIcon: false,
        size: 'small'
    };

    $('.date-picker').each(function(){
        $(this).datepicker(datePickerConfig);
    });
})\n";
        }

        if($swtime)                               
        {
            $script .=
        "
$(function(){
    let timePickerConfig = {
        format: 'HH:mm:ss',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
        showRightIcon: false,
        size: 'small'
    };

    $('.time-picker').each(function(){
        $(this).datepicker(timePickerConfig);
    });
})\n";
        }

        if($swdatetime)
        {
            $script .=
        "
$(function(){
    let datetimePickerConfig = {
        format: 'yyyy-mm-dd HH:mm:ss',
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
        showRightIcon: false,
        size: 'small'
    };

    $('.datetime-picker').each(function(){
        $(this).datepicker(datetimePickerConfig);
    });
})\n";
        }

        return array($primaria, $tabla, $tablamin, $terminacionCampo, $campos, $script, $librerias, $titulo);
    }

    private function crearMultiregistroVista($tabla, $campo, $campoHija, $tablaPadre)
    {
        
        $script = '';
        $librerias = '';
        $tablamin = $tabla;
        $claseAncho = 'col-sm-12';

        $titulos = '';
        $valorDetalle = '';

        $campos = "";
        $etiqueta = "";
        $tipo = "";
        $estilo = "";
        $clase = "";
        $sololectura = "";
        $opciones = "";
        $funciones = "";
        $otrosAtributos = "";
        $anchoTotal = 0;

        $swchosen = false;

        for ($i = 0; $i < count($campo); $i++) {

            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $primaria = $campo[$i]["COLUMN_NAME"];
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$i]["COLUMN_NAME"], 3);
                $tablamin = strtolower($tabla);
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);
                $titulo = ($campo[$i]["TABLE_COMMENT"] == '' ? $terminacionCampo : $campo[$i]["TABLE_COMMENT"]);
            }else{
                $anchoTotal +=  $this->longitudCampo($campo[$i]);
            }


        }
        
        $datosTablaRelacionada = '';
        for ($i = 0; $i < count($campo); $i++) {
            
            if ($campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania' and $campo[$i]["COLUMN_NAME"] != $campoHija) {
                $ancho = round(($this->longitudCampo($campo[$i])  / $anchoTotal) * 100, 0);

                // los campos que se van a validar como obligatorios, van a tener un * en el LABEL
                $Validacion = ($campo[$i]["IS_NULLABLE"] == 'NO' ? 'required' : '');
                $centrado = (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch' ? 'text-center' : '');
                $titulos .= (($primaria == $campo[$i]["COLUMN_NAME"]) ? '' : '<th class="' . $Validacion . ' '. $centrado. '" style="width: ' . $ancho . '%;" >' . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . '</th>');

                switch ($campo[$i]["DATA_TYPE"]) {
                    case 'int':

                        $valorDetalle .= "'0',";
                        // Si el campo tiene un Underline, es porque es una clave foránea, en ese caso  verificamos si es relacion con tabla maestra o es campo
                        // de unión entre una tabla padre y una tabla hija, si es de maestro hacemos lista, sino lo ignoramos
                        if (strpos($campo[$i]["COLUMN_NAME"], '_') !== false) {
                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'select',";
                            $tipo .= "'',";
                            $estilo .= "'',";
                            $clase .= "'chosen-select',";
                            $sololectura .= "false,";
                            $opciones .= $campo[$i]["COLUMN_NAME"] . "lista,";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";

                            $tablaRelacionada = explode('_', $campo[$i]["COLUMN_NAME"])[0];

                            // datos Relacionados
                            $datosTablaRelacionada .=
                                "var oid" . $tablaRelacionada . "lista = '<?php echo isset(\$oid" . $tablaRelacionada . "lista) ? \$oid" . $tablaRelacionada . "lista : \"\";?>';
                            var txNombre" . $tablaRelacionada . "lista = '<?php echo isset(\$txNombre" . $tablaRelacionada . "lista) ? \$txNombre" . $tablaRelacionada . "lista : \"\";?>';
                            var " . $campo[$i]["COLUMN_NAME"] . "lista = [JSON.parse(oid" . $tablaRelacionada . "lista), JSON.parse(txNombre" . $tablaRelacionada . "lista)];";

                        }

                        // si es un campo de checkbox
                        elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ch') {
                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'checkbox',";
                            $tipo .= "'',";
                            $estilo .= "'',";
                            $clase .= "'',";
                            $sololectura .= "false,";
                            $opciones .= "'',";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";
                        }
                        // si es un campo numérico normal
                        else {
                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'input',";
                            $tipo .= (($primaria == $campo[$i]["COLUMN_NAME"]) ? "'hidden'," : "'text',");
                            $estilo .= "'',";
                            $clase .= "'text-right ',";
                            $sololectura .= (($primaria == $campo[$i]["COLUMN_NAME"]) ? "true," : "false,");
                            $opciones .= "'',";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";
                        }
                        break;

                    case 'varchar':

                        $valorDetalle .= "'',";
                        // si es un campo de Lista de opciones fijas SELECCION UNICA
                        if (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'ls') {
                            $opcionesLista = $this->consultarOpcionesListaDetalle($campo[$i]);

                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'select',";
                            $tipo .= "'',";
                            $estilo .= "'',";
                            $clase .= "'chosen-select',";
                            $sololectura .= "false,";
                            $opciones .= $opcionesLista . ",";
                            $funciones .= "'',";
                            $otrosAtributos .= "'multiple',";
                            
                        }
                        // si es un campo de Lista de opciones fijas SELECCION MULTIPLE
                        elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'lm') {
                            $opcionesLista = $this->consultarOpcionesListaDetalle($campo[$i]);

                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'select',";
                            $tipo .= "'',";
                            $estilo .= "'',";
                            $clase .= "'chosen-select',";
                            $sololectura .= "false,";
                            $opciones .= $opcionesLista . ",";
                            $funciones .= "'',";
                            $otrosAtributos .= "'multiple',";

                        } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'cl') {
                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'input',";
                            $tipo .= "'text',";
                            $estilo .= "'',";
                            $clase .= "'',";
                            $sololectura .= "false,";
                            $opciones .= "'',";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";
                        } elseif (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'im') {
                            //Si es imagen, creamos dos campos, uno para seleccionar el archivo y el otro para guardar una vista previa de el
                            //Cuando se entre a editar el registro
                            $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'file',";
                            $tipo .= "'',";
                            $estilo .= "'',";
                            $clase .= "'',";
                            $sololectura .= "false,";
                            $opciones .= "'',";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";

                            $campos .= "'preview" . $campo[$i]["COLUMN_NAME"] . "',";
                            $etiqueta .= "'imagen',";
                            $tipo .= "'imagen',";
                            $estilo .= "'width: 80px; height:35px; display:inline-block;',";
                            $clase .= "'btn-primary',";
                            $sololectura .= "false,";
                            $opciones .= "'',";
                            $funciones .= "'',";
                            $otrosAtributos .= "'',";
                        } else {
                            // en los de tipo varchar, puede estar el campo de Estado, el cual haremos con un campos input readonly y con valor por defecto Activo
                            if ($campo[$i]["COLUMN_NAME"] == 'txEstado' . $terminacionCampo) {
                                $opcionesLista = "[['Activo','Anulado'],['Activo','Anulado']]";

                                $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                                $etiqueta .= "'select',";
                                $tipo .= "'',";
                                $estilo .= "'',";
                                $clase .= "'',";
                                $sololectura .= "false,";
                                $opciones .= $opcionesLista . ",";
                                $funciones .= "'',";
                                $otrosAtributos .= "'',";
                            } else {

                                $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                                $etiqueta .= "'input',";
                                $tipo .= "'text',";
                                $estilo .= "'',";
                                $clase .= "'',";
                                $sololectura .= "false,";
                                $opciones .= "'',";
                                $funciones .= "'',";
                                $otrosAtributos .= "'',";
                            }
                        }

                        break;

                    case 'date':
                        $valorDetalle .= "'',";

                        $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                        $etiqueta .= "'input',";
                        $tipo .= "'text',";
                        $estilo .= "'',";
                        $clase .= "'date-picker',";
                        $sololectura .= "false,";
                        $opciones .= "'',";
                        $funciones .= "'',";
                        $otrosAtributos .= "'',";
                        break;

                    case 'time':
                        $valorDetalle .= "'',";

                        $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                        $etiqueta .= "'input',";
                        $tipo .= "'text',";
                        $estilo .= "'',";
                        $clase .= "'',";
                        $sololectura .= "false,";
                        $opciones .= "'',";
                        $funciones .= "'',";
                        $otrosAtributos .= "'',";
                        break;

                    case 'datetime':
                        $valorDetalle .= "'',";

                        $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                        $etiqueta .= "'input',";
                        $tipo .= "'text',";
                        $estilo .= "'',";
                        $clase .= "'',";
                        $sololectura .= "false,";
                        $opciones .= "'',";
                        $funciones .= "'',";
                        $otrosAtributos .= "'',";
                        break;

                    case 'text':
                        $valorDetalle .= "'',";

                        $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                        $etiqueta .= "'input',";
                        $tipo .= "'text',";
                        $estilo .= "'',";
                        $clase .= "'',";
                        $sololectura .= "false,";
                        $opciones .= "'',";
                        $funciones .= "'',";
                        $otrosAtributos .= "'',";
                        break;

                    case 'decimal':
                        $valorDetalle .= "'',";

                        $campos .= "'" . $campo[$i]["COLUMN_NAME"] . "',";
                        $etiqueta .= "'input',";
                        $tipo .= "'text',";
                        $estilo .= "'',";
                        $clase .= "'',";
                        $sololectura .= "false,";
                        $opciones .= "'',";
                        $funciones .= "'',";
                        $otrosAtributos .= "'',";
                        break;
                }
            }

        }

        $titulos = '
                    <div class="row">
                        <div class="col-md-12 pt-3">
                            <input type="hidden" name="eliminar'.$terminacionCampo.'" id="eliminar'.$terminacionCampo.'"/>
                            <table class="table table-hover table-borderless table-sm">
                                <thead class="bg-primary text-light">
                                    <tr>
                                        <th>
                                            <button class="btn btn-primary btn-sm" onclick="' . $tablamin . '.agregarCampos(valor' . $terminacionCampo . ', \'A\');">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </th>
                                        ' . $titulos . '
                                    </tr>
                                </thead>
                                <tbody id="contenedor_' . $tablamin . '"></tbody>
                            </table>
                        </div>
                    </div>';

        $tablaPadre = explode('_',$tablaPadre)[1];
        $datosMulti = 
                " 
    <script>
        var $terminacionCampo = {!! $$tablaPadre->$terminacionCampo !!};
    
        $datosTablaRelacionada
    </script>";
        
        
        $configMulti =
            "var valor$terminacionCampo = [$valorDetalle];
var $tablamin;
$(function(){

    $tablamin = new GeneradorMultiRegistro('$tablamin','contenedor_$tablamin','" . $tablamin . "_');

    $tablamin.altura = '35px;';
    $tablamin.campoid = '$primaria';
    $tablamin.campoEliminacion = 'eliminar$terminacionCampo';
    $tablamin.botonEliminacion = true;
    $tablamin.funcionEliminacion = '';

    $tablamin.campos = [$campos];

    $tablamin.etiqueta = [$etiqueta];
    $tablamin.tipo = [$tipo];
    $tablamin.estilo = [$estilo];
    $tablamin.clase = [$clase];
    $tablamin.sololectura = [$sololectura];

    $tablamin.opciones = [$opciones];

    $tablamin.funciones = [$funciones];
    $tablamin.otrosAtributos = [$otrosAtributos];

    $terminacionCampo.forEach(".$terminacionCampo."reg => {
        $tablamin.agregarCampos(".$terminacionCampo."reg,'L');
    });

});";

        return array('', '', '', $terminacionCampo, $titulos, '', $configMulti, $titulo, $datosMulti);
    }


    private function crearCamposJS($campo, $tipoRelacion)
    {
        $validacion = '';
        if ($tipoRelacion != ' ') {
            
            //recorremos los campos de la tabla, aplicando las validaciones
            for ($i = 0; $i < count($campo); $i++) {
                if ($campo[$i]["IS_NULLABLE"] == 'NO' and substr($campo[$i]["COLUMN_NAME"], 0, 3) != 'oid' and $campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {

                    $validacion .= 
'let '.$campo[$i]["COLUMN_NAME"].'_Input = $("#'.$campo[$i]["COLUMN_NAME"].'");
        let '.$campo[$i]["COLUMN_NAME"].'_AE = '.$campo[$i]["COLUMN_NAME"].'_Input.val();
        '.$campo[$i]["COLUMN_NAME"].'_Input.removeClass(errorClass);
        if(!'.$campo[$i]["COLUMN_NAME"].'_AE){
            '.$campo[$i]["COLUMN_NAME"].'_Input.addClass(errorClass)
            mensajes.push("El campo '.explode('_',$campo[$i]["COLUMN_COMMENT"])[0].' es obligatorio");
        }'."\n\t\t";
                }
            }

        } else {
            

        }

        return $validacion;
    }


    private function generarVista($tabla, $campo, $estilo, $modulo)
    {

        $campos = '';
        // Creamos los campos de la tabla Principal
        $datos = $this->crearCamposVista($tabla, $campo, $tabla);

        $primaria = $datos[0];
        $tabla = $datos[1];
        $tablamin = $datos[2];
        $terminacionCampo = $datos[3];
        $campos = $datos[4];
        $script = $datos[5];
        $librerias = $datos[6];
        $titulo = $datos[7];

        // con la variable TerminacionCampo, Verificamos si el nombre de la tabla, lo encontramos en otras tablas de la misma base de datos com ouna clave foránea
        // lo que indicaria que esa tabla es parte de este formulario y la debemos anexar, ya sea como una multiregistro o como
        // campos simples, esto depende del nombre de la clave foránea que encontremos
        $consulta = $this->consultarTablas($campo[0]["TABLE_SCHEMA"], $terminacionCampo);

        // $consulta = DB::table('information_schema.COLUMNS')
        //     ->select(DB::raw('TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME'))
        //     ->where('COLUMN_NAME', "like", $terminacionCampo.'\_oid'.$terminacionCampo.'\_%')
        //     ->where('TABLE_SCHEMA', "=", $campo[0]["TABLE_SCHEMA"])
        //     ->orderby('TABLE_NAME')
        //     ->get();

        $camposdetalle = '';
        switch ($estilo) {
            case 'tabs':
                $tabs = "<ul class=\"nav nav-tabs\">";
                break;

            case 'panel':
                $tabs = '';
                break;
            
            case 'card':
                $tabs = '';
                break;

            case 'acordeon':

                break;
        }

        for ($i = 0; $i < count($consulta); $i++) {
            $tablaHija = get_object_vars($consulta[$i])["TABLE_NAME"];
            $basedatosHija = get_object_vars($consulta[$i])["TABLE_SCHEMA"];
            $campoHija = get_object_vars($consulta[$i])["COLUMN_NAME"];
            $tipoRelacion = explode('_', $campoHija)[2];

            // consultamos la estructura de la tabla y con esta creamos los campos para la vista

            $consultaCampos = $this->consultarCampos($tablaHija, $basedatosHija);

            $camposBD = array();
            for ($j = 0; $j < count($consultaCampos); $j++) {

                $camposBD[] = get_object_vars($consultaCampos[$j]);
                // Creamos los campos de la tabla Secundaria
            }

            // segun el tipo de relacion entre las tablas creamos de forma diferente los campos
            if ($tipoRelacion == '1a1') {
                // si es relacion 1 a 1, creamos campos Simples
                $datos = $this->crearCamposVista($camposBD[0]["TABLE_NAME"], $camposBD, $tablamin);
                $campoEliminacion = '';
                $script .= $datos[5];
            } else {
                // si es una relacion de 1 a Muchos, creamos una multiregistro
                $datos = $this->crearMultiregistroVista($camposBD[0]["TABLE_NAME"], $camposBD, $campoHija, $tablamin);
                $campoEliminacion = "<input type=\"hidden\" id=\"eliminar" . $datos[3] . "\" name=\"eliminar" . $datos[3] . "\" value=\"\">";
                $script .= $datos[6];
                $librerias .= $datos[8];
            }

            switch ($estilo) {
                case 'tabs':
                    $tabs .= "<li " . ($i == 0 ? 'class="active"' : '') . "><a data-toggle=\"tab\" href=\"#$i\">" . $datos[7] . "</a></li>";

                    $camposdetalle .=
                        "
                                <div id=\"$i\" class=\"tab-pane fade " . ($i == 0 ? 'in' : '') . " " . ($i == 0 ? 'active' : '') . "\">
                                $campoEliminacion
                                " . $datos[4] . "
                                </div>
                            ";
                    break;

                case 'panel':
                    $tabs .= '';
                    $camposdetalle .=
                "<div class=\"panel panel-primary\">
                        <div class=\"panel-heading\">" . $datos[7] . "</div>
                        $campoEliminacion
                        <div class=\"panel-body\">" . $datos[4] . "</div>
                    </div>";
                    break;

                case 'card':
                    $tabs .= '';
                    $camposdetalle .= 
                    
                    '<h1 class="h3 mb-2 text-gray-800">'.$datos[7].'</h1>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">      
                    '. $datos[4].'
                </div>
            </div>';
                    break;
            }

            // adicionamos los campos creados a la lista general para luego ponerlos en el formulario

        }

        switch ($estilo) {
            case 'tabs':
                $tabs .= "</ul>
                            <div class=\"tab-content\">";
                $camposdetalle .= "</div>";
                break;

            case 'panel':
                $tabs .= '';
                break;

            case 'card':
                $tabs .= '';
                break;

            case 'acordeon':

                break;
        }

        $slash = DIRECTORY_SEPARATOR;
        $raiz = (str_replace('MasterDev' . $slash . 'Http' . $slash . 'Controllers' . $slash . '', '', dirname(realpath(__FILE__)) . $slash));

        // creamos el archivo de controlador
        $archForm = fopen($raiz . $modulo . $slash . 'Resources' . $slash . 'views' . $slash . lcfirst($terminacionCampo) . 'Form.blade.php', "w");

        fwrite($archForm, 
'@extends("layouts.principal")
@section("nombreModulo")
    ' . $titulo . '
@endsection
@section("scripts")
    {{Html::script("modules/'.strtolower($modulo).'/js/' . lcfirst($terminacionCampo) . 'Form.js")}}
    ' . $librerias . '
@endsection
@section("contenido")
    @if(isset($' . strtolower($terminacionCampo) . '->oid' . $terminacionCampo . '))
        {!!Form::model($' . strtolower($terminacionCampo) . ',["route"=>["' . strtolower($terminacionCampo) . '.update",$' . strtolower($terminacionCampo) . '->oid' . $terminacionCampo . '],"method"=>"PUT", "id"=>"form-' . strtolower($terminacionCampo) . '" , "onsubmit" => "return false;"])!!}
    @else
        {!!Form::model($' . strtolower($terminacionCampo) . ',["route"=>["' . strtolower($terminacionCampo) . '.store",$' . strtolower($terminacionCampo) . '->oid' . $terminacionCampo . '],"method"=>"POST", "id"=>"form-' . strtolower($terminacionCampo) . '", "onsubmit" => "return false;"])!!}
    @endif
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}">

            ' . $campos . '
            '. $camposdetalle.'

            @if(isset($' . strtolower($terminacionCampo) . '->oid' . $terminacionCampo . '))
                {!!Form::button("Modificar",["type" => "button" ,"class"=>"btn btn-primary", "onclick"=>"grabar()"])!!}
            @else
                {!!Form::button("Adicionar",["type" => "button" ,"class"=>"btn btn-success", "onclick"=>"grabar()"])!!}
            @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection
        ');
        fclose($archForm);

        $raiz = (str_replace('MasterDev' . $slash . 'Http' . $slash . 'Controllers' . $slash . '', '', dirname(realpath(__FILE__)) . $slash));

        // creamos el archivo de controlador
        $archJS = fopen($raiz . $modulo . $slash . 'Resources' . $slash . 'assets' . $slash .'js'. $slash . lcfirst($terminacionCampo) . 'Form.js', "w");

        fwrite($archJS, 
'"use strict";
var errorClass = "is-invalid";

'.$script.'


function grabar(){
    modal.cargando();
    let mensajes = validarForm();
    if( mensajes && mensajes.length){
        modal.mostrarErrores(mensajes);
    }else{
        let formularioId = "#form-'.strtolower($terminacionCampo).'";
        let route = $(formularioId).attr("action");
        let data = $(formularioId).serialize();
        $.post(route,data, function( resp ){
            modal.establecerAccionCerrar(function(){
                location.href = "/'.strtolower($modulo).'/'.strtolower($terminacionCampo).'";
            });
            modal.mostrarModal("Informacion" , "<div class=\"alert alert-success\">Los datos han sido guardados correctamente</div>");
        },"json").fail( function(resp){
            $.each(resp.responseJSON.errors, function(index, value) {
                mensajes.push( value );
                $("#"+index).addClass(errorClass);
            });
            modal.mostrarErrores(mensajes);
        });
    }

    function validarForm(){
        
        let mensajes =[];
        '.$this->crearCamposJS($campo, $tipoRelacion = '')
        
        .'return mensajes;
    }
}');

        fclose($archJS);
    }

    private function crearCamposRequest($tabla, $campo, $tipoRelacion, $validCompania)
    {

        $validaciones = '';
        $mensajes = '';

        //recorremos los campos de la tabla, el que empieza por oid lo guardamos como campo primarykey
        for ($i = 0; $i < count($campo); $i++) {
            if (substr($campo[$i]["COLUMN_NAME"], 0, 3) == 'oid') {
                $primaria = $campo[$i]["COLUMN_NAME"];
                $tabla = ucfirst(explode('_', $tabla)[0]) . '_' . substr($campo[$i]["COLUMN_NAME"], 3);
                $tablamin = strtolower($tabla);
                $terminacionCampo = substr($campo[$i]["COLUMN_NAME"], 3);
            }
        }

        if ($tipoRelacion != '1aM') {
            //recorremos los campos de la tabla, aplicando las validaciones
            for ($i = 0; $i < count($campo); $i++) {
                $validacionTipo = '';
                if ($campo[$i]["IS_NULLABLE"] == 'NO' and substr($campo[$i]["COLUMN_NAME"], 0, 3) != 'oid' and $campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {
                    switch ($campo[$i]["DATA_TYPE"]) {
                        case 'varchar':
                            if (substr($campo[$i]["COLUMN_NAME"], 0, 2) == 'lm') {
                                $validacionTipo = '';
                                $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".string\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede contener valores seleccionados de la lista\";\n";
                            } else {
                                $validacionTipo = '|string';
                                $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".string\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede contener caracteres afabéticos, númericos, guión y guión bajo\";\n";
                            }
                            break;

                        case 'date':
                            $validacionTipo = '|date';
                            $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".date\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede contener una fecha en el formato AAAA-MM-DD\";\n";
                            break;

                        case 'datetime':
                            $validacionTipo = '|date';
                            $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".date_format\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede conteneruna fecha en el formato AAAA-MM-DD HH:MM:SS\";\n";
                            break;

                        case 'int':
                            $validacionTipo = '|integer';
                            $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".integer\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede contener un valor entero\";\n";
                            break;

                        case 'decimal':
                            $validacionTipo = '|numeric';
                            $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".numeric\"] =  \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " Solo puede contener un valor numérico con decimales\";\n";
                            break;

                    }
                }

                // NO validamos el campo de OID autonumerico
                if (substr($campo[$i]["COLUMN_NAME"], 0, 3) != 'oid') {
                    // Si el campo es el Código, lo validamos como Unico
                    if (substr($campo[$i]["COLUMN_NAME"], 0, 8) == 'txCodigo' and $campo[$i]["IS_NULLABLE"] == 'NO') {

                        //$validCompania .= "Compania_oidCompania, \". (\Session::get('oidCompania')),";

                        $validaciones .= "\"" . $campo[$i]["COLUMN_NAME"] . "\" => \"required" . $validacionTipo . "|unique:$tablamin," . $campo[$i]["COLUMN_NAME"] . ",\".\$this->get('" . $primaria . "') .\"," . $primaria . ($validCompania == '' ? '",' : ',') . $validCompania . "\n";
                        $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".required\"] =  \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " es obligatorio\";\n";
                        $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".unique\"] =  \"El valor ingresado en el campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " ya existe, éste debe ser único\";\n";
                    } // los campos que contienen underline, siempre y cuando no sea campos de relacion con una tabla padre, lo validamos como obligatorio
                    elseif (strpos($campo[$i]["COLUMN_NAME"], '_') !== false and $campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {
                        // tomamos el nombre de la tabla a consultar (la palabra que esta antes del underline)
                        $tablaRelacionada = explode('_', $campo[$i]["COLUMN_NAME"])[0];
                        // tomamos el nombre del campo (despues del underline)
                        $nombreCampo = substr(explode('_', $campo[$i]["COLUMN_NAME"])[1], 3);

                        // si la tabla relacionada y el campo son iguales, es porque este Foreign key esta apuntando a una tabla Padre (relacion uno a muchos)
                        // si son diferentes es porque esta apuntando a una relacion con una tabla maestra (que son las que nos interesa validar obligatorias)
                        if ($tablaRelacionada != $nombreCampo AND $tablaRelacionada != 'remember' and $campo[$i]["IS_NULLABLE"] == 'NO') {
                            $validaciones .= "\t\t\t\t\t\t\"" . $campo[$i]["COLUMN_NAME"] . "\" => \"required" . $validacionTipo . "\",\n";
                            $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".required\"] =  \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " es obligatorio\";\n";
                        }
                    } // los demas campos los validamos como obligatorios dependiendo si permiten o no NULL
                    elseif ($campo[$i]["IS_NULLABLE"] == 'NO' and $campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania') {
                        $validaciones .= "\t\t\t\t\t\t\"" . $campo[$i]["COLUMN_NAME"] . "\" => \"required" . $validacionTipo . "\",\n";
                        $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . ".required\"] =  \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " es obligatorio\";\n";
                    }
                }
            }

        } else {
            // las validaciones de campos de Multiregistro (relacion 1 a muchos) , se validan de forma diferente
            // recorriendolos (en el reques que vamos a crear)
            $validaciones =
                "\$reg = count(\$this->get('oid$terminacionCampo'));
                for(\$i = 0; \$i < \$reg; \$i++)
                {\n";

            $mensajes =
                "\$reg = count(\$this->get('oid$terminacionCampo'));
                    for(\$i = 0; \$i < \$reg; \$i++)
                    {\n";

            for ($i = 0; $i < count($campo); $i++) {

                // aplicamos validacion SI el campo no permite NULL, no es el campo de OID, no es el campo de compania y no es el campo de relacion con la tabla padre
                if ($campo[$i]["IS_NULLABLE"] == 'NO' and
                    $campo[$i]["COLUMN_NAME"] != 'oid' . $terminacionCampo and
                    $campo[$i]["COLUMN_NAME"] != 'Compania_oidCompania' and
                    !isset(explode("_", $campo[$i]["COLUMN_NAME"])[2])
                ) {
                    $validaciones .=
                        "\n\t\t\t\t\tif(trim(\$this->get('" . $campo[$i]["COLUMN_NAME"] . "')[\$i]) == '' )
                                \$validacion['" . $campo[$i]["COLUMN_NAME"] . "'.\$i] =  'required';\n";
                    $mensajes .= "\t\t\t\t\$mensaje[\"" . $campo[$i]["COLUMN_NAME"] . "\".\$i.\".required\"] = \"El campo " . explode('_',$campo[$i]["COLUMN_COMMENT"])[0] . " en la línea \".(\$i + 1).\" es obligatorio\";\n";
                }
            }

            $validaciones .= "\t\t\t\t}";
            $mensajes .= "\t\t\t\t}";

        }

        return array($tabla, $terminacionCampo, $validaciones, $mensajes);
    }

    private function generarRequest($tabla, $campo, $modulo)
    {

        $validCompania = '';

        // la verificación del campo de compania lo hacemos en un ciclo aparte porque normalmente este es de los ultimos campos de la tabla
        // y necesitamos verificar si lo tiene o no
        for ($i = 0; $i < count($campo); $i++) {
            // si la tabla tiene campo de id compania, validamos el campo Codigo como UNICO teniendo en cuenta que sea UNICO por compania
            // si no tiene compania, sería UNICO general
            if ($campo[$i]["COLUMN_NAME"] == 'Compania_oidCompania') {
                $validCompania .= "Compania_oidCompania, \". (\Session::get('oidCompania')),";
            }
        }

        $datos = $this->crearCamposRequest($tabla, $campo, $tipoRelacion = '', $validCompania);

        $validacionesDetalle = '';
        $mensajesDetalle = '';

        $tabla = $datos[0];
        $terminacionCampo = $datos[1];
        $validacionesEncabezado = $datos[2];
        $mensajesEncabezado = $datos[3];

        //*******************************
        // TABLAS HIJAS
        //*******************************
        // con la variable TerminacionCampo, Verificamos si el nombre de la tabla, lo encontramos en otras tablas de la misma base de datos com ouna clave foránea
        // lo que indicaria que esa tabla es parte de este formulario y la debemos anexar, ya sea como una multiregistro o como
        // campos simples, esto depende del nombre de la clave foránea que encontremos
        $consulta = $this->consultarTablas($campo[0]["TABLE_SCHEMA"], $terminacionCampo);
        // $consulta = DB::table('information_schema.COLUMNS')
        //     ->select(DB::raw('TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME'))
        //     ->where('COLUMN_NAME', "like", $terminacionCampo.'\_oid'.$terminacionCampo.'\_%')
        //     ->where('TABLE_SCHEMA', "=", $campo[0]["TABLE_SCHEMA"])
        //     ->orderby('TABLE_NAME')
        //     ->get();

        // recorremos cada una de las tablas hijas
        for ($i = 0; $i < count($consulta); $i++) {
            $tablaHija = get_object_vars($consulta[$i])["TABLE_NAME"];
            $basedatosHija = get_object_vars($consulta[$i])["TABLE_SCHEMA"];
            $campoHija = get_object_vars($consulta[$i])["COLUMN_NAME"];
            $tipoRelacion = explode('_', $campoHija)[2];

            //*******************************
            // ESTRUCTURA DE LA TABLA HIJA
            //*******************************
            // consultamos la estructura de la tabla y con esta creamos los campos para el controller
            $consultaCampos = $this->consultarCampos($tablaHija, $basedatosHija);
            // $consultaCampos = DB::table('information_schema.COLUMNS')
            //     ->select(DB::raw('COLUMNS.TABLE_SCHEMA, COLUMNS.TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, COLUMN_TYPE, COLUMN_COMMENT, COLUMN_DEFAULT, TABLE_COMMENT, IS_NULLABLE'))
            //     ->leftjoin('information_schema.TABLES','COLUMNS.TABLE_NAME','=','TABLES.TABLE_NAME')
            //     ->where('COLUMNS.TABLE_NAME', "=", $tablaHija)
            //     ->where('COLUMNS.TABLE_SCHEMA', "=", $basedatosHija)
            //     ->orderby('COLUMNS.TABLE_NAME')
            //     ->get();

            $camposBD = array();
            for ($j = 0; $j < count($consultaCampos); $j++) {
                $camposBD[] = get_object_vars($consultaCampos[$j]);
            }

            //generamos los campos para el crear Controlador de la tabla hija
            $datos = $this->crearCamposRequest($tabla, $camposBD, $tipoRelacion, $validCompania);

            // dependiendo del tipo de relacion, guardamos el resultado en diferente variable
            if ($tipoRelacion == '1aM') {
                $validacionesDetalle .= $datos[2];
                $mensajesDetalle .= $datos[3];
            } else {
                $validacionesEncabezado .= $datos[2];
                $mensajesEncabezado .= $datos[3];
            }

        }

        $slash = DIRECTORY_SEPARATOR;
        $raiz = (str_replace('MasterDev' . $slash . 'Http' . $slash . 'Controllers' . $slash . '', '', dirname(realpath(__FILE__)) . $slash));
        $arch = fopen($raiz . $modulo . $slash . 'Http' . $slash . 'Requests' . $slash . lcfirst($terminacionCampo) . 'Request.php', "w");

        fwrite($arch, "<?php

        namespace Modules\\" . $modulo . "\Http\Requests;
        use Illuminate\Foundation\Http\FormRequest;

        class " . $terminacionCampo . "Request extends FormRequest
        {
            /**
             * Determine if the user is authorized to make this request.
             *
             * @return bool
             */
            public function authorize()
            {
                return true;
            }
        ");

        fwrite($arch, "
            /**
            * Get the validation rules that apply to the request.
            *
            * @return array
            */
            public function rules()
            {
                \$validacion = array(
                $validacionesEncabezado
                );

                $validacionesDetalle

                return \$validacion;
            }

        ");

        fwrite($arch, "

            public function messages()
            {
                \$mensaje = array();
                $mensajesEncabezado

                $mensajesDetalle

                return \$mensaje;
            }
        }
        ");

        fclose($arch);

    }
}
