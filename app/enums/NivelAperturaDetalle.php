<?php

namespace App\Enums;

abstract class NivelAperturaDetalle {
    const REFERENCIA = "R*";
    const REFERENCIA_TALLA = self::REFERENCIA."T*";
    const REFERENCIA_COLOR = self::REFERENCIA."C*";
    const REFERENCIA_TALLA_COLOR = self::REFERENCIA_TALLA."C*";


    const NivelesDetalle = [
        self::REFERENCIA => "Referencia", 
        self::REFERENCIA_TALLA => "Referencia/Talla" , 
        self::REFERENCIA_COLOR => "Referencia/Color" ,
        self::REFERENCIA_TALLA_COLOR => "Referencia/Talla/Color"
    ];
}