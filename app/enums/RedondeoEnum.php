<?php
namespace App\Enums;
/**
 * Enum creada para mantener referencia estatica a los valores que pueden
 * 
 */
abstract class RedondeoEnum{
    const __default = self::ENTERO;
    const ENTERO = 0;
    const UN_DECIMAL = 1;
    const DOS_DECIMALES = 2;
    const DECENA = -1;
    const CENTENA = -2;
    const MILES = -3;
    const MILLONES = -4;


    public static function getLabelByValue($value){
        switch($value){
            default:
                return "Entero";
            case self::MILLONES:
                return "Millones";
            case self::MILES:
                return "Miles";
            case self::CENTENA:
                return "Centena";
            case self::DECENA:
                return "Decena";
            case self::UN_DECIMAL:
                return "Un decimal";
            case self::DOS_DECIMALES:
                return "Dos decimales";
        }
    }

    public static function getValues(){
        return [
            self::DOS_DECIMALES,
            self::UN_DECIMAL,
            self::ENTERO,
            self::DECENA,
            self::CENTENA,
            self::MILES,
            self::MILLONES
        ];
    }

    public static function pluck(){
        $retorno = [];
        foreach( self::getValues() as $value ){
            $retorno[$value] = self::getLabelByValue($value);
        }
        return $retorno;
    }
}