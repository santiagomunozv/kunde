<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class EmailService
{
  /**
   * Envía un correo electrónico.
   *
   * @param array|string $destinatarios Dirección(es) de correo (puede ser string o un array).
   * @param string $asunto Asunto del correo.
   * @param string $mensaje Contenido HTML del mensaje.
   * @param array $adjuntos Opcional. Arreglo de rutas de archivos a adjuntar.
   * @return void
   */
  public function sendEmail($destinatarios, $asunto, $mensaje, $adjuntos = [])
  {
    Mail::send('mail.email', ['mensaje' => $mensaje], function ($msj) use ($destinatarios, $asunto, $adjuntos) {
      $msj->to($destinatarios)
        ->subject($asunto);

      if (!empty($adjuntos)) {
        foreach ($adjuntos as $archivo) {
          $msj->attach($archivo);
        }
      }
    });
  }
}
