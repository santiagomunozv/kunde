<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;
use Throwable;

// class EmpleadoImport implements WithMultipleSheets
// {
    

//     public function sheets(): array
//     {
//         return [
//             0 => new FirstSheetImport()
//         ];
//     }

// }

// class FirstSheetImport implements ToModel
// {

//     public function model(array $row)
//     {
//         return new TerceroEmpleadoModel();
//     }
// }

// class SecondSheetImport implements ToModel
// {
//     public function model(array $row)
//     {
//         return new TerceroEmpleadoModel();
//     }

// }

// class ThirdSheetImport implements ToModel
// {
//     public function model(array $row)
//     {
//         return new TerceroEmpleadoModel();
//     }

// }


class EmpleadoImport implements ToModel, WithHeadingRow, SkipsOnError, WithValidation, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;



    public function model(array $row)
    {

        return new TerceroEmpleadoModel([
            // 'TipoIdentificacion_oidTerceroEmpleado' => $row[3],

        ]);
    }



    public function rules(): array
    {
        return [
            // validar por lotes 
            // Fila.columna

            // validar encabezados
            // '0.0' => 'in:TipoDocumento',
            // '0.1' => 'in:Documento',
            // '0.2' => 'in:Nombre',
            // '0.3' => 'in:Apellido',

            // '*.documento' => ['required', 'unique:asn_terceroempleado,txDocumentoEmpleado']

        ];
    }

 
}
