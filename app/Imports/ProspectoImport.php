<?php

namespace App\Imports;

use Modules\RegistroSeguimiento\Entities\ProspectoModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProspectoImport implements ToModel, WithHeadingRow, WithValidation
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProspectoModel([
            'txNombreProspecto' => $row['nombre'],
            'inTelefonoProspecto' => $row['telefono'],
        ]);
    }

    public function rules(): array
    {
        return [
            'nombre' => 'required',
            'telefono' => 'required',
        ];
    }
}
