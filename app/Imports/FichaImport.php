<?php

namespace App\Imports;

use Modules\FichaTecnica\Entities\FichaTecnicaModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ToModel;

class FichaImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            0 => new FirstSheetImport()
        ];
    }
}

class FirstSheetImport implements ToModel
{

    public function model(array $row)
    {
        return new FichaTecnicaModel();
    }

}

class SecondSheetImport implements ToModel
{
    public function model(array $row)
    {
        return new FichaTecnicaModel();
    }

}

class ThirdSheetImport implements ToModel
{
    public function model(array $row)
    {
        return new FichaTecnicaModel();
    }

}