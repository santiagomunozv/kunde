<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ToModel;
use Modules\AsociadoNegocio\Entities\TerceroPeriodicidadModel;

class TareaImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            0 => new FirstSheetImport()
        ];
    }
}

class FirstSheetImport implements ToModel
{

    public function model(array $row)
    {
        return new TerceroPeriodicidadModel();
    }
}

class SecondSheetImport implements ToModel
{
    public function model(array $row)
    {
        return new TerceroPeriodicidadModel();
    }
}

class ThirdSheetImport implements ToModel
{
    public function model(array $row)
    {
        return new TerceroPeriodicidadModel();
    }
}
