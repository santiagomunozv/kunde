<?php

    namespace App\utils;

use Illuminate\Support\Facades\DB;

class LoginFactory{

        public static function buildOpcion($rows){
            $modulosAssoc = [];
            foreach($rows as $modulo){
                $key = $modulo->oidModulo;
                if(!array_key_exists($key , $modulosAssoc)){
                    $modulosAssoc[$key] = collect();
                }
                $modulosAssoc[$key]->push($modulo);
            }
            return $modulosAssoc;
        }
    
        public static function buildModulos($rows){
            $modulosAssoc = [];
            $modulosAdded = [];
            foreach($rows as $modulo){
                $key = $modulo->oidPaquete;
                if(!array_key_exists($key , $modulosAssoc)){
                    $modulosAssoc[$key] = collect();
                }
                if(!in_array($modulo->oidModulo , $modulosAdded)){
                    $modulosAssoc[$key]->push($modulo);
                    array_push($modulosAdded , $modulo->oidModulo);
                }
            }
            return $modulosAssoc;
        }
    
        public static function buildPaquetes($rows){
            $paquetesAssoc = [];
            foreach($rows as $paquete){
                $key = $paquete->oidPaquete;
                if(!array_key_exists($key , $paquetesAssoc)){
                    $paquetesAssoc[$key] = $paquete;
                }
            }
            return $paquetesAssoc;
        }

        public static function getMenuData($rol){
            return $rol ? DB::select("SELECT oidPaquete,txNombrePaquete,oidModulo,txNombreModulo,txIconoModulo,txNombreOpcion,txRutaOpcion
            FROM seg_rolopcion
            join seg_opcion on Opcion_oidRolOpcion = oidOpcion
            join seg_modulo on oidModulo = Modulo_oidModulo_1aM
            join seg_paquete on oidPaquete = Paquete_oidPaquete_1aM
            where Rol_oidRol_1aM = $rol 
            order by inOrdenPaquete,txNombreModulo,txNombreOpcion") : [];
        }

    }