<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use App\AuthService;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct(){
        $this->middleware('guest',['only'=>'mostrarLoginForm']);
    }

    /**
     * Controlador empleado para recibir y procesar las solicitudes de logueo en la aplicación
     * Requisitos para hacer login:
     * 
     *  - El usuario debe tener un Tercero asociado
     *  - El usuario debe tener uno o varios registros en la tabla seg_usuariocompaniarol
     */
    public function login(){
        $credenciales = $this->validate(request(), [
            'txLoginUsuario' => 'required|string',
            'password' => 'required|string'
        ]);
        //si no se tienen datos en la tabla seg_usuariocompaniarol no dejar pasar el login
        if( Auth::attempt($credenciales) && AuthService::setSessionModels()){
            return redirect("/");
        }
        self::endSessionObjects();
        return back()->withErrors(['authFailed' => trans('auth.failed')]);
    }

    public function cambioCompania(Request $request){
        $idCompania= $request->get("compania");
        //si no se encuentran datos para la compania indicada redireccionar a 
        //login
        if($idCompania && AuthService::setSessionModelsByCompania($idCompania)){
            return redirect("/");
        }
        self::endSessionObjects();
        return redirect('/login');
    }
    
    public function logout(){
        self::endSessionObjects();
        return redirect('/login');
    }

    public function mostrarLoginForm(){
        session(['link' => url()->previous()]); 
        return view('auth.login');  
    }

    private static function endSessionObjects(){
        Session::flush();
        Auth::logout();
    }
}