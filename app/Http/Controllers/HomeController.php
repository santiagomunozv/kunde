<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        $AND = '';
        $tipo = (isset($_GET['tipo']) ? $_GET['tipo'] : '');
        if (Session::get('rolUsuario') != 1) {
            $AND = " AND txResponsableTarea REGEXP '" . Session::get('oidTercero') . "'";
        }

        $tareas = DB::Select(
            "SELECT
                oidTarea,
                txAsuntoTarea,
                txPrioridadTarea,
                dtFechaVencimientoTarea
            FROM
                rgs_tarea t
            WHERE dtFechaVencimientoTarea BETWEEN CURDATE() AND DATE_ADD(CURDATE(),INTERVAL 7 DAY)
                AND txFechaSolucionTarea IS NULL AND estadoAprobador != 'Pendiente'
                $AND
            GROUP BY
                t.oidTarea "
        );

        $publicaciones = DB::Select(
            "SELECT
                    oidPublicacionImagen,
                    txRutaPublicacionImagen
                FROM
                    geh_publicacion p
                    JOIN geh_publicacionimagen pi ON p.oidPublicacion = pi.Publicacion_oidPublicacion
                WHERE
                    dtInicioPublicacion <= curdate() AND dtFinPublicacion >= curdate()
                    AND lsTipoPublicacion = 'Cartelera'"
        );

        $metaLlamadas = DB::table('rgs_jornadaempleado as je')
            ->select(DB::raw("CASE
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaLlamadasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaLlamadasJornadaEmpleado
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaLlamadasJornadaEmpleado / 5
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaLlamadasJornadaEmpleado / 11
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaLlamadasJornadaEmpleado / 22
                        END AS metaLlamadas"))
            ->where('je.Tercero_oidTerceroEmpleado', Session::get('oidTercero'))
            ->first();
        $metaLlamadas = $metaLlamadas ? number_format($metaLlamadas->metaLlamadas, 0) : 0;

        $metasCitas = DB::table('rgs_jornadaempleado as je')
            ->select(DB::raw("CASE
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Hora' THEN je.inMetaCitasJornadaEmpleado * inHorasLaboralesJornadaEmpleado
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Día' THEN je.inMetaCitasJornadaEmpleado
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Semana' THEN je.inMetaCitasJornadaEmpleado / 5
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Quincena' THEN je.inMetaCitasJornadaEmpleado / 11
                            WHEN txUnidadMedidaLlamadasJornadaEmpleado = 'Mes' THEN je.inMetaCitasJornadaEmpleado / 22
                        END AS metasCitas"))
            ->where('je.Tercero_oidTerceroEmpleado', Session::get('oidTercero'))
            ->first();
        $metasCitas = $metasCitas ? number_format($metasCitas->metasCitas, 0) : 0;

        $llamadasRealizadas = DB::table('rgs_prospecto as p')
            ->join('rgs_prospectoseguimiento as ps', 'p.oidProspecto', '=', 'ps.Prospecto_oidProspecto')
            ->select(DB::raw('count(ps.oidProspectoSeguimiento) as llamadasRealizadas'))
            ->where('p.Tercero_oidEmpleado', Session::get('oidTercero'))
            ->whereDate('ps.dtFechaSeguimientoProspectoSeguimiento', DB::raw('CURDATE()'))
            ->first();
        $llamadasRealizadas = $llamadasRealizadas ? $llamadasRealizadas->llamadasRealizadas : 0;


        $llamadasLogradas = DB::table('rgs_prospecto as p')
            ->join('rgs_prospectoseguimiento as ps', 'p.oidProspecto', '=', 'ps.Prospecto_oidProspecto')
            ->join('asn_tercero as t', 'p.Tercero_oidEmpleado', '=', 't.oidTercero')
            ->join('rgs_resultados as rs', 'ps.Resultado_oidResultado', '=', 'rs.oidResultado')
            ->select(DB::raw("IFNULL(SUM(IF(rs.lsEstadoEmpleadoResultado = 'Exitoso', 1, 0)), 0) as llamadasLogradas"))
            ->where('p.Tercero_oidEmpleado', Session::get('oidTercero'))
            ->whereDate('ps.dtFechaSeguimientoProspectoSeguimiento', DB::raw('CURDATE()'))
            ->first();
        $llamadasLogradas = $llamadasLogradas ? $llamadasLogradas->llamadasLogradas : 0;

        $avanceLlamadas = number_format($llamadasRealizadas * 100 / ((bool)!$metaLlamadas ? 1 : $metaLlamadas), 2, '.', ',');
        $avanceCitas = number_format($llamadasLogradas * 100 / ((bool)!$metasCitas ? 1 : $metasCitas), 2, '.', ',');


        return view('layouts.carousel', compact('tareas', 'publicaciones', 'metaLlamadas', 'llamadasRealizadas', 'avanceLlamadas', 'metasCitas', 'llamadasLogradas', 'avanceCitas'));
    }
}
