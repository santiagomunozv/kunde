<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GeneralController extends Controller{

    /**
     * Método dirigido a ser generico para la subida de archivos al directorio temporal del
     * sistema. 
     */
    public function uploadTmpFile(Request $request){
        $imagen = $request->file('file');
        $path = Storage::disk('tmp')->putFile('scalia2', $imagen);
        return response(['tmpPath' => $path],200);
    }

    
    //Funcion para consultar imagenes mediante id
    public function downloadDiscoCFile(Request $request){        
        $path = Storage::disk('discoc')->path($request->get("path"));
        if (file_exists($path)) {
            return response()->file($path);
        }else{
            return response()->file(public_path('images/FichaImagePlaceHolder.png'));
        }
    }
}