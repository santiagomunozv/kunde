<?php

namespace App\Http\Controllers;

use App\Commons\SizfraSoap;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Database\Eloquent\Collection;
// use Auth;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function phpSelfClear(){
        $server = $_SERVER['PHP_SELF'];
        $sustituir = ['/index.php'];
        foreach($sustituir as $valor){
            $server = str_replace($valor,"",$server);
        }

        return $server;
    }

    protected function consultarPermisos(){
        return $this->consultarPermisosUrl($this->phpSelfClear());
    }

    protected function consultarPermisosUrl($url){

        $datos = DB::table('seg_rol as R')
            ->select('txNombreOpcion', 'txNombreRol', 'txRutaOpcion', 'chAdicionarRolOpcion', 'chModificarRolOpcion', 'chEliminarRolOpcion', 'chConsultarRolOpcion' , 'chInactivarRolOpcion')
            ->leftjoin('seg_usuariocompaniarol as UCR', 'UCR.Rol_oidUsuarioCompaniaRol', '=', 'R.oidRol')
            ->leftjoin('seg_rolopcion as RO', 'RO.Rol_oidRol_1aM', '=', 'UCR.Rol_oidUsuarioCompaniaRol')
            ->leftjoin('seg_opcion', 'Opcion_oidRolOpcion', '=', 'oidOpcion')
            ->leftjoin('seg_modulo', 'Modulo_oidModulo_1aM', '=', 'oidModulo')
            ->leftjoin('seg_paquete', 'Paquete_oidPaquete_1aM', '=', 'oidPaquete')
            //->groupby('oidPaquete', 'txNombrePaquete', 'txIconoPaquete','oidModulo', 'txNombreModulo', 'txIconoModulo','oidOpcion', 'txNombreOpcion', 'txRutaOpcion', 'txIconoOpcion','txNombreRol')
            ->orderby('oidPaquete')->orderby('txNombrePaquete')->orderby('txIconoPaquete')
            ->orderby('oidModulo')->orderby('txNombreModulo')->orderby('txIconoModulo')
            ->orderby('oidOpcion')->orderby('txNombreOpcion')->orderby('txRutaOpcion')
            ->orderby('txIconoOpcion')
            ->where('UCR.Usuario_oidUsuario_1aM', '=',  Auth::id())
            ->where('UCR.Compania_oidUsuarioCompaniaRol', '=', Session::get("oidCompania"))
            ->where('txRutaOpcion', '=', $url)
            ->get()->toArray();

        $permiso = $datos ? get_object_vars($datos[0]) : array('txNombreOpcion'=>'', 'txNombreRol'=>'', 'txRutaOpcion'=>'', 'chAdicionarRolOpcion'=>1, 'chModificarRolOpcion'=>1, 'chEliminarRolOpcion'=>1, 'chConsultarRolOpcion'=>1);
        return ($permiso);
    }

    /**
     * Método generico para construir los datos de una "Multi"
     * 
     * @param Request $request una solicitud del cliente la cual contiene los datos para poblar el array de datos
     * @param String $idPadreKey representa la columna por la cual se relaciona datos hacia otra tabla}
     * @param Int $index representa el indice al cual se está construyendo 
     * @param String[] $fields los campos fillables del objeto que se está construyendo
     * 
     * @return Object[] un array con los datos especificados.
     */
    protected function buildDatos($request ,$idPadreKey , $index , $fields){
        $datos = [];
        foreach($fields as $field){
            if(isset($request[$field] [$index])){
                $datos[$field] = $request[$field] [$index];
            }else{
                $datos[$field] = null;
            }
        }
        $datos[ $idPadreKey ] = $request->get($idPadreKey);
        return $datos;
    }

    function ejecutarServicioSizfra($servicio, $datos)
    {    
       return SizfraSoap::ejecutarServicioSizfra($servicio , $datos);
    }
   
    function ejecutarServicioSaya($encabezado,$detalle,$servicio,$metodo){
        $idUsuario = Auth::id();
        $BDCompania = Session::get('txBaseDatosCompania');
        $Empresa = DB::Select("SELECT idTercero FROM $BDCompania.Tercero WHERE tipoTercero like '%*17*%'");
        $oidEmpresa = get_object_vars($Empresa[0])['idTercero'];

        //consultamos el usuario creador homologado con saya para guardarlos en la db de saya
        $userCreador = DB::select("SELECT
        IL.id as terceroCreador
        FROM seg_usuario
        LEFT JOIN $BDCompania.SegLogin IL on txLoginUsuario = usuarioLogin
        WHERE oidUsuario = $idUsuario");
        $idCreador = ($userCreador[0]->terceroCreador ? $userCreador[0]->terceroCreador : '0');

        $sistema = strtolower($BDCompania);

        $cliente = new \nusoap_client("http://192.168.10.7/$sistema/clases/$servicio.php?wsdl", "wsdl");
        
        $parametros = array('encabezado' => $encabezado, 'detalle' => $detalle, 'empresa' => $oidEmpresa, 'idCreador' => $idCreador);
        
        $respuesta = $cliente->call("$metodo", $parametros);

        if ($cliente->fault){
            return $respuesta;
        }
        else 
        {   
            $err = $cliente->getError();
            if ($err)
                return "<h2>Error</h2><pre> . $err . </pre>";
            else
            {
                
                return $respuesta;
                
            }
        }  
     
    }

    protected function beginSayaTransaction(Collection $companias ){
        $companias->each(function($compania){
            DB::connection($compania->txBaseDatosCompania)->beginTransaction();
        });
    }

    protected function rollbackSayaTransaction( Collection $companias ){
        $companias->each(function($compania){
            DB::connection($compania->txBaseDatosCompania)->rollback();
        });
    }

    protected function commitSayaTransaction(Collection $companias ){
        $companias->each(function($compania){
            DB::connection($compania->txBaseDatosCompania)->commit();
        });
    }

}
