<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Seg_CompaniaModel extends Model 
{
    protected $table = 'seg_compania';
    protected $primaryKey = 'oidCompania';
    protected $fillable = ['txCodigoCompania', 'txNombreCompania','txBaseDatosCompania','txEstadoCompania'];
    public $timestamps = false;

    public static function listSelect($orId = 0){
        return self::where('txEstadoCompania','Activo')
            ->orWhere('oidCompania',$orId)
            ->orderBy('txNombreCompania','ASC')
            ->select('txNombreCompania','oidCompania')
            ->pluck('txNombreCompania','oidCompania');
    }
}
