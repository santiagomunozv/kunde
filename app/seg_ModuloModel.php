<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Seg_ModuloModel extends Model 
{
    protected $table = 'seg_modulo';
    protected $primaryKey = 'oidModulo';
    protected $fillable = ['Paquete_oidPaquete_1aM', 'txNombreModulo','txIconoModulo'];
    public $timestamps = false;

    public function opciones(){
        return $this->hasMany('App\Seg_OpcionModel','Modulo_oidModulo_1aM');
    }
}
