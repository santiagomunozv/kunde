<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use Modules\General\Entities\partidaArancelaria\PartidaArancelariaObserver;
use OwenIt\Auditing\Models\Audit;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(){
        setlocale(LC_TIME,'es_CO.UTF-8');
        Carbon::setLocale('es');
        //evitar que se audite cuando no se han modificado atributos 
        //de los objetos
        Audit::creating(function (Audit $model) {
            if (empty($model->old_values) && empty($model->new_values)) {
                return false;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){}
}
