<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Seg_OpcionModel extends Model 
{
    protected $table = 'seg_opcion';
    protected $primaryKey = 'oidOpcion';
    protected $fillable = ['Modulo_oidModulo_1aM', 'txNombreOpcion','txRutaOpcion','txIconoOpcion'];
    public $timestamps = false;
}
