<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class seg_RolModel extends Model 
{
    protected $table = 'seg_rol';
    protected $primaryKey = 'oidRol';
    protected $fillable = ['txCodigoRol', 'txNombreRol'];
    public $timestamps = false;
}
