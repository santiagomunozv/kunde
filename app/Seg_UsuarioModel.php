<?php

    namespace App;
    use Illuminate\Auth\Authenticatable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

    class Seg_UsuarioModel extends Model implements AuthenticatableContract
    {
        use Authenticatable;

        protected $table = 'seg_usuario';
        protected $primaryKey = 'oidUsuario';
        public $timestamps = false;
        protected $fillable = ['txLoginUsuario', 'password','Tercero_oidTercero', 'dtCambioClaveUsuario'];

        /**
         * The attributes excluded from the model's JSON form.
         *
         * @var array
         */
        protected $hidden = ['password'];

        public function setPasswordAttribute($value)
        {
            $this->attributes['password'] = bcrypt($value);
        }

        // public function UsuarioCompaniaRol()
        // {
        //     return $this->hasMany('App\Seg_UsuarioCompaniaRolModel','Usuario_oidUsuario_1aM');
        // }

    }
