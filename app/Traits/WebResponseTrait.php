<?php
namespace App\Traits;

trait WebResponseTrait {
    public function successResponse($data, $code)
    {
        return response()->jason($data, $code);
    }
}