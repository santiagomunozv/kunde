<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait FilesTrait {

    public function response(Request $request, $route)
    {
        $file = $request->file('file');
        if (!$file->isValid()) return abort(200, 'No se cargo el archivo correctamente.');
        $path = Storage::disk('local')->put('public/images/' . $route, $file);
        return response()->json(['tmpPath' => $path]);
    }

    public function save($data, $model, $folder = '', $url, $idPrincipal = '')
    {
        $total = count($data['imagenAdjunto']);
        $initPath = 'public/images/' . $folder;

        for ($i = 0; $i < $total; $i++) {
            $origin         = $data['imagenAdjunto'][$i];
            $extension      = File::extension($origin);
            $name           = basename($origin);
            $destination    = $initPath .'/'. $url . '/' . $name;
            $path           = Storage::disk('local')->move($origin, $destination);
            
            if ($path) {
                $model->create([
                    'txNombreArvchivo'      => $name,
                    'txRutaArchivo'         => $destination,
                    'txTipoArchivo'         => $extension,
                    'txDescripcionArchivo'  => null,
                    'dtFechaSubidaArchivo'  => date('Y-m-d'),
                    'Tarea_oidTarea'     => $idPrincipal
                ]);
            } else {
                return abort(200, 'No se pudo guardar el archivo');
                $this->clearFolder($initPath . '/*');
            }
        }
        
    }

    public function delete($data, $model)
    {
        $total = substr($data, 0, strlen($data) - 1);

        if (strlen($total) > 0) {
            foreach (explode(',', $total) as $idEliminar) {
                $file = $model->find($idEliminar);
                $path = Storage::path($file->txRutaArchivo);
                if (file_exists($path)) {
                    unlink($path);
                }
                $model->where('oidAdjunto', $idEliminar)->delete();
            }
        }
    }

    public function clearFolder($path)
    {
        $files = glob(storage_path('public/images/tareasPorEstado/*'));
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }

    public function get($path)
    {
        if (file_exists($path)) {
            return response()->file($path);
        } else {
            return response()->file(public_path('images/FichaImagePlaceHolder.png'));
        }
    }

}