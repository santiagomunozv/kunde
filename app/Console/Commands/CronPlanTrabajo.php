<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\PlanTrabajo\Mail\EjecucionActividad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CronPlanTrabajo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronPlanTrabajo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consulta para las ejecuciones de actividades';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{
            $this->ejecucionActividad();
        }catch(\Exception $error){
            Storage::disk("discoc")->put("/asociadonegocio/cronCertificado/".uniqid().".txt",$error->getMessage());
        }
       
    }

    
    //CRON CORREO DE CERTIFICADOS VENCIDOS
    public function ejecucionActividad(){
        
        $consulta = DB::select("SELECT 
        txActividadProgramadaConfiguracionPlanTrabajo,
        TE.txNombreTercero as nombreEncargado,
        TE.txCorreoElectronicoTercero as correoEncargado,
        TC.txNombreTercero as nombreCompania,
        TC.txCorreoElectronicoTercero as correoCompania,
        daFechaProgramadaActividadPlanTrabajo,
        txHorasProgramadasActividadPlanTrabajo,
        txCantidadHorasActividadPlanTrabajo
    FROM
        kunde.gen_programacionplantrabajo
            LEFT JOIN
        gen_actividadplantrabajo ON ProgramacionPlanTrabajo_oidPlanTrabajo_1aM = oidProgramacionPlanTrabajo
            LEFT JOIN
        asn_tercero TE ON Tercero_oidTerceroEncargado = TE.oidTercero
            LEFT JOIN
        asn_tercero TC ON Tercero_oidTerceroCompania = TC.oidTercero
            LEFT JOIN
        gen_configuracionplantrabajo ON ConfiguracionPlanTrabajo_oidConfiguracionPlan = oidConfiguracionPlanTrabajo
    WHERE
        daFechaProgramadaActividadPlanTrabajo = DATE_ADD(CURDATE(), INTERVAL 2 DAY)
        GROUP BY TC.oidTercero");
        // Storage::disk("discoc")->put("/asociadonegocio/cronCertificado/".uniqid().".txt",'casaa');
        for ($i=0; $i < count($consulta) ; $i++) {       
            $cita = new ejecucionActividad($consulta[$i]->txActividadProgramadaConfiguracionPlanTrabajo,$consulta[$i]->nombreEncargado,$consulta[$i]->nombreCompania,$consulta[$i]->daFechaProgramadaActividadPlanTrabajo,$consulta[$i]->txHorasProgramadasActividadPlanTrabajo,$consulta[$i]->txCantidadHorasActividadPlanTrabajo);   
            Mail::to($consulta[$i]->correoEncargado)->send($cita);
            Mail::to($consulta[$i]->correoCompania)->send($cita);
                
        }
    }
}
