<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\AsociadoNegocio\Entities\TerceroModel;
use Modules\AsociadoNegocio\Http\Controllers\TerceroController;
use Modules\AsociadoNegocio\Mail\CertificadoTerceroVencido;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class CronTercero extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronTercero';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consulta los certificados que estan por vencer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        try{
            $this->certificadoVencimiento();
        }catch(\Exception $error){
            Storage::disk("discoc")->put("/asociadonegocio/cronCertificado/".uniqid().".txt",$error->getMessage());
        }
       
    }

    
    //CRON CORREO DE CERTIFICADOS VENCIDOS
    public function certificadoVencimiento(){
        
        $consulta = DB::select("SELECT 
            daFechaVencimientoTerceroAdjunto,
            txNombreTercero,
            txNombreTipoCertificado,
            txDocumentoTercero,
            oidTercero
        FROM
            kunde.asn_terceroadjunto as  TA
            left join 
            asn_tercero on TA.Tercero_oidTercero_1aM  = oidTercero
            left join 
            gen_tipocertificado on TipoCertificado_oidTerceroCertificado = oidTipoCertificado
        WHERE
            daFechaVencimientoTerceroAdjunto is not null");
        // Storage::disk("discoc")->put("/asociadonegocio/cronCertificado/".uniqid().".txt",'casaa');
        for ($i=0; $i < count($consulta) ; $i++) { 

            $tiempoActual = Carbon::now();
            $fecha = $tiempoActual->toDateString();
            
            $nombreTercero =  $consulta[$i]->txNombreTercero;
            $fechaVencimiento = $consulta[$i]->daFechaVencimientoTerceroAdjunto;
            $nombreTipoCertificado = $consulta[$i]->txNombreTipoCertificado;
            $documentoTercero = $consulta[$i]->txDocumentoTercero;
            $idTercero = $consulta[$i]->oidTercero;

            $correo = DB::select("SELECT 
                txCorreoGrupoTercero
            FROM
                kunde.asn_tercerogrupo
                    LEFT JOIN
                asn_grupotercero ON GrupoTercero_oidTerceroGrupo = oidGrupoTercero
                    LEFT JOIN
                gen_clasificaciontercerogrupo ON ClasificacionTerceroGrupo_oidClasificacionTerceroGrupo = oidClasificacionTerceroGrupo
            WHERE
                Tercero_oidTercero_1aM = $idTercero
                AND txNombreClasificacionTerceroGrupo = 'Lider'
                AND txCorreoGrupoTercero != ''
            GROUP BY Tercero_oidTercero_1aM");
            
            if($consulta[$i]->daFechaVencimientoTerceroAdjunto > $fecha && $consulta[$i]->daFechaVencimientoTerceroAdjunto <= date("Y-m-d", strtotime("+ 10 DAYS" , strtotime($fecha)))){
                $vencido = new CertificadoTerceroVencido($nombreTercero,$fechaVencimiento,$nombreTipoCertificado,$documentoTercero);   
                Mail::to($correo[0]->txCorreoGrupoTercero)->send($vencido);
            }
                
        }
    }
}
