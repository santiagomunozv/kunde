<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SupervisarVencimientoTareas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vencimientoTareas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consultar vencimiento de tareas para actualizarla al día de hoy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::Update(
            "UPDATE 
                rgs_tarea 
            SET 
                dtFechaVencimientoTarea = curdate() 
            WHERE 
                dtFechaVencimientoTarea < curdate() 
                AND txFechaSolucionTarea IS NULL
                AND txEstadoTarea != 'Rechazado'");
    }
}
