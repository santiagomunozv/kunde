<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // "App\Console\Commands\CronFacturaElectronica",
        // "App\Console\Commands\CronTercero",
        // "App\Console\Commands\cronEstadoConsecutivoSizfra",
        "App\Console\Commands\CronPlanTrabajo"
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('CronTercero')->weekly();
        // $schedule->command('CronPlanTrabajo')->dailyAt('11:00');
        // $schedule->command('CronPlanTrabajo')->dailyAt('17:00');
        // $schedule->command('CronPlanTrabajo')->dailyAt('22:00');
        // $schedule->command('vencimientoTareas')->dailyAt('1:00');
    }

    /**
     * Register the commands for the application
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
