<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Modules\AsociadoNegocio\Entities\TerceroEmpleadoModel;

class TerceroEmpleadosExport implements FromCollection, WithHeadings
{
    use Exportable;

    protected $empleados;
    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    // encabezados
    public function headings(): array
    {
        return [
            // 'TIPO DE DOCUMENTO',
            // 'DOCUMENTO',
            // 'NOMBRE',
            // 'APELLIDO',

            // 'CARGO',
            // 'PAIS DE NACIMIENTO',
            // 'CIUDAD DE NACIMIENTO',
            // 'FECHA DE NACIMIENTO',
            // 'PAIS DE RECIDENCIA',
            // 'CIUDAD DE RECIDENCIA',
            // 'DIRECCIÓN',
            // 'CELULAR',
            // 'CORREO',
            // 'ESTRATO',
            // 'FECHA DE INGRESO',
            // 'TIEMPO EN LA EMPRESA',
            // 'EPS',
            // 'ARL',
            // 'HORARIO LABORAL',
            // 'TIPO DE CONTRATO',
            // 'GENERO',
            // 'ESTADO CIVIL',
            // 'NUMERO DE PERSONAS A CARGO',
            // 'TIPO DE VIVIENDA',
            // 'ESCOLARIDAD',
            // 'INGRESOS',
            // 'EDAD',
            // // 'ComFamiliar_oidEmpleadoTercero_1aM',

            // 'ACTIVIDADES DE TIMEPO LIBRE',
            // 'DEPORTES',
            // 'CONSUME SUSTANCIAS ALUCINOGENAS',
            // 'SUSTANCIAS',
            // 'ACEPTA TRATAMIENTO DE DATOS',

            'NOMBRE',
            'APELLIDO',
            'DOCUMENTOS DE IDENTIDAD',
            'GENERO',
            'FECHA DE NACIMIENTO',
            'PAIS DE NACIMIENTO',
            'CIUDAD DE NACIMIENTO',
            'EDAD',
            'ESTADO CIVIL',
            // 'CIUDAD RECIDENCIA',
            'DIRECCIÓN',
            'BARRIO',
            'CARGO',
            'CELULAR',
            'CORREO ELECTRONICO',
            'ESTRATO',
            'TIEMPO QUE LLEVA EN LA EMPRESA',
            'EPS',
            'ARL',
            'FONDO DE PENSION',
            'HORARIO DE TRABAJO',
            'TIPO DE CONTRATO',
            'NUMERO DE PERSONAS A CARGO',
            'TIPO DE VIVIENDA',
            'ESCOLARIDAD',
            'PRIMEDIO DE INGRESOS',
            'TIEMPO LIBRE',
            'DEPORTE',
            'CONSUME SUSTANCIAS PSICOACTIVA',
            'ENFERMEDAD',
            'NOMBRE FAMILIAR',
            'PARENTESCO',
            'CELULAR FAMILIAR'
        ];
    }

    public function function_constructor($empleados)
    {
        $this->empleados = $empleados;
    }

    public function collection()
    {
        // dd($this->id);
        return $this->empleados ?: TerceroEmpleadoModel::obtenerEmpleados($this->id);
    }
}
