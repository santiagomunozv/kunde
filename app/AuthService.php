<?php
namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\General\Entities\ClasificacionTerceroModel;
use App\utils\LoginFactory;
use Illuminate\Support\Facades\DB;

/**
 * Esta clase contiene la logica necesaria para establecer los objetos de la sesión 
 * de acuerdo al usuario que hace login en la aplicacion.
 * Los únicos metodos que deberían ser llamados son setSessionModels y setSessionModelsByCompania
 * 
 */
class AuthService{
    //El id de la compania que el usuario tiene en sesión
    public static $COMPANIA_ID = "oidCompania";
    public static $DATABASE_IDENTIFIER = 'txBaseDatosCompania';
    public static $USER_ROLE = "rolUsuario";
    public static $TERCERO_ID = "oidTercero";
    public static $TERCERO_NAME = "session_usr_nombreTercero";
    public static $USER_COMPANIES = 'session_usr_companiasAsociadas';
    
    private static $BASE_SQL = "SELECT 
    Usuario_oidUsuario_1aM,
    oidTercero,
    Compania_oidUsuarioCompaniaRol AS oidCompania, 
    txBaseDatosCompania,
    txPrimerNombreTercero,
    Rol_oidUsuarioCompaniaRol 
    FROM seg_usuariocompaniarol 
    INNER JOIN seg_usuario ON Usuario_oidUsuario_1aM = oidUsuario
    INNER JOIN seg_compania ON Compania_oidUsuarioCompaniaRol = oidCompania
    INNER JOIN asn_tercero ON Tercero_oidUsuario = oidTercero
    WHERE txEstadoUsuario = 'Activo' AND Usuario_oidUsuario_1aM =";

    /**
     * 
     * Metodo que establece los valores de la sesion con la primer compañia
     * que se encuentre en la base de datos para el usuario que hace login
     * 
     * @return true si se encuentran datos en la tabla seg_usuariocompaniarol
     */
    public static function setSessionModels(){
        $data = self::getSessionDataByUsuario(Auth::id());
        return self::validateAndSetSessionModels($data);
    }


    /**
     * 
     * Metodo que establece los valores de la sesion con la compania indicada como parametro
     * que se encuentre en la base de datos para el usuario que hace login
     * 
     * @param oidCompania el id de la compañia a la cual se está cambiando
     * @return true si se encuentran datos en la tabla seg_usuariocompaniarol
     */
    public static function setSessionModelsByCompania($oidCompania){
        $data = self::getSessionDataByUsuarioAndCompania(Auth::id() , $oidCompania);
        return self::validateAndSetSessionModels($data);
    }

    private static function validateAndSetSessionModels($data){
        if(!$data || count($data) < 1){
            return false;
        }
        self::setModels($data[0]);
        return true;
    }

    /**
     * Metodo encargado de determinar cuales variables de sesion se establecen
     */
    private static function setModels($data){
        self::setUserModels($data);
        self::setMenuModels($data);
    }

    /**
     * Metodo encargado de establecer los modelos relacionados al usuario que está logueado
     */
    private static function setUserModels($data){
        Session::put(self::$DATABASE_IDENTIFIER , $data->txBaseDatosCompania);
        Session::put(self::$COMPANIA_ID , $data->oidCompania);
        Session::put(self::$USER_ROLE , $data->Rol_oidUsuarioCompaniaRol);
        Session::put(self::$TERCERO_ID , $data->oidTercero);
        Session::put(self::$TERCERO_NAME , $data->txPrimerNombreTercero);
        Session::put(self::$USER_COMPANIES , self::getCompaniasByUsuarioId(Auth::id()));
    }

    /**
     * Metodo encargado de establecer los modelos usados para construir el menu de la aplicacion
     * 
     */
    private static function setMenuModels($data){
        $idUsuario = $data->Usuario_oidUsuario_1aM;
        $idRol = $data->Rol_oidUsuarioCompaniaRol;
        $clasificacionTercero = ClasificacionTerceroModel::getOpcionMenu($idUsuario);
        $menuGeneral = LoginFactory::getMenuData($idRol);
        Session::put('packageDinamicMenuScaliaClaTer',$clasificacionTercero);
        Session::put('packageDinamicMenuScaliaPaquetes',LoginFactory::buildPaquetes($menuGeneral));
        Session::put('packageDinamicMenuScaliaModulos',LoginFactory::buildModulos($menuGeneral));
        Session::put('packageDinamicMenuScaliaOpciones',LoginFactory::buildOpcion($menuGeneral));
    }

    private static function getSessionDataByUsuario($oidUsuario){
        return DB::select(self::$BASE_SQL ."{$oidUsuario} LIMIT 1");
    }

    private static function getSessionDataByUsuarioAndCompania($oidUsuario , $oidCompania){
        return DB::select(self::$BASE_SQL. "{$oidUsuario} 
            AND Compania_oidUsuarioCompaniaRol = {$oidCompania} LIMIT 1");
    }

    private static function getCompaniasByUsuarioId($usuarioId){
        return DB::select(
            "SELECT 
                oidCompania,
                txNombreCompania 
            FROM seg_usuariocompaniarol 
            INNER JOIN seg_compania ON oidCompania = Compania_oidUsuarioCompaniaRol
            WHERE Usuario_oidUsuario_1aM = {$usuarioId} 
            ORDER BY txNombreCompania ASC"
        );
    }
}