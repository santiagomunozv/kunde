<?php

namespace App\Commons\InteligenciaArtificial\ListaPrecios;

use Modules\Comercial\Entities\ListaPrecioDetalleModel;

class NivelAperturaDetector{
    const R = 'REF';
    const RC = 'REF_COL';
    const RT = 'REF_TAL';
    const RTC = 'REF_COL_TAL';

    function __construct($detalles)
    {
        $this->r = collect();
        $this->rc = collect();
        $this->rt = collect();
        $this->rtc = collect();
        $this->build($detalles);
    }

    private function build($detalles){
        $detalles->each(function($detalle){
            $this->casesManager($detalle);
        });
    }

    private static function resolve(ListaPrecioDetalleModel $detalle){
        if(!$detalle->Color_oidListaPrecioDetalle && !$detalle->Talla_oidListaPrecioDetalle){
           return self::R; 
        }
        if($detalle->Color_oidListaPrecioDetalle && !$detalle->Talla_oidListaPrecioDetalle){
            return self::RC; 
        }
        if($detalle->Talla_oidListaPrecioDetalle && !$detalle->Color_oidListaPrecioDetalle){
            return self::RT; 
        }
        return self::RTC;
    }

    /**
     * Metodo encargado de definir cual es el nivel de apertura para un detalle
     * y ponerlo en su respectiva lista.
     */
    private function casesManager($detalle){
        $nivel = self::resolve($detalle);
        switch($nivel){
            case self::R: 
                $this->r->push($detalle);
                break;
            case self::RC:
                $this->rc->push($detalle);
                break;
            case self::RT:
                $this->rt->push( $detalle);
                break;
            case self::RTC:
                $this->rtc->push( $detalle);
                break;
        }
    }

    public function getFichasIdForNivel($nivel){
        switch($nivel){
            case self::R: 
                return $this->extractFichasIdFrom($this->r);
            case self::RC:
                return $this->extractFichasIdFrom($this->rc);
            case self::RT:
                return $this->extractFichasIdFrom($this->rt);
            case self::RTC:
                return $this->extractFichasIdFrom($this->rtc);
        }
    }

    private function extractFichasIdFrom($from){
        return implode(',' , $from->map(function($detalle){
            return $detalle->FichaTecnica_oidListaPrecioDetalle;
        })->toArray());
    }
}