<?php
namespace App\Commons\InteligenciaArtificial\ListaPrecios;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Comercial\Entities\ListaPrecioDetalleModel;
use Modules\Comercial\Entities\ListaPrecioModel;
use Modules\General\Entities\ComponenteCostoConceptoModel;

/**
 * Clase que contiene algunos pasos lógicos para determinar cuales productos
 * y en que orden se crearán para un archivo excel de lista precios.
 * 
 * Se encarga de dar prioridad a los productos que en la lista precio
 * son creados con nivel de apertura RCT seguido por RT, seguido por RC y por ultimo los
 * Rs
 */
class ListaPreciosAi
{
    /**
     * Constructor por defecto para esta clase. Inicializa y realiza algunos procesos internos de la clase
     * 
     * @param ListaPrecioModel $listaPrecioModel la lista precio que se enviará a saya
     */
    function __construct(ListaPrecioModel $listaPrecioModel)
    {
        $this->listaPrecios = $listaPrecioModel;
        $this->eanTercero = $listaPrecioModel->cliente->txCodigoBarrasTercero;
        $this->listaPreciosData = [];
        $this->productosData = [];
        $this->sqls = collect();
        $this->conceptosScalia = ComponenteCostoConceptoModel::findByComponenteCostoId($listaPrecioModel->ComponenteCosto_oidListaPrecio)->get();
        $this->build();
    }

    /**
     * Método creado para contener los pasos lógicos para construir esta clase.
     */
    private function build()
    {
        $this->setFields();
        $this->buildProductos();
    }

    /**
     * Metodo que contiene la lógica para construir los campos de esta clase.
     * define cual es nivel de apertura de los detalles de la lista de precios
     * y establece un diccionario para buscar los valores para los conceptos
     * del componente costo.
     * 
     */
    private function setFields()
    {
        $detallesListaPrecios = ListaPrecioDetalleModel::findByListaPrecios($this->listaPrecios->oidListaPrecio);
        $this->aperturaDetector = new NivelAperturaDetector($detallesListaPrecios);
        $this->componentesCosto = ComponenteCostoConceptoModel::findByComponenteCostoId($this->listaPrecios->ComponenteCosto_oidListaPrecio);
        $this->buildConceptosDetalle();
    }


    /**
     * Metodo que contiene la logica para construir el diccionario de los conceptos.
     * El diccionario de los conceptos permitirá que dado un detalleListaPrecio y un conceptoComponenteCosto
     * se entregue el valor para dicho producto.
     */
    private function buildConceptosDetalle()
    {
        $query = DB::select("SELECT ldc.* FROM com_listapreciodetalle AS ld INNER JOIN com_listapreciocomponente AS ldc ON ld.oidListaPrecioDetalle = ldc.ListaPrecioDetalle_oidListaPrecioDetalle WHERE ld.ListaPrecio_oidListaPrecio_1aM = {$this->listaPrecios->oidListaPrecio}  ORDER BY ListaPrecio_oidListaPrecio_1aM");
        $this->conceptosDictionary = [];
        foreach($query AS $row){
            $key = self::generateDictionaryKey($row->ListaPrecioDetalle_oidListaPrecioDetalle,$row->ComponenteCostoConcepto_oidListaPrecioComponente);
            $this->conceptosDictionary[$key] = (object)$row;
        }
    }

    /**
     * Método que contiene la logica para construir los productos
     * discriminandolos por nivel de apertura RTC y por ultimo 
     * elimina los productos duplicados en cada uno de los arrays
     * que representan un nivel de apertura.
     */
    private function buildProductos()
    {
        $this->productosRTC = self::queryToDb( $this->buildProductsSql(NivelAperturaDetector::RTC) );
        $this->productosRT = self::queryToDb( $this->buildProductsSql(NivelAperturaDetector::RT) );
        $this->productosRC = self::queryToDb( $this->buildProductsSql(NivelAperturaDetector::RC) );
        $this->productosR = self::queryToDb( $this->buildProductsSql(NivelAperturaDetector::R) );
        $this->cleanProductos();
    }

    /**
     * Metodo creado para retornar un array cuando no se ha logrado definir el parametro
     * sql, ya que si se ejecuta el sql vacío generaría una exception
     * 
     * @param string $sql el sql para realizar la busqueda de los productos
     * @return array con los elementos que retorna el sql al lanzarse a la base de datos o un array vacio si el sql está vacío
     */
    private static function queryToDb($sql)
    {
        return $sql ? DB::select( $sql ) : [];
    }


    /**
     * Metodo que genera un sql dinamicamente dependiendo del parametro $nivelApertura.
     * ya que los join de las tablas pueden ser mas o menos estrictos dependiendo de 
     * dicho parametro.
     * 
     * Este metodo hace uso de los campos propios de esta clase.
     * 
     * @param string $nivelApertura el nivel de apertura con el que se generará el sql.
     */
    private function buildProductsSql($nivelApertura)
    {
        $idProductoField = self::$ID_PRODUCTO_FIELD;
        $on = self::defineOnSql($nivelApertura);
        $whereIn = $this->aperturaDetector->getFichasIdForNivel($nivelApertura);
        return $whereIn ? "SELECT 
                '{$this->listaPrecios->bodega->txCodigoBodega}' AS bodega,
                p.txReferenciaProducto AS referencia,
                p.txNombreProducto AS nombreProducto,
                ld.deValorBaseListaPrecioDetalle AS valorBase,
                ld.dePrecioVentaListaPrecioDetalle AS precioProducto,
                ld.oidListaPrecioDetalle AS idListaPrecioDetalle,
                p.oidProducto AS {$idProductoField}
            FROM
                com_listapreciodetalle AS ld
                    INNER JOIN
                gen_producto AS p $on 
            WHERE ld.ListaPrecio_oidListaPrecio_1aM = {$this->listaPrecios->oidListaPrecio} AND FichaTecnica_oidListaPrecioDetalle IN( $whereIn )": null;
    }

    /**
     * Metodo que contiene la logica para procurar dar prioridad a los productos que están 
     * abiertos por referencia/color/talla , sobre los demás
     */
    private function cleanProductos(){
        $this->clean('productosRTC' , ['productosRT' , 'productosRC' , 'productosR']);
        $this->clean('productosRT' , ['productosRC' , 'productosR']);
        $this->clean('productosRC' , ['productosR']);
    }

    /**
     * Metodo que se encarga de limpiar los items de un array (field) en otros arrays ($toCheck)
     * @param $field el campo que se está verificando
     * @param $toCheck los campos contra los que se comparará el parametro $field
     */
    private function clean($field , $toCheck)
    {
        $campoIdProducto = self::$ID_PRODUCTO_FIELD;
        for($i=0; $i < count($this->$field);$i++){
            $idProducto = $this->$field[$i]->$campoIdProducto;
            for($j = 0; $j < count($toCheck); $j++){
                $this->unsetProducto($toCheck[$j] , $idProducto);
            }
        }
    }

    /**
     * Metodo que contiene la logica para verificar que un item no existe en otro,
     * y si existe lo elimina
     */
    private function unsetProducto($array , $idProducto)
    {
        $campoIdProducto = self::$ID_PRODUCTO_FIELD;
        for($i=0; $i < count($this->$array);$i++){
            if($idProducto === $this->$array[$i]->$campoIdProducto){
                unset($this->$array[$i]);
            }
        }
    }

    /**
     * Metodo que se encarga de definir que tipo de ON se generará
     * para un determinado nivel de apertura.
     * 
     * @param $nivel representa el nivel de apertura.
     * @return string un texto de tipo sql que solo representa el ON  a utilizar en los join del metodo @see buildProductsSql()
     */
    private static function defineOnSql($nivel)
    {
        switch($nivel){
            case NivelAperturaDetector::R: 
                return self::$SQL_R;
            case NivelAperturaDetector::RC:
                return self::$SQL_RC;
            case NivelAperturaDetector::RT:
                return self::$SQL_RT;
            case NivelAperturaDetector::RTC:
                return self::$SQL_RTC;
        }
    }

    /**
     * Metodo encargado de construir el encabezado para el reporte 
     * sayaListaPrecioTable desde la clase @see SayaExcelExportView
     * 
     */
    public function buildEncabezado()
    {
        $yearFormat = 'Y-m-d';
        $hourFormat = "H:i:s";
        $precio = [];
        $precio["codigoAlternoListaPrecio"] = $this->listaPrecios->inNumeroListaPrecio;
        $precio["nombreListaPrecio"] = $this->listaPrecios->txNombreListaPrecio;
        $precio["redondeoListaPrecio"] = $this->listaPrecios->lsRedondeoListaPrecio;
        $precio["fechaInicialListaPrecio"] = Carbon::parse($this->listaPrecios->dtFechaInicioListaPrecio)->format($yearFormat);
        $precio["horaInicialListaPrecio"] = Carbon::parse($this->listaPrecios->dtFechaFinListaPrecio)->format($hourFormat);
        $precio["fechaFinalListaPrecio"] = Carbon::parse($this->listaPrecios->dtFechaFinListaPrecio)->format($yearFormat);
        $precio["horaFinalListaPrecio"] = Carbon::parse($this->listaPrecios->dtFechaFinListaPrecio)->format($hourFormat);
        $precio["codigoAlternoMoneda"] = $this->listaPrecios->monedaDestino->txCodigoMoneda;
        $precio["codigoAlternoComponenteCosto"] = $this->listaPrecios->componenteCosto->inConsecutivoComponenteCosto;
        //FIXME: arreglar esto
        $precio["modificarPrecioProductoListaPrecio"] = 'X';
        //FIXME: arreglar esto
        $precio["ivaIncluidoProductoListaPrecio"] = 0;

        $precio["actualizar"] = 'Reemplazar';
        //FIXME: verificar valor
        $precio["estadoListaPrecioDetalle"] = 'ACTIVO';
        return (object)$precio;
    }

    public function buildDetalles()
    {
        $return_ = [];
        $return_ = array_merge($return_ , $this->buildProductosArray($this->productosRTC));
        $return_ = array_merge($return_ , $this->buildProductosArray($this->productosRT));
        $return_ = array_merge($return_ , $this->buildProductosArray($this->productosRC));
        $return_ = array_merge($return_ , $this->buildProductosArray($this->productosR));
        return $return_;
    }

    private function buildProductosArray(array $array){
        $return_ = [];
        $productoIdProducto = self::$ID_PRODUCTO_FIELD;
        $eanTercero = $this->listaPrecios->cliente->txCodigoBarrasTercero;
        for($i = 0 ; $i < count($array) ; $i++){
            $row = $array[$i];
            $producto = [
                'codigoAlternoBodega' => $row->bodega,
                'referenciaProducto' => $row->referencia,
                'nombreProducto' => $row->nombreProducto,
                'valorBaseListaPrecioDetalle' => $row->valorBase,
                'precioListaPrecioDetalle' => $row->precioProducto,
                'ean' => $eanTercero,
                'Producto_idProducto' => $row->$productoIdProducto,
                'conceptos' => $this->buildComponentes($row->idListaPrecioDetalle)
            ];
            array_push($return_,(object)$producto);
        }
        return $return_;
    }

    private function buildComponentes($idDetalleListaPrecioScalia){
        $return_ = [];
        foreach($this->conceptosScalia as $concepto){
            $dictionaryData = $this->getFromDictionary($idDetalleListaPrecioScalia , $concepto->oidComponenteCostoConcepto);
            $return_[$concepto->txCodigoComponenteCostoConcepto] = $dictionaryData->deValorListaPrecioComponente;
        }
        return $return_;
    }

    private function getFromDictionary($idDetalleListaPrecio , $idConcepto){
        $key = self::generateDictionaryKey($idDetalleListaPrecio , $idConcepto);
        return $this->conceptosDictionary[$key];
    }

    private static function generateDictionaryKey($idDetalleListaPrecio , $idConcepto){
        return "{$idDetalleListaPrecio} - {$idConcepto}";
    }

    private static $SQL_R = " ON ld.FichaTecnica_oidListaPrecioDetalle = p.FichaTecnica_oidFichaTecnica_1aM ";
    private static $SQL_RC = " ON ld.FichaTecnica_oidListaPrecioDetalle = p.FichaTecnica_oidFichaTecnica_1aM AND ld.Color_oidListaPrecioDetalle = p.Color_oidProducto";
    private static $SQL_RT = " ON ld.FichaTecnica_oidListaPrecioDetalle = p.FichaTecnica_oidFichaTecnica_1aM AND ld.Talla_oidListaPrecioDetalle = p.Talla_oidProducto";
    private static $SQL_RTC = " ON ld.FichaTecnica_oidListaPrecioDetalle = p.FichaTecnica_oidFichaTecnica_1aM  AND ld.Talla_oidListaPrecioDetalle = p.Talla_oidProducto AND ld.Color_oidListaPrecioDetalle = p.Color_oidProducto";
    private static $ID_PRODUCTO_FIELD = "idProducto";
}