<?php

namespace App\Commons\FormulaPreinspeccionCore;

use Modules\General\Entities\PartidaArancelariaFormulaFields;

class FormulaResolver{
    private $campos;
    private $formula;
    private $resultado;

    /**
     * Constructor del objeto, reemplaza y establece el valor de la fórmula a evaluar.
     * 
     * @param FieldsWrapper $campos el envoltorio diseñado para contenero los campos que hacen referencia a los 
     * campos contenidos en PartidaArancelariaFormulaFields.
     * @param String $formula representa la formula a evaluar con la funcion eval.
     * @throws \Exception por diversos motivos tales como: la formula no evaluó correctamente o al intentar reemplazar
     * los valores estaticos de la formula.
     */
    function __construct(FieldsWrapper $campos , $formula){
        $this->campos = $campos;
        $this->formula = $formula;
        $this->replaceStaticFields();
        $this->resultado = eval("return {$this->formula};");
    }

    /**
     * Método que reemplaza los campos fijos de la fórmula por los que ingresan en el attributo 
     * $campos
     */
    private function replaceStaticFields(){
        foreach(PartidaArancelariaFormulaFields::getValues() as $campo){
            str_replace($campo , $this->campos->get($campo), $this->formula);
        }
    }

    /**
     * Entrega el resultado de la evaluación de la fórmula
     */
    public function getResultado(){
        return $this->resultado;
    }
}