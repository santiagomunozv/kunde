<?php
namespace App\Commons\FormulaPreinspeccionCore;

use Modules\General\Entities\PartidaArancelariaFormulaFields;

/**
 * Clase creada para estandarizar la resolucion de las formulas para gravamen y para iva presentes en las 
 * partidas arancelarias. Los atributos de esta clase representan cada valor presente en PartidaArancelariaFormulaFields.php
 * 
 */
class FieldsWrapper{


    /**
     * Constructor de esta clase, recibe todos los parametros que hacen referencia a cada uno de los campos presentes en 
     * PartidaArancelariaField.php. Dichos campos son requeridos por lo que se hace necesario ingresarlos, ya que serán 
     * reemplazados en la formula final
     * 
     * @param $cantidad  hace referencia al campo PartidaArancelariaFormulaFields#CANTIDAD
     * @param $trm hace referencia al campo PartidaArancelariaFormulaFields#TRM
     * @param $fob hace referencia al campo PartidaArancelariaFormulaFields#FOB
     * @param $pesoBruto hace referencia al campo PartidaArancelariaFormulaFields#PESO_BRUTO
     * @param $pesoNeto hace referencia al campo PartidaArancelariaFormulaFields#PESO_NETO
     */
    function __construct($cantidad , $trm , $fob , $pesoBruto , $pesoNeto){
        $this->cantidad = $cantidad;
        $this->trm = $trm;
        $this->fob = $fob;
        $this->pesoBruto = $pesoBruto;
        $this->pesoNeto = $pesoNeto;
        $this->validateParams();
    }

    private function validateParams(){
        if(!$this->cantidad || !$this->trm || !$this->fob || !$this->pesoBruto || !$this->pesoNeto){
            //Todos los campos son requeridos, para poder realizar el reemplazo de los mismos en las formulas,
            throw new \Exception("Los parámetros para este objeto son requeridos");
        }
    }

    public function get($campo){
        switch($campo){
            case PartidaArancelariaFormulaFields::CANTIDAD:
                return $this->cantidad;
            case PartidaArancelariaFormulaFields::FOB:
                return $this->fob;
            case PartidaArancelariaFormulaFields::PESO_BRUTO:
                return $this->pesoBruto;
            case PartidaArancelariaFormulaFields::PESO_NETO:
                return $this->pesoNeto;
            case PartidaArancelariaFormulaFields::TRM:
                return $this->trm;
        }
    }
}