<?php

namespace App\Commons\FormulaComponenteCostoCore;

class FormulaNotResolvedException extends \Exception{
    public function __construct(){
        parent::__construct("No se puede continuar!!, no se ha logrado resolver la logica de el componente costo indicado");
    }
}