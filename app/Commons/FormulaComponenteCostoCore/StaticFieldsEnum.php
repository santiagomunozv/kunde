<?php

namespace App\Commons\FormulaComponenteCostoCore;

use Modules\Comercial\Entities\ListaPrecioDetalleModel;
use Modules\Comercial\Entities\ListaPrecioModel;

abstract class StaticFieldsEnum{
    const __default = self::COSTO_MP;
    const COSTO_MP = "[COSTO MP]";
    const COSTO_MO = "[COSTO MO]";
    const COSTO_CI = "[COSTO CI]";
    const ARANCEL = "[ARANCEL]";
    const FOB_ORIGEN = "[FOB ORIGEN]";
    const FOB_DESTINO = "[FOB DESTINO]";
    const FOB_PESOS = "[FOB PESOS]";
    const TRM = "[TRM]";
    
    static function fields(){
        return [
            self::COSTO_MP,
            self::COSTO_MO,
            self::COSTO_CI,
            self::ARANCEL,
            self::FOB_ORIGEN,
            self::FOB_DESTINO,
            self::FOB_PESOS,
            self::TRM
        ];
    }

    static function fieldsNoBrackets(){
        $toReturn = [];
        foreach(self::fields() as $field){
            array_push($toReturn , preg_replace( "/[\[\]]/", "" ,$field));
        }
        return $toReturn;
    }


    /**
     * Metodo que contiene la logica para determinar cual campo se entregará cuando se solicite uno 
     * de los campos estáticos en el proceso de resolución de fórmulas para las lista 
     * 
     */
    static function homologate($key, ListaPrecioDetalleModel $detalle, ListaPrecioModel $listaPrecio){
        switch($key){
            case self::COSTO_MP: 
                return $detalle->deMaterialesListaPrecioDetalle;
            case self::COSTO_MO:
                return $detalle->deManoObraListaPrecioDetalle;
            case self::COSTO_CI:
                return $detalle->deCostoIndirectoListaPrecioDetalle;
            case self::ARANCEL:
                return $detalle->deGastoAduaneroListaPrecioDetalle;
            case self::FOB_ORIGEN:
                return $detalle->deValorBaseListaPrecioDetalle;
            case self::FOB_DESTINO:
                return $detalle->deValorBaseDestinoListaPrecioDetalle;
            case self::FOB_PESOS:
                return $detalle->deValorBaseLocalListaPrecioDetalle;
            case self::TRM:
                return $listaPrecio->deTasaCambioListaPrecio;
        }
    }

}