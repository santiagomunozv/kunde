<?php

namespace App\Commons\FormulaComponenteCostoCore;

use Modules\General\Entities\ComponenteCostoModel;
use Modules\Comercial\Entities\ListaPrecioDetalleModel;
use Modules\Comercial\Entities\ListaPrecioModel;

class FormulaPrecioWrapper extends AbstractWrapper{
    private $continue = true;
    function __construct(ComponenteCostoModel $componenteCosto = null , array $wrappers){
        $this->componenteCosto = $componenteCosto;
        $this->valor = $componenteCosto ? $componenteCosto->txFormulaPrecio : "0";
        $this->buildFormula($wrappers);
    }

    /**
     * 
     * Metodo encargado de construir la formula del precio final para los detalles listaprecio
     * 
     */
    private function buildFormula($wrappers){
        while($this->continue && !$this->isFormulaOk()){
            $this->continue = false;
            foreach($wrappers as $key => $wrapper){
                $concat = $wrapper->concepto->lsTipoComponenteCostoConcepto == "Porcentaje" ? "/100" : "";
                $changed = $this->updateFormula( $key , "{$wrapper->valor}{$concat}" );
                if(!$this->continue && $changed){
                    $this->continue = true;
                }
            }
        }
    }

    public function resolvePrecioDetalle(ListaPrecioDetalleModel $detalle,ListaPrecioModel $listaPrecio){
        $eval = $this->evalPrecio($detalle , $listaPrecio);
        $detalle->dePrecioVentaListaPrecioDetalle = round($eval, $listaPrecio->lsRedondeoListaPrecio);
    }


    /**
     * Metodo encargado de evaluar el precio final para este componente costo,
     * IMPORTANTE : NO SE DEBERÍA HACER CATCH DE NINGUNA EXCEPTION A ESTE NIVEL,
     * ya que afectaría la logica de los metodos isOk de preciowrapper y de FormulaWrapper
     * generando inconsistencias en toda esta librería
     */
    private function evalPrecio($detalle , ListaPrecioModel $listaPrecio){
        $eval =  eval("return {$this->replaceStaticFields($detalle , $listaPrecio)};") ;
        return $eval;
    }
}