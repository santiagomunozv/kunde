<?php

namespace App\Commons\FormulaComponenteCostoCore;

use Modules\General\Entities\ComponenteCostoConceptoModel;

class FormulaWrapper extends AbstractWrapper{
    function __construct(ComponenteCostoConceptoModel $concepto){
        $this->concepto = $concepto;
        $this->buildValor();
    }

    private function buildValor(){
        switch($this->concepto->lsTipoComponenteCostoConcepto){
            case "Fórmula":
                $this->valor = $this->concepto->txValorComponenteCostoConcepto;
                break;
            default:
                $this->valor = $this->concepto->txValorComponenteCostoConcepto + 0;
                break;
        }
    }
}