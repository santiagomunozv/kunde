<?php

namespace App\Commons\FormulaComponenteCostoCore;

use App\Enums\RedondeoEnum;
use Modules\General\Entities\ComponenteCostoConceptoModel;
use Modules\General\Entities\ComponenteCostoModel;
use Modules\Comercial\Entities\ListaPrecioDetalleModel;
use Modules\Comercial\Entities\ListaPrecioComponenteModel;
use Illuminate\Http\Request;
use Modules\Comercial\Entities\ListaPrecioModel;
use Modules\General\Entities\ComponenteCostoConceptoFactory;

/**
 * Esta clase se ha creado para administrar, validar y resolver las formulas de componentecosto.
 * El orden de ejecución para dicha validacion y resolución se dá de la siguiente forma:
 * - Se resuelven las formulas del detalle componente costo
 * - Se resuelven las formulas del componente costo
 */
class ComponenteCostoFormulaResolver{
    private $continue = true;
    public $formulaPrecio;

    public function __construct(){
        $this->sinResolver = [];
        $this->resueltos = [];
    }

    public static function fromIdComponenteCosto($idComponenteCosto){
        $resolver = new self();
        $resolver->componenteCosto = ComponenteCostoModel::find($idComponenteCosto);
        $resolver->buildFields(ComponenteCostoConceptoModel::findByComponenteCostoId($idComponenteCosto)->get());
        $resolver->resolve();
        $resolver->resolveFormulaValor();
        return $resolver;
    }

    public static function fromComponenteCostoRequest(Request $request){
        $resolver = new self();
        $resolver->componenteCosto = new ComponenteCostoModel();
        $resolver->componenteCosto->fill($request->all());
        $resolver->buildFields(ComponenteCostoConceptoFactory::initFromRequest($request));
        $resolver->resolve();
        $resolver->resolveFormulaValor();
        return $resolver;
    }

    public static function fromIdComponenteCostoAndComponentes($idComponenteCosto , $idConcepto , $valueConcepto , $listaPrecioComponentes){
        $resolver = new self();
        $resolver->componenteCosto = ComponenteCostoModel::find($idComponenteCosto);
        $resolver->buildFieldsFromComponentes(ComponenteCostoConceptoModel::findByComponenteCostoId($idComponenteCosto)->get() , $idConcepto , $valueConcepto , $listaPrecioComponentes);
        $resolver->resolve();
        $resolver->resolveFormulaValor();
        return $resolver;
    }


    private function buildFieldsFromComponentes($conceptos , $idConcepto , $valueConcepto , $listaPrecioComponentes){
        $map = [];
        $listaPrecioComponentes->each(function($componente) use (&$map , $idConcepto , $valueConcepto){
            $map[$componente->ComponenteCostoConcepto_oidListaPrecioComponente] = $componente->ComponenteCostoConcepto_oidListaPrecioComponente  == $idConcepto ? $valueConcepto : $componente->deValorListaPrecioComponente;
        });
        $conceptos->each(function($concepto) use ($map){
            if($concepto->lsTipoComponenteCostoConcepto != 'Fórmula'){
                $concepto->txValorComponenteCostoConcepto = $map[$concepto->oidComponenteCostoConcepto];
            }
            $this->resolveWrapper($concepto);
        });
    }

    private function resolveWrapper($concepto){
        $wrapper = new  FormulaWrapper($concepto);
        if($wrapper->isFormulaOk()){
            $this->resueltos[$concepto->txNombreComponenteCostoConcepto] = $wrapper;
        }else{
            $this->sinResolver[$concepto->txNombreComponenteCostoConcepto] = $wrapper;
        }
    }

    /**
     * Metodo encargado de construir los campos básicos de este objeto.
     * En cada concepto detecta si es de tipo valor o de tipo porcentaje, si es porcentaje 
     * se resuelve el porcentaje x/100 o si es de tipo valor simplemente se agregan como 
     * resueltos. si es de tipo formula se agrega como no resuelto
     */
    private function buildFields($conceptos){
        $conceptos->each(function($concepto){
            $this->resolveWrapper($concepto);
        });
    }

    private function resolve(){
        while($this->continue && count($this->sinResolver) > 0){
            $this->continue = false;
            foreach($this->sinResolver as $wrapper){
                foreach($this->resueltos as $key => $value){
                    $concat = $value->concepto->lsTipoComponenteCostoConcepto == "Porcentaje" ? "/100" : "";
                    $changed = $wrapper->updateFormula( $key , "{$value->valor}{$concat}");
                    if(!$this->continue && $changed){
                        $this->continue = true;
                    }
                }
            }
            $this->checkForCompleted();
        }
    }


    /**
     * metodo que contiene la logica para verificar si alguno de los items 
     * dentro de "sinResolver" puede ser pasado hacia el array de "resueltos"
     */
    private function checkForCompleted(){
        $toSwitch = collect();      
        foreach($this->sinResolver as $key => $wrapper){
            if($wrapper->isFormulaOk()){
                $toSwitch->push($key);
                $this->resueltos[$key] = $wrapper;
            }
        }
        $toSwitch->each(function($key){
            unset($this->sinResolver[$key]);
        });
    }

    /**
     * Metodo que resuelve la formula del precio final
     */
    private function resolveFormulaValor(){
        if(count($this->sinResolver )== 0){
            $this->formulaPrecio = new FormulaPrecioWrapper($this->componenteCosto , $this->resueltos);
        }
    }

    public function setPrecioDetalle(ListaPrecioDetalleModel $detalle,ListaPrecioModel $listaPrecio){
        $this->formulaPrecio->resolvePrecioDetalle($detalle, $listaPrecio);
    }

    public function updateListaPrecioComponente(ListaPrecioComponenteModel $componente , ListaPrecioDetalleModel $detalle , ListaPrecioModel $listaPrecio){
        foreach($this->resueltos as $wrapper){
            if($wrapper->concepto->oidComponenteCostoConcepto == $componente->ComponenteCostoConcepto_oidListaPrecioComponente){
                $componente->deValorListaPrecioComponente = $wrapper->resolveForDetalle($detalle, $listaPrecio);
                return true;
            }
        }
        return false;
    }

    /**
     * Función que indica si se ha logrado resolver todas las formulas para el componente costo indicado
     * IMPORTANTE EL ORDEN DE LOS CONDICIONALES ya que si se realiza en diferente orden podría generar
     * inconsistencias en las validaciones.
     * Se recomienda llamar este metodo antes de llamar {@see ComponenteCostoFormulaResolver#updateListaPrecioComponente(ListaPrecioComponenteModel $componente , ListaPrecioDetalleModel $detalle)}
     * y no debe ser incluido en dicho metodo ya que se genera un cliclo infinito.
     * 
     * El orden recomendado para las validaciones es siguiendo el orden de la logica de esta libreria
     * - validar si se han resuelto los conceptos del componente costo
     * - validar si se establecio el wrapper para formulaprecio
     * - validar si se logra resolver la formula del precio basado en los conceptos.
     */
    public function isResolverPassed(){
        return count($this->sinResolver) == 0 && $this->formulaPrecio &&  $this->isOkFormulaPrecio();
    }

    /**
     * 
     * 
     */
    private function isOkFormulaPrecio(){
        //en este punto se crean una listaprecios ficticia y un detalle listaprecio ficticio,
        //esto para realizar una simulación de reemplazo para los campos de las formulas
        $listaPrecio = new ListaPrecioModel();
        $detalle = new ListaPrecioDetalleModel();
        //preparar campos ficticios con valores ficticios para cada objeto
        $listaPrecio->deTasaCambioListaPrecio = 3000;
        $listaPrecio->lsRedondeoListaPrecio = RedondeoEnum::DOS_DECIMALES;

        $detalle->deMaterialesListaPrecioDetalle = 1;
        $detalle->deManoObraListaPrecioDetalle = 2;
        $detalle->deCostoIndirectoListaPrecioDetalle = 3;
        $detalle->deGastoAduaneroListaPrecioDetalle = 4;
        $detalle->deValorBaseLocalListaPrecioDetalle = 5;
        $detalle->deValorBaseDestinoListaPrecioDetalle = 6;
        $detalle->deValorBaseListaPrecioDetalle = 7;

        try{
            $this->setPrecioDetalle( $detalle , $listaPrecio);
            return true;
        }catch(\ParseError $e){
            return false;
        }catch(FormulaNotResolvedException $e){
            return false;
        }
    }
}