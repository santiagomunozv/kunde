<?php

namespace App\Commons\FormulaComponenteCostoCore;

use Modules\Comercial\Entities\ListaPrecioDetalleModel;
use Modules\Comercial\Entities\ListaPrecioModel;

abstract class AbstractWrapper{
    public $valor;

    public function updateFormula($strReplace , $newValue){
        $previousValue = $this->valor;
        $strReplace = "[$strReplace]";
        $this->valor = str_replace($strReplace , $newValue , $this->valor);
        return $this->valor != $previousValue;
    }

    public function isFormulaOk(){
        try{
            //se testea si la formula está bien sobre otra variable 
            //esto de permitir que permanezcan los valores estaticos
            //en la formula @see StaticFieldsEnum.php
            $testValue = $this->valor;
            foreach(StaticFieldsEnum::fields() as $field){
                $testValue = str_replace($field , "1" , $testValue);
            }
            $testValue = eval("return $testValue;");
            return true;
        }catch(\ParseError $e){
            return false;
        }catch(\Exception $e){
            return false;
        }
    }

    public function resolveForDetalle(ListaPrecioDetalleModel $detalle, ListaPrecioModel $listaPrecio){
        try{
            return eval("return {$this->replaceStaticFields($detalle,$listaPrecio)};");
        }catch(\ParseError $e){
            return 0;
        }
    }

    protected function replaceStaticFields(ListaPrecioDetalleModel $detalle , ListaPrecioModel $listaPrecio){
        $value = $this->valor;
        foreach(StaticFieldsEnum::fields() as $field){
            $value = str_replace($field , StaticFieldsEnum::homologate($field,$detalle , $listaPrecio) , $value);
        }
        return $value;
    }
}