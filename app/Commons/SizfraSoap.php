<?php

namespace App\Commons;

class SizfraSoap
{
    public static function ejecutarServicioSizfra($servicio, $datos)
    {    
        $cliente = new \nusoap_client("http://sizfra.zonafrancarionegro.co:8090/ServiciosSizfra.asmx?wsdl", "wsdl");
        //PARAMETROS REALES PARA MOVIMIENTOS EN SIZFRA
        $parametros = array('zonaFranca' => ENV('SIZFRA_ZONFRANCA','917'), 
                            'empresa' => ENV('SIZFRA_EMPRESA','169') , 
                            'usuario' => ENV('SIZFRA_USUARIO','1691036941769'), 
                            'password' => ENV('SIZFRA_PASSWORD','Josearias1823'), 
                            'baseDatos' => ENV('SIZFRA_BASEDATOS','prod'));

        //PARAMETROS DE SIZFRA PARA HACER PRUEBAS
        // $parametros = array('zonaFranca' => '917', 
        //                     'empresa' => '169', 
        //                     'usuario' => '1691036941769', 
        //                     'password' => 'Josearias1823', 
        //                     'baseDatos' => 'test');
        
        foreach ($datos as $key => $value) {
            $parametros[$key] = $value;
        }

        $respuesta = $cliente->call($servicio, $parametros);
        if ($cliente->fault)
                return $respuesta;
            else 
            {   
                $err = $cliente->getError();
                if ($err)
                    return "<h2>Error</h2><pre> . $err . </pre>";
                else
                    return $respuesta[$servicio.'Result'];
            }
        
        return $respuesta[$servicio.'Result'];
    }
}