<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Seg_PaqueteModel extends Model 
{
    protected $table = 'seg_paquete';
    protected $primaryKey = 'oidPaquete';
    protected $fillable = ['inOrdenPaquete', 'txNombrePaquete','txIconoPaquete'];
    public $timestamps = false;

    public function modulos(){
        return $this->hasMany('App\Seg_ModuloModel','Paquete_oidPaquete_1aM');
    }
}
